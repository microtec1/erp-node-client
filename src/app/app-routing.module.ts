import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './common/services/auth/guards/auth.guard';
import { SidebarLayoutComponent } from './common/components/layout/sidebar-layout/sidebar-layout.component';
import { NoSidebarComponent } from './common/components/layout/no-sidebar/no-sidebar.component';

const routes: Routes = [
  { path: '', redirectTo: 'account', pathMatch: 'full' },
  {
    path: '',
    component: SidebarLayoutComponent,
   
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('./modules/home/home.module').then(mod => mod.HomeModule)
      },
      {
        path: 'notifications',
        loadChildren: () =>
          import('./modules/notifications/notifications.module').then(mod => mod.NotificationsModule)
      },
      {
        path: 'accounting',
        loadChildren: () =>
          import('./modules/accounting/accounting.module').then(mod => mod.AccountingModule)
      },
      {
        path: 'inventory',
        loadChildren: () =>
          import('./modules/inventory/inventory.module').then(mod => mod.InventoryModule)
      },
      {
        path: 'purchases',
        loadChildren: () =>
          import('./modules/purchases/purchases.module').then(mod => mod.PurchasesModule)
      },
      {
        path: 'sales',
        loadChildren: () =>
          import('./modules/sales/sales.module').then(mod => mod.SalesModule)
      },

      {
        path: 'pos',
        loadChildren: () =>
          import('./modules/pos/pos.module').then(mod => mod.PosModule)
      },
      {
        path: 'general',
        loadChildren: () =>
          import('./modules/general/general.module').then(mod => mod.GeneralModule)
      },

      {
        path: 'gl',
        loadChildren: () =>
          import('./modules/accounting/modules/gl/gl.module').then(mod => mod.GlModule)
      },
      {
        path: 'financialAnalysis',
        loadChildren: () =>
          import('./modules/accounting/modules/financial-analysis/financial-analysis.module')
            .then(mod => mod.FinancialAnalysisModule)
      },
      {
        path: 'financialTransactions',
        loadChildren: () =>
          import('./modules/accounting/modules/financial-transactions/financial-transactions.module')
            .then(mod => mod.FinancialTransactionsModule)
      },
      {
        path: 'cheques',
        loadChildren: () =>
          import('./modules/accounting/modules/cheques/cheques.module').then(mod => mod.ChequesModule)
      }
    ]
  },
  {
    path: '',
    component: NoSidebarComponent,
    children: [
      {
        path: 'account',
        loadChildren: () =>
          import('./modules/account/account.module').then(mod => mod.AccountModule)
      }
    ]
  },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
