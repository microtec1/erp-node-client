import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { UiService } from './common/services/ui/ui.service';
import { DOCUMENT } from '@angular/common';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { arLocale, enGbLocale } from 'ngx-bootstrap/locale';
import { DataService } from './common/services/shared/data.service';
import { getGlSettingsApi, getInventorySettingsApi, getSalesSettingsApi, getPurchasesSettingsApi } from './common/constants/api.constants';
import { IGlAccountSettings } from './modules/accounting/modules/gl/interfaces/ISettings';
import { AuthService } from './common/services/auth/auth.service';
import { IInventorySettings } from './modules/inventory/interfaces/IInventorySettings';
import { ISalesSettings } from './modules/sales/interfaces/ISalesSettings';
import { IPurchasesSettings } from './modules/purchases/interfaces/IPurchasesSettings';
defineLocale('ar', arLocale);
defineLocale('en', enGbLocale);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Microtec';
  currentLang: string;
  collapse: boolean;
  loading: boolean;

  constructor(
    private translate: TranslateService,
    private uiService: UiService,
    private data: DataService,
    private auth: AuthService,
    private localeService: BsLocaleService,
    @Inject(DOCUMENT) private document: Document
  ) {
    translate.addLangs(['en', 'ar']);
    translate.setDefaultLang('ar');
    localStorage.setItem('userLang', 'ar');
    const userLang = localStorage.getItem('userLang');
    translate.use(userLang);

    // const browserLang = translate.getBrowserLang();
    // translate.use(browserLang.match(/ar|en/) ? browserLang : 'ar');
  }

  ngOnInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang;
      localStorage.setItem('userLang', event.lang);
      const bodyDir = this.currentLang !== 'ar' ? 'ltr' : 'rtl';
      const bodyClass = this.currentLang !== 'ar' ? 'ltr' : 'rtl';
      this.document.body.setAttribute('dir', bodyDir);
      this.document.body.removeAttribute('class');
      this.document.body.classList.add(bodyClass);
    });

    this.uiService.isCollapsed.subscribe(
      collapse => (this.collapse = collapse)
    );
    this.uiService.isLoading.subscribe(loading => this.loading = loading);
    if (this.auth.isAuthenticated()) {
      this.data.post(getGlSettingsApi, {}).subscribe((res: IGlAccountSettings) => {
        const settings = JSON.stringify(res);
        localStorage.setItem('glSettings', settings);
        this.uiService.isLoading.next(false);
      });
      this.data.post(getInventorySettingsApi, {}).subscribe((res: IInventorySettings) => {
        const settings = JSON.stringify(res);
        localStorage.setItem('inventorySettings', settings);
        this.uiService.isLoading.next(false);
      });
      this.data.post(getSalesSettingsApi, {}).subscribe((res: ISalesSettings) => {
        const settings = JSON.stringify(res);
        localStorage.setItem('salesSettings', settings);
        this.uiService.isLoading.next(false);
      });
      this.data.post(getPurchasesSettingsApi, {}).subscribe((res: IPurchasesSettings) => {
        const settings = JSON.stringify(res);
        localStorage.setItem('purchasesSettings', settings);
        this.uiService.isLoading.next(false);
      });
    }
  }
}
