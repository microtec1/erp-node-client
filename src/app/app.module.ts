import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import {
  HttpClientModule,
  HttpClient,
  HTTP_INTERCEPTORS,
} from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { AgGridModule } from "ag-grid-angular";

// Modules
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { ModalModule } from "ngx-bootstrap/modal";
import { SharedModule } from "./modules/shared/shared.module";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { TabsModule } from "ngx-bootstrap/tabs";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { NgbDatepickerModule } from "@ng-bootstrap/ng-bootstrap";

// Services
import { AuthService } from "./common/services/auth/auth.service";
import { AuthGuard } from "./common/services/auth/guards/auth.guard";
import { UiService } from "./common/services/ui/ui.service";
import { CookieService } from "ngx-cookie-service";

// Components
import { HeaderComponent } from "./common/components/layout/header/header.component";
import { SidebarLayoutComponent } from "./common/components/layout/sidebar-layout/sidebar-layout.component";
import { NoSidebarComponent } from "./common/components/layout/no-sidebar/no-sidebar.component";
import { SidebarComponent } from "./common/components/layout/sidebar/sidebar.component";
import { LoaderComponent } from "./common/components/loader/loader.component";
import { HasCompanyGuard } from "./common/services/auth/guards/hasCompany.guard";
import { TokenInterceptor } from "./common/services/auth/token.interceptor";
import { LoggedGuard } from "./common/services/auth/guards/logged.guard";

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "./assets/i18n/", ".json");
}

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarLayoutComponent,
    NoSidebarComponent,
    SidebarComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    PerfectScrollbarModule,
    NgbDatepickerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),

    // AgGridModule.withComponents([]),

    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
  ],
  providers: [
    AuthService,
    AuthGuard,
    LoggedGuard,
    HasCompanyGuard,
    UiService,
    CookieService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
