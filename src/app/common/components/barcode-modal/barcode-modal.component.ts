import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-barcode-modal',
  templateUrl: './barcode-modal.component.html',
  styleUrls: ['./barcode-modal.component.scss']
})
export class BarcodeModalComponent implements OnInit {
  barCodes: any[] = [];
  selectedBarCode: any;
  selected: number;
  @Output() confirmed: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  select(item, index) {
    this.selectedBarCode = item;
    this.selected = index;
  }

  confirm() {
    this.confirmed.emit(this.selectedBarCode);
    this.selectedBarCode = null;
  }

  decline() {
    this.confirmed.emit(false);
  }

}
