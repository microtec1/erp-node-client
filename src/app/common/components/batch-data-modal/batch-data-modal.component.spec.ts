import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchDataModalComponent } from './batch-data-modal.component';

describe('BatchDataModalComponent', () => {
  let component: BatchDataModalComponent;
  let fixture: ComponentFixture<BatchDataModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchDataModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
