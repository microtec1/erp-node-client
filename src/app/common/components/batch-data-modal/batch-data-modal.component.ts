import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { storesItemsApi } from 'src/app/common/constants/api.constants';
import { IStoreItem } from '../../../modules/inventory/interfaces/IStoreItem';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/modules/general/services/general.service';

@Component({
  selector: 'app-batch-data-modal',
  templateUrl: './batch-data-modal.component.html',
  styleUrls: ['./batch-data-modal.component.scss']
})
export class BatchDataModalComponent implements OnInit {
  itemId: string;
  batchForm: FormGroup;
  formReady: boolean;
  submitted: boolean;
  request: any;
  @Output() formValue: EventEmitter<any> = new EventEmitter();
  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
  ) { }

  ngOnInit() {
    this.data.get(storesItemsApi, null, null, this.itemId).subscribe((res: IStoreItem) => {
      this.formReady = true;
      this.initForm();
      if (res.otherInformation.workBatchType !== 'serialNumber') {
        for (const item of this.getItemBatchDetailsArray.controls) {
          const group = item as FormGroup;
          group.controls.batchDateGregorian.setValidators(Validators.required);
          group.controls.batchDateGregorian.updateValueAndValidity();
          group.controls.batchDateHijri.setValidators(Validators.required);
          group.controls.batchDateHijri.updateValueAndValidity();
          group.controls.expiryDateGregorian.setValidators(Validators.required);
          group.controls.expiryDateGregorian.updateValueAndValidity();
          group.controls.expiryDateHijri.setValidators(Validators.required);
          group.controls.expiryDateHijri.updateValueAndValidity();
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  decline() {
    this.formValue.emit(false);
  }

  get form() {
    return this.batchForm.controls;
  }

  get getItemBatchDetailsArray() {
    return this.batchForm.get('itemBatchDetails') as FormArray;
  }

  addItem() {
    const control = this.batchForm.get('itemBatchDetails') as FormArray;
    control.push(
      new FormGroup({
        batchNumber: new FormControl('', Validators.required),
        quantity: new FormControl(null, Validators.required),
        batchDateGregorian: new FormControl(null),
        batchDateHijri: new FormControl(null),
        expiryDateGregorian: new FormControl(null),
        expiryDateHijri: new FormControl(null)
      })
    );
  }

  deleteItem(index) {
    const control = this.batchForm.get('itemBatchDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.batchForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.batchForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  submit() {
    this.submitted = true;
    if (this.batchForm.valid) {
      this.formValue.emit(this.batchForm.value);
    }
  }

  private initForm() {

    let itemBatchDetailsArray = new FormArray([
      new FormGroup({
        batchNumber: new FormControl('', Validators.required),
        quantity: new FormControl(null, Validators.required),
        batchDateGregorian: new FormControl(''),
        batchDateHijri: new FormControl(''),
        expiryDateGregorian: new FormControl(''),
        expiryDateHijri: new FormControl('')
      })
    ])

    if (this.request.length) {
      itemBatchDetailsArray = new FormArray([]);
      for (const item of this.request) {
        itemBatchDetailsArray.push(
          new FormGroup({
            batchNumber: new FormControl(item.batchNumber, Validators.required),
            quantity: new FormControl(item.quantity, Validators.required),
            batchDateGregorian: new FormControl(new Date(item.batchDateGregorian + 'UTC')),
            batchDateHijri: new FormControl(item.batchDateHijri),
            expiryDateGregorian: new FormControl(new Date(item.expiryDateGregorian + 'UTC')),
            expiryDateHijri: new FormControl(item.batchDateHijri),
          })
        );
      }

    }
    this.batchForm = new FormGroup({
      itemBatchDetails: itemBatchDetailsArray
    });
  }

}
