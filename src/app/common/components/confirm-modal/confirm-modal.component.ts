import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IUser } from 'src/app/modules/general/interfaces/IUser.model';
import { CostCentersService } from 'src/app/modules/accounting/modules/gl/services/cost-centers.service';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {
  id: string;
  code: string;
  nameAr: string;
  nameEn: string;
  user: IUser;

  @Output() confirmed: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private CCService: CostCentersService
  ) { }

  ngOnInit() {
    // const searchBody = {
    //   parentId: this.id
    // };
    // this.CCService.getAllCostCenters(searchBody).subscribe(res => {
    //   console.log(res);
    // });
  }

  confirm() {
    this.confirmed.emit(true);
  }

  decline() {
    this.confirmed.emit(false);
  }

}
