import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-entries-modal',
  templateUrl: './entries-modal.component.html',
  styleUrls: ['./entries-modal.component.scss']
})
export class EntriesModalComponent implements OnInit {
  entries: any[] = [];
  selected: number;
  @Output() confirmed: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  select(item, index) {
    this.selected = index;
  }

  confirm() {
    this.confirmed.emit(this.selected);
  }

  decline() {
    this.confirmed.emit(false);
  }

}
