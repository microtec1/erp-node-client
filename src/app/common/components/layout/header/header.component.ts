import { Component, OnInit } from '@angular/core';
import { UiService } from 'src/app/common/services/ui/ui.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  otherLang: string;
  currentCompany: string;
  currentBranch: string;
  collapseSidebar = true;
  constructor(
    public uiService: UiService
    ) {
    this.uiService.isCollapsed.subscribe(res => {
      if (res === false) {
        this.collapseSidebar = true;
      } else {
        this.collapseSidebar = false;
      }
    });
  }

  ngOnInit() {
    const currentCompany = JSON.parse(localStorage.getItem('company'));
    const currentBranch = JSON.parse(localStorage.getItem('branch'));
    this.currentCompany = currentCompany.companyNameAr;
    this.currentBranch = currentBranch.branchNameAr;
  }

  changeLang() {
    this.uiService.changeLang();
  }

  toggleSidebar() {
    this.uiService.toggleSidebar(this.collapseSidebar);
  }
}
