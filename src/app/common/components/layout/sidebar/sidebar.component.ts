import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/common/services/auth/auth.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { IUser } from 'src/app/modules/general/interfaces/IUser.model';
import { Subscription } from 'rxjs';
import { companyId } from 'src/app/common/constants/general.constants';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  userName: any;
  subscriptions: Subscription[] = [];
  superAdmin: boolean ;
  constructor(
    private authService: AuthService,
    private uiService: UiService
  ) { }

  ngOnInit() {
    this.userName = localStorage.getItem('userName');
  }

  toggleClass(el) {
    const targetClass = 'opened';
    if (el.srcElement.className === targetClass) {
      el.srcElement.className = '';
    } else {
      el.srcElement.className = targetClass;
    }
  }

  closeSidebar() {
    this.uiService.isCollapsed.next(false);
  }

  logout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
