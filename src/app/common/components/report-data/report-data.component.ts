import { Component, ViewChild,Input, Output, EventEmitter } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { Router } from '@angular/router';
import { AccountReportsService } from 'src/app/modules/accounting/modules/gl/services/account-reports.service';


@Component({
  selector: 'app-report-data',
  templateUrl: './report-data.component.html',
  styleUrls: ['./report-data.component.scss']
})
export class ReportDataComponent {
  @Input('tablesData') tablesData:[];
  @Input('defaultColDef') defaultColDef:[];
  @Input('columnDefs') columnDefs:[];
  @Input('pageSize') pageSize
  @Input('p') p ; //current page
  @Input('total') total;
  @Input('hideButton') hideButton : boolean = true ;
  @Input('costCenter') costCenter : boolean = false ;
  @Output() pageChanged = new EventEmitter();
  
  @ViewChild("agGrid", {
    static: true
  })

  agGrid: AgGridAngular;
  paginationConfig :any ;
  private gridApi ;
  private gridColumnApi;
  
  
constructor(private router:Router,private accountReport : AccountReportsService) {}

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    // let allColIds = this.gridColumnApi.getAllColumns().map(column => column.colId);
    // this.gridColumnApi.autoSizeColumns(allColIds,false);
    this.gridApi.sizeColumnsToFit();
    //this.gridApi.setDomLayout('print');

  }

  pageChangeFired(e){
    
    this.pageChanged.emit(e);
    
  }

  redirect(data){
    this.router.navigateByUrl('/account/print/')
    this.accountReport.setTableLayoutToPrint(data)
  }
}
