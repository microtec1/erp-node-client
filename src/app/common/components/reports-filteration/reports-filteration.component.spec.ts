import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsFilterationComponent } from './reports-filteration.component';

describe('ReportsFilterationComponent', () => {
  let component: ReportsFilterationComponent;
  let fixture: ComponentFixture<ReportsFilterationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsFilterationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsFilterationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
