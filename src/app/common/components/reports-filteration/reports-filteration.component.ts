import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, Renderer2, ElementRef, AfterViewChecked } from '@angular/core';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import {
  detailedChartOfAccountsApi,
  detailedCostCentersApi,
  safeBoxesApi, banksApi,
  branchCurrenciesApi,
  beneficiariesApi,
  companiesApi,
  customersApi,
  detailedCustomersClassificationsApi,
  customersGroupsApi,
  regionsApi,
  citiesApi,
  nationalitiesApi,
  suppliersApi,
  employeesApi,
  suppliersGroupsApi,
  detailedSuppliersClassificationsApi,
  storesItemsApi,
  itemsClassificationsApi,
  itemsVariablesApi,
  itemsTypesApi,
  itemsUnitsApi,
  itemsFamiliesApi,
  warehousesApi,
  itemsGroupsApi,
  servicesItemsGroupsApi,
  servicesItemsApi,
  projectsApi,
  checkReasonsRejectionApi,
  countriesApi,
  chartOfAccountsApi,
  costCentersApi
} from '../../constants/api.constants';
import { Subscription } from 'rxjs';
import { companyId, searchLength } from '../../constants/general.constants';
import { DataService } from '../../services/shared/data.service';
import { UiService } from '../../services/ui/ui.service';
import { ICostCenter } from 'src/app/modules/accounting/modules/gl/interfaces/ICostCenter';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IBeneficiary } from 'src/app/modules/accounting/modules/cheques/interfaces/IBeneficiary';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IDataRes } from '../../interfaces/IDataResponse.model';
import { IBranch } from 'src/app/modules/general/interfaces/IBranch';
import { ICompany } from 'src/app/modules/general/interfaces/ICompany';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { ICustomersClassification } from 'src/app/modules/sales/interfaces/ICustomersClassification';
import { ICustomersGroup } from 'src/app/modules/sales/interfaces/ICustomersGroup';
import { INationality } from 'src/app/modules/general/interfaces/INationality';
import { IRegion } from 'src/app/modules/general/interfaces/IRegion.model';
import { ICity } from 'src/app/modules/general/interfaces/ICity.model';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';
import { ISupplierCLassification } from 'src/app/modules/purchases/interfaces/ISupplierCLassification.model';
import { ISuppliersGroup } from 'src/app/modules/purchases/interfaces/ISuppliersGroup.model';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IItemsClassification } from 'src/app/modules/inventory/interfaces/IItemsClassification';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { IItemsType } from 'src/app/modules/inventory/interfaces/IItemsType';
import { IItemsFamily } from 'src/app/modules/inventory/interfaces/IItemsFamily';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { IItemsGroup } from 'src/app/modules/inventory/interfaces/IItemsGroup';
import { IServicesItemsGroup } from 'src/app/modules/inventory/interfaces/IServicesItemsGroup';
import { IServicesItem } from 'src/app/modules/inventory/interfaces/IServicesItem';
import { IProject } from 'src/app/modules/purchases/interfaces/IProject.model';
import { ICheckReasonRejection } from 'src/app/modules/accounting/modules/cheques/interfaces/ICheckReasonRejection';
import { ICountry } from 'src/app/modules/general/interfaces/ICountry.model';
import { FormGroup, FormControl } from '@angular/forms';
import { NumbersOnlyDirective } from 'src/app/modules/accounting/modules/gl/directives/numbers-only.directive';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import { from } from "rxjs";
import { AccountReportsService } from 'src/app/modules/accounting/modules/gl/services/account-reports.service';

@Component({
  selector: 'app-reports-filteration',
  templateUrl: './reports-filteration.component.html',
  styleUrls: ['./reports-filteration.component.scss']
})
export class ReportsFilterationComponent implements OnInit, OnDestroy,AfterViewChecked {
  subscriptions: Subscription[] = [];
  companyId = companyId;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Cost Centers
  costCenters: ICostCenter[] = [];
  costCentersInputFocused: boolean;
  costCentersCount: number;
  noCostCenters: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;


  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Beneficiary
  beneficiaries: IBeneficiary[] = [];
  beneficiariesInputFocused: boolean;
  hasMoreBeneficiaries: boolean;
  beneficiariesCount: number;
  selectedBeneficiariesPage = 1;
  beneficiariesPagesNo: number;
  noBeneficiaries: boolean;

  // Branches
  branches: IBranch[] = [];
  branchesInputFocused: boolean;
  branchesCount: number;
  noBranches: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Customers Classifications
  customersClassifications: ICustomersClassification[] = [];
  customersClassificationsInputFocused: boolean;
  customersClassificationsCount: number;
  noCustomersClassifications: boolean;

  // Cities
  cities: ICity[] = [];
  citiesInputFocused: boolean;
  hasMoreCities: boolean;
  citiesCount: number;
  selectedCitiesPage = 1;
  citiesPagesNo: number;
  noCities: boolean;

  // Regions
  regions: IRegion[] = [];
  regionsInputFocused: boolean;
  hasMoreRegions: boolean;
  regionsCount: number;
  selectedRegionsPage = 1;
  regionsPagesNo: number;
  noRegions: boolean;

  // Customers Groups
  customersGroups: ICustomersGroup[] = [];
  customersGroupsInputFocused: boolean;
  hasMoreCustomersGroups: boolean;
  customersGroupsCount: number;
  selectedCustomersGroupsPage = 1;
  customersGroupsPagesNo: number;
  noCustomersGroups: boolean;

  // Nationalities
  nationalities: INationality[] = [];
  nationalitiesInputFocused: boolean;
  hasMoreNationalities: boolean;
  nationalitiesCount: number;
  selectedNationalitiesPage = 1;
  nationalitiesPagesNo: number;
  noNationalities: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  // Employees
  employees: IEmployee[] = [];
  employeesInputFocused: boolean;
  hasMoreEmployees: boolean;
  employeesCount: number;
  selectedEmployeesPage = 1;
  employeesPagesNo: number;
  noEmployees: boolean;

  // Suppliers Classifications
  suppliersClassifications: ISupplierCLassification[] = [];
  suppliersClassificationsInputFocused: boolean;
  suppliersClassificationsCount: number;
  noSuppliersClassifications: boolean;

  // Suppliers Groups
  suppliersGroups: ISuppliersGroup[] = [];
  suppliersGroupsInputFocused: boolean;
  hasMoreSuppliersGroups: boolean;
  suppliersGroupsCount: number;
  selectedSuppliersGroupsPage = 1;
  suppliersGroupsPagesNo: number;
  noSuppliersGroups: boolean;

  // Sales Representatives
  salesRepresentatives: IEmployee[] = [];
  salesRepresentativesInputFocused: boolean;
  salesRepresentativesCount: number;
  noSalesRepresentatives: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Items Types
  itemsTypes: IItemsType[] = [];
  itemsTypesInputFocused: boolean;
  hasMoreItemsTypes: boolean;
  itemsTypesCount: number;
  selectedItemsTypesPage = 1;
  itemsTypesPagesNo: number;
  noItemsTypes: boolean;

  // Items Classifications
  itemsClassifications: IItemsClassification[] = [];
  itemsClassificationsInputFocused: boolean;
  hasMoreItemsClassifications: boolean;
  itemsClassificationsCount: number;
  selectedItemsClassificationsPage = 1;
  itemsClassificationsPagesNo: number;
  noItemsClassifications: boolean;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Items Groups
  itemsGroups: IItemsGroup[] = [];
  itemsGroupsInputFocused: boolean;
  hasMoreItemsGroups: boolean;
  itemsGroupsCount: number;
  selectedItemsGroupsPage = 1;
  itemsGroupsPagesNo: number;
  noItemsGroups: boolean;

  // Items Families
  itemsFamilies: IItemsFamily[] = [];
  itemsFamiliesInputFocused: boolean;
  hasMoreItemsFamilies: boolean;
  itemsFamiliesCount: number;
  selectedItemsFamiliesPage = 1;
  itemsFamiliesPagesNo: number;
  noItemsFamilies: boolean;

  // Items Units
  itemsUnits: IItemsUnit[] = [];
  itemsUnitsInputFocused: boolean;
  hasMoreItemsUnits: boolean;
  itemsUnitsCount: number;
  selectedItemsUnitsPage = 1;
  itemsUnitsPagesNo: number;
  noItemsUnits: boolean;

  // Services Items Groups
  servicesItemsGroups: IServicesItemsGroup[] = [];
  servicesItemsGroupsInputFocused: boolean;
  hasMoreServicesItemsGroups: boolean;
  servicesItemsGroupsCount: number;
  selectedServicesItemsGroupsPage = 1;
  servicesItemsGroupsPagesNo: number;
  noServicesItemsGroups: boolean;

  // Services Items
  servicesItems: IServicesItem[] = [];
  servicesItemsInputFocused: boolean;
  hasMoreServicesItems: boolean;
  servicesItemsCount: number;
  selectedServicesItemsPage = 1;
  servicesItemsPagesNo: number;
  noServicesItems: boolean;

  // Projects
  projects: IProject[] = [];
  projectsInputFocused: boolean;
  hasMoreProjects: boolean;
  projectsCount: number;
  selectedProjectsPage = 1;
  projectsPagesNo: number;
  noProjects: boolean;

  // Rejection Reasons
  rejectionReasons: ICheckReasonRejection[] = [];
  rejectionReasonsInputFocused: boolean;
  hasMoreRejectionReasons: boolean;
  rejectionReasonsCount: number;
  selectedRejectionReasonsPage = 1;
  rejectionReasonsPagesNo: number;
  noRejectionReasons: boolean;

  // Countries
  countries: ICountry[] = [];
  countriesInputFocused: boolean;
  hasMoreCountries: boolean;
  countriesCount: number;
  selectedCountriesPage = 1;
  countriesPagesNo: number;
  noCountries: boolean;

  
  options:boolean=false;
  grouped:boolean= false ;
  chartOfAccount= [];
  companyBranches = [];
  costCentersData = [] ;
  loading = false;
  pages:number = 0;
  count:number = 0;
  limit:number = 0;
  currentPage:number = 1;
  costCenterPages:number = 0;
  costCenterCount:number = 0;
  costCenterLimit:number = 0;
  costCenterCurrentPage:number = 1;
  isOpened : boolean;
  elementWidth ;
  showAccountRange:boolean = true;
  showBranchRange:boolean = true;
  showCostCentersRange:boolean = true;
  @Input() config: any;
  @Output() submitFilter = new EventEmitter();
  @Input('alwaysAllLevels') alwaysAllLevels: boolean;
  @Input("filterForm") filterForm : FormGroup = new FormGroup({}) ;
  @Input('chartOfAccountsReportType') chartOfAccountsReportType:string;
  @Input('opened')
  set opened(val){
    this.isOpened = val;
  }


  constructor(
    private data: DataService,
    private uiService: UiService,
    private renderer : Renderer2,
    private el : ElementRef,
    private accoutReports:AccountReportsService
  ) { }

  changeChartOfAccountsOption(showAccountRange){
    this.showAccountRange =  (showAccountRange.value == 'false')?false:true;
    this.filterForm.patchValue({from:null,to:null,accountsGroup:null})
  }

  changeChartOfCostCenters(showCostCentersRange){
    this.showCostCentersRange =  (showCostCentersRange.value == 'false')?false:true;
    this.filterForm.patchValue({fromCostCenter:null,toCostCenter:null,costCentersGroup:null})
  }

  changeChartOfBranchOption(showBranchRange){
    this.showBranchRange =  (showBranchRange.value == 'false')?false:true;
    console.log(this.showBranchRange)
    this.filterForm.patchValue({fromBranch:null,toBranch:null,branchGroup:null})
  }

  clearDateOnClick(){
    this.filterForm.patchValue({date:""})
  }

  toggleDrawer(){
    this.isOpened = !this.isOpened;
    this.renderer.setStyle(this.el.nativeElement,'transform',`translateX(${this.isOpened?0:this.elementWidth}px)`)
    
  }

  onSubmit(f){
    this.submitFilter.emit(f.value)
    
  }
  
  showAndHide(e){
    this.grouped = (e == 'false')?false:true;
    this.filterForm.patchValue({from:null,to:null,accountsGroup:null})
  }

  onScrollToEnd() {
    if (this.pages <= this.currentPage ) {
        return;
    }
    else {
        
        this.fetchChartsOfAccount(++this.currentPage)      
    }
  }

  searchApi(term){

    if(!term.searchTerm){

      this.data.postWithoutSpinner(chartOfAccountsApi+"/search",{}).subscribe((res:{results})=>{
        let result = res.results
        this.chartOfAccount = [];
        this.chartOfAccount.push(...result);
        this.chartOfAccount = this.chartOfAccount.slice(0);
      });
    }else{
      let body={
        "OR": [
          {
              "REGEX": {
                  "code": term.searchTerm.toString()
              }
          },
          {
              "REGEX": {
                  "chartOfAccountsNameAr": term.searchTerm.toString()
              }
          }
      ],
        "chartOfAccountsReportType": this.chartOfAccountsReportType || ""
      }
      console.log(body)
      this.accoutReports.removeObjEmptyAttributes(body);
      this.data.postWithoutSpinner(chartOfAccountsApi+"/search",body).subscribe((res:{results})=>{
        let result = res.results
        this.chartOfAccount = [];
        this.chartOfAccount.push(...result);
        this.chartOfAccount = this.chartOfAccount.slice(0);
      });
    }
   
  }

  clearSearchHistory(){
    this.chartOfAccount = []
    this.fetchChartsOfAccount(1);
  }

  clearSearchHistoryForCostCenter(){
    this.costCentersData = []
    this.fetchCostCenter(1);
  }

  onScrollToEndForCostCenter() {
    if (this.costCenterPages <= this.costCenterCurrentPage ) {
        return;
    }
    else {
      
        this.fetchCostCenter(++this.costCenterCurrentPage)      
    }
  }


  private fetchChartsOfAccount(pageNumber){
    let body={
      "chartOfAccountsReportType": this.chartOfAccountsReportType 
    }
    this.accoutReports.removeObjEmptyAttributes(body);
    //this.loading = true;
    this.data.postWithoutSpinner(chartOfAccountsApi+"/search?limit=10&page="+pageNumber,body)
    .subscribe((res:{results,pages,count,limit,currentPage})=>{
      let result = res.results
      this.pages=res.pages;
      this.count=res.count;
      this.limit=res.limit;
      this.currentPage=res.currentPage;
      //this.loading = false;
      this.chartOfAccount.push(...result);
      this.chartOfAccount = this.chartOfAccount.slice(0);
    });
  }

  private fetchBranchs(){
    let companyNameAr :any=JSON.parse(localStorage.getItem('company')).companyNameAr
    console.log("company name " , companyNameAr)
    let body={
      "companyNameAr": companyNameAr 
    }
    this.accoutReports.removeObjEmptyAttributes(body);
    console.log("body " , body)
    //this.loading = true;
    this.data.postWithoutSpinner(companiesApi+"/search?limit=100",body)
    .subscribe((res:{results})=>{
      let result = res.results[0].branches
    
      
      this.companyBranches = result;
      console.log("resuklt " ,result)
    });

  }

  private fetchCostCenter(pageNumber){    
    //this.loading = true;
    this.data.postWithoutSpinner(costCentersApi+"/search?limit=10&page="+pageNumber,{})
    .subscribe((res:{results,pages,count,limit,currentPage})=>{
      let result = res.results
      this.costCenterPages=res.pages;
      this.costCenterCount=res.count;
      this.costCenterLimit=res.limit;
      this.costCenterCurrentPage=res.currentPage;
      //this.loading = false;
      this.costCentersData.push(...result);
      this.costCentersData = this.costCentersData.slice(0);
      console.log(this.costCentersData)
    });
  }
    
  ngAfterViewChecked(){
    if(this.elementWidth){
      return
    }
  
    this.elementWidth = this.el.nativeElement.offsetWidth
    this.renderer.setStyle(this.el.nativeElement,'transform',`translateX(${this.elementWidth})`)

  }
  
  ngOnInit() {

    this.fetchChartsOfAccount(1);
    this.fetchBranchs();
    this.fetchCostCenter(1);


    this.subscriptions.push(
      this.data.get(servicesItemsGroupsApi, 1).subscribe((res: IDataRes) => {
        this.servicesItemsGroupsPagesNo = res.pages;
        this.servicesItemsGroupsCount = res.count;
        if (this.servicesItemsGroupsPagesNo > this.selectedServicesItemsGroupsPage) {
          this.hasMoreServicesItemsGroups = true;
        }
        this.servicesItemsGroups.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(countriesApi, 1).subscribe((res: IDataRes) => {
        this.countriesPagesNo = res.pages;
        this.countriesCount = res.count;
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        }
        this.countries.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(servicesItemsApi, 1).subscribe((res: IDataRes) => {
        this.servicesItemsPagesNo = res.pages;
        this.servicesItemsCount = res.count;
        if (this.servicesItemsPagesNo > this.selectedServicesItemsPage) {
          this.hasMoreServicesItems = true;
        }
        this.servicesItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsGroupsApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsGroupsPagesNo = res.pages;
          this.itemsGroupsCount = res.count;
          if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
            this.hasMoreItemsGroups = true;
          }
          this.itemsGroups.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          this.warehouses.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsFamiliesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsFamiliesPagesNo = res.pages;
          this.itemsFamiliesCount = res.count;
          if (this.itemsFamiliesPagesNo > this.selectedItemsFamiliesPage) {
            this.hasMoreItemsFamilies = true;
          }
          this.itemsFamilies.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsUnitsApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsUnitsPagesNo = res.pages;
          this.itemsUnitsCount = res.count;
          if (this.itemsUnitsPagesNo > this.selectedItemsUnitsPage) {
            this.hasMoreItemsUnits = true;
          }
          this.itemsUnits.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsTypesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsTypesPagesNo = res.pages;
          this.itemsTypesCount = res.count;
          if (this.itemsTypesPagesNo > this.selectedItemsTypesPage) {
            this.hasMoreItemsTypes = true;
          }
          this.itemsTypes.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsClassificationsApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsClassificationsPagesNo = res.pages;
          this.itemsClassificationsCount = res.count;
          if (this.itemsClassificationsPagesNo > this.selectedItemsClassificationsPage) {
            this.hasMoreItemsClassifications = true;
          }
          this.itemsClassifications.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(storesItemsApi, 1)
        .subscribe((res: IDataRes) => {
          this.storeItemsPagesNo = res.pages;
          this.storeItemsCount = res.count;
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          }
          this.storeItems.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(employeesApi, 1).subscribe((res: IDataRes) => {
        this.employeesPagesNo = res.pages;
        this.employeesCount = res.count;
        if (this.employeesPagesNo > this.selectedEmployeesPage) {
          this.hasMoreEmployees = true;
        }
        this.employees.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(nationalitiesApi, 1)
        .subscribe((res: IDataRes) => {
          this.nationalitiesPagesNo = res.pages;
          this.nationalitiesCount = res.count;
          if (this.nationalitiesPagesNo > this.selectedNationalitiesPage) {
            this.hasMoreNationalities = true;
          }
          this.nationalities.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(citiesApi, 1).subscribe((res: IDataRes) => {
        this.citiesPagesNo = res.pages;
        this.citiesCount = res.count;
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        }
        this.cities.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(regionsApi, 1).subscribe((res: IDataRes) => {
        this.regionsPagesNo = res.pages;
        this.regionsCount = res.count;
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        }
        this.regions.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(customersGroupsApi, 1)
        .subscribe((res: IDataRes) => {
          this.customersGroupsPagesNo = res.pages;
          this.customersGroupsCount = res.count;
          if (this.customersGroupsPagesNo > this.selectedCustomersGroupsPage) {
            this.hasMoreCustomersGroups = true;
          }
          this.customersGroups.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.getByRoute(detailedCustomersClassificationsApi).subscribe(
        (res: IDataRes) => {
          if (res.results.length) {
            this.customersClassifications.push(...res.results);
            this.customersClassificationsCount = res.count;
          } else {
            this.noCustomersClassifications = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );

    this.subscriptions.push(
      this.data.get(detailedCostCentersApi, null, null, companyId).subscribe((res: ICostCenter[]) => {
        if (res.length) {
          this.costCenters.push(...res);
          this.costCentersCount = res.length;
        } else {
          this.noCostCenters = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(beneficiariesApi, 1).subscribe((res: IDataRes) => {
        this.beneficiariesPagesNo = res.pages;
        this.beneficiariesCount = res.count;
        if (this.beneficiariesPagesNo > this.selectedBeneficiariesPage) {
          this.hasMoreBeneficiaries = true;
        }
        this.beneficiaries.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    // this.subscriptions.push(
    //   this.data.get(companiesApi, null, null, companyId).subscribe((res: ICompany) => {
    //     if (res.branches.length) {
    //       this.branchesCount = res.branches.length;
    //       this.branches.push(...res.branches);
    //     } else {
    //       this.noBranches = true;
    //     }
    //     this.uiService.isLoading.next(false);
    //   })
    // );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(suppliersGroupsApi, 1)
        .subscribe((res: IDataRes) => {
          this.suppliersGroupsPagesNo = res.pages;
          this.suppliersGroupsCount = res.count;
          if (this.suppliersGroupsPagesNo > this.selectedSuppliersGroupsPage) {
            this.hasMoreSuppliersGroups = true;
          }
          this.suppliersGroups.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.getByRoute(detailedSuppliersClassificationsApi).subscribe(
        (res: IDataRes) => {
          if (res.results.length) {
            this.suppliersClassifications.push(...res.results);
            this.suppliersClassificationsCount = res.count;
          } else {
            this.noSuppliersClassifications = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );
    const searchQuery = {
      salesRepresentative: true,
      companyId
    };
    this.subscriptions.push(
      this.data
        .get(employeesApi, null, searchQuery)
        .subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSalesRepresentatives = true;
          } else {
            this.noSalesRepresentatives = false;
            for (const item of res.results) {
              if (this.salesRepresentatives.length) {
                const uniqueSalesRepresentatives = this.salesRepresentatives.filter(
                  x => x._id !== item._id
                );
                this.salesRepresentatives = uniqueSalesRepresentatives;
              }
              this.salesRepresentatives.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(projectsApi, 1)
        .subscribe((res: IDataRes) => {
          this.projectsPagesNo = res.pages;
          this.projectsCount = res.count;
          if (this.projectsPagesNo > this.selectedProjectsPage) {
            this.hasMoreProjects = true;
          }
          this.projects.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(checkReasonsRejectionApi, 1).subscribe((res: IDataRes) => {
        this.rejectionReasonsPagesNo = res.pages;
        this.rejectionReasonsCount = res.count;
        if (this.rejectionReasonsPagesNo > this.selectedRejectionReasonsPage) {
          this.hasMoreRejectionReasons = true;
        }
        this.rejectionReasons.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  getItems(name) {
    return this[name];
  }

  setTrue(name) {
    this[`${name}InputFocused`] = true;
  }

  setFalse(name) {
    this[`${name}InputFocused`] = false;
  }

  isFocused(name, status?: string) {
    return this[`${name}InputFocused`];
  }

  getlength(name) {
    return this[name].length;
  }

  getCount(name) {
    return this[`${name}Count`];
  }

  capitalize(word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }

  checkMore(name) {
    const value = this.capitalize(name);
    return this[`hasMore${value}`];
  }

  loadMore(name) {
    const value = this.capitalize(name);
    this[`loadMore${value}`]();
  }

  searchBeneficiaries(event) {
    const searchValue = event;
    const searchQuery = {
      beneficiaryNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(beneficiariesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noBeneficiaries = true;
          } else {
            this.noBeneficiaries = false;
            for (const item of res.results) {
              if (this.beneficiaries.length) {
                const uniqueBeneficiaries = this.beneficiaries.filter(x => x._id !== item._id);
                this.beneficiaries = uniqueBeneficiaries;
              }
              this.beneficiaries.push(item);
            }
          }
          this.beneficiaries = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMorebeneficiaries() {
    this.selectedBeneficiariesPage = this.selectedBeneficiariesPage + 1;
    this.subscriptions.push(
      this.data.get(beneficiariesApi, this.selectedBeneficiariesPage).subscribe((res: IDataRes) => {
        if (this.beneficiariesPagesNo > this.selectedBeneficiariesPage) {
          this.hasMoreBeneficiaries = true;
        } else {
          this.hasMoreBeneficiaries = false;
        }
        for (const item of res.results) {
          if (this.beneficiaries.length) {
            const uniqueBeneficiaries = this.beneficiaries.filter(x => x._id !== item._id);
            this.beneficiaries = uniqueBeneficiaries;
          }
          this.beneficiaries.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchRegions(event) {
    const searchValue = event;
    const searchQuery = {
      regionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(regionsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noRegions = true;
            } else {
              this.noRegions = false;
              for (const item of res.results) {
                if (this.regions.length) {
                  const uniqueRegions = this.regions.filter(
                    x => x._id !== item._id
                  );
                  this.regions = uniqueRegions;
                }
                this.regions.push(item);
              }
            }
            this.regions = res.results;
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreRegions() {
    this.selectedRegionsPage = this.selectedRegionsPage + 1;
    this.subscriptions.push(
      this.data
        .get(regionsApi, this.selectedRegionsPage)
        .subscribe((res: IDataRes) => {
          if (this.regionsPagesNo > this.selectedRegionsPage) {
            this.hasMoreRegions = true;
          } else {
            this.hasMoreRegions = false;
          }
          for (const item of res.results) {
            if (this.regions.length) {
              const uniqueRegions = this.regions.filter(
                x => x._id !== item._id
              );
              this.regions = uniqueRegions;
            }
            this.regions.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCities(event) {
    const searchValue = event;
    const searchQuery = {
      cityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(citiesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noCities = true;
            } else {
              this.noCities = false;
              for (const item of res.results) {
                if (this.cities.length) {
                  const uniqueCities = this.cities.filter(
                    x => x._id !== item._id
                  );
                  this.cities = uniqueCities;
                }
                this.cities.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreCities() {
    this.selectedCitiesPage = this.selectedCitiesPage + 1;
    this.subscriptions.push(
      this.data
        .get(citiesApi, this.selectedCitiesPage)
        .subscribe((res: IDataRes) => {
          if (this.citiesPagesNo > this.selectedCitiesPage) {
            this.hasMoreCities = true;
          } else {
            this.hasMoreCities = false;
          }
          for (const item of res.results) {
            if (this.cities.length) {
              const uniqueCities = this.cities.filter(x => x._id !== item._id);
              this.cities = uniqueCities;
            }
            this.cities.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchNationalities(event) {
    const searchValue = event;
    const searchQuery = {
      nationalityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(nationalitiesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noNationalities = true;
            } else {
              this.noNationalities = false;
              for (const item of res.results) {
                if (this.nationalities.length) {
                  const uniqueNationalities = this.nationalities.filter(
                    x => x._id !== item._id
                  );
                  this.nationalities = uniqueNationalities;
                }
                this.nationalities.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreNationalities() {
    this.selectedNationalitiesPage = this.selectedNationalitiesPage + 1;
    this.subscriptions.push(
      this.data
        .get(nationalitiesApi, this.selectedNationalitiesPage)
        .subscribe((res: IDataRes) => {
          if (this.nationalitiesPagesNo > this.selectedNationalitiesPage) {
            this.hasMoreNationalities = true;
          } else {
            this.hasMoreNationalities = false;
          }
          for (const item of res.results) {
            if (this.nationalities.length) {
              const uniqueNationalities = this.nationalities.filter(
                x => x._id !== item._id
              );
              this.nationalities = uniqueNationalities;
            }
            this.nationalities.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCustomersGroups(event) {
    const searchValue = event;
    const searchQuery = {
      customersGroupNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(customersGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noCustomersGroups = true;
            } else {
              this.noCustomersGroups = false;
              for (const item of res.results) {
                if (this.customersGroups.length) {
                  const uniqueCustomersGroups = this.customersGroups.filter(
                    x => x._id !== item._id
                  );
                  this.customersGroups = uniqueCustomersGroups;
                }
                this.customersGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreCustomersGroups() {
    this.selectedCustomersGroupsPage = this.selectedCustomersGroupsPage + 1;
    this.subscriptions.push(
      this.data
        .get(customersGroupsApi, this.selectedCustomersGroupsPage)
        .subscribe((res: IDataRes) => {
          if (this.customersGroupsPagesNo > this.selectedCustomersGroupsPage) {
            this.hasMoreCustomersGroups = true;
          } else {
            this.hasMoreCustomersGroups = false;
          }
          for (const item of res.results) {
            if (this.customersGroups.length) {
              const uniqueCustomersGroups = this.customersGroups.filter(
                x => x._id !== item._id
              );
              this.customersGroups = uniqueCustomersGroups;
            }
            this.customersGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchEmployees(event) {
    const searchValue = event;
    const searchQuery = {
      employeeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(employeesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noEmployees = true;
            } else {
              this.noEmployees = false;
              for (const item of res.results) {
                if (this.employees.length) {
                  const uniqueEmployees = this.employees.filter(
                    x => x._id !== item._id
                  );
                  this.employees = uniqueEmployees;
                }
                this.employees.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreEmployees() {
    this.selectedEmployeesPage = this.selectedEmployeesPage + 1;
    this.subscriptions.push(
      this.data
        .get(employeesApi, this.selectedEmployeesPage)
        .subscribe((res: IDataRes) => {
          if (this.employeesPagesNo > this.selectedEmployeesPage) {
            this.hasMoreEmployees = true;
          } else {
            this.hasMoreEmployees = false;
          }
          for (const item of res.results) {
            if (this.employees.length) {
              const uniqueEmployees = this.employees.filter(x => x._id !== item._id);
              this.employees = uniqueEmployees;
            }
            this.employees.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSuppliersGroups(event) {
    const searchValue = event;
    const searchQuery = {
      suppliersGroupNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(suppliersGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSuppliersGroups = true;
            } else {
              this.noSuppliersGroups = false;
              for (const item of res.results) {
                if (this.suppliersGroups.length) {
                  const uniqueSuppliersGroups = this.suppliersGroups.filter(
                    x => x._id !== item._id
                  );
                  this.suppliersGroups = uniqueSuppliersGroups;
                }
                this.suppliersGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSuppliersGroups() {
    this.selectedSuppliersGroupsPage = this.selectedSuppliersGroupsPage + 1;
    this.subscriptions.push(
      this.data
        .get(suppliersGroupsApi, this.selectedSuppliersGroupsPage)
        .subscribe((res: IDataRes) => {
          if (this.suppliersGroupsPagesNo > this.selectedSuppliersGroupsPage) {
            this.hasMoreSuppliersGroups = true;
          } else {
            this.hasMoreSuppliersGroups = false;
          }
          for (const item of res.results) {
            if (this.suppliersGroups.length) {
              const uniqueSuppliersGroups = this.suppliersGroups.filter(
                x => x._id !== item._id
              );
              this.suppliersGroups = uniqueSuppliersGroups;
            }
            this.suppliersGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              for (const item of res.results) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMorewarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
          }
          for (const item of res.results) {
            if (this.warehouses.length) {
              const uniqueWarehouses = this.warehouses.filter(
                x => x._id !== item._id
              );
              this.warehouses = uniqueWarehouses;
            }
            this.warehouses.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      itemsGroupNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsGroups = true;
            } else {
              this.noItemsGroups = false;
              for (const item of res.results) {
                if (this.itemsGroups.length) {
                  const uniqueItemsGroups = this.itemsGroups.filter(
                    x => x._id !== item._id
                  );
                  this.itemsGroups = uniqueItemsGroups;
                }
                this.itemsGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsGroups() {
    this.selectedItemsGroupsPage = this.selectedItemsGroupsPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsGroupsApi, this.selectedItemsGroupsPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
            this.hasMoreItemsGroups = true;
          } else {
            this.hasMoreItemsGroups = false;
          }
          for (const item of res.results) {
            if (this.itemsGroups.length) {
              const uniqueItemsGroups = this.itemsGroups.filter(
                x => x._id !== item._id
              );
              this.itemsGroups = uniqueItemsGroups;
            }
            this.itemsGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsVariables(event) {
    const searchValue = event;
    const searchQuery = {
      itemsVariableNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsVariablesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsVariables = true;
            } else {
              this.noItemsVariables = false;
              for (const item of res.results) {
                if (this.itemsVariables.length) {
                  const uniqueItemsVariables = this.itemsVariables.filter(
                    x => x._id !== item._id
                  );
                  this.itemsVariables = uniqueItemsVariables;
                }
                this.itemsVariables.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsVariables() {
    this.selectedItemsVariablesPage = this.selectedItemsVariablesPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, this.selectedItemsVariablesPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          } else {
            this.hasMoreItemsVariables = false;
          }
          for (const item of res.results) {
            if (this.itemsVariables.length) {
              const uniqueItemsVariables = this.itemsVariables.filter(
                x => x._id !== item._id
              );
              this.itemsVariables = uniqueItemsVariables;
            }
            this.itemsVariables.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsTypes(event) {
    const searchValue = event;
    const searchQuery = {
      itemsTypeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsTypesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsTypes = true;
            } else {
              this.noItemsTypes = false;
              for (const item of res.results) {
                if (this.itemsTypes.length) {
                  const uniqueItemsTypes = this.itemsTypes.filter(
                    x => x._id !== item._id
                  );
                  this.itemsTypes = uniqueItemsTypes;
                }
                this.itemsTypes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsTypes() {
    this.selectedItemsTypesPage = this.selectedItemsTypesPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsTypesApi, this.selectedItemsTypesPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsTypesPagesNo > this.selectedItemsTypesPage) {
            this.hasMoreItemsTypes = true;
          } else {
            this.hasMoreItemsTypes = false;
          }
          for (const item of res.results) {
            if (this.itemsTypes.length) {
              const uniqueItemsTypes = this.itemsTypes.filter(
                x => x._id !== item._id
              );
              this.itemsTypes = uniqueItemsTypes;
            }
            this.itemsTypes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsUnits(event) {
    const searchValue = event;
    const searchQuery = {
      itemsUnitNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsUnitsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsUnits = true;
            } else {
              this.noItemsUnits = false;
              for (const item of res.results) {
                if (this.itemsUnits.length) {
                  const uniqueItemsUnits = this.itemsUnits.filter(
                    x => x._id !== item._id
                  );
                  this.itemsUnits = uniqueItemsUnits;
                }
                this.itemsUnits.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsUnits() {
    this.selectedItemsUnitsPage = this.selectedItemsUnitsPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsUnitsApi, this.selectedItemsUnitsPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsUnitsPagesNo > this.selectedItemsUnitsPage) {
            this.hasMoreItemsUnits = true;
          } else {
            this.hasMoreItemsUnits = false;
          }
          for (const item of res.results) {
            if (this.itemsUnits.length) {
              const uniqueItemsUnits = this.itemsUnits.filter(
                x => x._id !== item._id
              );
              this.itemsUnits = uniqueItemsUnits;
            }
            this.itemsUnits.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsClassifications(event) {
    const searchValue = event;
    const searchQuery = {
      itemsClassificationNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsClassificationsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsClassifications = true;
            } else {
              this.noItemsClassifications = false;
              for (const item of res.results) {
                if (this.itemsClassifications.length) {
                  const uniqueItemsClassifications = this.itemsClassifications.filter(
                    x => x._id !== item._id
                  );
                  this.itemsClassifications = uniqueItemsClassifications;
                }
                this.itemsClassifications.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsClassifications() {
    this.selectedItemsClassificationsPage = this.selectedItemsClassificationsPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsClassificationsApi, this.selectedItemsClassificationsPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsClassificationsPagesNo > this.selectedItemsClassificationsPage) {
            this.hasMoreItemsClassifications = true;
          } else {
            this.hasMoreItemsClassifications = false;
          }
          for (const item of res.results) {
            if (this.itemsClassifications.length) {
              const uniqueItemsClassifications = this.itemsClassifications.filter(
                x => x._id !== item._id
              );
              this.itemsClassifications = uniqueItemsClassifications;
            }
            this.itemsClassifications.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsFamilies(event) {
    const searchValue = event;
    const searchQuery = {
      itemsFamilyNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsFamiliesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsFamilies = true;
            } else {
              this.noItemsFamilies = false;
              for (const item of res.results) {
                if (this.itemsFamilies.length) {
                  const uniqueItemsFamilies = this.itemsFamilies.filter(
                    x => x._id !== item._id
                  );
                  this.itemsFamilies = uniqueItemsFamilies;
                }
                this.itemsFamilies.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsFamilies() {
    this.selectedItemsFamiliesPage = this.selectedItemsFamiliesPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsFamiliesApi, this.selectedItemsFamiliesPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsFamiliesPagesNo > this.selectedItemsFamiliesPage) {
            this.hasMoreItemsFamilies = true;
          } else {
            this.hasMoreItemsFamilies = false;
          }
          for (const item of res.results) {
            if (this.itemsFamilies.length) {
              const uniqueItemsFamilies = this.itemsFamilies.filter(
                x => x._id !== item._id
              );
              this.itemsFamilies = uniqueItemsFamilies;
            }
            this.itemsFamilies.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchServicesItems(event) {
    const searchValue = event;
    const searchQuery = {
      servicesItemsGroupNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(servicesItemsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noServicesItems = true;
          } else {
            this.noServicesItems = false;
            for (const item of res.results) {
              if (this.servicesItems.length) {
                const uniqueServicesItems = this.servicesItems.filter(x => x._id !== item._id);
                this.servicesItems = uniqueServicesItems;
              }
              this.servicesItems.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreServicesItems() {
    this.selectedServicesItemsPage = this.selectedServicesItemsPage + 1;
    this.subscriptions.push(
      this.data.get(servicesItemsApi, this.selectedServicesItemsPage).subscribe((res: IDataRes) => {
        if (this.servicesItemsPagesNo > this.selectedServicesItemsPage) {
          this.hasMoreServicesItems = true;
        } else {
          this.hasMoreServicesItems = false;
        }
        for (const item of res.results) {
          if (this.servicesItems.length) {
            const uniqueServicesItems = this.servicesItems.filter(x => x._id !== item._id);
            this.servicesItems = uniqueServicesItems;
          }
          this.servicesItems.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchProjects(event) {
    const searchValue = event;
    const searchQuery = {
      projectNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noProjects = true;
            } else {
              this.noProjects = false;
              for (const item of res.results) {
                if (this.projects.length) {
                  const uniqueprojects = this.projects.filter(
                    x => x._id !== item._id
                  );
                  this.projects = uniqueprojects;
                }
                this.projects.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreProjects() {
    this.selectedProjectsPage = this.selectedProjectsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedProjectsPage)
        .subscribe((res: IDataRes) => {
          if (this.projectsPagesNo > this.selectedProjectsPage) {
            this.hasMoreProjects = true;
          } else {
            this.hasMoreProjects = false;
          }
          for (const item of res.results) {
            if (this.projects.length) {
              const uniqueProjects = this.projects.filter(
                x => x._id !== item._id
              );
              this.projects = uniqueProjects;
            }
            this.projects.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchRejectionReasons(event) {
    const searchValue = event;
    const searchQuery = {
      checkReasonsRejectionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(checkReasonsRejectionApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noRejectionReasons = true;
            } else {
              this.noRejectionReasons = false;
              for (const item of res.results) {
                if (this.rejectionReasons.length) {
                  const uniqueRejectionReasons = this.rejectionReasons.filter(
                    x => x._id !== item._id
                  );
                  this.rejectionReasons = uniqueRejectionReasons;
                }
                this.rejectionReasons.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreRejectionReasons() {
    this.selectedRejectionReasonsPage = this.selectedRejectionReasonsPage + 1;
    this.subscriptions.push(
      this.data
        .get(checkReasonsRejectionApi, this.selectedRejectionReasonsPage)
        .subscribe((res: IDataRes) => {
          if (this.rejectionReasonsPagesNo > this.selectedRejectionReasonsPage) {
            this.hasMoreRejectionReasons = true;
          } else {
            this.hasMoreRejectionReasons = false;
          }
          for (const item of res.results) {
            if (this.rejectionReasons.length) {
              const uniquerejectionReasons = this.rejectionReasons.filter(x => x._id !== item._id);
              this.rejectionReasons = uniquerejectionReasons;
            }
            this.rejectionReasons.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCountries(event) {
    const searchValue = event;
    const searchQuery = {
      countryNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(countriesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCountries = true;
          } else {
            this.noCountries = false;
            for (const item of res.results) {
              if (this.countries.length) {
                const uniqueCountries = this.countries.filter(x => x._id !== item._id);
                this.countries = uniqueCountries;
              }
              this.countries.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        },
          err => {
            this.uiService.isLoading.next(false);
            //this.uiService.showErrorMessage(err);
          })
      );
    }
    if (searchValue.length <= 0) {
      this.noCountries = false;
      this.countriesInputFocused = true;
    } else {
      this.countriesInputFocused = false;
    }
  }

  loadMoreCountries() {
    this.selectedCountriesPage = this.selectedCountriesPage + 1;
    this.subscriptions.push(
      this.data.get(countriesApi, this.selectedCountriesPage).subscribe((res: IDataRes) => {
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        } else {
          this.hasMoreCountries = false;
        }
        for (const item of res.results) {
          if (this.countries.length) {
            const uniqueCountries = this.countries.filter(x => x._id !== item._id);
            this.countries = uniqueCountries;
          }
          this.countries.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
