import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { DataService } from '../../services/shared/data.service';
import { itemsVariablesApi } from '../../constants/api.constants';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { UiService } from '../../services/ui/ui.service';
import { Subscription } from 'rxjs';
import { IDataRes } from '../../interfaces/IDataResponse.model';
import { searchLength } from '../../constants/general.constants';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-variables-modal',
  templateUrl: './variables-modal.component.html',
  styleUrls: ['./variables-modal.component.scss']
})
export class VariablesModalComponent implements OnInit, OnDestroy {
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;
  subscriptions: Subscription[] = [];
  variablesForm: FormGroup;
  values: any[] = [];
  formData: any;
  @Output() confirmed: EventEmitter<any> = new EventEmitter();

  constructor(
    public data: DataService,
    public uiService: UiService,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
    this.initForm();
  }

  private initForm() {
    let variablesArray = new FormArray([
      new FormGroup({
        variableId: new FormControl(''),
        itemVariableNameId: new FormControl('')
      })
    ]);

    if (this.formData) {
      variablesArray = new FormArray([]);
      for (const item of this.formData) {
        variablesArray.push(
          new FormGroup({
            variableId: new FormControl(item.variableId),
            itemVariableNameId: new FormControl(item.itemVariableNameId)
          })
        );
      }
    }

    this.variablesForm = new FormGroup({
      variables: variablesArray
    });
  }

  get getVariablesArray() {
    return this.variablesForm.get('variables') as FormArray;
  }

  addVariable() {
    const control = this.variablesForm.get('variables') as FormArray;
    control.push(
      new FormGroup({
        variableId: new FormControl(''),
        itemVariableNameId: new FormControl('')
      })
    );
  }

  deleteVariable(index) {
    const control = this.variablesForm.get('variables') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  fillValues(event) {
    const variable = this.itemsVariables.find(item => item._id === event);
    if (variable) {
      return variable.itemsVariables;
    }
  }

  searchItemsVariables(event) {
    const searchValue = event;
    const searchQuery = {
      itemsVariableNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsVariablesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsVariables = true;
            } else {
              this.noItemsVariables = false;
              for (const item of res.results) {
                if (this.itemsVariables.length) {
                  const uniqueItemsVariables = this.itemsVariables.filter(
                    x => x._id !== item._id
                  );
                  this.itemsVariables = uniqueItemsVariables;
                }
                this.itemsVariables.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsVariables() {
    this.selectedItemsVariablesPage = this.selectedItemsVariablesPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, this.selectedItemsVariablesPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          } else {
            this.hasMoreItemsVariables = false;
          }
          for (const item of res.results) {
            if (this.itemsVariables.length) {
              const uniqueItemsVariables = this.itemsVariables.filter(
                x => x._id !== item._id
              );
              this.itemsVariables = uniqueItemsVariables;
            }
            this.itemsVariables.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  confirm() {
    this.confirmed.emit(this.variablesForm.value);
  }

  decline() {
    this.confirmed.emit(false);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
