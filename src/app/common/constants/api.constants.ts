export const localBaseUrl = "http://localhost:4200/";
//export const baseUrl = "http://intmicrotec.neat-url.com:4000/";
//export const baseUrl = 'http://localhost:4000/';
export const baseUrl = "http://localhost:7040/";
// For Developers

export const apiUrl = baseUrl + "api/";

// ---- APIS ---- //
export const loginApi = apiUrl + "identity/authenticationLogin";
export const companyLoginApi = apiUrl + "identity/authorizationLogin";
export const changePasswordApi = apiUrl + "identity/changePassword";

export const countriesApi = apiUrl + "common/systemFiles/countries";
export const citiesApi = apiUrl + "common/systemFiles/cities";
export const regionsApi = apiUrl + "common/systemFiles/regions";
export const nationalitiesApi = apiUrl + "common/systemFiles/nationalities";
export const currenciesApi = apiUrl + "common/systemFiles/currencies";
export const attachmentTypesApi = apiUrl + "common/systemFiles/attachmentTypes";
export const employeesApi = apiUrl + "general/files/employees";
export const filesNumberingApi = apiUrl + "general/settings/filesNumbering";
export const companiesApi = apiUrl + "common/systemFiles/companies";
export const companyCurrenciesApi = companiesApi + "/companyCurrencies";
export const branchCurrenciesApi = companiesApi + "/branchCurrencies";
export const fiscalYearsApi = apiUrl + "general/files/fiscalYears";
export const nextFiscalYearDateApi =
  fiscalYearsApi + "/nextFiscalYearStartDate";
export const usersApi = apiUrl + "common/users";

export const returnReasonsApi = apiUrl + "sales/files/returnReasons";
export const taxesApi = apiUrl + "sales/files/taxes";
export const customersGroupsApi = apiUrl + "sales/files/customersGroups";
export const customersClassificationsApi =
  apiUrl + "sales/files/customersClassifications";
export const detailedCustomersClassificationsApi =
  customersClassificationsApi + "/getDetailedCustomersClassifications";
export const customersClassificationsTreeApi =
  customersClassificationsApi + "/tree";
export const pricingPoliciesApi = apiUrl + "sales/files/pricingPolicies";
export const customersApi = apiUrl + "sales/files/customers";
export const discountClassificationsApi =
  apiUrl + "sales/files/discountClassifications";
export const itemsPricesApi = apiUrl + "sales/files/itemPrices";
export const salespersonsCommissionsApi =
  apiUrl + "sales/files/salespersonsCommissions";
export const salesSettingsApi = apiUrl + "sales/settings";
export const getSalesSettingsApi = salesSettingsApi + "/getSettings";
export const salesQuotationApi = apiUrl + "sales/transactions/salesQuotation";
export const postSalesQuotationApi = salesQuotationApi + "/post";
export const unPostSalesQuotationApi = salesQuotationApi + "/unpost";
export const salesContractApi = apiUrl + "sales/transactions/salesContract";
export const postSalesContractApi = salesContractApi + "/post";
export const unPostSalesContractApi = salesContractApi + "/unpost";
export const salesOrderApi = apiUrl + "sales/transactions/salesOrder";
export const postSalesOrderApi = salesOrderApi + "/post";
export const unPostSalesOrderApi = salesOrderApi + "/unpost";
export const salesInvoiceApi = apiUrl + "sales/transactions/salesInvoice";
export const postSalesInvoiceApi = salesInvoiceApi + "/post";
export const unPostSalesInvoiceApi = salesInvoiceApi + "/unpost";
export const salesReturnApi = apiUrl + "sales/transactions/salesReturn";
export const postSalesReturnApi = salesReturnApi + "/post";
export const unPostSalesReturnApi = salesReturnApi + "/unpost";

export const inventoryControlRejectionReasonsApi =
  apiUrl + "purchases/files/inventoryControlRejectionReasons";
export const projectsApi = apiUrl + "purchases/files/projects";
export const suppliersGroupsApi = apiUrl + "purchases/files/suppliersGroups";
export const suppliersClassificationsApi =
  apiUrl + "purchases/files/suppliersClassifications";
export const detailedSuppliersClassificationsApi =
  suppliersClassificationsApi + "/getDetailedSuppliersClassifications";
export const suppliersClassificationsTreeApi =
  suppliersClassificationsApi + "/tree";
export const suppliersApi = apiUrl + "purchases/files/suppliers";
export const purchasingRequestApi =
  apiUrl + "purchases/transactions/purchasingRequest";
export const postPurchasingRequestApi = purchasingRequestApi + "/post";
export const unPostPurchasingRequestApi = purchasingRequestApi + "/unpost";
export const purchasingStockControlApi =
  apiUrl + "purchases/transactions/purchasingStockControl";
export const postPurchasingStockControlApi =
  purchasingStockControlApi + "/post";
export const unPostPurchasingStockControlApi =
  purchasingStockControlApi + "/unpost";
export const purchaseQuotationRequestApi =
  apiUrl + "purchases/transactions/purchaseQuotationRequest";
export const postPurchaseQuotationRequestApi =
  purchaseQuotationRequestApi + "/post";
export const unPostPurchaseQuotationRequestApi =
  purchaseQuotationRequestApi + "/unpost";
export const purchaseQuotationApi =
  apiUrl + "purchases/transactions/purchaseQuotation";
export const postPurchaseQuotationApi = purchaseQuotationApi + "/post";
export const unPostPurchaseQuotationApi = purchaseQuotationApi + "/unpost";
export const purchasingPriceComparisonApi =
  apiUrl + "purchases/transactions/purchasingPriceComparison";
export const postPurchasingPriceComparisonApi =
  purchasingPriceComparisonApi + "/post";
export const unPostPurchasingPriceComparisonApi =
  purchasingPriceComparisonApi + "/unpost";
export const purchaseOrderApi = apiUrl + "purchases/transactions/purchaseOrder";
export const postPurchaseOrderApi = purchaseOrderApi + "/post";
export const unPostPurchaseOrderApi = purchaseOrderApi + "/unpost";
export const purchasingInvoiceApi =
  apiUrl + "purchases/transactions/purchasingInvoice";
export const postPurchasingInvoiceApi = purchasingInvoiceApi + "/post";
export const unPostPurchasingInvoiceApi = purchasingInvoiceApi + "/unpost";
export const purchasingReturnsApi =
  apiUrl + "purchases/transactions/purchasingReturns";
export const postPurchasingReturnsApi = purchasingReturnsApi + "/post";
export const unPostPurchasingReturnsApi = purchasingReturnsApi + "/unpost";
export const purchasesSettingsApi = apiUrl + "purchases/settings";
export const getPurchasesSettingsApi = purchasesSettingsApi + "/getSettings";

export const itemsClassificationsApi =
  apiUrl + "inventory/files/itemsClassifications";
export const itemsFamiliesApi = apiUrl + "inventory/files/itemsFamilies";
export const itemsGroupsApi = apiUrl + "inventory/files/itemsGroups";
export const itemsSectionsApi = apiUrl + "inventory/files/itemsSections";
export const itemsTypesApi = apiUrl + "inventory/files/itemsTypes";
export const itemsUnitsApi = apiUrl + "inventory/files/itemsUnits";
export const itemsVariablesApi = apiUrl + "inventory/files/itemsVariables";
export const servicesItemsGroupsApi =
  apiUrl + "inventory/files/servicesItemsGroups";
export const servicesItemsApi = apiUrl + "inventory/files/servicesItems";
export const warehousesApi = apiUrl + "inventory/files/warehouses";
export const warehousesTreeApi = warehousesApi + "/tree";
export const storesItemsApi = apiUrl + "inventory/files/storesItems";
export const getStoreItemViaBarcodeApi = storesItemsApi + "/itemSelect";
export const storeitemsWarehouseBalanceAndCodeApi =
  storesItemsApi + "/getItemWarehouseAverageCostAndBalance";
export const damageStoreItemsApi =
  apiUrl + "inventory/transactions/damageStoreItems";
export const postDamageStoreItemsApi = damageStoreItemsApi + "/post";
export const unPostDamageStoreItemsApi = damageStoreItemsApi + "/unpost";
export const deliveringInventoryRequestApi =
  apiUrl + "inventory/transactions/deliveringInventoryRequest";
export const postDeliveringInventoryRequestApi =
  deliveringInventoryRequestApi + "/post";
export const unPostDeliveringInventoryRequestApi =
  deliveringInventoryRequestApi + "/unpost";
export const deliveringInventoryApi =
  apiUrl + "inventory/transactions/deliveringInventory";
export const postDeliveringInventoryApi = deliveringInventoryApi + "/post";
export const unPostDeliveringInventoryApi = deliveringInventoryApi + "/unpost";
export const holdingQuantitiesRequestApi =
  apiUrl + "inventory/transactions/holdingQuantitiesRequest";
export const postHoldingQuantitiesRequestApi =
  holdingQuantitiesRequestApi + "/post";
export const unPostHoldingQuantitiesRequestApi =
  holdingQuantitiesRequestApi + "/unpost";
export const holdingQuantitiesApi =
  apiUrl + "inventory/transactions/holdingQuantities";
export const postHoldingQuantitiesApi = holdingQuantitiesApi + "/post";
export const unPostHoldingQuantitiesApi = holdingQuantitiesApi + "/unpost";
export const unHoldingQuantitiesApi =
  apiUrl + "inventory/transactions/unHoldingQuantities";
export const postUnHoldingQuantitiesApi = unHoldingQuantitiesApi + "/post";
export const unPostUnHoldingQuantitiesApi = unHoldingQuantitiesApi + "/unpost";
export const inventoryReEvaluationApi =
  apiUrl + "inventory/transactions/reEvaluation";
export const postInventoryReEvaluationApi = inventoryReEvaluationApi + "/post";
export const unPostInventoryReEvaluationApi =
  inventoryReEvaluationApi + "/unpost";
export const inventoryOpeningBalanceApi =
  apiUrl + "inventory/transactions/openingBalance";
export const postInventoryOpeningBalanceApi =
  inventoryOpeningBalanceApi + "inventory/transactions/openingBalance";
export const unPostInventoryOpeningBalanceApi =
  inventoryOpeningBalanceApi + "inventory/transactions/openingBalance";
export const receivingInventoryApi =
  apiUrl + "inventory/transactions/receivingInventory";
export const postReceivingInventoryApi = receivingInventoryApi + "/post";
export const unPostReceivingInventoryApi = receivingInventoryApi + "/unpost";
export const inventoryAdjustmentApi =
  apiUrl + "inventory/transactions/adjustment";
export const postInventoryAdjustmentApi = inventoryAdjustmentApi + "/post";
export const unPostInventoryAdjustmentApi = inventoryAdjustmentApi + "/unpost";
export const mergingCountingReportApi =
  apiUrl + "inventory/transactions/mergingCountingReport";
export const postMergingCountingReportApi = mergingCountingReportApi + "/post";
export const unPostMergingCountingReportApi =
  mergingCountingReportApi + "/unpost";
export const countingReportApi =
  apiUrl + "inventory/transactions/countingReport";
export const postCountingReportApi = countingReportApi + "/post";
export const unPostCountingReportApi = countingReportApi + "/unpost";
export const storeCountingApi = apiUrl + "inventory/transactions/storeCounting";
export const postStoreCountingApi = storeCountingApi + "/post";
export const unPostStoreCountingApi = storeCountingApi + "/unpost";
export const transferRequestApi =
  apiUrl + "inventory/transactions/transferRequest";
export const postTransferRequestApi = transferRequestApi + "/post";
export const unPostTransferRequestApi = transferRequestApi + "/unpost";
export const importTransferApi =
  apiUrl + "inventory/transactions/importTransfer";
export const postImportTransferApi = importTransferApi + "/post";
export const unPostImportTransferApi = importTransferApi + "/unpost";
export const exportTransferApi =
  apiUrl + "inventory/transactions/exportTransfer";
export const postExportTransferApi = exportTransferApi + "/post";
export const unPostExportTransferApi = exportTransferApi + "/unpost";
export const directTransferApi =
  apiUrl + "inventory/transactions/directTransfer";
export const postDirectTransferApi = directTransferApi + "/post";
export const unPostDirectTransferApi = directTransferApi + "/unpost";
export const internalTransferApi =
  apiUrl + "inventory/transactions/internalTransfer";
export const postInternalTransferApi = internalTransferApi + "/post";
export const unPostInternalTransferApi = internalTransferApi + "/unpost";
export const assemblingItemsApi =
  apiUrl + "inventory/transactions/assemblingItems";
export const postAssemblingItemsApi = assemblingItemsApi + "/post";
export const unPostAssemblingItemsApi = assemblingItemsApi + "/unpost";
export const disassemblingItemsApi =
  apiUrl + "inventory/transactions/disassemblingItems";
export const postDisassemblingItemsApi = disassemblingItemsApi + "/post";
export const unPostDisassemblingItemsApi = disassemblingItemsApi + "/unpost";
export const inventorySettingsApi = apiUrl + "inventory/settings";
export const getInventorySettingsApi = inventorySettingsApi + "/getSettings";

export const incomeStatementClassificationsApi =
  apiUrl + "accounting/gl/files/incomeStatementClassifications";
export const balanceSheetClassificationsApi =
  apiUrl + "accounting/gl/files/balanceSheetClassifications";
export const costCentersApi = apiUrl + "accounting/gl/files/costCenters";
export const costCentersTreeApi = costCentersApi + "/tree";
export const detailedCostCentersApi = costCentersApi + "/getDetailedCostCenter";

export const detailedCostCenter =
  apiUrl + "accounting/gl/reports/costCenter/detailed";
export const groupedCostCenter =
  apiUrl + "accounting/gl/reports/costCenter/grouped";

export const chartOfAccountsApi =
  apiUrl + "accounting/gl/files/chartOfAccounts";
export const chartOfAccountsTreeApi = chartOfAccountsApi + "/tree";
export const detailedChartOfAccountsApi =
  chartOfAccountsApi + "/getDetailedChartOfAccount";
export const accountsOpeningBalancesApi =
  apiUrl + "accounting/gl/transactions/accountsOpeningBalances";
export const postAccountsOpeningBalancesApi =
  accountsOpeningBalancesApi + "/post";
export const unPostAccountsOpeningBalancesApi =
  accountsOpeningBalancesApi + "/unpost";
export const journalEntriesApi =
  apiUrl + "accounting/gl/transactions/journalEntries";
export const postJournalEntriesApi = journalEntriesApi + "/post";
export const unPostjournalEntriesApi = journalEntriesApi + "/unpost";
export const glSettingsApi = apiUrl + "accounting/settings";
export const getGlSettingsApi = glSettingsApi + "/getSettings";

export const banksApi = apiUrl + "accounting/financialTransactions/files/banks";
export const safeBoxesApi =
  apiUrl + "accounting/financialTransactions/files/safeBoxes";
export const moneyTransfersApi =
  apiUrl + "accounting/financialTransactions/transactions/moneyTransfers";
export const postMoneyTransfersApi = moneyTransfersApi + "/post";
export const unPostMoneyTransfersApi = moneyTransfersApi + "/unpost";
export const debitTransfersApi =
  apiUrl + "accounting/financialTransactions/transactions/debitTransfer";
export const postDebitTransfersApi = debitTransfersApi + "/post";
export const unPostDebitTransfersApi = debitTransfersApi + "/unpost";
export const cashReceivedReceiptsApi =
  apiUrl + "accounting/financialTransactions/transactions/cashReceivedReceipt";
export const postCashReceivedReceiptsApi = cashReceivedReceiptsApi + "/post";
export const unPostCashReceivedReceiptsApi =
  cashReceivedReceiptsApi + "/unpost";
export const cashPayableReceiptsApi =
  apiUrl + "accounting/financialTransactions/transactions/cashPayableReceipt";
export const postCashPayableReceiptsApi = cashPayableReceiptsApi + "/post";
export const unPostCashPayableReceiptsApi = cashPayableReceiptsApi + "/unpost";
export const creditNotesApi =
  apiUrl + "accounting/financialTransactions/transactions/creditNotes";
export const postCreditNotesApi = creditNotesApi + "/post";
export const unPostCreditNotesApi = creditNotesApi + "/unpost";
export const debitNotesApi =
  apiUrl + "accounting/financialTransactions/transactions/debitNotes";
export const postDebitNotesApi = debitNotesApi + "/post";
export const unPostDebitNotesApi = debitNotesApi + "/unpost";
export const adjustmentApi =
  apiUrl + "accounting/financialTransactions/transactions/adjustment";
export const postAdjustmentApi = adjustmentApi + "/post";
export const unPostAdjustmentApi = adjustmentApi + "/unpost";

export const identifiedChecksApi =
  apiUrl + "accounting/checks/files/identifiedChecks/search";
export const beneficiariesApi = apiUrl + "accounting/checks/files/beneficiary";
export const checkReasonsRejectionApi =
  apiUrl + "accounting/checks/files/checkReasonsRejection";
export const returnCheckUnderPaidApi =
  apiUrl + "accounting/checks/transactions/payable/returnCheckUnderPaid";
export const postReturnCheckUnderPaidApi = returnCheckUnderPaidApi + "/post";
export const unPostReturnCheckUnderPaidApi =
  returnCheckUnderPaidApi + "/unpost";
export const cashCheckApi =
  apiUrl + "accounting/checks/transactions/payable/cashCheck";
export const postCashCheckApi = cashCheckApi + "/post";
export const unPostCashCheckApi = cashCheckApi + "/unpost";
export const cashPaidApi =
  apiUrl + "accounting/checks/transactions/payable/checkPaid";
export const postCashPaidApi = cashPaidApi + "/post";
export const unPostCashPaidApi = cashPaidApi + "/unpost";
export const payableChecksOpeningBalanceApi =
  apiUrl + "accounting/checks/transactions/payable/payableChecksOpeningBalance";
export const postPayableChecksOpeningBalanceApi =
  payableChecksOpeningBalanceApi + "/post";
export const unPostPayableChecksOpeningBalanceApi =
  payableChecksOpeningBalanceApi + "/unpost";
export const receivedChecksOpeningBalanceApi =
  apiUrl +
  "accounting/checks/transactions/received/receivedChecksOpeningBalance";
export const postReceivedChecksOpeningBalanceApi =
  receivedChecksOpeningBalanceApi + "/post";
export const unPostReceivedChecksOpeningBalanceApi =
  receivedChecksOpeningBalanceApi + "/unpost";
export const receivingChecksApi =
  apiUrl + "accounting/checks/transactions/received/receivingChecks";
export const postReceivingChecksApi = receivingChecksApi + "/post";
export const unPostReceivingChecksApi = receivingChecksApi + "/unpost";
export const deliveringChecksApi =
  apiUrl + "accounting/checks/transactions/received/deliveredChecks";
export const postDeliveringChecksApi = deliveringChecksApi + "/post";
export const unPostDeliveringChecksApi = deliveringChecksApi + "/unpost";
export const depositChecksApi =
  apiUrl + "accounting/checks/transactions/received/depositChecks";
export const postDepositChecksApi = depositChecksApi + "/post";
export const unPostDepositChecksApi = depositChecksApi + "/unpost";
export const checkCollectionApi =
  apiUrl + "accounting/checks/transactions/received/checkCollection";
export const postCheckCollectionApi = checkCollectionApi + "/post";
export const unPostCheckCollectionApi = checkCollectionApi + "/unpost";
export const returnedChecksApi =
  apiUrl + "accounting/checks/transactions/received/returnedChecks";
export const postReturnedChecksApi = returnedChecksApi + "/post";
export const unPostReturnedChecksApi = returnedChecksApi + "/unpost";
export const returnChecksToCustomersApi =
  apiUrl + "accounting/checks/transactions/received/returnChecksToCustomers";
export const postReturnChecksToCustomersApi =
  returnChecksToCustomersApi + "/post";
export const unPostReturnChecksToCustomersApi =
  returnChecksToCustomersApi + "/unpost";

export const estimatedAccountsBudgetApi =
  apiUrl + "accounting/financialAnalysis/transactions/estimatedAccountsBudget";
export const estimatedAccountsBudgetByLevelApi =
  estimatedAccountsBudgetApi + "/getBudgetByLevel";
export const estimatedCostCenterBudgetApi =
  apiUrl +
  "accounting/financialAnalysis/transactions/estimatedCostCenterBudget";
export const estimatedCostCenterBudgetByLevelApi =
  estimatedCostCenterBudgetApi + "/getBudgetByLevel";

// POS
export const DriversApi = apiUrl + "pos/files/drivers";
export const OperatorsApi = apiUrl + "pos/files/operators";
export const MediaMethodApi = apiUrl + "pos/files/mediaMethods";
export const DeliveryRegionApi = apiUrl + "pos/files/deliveryRegions";
export const RewardsProgramCustomerApi =
  apiUrl + "pos/files/rewardsProgramCustomers";
export const RewardsProgramApi = apiUrl + "pos/files/rewardsPrograms";
export const PosSettingApi = apiUrl + "pos/settings";
export const PosDefinitionApi = apiUrl + "pos/files/PointOfSale";
export const SectionsApi = apiUrl + "pos/files/sections";
export const PromotionsApi = apiUrl + "pos/files/promotions";

// Reports

export const ReportsAccountStatementApi =
  apiUrl + "accounting/gl/reports/accountStatement/detailed";
export const GroupedAccountReportsApi =
  apiUrl + "accounting/gl/reports/accountStatement/grouped";
