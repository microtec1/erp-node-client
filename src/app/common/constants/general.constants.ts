export const searchLength = 2;
export const pagingStart = 0;
export const pagingEnd = 3;
export const companyId = JSON.parse(localStorage.getItem('company')) && JSON.parse(localStorage.getItem('company')).companyId;
export const branchId = JSON.parse(localStorage.getItem('branch')) && JSON.parse(localStorage.getItem('branch')).branchId;
