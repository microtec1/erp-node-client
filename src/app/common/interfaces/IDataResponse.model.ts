import { ICurrency } from 'src/app/modules/general/interfaces/ICurrency';

export interface IDataRes {
  count: number;
  currentPage: number;
  limit: number;
  pages: number;
  query: {};
  sort: {};
  companyCurrency: any[];
  branches: [
    {
      branchCurrency: ICurrency[]
    }
  ];
  results: any[];
}
