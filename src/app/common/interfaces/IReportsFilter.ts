
export interface IReportsFilter {
  lockups: [
    {
      name: string;
      addMoreURL: string;
      loadMoreURL?: string;
      label: string;
      lockupName: string;
      arName: string;
      idName: string;
    }
  ];
}
