import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, } from '@angular/common/http';
import { loginApi, changePasswordApi, companyLoginApi } from '../../constants/api.constants';
import { ILoginRes } from '../../interfaces/ILoginResponse.model';
import { tap } from 'rxjs/operators';
import { UiService } from '../ui/ui.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private router: Router,
    private http: HttpClient,
    private uiService: UiService
  ) { }

  login(user) {
    return this.http.post(`${loginApi}`, user);
  }

  companyLogin(body) {
    return this.http.post(`${companyLoginApi}`, body);
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('company');
    localStorage.removeItem('branch');
    this.router.navigate(['/account']);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getUserCompanies() {
    return localStorage.getItem('userCompanies') !== null;
  }

  isAuthenticated() {
    return localStorage.getItem('token') !== null;
 }

  changePassword(passwords, userId) {
    this.uiService.isLoading.next(true);
    return this.http.put(`${changePasswordApi}/${userId}`, passwords);
  }
}
