import { companyId } from 'src/app/common/constants/general.constants';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UiService } from '../ui/ui.service';
import { catchError, retry } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(
    public http: HttpClient,
    private uiService: UiService,
    private translate: TranslateService
  ) { }

  post(route, body) {
    this.uiService.isLoading.next(true);
    return this.http.post(`${route}`, body).pipe(
      catchError(err => {
        this.uiService.showErrorMessage(err);
        this.uiService.isLoading.next(false);
        return throwError(err);
      })
    );
  }

  postWithoutSpinner(route, body){
    //this.uiService.isLoading.next(true);
    return this.http.post(`${route}`, body).pipe(
      retry(2),
      catchError(err => {
        this.uiService.showErrorMessage(err);
        this.uiService.isLoading.next(false);
        return throwError(err);
      })
    );
  }

  get(route, pageNumber?, searchValues?, id?) {
    this.uiService.isLoading.next(true);
    if (id) {
      return this.http.get(`${route}/${id}`).pipe(
        catchError(err => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
          return throwError(err);
        })
      );
    } else {
      return this.post(`${route}/search?page=${pageNumber}`, searchValues ? searchValues : {}).pipe(
        catchError(err => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
          return throwError(err);
        })
      );
    }
  }

  getByID(route, id) {
    this.uiService.isLoading.next(true);
    return this.http.get(`${route}/${id}`).pipe(
      catchError(err => {
        this.uiService.showErrorMessage(err);
        this.uiService.isLoading.next(false);
        return throwError(err);
      })
    );
  }
  put(route, body) {
    this.uiService.isLoading.next(true);
    return this.http.put(`${route}/${body._id}`, body).pipe(
      catchError(err => {
        this.uiService.showErrorMessage(err);
        this.uiService.isLoading.next(false);
        return throwError(err);
      })
    );
  }

  delete(route, id) {
    this.uiService.isLoading.next(true);
    return this.http.delete(`${route}/${id}`).pipe(
      catchError(err => {
        this.uiService.showErrorMessage(err);
        this.uiService.isLoading.next(false);
        return throwError(err);
      })
    );
  }

  getBasicData(route, pageNumber?, searchValues?) {
    this.uiService.isLoading.next(true);
    return this.post(`${route}/searchBasic?page=${pageNumber}`, searchValues ? searchValues : {}).pipe(
      catchError(err => {
        this.uiService.showErrorMessage(err);
        this.uiService.isLoading.next(false);
        return throwError(err);
      })
    );
  }

  getByRoute(route) {
    this.uiService.isLoading.next(true);
    return this.http.get(`${route}`).pipe(
      catchError(err => {
        this.uiService.showErrorMessage(err);
        this.uiService.isLoading.next(false);
        return throwError(err);
      })
    );
  }

  dataSort(route, pageNumber, sortValue, sortType) {
    this.uiService.isLoading.next(true);
    const sort = sortValue + '-' + sortType;
    return this.http.post(`${route}/search?page=${pageNumber}`, {}, {
      params: {
        sort
      }
    }).pipe(
      catchError(err => {
        this.uiService.showErrorMessage(err);
        this.uiService.isLoading.next(false);
        return throwError(err);
      })
    );
  }

  filterData:{} = {};

  setfilterData(filterData){
    this.filterData = filterData
  }
  getfilterData(){
    return this.filterData
  }

}
