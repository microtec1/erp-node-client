import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  otherLang: string;
  isLoading = new Subject<boolean>();
  isCollapsed = new Subject<boolean>();
  constructor(private translate: TranslateService, private toastr: ToastrService) { }

  get getOtherLang() {
    return this.translate
      .getLangs()
      .find(lang => lang !== this.translate.currentLang);
  }

  changeLang() {
    this.otherLang = this.getOtherLang;
    this.translate.use(this.otherLang);
  }

  toggleSidebar(collapse) {
    this.isCollapsed.next(collapse);
  }



  // Toaster

  showSuccess(title: string, message: string) {
    this.translate.get([title, message]).subscribe(res => {
      const text: string[] = Object.values(res);
      const toastrMsg: string = text[1];
      const toastrTitle: string = text[0];
      this.toastr.success(toastrMsg, toastrTitle, {
        timeOut: 3000
      });
    });
  }

  showError(title: string, message: string) {
    this.translate.get([title, message]).subscribe(res => {
      const text: string[] = Object.values(res);
      const toastrMsg: string = text[1];
      const toastrTitle: string = text[0];
      this.toastr.error(toastrMsg, toastrTitle, {
        timeOut: 5000
      });
    });
  }

  // Show Error
  showErrorMessage(err) {
    if (this.translate.currentLang === 'en') {
      this.showError(err.error.error.en, '');
    } else {
      this.showError(err.error.error.ar, '');
    }
  }

}
