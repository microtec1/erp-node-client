import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { CompanyLoginComponent } from './components/company-login/company-login.component';
import { HasCompanyGuard } from 'src/app/common/services/auth/guards/hasCompany.guard';
import { LoggedGuard } from 'src/app/common/services/auth/guards/logged.guard';
import { PrintLayoutComponent } from './components/print-layout/print-layout.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [LoggedGuard] },
  {
    path: 'company',
    component: CompanyLoginComponent,
    canActivate: [HasCompanyGuard]
  },
  { path: 'print/:data', component: PrintLayoutComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AccountRoutingModule { }
