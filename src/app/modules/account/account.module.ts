import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AccountRoutingModule } from './account-routing.module';
import { LoginComponent } from './components/login/login.component';
import { CompanyLoginComponent } from './components/company-login/company-login.component';
import { PrintLayoutComponent } from './components/print-layout/print-layout.component';

@NgModule({
  imports: [AccountRoutingModule, SharedModule],
  declarations: [LoginComponent, CompanyLoginComponent, PrintLayoutComponent]
})
export class AccountModule {}
