import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { IUser } from 'src/app/modules/general/interfaces/IUser.model';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/common/services/auth/auth.service';

@Component({
  selector: 'app-company-login',
  templateUrl: './company-login.component.html',
  styleUrls: ['./company-login.component.scss']
})
export class CompanyLoginComponent implements OnInit, OnDestroy {
  userCompanies: any[];
  branches: any[] = [];
  CompanyLoginForm: FormGroup;
  loadingButton: boolean;
  submitted: boolean;
  subscriptions: Subscription[] = [];
  constructor(
    private router: Router,
    public uiService: UiService,
    public auth: AuthService,
  ) { }

  ngOnInit() {
    this.userCompanies = JSON.parse(localStorage.getItem('userCompanies'));
    this.initCompanyLoginForm();
  }

  get form() {
    return this.CompanyLoginForm.controls;
  }

  isValid(fieldName, validationName: string) {
    if (fieldName.errors[validationName]) {
      return true;
    }
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  fillBranches(value) {
    const company = this.userCompanies.find(item => item.companyId === value);
    this.branches = company.branches;
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.CompanyLoginForm.valid) {
      const userCredentials = JSON.parse(localStorage.getItem('userCredentials'));
      const body = {
        ...userCredentials,
        companyId: this.CompanyLoginForm.value.company,
        branchId: this.CompanyLoginForm.value.branch
      };
      this.auth.companyLogin(body).subscribe((res: any) => {
        const company = this.userCompanies.find(item => item.companyId === this.CompanyLoginForm.value.company);
        const branch = this.branches.find(item => item.branchId === this.CompanyLoginForm.value.branch);
        localStorage.setItem('company', JSON.stringify(company));
        localStorage.setItem('branch', JSON.stringify(branch));
        localStorage.setItem('token', res.token);
        localStorage.removeItem('userCredentials');
        this.router.navigate(['/home']);
      });
    } else {
      this.loadingButton = false;
    }
  }

  initCompanyLoginForm() {
    this.CompanyLoginForm = new FormGroup({
      company: new FormControl('', Validators.required),
      branch: new FormControl('', Validators.required)
    });
  }

  changeLang() {
    this.uiService.changeLang();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
