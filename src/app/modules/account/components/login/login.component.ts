import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ILoginRes } from 'src/app/common/interfaces/ILoginResponse.model';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/common/services/auth/auth.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  passwordType: boolean;
  loginForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private authService: AuthService,
    public uiService: UiService
  ) { }

  ngOnInit() {
    this.initLoginForm();
  }

  get form() {
    return this.loginForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.loginForm.valid) {
      this.subscriptions.push(
        this.authService
          .login(this.loginForm.value)
          .subscribe((res: any) => {
            this.loadingButton = false;
            const userCompanies = JSON.stringify(res.userCompanies);
            const userCredentials = JSON.stringify(this.loginForm.value);
            localStorage.setItem('userCompanies', userCompanies);
            localStorage.setItem('userName', res.userName);
            localStorage.setItem('userId', res.userId);
            localStorage.setItem('userCredentials', userCredentials);
            this.router.navigate(['/account/company']);
          }, err => {
            this.loadingButton = false;
            this.uiService.showErrorMessage(err);
          }
          )
      );
    } else {
      this.loadingButton = false;
    }
  }

  initLoginForm() {
    this.loginForm = new FormGroup({
      userName: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }

  changeLang() {
    this.uiService.changeLang();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
