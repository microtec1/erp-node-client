import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccountReportsService } from 'src/app/modules/accounting/modules/gl/services/account-reports.service';

@Component({
  selector: 'app-print-layout',
  templateUrl: './print-layout.component.html',
  styleUrls: ['./print-layout.component.scss']
})
export class PrintLayoutComponent implements OnInit {

  tablesData = [];
  defaultColDef = {
    sortable: true,
    resizable: true,
    domLayout: 'autoHeight'
  };
  constructor(private router: ActivatedRoute, private accountReport: AccountReportsService) { }

  ngOnInit() {
    this.accountReport.currentTableLayout.subscribe(data => {
      this.tablesData = [data];
      this.tablesData[0].pagination = false;
      console.log("data", data);
    })

  }

}
