import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralLedgerComponent } from './components/general-ledger/general-ledger.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { FinancialAnalysisComponent } from './components/finanical-analysis/finanical-analysis.component';
import { ChequesComponent } from './components/cheques/cheques.component';


const routes: Routes = [
  { path: '', redirectTo: 'general-ledger', pathMatch: 'full' },
  { path: 'general-ledger', component: GeneralLedgerComponent },
  { path: 'transactions', component: TransactionsComponent },
  { path: 'financial-analysis', component: FinancialAnalysisComponent },
  { path: 'cheques', component: ChequesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule {}
