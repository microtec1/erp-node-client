import { NgModule } from '@angular/core';
import { GeneralLedgerComponent } from './components/general-ledger/general-ledger.component';
import { TransactionsComponent } from './components/transactions/transactions.component';
import { FinancialAnalysisComponent } from './components/finanical-analysis/finanical-analysis.component';
import { ChequesComponent } from './components/cheques/cheques.component';
import { AccountingRoutingModule } from './accounting-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [GeneralLedgerComponent, TransactionsComponent, FinancialAnalysisComponent, ChequesComponent],
  imports: [
    SharedModule,
    AccountingRoutingModule
  ]
})
export class AccountingModule { }
