import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BeneficiaryComponent } from './components/files/beneficiary/beneficiary.component';
import { CheckReasonsRejectionComponent } from './components/files/check-reasons-rejection/check-reasons-rejection.component';
import { AddBeneficiaryComponent } from './components/files/beneficiary/add-beneficiary/add-beneficiary.component';
import { AddReasonComponent } from './components/files/check-reasons-rejection/add-reason/add-reason.component';
import { IdentifiedChecksComponent } from './components/files/identified-checks/identified-checks.component';
import { ReceivedChecksOpeningBalanceComponent } from './components/transactions/received-checks/received-checks-opening-balance/received-checks-opening-balance.component';
import { AddReceivedChecksOpeningBalanceComponent } from './components/transactions/received-checks/received-checks-opening-balance/add-received-checks-opening-balance/add-received-checks-opening-balance.component';
import { AddReceivingChecksComponent } from './components/transactions/received-checks/receiving-checks/add-receiving-checks/add-receiving-checks.component';
import { ReceivingChecksComponent } from './components/transactions/received-checks/receiving-checks/receiving-checks.component';
import { AddDeliverChecksComponent } from './components/transactions/received-checks/deliver-checks/add-deliver-checks/add-deliver-checks.component';
import { DeliverChecksComponent } from './components/transactions/received-checks/deliver-checks/deliver-checks.component';
import { ChecksDepositComponent } from './components/transactions/received-checks/checks-deposit/checks-deposit.component';
import { AddChecksDepositComponent } from './components/transactions/received-checks/checks-deposit/add-checks-deposit/add-checks-deposit.component';
import { AddChecksReturnComponent } from './components/transactions/received-checks/checks-return/add-checks-return/add-checks-return.component';
import { ChecksReturnComponent } from './components/transactions/received-checks/checks-return/checks-return.component';
import { ChecksCollectionComponent } from './components/transactions/received-checks/checks-collection/checks-collection.component';
import { AddChecksCollectionComponent } from './components/transactions/received-checks/checks-collection/add-checks-collection/add-checks-collection.component';
import { ChecksReturnToCustomerComponent } from './components/transactions/received-checks/checks-return-to-customer/checks-return-to-customer.component';
import { AddChecksReturnToCustomerComponent } from './components/transactions/received-checks/checks-return-to-customer/add-checks-return-to-customer/add-checks-return-to-customer.component';
import { PaidChecksOpeningBalanceComponent } from './components/transactions/payable-checks/paid-checks-opening-balance/paid-checks-opening-balance.component';
import { AddPaidChecksOpeningBalanceComponent } from './components/transactions/payable-checks/paid-checks-opening-balance/add-paid-checks-opening-balance/add-paid-checks-opening-balance.component';
import { WritingPayCheckComponent } from './components/transactions/payable-checks/writing-pay-check/writing-pay-check.component';
import { AddWritingPayCheckComponent } from './components/transactions/payable-checks/writing-pay-check/add-writing-pay-check/add-writing-pay-check.component';
import { PaidChecksReturnComponent } from './components/transactions/payable-checks/paid-checks-return/paid-checks-return.component';
import { AddPaidChecksReturnComponent } from './components/transactions/payable-checks/paid-checks-return/add-paid-checks-return/add-paid-checks-return.component';
import { AddPayCheckComponent } from './components/transactions/payable-checks/pay-check/add-pay-check/add-pay-check.component';
import { PayCheckComponent } from './components/transactions/payable-checks/pay-check/pay-check.component';


const routes: Routes = [
  { path: '', redirectTo: 'beneficiary', pathMatch: 'full' },
  { path: 'beneficiary', component: BeneficiaryComponent },
  { path: 'beneficiary/add', component: AddBeneficiaryComponent },
  { path: 'beneficiary/edit/:id', component: AddBeneficiaryComponent },
  { path: 'beneficiary/details/:id', component: AddBeneficiaryComponent },
  { path: 'checkReasonsRejection', component: CheckReasonsRejectionComponent },
  { path: 'checkReasonsRejection/add', component: AddReasonComponent },
  { path: 'checkReasonsRejection/edit/:id', component: AddReasonComponent },
  { path: 'checkReasonsRejection/details/:id', component: AddReasonComponent },
  { path: 'identifiedChecks', component: IdentifiedChecksComponent },
  { path: 'receivedChecksOpeningBalance', component: ReceivedChecksOpeningBalanceComponent },
  { path: 'receivedChecksOpeningBalance/add', component: AddReceivedChecksOpeningBalanceComponent },
  { path: 'receivedChecksOpeningBalance/edit/:id', component: AddReceivedChecksOpeningBalanceComponent },
  { path: 'receivedChecksOpeningBalance/details/:id', component: AddReceivedChecksOpeningBalanceComponent },


  { path: 'receivingChecks', component: ReceivingChecksComponent },
  { path: 'receivingChecks/add', component: AddReceivingChecksComponent },
  { path: 'receivingChecks/edit/:id', component: AddReceivingChecksComponent },
  { path: 'receivingChecks/details/:id', component: AddReceivingChecksComponent },


  { path: 'deliverChecks', component: DeliverChecksComponent },
  { path: 'deliverChecks/add', component: AddDeliverChecksComponent },
  { path: 'deliverChecks/edit/:id', component: AddDeliverChecksComponent },
  { path: 'deliverChecks/details/:id', component: AddDeliverChecksComponent },


  { path: 'depositChecks', component: ChecksDepositComponent },
  { path: 'depositChecks/add', component: AddChecksDepositComponent },
  { path: 'depositChecks/edit/:id', component: AddChecksDepositComponent },
  { path: 'depositChecks/details/:id', component: AddChecksDepositComponent },

  { path: 'checksReturn', component: ChecksReturnComponent },
  { path: 'checksReturn/add', component: AddChecksReturnComponent },
  { path: 'checksReturn/edit/:id', component: AddChecksReturnComponent },
  { path: 'checksReturn/details/:id', component: AddChecksReturnComponent },

  { path: 'checksCollection', component: ChecksCollectionComponent },
  { path: 'checksCollection/add', component: AddChecksCollectionComponent },
  { path: 'checksCollection/edit/:id', component: AddChecksCollectionComponent },
  { path: 'checksCollection/details/:id', component: AddChecksCollectionComponent },

  { path: 'checksReturnToCustomer', component: ChecksReturnToCustomerComponent },
  { path: 'checksReturnToCustomer/add', component: AddChecksReturnToCustomerComponent },
  { path: 'checksReturnToCustomer/edit/:id', component: AddChecksReturnToCustomerComponent },
  { path: 'checksReturnToCustomer/details/:id', component: AddChecksReturnToCustomerComponent },

  { path: 'paidChecksOpeningBalance', component: PaidChecksOpeningBalanceComponent },
  { path: 'paidChecksOpeningBalance/add', component: AddPaidChecksOpeningBalanceComponent },
  { path: 'paidChecksOpeningBalance/edit/:id', component: AddPaidChecksOpeningBalanceComponent },
  { path: 'paidChecksOpeningBalance/details/:id', component: AddPaidChecksOpeningBalanceComponent },

  { path: 'writingPayChecks', component: WritingPayCheckComponent },
  { path: 'writingPayChecks/add', component: AddWritingPayCheckComponent },
  { path: 'writingPayChecks/edit/:id', component: AddWritingPayCheckComponent },
  { path: 'writingPayChecks/details/:id', component: AddWritingPayCheckComponent },

  { path: 'paidChecksReturn', component: PaidChecksReturnComponent },
  { path: 'paidChecksReturn/add', component: AddPaidChecksReturnComponent },
  { path: 'paidChecksReturn/edit/:id', component: AddPaidChecksReturnComponent },
  { path: 'paidChecksReturn/details/:id', component: AddPaidChecksReturnComponent },

  { path: 'payChecks', component: PayCheckComponent },
  { path: 'payChecks/add', component: AddPayCheckComponent },
  { path: 'payChecks/edit/:id', component: AddPayCheckComponent },
  { path: 'payChecks/details/:id', component: AddPayCheckComponent },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChequesRoutingModule { }
