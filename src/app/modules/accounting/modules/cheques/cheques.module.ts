import { NgModule } from '@angular/core';
import { BeneficiaryComponent } from './components/files/beneficiary/beneficiary.component';
import { CheckReasonsRejectionComponent } from './components/files/check-reasons-rejection/check-reasons-rejection.component';
import { AddBeneficiaryComponent } from './components/files/beneficiary/add-beneficiary/add-beneficiary.component';
import { AddReasonComponent } from './components/files/check-reasons-rejection/add-reason/add-reason.component';
import { ChequesRoutingModule } from './cheques-routing.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { IdentifiedChecksComponent } from './components/files/identified-checks/identified-checks.component';
import { ReceivedChecksOpeningBalanceComponent } from './components/transactions/received-checks/received-checks-opening-balance/received-checks-opening-balance.component';
import { AddReceivedChecksOpeningBalanceComponent } from './components/transactions/received-checks/received-checks-opening-balance/add-received-checks-opening-balance/add-received-checks-opening-balance.component';
import { ReceivingChecksComponent } from './components/transactions/received-checks/receiving-checks/receiving-checks.component';
import { AddReceivingChecksComponent } from './components/transactions/received-checks/receiving-checks/add-receiving-checks/add-receiving-checks.component';
import { DeliverChecksComponent } from './components/transactions/received-checks/deliver-checks/deliver-checks.component';
import { AddDeliverChecksComponent } from './components/transactions/received-checks/deliver-checks/add-deliver-checks/add-deliver-checks.component';
import { ChecksDepositComponent } from './components/transactions/received-checks/checks-deposit/checks-deposit.component';
import { AddChecksDepositComponent } from './components/transactions/received-checks/checks-deposit/add-checks-deposit/add-checks-deposit.component';
import { ChecksReturnComponent } from './components/transactions/received-checks/checks-return/checks-return.component';
import { AddChecksReturnComponent } from './components/transactions/received-checks/checks-return/add-checks-return/add-checks-return.component';
import { ChecksCollectionComponent } from './components/transactions/received-checks/checks-collection/checks-collection.component';
import { AddChecksCollectionComponent } from './components/transactions/received-checks/checks-collection/add-checks-collection/add-checks-collection.component';
import { ChecksReturnToCustomerComponent } from './components/transactions/received-checks/checks-return-to-customer/checks-return-to-customer.component';
import { AddChecksReturnToCustomerComponent } from './components/transactions/received-checks/checks-return-to-customer/add-checks-return-to-customer/add-checks-return-to-customer.component';
import { PaidChecksOpeningBalanceComponent } from './components/transactions/payable-checks/paid-checks-opening-balance/paid-checks-opening-balance.component';
import { AddPaidChecksOpeningBalanceComponent } from './components/transactions/payable-checks/paid-checks-opening-balance/add-paid-checks-opening-balance/add-paid-checks-opening-balance.component';
import { WritingPayCheckComponent } from './components/transactions/payable-checks/writing-pay-check/writing-pay-check.component';
import { AddWritingPayCheckComponent } from './components/transactions/payable-checks/writing-pay-check/add-writing-pay-check/add-writing-pay-check.component';
import { PaidChecksReturnComponent } from './components/transactions/payable-checks/paid-checks-return/paid-checks-return.component';
import { AddPaidChecksReturnComponent } from './components/transactions/payable-checks/paid-checks-return/add-paid-checks-return/add-paid-checks-return.component';
import { PayCheckComponent } from './components/transactions/payable-checks/pay-check/pay-check.component';
import { AddPayCheckComponent } from './components/transactions/payable-checks/pay-check/add-pay-check/add-pay-check.component';

@NgModule({
  declarations: [
    BeneficiaryComponent,
    AddBeneficiaryComponent,
    CheckReasonsRejectionComponent,
    AddReasonComponent,
    IdentifiedChecksComponent,
    ReceivedChecksOpeningBalanceComponent,
    AddReceivedChecksOpeningBalanceComponent,
    ReceivedChecksOpeningBalanceComponent,
    AddReceivedChecksOpeningBalanceComponent,
    ReceivingChecksComponent,
    AddReceivingChecksComponent,
    DeliverChecksComponent,
    AddDeliverChecksComponent,
    ChecksDepositComponent,
    AddChecksDepositComponent,
    ChecksReturnComponent,
    AddChecksReturnComponent,
    ChecksCollectionComponent,
    AddChecksCollectionComponent,
    ChecksReturnToCustomerComponent,
    AddChecksReturnToCustomerComponent,
    PaidChecksOpeningBalanceComponent,
    AddPaidChecksOpeningBalanceComponent,
    WritingPayCheckComponent,
    AddWritingPayCheckComponent,
    PaidChecksReturnComponent,
    AddPaidChecksReturnComponent,
    PayCheckComponent,
    AddPayCheckComponent
  ],
  imports: [
    SharedModule,
    ChequesRoutingModule
  ]
})
export class ChequesModule { }
