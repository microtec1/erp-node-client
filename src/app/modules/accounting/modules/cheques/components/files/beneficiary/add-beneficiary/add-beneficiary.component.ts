import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IBeneficiary } from '../../../../interfaces/IBeneficiary';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { beneficiariesApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-beneficiary',
  templateUrl: './add-beneficiary.component.html',
  styleUrls: ['./add-beneficiary.component.scss']
})
export class AddBeneficiaryComponent implements OnInit, OnDestroy {
  beneficiaryForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  beneficiary: IBeneficiary;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  companyId = companyId;
  detailsMode: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(beneficiariesApi, null, null, id).subscribe((beneficiary: IBeneficiary) => {
              this.beneficiary = beneficiary;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.beneficiaryForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  get form() {
    return this.beneficiaryForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.beneficiary) {
      if (this.beneficiaryForm.valid) {
        const newBeneficiary = {
          _id: this.beneficiary._id,
          ...this.generalService.checkEmptyFields(this.beneficiaryForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(beneficiariesApi, newBeneficiary).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/cheques/beneficiary']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.beneficiaryForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.beneficiaryForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(beneficiariesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.beneficiaryForm.reset();
            this.beneficiaryForm.patchValue({
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let beneficiaryNameAr = '';
    let beneficiaryNameEn = '';
    let isActive = true;

    if (this.beneficiary) {
      code = this.beneficiary.code;
      beneficiaryNameAr = this.beneficiary.beneficiaryNameAr;
      beneficiaryNameEn = this.beneficiary.beneficiaryNameEn;
      isActive = this.beneficiary.isActive;
    }
    this.beneficiaryForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      beneficiaryNameAr: new FormControl(beneficiaryNameAr, Validators.required),
      beneficiaryNameEn: new FormControl(beneficiaryNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
