import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ICheckReasonRejection } from '../../../../interfaces/ICheckReasonRejection';
import { companyId } from 'src/app/common/constants/general.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { checkReasonsRejectionApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-reason',
  templateUrl: './add-reason.component.html',
  styleUrls: ['./add-reason.component.scss']
})

export class AddReasonComponent implements OnInit, OnDestroy {
  reasonForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  reason: ICheckReasonRejection;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  companyId = companyId;
  detailsMode: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(checkReasonsRejectionApi, null, null, id).subscribe((reason: ICheckReasonRejection) => {
              this.reason = reason;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.reasonForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  get form() {
    return this.reasonForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.reason) {
      if (this.reasonForm.valid) {
        const newReason = {
          _id: this.reason._id,
          ...this.generalService.checkEmptyFields(this.reasonForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(checkReasonsRejectionApi, newReason).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/cheques/checkReasonsRejection']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.reasonForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.reasonForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(checkReasonsRejectionApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.reasonForm.reset();
            this.reasonForm.patchValue({
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let checkReasonsRejectionNameAr = '';
    let checkReasonsRejectionNameEn = '';
    let isActive = true;

    if (this.reason) {
      code = this.reason.code;
      checkReasonsRejectionNameAr = this.reason.checkReasonsRejectionNameAr;
      checkReasonsRejectionNameEn = this.reason.checkReasonsRejectionNameEn;
      isActive = this.reason.isActive;
    }
    this.reasonForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      checkReasonsRejectionNameAr: new FormControl(checkReasonsRejectionNameAr, Validators.required),
      checkReasonsRejectionNameEn: new FormControl(checkReasonsRejectionNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
