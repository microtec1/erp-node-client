import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, checkReasonsRejectionApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { ICheckReasonRejection } from '../../../interfaces/ICheckReasonRejection';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-check-reasons-rejection',
  templateUrl: './check-reasons-rejection.component.html',
  styleUrls: ['./check-reasons-rejection.component.scss']
})

export class CheckReasonsRejectionComponent implements OnInit, OnDestroy {
  checkReasonsRejection: ICheckReasonRejection[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
  ) { }

  ngOnInit() {
    this.getCheckReasonsRejectionFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(checkReasonsRejectionApi, pageNumber).subscribe((res: IDataRes) => {
      this.checkReasonsRejection = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(checkReasonsRejectionApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.checkReasonsRejection = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCheckReasonsRejectionFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(checkReasonsRejectionApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.checkReasonsRejection = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      checkReasonsRejectionNameAr: new FormControl(''),
      checkReasonsRejectionNameEn: new FormControl('')
    });
  }

  deleteModal(checkReasonsRejection: ICheckReasonRejection) {
    const initialState = {
      code: checkReasonsRejection.code,
      nameAr: checkReasonsRejection.checkReasonsRejectionNameAr,
      nameEn: checkReasonsRejection.checkReasonsRejectionNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(checkReasonsRejection._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(checkReasonsRejectionApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.checkReasonsRejection = this.generalService.removeItem(this.checkReasonsRejection, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getCheckReasonsRejectionFirstPage() {
    this.subscriptions.push(
      this.data.get(checkReasonsRejectionApi, 1).subscribe((res: IDataRes) => {
        this.checkReasonsRejection = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
