import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentifiedChecksComponent } from './identified-checks.component';

describe('IdentifiedChecksComponent', () => {
  let component: IdentifiedChecksComponent;
  let fixture: ComponentFixture<IdentifiedChecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentifiedChecksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentifiedChecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
