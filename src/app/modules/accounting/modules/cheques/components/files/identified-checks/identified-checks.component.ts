import { Component, OnInit } from '@angular/core';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { identifiedChecksApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-identified-checks',
  templateUrl: './identified-checks.component.html',
  styleUrls: ['./identified-checks.component.scss']
})
export class IdentifiedChecksComponent implements OnInit {
  showSearch: boolean;
  subscriptions: Subscription[] = [];
  constructor(
    private data: DataService,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.getChecks();
  }

  getChecks() {
    this.subscriptions.push(
      this.data
        .post(identifiedChecksApi, {})
        .subscribe((res: IDataRes) => {
          console.log(res);
          this.uiService.isLoading.next(false);
        })
    );
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

}
