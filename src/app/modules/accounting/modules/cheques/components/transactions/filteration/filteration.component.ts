import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';
import { IBeneficiary } from '../../../interfaces/IBeneficiary';
import { IBox } from '../../../../financial-transactions/interfaces/IBox';
import { IBank } from '../../../../financial-transactions/interfaces/IBank';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { Subscription } from 'rxjs';
import { beneficiariesApi, branchCurrenciesApi, banksApi, safeBoxesApi, customersApi, employeesApi } from 'src/app/common/constants/api.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { searchLength } from 'src/app/common/constants/general.constants';

@Component({
  selector: 'app-filteration',
  templateUrl: './filteration.component.html',
  styleUrls: ['./filteration.component.scss']
})
export class FilterationComponent implements OnInit, OnDestroy {
  filterationForm: FormGroup;
  subscriptions: Subscription[] = [];
  showFilteration: boolean;
  @Output() formSubmitted: EventEmitter<any> = new EventEmitter();

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;


  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Beneficiary
  beneficiaries: IBeneficiary[] = [];
  beneficiariesInputFocused: boolean;
  hasMoreBeneficiaries: boolean;
  beneficiariesCount: number;
  selectedBeneficiariesPage = 1;
  beneficiariesPagesNo: number;
  noBeneficiaries: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.data.get(beneficiariesApi, 1).subscribe((res: IDataRes) => {
        this.beneficiariesPagesNo = res.pages;
        this.beneficiariesCount = res.count;
        if (this.beneficiariesPagesNo > this.selectedBeneficiariesPage) {
          this.hasMoreBeneficiaries = true;
        }
        this.beneficiaries.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
    this.filteration();
  }

  private filteration() {
    this.filterationForm = new FormGroup({
      checkAmount: new FormControl(null),
      checkBankId: new FormControl(null),
      checkBeneficiaryId: new FormControl(null),
      checkNumber: new FormControl(null),
      customerId: new FormControl(null),
      safeBoxId: new FormControl(null)
    });
  }

  clear() {
    this.filterationForm.reset();
    const formValue = this.generalService.checkEmptyFields(this.filterationForm.value);
    this.formSubmitted.emit(formValue);
  }

  filterChecks() {
    const formValue = this.generalService.checkEmptyFields(this.filterationForm.value);
    this.formSubmitted.emit(formValue);
  }

  searchBeneficiaries(event) {
    const searchValue = event;
    const searchQuery = {
      beneficiaryNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(beneficiariesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noBeneficiaries = true;
          } else {
            this.noBeneficiaries = false;
            for (const item of res.results) {
              if (this.beneficiaries.length) {
                const uniqueBeneficiaries = this.beneficiaries.filter(x => x._id !== item._id);
                this.beneficiaries = uniqueBeneficiaries;
              }
              this.beneficiaries.push(item);
            }
          }
          this.beneficiaries = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMorebeneficiaries() {
    this.selectedBeneficiariesPage = this.selectedBeneficiariesPage + 1;
    this.subscriptions.push(
      this.data.get(beneficiariesApi, this.selectedBeneficiariesPage).subscribe((res: IDataRes) => {
        if (this.beneficiariesPagesNo > this.selectedBeneficiariesPage) {
          this.hasMoreBeneficiaries = true;
        } else {
          this.hasMoreBeneficiaries = false;
        }
        for (const item of res.results) {
          if (this.beneficiaries.length) {
            const uniqueBeneficiaries = this.beneficiaries.filter(x => x._id !== item._id);
            this.beneficiaries = uniqueBeneficiaries;
          }
          this.beneficiaries.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
