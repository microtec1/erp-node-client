import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPaidChecksOpeningBalanceComponent } from './add-paid-checks-opening-balance.component';

describe('AddPaidChecksOpeningBalanceComponent', () => {
  let component: AddPaidChecksOpeningBalanceComponent;
  let fixture: ComponentFixture<AddPaidChecksOpeningBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPaidChecksOpeningBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPaidChecksOpeningBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
