import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaidChecksOpeningBalanceComponent } from './paid-checks-opening-balance.component';

describe('PaidChecksOpeningBalanceComponent', () => {
  let component: PaidChecksOpeningBalanceComponent;
  let fixture: ComponentFixture<PaidChecksOpeningBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaidChecksOpeningBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaidChecksOpeningBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
