import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPaidChecksReturnComponent } from './add-paid-checks-return.component';

describe('AddPaidChecksReturnComponent', () => {
  let component: AddPaidChecksReturnComponent;
  let fixture: ComponentFixture<AddPaidChecksReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPaidChecksReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPaidChecksReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
