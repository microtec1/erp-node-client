import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaidChecksReturnComponent } from './paid-checks-return.component';

describe('PaidChecksReturnComponent', () => {
  let component: PaidChecksReturnComponent;
  let fixture: ComponentFixture<PaidChecksReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaidChecksReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaidChecksReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
