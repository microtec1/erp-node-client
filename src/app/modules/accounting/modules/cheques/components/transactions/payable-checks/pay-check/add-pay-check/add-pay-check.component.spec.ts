import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPayCheckComponent } from './add-pay-check.component';

describe('AddPayCheckComponent', () => {
  let component: AddPayCheckComponent;
  let fixture: ComponentFixture<AddPayCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPayCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPayCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
