import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddWritingPayCheckComponent } from './add-writing-pay-check.component';

describe('AddWritingPayCheckComponent', () => {
  let component: AddWritingPayCheckComponent;
  let fixture: ComponentFixture<AddWritingPayCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddWritingPayCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWritingPayCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
