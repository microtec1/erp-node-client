import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { baseUrl, branchCurrenciesApi, banksApi, safeBoxesApi, beneficiariesApi, cashCheckApi, suppliersApi } from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IBeneficiary } from '../../../../../interfaces/IBeneficiary';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IWritingPayCheck } from '../../../../../interfaces/IWritingPayCheck';

@Component({
  selector: 'app-add-writing-pay-check',
  templateUrl: './add-writing-pay-check.component.html',
  styleUrls: ['./add-writing-pay-check.component.scss']
})
export class AddWritingPayCheckComponent implements OnInit, OnDestroy {
  cashCheckForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  cashCheck: IWritingPayCheck;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;


  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Suppliers
  suppliers: ICustomer[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(cashCheckApi, null, null, id).subscribe((check: IWritingPayCheck) => {
              this.cashCheck = check;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.cashCheckForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  get form() {
    return this.cashCheckForm.controls;
  }

  get getCheckListArray() {
    return this.cashCheckForm.get('checksList') as FormArray;
  }

  addList() {
    const control = this.cashCheckForm.get('checksList') as FormArray;
    control.push(
      new FormGroup({
        checkNumber: new FormControl('', Validators.required),
        issueGregorianDate: new FormControl(null, Validators.required),
        issueHijriDate: new FormControl(null, Validators.required),
        maturityGregorianDate: new FormControl(null, Validators.required),
        maturityHijriDate: new FormControl(null, Validators.required),
        checkBankId: new FormControl('', Validators.required),
        checkAmount: new FormControl(null, Validators.required),
        currencyId: new FormControl('', Validators.required),
        currencyExchangeRate: new FormControl(null, Validators.required),
        supplierId: new FormControl('', Validators.required)
      })
    );
  }

  deleteList(index) {
    const control = this.cashCheckForm.get('checksList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.cashCheckForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.cashCheckForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  fillRate(event, formGroup: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        currencyExchangeRate: currency.exchangeRate
      });
    } else {
      this.cashCheckForm.patchValue({
        currencyExchangeRate: currency.exchangeRate
      });
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (typeof this.cashCheckForm.value.gregorianDate !== 'string') {
      this.cashCheckForm.value.gregorianDate =
        this.generalService.format(this.cashCheckForm.value.gregorianDate);
    }
    if (typeof this.cashCheckForm.value.hijriDate !== 'string') {
      this.cashCheckForm.value.hijriDate =
        this.generalService.formatHijriDate(this.cashCheckForm.value.hijriDate);
    }
    for (
      let i = 0;
      i <= this.cashCheckForm.value.checksList.length - 1;
      i++
    ) {
      if (this.cashCheckForm.value.checksList[i].issueGregorianDate) {
        if (
          typeof this.cashCheckForm.value.checksList[i].issueGregorianDate !==
          'string'
        ) {
          this.cashCheckForm.value.checksList[i].issueGregorianDate = this.generalService.format(
            this.cashCheckForm.value.checksList[i].issueGregorianDate
          );
        }
        if (typeof this.cashCheckForm.value.checksList[i].issueHijriDate !==
          'string') {
          this.cashCheckForm.value.checksList[i].issueHijriDate = this.generalService.formatHijriDate(
            this.cashCheckForm.value.checksList[i].issueHijriDate
          );
        }
      }

      if (this.cashCheckForm.value.checksList[i].maturityGregorianDate) {
        if (
          typeof this.cashCheckForm.value.checksList[i].maturityGregorianDate !==
          'string'
        ) {
          this.cashCheckForm.value.checksList[i].maturityGregorianDate = this.generalService.format(
            this.cashCheckForm.value.checksList[i].maturityGregorianDate
          );
        }
        if (typeof this.cashCheckForm.value.checksList[i].maturityHijriDate !==
          'string') {
          this.cashCheckForm.value.checksList[i].maturityHijriDate = this.generalService.formatHijriDate(
            this.cashCheckForm.value.checksList[i].maturityHijriDate
          );
        }
      }
    }
    if (this.cashCheck) {
      if (this.cashCheckForm.valid) {
        const newReceiveCheck = {
          _id: this.cashCheck._id,
          ...this.generalService.checkEmptyFields(this.cashCheckForm.value),
          hijriDate: this.generalService.formatHijriDate(this.cashCheckForm.value.hijriDate)
        };
        this.data.put(cashCheckApi, newReceiveCheck).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/cheques/writingPayCheck']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {
      if (this.cashCheckForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.cashCheckForm.value)
        };
        this.subscriptions.push(
          this.data.post(cashCheckApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.cashCheckForm.reset();
            this.cashCheckForm.patchValue({
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let transactionStatus = 'unposted';
    let descriptionAr = '';
    let descriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let safeBoxId = '';
    let checksListArray = new FormArray([
      new FormGroup({
        checkNumber: new FormControl('', Validators.required),
        issueGregorianDate: new FormControl(null, Validators.required),
        issueHijriDate: new FormControl(null, Validators.required),
        maturityGregorianDate: new FormControl(null, Validators.required),
        maturityHijriDate: new FormControl(null, Validators.required),
        checkBankId: new FormControl('', Validators.required),
        checkAmount: new FormControl(null, Validators.required),
        currencyId: new FormControl('', Validators.required),
        currencyExchangeRate: new FormControl(null, Validators.required),
        supplierId: new FormControl('', Validators.required)
      })
    ]);
    let isActive = true;

    if (this.cashCheck) {
      code = this.cashCheck.code;
      transactionStatus = this.cashCheck.transactionStatus;
      descriptionAr = this.cashCheck.descriptionAr;
      descriptionEn = this.cashCheck.descriptionEn;
      gregorianDate = new Date(this.cashCheck.gregorianDate + 'UTC');
      hijriDate = this.cashCheck.hijriDate;
      safeBoxId = this.cashCheck.safeBoxId;
      isActive = this.cashCheck.isActive;
      checksListArray = new FormArray([]);
      for (const control of this.cashCheck.checksList) {
        checksListArray.push(
          new FormGroup({
            checkNumber: new FormControl(control.checkNumber, Validators.required),
            issueGregorianDate: new FormControl(new Date(control.issueGregorianDate + 'UTC'), Validators.required),
            issueHijriDate: new FormControl(control.issueHijriDate, Validators.required),
            maturityGregorianDate: new FormControl(new Date(control.maturityGregorianDate + 'UTC'), Validators.required),
            maturityHijriDate: new FormControl(control.maturityHijriDate, Validators.required),
            checkBankId: new FormControl(control.checkBankId, Validators.required),
            checkAmount: new FormControl(control.checkAmount, Validators.required),
            currencyId: new FormControl(control.currencyId, Validators.required),
            currencyExchangeRate: new FormControl(control.currencyExchangeRate, Validators.required),
            supplierId: new FormControl(control.supplierId, Validators.required),
          })
        );
      }
    }
    this.cashCheckForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      transactionStatus: new FormControl(transactionStatus),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      safeBoxId: new FormControl(safeBoxId, Validators.required),
      checksList: checksListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
