import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WritingPayCheckComponent } from './writing-pay-check.component';

describe('WritingPayCheckComponent', () => {
  let component: WritingPayCheckComponent;
  let fixture: ComponentFixture<WritingPayCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WritingPayCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WritingPayCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
