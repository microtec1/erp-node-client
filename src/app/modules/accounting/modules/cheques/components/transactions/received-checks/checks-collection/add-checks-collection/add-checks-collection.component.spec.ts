import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChecksCollectionComponent } from './add-checks-collection.component';

describe('AddChecksCollectionComponent', () => {
  let component: AddChecksCollectionComponent;
  let fixture: ComponentFixture<AddChecksCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChecksCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChecksCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
