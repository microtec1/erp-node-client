import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { ICollectCheck } from '../../../../../interfaces/ICollectCheck';
import { Subscription } from 'rxjs';
import { baseUrl, checkCollectionApi, branchCurrenciesApi, banksApi, customersApi, identifiedChecksApi } from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { ICheck } from '../../../../../interfaces/ICheck';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-add-checks-collection',
  templateUrl: './add-checks-collection.component.html',
  styleUrls: ['./add-checks-collection.component.scss']
})
export class AddChecksCollectionComponent implements OnInit, OnDestroy {
  collectCheckForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  collectCheck: ICollectCheck;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  checks: ICheck[] = [];
  searchBody = {
    checkType: 'received_check',
    checkStatus: 'under_collection'
  };

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(checkCollectionApi, null, null, id).subscribe((collectCheck: ICollectCheck) => {
              this.collectCheck = collectCheck;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.collectCheckForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.getChecks(this.searchBody);
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  filterChecks(formValue) {
    const searchBody = {
      ...this.searchBody,
      ...formValue
    };
    this.getChecks(searchBody);
  }

  getChecks(searchBody) {
    this.subscriptions.push(
      this.data
        .post(identifiedChecksApi, searchBody)
        .subscribe((res: IDataRes) => {
          const checkList: ICheck[] = res.results;
          if (checkList.length) {
            this.getCheckListArray.controls = [];
            for (const control of checkList) {
              this.getCheckListArray.push(
                new FormGroup({
                  checkNumber: new FormControl(control.checkNumber),
                  issueGregorianDate: new FormControl(control.issueGregorianDate),
                  issueHijriDate: new FormControl(control.issueHijriDate),
                  maturityGregorianDate: new FormControl(control.maturityGregorianDate),
                  maturityHijriDate: new FormControl(control.maturityHijriDate),
                  checkBankId: new FormControl(control.checkBankId),
                  checkAmount: new FormControl(control.checkAmount),
                  currencyId: new FormControl(control.currencyId),
                  currencyExchangeRate: new FormControl(control.currencyExchangeRate),
                  customerId: new FormControl(control.customerId),
                  checkStatus: new FormControl(control.checkStatus)
                })
              );
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.collectCheckForm.controls;
  }

  get getCheckListArray() {
    return this.collectCheckForm.get('checksList') as FormArray;
  }

  deleteList(index) {
    const control = this.collectCheckForm.get('checksList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.collectCheckForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.collectCheckForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.collectCheckForm.value.gregorianDate !== 'string') {
      this.collectCheckForm.value.gregorianDate =
        this.generalService.format(this.collectCheckForm.value.gregorianDate);
    }
    if (typeof this.collectCheckForm.value.hijriDate !== 'string') {
      this.collectCheckForm.value.hijriDate =
        this.generalService.formatHijriDate(this.collectCheckForm.value.hijriDate);
    }

    if (this.collectCheck) {
      if (this.collectCheckForm.valid) {
        const newCollectCheck = {
          _id: this.collectCheck._id,
          ...this.generalService.checkEmptyFields(this.collectCheckForm.value)
        };
        this.data.put(checkCollectionApi, newCollectCheck).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/cheques/checksCollection']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {
      if (this.collectCheckForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.collectCheckForm.value),
          companyId
        };
        const formValueModified = {
          ...formValue
        };
        this.subscriptions.push(
          this.data.post(checkCollectionApi, formValueModified).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.collectCheckForm.reset();
            this.collectCheckForm.patchValue({
              isActive: true
            });
            this.getCheckListArray.controls = [];
            this.getChecks(this.searchBody);
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let transactionStatus = 'unposted';
    let descriptionAr = '';
    let descriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let bankId = '';
    let checksListArray = new FormArray([]);
    let isActive = true;

    if (this.collectCheck) {
      code = this.collectCheck.code;
      transactionStatus = this.collectCheck.transactionStatus;
      descriptionAr = this.collectCheck.descriptionAr;
      descriptionEn = this.collectCheck.descriptionEn;
      gregorianDate = new Date(this.collectCheck.gregorianDate + 'UTC');
      hijriDate = this.collectCheck.hijriDate;
      bankId = this.collectCheck.bankId;
      isActive = this.collectCheck.isActive;
      checksListArray = new FormArray([]);
      for (const control of this.collectCheck.checksList) {
        checksListArray.push(
          new FormGroup({
            checkNumber: new FormControl(control.checkNumber),
            issueGregorianDate: new FormControl(control.issueGregorianDate),
            issueHijriDate: new FormControl(control.issueHijriDate),
            maturityGregorianDate: new FormControl(control.maturityGregorianDate),
            maturityHijriDate: new FormControl(control.maturityHijriDate),
            checkBankId: new FormControl(control.checkBankId),
            checkAmount: new FormControl(control.checkAmount),
            currencyId: new FormControl(control.currencyId),
            currencyExchangeRate: new FormControl(control.currencyExchangeRate),
            customerId: new FormControl(control.customerId),
            checkStatus: new FormControl(control.checkStatus)
          })
        );
      }
    }
    this.collectCheckForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      transactionStatus: new FormControl(transactionStatus),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      bankId: new FormControl(bankId, Validators.required),
      checksList: checksListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
