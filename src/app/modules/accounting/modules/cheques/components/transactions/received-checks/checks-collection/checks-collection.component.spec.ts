import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecksCollectionComponent } from './checks-collection.component';

describe('ChecksCollectionComponent', () => {
  let component: ChecksCollectionComponent;
  let fixture: ComponentFixture<ChecksCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecksCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecksCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
