import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICollectCheck } from '../../../../interfaces/ICollectCheck';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, checkCollectionApi, postCheckCollectionApi, unPostCheckCollectionApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-checks-collection',
  templateUrl: './checks-collection.component.html',
  styleUrls: ['./checks-collection.component.scss']
})
export class ChecksCollectionComponent implements OnInit, OnDestroy {
  checksCollection: ICollectCheck[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getChecksCollectionFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(checkCollectionApi, pageNumber).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'check_collection') {
          if (this.checksCollection.length) {
            const uniquechecksCollection = this.checksCollection.filter(x => x._id !== item._id);
            this.checksCollection = uniquechecksCollection;
          }
          this.checksCollection.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(checkCollectionApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'check_collection') {
          if (this.checksCollection.length) {
            const uniquechecksCollection = this.checksCollection.filter(x => x._id !== item._id);
            this.checksCollection = uniquechecksCollection;
          }
          this.checksCollection.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postCheckCollectionApi}/${id}`, {}).subscribe(res => {
        this.checksCollection = this.generalService.changeStatus(this.checksCollection, id, 'posted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostCheckCollectionApi}/${id}`, {}).subscribe(res => {
        this.checksCollection = this.generalService.changeStatus(this.checksCollection, id, 'unposted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getChecksCollectionFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(checkCollectionApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            for (const item of res.results) {
              if (item.transactionName === 'check_collection') {
                if (this.checksCollection.length) {
                  const uniquechecksCollection = this.checksCollection.filter(x => x._id !== item._id);
                  this.checksCollection = uniquechecksCollection;
                }
                this.checksCollection.push(item);
              }
            }
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      descriptionAr: new FormControl(''),
      descriptionEn: new FormControl('')
    });
  }

  deleteModal(checkCollection: ICollectCheck) {
    const initialState = {
      code: checkCollection.code,
      nameAr: checkCollection.descriptionAr,
      nameEn: checkCollection.descriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(checkCollection._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(checkCollectionApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.checksCollection = this.generalService.removeItem(this.checksCollection, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getChecksCollectionFirstPage() {
    this.subscriptions.push(
      this.data.get(checkCollectionApi, 1).subscribe((res: IDataRes) => {
        for (const item of res.results) {
          if (item.transactionName === 'check_collection') {
            if (this.checksCollection.length) {
              const uniquechecksCollection = this.checksCollection.filter(x => x._id !== item._id);
              this.checksCollection = uniquechecksCollection;
            }
            this.checksCollection.push(item);
          }
        }
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
