import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChecksDepositComponent } from './add-checks-deposit.component';

describe('AddChecksDepositComponent', () => {
  let component: AddChecksDepositComponent;
  let fixture: ComponentFixture<AddChecksDepositComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChecksDepositComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChecksDepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
