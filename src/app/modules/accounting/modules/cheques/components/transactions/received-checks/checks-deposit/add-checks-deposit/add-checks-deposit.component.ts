import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { IDepositCheck } from '../../../../../interfaces/IDepositCheck';
import { Subscription } from 'rxjs';
import { baseUrl, depositChecksApi, employeesApi, branchCurrenciesApi, banksApi, customersApi, receivingChecksApi, identifiedChecksApi } from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { ICheck } from '../../../../../interfaces/ICheck';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-add-checks-deposit',
  templateUrl: './add-checks-deposit.component.html',
  styleUrls: ['./add-checks-deposit.component.scss']
})
export class AddChecksDepositComponent implements OnInit, OnDestroy {
  depositCheckForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  depositCheck: IDepositCheck;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  checks: ICheck[] = [];
  searchBody = {
    checkType: 'received_check',
    checkStatus: 'delivered_to_employee'
  };

  // Employees
  employees: IEmployee[] = [];
  employeesInputFocused: boolean;
  hasMoreEmployees: boolean;
  employeesCount: number;
  selectedEmployeesPage = 1;
  employeesPagesNo: number;
  noEmployees: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(depositChecksApi, null, null, id).subscribe((depositCheck: IDepositCheck) => {
              this.depositCheck = depositCheck;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.depositCheckForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.getChecks(this.searchBody);
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(employeesApi, 1).subscribe((res: IDataRes) => {
        this.employeesPagesNo = res.pages;
        this.employeesCount = res.count;
        if (this.employeesPagesNo > this.selectedEmployeesPage) {
          this.hasMoreEmployees = true;
        }
        this.employees.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  filterChecks(formValue) {
    const searchBody = {
      ...this.searchBody,
      ...formValue
    };
    this.getChecks(searchBody);
  }

  getChecks(searchBody) {
    this.subscriptions.push(
      this.data
        .post(identifiedChecksApi, searchBody)
        .subscribe((res: IDataRes) => {
          const checkList: ICheck[] = res.results;
          if (checkList.length) {
            this.getCheckListArray.controls = [];
            for (const control of checkList) {
              this.getCheckListArray.push(
                new FormGroup({
                  checkNumber: new FormControl(control.checkNumber),
                  issueGregorianDate: new FormControl(control.issueGregorianDate),
                  issueHijriDate: new FormControl(control.issueHijriDate),
                  maturityGregorianDate: new FormControl(control.maturityGregorianDate),
                  maturityHijriDate: new FormControl(control.maturityHijriDate),
                  checkBankId: new FormControl(control.checkBankId),
                  checkReceiverId: new FormControl(control.checkReceiverId),
                  checkAmount: new FormControl(control.checkAmount),
                  currencyId: new FormControl(control.currencyId),
                  currencyExchangeRate: new FormControl(control.currencyExchangeRate),
                  customerId: new FormControl(control.customerId),
                  checkStatus: new FormControl(control.checkStatus)
                })
              );
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchEmployees(event) {
    const searchValue = event;
    const searchQuery = {
      employeeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(employeesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noEmployees = true;
            } else {
              this.noEmployees = false;
              for (const item of res.results) {
                if (this.employees.length) {
                  const uniqueEmployees = this.employees.filter(
                    x => x._id !== item._id
                  );
                  this.employees = uniqueEmployees;
                }
                this.employees.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreEmployees() {
    this.selectedEmployeesPage = this.selectedEmployeesPage + 1;
    this.subscriptions.push(
      this.data
        .get(employeesApi, this.selectedEmployeesPage)
        .subscribe((res: IDataRes) => {
          if (this.employeesPagesNo > this.selectedEmployeesPage) {
            this.hasMoreEmployees = true;
          } else {
            this.hasMoreEmployees = false;
          }
          for (const item of res.results) {
            if (this.employees.length) {
              const uniqueEmployees = this.employees.filter(x => x._id !== item._id);
              this.employees = uniqueEmployees;
            }
            this.employees.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.depositCheckForm.controls;
  }

  get getCheckListArray() {
    return this.depositCheckForm.get('checksList') as FormArray;
  }

  deleteList(index) {
    const control = this.depositCheckForm.get('checksList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.depositCheckForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.depositCheckForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.depositCheckForm.value.gregorianDate !== 'string') {
      this.depositCheckForm.value.gregorianDate =
        this.generalService.format(this.depositCheckForm.value.gregorianDate);
    }
    if (typeof this.depositCheckForm.value.hijriDate !== 'string') {
      this.depositCheckForm.value.hijriDate =
        this.generalService.formatHijriDate(this.depositCheckForm.value.hijriDate);
    }

    if (this.depositCheck) {
      if (this.depositCheckForm.valid) {
        const newDepositCheck = {
          _id: this.depositCheck._id,
          ...this.generalService.checkEmptyFields(this.depositCheckForm.value)
        };
        this.data.put(depositChecksApi, newDepositCheck).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/cheques/depositChecks']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {
      if (this.depositCheckForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.depositCheckForm.value),
          companyId
        };
        const formValueModified = {
          ...formValue
        };
        this.subscriptions.push(
          this.data.post(depositChecksApi, formValueModified).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.depositCheckForm.reset();
            this.depositCheckForm.patchValue({
              isActive: true
            });
            this.getCheckListArray.controls = [];
            this.getChecks(this.searchBody);
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let transactionStatus = 'unposted';
    let descriptionAr = '';
    let descriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let depositBankId = '';
    let checksListArray = new FormArray([]);
    let isActive = true;

    if (this.depositCheck) {
      code = this.depositCheck.code;
      transactionStatus = this.depositCheck.transactionStatus;
      descriptionAr = this.depositCheck.descriptionAr;
      descriptionEn = this.depositCheck.descriptionEn;
      gregorianDate = new Date(this.depositCheck.gregorianDate + 'UTC');
      hijriDate = this.depositCheck.hijriDate;
      depositBankId = this.depositCheck.depositBankId;
      isActive = this.depositCheck.isActive;
      checksListArray = new FormArray([]);
      for (const control of this.depositCheck.checksList) {
        checksListArray.push(
          new FormGroup({
            checkNumber: new FormControl(control.checkNumber),
            issueGregorianDate: new FormControl(control.issueGregorianDate),
            issueHijriDate: new FormControl(control.issueHijriDate),
            maturityGregorianDate: new FormControl(control.maturityGregorianDate),
            maturityHijriDate: new FormControl(control.maturityHijriDate),
            checkBankId: new FormControl(control.checkBankId),
            checkAmount: new FormControl(control.checkAmount),
            currencyId: new FormControl(control.currencyId),
            currencyExchangeRate: new FormControl(control.currencyExchangeRate),
            customerId: new FormControl(control.customerId),
            checkReceiverId: new FormControl(control.checkReceiverId),
            checkStatus: new FormControl(control.checkStatus)
          })
        );
      }
    }
    this.depositCheckForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      transactionStatus: new FormControl(transactionStatus),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      depositBankId: new FormControl(depositBankId, Validators.required),
      checksList: checksListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
