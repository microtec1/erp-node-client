import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecksDepositComponent } from './checks-deposit.component';

describe('ChecksDepositComponent', () => {
  let component: ChecksDepositComponent;
  let fixture: ComponentFixture<ChecksDepositComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecksDepositComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecksDepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
