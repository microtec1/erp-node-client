import { Component, OnInit, OnDestroy } from '@angular/core';
import { depositChecksApi, baseUrl, postDepositChecksApi, unPostDepositChecksApi, identifiedChecksApi } from 'src/app/common/constants/api.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IDepositCheck } from '../../../../interfaces/IDepositCheck';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';

@Component({
  selector: 'app-checks-deposit',
  templateUrl: './checks-deposit.component.html',
  styleUrls: ['./checks-deposit.component.scss']
})
export class ChecksDepositComponent implements OnInit, OnDestroy {
  depositChecks: IDepositCheck[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getDepositChecksFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(depositChecksApi, pageNumber).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'deliver_check') {
          if (this.depositChecks.length) {
            const uniqueDepositChecks = this.depositChecks.filter(x => x._id !== item._id);
            this.depositChecks = uniqueDepositChecks;
          }
          this.depositChecks.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(depositChecksApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'deliver_check') {
          if (this.depositChecks.length) {
            const uniqueDepositChecks = this.depositChecks.filter(x => x._id !== item._id);
            this.depositChecks = uniqueDepositChecks;
          }
          this.depositChecks.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postDepositChecksApi}/${id}`, {}).subscribe(res => {
        this.depositChecks = this.generalService.changeStatus(this.depositChecks, id, 'posted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostDepositChecksApi}/${id}`, {}).subscribe(res => {
        this.depositChecks = this.generalService.changeStatus(this.depositChecks, id, 'unposted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getDepositChecksFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(depositChecksApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            for (const item of res.results) {
              if (item.transactionName === 'deposit_check') {
                if (this.depositChecks.length) {
                  const uniqueDepositChecks = this.depositChecks.filter(x => x._id !== item._id);
                  this.depositChecks = uniqueDepositChecks;
                }
                this.depositChecks.push(item);
              }
            }
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      descriptionAr: new FormControl(''),
      descriptionEn: new FormControl('')
    });
  }

  deleteModal(depositCheck: IDepositCheck) {
    const initialState = {
      code: depositCheck.code,
      nameAr: depositCheck.descriptionAr,
      nameEn: depositCheck.descriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(depositCheck._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(depositChecksApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.depositChecks = this.generalService.removeItem(this.depositChecks, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getDepositChecksFirstPage() {
    this.subscriptions.push(
      this.data.get(depositChecksApi, 1).subscribe((res: IDataRes) => {
        for (const item of res.results) {
          if (item.transactionName === 'deposit_check') {
            if (this.depositChecks.length) {
              const uniqueDepositChecks = this.depositChecks.filter(x => x._id !== item._id);
              this.depositChecks = uniqueDepositChecks;
            }
            this.depositChecks.push(item);
          }
        }
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
