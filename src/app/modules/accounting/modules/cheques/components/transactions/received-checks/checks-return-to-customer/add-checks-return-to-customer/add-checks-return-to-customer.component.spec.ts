import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChecksReturnToCustomerComponent } from './add-checks-return-to-customer.component';

describe('AddChecksReturnToCustomerComponent', () => {
  let component: AddChecksReturnToCustomerComponent;
  let fixture: ComponentFixture<AddChecksReturnToCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChecksReturnToCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChecksReturnToCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
