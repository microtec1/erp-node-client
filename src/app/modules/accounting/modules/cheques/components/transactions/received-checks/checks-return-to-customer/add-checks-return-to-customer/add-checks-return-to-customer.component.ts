import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { baseUrl, branchCurrenciesApi, banksApi, customersApi, identifiedChecksApi, returnChecksToCustomersApi, safeBoxesApi } from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { ICheck } from '../../../../../interfaces/ICheck';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IReturnCheckToCustomer } from '../../../../../interfaces/IReturnCheckToCustomer';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';

@Component({
  selector: 'app-add-checks-return-to-customer',
  templateUrl: './add-checks-return-to-customer.component.html',
  styleUrls: ['./add-checks-return-to-customer.component.scss']
})
export class AddChecksReturnToCustomerComponent implements OnInit, OnDestroy {
  returnCheckToCustomerForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  returnCheck: IReturnCheckToCustomer;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  checks: ICheck[] = [];
  searchBody = {
    checkType: 'received_check',
    checkStatus: 'returned'
  };

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Safeboxes
  safeboxes: IBox[] = [];
  safeboxesInputFocused: boolean;
  hasMoreSafeboxes: boolean;
  safeboxesCount: number;
  selectedSafeboxesPage = 1;
  safeboxesPagesNo: number;
  noSafeboxes: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(returnChecksToCustomersApi, null, null, id).subscribe((returnCheck: IReturnCheckToCustomer) => {
              this.returnCheck = returnCheck;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.returnCheckToCustomerForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.getChecks(this.searchBody);
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );


    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeboxesPagesNo = res.pages;
        this.safeboxesCount = res.count;
        if (this.safeboxesPagesNo > this.selectedSafeboxesPage) {
          this.hasMoreSafeboxes = true;
        }
        this.safeboxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  filterChecks(formValue) {
    const searchBody = {
      ...this.searchBody,
      ...formValue
    };
    this.getChecks(searchBody);
  }

  getChecks(searchBody) {
    this.subscriptions.push(
      this.data
        .post(identifiedChecksApi, searchBody)
        .subscribe((res: IDataRes) => {
          const checkList: ICheck[] = res.results;
          if (checkList.length) {
            this.getCheckListArray.controls = [];
            for (const control of checkList) {
              this.getCheckListArray.push(
                new FormGroup({
                  checkNumber: new FormControl(control.checkNumber),
                  issueGregorianDate: new FormControl(control.issueGregorianDate),
                  issueHijriDate: new FormControl(control.issueHijriDate),
                  maturityGregorianDate: new FormControl(control.maturityGregorianDate),
                  maturityHijriDate: new FormControl(control.maturityHijriDate),
                  checkBankId: new FormControl(control.checkBankId),
                  safeBoxId: new FormControl(control.safeBoxId),
                  checkAmount: new FormControl(control.checkAmount),
                  currencyId: new FormControl(control.currencyId),
                  currencyExchangeRate: new FormControl(control.currencyExchangeRate),
                  customerId: new FormControl(control.customerId),
                  checkStatus: new FormControl(control.checkStatus)
                })
              );
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.returnCheckToCustomerForm.controls;
  }

  get getCheckListArray() {
    return this.returnCheckToCustomerForm.get('checksList') as FormArray;
  }

  deleteList(index) {
    const control = this.returnCheckToCustomerForm.get('checksList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.returnCheckToCustomerForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.returnCheckToCustomerForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.returnCheckToCustomerForm.value.gregorianDate !== 'string') {
      this.returnCheckToCustomerForm.value.gregorianDate =
        this.generalService.format(this.returnCheckToCustomerForm.value.gregorianDate);
    }
    if (typeof this.returnCheckToCustomerForm.value.hijriDate !== 'string') {
      this.returnCheckToCustomerForm.value.hijriDate =
        this.generalService.formatHijriDate(this.returnCheckToCustomerForm.value.hijriDate);
    }

    if (this.returnCheck) {
      if (this.returnCheckToCustomerForm.valid) {
        const newReturnCheck = {
          _id: this.returnCheck._id,
          ...this.generalService.checkEmptyFields(this.returnCheckToCustomerForm.value)
        };
        this.data.put(returnChecksToCustomersApi, newReturnCheck).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/cheques/checksReturnToCustomer']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {
      if (this.returnCheckToCustomerForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.returnCheckToCustomerForm.value),
          companyId
        };
        const formValueModified = {
          ...formValue
        };
        this.subscriptions.push(
          this.data.post(returnChecksToCustomersApi, formValueModified).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.returnCheckToCustomerForm.reset();
            this.returnCheckToCustomerForm.patchValue({
              isActive: true
            });
            this.getCheckListArray.controls = [];
            this.getChecks(this.searchBody);
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let transactionStatus = 'unposted';
    let descriptionAr = '';
    let descriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let checksListArray = new FormArray([]);
    let isActive = true;

    if (this.returnCheck) {
      code = this.returnCheck.code;
      transactionStatus = this.returnCheck.transactionStatus;
      descriptionAr = this.returnCheck.descriptionAr;
      descriptionEn = this.returnCheck.descriptionEn;
      gregorianDate = new Date(this.returnCheck.gregorianDate + 'UTC');
      hijriDate = this.returnCheck.hijriDate;
      isActive = this.returnCheck.isActive;
      checksListArray = new FormArray([]);
      for (const control of this.returnCheck.checksList) {
        checksListArray.push(
          new FormGroup({
            checkNumber: new FormControl(control.checkNumber),
            issueGregorianDate: new FormControl(control.issueGregorianDate),
            issueHijriDate: new FormControl(control.issueHijriDate),
            maturityGregorianDate: new FormControl(control.maturityGregorianDate),
            maturityHijriDate: new FormControl(control.maturityHijriDate),
            checkBankId: new FormControl(control.checkBankId),
            safeBoxId: new FormControl(control.safeBoxId),
            checkAmount: new FormControl(control.checkAmount),
            currencyId: new FormControl(control.currencyId),
            currencyExchangeRate: new FormControl(control.currencyExchangeRate),
            customerId: new FormControl(control.customerId),
            checkStatus: new FormControl(control.checkStatus)
          })
        );
      }
    }
    this.returnCheckToCustomerForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      transactionStatus: new FormControl(transactionStatus),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      checksList: checksListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
