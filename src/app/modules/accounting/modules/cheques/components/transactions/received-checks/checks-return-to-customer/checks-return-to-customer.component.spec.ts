import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecksReturnToCustomerComponent } from './checks-return-to-customer.component';

describe('ChecksReturnToCustomerComponent', () => {
  let component: ChecksReturnToCustomerComponent;
  let fixture: ComponentFixture<ChecksReturnToCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecksReturnToCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecksReturnToCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
