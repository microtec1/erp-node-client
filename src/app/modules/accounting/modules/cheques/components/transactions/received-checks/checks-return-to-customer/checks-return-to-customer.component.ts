import { Component, OnInit, OnDestroy } from '@angular/core';
import { IReturnCheckToCustomer } from '../../../../interfaces/IReturnCheckToCustomer';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, returnChecksToCustomersApi, postReturnChecksToCustomersApi, unPostReturnChecksToCustomersApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-checks-return-to-customer',
  templateUrl: './checks-return-to-customer.component.html',
  styleUrls: ['./checks-return-to-customer.component.scss']
})
export class ChecksReturnToCustomerComponent implements OnInit, OnDestroy {
  returnChecksToCustomer: IReturnCheckToCustomer[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getReturnedChecksToCustomerFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(returnChecksToCustomersApi, pageNumber).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'return_check_to_customers') {
          if (this.returnChecksToCustomer.length) {
            const uniqueReturnChecksToCustomer = this.returnChecksToCustomer.filter(x => x._id !== item._id);
            this.returnChecksToCustomer = uniqueReturnChecksToCustomer;
          }
          this.returnChecksToCustomer.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(returnChecksToCustomersApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'return_check_to_customers') {
          if (this.returnChecksToCustomer.length) {
            const uniqueReturnChecksToCustomer = this.returnChecksToCustomer.filter(x => x._id !== item._id);
            this.returnChecksToCustomer = uniqueReturnChecksToCustomer;
          }
          this.returnChecksToCustomer.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postReturnChecksToCustomersApi}/${id}`, {}).subscribe(res => {
        this.returnChecksToCustomer = this.generalService.changeStatus(this.returnChecksToCustomer, id, 'posted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostReturnChecksToCustomersApi}/${id}`, {}).subscribe(res => {
        this.returnChecksToCustomer = this.generalService.changeStatus(this.returnChecksToCustomer, id, 'unposted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getReturnedChecksToCustomerFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(returnChecksToCustomersApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            for (const item of res.results) {
              if (item.transactionName === 'return_check_to_customers') {
                if (this.returnChecksToCustomer.length) {
                  const uniqueReturnChecksToCustomer = this.returnChecksToCustomer.filter(x => x._id !== item._id);
                  this.returnChecksToCustomer = uniqueReturnChecksToCustomer;
                }
                this.returnChecksToCustomer.push(item);
              }
            }
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      descriptionAr: new FormControl(''),
      descriptionEn: new FormControl('')
    });
  }

  deleteModal(check: IReturnCheckToCustomer) {
    const initialState = {
      code: check.code,
      nameAr: check.descriptionAr,
      nameEn: check.descriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(check._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(returnChecksToCustomersApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.returnChecksToCustomer = this.generalService.removeItem(this.returnChecksToCustomer, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getReturnedChecksToCustomerFirstPage() {
    this.subscriptions.push(
      this.data.get(returnChecksToCustomersApi, 1).subscribe((res: IDataRes) => {
        for (const item of res.results) {
          if (item.transactionName === 'return_check_to_customers') {
            if (this.returnChecksToCustomer.length) {
              const uniqueReturnChecksToCustomer = this.returnChecksToCustomer.filter(x => x._id !== item._id);
              this.returnChecksToCustomer = uniqueReturnChecksToCustomer;
            }
            this.returnChecksToCustomer.push(item);
          }
        }
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
