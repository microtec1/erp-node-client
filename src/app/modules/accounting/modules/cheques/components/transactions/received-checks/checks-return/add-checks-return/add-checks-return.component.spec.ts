import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChecksReturnComponent } from './add-checks-return.component';

describe('AddChecksReturnComponent', () => {
  let component: AddChecksReturnComponent;
  let fixture: ComponentFixture<AddChecksReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChecksReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChecksReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
