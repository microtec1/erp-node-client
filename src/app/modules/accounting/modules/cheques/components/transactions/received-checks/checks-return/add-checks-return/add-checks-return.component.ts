import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { IReturnCheck } from '../../../../../interfaces/IReturnCheck';
import { Subscription } from 'rxjs';
import { baseUrl, returnedChecksApi, branchCurrenciesApi, banksApi, customersApi, checkReasonsRejectionApi, identifiedChecksApi, safeBoxesApi } from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { ICheck } from '../../../../../interfaces/ICheck';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { ICheckReasonRejection } from '../../../../../interfaces/ICheckReasonRejection';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-add-checks-return',
  templateUrl: './add-checks-return.component.html',
  styleUrls: ['./add-checks-return.component.scss']
})
export class AddChecksReturnComponent implements OnInit, OnDestroy {
  returnCheckForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  returnCheck: IReturnCheck;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  checks: ICheck[] = [];
  searchBody = {
    checkType: 'received_check',
    checkStatus: 'under_collection'
  };

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Safeboxes
  safeboxes: IBank[] = [];
  safeboxesInputFocused: boolean;
  hasMoreSafeboxes: boolean;
  safeboxesCount: number;
  selectedSafeboxesPage = 1;
  safeboxesPagesNo: number;
  noSafeboxes: boolean;

  // Rejection Reasons
  rejectionReasons: ICheckReasonRejection[] = [];
  rejectionReasonsInputFocused: boolean;
  hasMoreRejectionReasons: boolean;
  rejectionReasonsCount: number;
  selectedRejectionReasonsPage = 1;
  rejectionReasonsPagesNo: number;
  noRejectionReasons: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(returnedChecksApi, null, null, id).subscribe((returnCheck: IReturnCheck) => {
              this.returnCheck = returnCheck;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.returnCheckForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.getChecks(this.searchBody);
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeboxesPagesNo = res.pages;
        this.safeboxesCount = res.count;
        if (this.safeboxesPagesNo > this.selectedSafeboxesPage) {
          this.hasMoreSafeboxes = true;
        }
        this.safeboxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(checkReasonsRejectionApi, 1).subscribe((res: IDataRes) => {
        this.rejectionReasonsPagesNo = res.pages;
        this.rejectionReasonsCount = res.count;
        if (this.rejectionReasonsPagesNo > this.selectedRejectionReasonsPage) {
          this.hasMoreRejectionReasons = true;
        }
        this.rejectionReasons.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  filterChecks(formValue) {
    const searchBody = {
      ...this.searchBody,
      ...formValue
    };
    this.getChecks(searchBody);
  }

  getChecks(searchBody) {
    this.subscriptions.push(
      this.data
        .post(identifiedChecksApi, searchBody)
        .subscribe((res: IDataRes) => {
          const checkList: ICheck[] = res.results;
          if (checkList.length) {
            this.getCheckListArray.controls = [];
            for (const control of checkList) {
              this.getCheckListArray.push(
                new FormGroup({
                  checkNumber: new FormControl(control.checkNumber),
                  issueGregorianDate: new FormControl(control.issueGregorianDate),
                  issueHijriDate: new FormControl(control.issueHijriDate),
                  maturityGregorianDate: new FormControl(control.maturityGregorianDate),
                  maturityHijriDate: new FormControl(control.maturityHijriDate),
                  checkBankId: new FormControl(control.checkBankId),
                  checkAmount: new FormControl(control.checkAmount),
                  currencyId: new FormControl(control.currencyId),
                  currencyExchangeRate: new FormControl(control.currencyExchangeRate),
                  customerId: new FormControl(control.customerId),
                  depositBankId: new FormControl(control.depositBankId),
                  safeBoxId: new FormControl(control.safeBoxId),
                  checkReasonsRejectionId: new FormControl(control.checkReasonsRejectionId),
                  checkStatus: new FormControl(control.checkStatus)
                })
              );
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.returnCheckForm.controls;
  }

  get getCheckListArray() {
    return this.returnCheckForm.get('checksList') as FormArray;
  }

  deleteList(index) {
    const control = this.returnCheckForm.get('checksList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  searchRejectionReasons(event) {
    const searchValue = event;
    const searchQuery = {
      checkReasonsRejectionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(checkReasonsRejectionApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noRejectionReasons = true;
            } else {
              this.noRejectionReasons = false;
              for (const item of res.results) {
                if (this.rejectionReasons.length) {
                  const uniqueRejectionReasons = this.rejectionReasons.filter(
                    x => x._id !== item._id
                  );
                  this.rejectionReasons = uniqueRejectionReasons;
                }
                this.rejectionReasons.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreRejectionReasons() {
    this.selectedRejectionReasonsPage = this.selectedRejectionReasonsPage + 1;
    this.subscriptions.push(
      this.data
        .get(checkReasonsRejectionApi, this.selectedRejectionReasonsPage)
        .subscribe((res: IDataRes) => {
          if (this.rejectionReasonsPagesNo > this.selectedRejectionReasonsPage) {
            this.hasMoreRejectionReasons = true;
          } else {
            this.hasMoreRejectionReasons = false;
          }
          for (const item of res.results) {
            if (this.rejectionReasons.length) {
              const uniquerejectionReasons = this.rejectionReasons.filter(x => x._id !== item._id);
              this.rejectionReasons = uniquerejectionReasons;
            }
            this.rejectionReasons.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.returnCheckForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.returnCheckForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.returnCheckForm.value.gregorianDate !== 'string') {
      this.returnCheckForm.value.gregorianDate =
        this.generalService.format(this.returnCheckForm.value.gregorianDate);
    }
    if (typeof this.returnCheckForm.value.hijriDate !== 'string') {
      this.returnCheckForm.value.hijriDate =
        this.generalService.formatHijriDate(this.returnCheckForm.value.hijriDate);
    }

    if (this.returnCheck) {
      if (this.returnCheckForm.valid) {
        const newReturnCheck = {
          _id: this.returnCheck._id,
          ...this.generalService.checkEmptyFields(this.returnCheckForm.value)
        };
        this.data.put(returnedChecksApi, newReturnCheck).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/cheques/checksReturn']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {
      if (this.returnCheckForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.returnCheckForm.value),
          companyId
        };
        const formValueModified = {
          ...formValue
        };
        this.subscriptions.push(
          this.data.post(returnedChecksApi, formValueModified).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.returnCheckForm.reset();
            this.returnCheckForm.patchValue({
              isActive: true
            });
            this.getCheckListArray.controls = [];
            this.getChecks(this.searchBody);
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let transactionStatus = 'unposted';
    let descriptionAr = '';
    let descriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let checksListArray = new FormArray([]);
    let isActive = true;

    if (this.returnCheck) {
      code = this.returnCheck.code;
      transactionStatus = this.returnCheck.transactionStatus;
      descriptionAr = this.returnCheck.descriptionAr;
      descriptionEn = this.returnCheck.descriptionEn;
      gregorianDate = new Date(this.returnCheck.gregorianDate + 'UTC');
      hijriDate = this.returnCheck.hijriDate;
      isActive = this.returnCheck.isActive;
      checksListArray = new FormArray([]);
      for (const control of this.returnCheck.checksList) {
        checksListArray.push(
          new FormGroup({
            checkNumber: new FormControl(control.checkNumber),
            issueGregorianDate: new FormControl(control.issueGregorianDate),
            issueHijriDate: new FormControl(control.issueHijriDate),
            maturityGregorianDate: new FormControl(control.maturityGregorianDate),
            maturityHijriDate: new FormControl(control.maturityHijriDate),
            checkBankId: new FormControl(control.checkBankId),
            checkAmount: new FormControl(control.checkAmount),
            currencyId: new FormControl(control.currencyId),
            currencyExchangeRate: new FormControl(control.currencyExchangeRate),
            customerId: new FormControl(control.customerId),
            depositBankId: new FormControl(control.depositBankId),
            safeBoxId: new FormControl(control.safeBoxId),
            checkReasonsRejectionId: new FormControl(control.checkReasonsRejectionId, Validators.required),
            checkStatus: new FormControl(control.checkStatus)
          })
        );
      }
    }
    this.returnCheckForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      transactionStatus: new FormControl(transactionStatus),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      checksList: checksListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
