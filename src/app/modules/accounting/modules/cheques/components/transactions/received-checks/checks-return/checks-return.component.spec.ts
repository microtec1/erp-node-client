import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecksReturnComponent } from './checks-return.component';

describe('ChecksReturnComponent', () => {
  let component: ChecksReturnComponent;
  let fixture: ComponentFixture<ChecksReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecksReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecksReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
