import { Component, OnInit, OnDestroy } from '@angular/core';
import { returnedChecksApi, baseUrl, deliveringChecksApi, postReturnedChecksApi, unPostReturnedChecksApi } from 'src/app/common/constants/api.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IReturnCheck } from '../../../../interfaces/IReturnCheck';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-checks-return',
  templateUrl: './checks-return.component.html',
  styleUrls: ['./checks-return.component.scss']
})
export class ChecksReturnComponent implements OnInit, OnDestroy {
  returnedChecks: IReturnCheck[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getReturnedChecksFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(deliveringChecksApi, pageNumber).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'return_check') {
          if (this.returnedChecks.length) {
            const uniqueReturnedChecks = this.returnedChecks.filter(x => x._id !== item._id);
            this.returnedChecks = uniqueReturnedChecks;
          }
          this.returnedChecks.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(deliveringChecksApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'return_check') {
          if (this.returnedChecks.length) {
            const uniqueReturnedChecks = this.returnedChecks.filter(x => x._id !== item._id);
            this.returnedChecks = uniqueReturnedChecks;
          }
          this.returnedChecks.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postReturnedChecksApi}/${id}`, {}).subscribe(res => {
        this.returnedChecks = this.generalService.changeStatus(this.returnedChecks, id, 'posted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostReturnedChecksApi}/${id}`, {}).subscribe(res => {
        this.returnedChecks = this.generalService.changeStatus(this.returnedChecks, id, 'unposted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getReturnedChecksFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(deliveringChecksApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            for (const item of res.results) {
              if (item.transactionName === 'return_check') {
                if (this.returnedChecks.length) {
                  const uniqueReturnedChecks = this.returnedChecks.filter(x => x._id !== item._id);
                  this.returnedChecks = uniqueReturnedChecks;
                }
                this.returnedChecks.push(item);
              }
            }
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      descriptionAr: new FormControl(''),
      descriptionEn: new FormControl('')
    });
  }

  deleteModal(returnedCheck: IReturnCheck) {
    const initialState = {
      code: returnedCheck.code,
      nameAr: returnedCheck.descriptionAr,
      nameEn: returnedCheck.descriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(returnedCheck._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(deliveringChecksApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.returnedChecks = this.generalService.removeItem(this.returnedChecks, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getReturnedChecksFirstPage() {
    this.subscriptions.push(
      this.data.get(returnedChecksApi, 1).subscribe((res: IDataRes) => {
        console.log(res);

        for (const item of res.results) {
          if (item.transactionName === 'return_check') {
            if (this.returnedChecks.length) {
              const uniqueReturnedChecks = this.returnedChecks.filter(x => x._id !== item._id);
              this.returnedChecks = uniqueReturnedChecks;
            }
            this.returnedChecks.push(item);
          }
        }
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
