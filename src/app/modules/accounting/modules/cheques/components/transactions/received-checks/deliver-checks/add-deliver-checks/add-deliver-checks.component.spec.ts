import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDeliverChecksComponent } from './add-deliver-checks.component';

describe('AddDeliverChecksComponent', () => {
  let component: AddDeliverChecksComponent;
  let fixture: ComponentFixture<AddDeliverChecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDeliverChecksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDeliverChecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
