import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IDeliverCheck } from '../../../../../interfaces/IDeliverCheck';
import { Subscription } from 'rxjs';
import { baseUrl, deliveringChecksApi, employeesApi, receivingChecksApi, customersApi, banksApi, branchCurrenciesApi, identifiedChecksApi, safeBoxesApi } from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICheck } from '../../../../../interfaces/ICheck';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';

@Component({
  selector: 'app-add-deliver-checks',
  templateUrl: './add-deliver-checks.component.html',
  styleUrls: ['./add-deliver-checks.component.scss']
})
export class AddDeliverChecksComponent implements OnInit, OnDestroy {
  deliverCheckForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  deliverCheck: IDeliverCheck;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  checks: ICheck[] = [];
  searchBody = {
    checkType: 'received_check',
    checkStatus: 'received_in_safebox'
  };

  // Employees
  employees: IEmployee[] = [];
  employeesInputFocused: boolean;
  hasMoreEmployees: boolean;
  employeesCount: number;
  selectedEmployeesPage = 1;
  employeesPagesNo: number;
  noEmployees: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Safeboxes
  safeboxes: IBox[] = [];
  safeboxesInputFocused: boolean;
  hasMoreSafeboxes: boolean;
  safeboxesCount: number;
  selectedSafeboxesPage = 1;
  safeboxesPagesNo: number;
  noSafeboxes: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(deliveringChecksApi, null, null, id).subscribe((deliverCheck: IDeliverCheck) => {
              this.deliverCheck = deliverCheck;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.deliverCheckForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.getChecks(this.searchBody);
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(employeesApi, 1).subscribe((res: IDataRes) => {
        this.employeesPagesNo = res.pages;
        this.employeesCount = res.count;
        if (this.employeesPagesNo > this.selectedEmployeesPage) {
          this.hasMoreEmployees = true;
        }
        this.employees.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeboxesPagesNo = res.pages;
        this.safeboxesCount = res.count;
        if (this.safeboxesPagesNo > this.selectedSafeboxesPage) {
          this.hasMoreSafeboxes = true;
        }
        this.safeboxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  filterChecks(formValue) {
    const searchBody = {
      ...this.searchBody,
      ...formValue
    };
    this.getChecks(searchBody);
  }

  getChecks(searchBody) {
    this.subscriptions.push(
      this.data
        .post(identifiedChecksApi, searchBody)
        .subscribe((res: IDataRes) => {
          console.log(res);

          const checkList: ICheck[] = res.results;
          if (checkList.length) {
            this.getCheckListArray.controls = [];
            for (const control of checkList) {
              this.getCheckListArray.push(
                new FormGroup({
                  checkNumber: new FormControl(control.checkNumber),
                  issueGregorianDate: new FormControl(control.issueGregorianDate),
                  issueHijriDate: new FormControl(control.issueHijriDate),
                  maturityGregorianDate: new FormControl(control.maturityGregorianDate),
                  maturityHijriDate: new FormControl(control.maturityHijriDate),
                  checkBankId: new FormControl(control.checkBankId),
                  checkAmount: new FormControl(control.checkAmount),
                  currencyId: new FormControl(control.currencyId),
                  currencyExchangeRate: new FormControl(control.currencyExchangeRate),
                  customerId: new FormControl(control.customerId),
                  checkStatus: new FormControl(control.checkStatus),
                  safeBoxId: new FormControl(control.safeBoxId),
                })
              );
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchEmployees(event) {
    const searchValue = event;
    const searchQuery = {
      employeeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(employeesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noEmployees = true;
            } else {
              this.noEmployees = false;
              for (const item of res.results) {
                if (this.employees.length) {
                  const uniqueEmployees = this.employees.filter(
                    x => x._id !== item._id
                  );
                  this.employees = uniqueEmployees;
                }
                this.employees.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreEmployees() {
    this.selectedEmployeesPage = this.selectedEmployeesPage + 1;
    this.subscriptions.push(
      this.data
        .get(employeesApi, this.selectedEmployeesPage)
        .subscribe((res: IDataRes) => {
          if (this.employeesPagesNo > this.selectedEmployeesPage) {
            this.hasMoreEmployees = true;
          } else {
            this.hasMoreEmployees = false;
          }
          for (const item of res.results) {
            if (this.employees.length) {
              const uniqueEmployees = this.employees.filter(x => x._id !== item._id);
              this.employees = uniqueEmployees;
            }
            this.employees.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.deliverCheckForm.controls;
  }

  get getCheckListArray() {
    return this.deliverCheckForm.get('checksList') as FormArray;
  }

  deleteList(index) {
    const control = this.deliverCheckForm.get('checksList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.deliverCheckForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.deliverCheckForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.deliverCheckForm.value.gregorianDate !== 'string') {
      this.deliverCheckForm.value.gregorianDate =
        this.generalService.format(this.deliverCheckForm.value.gregorianDate);
    }
    if (typeof this.deliverCheckForm.value.hijriDate !== 'string') {
      this.deliverCheckForm.value.hijriDate =
        this.generalService.formatHijriDate(this.deliverCheckForm.value.hijriDate);
    }

    if (this.deliverCheck) {
      if (this.deliverCheckForm.valid) {
        const newDeliverCheck = {
          _id: this.deliverCheck._id,
          ...this.generalService.checkEmptyFields(this.deliverCheckForm.value)
        };
        this.data.put(deliveringChecksApi, newDeliverCheck).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/cheques/deliverChecks']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {
      if (this.deliverCheckForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.deliverCheckForm.value),
          companyId
        };
        const formValueModified = {
          ...formValue
        };
        this.subscriptions.push(
          this.data.post(deliveringChecksApi, formValueModified).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.deliverCheckForm.reset();
            this.deliverCheckForm.patchValue({
              isActive: true
            });
            this.getCheckListArray.controls = [];
            this.getChecks(this.searchBody);
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let transactionStatus = 'unposted';
    let descriptionAr = '';
    let descriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let checkReceiverId = '';
    let checksListArray = new FormArray([]);
    let isActive = true;

    if (this.deliverCheck) {
      code = this.deliverCheck.code;
      transactionStatus = this.deliverCheck.transactionStatus;
      descriptionAr = this.deliverCheck.descriptionAr;
      descriptionEn = this.deliverCheck.descriptionEn;
      gregorianDate = new Date(this.deliverCheck.gregorianDate + 'UTC');
      hijriDate = this.deliverCheck.hijriDate;
      checkReceiverId = this.deliverCheck.checkReceiverId;
      isActive = this.deliverCheck.isActive;
      checksListArray = new FormArray([]);
      for (const control of this.deliverCheck.checksList) {
        checksListArray.push(
          new FormGroup({
            checkNumber: new FormControl(control.checkNumber),
            issueGregorianDate: new FormControl(control.issueGregorianDate),
            issueHijriDate: new FormControl(control.issueHijriDate),
            maturityGregorianDate: new FormControl(control.maturityGregorianDate),
            maturityHijriDate: new FormControl(control.maturityHijriDate),
            checkBankId: new FormControl(control.checkBankId),
            checkAmount: new FormControl(control.checkAmount),
            currencyId: new FormControl(control.currencyId),
            currencyExchangeRate: new FormControl(control.currencyExchangeRate),
            customerId: new FormControl(control.customerId),
            checkStatus: new FormControl(control.checkStatus),
            safeBoxId: new FormControl(control.safeBoxId)
          })
        );
      }
    }
    this.deliverCheckForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      transactionStatus: new FormControl(transactionStatus),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      checkReceiverId: new FormControl(checkReceiverId, Validators.required),
      checksList: checksListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
