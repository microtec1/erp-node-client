import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverChecksComponent } from './deliver-checks.component';

describe('DeliverChecksComponent', () => {
  let component: DeliverChecksComponent;
  let fixture: ComponentFixture<DeliverChecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverChecksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverChecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
