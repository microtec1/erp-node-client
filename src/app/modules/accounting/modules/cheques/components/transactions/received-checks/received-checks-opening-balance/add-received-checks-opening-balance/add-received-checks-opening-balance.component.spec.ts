import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReceivedChecksOpeningBalanceComponent } from './add-received-checks-opening-balance.component';

describe('AddReceivedChecksOpeningBalanceComponent', () => {
  let component: AddReceivedChecksOpeningBalanceComponent;
  let fixture: ComponentFixture<AddReceivedChecksOpeningBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReceivedChecksOpeningBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReceivedChecksOpeningBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
