import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IReceivedChecksOpeningBalance } from '../../../../../interfaces/IReceivedCheckOpeningBalance';
import { Subscription } from 'rxjs';
import { baseUrl, receivedChecksOpeningBalanceApi, branchCurrenciesApi, banksApi, safeBoxesApi, beneficiariesApi, customersApi, checkReasonsRejectionApi, employeesApi } from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IBeneficiary } from '../../../../../interfaces/IBeneficiary';
import { ICheckReasonRejection } from '../../../../../interfaces/ICheckReasonRejection';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';

@Component({
  selector: 'app-add-received-checks-opening-balance',
  templateUrl: './add-received-checks-opening-balance.component.html',
  styleUrls: ['./add-received-checks-opening-balance.component.scss']
})
export class AddReceivedChecksOpeningBalanceComponent implements OnInit, OnDestroy {
  receivingCheckForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  receivingCheck: IReceivedChecksOpeningBalance;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  banksInputFocused2: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;


  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Beneficiary
  beneficiaries: IBeneficiary[] = [];
  beneficiariesInputFocused: boolean;
  hasMoreBeneficiaries: boolean;
  beneficiariesCount: number;
  selectedBeneficiariesPage = 1;
  beneficiariesPagesNo: number;
  noBeneficiaries: boolean;

  // Rejection Reasons
  rejectionReasons: ICheckReasonRejection[] = [];
  rejectionReasonsInputFocused: boolean;
  hasMoreRejectionReasons: boolean;
  rejectionReasonsCount: number;
  selectedRejectionReasonsPage = 1;
  rejectionReasonsPagesNo: number;
  noRejectionReasons: boolean;

  // Employees
  employees: IEmployee[] = [];
  employeesInputFocused: boolean;
  hasMoreEmployees: boolean;
  employeesCount: number;
  selectedEmployeesPage = 1;
  employeesPagesNo: number;
  noEmployees: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(receivedChecksOpeningBalanceApi, null, null, id).subscribe((receivingCheck: IReceivedChecksOpeningBalance) => {
              this.receivingCheck = receivingCheck;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.receivingCheckForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(beneficiariesApi, 1).subscribe((res: IDataRes) => {
        this.beneficiariesPagesNo = res.pages;
        this.beneficiariesCount = res.count;
        if (this.beneficiariesPagesNo > this.selectedBeneficiariesPage) {
          this.hasMoreBeneficiaries = true;
        }
        this.beneficiaries.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(checkReasonsRejectionApi, 1).subscribe((res: IDataRes) => {
        this.rejectionReasonsPagesNo = res.pages;
        this.rejectionReasonsCount = res.count;
        if (this.rejectionReasonsPagesNo > this.selectedRejectionReasonsPage) {
          this.hasMoreRejectionReasons = true;
        }
        this.rejectionReasons.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(employeesApi, 1).subscribe((res: IDataRes) => {
        this.employeesPagesNo = res.pages;
        this.employeesCount = res.count;
        if (this.employeesPagesNo > this.selectedEmployeesPage) {
          this.hasMoreEmployees = true;
        }
        this.employees.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchBeneficiaries(event) {
    const searchValue = event;
    const searchQuery = {
      beneficiaryNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(beneficiariesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noBeneficiaries = true;
          } else {
            this.noBeneficiaries = false;
            for (const item of res.results) {
              if (this.beneficiaries.length) {
                const uniqueBeneficiaries = this.beneficiaries.filter(x => x._id !== item._id);
                this.beneficiaries = uniqueBeneficiaries;
              }
              this.beneficiaries.push(item);
            }
          }
          this.beneficiaries = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMorebeneficiaries() {
    this.selectedBeneficiariesPage = this.selectedBeneficiariesPage + 1;
    this.subscriptions.push(
      this.data.get(beneficiariesApi, this.selectedBeneficiariesPage).subscribe((res: IDataRes) => {
        if (this.beneficiariesPagesNo > this.selectedBeneficiariesPage) {
          this.hasMoreBeneficiaries = true;
        } else {
          this.hasMoreBeneficiaries = false;
        }
        for (const item of res.results) {
          if (this.beneficiaries.length) {
            const uniqueBeneficiaries = this.beneficiaries.filter(x => x._id !== item._id);
            this.beneficiaries = uniqueBeneficiaries;
          }
          this.beneficiaries.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchRejectionReasons(event) {
    const searchValue = event;
    const searchQuery = {
      checkReasonsRejectionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(checkReasonsRejectionApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noRejectionReasons = true;
            } else {
              this.noRejectionReasons = false;
              for (const item of res.results) {
                if (this.rejectionReasons.length) {
                  const uniqueRejectionReasons = this.rejectionReasons.filter(
                    x => x._id !== item._id
                  );
                  this.rejectionReasons = uniqueRejectionReasons;
                }
                this.rejectionReasons.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreRejectionReasons() {
    this.selectedRejectionReasonsPage = this.selectedRejectionReasonsPage + 1;
    this.subscriptions.push(
      this.data
        .get(checkReasonsRejectionApi, this.selectedRejectionReasonsPage)
        .subscribe((res: IDataRes) => {
          if (this.rejectionReasonsPagesNo > this.selectedRejectionReasonsPage) {
            this.hasMoreRejectionReasons = true;
          } else {
            this.hasMoreRejectionReasons = false;
          }
          for (const item of res.results) {
            if (this.rejectionReasons.length) {
              const uniquerejectionReasons = this.rejectionReasons.filter(x => x._id !== item._id);
              this.rejectionReasons = uniquerejectionReasons;
            }
            this.rejectionReasons.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchEmployees(event) {
    const searchValue = event;
    const searchQuery = {
      employeeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(employeesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noEmployees = true;
            } else {
              this.noEmployees = false;
              for (const item of res.results) {
                if (this.employees.length) {
                  const uniqueEmployees = this.employees.filter(
                    x => x._id !== item._id
                  );
                  this.employees = uniqueEmployees;
                }
                this.employees.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreEmployees() {
    this.selectedEmployeesPage = this.selectedEmployeesPage + 1;
    this.subscriptions.push(
      this.data
        .get(employeesApi, this.selectedEmployeesPage)
        .subscribe((res: IDataRes) => {
          if (this.employeesPagesNo > this.selectedEmployeesPage) {
            this.hasMoreEmployees = true;
          } else {
            this.hasMoreEmployees = false;
          }
          for (const item of res.results) {
            if (this.employees.length) {
              const uniqueEmployees = this.employees.filter(x => x._id !== item._id);
              this.employees = uniqueEmployees;
            }
            this.employees.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.receivingCheckForm.controls;
  }

  get getCheckListArray() {
    return this.receivingCheckForm.get('checksList') as FormArray;
  }

  addList() {
    const control = this.receivingCheckForm.get('checksList') as FormArray;
    control.push(
      new FormGroup({
        checkNumber: new FormControl('', Validators.required),
        issueGregorianDate: new FormControl(null, Validators.required),
        issueHijriDate: new FormControl(null, Validators.required),
        maturityGregorianDate: new FormControl(null, Validators.required),
        maturityHijriDate: new FormControl(null, Validators.required),
        checkBankId: new FormControl('', Validators.required),
        checkAmount: new FormControl(null, Validators.required),
        currencyId: new FormControl('', Validators.required),
        currencyExchangeRate: new FormControl(null, Validators.required),
        customerId: new FormControl('', Validators.required),
        checkStatus: new FormControl('without', Validators.required),
        checkBeneficiaryId: new FormControl('', Validators.required),
        depositBankId: new FormControl(''),
        checkReceiverId: new FormControl(''),
        checkReasonsRejectionId: new FormControl('')
      })
    );
  }

  deleteList(index) {
    const control = this.receivingCheckForm.get('checksList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.receivingCheckForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.receivingCheckForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  fillRate(event, formGroup: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        currencyExchangeRate: currency.exchangeRate
      });
    } else {
      this.receivingCheckForm.patchValue({
        currencyExchangeRate: currency.exchangeRate
      });
    }
  }

  onStatusChange(status, formGroup: FormGroup) {
    if (status === 'under_collection' || status === 'collected' || status === 'returned' || status === 'returned_to_customer') {
      formGroup.controls.depositBankId.setValidators(Validators.required);
      formGroup.controls.depositBankId.updateValueAndValidity();
    } else {
      formGroup.controls.depositBankId.clearValidators();
      formGroup.controls.depositBankId.updateValueAndValidity();
    }

    if (status !== 'received_in_safebox') {
      formGroup.controls.checkReceiverId.setValidators(Validators.required);
      formGroup.controls.checkReceiverId.updateValueAndValidity();
    } else {
      formGroup.controls.checkReceiverId.clearValidators();
      formGroup.controls.checkReceiverId.updateValueAndValidity();
    }

    if (status === 'returned') {
      formGroup.controls.checkReasonsRejectionId.setValidators(Validators.required);
      formGroup.controls.checkReasonsRejectionId.updateValueAndValidity();
    } else {
      formGroup.controls.checkReasonsRejectionId.clearValidators();
      formGroup.controls.checkReasonsRejectionId.updateValueAndValidity();
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (typeof this.receivingCheckForm.value.gregorianDate !== 'string') {
      this.receivingCheckForm.value.gregorianDate =
        this.generalService.format(this.receivingCheckForm.value.gregorianDate);
    }
    if (typeof this.receivingCheckForm.value.hijriDate !== 'string') {
      this.receivingCheckForm.value.hijriDate =
        this.generalService.formatHijriDate(this.receivingCheckForm.value.hijriDate);
    }
    for (
      let i = 0;
      i <= this.receivingCheckForm.value.checksList.length - 1;
      i++
    ) {
      if (this.receivingCheckForm.value.checksList[i].issueGregorianDate) {
        if (
          typeof this.receivingCheckForm.value.checksList[i].issueGregorianDate !==
          'string'
        ) {
          this.receivingCheckForm.value.checksList[i].issueGregorianDate = this.generalService.format(
            this.receivingCheckForm.value.checksList[i].issueGregorianDate
          );
        }
        if (typeof this.receivingCheckForm.value.checksList[i].issueHijriDate !==
          'string') {
          this.receivingCheckForm.value.checksList[i].issueHijriDate = this.generalService.formatHijriDate(
            this.receivingCheckForm.value.checksList[i].issueHijriDate
          );
        }
      }

      if (this.receivingCheckForm.value.checksList[i].maturityGregorianDate) {
        if (
          typeof this.receivingCheckForm.value.checksList[i].maturityGregorianDate !==
          'string'
        ) {
          this.receivingCheckForm.value.checksList[i].maturityGregorianDate = this.generalService.format(
            this.receivingCheckForm.value.checksList[i].maturityGregorianDate
          );
        }
        if (typeof this.receivingCheckForm.value.checksList[i].maturityHijriDate !==
          'string') {
          this.receivingCheckForm.value.checksList[i].maturityHijriDate = this.generalService.formatHijriDate(
            this.receivingCheckForm.value.checksList[i].maturityHijriDate
          );
        }
      }
    }
    if (this.receivingCheck) {
      if (this.receivingCheckForm.valid) {
        const newReceiveCheck = {
          _id: this.receivingCheck._id,
          ...this.generalService.checkEmptyFields(this.receivingCheckForm.value),
          hijriDate: this.generalService.formatHijriDate(this.receivingCheckForm.value.hijriDate)
        };
        this.data.put(receivedChecksOpeningBalanceApi, newReceiveCheck).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/cheques/receivedChecksOpeningBalance']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {
      if (this.receivingCheckForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.receivingCheckForm.value),
          companyId
        };
        const formValueModified = {
          ...formValue
        };
        this.subscriptions.push(
          this.data.post(receivedChecksOpeningBalanceApi, formValueModified).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.receivingCheckForm.reset();
            this.receivingCheckForm.patchValue({
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let transactionStatus = 'unposted';
    let descriptionAr = '';
    let descriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let safeBoxId = '';
    let checksListArray = new FormArray([
      new FormGroup({
        checkNumber: new FormControl('', Validators.required),
        issueGregorianDate: new FormControl(null, Validators.required),
        issueHijriDate: new FormControl(null, Validators.required),
        maturityGregorianDate: new FormControl(null, Validators.required),
        maturityHijriDate: new FormControl(null, Validators.required),
        checkBankId: new FormControl('', Validators.required),
        checkAmount: new FormControl(null, Validators.required),
        currencyId: new FormControl('', Validators.required),
        currencyExchangeRate: new FormControl(null, Validators.required),
        customerId: new FormControl('', Validators.required),
        checkStatus: new FormControl('without', Validators.required),
        checkBeneficiaryId: new FormControl('', Validators.required),
        depositBankId: new FormControl(''),
        checkReceiverId: new FormControl(''),
        checkReasonsRejectionId: new FormControl('')
      })
    ]);
    let isActive = true;

    if (this.receivingCheck) {
      code = this.receivingCheck.code;
      transactionStatus = this.receivingCheck.transactionStatus;
      descriptionAr = this.receivingCheck.descriptionAr;
      descriptionEn = this.receivingCheck.descriptionEn;
      gregorianDate = new Date(this.receivingCheck.gregorianDate + 'UTC');
      hijriDate = this.receivingCheck.hijriDate;
      safeBoxId = this.receivingCheck.safeBoxId;
      isActive = this.receivingCheck.isActive;
      checksListArray = new FormArray([]);
      for (const control of this.receivingCheck.checksList) {
        checksListArray.push(
          new FormGroup({
            checkNumber: new FormControl(control.checkNumber, Validators.required),
            issueGregorianDate: new FormControl(new Date(control.issueGregorianDate + 'UTC'), Validators.required),
            issueHijriDate: new FormControl(control.issueHijriDate, Validators.required),
            maturityGregorianDate: new FormControl(new Date(control.issueGregorianDate + 'UTC'), Validators.required),
            maturityHijriDate: new FormControl(control.maturityHijriDate, Validators.required),
            checkBankId: new FormControl(control.checkBankId, Validators.required),
            checkAmount: new FormControl(control.checkAmount, Validators.required),
            currencyId: new FormControl(control.currencyId, Validators.required),
            currencyExchangeRate: new FormControl(control.currencyExchangeRate, Validators.required),
            customerId: new FormControl(control.customerId, Validators.required),
            checkStatus: new FormControl(control.checkStatus, Validators.required),
            checkBeneficiaryId: new FormControl(control.checkBeneficiaryId, Validators.required),
            depositBankId: new FormControl(control.depositBankId),
            checkReceiverId: new FormControl(control.checkReceiverId),
            checkReasonsRejectionId: new FormControl(control.checkReasonsRejectionId)
          })
        );
      }
    }
    this.receivingCheckForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      transactionStatus: new FormControl(transactionStatus),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      safeBoxId: new FormControl(safeBoxId, Validators.required),
      checksList: checksListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
