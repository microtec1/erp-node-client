import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivedChecksOpeningBalanceComponent } from './received-checks-opening-balance.component';

describe('ReceivedChecksOpeningBalanceComponent', () => {
  let component: ReceivedChecksOpeningBalanceComponent;
  let fixture: ComponentFixture<ReceivedChecksOpeningBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivedChecksOpeningBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivedChecksOpeningBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
