import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReceivingChecksComponent } from './add-receiving-checks.component';

describe('AddReceivingChecksComponent', () => {
  let component: AddReceivingChecksComponent;
  let fixture: ComponentFixture<AddReceivingChecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReceivingChecksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReceivingChecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
