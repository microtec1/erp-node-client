import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivingChecksComponent } from './receiving-checks.component';

describe('ReceivingChecksComponent', () => {
  let component: ReceivingChecksComponent;
  let fixture: ComponentFixture<ReceivingChecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivingChecksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivingChecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
