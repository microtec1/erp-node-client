import { Component, OnInit, OnDestroy } from '@angular/core';
import { IReceiveCheck } from '../../../../interfaces/IReceiveCheck';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, receivingChecksApi, postReceivingChecksApi, unPostReceivingChecksApi, unPostMoneyTransfersApi } from 'src/app/common/constants/api.constants';
import { branchId, companyId } from 'src/app/common/constants/general.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-receiving-checks',
  templateUrl: './receiving-checks.component.html',
  styleUrls: ['./receiving-checks.component.scss']
})
export class ReceivingChecksComponent implements OnInit, OnDestroy {
  receivingChecks: IReceiveCheck[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getReceivingChecksFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(receivingChecksApi, pageNumber).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'receive_check') {
          if (this.receivingChecks.length) {
            const uniqueReceivingChecks = this.receivingChecks.filter(x => x._id !== item._id);
            this.receivingChecks = uniqueReceivingChecks;
          }
          this.receivingChecks.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(receivingChecksApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      for (const item of res.results) {
        if (item.transactionName === 'receive_check') {
          if (this.receivingChecks.length) {
            const uniqueReceivingChecks = this.receivingChecks.filter(x => x._id !== item._id);
            this.receivingChecks = uniqueReceivingChecks;
          }
          this.receivingChecks.push(item);
        }
      }
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postReceivingChecksApi}/${id}`, {}).subscribe(res => {
        this.receivingChecks = this.generalService.changeStatus(this.receivingChecks, id, 'posted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostReceivingChecksApi}/${id}`, {}).subscribe(res => {
        this.receivingChecks = this.generalService.changeStatus(this.receivingChecks, id, 'unposted');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getReceivingChecksFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(receivingChecksApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.receivingChecks = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      descriptionAr: new FormControl(''),
      descriptionEn: new FormControl('')
    });
  }

  deleteModal(receivingCheck: IReceiveCheck) {
    const initialState = {
      code: receivingCheck.code,
      nameAr: receivingCheck.descriptionAr,
      nameEn: receivingCheck.descriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(receivingCheck._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(receivingChecksApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.receivingChecks = this.generalService.removeItem(this.receivingChecks, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getReceivingChecksFirstPage() {
    this.subscriptions.push(
      this.data.get(receivingChecksApi, 1).subscribe((res: IDataRes) => {
        for (const item of res.results) {
          if (item.transactionName === 'receive_check') {
            if (this.receivingChecks.length) {
              const uniqueReceivingChecks = this.receivingChecks.filter(x => x._id !== item._id);
              this.receivingChecks = uniqueReceivingChecks;
            }
            this.receivingChecks.push(item);
          }
        }
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
