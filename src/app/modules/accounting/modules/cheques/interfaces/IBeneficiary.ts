export interface IBeneficiary {
  _id: string;
  code: string;
  beneficiaryNameAr: string;
  beneficiaryNameEn: string;
  companyId: string;
  isActive: boolean;
}
