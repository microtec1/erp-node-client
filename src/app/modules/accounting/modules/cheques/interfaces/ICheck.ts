export interface ICheck {
  _id: string;
  checkNumber: string;
  issueGregorianDate: string;
  issueHijriDate: string;
  maturityGregorianDate: string;
  maturityHijriDate: string;
  checkBankId: string;
  checkAmount: number;
  currencyId: string;
  currencyExchangeRate: number;
  customerId: string;
  supplierId: string;
  checkReceiverId: string;
  depositBankId: string;
  checkReasonsRejectionId: string;
  checkStatus: string;
  hasTransaction: boolean;
  safeBoxId: string;
}
