export interface ICheckReasonRejection {
  _id: string;
  code: string;
  checkReasonsRejectionNameAr: string;
  checkReasonsRejectionNameEn: string;
  companyId: string;
  isActive: boolean;
}
