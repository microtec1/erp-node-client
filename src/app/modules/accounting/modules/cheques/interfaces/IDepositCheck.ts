export interface IDepositCheck {
  _id: string;
  code: string;
  transactionStatus: string;
  descriptionAr: string;
  descriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  transactionName: string;
  depositBankId: string;
  checksList: [
    {
      checkNumber: string;
      issueGregorianDate: string;
      issueHijriDate: string;
      maturityGregorianDate: string;
      maturityHijriDate: string;
      checkBankId: string;
      checkAmount: number;
      currencyId: string;
      currencyExchangeRate: number;
      customerId: string;
      checkReceiverId: string;
      checkStatus: string;
      hasTransaction: boolean;
      safeBoxId: string;
      depositBankId: string;
    }
  ];
  isActive: boolean;
}
