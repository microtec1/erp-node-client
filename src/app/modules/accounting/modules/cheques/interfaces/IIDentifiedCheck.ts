export interface IIdentifiedCheck {
    _id: string;
    checkId: string;
    companyId: string;
    checkNumber: string;
    issueGregorianDate: string;
    issueHijriDate: string;
    maturityGregorianDate: string;
    maturityHijriDate: string;
    checkBankId: string;
    checkAmount: string;
    currencyExchangeRate: string;
    currencyId: string;
    customerId: string;
    supplierId: string;
    checkBankDepositCheckId: string;
    transactionName: string;
    checkStatus: string;
    checkReceiverId: string;
    checkBeneficiaryId: string;
    checkReasonsRejectionId: string;
    depositBankId: string;
    checkType: string;
    branchId: string;
    safeBoxId: string;
    hasTransaction: boolean;
}
