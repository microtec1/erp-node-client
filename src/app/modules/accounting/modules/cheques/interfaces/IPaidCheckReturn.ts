export interface IPaidCheckReturn {
    _id: string;
    image: string;
    code: string;
    gregorianDate: string;
    hijriDate: string;
    transactionStatus: string;
    descriptionAr: string;
    descriptionEn: string;
    checksList: [{
        checkNumber: number;
        issueGregorianDate: string;
        issueHijriDate: string;
        maturityGregorianDate: string;
        maturityHijriDate: string;
        checkBankId: string;
        checkAmount: number;
        currencyId: string;
        currencyExchangeRate: number;
        supplierId: string;
        checkStatus: string;
        hasTransaction: boolean;
        transactionName: string;
        checkReasonsRejectionId: string;
        checkId: string;
    }];
    transactionName: string;
}
