export interface IPayCheck {
    _id: string;
    image: string;
    code: string;
    firstCheck: boolean;
    gregorianDate: string;
    hijriDate: string;
    transactionStatus: string;
    descriptionAr: string;
    descriptionEn: string;
    isActive: boolean;
    checksList: [{
        checkNumber: number;
        issueGregorianDate: string;
        issueHijriDate: string;
        maturityGregorianDate: string;
        maturityHijriDate: string;
        checkBankId: string;
        checkAmount: number;
        currencyId: string;
        currencyExchangeRate: number;
        supplierId: string;
        checkStatus: string;
        checkId: string;
        transactionName: string;
        hasTransaction: boolean;
    }];
    transactionName: string;

}
