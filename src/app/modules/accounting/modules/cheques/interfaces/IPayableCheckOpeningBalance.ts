export interface IPayableCheckOpeningBalance {
  _id: string;
  image: string;
  code: string;
  firstCheck: boolean;
  transactionStatus: string;
  descriptionAr: string;
  descriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  safeBoxId: string;
  transactionName: string;
  isActive: boolean;
  checksList: [{
    checkNumber: number;
    issueGregorianDate: string;
    issueHijriDate: string;
    maturityGregorianDate: string;
    maturityHijriDate: string;
    checkBankId: string;
    checkAmount: number;
    currencyId: string;
    currencyExchangeRate: number;
    supplierId: string;
    checkStatus: string;
    hasTransaction: boolean;
    checkReceiverId: string;
    checkBeneficiaryId: string;
    checkReasonsRejectionId: string;
    transactionName: string;
    checkId: string;
  }];
}
