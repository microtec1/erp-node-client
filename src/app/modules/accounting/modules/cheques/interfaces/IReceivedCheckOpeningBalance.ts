export interface IReceivedChecksOpeningBalance {
    _id: string;
    companyId: string;
    branchId: string;
    image: string;
    code: string;
    gregorianDate: string;
    hijriDate: string;
    transactionStatus: string;
    descriptionAr: string;
    descriptionEn: string;
    safeBoxId: string;
    checksList: [{
        checkNumber: string;
        issueGregorianDate: string;
        issueHijriDate: string;
        maturityGregorianDate: string;
        maturityHijriDate: string;
        checkBankId: string;
        checkAmount: number;
        currencyId: string;
        currencyExchangeRate: number;
        customerId: string;
        checkStatus: string;
        hasTransaction: boolean;
        depositBankId: string;
        checkReceiverId: string;
        checkBeneficiaryId: string;
        checkReasonsRejectionId: string;
    }];
    transactionName: string;
    isActive: boolean;
}
