export interface IReturnCheckToCustomer {
  _id: string;
  code: string;
  transactionStatus: string;
  descriptionAr: string;
  descriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  transactionName: string;
  checksList: [
    {
      checkNumber: string;
      issueGregorianDate: string;
      issueHijriDate: string;
      maturityGregorianDate: string;
      maturityHijriDate: string;
      checkBankId: string;
      checkAmount: number;
      currencyId: string;
      currencyExchangeRate: number;
      customerId: string;
      safeBoxId: string;
      checkStatus: string;
      transactionName: string;
      checkId: string;
      hasTransaction: string;
    }
  ];
  isActive: boolean;
}
