export interface IWritingPayCheck {
    _id: string;
    image: string;
    code: string;
    transactionStatus: string;
    descriptionAr: string;
    descriptionEn: string;
    gregorianDate: string;
    hijriDate: string;
    safeBoxId: string;
    isActive: boolean;
    checksList: [{
        checkNumber: number;
        issueGregorianDate: string;
        issueHijriDate: string;
        maturityGregorianDate: string;
        maturityHijriDate: string;
        hasTransaction: boolean;
        checkBankId: string;
        checkAmount: number;
        currencyId: string;
        currencyExchangeRate: number;
        supplierId: string;
        checkStatus: string;
        transactionName: string;
    }];
    transactionName: string;
}
