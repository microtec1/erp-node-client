import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IEstimatedAccountsBudget } from '../../../interfaces/IEstimatedAccountsBudget.model';
import { Subscription } from 'rxjs';
import { baseUrl, estimatedAccountsBudgetApi, fiscalYearsApi, detailedChartOfAccountsApi, estimatedAccountsBudgetByLevelApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { companyId, branchId } from 'src/app/common/constants/general.constants';
import { CustomValidators } from 'ng2-validation';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IFinancialYear } from 'src/app/modules/general/interfaces/IFinancialYear';
import { IChartOfAccount } from '../../../../gl/interfaces/IChartOfAccount';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-budget',
  templateUrl: './add-budget.component.html',
  styleUrls: ['./add-budget.component.scss']
})
export class AddBudgetComponent implements OnInit, OnDestroy {
  estimatedAccountsBudgetForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  estimatedAccountsBudget: IEstimatedAccountsBudget;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  accountPeriods: any[] = [];
  selectedAccounts: any[] = [];
  baseUrl = baseUrl;
  selectedAccount = 0;
  addMode: boolean;
  detailsMode: boolean;

  // Financial Years
  financialYears: IFinancialYear[] = [];
  financialYearInputFocused: boolean;
  financialYearsCount: number;
  noFinancialYears: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  constructor(
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(estimatedAccountsBudgetApi, null, null, id)
              .subscribe((estimatedAccountsBudget: IEstimatedAccountsBudget) => {
                this.estimatedAccountsBudget = estimatedAccountsBudget;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.estimatedAccountsBudgetForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    const search = {
      companyId,
      fiscalYearStatus: 'opened'
    };
    this.subscriptions.push(
      this.data.get(fiscalYearsApi, null, search).subscribe((res: IDataRes) => {
        if (res.results.length) {
          this.financialYears = res.results;
          this.financialYearsCount = res.results.length;
          if (this.estimatedAccountsBudget) {
            this.accountPeriods =
              this.financialYears.find(item => item._id === this.estimatedAccountsBudget.fiscalYearId).accountingPeriods;
          }
        } else {
          this.noFinancialYears = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
          if (this.estimatedAccountsBudget) {
            this.selectedAccounts = [];
            for (const account of this.estimatedAccountsBudget.accountDetails) {
              const foundAccount = this.chartOfAccounts.find(item => item._id === account.accountId);
              this.selectedAccounts.push(foundAccount);
            }
          }

        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  get form() {
    return this.estimatedAccountsBudgetForm.controls;
  }

  get getAccountDetailsArray() {
    return this.estimatedAccountsBudgetForm.get('accountDetails') as FormArray;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setSelectedAccount(index) {
    this.selectedAccount = index;
  }

  get selectedAccountIndex() {
    return this.selectedAccount;
  }

  get selectedAccountPeriods() {
    const array = this.estimatedAccountsBudgetForm.get('accountDetails') as FormArray;
    const group = array.controls[this.selectedAccount].get('accountingPeriodsDetails') as FormArray;
    return group;
  }

  fillAccounts(event) {
    const selected = this.chartOfAccounts.find(item => item._id === event);
    const accountDetailsArray = this.estimatedAccountsBudgetForm.get('accountDetails') as FormArray;
    this.selectedAccounts.push(
      selected
    );

    if (this.selectedAccounts.length >= 1) {
      const accountingPeriodsDetailsArray = new FormArray([]);
      for (const period of this.accountPeriods) {
        accountingPeriodsDetailsArray.push(
          new FormGroup({
            periodCode: new FormControl(period.periodCode),
            debit: new FormControl(null),
            credit: new FormControl(null),
          })
        );
      }
      accountDetailsArray.push(
        new FormGroup({
          accountId: new FormControl(selected._id),
          debit: new FormControl(null),
          credit: new FormControl(null),
          accountingPeriodsDetails: accountingPeriodsDetailsArray
        })
      );
    }
  }

  fillPeriods(event) {
    const year = this.financialYears.find(item => item._id === event);
    this.accountPeriods = year.accountingPeriods;
  }

  autoDistribution(array: FormArray) {
    const group = array.controls[this.selectedAccount] as FormGroup;
    const accountingPeriodsDetailsArray = group.get('accountingPeriodsDetails') as FormArray;
    if (group.controls.credit.value) {
      for (let i = 0; i <= accountingPeriodsDetailsArray.controls.length - 1; i++) {
        const accountingPeriodsDetailsGroup = accountingPeriodsDetailsArray.controls[i] as FormGroup;
        accountingPeriodsDetailsGroup.patchValue({
          credit: group.controls.credit.value / this.accountPeriods.length,
          debit: null
        });
      }
    } else if (group.controls.debit.value) {
      for (let i = 0; i <= accountingPeriodsDetailsArray.controls.length - 1; i++) {
        const accountingPeriodsDetailsGroup = accountingPeriodsDetailsArray.controls[i] as FormGroup;
        accountingPeriodsDetailsGroup.patchValue({
          debit: group.controls.debit.value / this.accountPeriods.length,
          credit: null
        });
      }
    }
  }

  reset(event) {
    this.selectedAccount = 0;
    const removedItem = this.selectedAccounts.find(item => item._id === event);
    const index = this.selectedAccounts.indexOf(removedItem);
    this.selectedAccounts.splice(index, 1);
    this.getAccountDetailsArray.removeAt(index);
  }

  clearValue(array: FormArray, index, fieldName, value) {
    const group = array.controls[index] as FormGroup;
    const adjustmentRatioValue = this.estimatedAccountsBudgetForm.value.adjustmentRatio;
    if (fieldName === 'debit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        credit: null,
        // debit: adjustmentRatioValue ? group.controls.debit.value * adjustmentRatioValue : group.controls.debit.value
      });
    }
    if (fieldName === 'credit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        debit: null,
        // credit: adjustmentRatioValue ? value * adjustmentRatioValue : value
      });
    }
  }

  getEstimatedBudget(value) {
    if (value !== '0') {
      this.addMode = true;
      const body = {
        level: value
      };
      this.subscriptions.push(
        this.data.post(estimatedAccountsBudgetByLevelApi, body).subscribe((res: IEstimatedAccountsBudget) => {
          this.estimatedAccountsBudget = res;
          if (res.accountDetails.length) {
            this.selectedAccounts = [];
            for (const account of this.estimatedAccountsBudget.accountDetails) {
              const foundAccount = this.chartOfAccounts.find(item => item._id === account.accountId);
              this.selectedAccounts.push(foundAccount);
            }
            this.initForm();
            this.estimatedAccountsBudgetForm.patchValue({
              level: '1'
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
  }

  setAdjustmentRatio(ratioValue) {
    if (ratioValue > 0) {
      for (let i = 0; i <= this.getAccountDetailsArray.controls.length - 1; i++) {
        const group = this.getAccountDetailsArray.controls[i] as FormGroup;
        if (group.controls.credit.value) {
          group.patchValue({
            credit: group.controls.credit.value * ratioValue
          });
        } else if (group.controls.debit.value) {
          group.patchValue({
            debit: group.controls.debit.value * ratioValue
          });
        }
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.estimatedAccountsBudget && !this.addMode) {
      if (this.estimatedAccountsBudgetForm.valid) {
        const newEstimatedAccountsBudget = {
          _id: this.estimatedAccountsBudget._id, ...this.generalService
            .checkEmptyFields(this.estimatedAccountsBudgetForm.value)
        };
        delete newEstimatedAccountsBudget.level;
        delete newEstimatedAccountsBudget.multiSelect;
        this.subscriptions.push(
          this.data.put(estimatedAccountsBudgetApi, newEstimatedAccountsBudget).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/financialAnalysis/estimatedAccountsBudget']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.estimatedAccountsBudgetForm.valid) {

        const formValue = {
          ...this.generalService.checkEmptyFields(this.estimatedAccountsBudgetForm.value),
          companyId,
          branchId
        };
        delete formValue.level;
        delete formValue.multiSelect;
        this.subscriptions.push(
          this.data.post(estimatedAccountsBudgetApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.estimatedAccountsBudgetForm.reset();
            this.filesAdded = false;
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let fiscalYearId = '';
    let multiSelect = null;
    const level = 0;
    let adjustmentRatio = null;
    let descriptionAr = '';
    let descriptionEn = '';
    const accountId = '';
    const accountDebit = null;
    const accountCredit = null;
    const periodCode = null;
    const periodDebit = null;
    const periodCredit = null;
    let accountingPeriodsDetailsArray = new FormArray([]);
    let accountDetailsArray = new FormArray([]);

    if (this.estimatedAccountsBudget) {
      fiscalYearId = this.estimatedAccountsBudget.fiscalYearId;
      adjustmentRatio = this.estimatedAccountsBudget.adjustmentRatio;
      descriptionAr = this.estimatedAccountsBudget.descriptionAr;
      descriptionEn = this.estimatedAccountsBudget.descriptionEn;
      multiSelect = [];
      for (const control of this.estimatedAccountsBudget.accountDetails) {
        multiSelect.push(control.accountId);
      }
      accountDetailsArray = new FormArray([]);
      for (const control of this.estimatedAccountsBudget.accountDetails) {
        accountingPeriodsDetailsArray = new FormArray([]);
        for (const item of control.accountingPeriodsDetails) {
          accountingPeriodsDetailsArray.push(
            new FormGroup({
              periodCode: new FormControl(item.periodCode),
              debit: new FormControl(item.debit),
              credit: new FormControl(item.credit),
            })
          );
        }
        accountDetailsArray.push(
          new FormGroup({
            accountId: new FormControl(control.accountId),
            debit: new FormControl(control.debit),
            credit: new FormControl(control.credit),
            accountingPeriodsDetails: accountingPeriodsDetailsArray
          })
        );
      }
    }

    this.estimatedAccountsBudgetForm = new FormGroup({
      fiscalYearId: new FormControl(fiscalYearId, Validators.required),
      multiSelect: new FormControl(multiSelect),
      level: new FormControl(level, Validators.required),
      adjustmentRatio: new FormControl(adjustmentRatio, CustomValidators.number),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      accountDetails: accountDetailsArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
