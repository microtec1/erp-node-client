import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IEstimatedAccountsBudget } from '../../interfaces/IEstimatedAccountsBudget.model';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup } from '@angular/forms';
import { baseUrl, estimatedAccountsBudgetApi } from 'src/app/common/constants/api.constants';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-estimated-accounts-budget',
  templateUrl: './estimated-accounts-budget.component.html',
  styleUrls: ['./estimated-accounts-budget.component.scss']
})
export class EstimatedAccountsBudgetComponent implements OnInit, OnDestroy {
  estimatedAccountsBudgets: IEstimatedAccountsBudget[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
  ) { }

  ngOnInit() {
    this.getEstimatedAccountsBudgetsFirstPage();
    // this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(estimatedAccountsBudgetApi, pageNumber).subscribe((res: IDataRes) => {
      this.estimatedAccountsBudgets = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(estimatedAccountsBudgetApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.estimatedAccountsBudgets = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getEstimatedAccountsBudgetsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(estimatedAccountsBudgetApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.estimatedAccountsBudgets = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  // openSearch() {
  //   this.showSearch = !this.showSearch;
  // }

  clear() {
    this.searchForm.reset();
  }

  // private initSearchForm() {
  //   this.searchForm = new FormGroup({
  //     estimatedAccountsBudgetCode: new FormControl(''),
  //     estimatedAccountsBudgetNameAr: new FormControl(''),
  //     estimatedAccountsBudgetNameEn: new FormControl('')
  //   });
  // }

  deleteModal(estimatedAccountsBudget: IEstimatedAccountsBudget) {
    const initialState = {
      level: estimatedAccountsBudget.level,
      fiscalYear: estimatedAccountsBudget.fiscalYearId,
      adjustmentRatio: estimatedAccountsBudget.adjustmentRatio
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(estimatedAccountsBudget._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(estimatedAccountsBudgetApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.estimatedAccountsBudgets = this.generalService.removeItem(this.estimatedAccountsBudgets, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getEstimatedAccountsBudgetsFirstPage() {
    this.subscriptions.push(
      this.data.get(estimatedAccountsBudgetApi, 1).subscribe((res: IDataRes) => {
        this.estimatedAccountsBudgets = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
