import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { baseUrl, estimatedCostCenterBudgetApi, fiscalYearsApi, detailedCostCentersApi, estimatedCostCenterBudgetByLevelApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { companyId, branchId } from 'src/app/common/constants/general.constants';
import { CustomValidators } from 'ng2-validation';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IFinancialYear } from 'src/app/modules/general/interfaces/IFinancialYear';
import { IEstimatedCostCentersBudget } from '../../../interfaces/IEstimatedCostCentersBudget.model';
import { ICostCenter } from '../../../../gl/interfaces/ICostCenter';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-cost-centers-budget',
  templateUrl: './add-cost-centers-budget.component.html',
  styleUrls: ['./add-cost-centers-budget.component.scss']
})
export class AddCostCentersBudgetComponent implements OnInit, OnDestroy {
  estimatedCostCentersBudgetForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  estimatedCostCentersBudget: IEstimatedCostCentersBudget;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  accountPeriods: any[] = [];
  selectedAccounts: any[] = [];
  baseUrl = baseUrl;
  selectedAccount = 0;
  addMode: boolean;
  detailsMode: boolean;

  // Financial Years
  financialYears: IFinancialYear[] = [];
  financialYearInputFocused: boolean;
  financialYearsCount: number;
  noFinancialYears: boolean;

  // Cost Centers
  costCenters: ICostCenter[] = [];
  costCentersInputFocused: boolean;
  costCentersCount: number;
  noCostCenters: boolean;

  constructor(
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
    private data: DataService,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(estimatedCostCenterBudgetApi, null, null, id)
              .subscribe((estimatedCostCentersBudget: IEstimatedCostCentersBudget) => {
                this.estimatedCostCentersBudget = estimatedCostCentersBudget;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.estimatedCostCentersBudgetForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    const search = {
      companyId,
      fiscalYearStatus: 'opened'
    };
    this.subscriptions.push(
      this.data.get(fiscalYearsApi, null, search).subscribe((res: IDataRes) => {
        if (res.results.length) {
          this.financialYears = res.results;
          this.financialYearsCount = res.results.length;
          if (this.estimatedCostCentersBudget) {
            this.accountPeriods =
              this.financialYears.find(item => item._id === this.estimatedCostCentersBudget.fiscalYearId).accountingPeriods;
          }
        } else {
          this.noFinancialYears = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedCostCentersApi, null, null, companyId).subscribe((res: ICostCenter[]) => {
        if (res.length) {
          this.costCenters.push(...res);
          this.costCentersCount = res.length;
          if (this.estimatedCostCentersBudget) {
            this.selectedAccounts = [];
            for (const account of this.estimatedCostCentersBudget.costCenterDetails) {
              const foundAccount = this.costCenters.find(item => item._id === account.costCenterId);
              this.selectedAccounts.push(foundAccount);
            }
          }

        } else {
          this.noCostCenters = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  get form() {
    return this.estimatedCostCentersBudgetForm.controls;
  }

  get getAccountDetailsArray() {
    return this.estimatedCostCentersBudgetForm.get('costCenterDetails') as FormArray;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setSelectedAccount(index) {
    this.selectedAccount = index;
  }

  get selectedAccountIndex() {
    return this.selectedAccount;
  }

  get selectedAccountPeriods() {
    const array = this.estimatedCostCentersBudgetForm.get('costCenterDetails') as FormArray;
    const group = array.controls[this.selectedAccount].get('accountingPeriodsDetails') as FormArray;
    return group;
  }

  fillAccounts(event) {
    const selected = this.costCenters.find(item => item._id === event);
    const accountDetailsArray = this.estimatedCostCentersBudgetForm.get('costCenterDetails') as FormArray;
    this.selectedAccounts.push(
      selected
    );

    if (this.selectedAccounts.length >= 1) {
      const accountingPeriodsDetailsArray = new FormArray([]);
      for (const period of this.accountPeriods) {
        accountingPeriodsDetailsArray.push(
          new FormGroup({
            periodCode: new FormControl(period.periodCode),
            debit: new FormControl(null),
            credit: new FormControl(null),
          })
        );
      }
      accountDetailsArray.push(
        new FormGroup({
          costCenterId: new FormControl(selected._id),
          debit: new FormControl(null),
          credit: new FormControl(null),
          accountingPeriodsDetails: accountingPeriodsDetailsArray
        })
      );
    }
  }

  fillPeriods(event) {
    const year = this.financialYears.find(item => item._id === event);
    this.accountPeriods = year.accountingPeriods;
  }

  autoDistribution(array: FormArray) {
    const group = array.controls[this.selectedAccount] as FormGroup;
    const accountingPeriodsDetailsArray = group.get('accountingPeriodsDetails') as FormArray;
    if (group.controls.credit.value) {
      for (let i = 0; i <= accountingPeriodsDetailsArray.controls.length - 1; i++) {
        const accountingPeriodsDetailsGroup = accountingPeriodsDetailsArray.controls[i] as FormGroup;
        accountingPeriodsDetailsGroup.patchValue({
          credit: group.controls.credit.value / this.accountPeriods.length,
          debit: null
        });
      }
    } else if (group.controls.debit.value) {
      for (let i = 0; i <= accountingPeriodsDetailsArray.controls.length - 1; i++) {
        const accountingPeriodsDetailsGroup = accountingPeriodsDetailsArray.controls[i] as FormGroup;
        accountingPeriodsDetailsGroup.patchValue({
          debit: group.controls.debit.value / this.accountPeriods.length,
          credit: null
        });
      }
    }
  }

  reset(event) {
    this.selectedAccount = 0;
    const removedItem = this.selectedAccounts.find(item => item._id === event);
    const index = this.selectedAccounts.indexOf(removedItem);
    this.selectedAccounts.splice(index, 1);
    this.getAccountDetailsArray.removeAt(index);
  }

  clearValue(array: FormArray, index, fieldName, value) {
    const group = array.controls[index] as FormGroup;
    const adjustmentRatioValue = this.estimatedCostCentersBudgetForm.value.adjustmentRatio;
    if (fieldName === 'debit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        credit: 0,
        // debit: adjustmentRatioValue ? group.controls.debit.value * adjustmentRatioValue : group.controls.debit.value
      });
    }
    if (fieldName === 'credit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        debit: 0,
        // credit: adjustmentRatioValue ? value * adjustmentRatioValue : value
      });
    }
  }

  getEstimatedBudget(value) {
    if (value !== '0') {
      this.addMode = true;
      const body = {
        level: value
      };
      this.subscriptions.push(
        this.data.post(estimatedCostCenterBudgetByLevelApi, body).subscribe((res: IEstimatedCostCentersBudget) => {
          this.estimatedCostCentersBudget = res;
          if (res.costCenterDetails.length) {
            this.selectedAccounts = [];
            for (const account of this.estimatedCostCentersBudget.costCenterDetails) {
              const foundAccount = this.costCenters.find(item => item._id === account.costCenterId);
              this.selectedAccounts.push(foundAccount);
            }
            this.initForm();
            this.estimatedCostCentersBudgetForm.patchValue({
              level: '1'
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
  }

  setAdjustmentRatio(ratioValue) {
    if (ratioValue > 0) {
      for (let i = 0; i <= this.getAccountDetailsArray.controls.length - 1; i++) {
        const group = this.getAccountDetailsArray.controls[i] as FormGroup;
        if (group.controls.credit.value) {
          group.patchValue({
            credit: group.controls.credit.value * ratioValue
          });
        } else if (group.controls.debit.value) {
          group.patchValue({
            debit: group.controls.debit.value * ratioValue
          });
        }
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.estimatedCostCentersBudget && !this.addMode) {
      if (this.estimatedCostCentersBudgetForm.valid) {
        const newEstimatedCostCentersBudget = {
          _id: this.estimatedCostCentersBudget._id, ...this.generalService
            .checkEmptyFields(this.estimatedCostCentersBudgetForm.value)
        };
        delete newEstimatedCostCentersBudget.level;
        delete newEstimatedCostCentersBudget.multiSelect;
        this.subscriptions.push(
          this.data.put(estimatedCostCenterBudgetApi, newEstimatedCostCentersBudget).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/financialAnalysis/estimatedCostCentersBudget']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.estimatedCostCentersBudgetForm.valid) {

        const formValue = {
          ...this.generalService.checkEmptyFields(this.estimatedCostCentersBudgetForm.value),
          companyId,
          branchId
        };
        delete formValue.level;
        delete formValue.multiSelect;
        this.subscriptions.push(
          this.data.post(estimatedCostCenterBudgetApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.estimatedCostCentersBudgetForm.reset();
            this.filesAdded = false;
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let fiscalYearId = '';
    let multiSelect = null;
    const level = 0;
    let adjustmentRatio = null;
    let descriptionAr = '';
    let descriptionEn = '';
    const accountId = '';
    const accountDebit = null;
    const accountCredit = null;
    const periodCode = null;
    const periodDebit = null;
    const periodCredit = null;
    let accountingPeriodsDetailsArray = new FormArray([]);
    let accountDetailsArray = new FormArray([]);

    if (this.estimatedCostCentersBudget) {
      fiscalYearId = this.estimatedCostCentersBudget.fiscalYearId;
      adjustmentRatio = this.estimatedCostCentersBudget.adjustmentRatio;
      descriptionAr = this.estimatedCostCentersBudget.descriptionAr;
      descriptionEn = this.estimatedCostCentersBudget.descriptionEn;
      multiSelect = [];
      for (const control of this.estimatedCostCentersBudget.costCenterDetails) {
        multiSelect.push(control.costCenterId);
      }
      accountDetailsArray = new FormArray([]);
      for (const control of this.estimatedCostCentersBudget.costCenterDetails) {
        accountingPeriodsDetailsArray = new FormArray([]);
        for (const item of control.accountingPeriodsDetails) {
          accountingPeriodsDetailsArray.push(
            new FormGroup({
              periodCode: new FormControl(item.periodCode),
              debit: new FormControl(item.debit),
              credit: new FormControl(item.credit),
            })
          );
        }
        accountDetailsArray.push(
          new FormGroup({
            accountId: new FormControl(control.costCenterId),
            debit: new FormControl(control.debit),
            credit: new FormControl(control.credit),
            accountingPeriodsDetails: accountingPeriodsDetailsArray
          })
        );
      }
    }

    this.estimatedCostCentersBudgetForm = new FormGroup({
      fiscalYearId: new FormControl(fiscalYearId, Validators.required),
      multiSelect: new FormControl(multiSelect),
      level: new FormControl(level, Validators.required),
      adjustmentRatio: new FormControl(adjustmentRatio, CustomValidators.number),
      descriptionAr: new FormControl(descriptionAr),
      descriptionEn: new FormControl(descriptionEn),
      costCenterDetails: accountDetailsArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
