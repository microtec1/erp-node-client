import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup } from '@angular/forms';
import { baseUrl, estimatedCostCenterBudgetApi, estimatedAccountsBudgetByLevelApi } from 'src/app/common/constants/api.constants';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IEstimatedCostCentersBudget } from '../../interfaces/IEstimatedCostCentersBudget.model';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-estimated-cost-centers-budget',
  templateUrl: './estimated-cost-centers-budget.component.html',
  styleUrls: ['./estimated-cost-centers-budget.component.scss']
})
export class EstimatedCostCentersBudgetComponent implements OnInit, OnDestroy {
  estimatedCostCentersBudgets: IEstimatedCostCentersBudget[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  pageCount: number[] = [];
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getEstimatedCostCentersBudgetsFirstPage();
    // this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(estimatedCostCenterBudgetApi, pageNumber).subscribe((res: IDataRes) => {
      this.estimatedCostCentersBudgets = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(estimatedCostCenterBudgetApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.estimatedCostCentersBudgets = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getEstimatedCostCentersBudgetsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(estimatedCostCenterBudgetApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.estimatedCostCentersBudgets = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  // openSearch() {
  //   this.showSearch = !this.showSearch;
  // }

  clear() {
    this.searchForm.reset();
  }

  // private initSearchForm() {
  //   this.searchForm = new FormGroup({
  //     estimatedCostCentersBudgetCode: new FormControl(''),
  //     estimatedCostCentersBudgetNameAr: new FormControl(''),
  //     estimatedCostCentersBudgetNameEn: new FormControl('')
  //   });
  // }

  deleteModal(estimatedCostCentersBudget: IEstimatedCostCentersBudget) {
    const initialState = {
      level: estimatedCostCentersBudget.level,
      fiscalYear: estimatedCostCentersBudget.fiscalYearId,
      adjustmentRatio: estimatedCostCentersBudget.adjustmentRatio
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(estimatedCostCentersBudget._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(estimatedCostCenterBudgetApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.estimatedCostCentersBudgets = this.generalService.removeItem(this.estimatedCostCentersBudgets, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getEstimatedCostCentersBudgetsFirstPage() {
    this.subscriptions.push(
      this.data.get(estimatedCostCenterBudgetApi, 1).subscribe((res: IDataRes) => {
        this.estimatedCostCentersBudgets = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
