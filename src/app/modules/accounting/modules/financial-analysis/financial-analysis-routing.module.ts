import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstimatedAccountsBudgetComponent } from './components/estimated-accounts-budget/estimated-accounts-budget.component';
import { AddBudgetComponent } from './components/estimated-accounts-budget/add-budget/add-budget.component';
import { EstimatedCostCentersBudgetComponent } from './components/estimated-cost-centers-budget/estimated-cost-centers-budget.component';
// tslint:disable-next-line:max-line-length
import { AddCostCentersBudgetComponent } from './components/estimated-cost-centers-budget/add-cost-centers-budget/add-cost-centers-budget.component';


const routes: Routes = [
  { path: '', redirectTo: 'estimatedAccountsBudget', pathMatch: 'full' },
  { path: 'estimatedAccountsBudget', component: EstimatedAccountsBudgetComponent },
  { path: 'estimatedAccountsBudget/add', component: AddBudgetComponent },
  { path: 'estimatedAccountsBudget/edit/:id', component: AddBudgetComponent },
  { path: 'estimatedAccountsBudget/details/:id', component: AddBudgetComponent },
  { path: 'estimatedCostCentersBudget', component: EstimatedCostCentersBudgetComponent },
  { path: 'estimatedCostCentersBudget/add', component: AddCostCentersBudgetComponent },
  { path: 'estimatedCostCentersBudget/edit/:id', component: AddCostCentersBudgetComponent },
  { path: 'estimatedCostCentersBudget/details/:id', component: AddCostCentersBudgetComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancialAnalysisRoutingModule { }
