import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { FinancialAnalysisRoutingModule } from './financial-analysis-routing.module';
import { EstimatedAccountsBudgetComponent } from './components/estimated-accounts-budget/estimated-accounts-budget.component';
import { AddBudgetComponent } from './components/estimated-accounts-budget/add-budget/add-budget.component';
import { EstimatedCostCentersBudgetComponent } from './components/estimated-cost-centers-budget/estimated-cost-centers-budget.component';
// tslint:disable-next-line:max-line-length
import { AddCostCentersBudgetComponent } from './components/estimated-cost-centers-budget/add-cost-centers-budget/add-cost-centers-budget.component';


@NgModule({
  declarations: [
    EstimatedAccountsBudgetComponent,
    AddBudgetComponent,
    EstimatedCostCentersBudgetComponent,
    AddCostCentersBudgetComponent
  ],
  imports: [
    SharedModule,
    FinancialAnalysisRoutingModule
  ]
})
export class FinancialAnalysisModule { }
