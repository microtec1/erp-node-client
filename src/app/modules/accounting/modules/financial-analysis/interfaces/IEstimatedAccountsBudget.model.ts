export interface IEstimatedAccountsBudget {
  _id: string;
  companyId: string;
  branchId: string;
  fiscalYearId: string;
  level: number;
  adjustmentRatio: number;
  descriptionAr: string;
  descriptionEn: string;
  accountDetails: [
    {
      accountId: string;
      debit: number;
      credit: number;
      accountingPeriodsDetails: [
        {
          periodCode: string;
          debit: number;
          credit: number;
        }
      ]
    }
  ];
}
