import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl } from 'src/app/common/constants/api.constants';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { companyId, branchId } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { IEstimatedAccountsBudget } from '../interfaces/IEstimatedAccountsBudget.model';
import { GeneralService } from 'src/app/modules/general/services/general.service';

@Injectable({
  providedIn: 'root'
})
export class EstimatedAccountsBudgetService {
  estimatedAccountsBudgets: IEstimatedAccountsBudget[] = [];
  estimatedAccountsBudgetsChanged = new Subject<IEstimatedAccountsBudget[]>();
  pageCountChanged = new Subject<any>();
  pageInfoChanged = new Subject<IDataRes>();
  sort: string;

  constructor(
    private http: HttpClient,
    private uiService: UiService,
    private generalService: GeneralService,
    private translate: TranslateService,
    private router: Router,
  ) { }

  setEstimatedAccountsBudgets(estimatedAccountsBudgets: IEstimatedAccountsBudget[]) {
    this.estimatedAccountsBudgets = estimatedAccountsBudgets;
    this.estimatedAccountsBudgetsChanged.next(this.estimatedAccountsBudgets);
  }

  getAllEstimatedAccountsBudgets(searchValues) {
    this.uiService.isLoading.next(true);
    return this.http.post(apiUrl + 'estimatedAccountsBudget/search', searchValues);
  }

  getEstimatedAccountsBudgets(pageNumber) {
    this.uiService.isLoading.next(true);
    return this.http.post(apiUrl + 'estimatedAccountsBudget/search?page=' + pageNumber, {
      companyId,
      branchId
    }, {
        params: {
          sort: this.sort ? this.sort : ''
        }
      }).pipe(
        tap((res: IDataRes) => {
          this.setEstimatedAccountsBudgets(res.results);
        })
      );
  }

  sortEstimatedAccountsBudgets(pageNumber, sortValue, sortType) {
    this.uiService.isLoading.next(true);
    this.sort = sortValue + '-' + sortType;
    return this.http.post(apiUrl + 'estimatedAccountsBudget/search?page=' + pageNumber, {
      companyId,
      branchId
    }, {
        params: {
          sort: this.sort
        }
      }).pipe(
        tap((res: IDataRes) => {
          this.setEstimatedAccountsBudgets(res.results);
        })
      );
  }

  getEstimatedAccountsBudget(estimatedAccountsBudgetId: string) {
    this.uiService.isLoading.next(true);
    return this.http.get(apiUrl + 'estimatedAccountsBudget/' + estimatedAccountsBudgetId);
  }

  addEstimatedAccountsBudget(estimatedAccountsBudget: IEstimatedAccountsBudget) {
    this.uiService.isLoading.next(true);
    return this.http.post(apiUrl + 'estimatedAccountsBudget', estimatedAccountsBudget).pipe(
      tap(((res: IEstimatedAccountsBudget) => {
        this.estimatedAccountsBudgets.push(res);
        this.setEstimatedAccountsBudgets(this.estimatedAccountsBudgets);
        this.getEstimatedAccountsBudgets(1).subscribe((data: IDataRes) => {
          this.pageInfoChanged.next(data);
          this.pageCountChanged.next(data.pages);
        });
      }))
    );
  }

  updateEstimatedAccountsBudget(newEstimatedAccountsBudget: IEstimatedAccountsBudget, estimatedAccountsBudgetId) {
    this.uiService.isLoading.next(true);
    this.http.put(apiUrl + 'estimatedAccountsBudget/' + estimatedAccountsBudgetId, newEstimatedAccountsBudget)
      .subscribe((res: IEstimatedAccountsBudget) => {
        const updateEstimatedAccountsBudget = this.estimatedAccountsBudgets
          .find(this.generalService.findIndexToUpdate, estimatedAccountsBudgetId);
        const index = this.estimatedAccountsBudgets.indexOf(updateEstimatedAccountsBudget);
        this.estimatedAccountsBudgets[index] = { _id: estimatedAccountsBudgetId, ...res };
        this.setEstimatedAccountsBudgets(this.estimatedAccountsBudgets);
        this.uiService.isLoading.next(false);
        this.router.navigate(['/financialAnalysis/estimatedAccountsBudget']);
        this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
      },
        err => {
          this.uiService.isLoading.next(false);
          if (this.translate.currentLang === 'en') {
            this.uiService.showError(err.error.error.en, '');
          } else {
            this.uiService.showError(err.error.error.ar, '');
          }
        });
  }

  deleteEstimatedAccountsBudget(estimatedAccountsBudgetId: string) {
    this.uiService.isLoading.next(true);
    return this.http.delete(apiUrl + 'estimatedAccountsBudget/' + estimatedAccountsBudgetId, {
      responseType: 'text',
    }).pipe(
      tap(res => {
        this.getEstimatedAccountsBudgets(1).subscribe((data: IDataRes) => {
          this.pageInfoChanged.next(data);
          this.pageCountChanged.next(data.pages);
          const updatedestimatedAccountsBudget = this.estimatedAccountsBudgets
            .filter(estimatedAccountsBudget => estimatedAccountsBudget._id !== estimatedAccountsBudgetId);
          this.setEstimatedAccountsBudgets(updatedestimatedAccountsBudget);
        });
      })
    );
  }

  getEstimatedAccountsBudgetByLevel(level) {
    this.uiService.isLoading.next(true);
    const requestBody = {
      companyId,
      branchId,
      level
    };
    return this.http.post(apiUrl + 'estimatedAccountsBudget/getBudgetByLevel', requestBody);
  }


}
