import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl } from 'src/app/common/constants/api.constants';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { companyId, branchId } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IEstimatedCostCentersBudget } from '../interfaces/IEstimatedCostCentersBudget.model';

@Injectable({
  providedIn: 'root'
})
export class EstimatedCostCentersBudgetService {
  estimatedCostCentersBudgets: IEstimatedCostCentersBudget[] = [];
  estimatedCostCentersBudgetsChanged = new Subject<IEstimatedCostCentersBudget[]>();
  pageCountChanged = new Subject<any>();
  pageInfoChanged = new Subject<IDataRes>();
  sort: string;

  constructor(
    private http: HttpClient,
    private uiService: UiService,
    private generalService: GeneralService,
    private translate: TranslateService,
    private router: Router,
  ) { }

  setEstimatedCostCentersBudgets(estimatedCostCentersBudgets: IEstimatedCostCentersBudget[]) {
    this.estimatedCostCentersBudgets = estimatedCostCentersBudgets;
    this.estimatedCostCentersBudgetsChanged.next(this.estimatedCostCentersBudgets);
  }

  getAllEstimatedCostCentersBudgets(searchValues) {
    this.uiService.isLoading.next(true);
    return this.http.post(apiUrl + 'estimatedCostCenterBudget/search', searchValues);
  }

  getEstimatedCostCentersBudgets(pageNumber) {
    this.uiService.isLoading.next(true);
    return this.http.post(apiUrl + 'estimatedCostCenterBudget/search?page=' + pageNumber, {
      companyId,
      branchId
    }, {
      params: {
        sort: this.sort ? this.sort : ''
      }
    }).pipe(
      tap((res: IDataRes) => {
        this.setEstimatedCostCentersBudgets(res.results);
      })
    );
  }

  sortEstimatedCostCentersBudgets(pageNumber, sortValue, sortType) {
    this.uiService.isLoading.next(true);
    this.sort = sortValue + '-' + sortType;
    return this.http.post(apiUrl + 'estimatedCostCenterBudget/search?page=' + pageNumber, {
      companyId,
      branchId
    }, {
      params: {
        sort: this.sort
      }
    }).pipe(
      tap((res: IDataRes) => {
        this.setEstimatedCostCentersBudgets(res.results);
      })
    );
  }

  getEstimatedCostCentersBudget(estimatedCostCentersBudgetId: string) {
    this.uiService.isLoading.next(true);
    return this.http.get(apiUrl + 'estimatedCostCenterBudget/' + estimatedCostCentersBudgetId);
  }

  addEstimatedCostCentersBudget(estimatedCostCentersBudget: IEstimatedCostCentersBudget) {
    this.uiService.isLoading.next(true);
    return this.http.post(apiUrl + 'estimatedCostCenterBudget', estimatedCostCentersBudget).pipe(
      tap(((res: IEstimatedCostCentersBudget) => {
        this.estimatedCostCentersBudgets.push(res);
        this.setEstimatedCostCentersBudgets(this.estimatedCostCentersBudgets);
        this.getEstimatedCostCentersBudgets(1).subscribe((data: IDataRes) => {
          this.pageInfoChanged.next(data);
          this.pageCountChanged.next(data.pages);
        });
      }))
    );
  }

  updateEstimatedCostCentersBudget(newEstimatedCostCentersBudget: IEstimatedCostCentersBudget, estimatedCostCentersBudgetId) {
    this.uiService.isLoading.next(true);
    this.http.put(apiUrl + 'estimatedCostCenterBudget/' + estimatedCostCentersBudgetId, newEstimatedCostCentersBudget)
      .subscribe((res: IEstimatedCostCentersBudget) => {
        const updateEstimatedCostCentersBudget = this.estimatedCostCentersBudgets
          .find(this.generalService.findIndexToUpdate, estimatedCostCentersBudgetId);
        const index = this.estimatedCostCentersBudgets.indexOf(updateEstimatedCostCentersBudget);
        this.estimatedCostCentersBudgets[index] = { _id: estimatedCostCentersBudgetId, ...res };
        this.setEstimatedCostCentersBudgets(this.estimatedCostCentersBudgets);
        this.uiService.isLoading.next(false);
        this.router.navigate(['/financialAnalysis/estimatedCostCentersBudget']);
        this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
      },
        err => {
          this.uiService.isLoading.next(false);
          if (this.translate.currentLang === 'en') {
            this.uiService.showError(err.error.error.en, '');
          } else {
            this.uiService.showError(err.error.error.ar, '');
          }
        });
  }

  deleteEstimatedCostCentersBudget(estimatedCostCentersBudgetId: string) {
    this.uiService.isLoading.next(true);
    return this.http.delete(apiUrl + 'estimatedCostCenterBudget/' + estimatedCostCentersBudgetId, {
      responseType: 'text',
    }).pipe(
      tap(res => {
        this.getEstimatedCostCentersBudgets(1).subscribe((data: IDataRes) => {
          this.pageInfoChanged.next(data);
          this.pageCountChanged.next(data.pages);
          const updatedestimatedCostCentersBudget = this.estimatedCostCentersBudgets
            .filter(estimatedCostCentersBudget => estimatedCostCentersBudget._id !== estimatedCostCentersBudgetId);
          this.setEstimatedCostCentersBudgets(updatedestimatedCostCentersBudget);
        });
      })
    );
  }

  getEstimatedCostCentersBudgetByLevel(level) {
    this.uiService.isLoading.next(true);
    const requestBody = {
      companyId,
      branchId,
      level
    };
    return this.http.post(apiUrl + 'estimatedCostCenterBudget/getBudgetByLevel', requestBody);
  }


}
