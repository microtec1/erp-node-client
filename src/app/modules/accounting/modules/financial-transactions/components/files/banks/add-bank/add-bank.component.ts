import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IBank } from '../../../../interfaces/IBank';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { CustomValidators } from 'ng2-validation';
import { searchLength, companyId } from 'src/app/common/constants/general.constants';
import { ICurrency } from 'src/app/modules/general/interfaces/ICurrency';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IBranch } from 'src/app/modules/general/interfaces/IBranch';
import { ICompany } from 'src/app/modules/general/interfaces/ICompany';
import { DataService } from 'src/app/common/services/shared/data.service';
import { banksApi, currenciesApi, detailedChartOfAccountsApi, companyCurrenciesApi, companiesApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-bank',
  templateUrl: './add-bank.component.html',
  styleUrls: ['./add-bank.component.scss']
})
export class AddBankComponent implements OnInit, OnDestroy {
  bankForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  bank: IBank;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  expensesAccountsInputFocused: boolean;
  underCollectionChecksAccountsInputFocused: boolean;
  underPaymentChecksAccountsInputFocused: boolean;
  companyId = companyId;
  detailsMode: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Currencies
  currencies: ICurrency[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  hasMoreCurrencies: boolean;
  currenciesCount: number;
  selectedCurrenciesPage = 1;
  currenciesPagesNo: number;
  noCurrencies: boolean;

  // Branches
  branches: IBranch[] = [];
  branchesInputFocused: boolean;
  branchesCount: number;
  noBranches: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(banksApi, null, null, id).subscribe((bank: IBank) => {
              this.bank = bank;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.bankForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(currenciesApi, 1).subscribe((res: IDataRes) => {
        this.currenciesPagesNo = res.pages;
        this.currenciesCount = res.count;
        if (this.currenciesPagesNo > this.selectedCurrenciesPage) {
          this.hasMoreCurrencies = true;
        }
        this.currencies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(companiesApi, null, null, companyId).subscribe((res: ICompany) => {
        if (res.branches.length) {
          this.branchesCount = res.branches.length;
          this.branches.push(...res.branches);
        } else {
          this.noBranches = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCurrencies(event) {
    const searchValue = event;
    const searchQuery = {
      currencyNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(currenciesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCurrencies = true;
          } else {
            this.noCurrencies = false;
            for (const item of res.results) {
              if (this.currencies.length) {
                const uniqueCurrencies = this.currencies.filter(x => x._id !== item._id);
                this.currencies = uniqueCurrencies;
              }
              this.currencies.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCurrencies() {
    this.selectedCurrenciesPage = this.selectedCurrenciesPage + 1;
    this.subscriptions.push(
      this.data.get(currenciesApi, this.selectedCurrenciesPage).subscribe((res: IDataRes) => {
        if (this.currenciesPagesNo > this.selectedCurrenciesPage) {
          this.hasMoreCurrencies = true;
        } else {
          this.hasMoreCurrencies = false;
        }
        for (const item of res.results) {
          if (this.currencies.length) {
            const uniqueCurrencies = this.currencies.filter(x => x._id !== item._id);
            this.currencies = uniqueCurrencies;
          }
          this.currencies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  get form() {
    return this.bankForm.controls;
  }

  clearValue(array: FormArray, index, fieldName) {
    const group = array.controls[index] as FormGroup;
    if (fieldName === 'debit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        credit: null
      });
    }
    if (fieldName === 'credit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        debit: null
      });
    }
  }

  get getOpeningBalancesArray() {
    return this.bankForm.get('openingBalances') as FormArray;
  }

  addOpeningBalance() {
    const control = this.bankForm.get('openingBalances') as FormArray;
    control.push(new FormGroup({
      accountId: new FormControl(''),
      currencyId: new FormControl(''),
      exchangeRate: new FormControl('', CustomValidators.number),
      debit: new FormControl('', CustomValidators.number),
      credit: new FormControl('', CustomValidators.number),
    }));
  }

  deleteOpeningBalance(index) {
    const control = this.bankForm.get('openingBalances') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  allBranches(value) {
    if (!value) {
      this.bankForm.controls.branchesId.setValidators(Validators.required);
      this.bankForm.controls.branchesId.updateValueAndValidity();
    } else {
      this.bankForm.controls.branchesId.clearValidators();
      this.bankForm.controls.branchesId.updateValueAndValidity();
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (this.bank) {
      if (this.bankForm.valid) {
        const newBank = { _id: this.bank._id, ...this.generalService.checkEmptyFields(this.bankForm.value) };
        this.subscriptions.push(
          this.data.put(banksApi, newBank).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/financialTransactions/banks']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.bankForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.bankForm.value),
          companyId: this.companyId
        };
        const emptyObject = this.generalService.isEmpty(this.bankForm.value.openingBalances[0]);
        if (emptyObject) {
          formValue.openingBalances = [];
        }
        this.subscriptions.push(
          this.data.post(banksApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.bankForm.reset();
            this.bankForm.patchValue({
              allBranches: true,
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let bankNameAr = '';
    let bankNameEn = '';
    let ibanAccountNumber = '';
    let glAccountId = '';
    let currencyId = '';
    let allBranches = true;
    let branchesId = [];
    let expensesAccountId = '';
    let underCollectionChecksAccountId = '';
    let underPaymentChecksAccountId = '';
    let creditCardCommissionRatio = '';
    let creditCardMaximumLimit = '';
    let masterCardCommissionRatio = '';
    let masterCardMaximumLimit = '';
    let networkCommissionRatio = '';
    let networkMaximumLimit = '';
    let bankAddress = '';
    let bankResponsible = '';
    let telephoneNumber = '';
    let faxNumber = '';
    let openingBalancesArray = new FormArray([
      new FormGroup({
        accountId: new FormControl(''),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl('', CustomValidators.number),
        debit: new FormControl('', CustomValidators.number),
        credit: new FormControl('', CustomValidators.number),
      })
    ]);
    let isActive = true;

    if (this.bank) {
      code = this.bank.code;
      bankNameAr = this.bank.bankNameAr;
      bankNameEn = this.bank.bankNameEn;
      ibanAccountNumber = this.bank.ibanAccountNumber;
      glAccountId = this.bank.glAccountId;
      currencyId = this.bank.currencyId;
      allBranches = this.bank.allBranches;
      expensesAccountId = this.bank.expensesAccountId;
      underCollectionChecksAccountId = this.bank.underCollectionChecksAccountId;
      underPaymentChecksAccountId = this.bank.underPaymentChecksAccountId;
      creditCardCommissionRatio = this.bank.creditCardCommissionRatio;
      creditCardMaximumLimit = this.bank.creditCardMaximumLimit;
      masterCardCommissionRatio = this.bank.masterCardCommissionRatio;
      masterCardMaximumLimit = this.bank.masterCardMaximumLimit;
      networkCommissionRatio = this.bank.networkCommissionRatio;
      networkMaximumLimit = this.bank.networkMaximumLimit;
      bankAddress = this.bank.bankAddress;
      bankResponsible = this.bank.bankResponsible;
      telephoneNumber = this.bank.telephoneNumber;
      faxNumber = this.bank.faxNumber;
      isActive = this.bank.isActive;
      if (this.bank.branchesId.length) {
        branchesId = [];
        for (const bank of this.bank.branchesId) {
          branchesId.push(bank);
        }
      }
      if (this.bank.openingBalances.length) {
        openingBalancesArray = new FormArray([]);
        for (const control of this.bank.openingBalances) {
          openingBalancesArray.push(
            new FormGroup({
              accountId: new FormControl(control.accountId),
              currencyId: new FormControl(control.currencyId),
              exchangeRate: new FormControl(control.exchangeRate, CustomValidators.number),
              debit: new FormControl(control.debit, CustomValidators.number),
              credit: new FormControl(control.credit, CustomValidators.number),
            })
          );
        }
      }
    }
    this.bankForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      ibanAccountNumber: new FormControl(ibanAccountNumber),
      bankNameAr: new FormControl(bankNameAr, Validators.required),
      bankNameEn: new FormControl(bankNameEn),
      glAccountId: new FormControl(glAccountId),
      currencyId: new FormControl(currencyId, Validators.required),
      allBranches: new FormControl(allBranches),
      branchesId: new FormControl(branchesId),
      expensesAccountId: new FormControl(expensesAccountId),
      underCollectionChecksAccountId: new FormControl(underCollectionChecksAccountId),
      underPaymentChecksAccountId: new FormControl(underPaymentChecksAccountId),
      creditCardCommissionRatio: new FormControl(creditCardCommissionRatio, CustomValidators.number),
      creditCardMaximumLimit: new FormControl(creditCardMaximumLimit, CustomValidators.number),
      masterCardCommissionRatio: new FormControl(masterCardCommissionRatio, CustomValidators.number),
      masterCardMaximumLimit: new FormControl(masterCardMaximumLimit, CustomValidators.number),
      networkCommissionRatio: new FormControl(networkCommissionRatio, CustomValidators.number),
      networkMaximumLimit: new FormControl(networkMaximumLimit, CustomValidators.number),
      bankAddress: new FormControl(bankAddress),
      bankResponsible: new FormControl(bankResponsible),
      telephoneNumber: new FormControl(telephoneNumber, CustomValidators.number),
      faxNumber: new FormControl(faxNumber, CustomValidators.number),
      openingBalances: openingBalancesArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
