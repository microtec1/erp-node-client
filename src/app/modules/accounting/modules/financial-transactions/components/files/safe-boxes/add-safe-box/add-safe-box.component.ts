import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { CustomValidators } from 'ng2-validation';
import { searchLength, companyId } from 'src/app/common/constants/general.constants';
import { ICurrency } from 'src/app/modules/general/interfaces/ICurrency';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IBox } from '../../../../interfaces/IBox';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IBranch } from 'src/app/modules/general/interfaces/IBranch';
import { ICompany } from 'src/app/modules/general/interfaces/ICompany';
import { DataService } from 'src/app/common/services/shared/data.service';
import { safeBoxesApi, currenciesApi, detailedChartOfAccountsApi, companiesApi, companyCurrenciesApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-safe-box',
  templateUrl: './add-safe-box.component.html',
  styleUrls: ['./add-safe-box.component.scss']
})
export class AddSafeBoxComponent implements OnInit, OnDestroy {
  safeBoxForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  safeBox: IBox;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  companyId = companyId;
  detailsMode: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  hasMoreCurrencies: boolean;
  currenciesCount: number;
  selectedCurrenciesPage = 1;
  currenciesPagesNo: number;
  noCurrencies: boolean;

  // Branches
  branches: IBranch[] = [];
  branchesInputFocused: boolean;
  branchesCount: number;
  noBranches: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(safeBoxesApi, null, null, id).subscribe((safeBox: IBox) => {
              this.safeBox = safeBox;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.safeBoxForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(companyCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.companyCurrency.length) {
            this.currencies.push(...res.companyCurrency);
            this.currenciesCount = res.companyCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(companiesApi, null, null, companyId).subscribe((res: ICompany) => {
        if (res.branches.length) {
          this.branchesCount = res.branches.length;
          this.branches.push(...res.branches);
        } else {
          this.noBranches = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCurrencies(event) {
    const searchValue = event;
    const searchQuery = {
      currencyNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(currenciesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCurrencies = true;
          } else {
            this.noCurrencies = false;
            for (const item of res.results) {
              if (this.currencies.length) {
                const uniqueCurrencies = this.currencies.filter(x => x._id !== item._id);
                this.currencies = uniqueCurrencies;
              }
              this.currencies.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCurrencies() {
    this.selectedCurrenciesPage = this.selectedCurrenciesPage + 1;
    this.subscriptions.push(
      this.data.get(currenciesApi, this.selectedCurrenciesPage).subscribe((res: IDataRes) => {
        if (this.currenciesPagesNo > this.selectedCurrenciesPage) {
          this.hasMoreCurrencies = true;
        } else {
          this.hasMoreCurrencies = false;
        }
        for (const item of res.results) {
          if (this.currencies.length) {
            const uniqueCurrencies = this.currencies.filter(x => x._id !== item._id);
            this.currencies = uniqueCurrencies;
          }
          this.currencies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  get form() {
    return this.safeBoxForm.controls;
  }

  allBranches(value) {
    if (!value) {
      this.safeBoxForm.controls.branchesId.setValidators(Validators.required);
      this.safeBoxForm.controls.branchesId.updateValueAndValidity();
    } else {
      this.safeBoxForm.controls.branchesId.clearValidators();
      this.safeBoxForm.controls.branchesId.updateValueAndValidity();
    }
  }

  fillExchangeRate(value, formGroup: FormGroup) {
    if (value) {
      const currency = this.currencies.find(x => x.currencyId === value);
      formGroup.patchValue({
        exchangeRate: currency.exchangeRate
      });
    }
  }

  clearValue(array: FormArray, index, fieldName) {
    const group = array.controls[index] as FormGroup;
    if (fieldName === 'debit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        credit: null
      });
    }
    if (fieldName === 'credit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        debit: null
      });
    }
  }

  get getOpeningBalancesArray() {
    return this.safeBoxForm.get('openingBalances') as FormArray;
  }

  addOpeningBalance() {
    const control = this.safeBoxForm.get('openingBalances') as FormArray;
    control.push(new FormGroup({
      currencyId: new FormControl(''),
      exchangeRate: new FormControl('', CustomValidators.number),
      debit: new FormControl('', CustomValidators.number),
      credit: new FormControl('', CustomValidators.number),
    }));
  }

  deleteOpeningBalance(index) {
    const control = this.safeBoxForm.get('openingBalances') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.safeBox) {
      if (this.safeBoxForm.valid) {
        const newsafeBox = { _id: this.safeBox._id, ...this.generalService.checkEmptyFields(this.safeBoxForm.value) };
        this.subscriptions.push(
          this.data.put(safeBoxesApi, newsafeBox).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/financialTransactions/safeBoxes']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {

      if (this.safeBoxForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.safeBoxForm.value),
          companyId: this.companyId
        };
        const emptyObject = this.generalService.isEmpty(this.safeBoxForm.value.openingBalances[0]);
        if (emptyObject) {
          formValue.openingBalances = [];
        }

        this.subscriptions.push(
          this.data.post(safeBoxesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.safeBoxForm.reset();
            this.safeBoxForm.patchValue({
              allBranches: true,
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let safeBoxNameAr = '';
    let safeBoxNameEn = '';
    let glAccountId = '';
    let currencyId = '';
    let allBranches = true;
    let branchesId = [];
    let openingBalancesArray = new FormArray([
      new FormGroup({
        currencyId: new FormControl(''),
        exchangeRate: new FormControl('', CustomValidators.number),
        debit: new FormControl('', CustomValidators.number),
        credit: new FormControl('', CustomValidators.number),
      })
    ]);
    let isActive = true;

    if (this.safeBox) {
      code = this.safeBox.code;
      safeBoxNameAr = this.safeBox.safeBoxNameAr;
      safeBoxNameEn = this.safeBox.safeBoxNameEn;
      glAccountId = this.safeBox.glAccountId;
      currencyId = this.safeBox.currencyId;
      allBranches = this.safeBox.allBranches;
      isActive = this.safeBox.isActive;
      if (this.safeBox.branchesId.length) {
        branchesId = [];
        for (const branch of this.safeBox.branchesId) {
          branchesId.push(branch);
        }
      }
      if (this.safeBox.openingBalances.length) {
        openingBalancesArray = new FormArray([]);
        for (const control of this.safeBox.openingBalances) {
          openingBalancesArray.push(
            new FormGroup({
              currencyId: new FormControl(control.currencyId),
              exchangeRate: new FormControl(control.exchangeRate, CustomValidators.number),
              debit: new FormControl(control.debit, CustomValidators.number),
              credit: new FormControl(control.credit, CustomValidators.number),
            })
          );
        }
      }
    }
    this.safeBoxForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      safeBoxNameAr: new FormControl(safeBoxNameAr, Validators.required),
      safeBoxNameEn: new FormControl(safeBoxNameEn),
      glAccountId: new FormControl(glAccountId),
      currencyId: new FormControl(currencyId, Validators.required),
      allBranches: new FormControl(allBranches),
      branchesId: new FormControl(branchesId),
      openingBalances: openingBalancesArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
