import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { companyId } from 'src/app/common/constants/general.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, safeBoxesApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IBox } from '../../../interfaces/IBox';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-safe-boxes',
  templateUrl: './safe-boxes.component.html',
  styleUrls: ['./safe-boxes.component.scss']
})
export class SafeBoxesComponent implements OnInit, OnDestroy {
  safeBoxes: IBox[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getSafeBoxesFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(safeBoxesApi, pageNumber).subscribe((res: IDataRes) => {
      this.safeBoxes = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(safeBoxesApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.safeBoxes = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getSafeBoxesFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(safeBoxesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.safeBoxes = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      safeBoxNameAr: new FormControl(''),
      safeBoxNameEn: new FormControl('')
    });
  }

  deleteModal(safeBox: IBox) {
    const initialState = {
      code: safeBox.code,
      nameAr: safeBox.safeBoxNameAr,
      nameEn: safeBox.safeBoxNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(safeBox._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(safeBoxesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.safeBoxes = this.generalService.removeItem(this.safeBoxes, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getSafeBoxesFirstPage() {
    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxes = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
