import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router, Data } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { searchLength, companyId, branchId } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { IAdjustment } from '../../../../interfaces/IAdjustment';
import { branchCurrenciesApi, customersApi, suppliersApi, adjustmentApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-adjustment',
  templateUrl: './add-adjustment.component.html',
  styleUrls: ['./add-adjustment.component.scss']
})
export class AddAdjustmentComponent implements OnInit, OnDestroy {
  adjustmentForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  adjustment: IAdjustment;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  bsModalRef: BsModalRef;
  detailsMode: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  constructor(
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(adjustmentApi, null, null, id).subscribe((adjustment: IAdjustment) => {
              this.adjustment = adjustment;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.adjustmentForm.disable({ onlySelf: true });
              }
              this.syncControls();
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.syncControls();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  fillRate(event, formGroup: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        exchangeRate: currency.exchangeRate
      });
    } else {
      this.adjustmentForm.patchValue({
        exchangeRate: currency.exchangeRate
      });
    }
  }

  syncControls() {
    this.adjustmentForm.controls.adjustmentsDetailsList.valueChanges.subscribe(res => {
      const valuesArray = [];
      for (let i = 0; i <= res.length - 1; i++) {
        if (res[i]) {
          valuesArray.push(res[i].value);
        }
      }
      const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);

      this.adjustmentForm.patchValue({
        totalValue: valuesSum
      });
    });
  }

  get form() {
    return this.adjustmentForm.controls;
  }

  get getInvoicesTableArray() {
    return this.adjustmentForm.get('invoicesTable') as FormArray;
  }

  get getDebenturesTableArray() {
    return this.adjustmentForm.get('debenturesTable') as FormArray;
  }

  addDebenture() {
    const control = this.adjustmentForm.get('debenturesTable') as FormArray;
    control.push(
      new FormGroup({
        debentureId: new FormControl(''),
        documentNumber: new FormControl(''),
        debentureType: new FormControl(''),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        descriptionStatement: new FormControl(''),
        dateGregorian: new FormControl(''),
        dateHijri: new FormControl(null),
        sourceValue: new FormControl(null),
        balance: new FormControl(null),
        amountPaid: new FormControl(null),
        amountAdjustmentCurrency: new FormControl(null),
        differenceValue: new FormControl(null),
      })
    );
  }

  deleteDebenture(index) {
    const control = this.adjustmentForm.get('debenturesTable') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }
  addInvoice() {
    const control = this.adjustmentForm.get('invoicesTable') as FormArray;
    control.push(
      new FormGroup({
        invoiceId: new FormControl(''),
        documentNumber: new FormControl(''),
        invoiceType: new FormControl(''),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        descriptionStatement: new FormControl(''),
        dateGregorian: new FormControl(''),
        dateHijri: new FormControl(null),
        sourceValue: new FormControl(null),
        balance: new FormControl(null),
        amountPaid: new FormControl(null),
        amountAdjustmentCurrency: new FormControl(null),
        differenceValue: new FormControl(null),
      })
    );
  }

  deleteInvoice(index) {
    const control = this.adjustmentForm.get('invoicesTable') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup: FormGroup) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      formGroup.patchValue({
        hijriDate: {
          year: hijriDate.iYear(),
          month: hijriDate.iMonth() + 1,
          day: hijriDate.iDate()
        }
      });
    }
  }

  setGregorianDate(value, formGroup: FormGroup) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      formGroup.patchValue({
        gregorianDate: this.generalService.format(
          new Date(
            gegorianDate.year(),
            gegorianDate.month(),
            gegorianDate.date()
          )
        )
      });
    }
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.adjustmentForm.value.gregorianDate) {
      if (
        typeof this.adjustmentForm.value.gregorianDate !==
        'string'
      ) {
        this.adjustmentForm.value.gregorianDate = this.generalService.format(
          this.adjustmentForm.value.gregorianDate
        );
      }
      if (
        typeof this.adjustmentForm.value.hijriDate !==
        'string'
      ) {
        this.adjustmentForm.value.hijriDate = this.generalService.formatHijriDate(
          this.adjustmentForm.value.hijriDate
        );
      }
    }
    if (this.adjustment) {
      if (this.adjustmentForm.valid) {
        const newadjustment = {
          _id: this.adjustment._id,
          ...this.generalService.checkEmptyFields(this.adjustmentForm.value),
          companyId,
          branchId
        };
        this.subscriptions.push(
          this.data.put(adjustmentApi, newadjustment).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/financialTransactions/adjustments']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
            }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
        this.loadingButton = false;
      }
    } else {

      if (this.adjustmentForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.adjustmentForm.value),
          companyId,
          branchId
        };

        this.subscriptions.push(
          this.data.post(adjustmentApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.adjustmentForm.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let gregorianDate = '';
    let hijriDate = null;
    let status = 'unposted';
    let adjustmentArabicDescription = '';
    let adjustmentEnglishDescription = '';
    let adjustmentType = '';
    let supplierId = '';
    let customerId = '';
    let currencyId = '';
    let exchangeRate = null;
    let totalValue = null;
    let isActive = true;
    let invoicesTableArray = new FormArray([
      new FormGroup({
        invoiceId: new FormControl(''),
        documentNumber: new FormControl(''),
        invoiceType: new FormControl(''),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        descriptionStatement: new FormControl(''),
        dateGregorian: new FormControl(''),
        dateHijri: new FormControl(null),
        sourceValue: new FormControl(null),
        balance: new FormControl(null),
        amountPaid: new FormControl(null),
        amountAdjustmentCurrency: new FormControl(null),
        differenceValue: new FormControl(null),
      })
    ]);
    let debenturesTableArray = new FormArray([
      new FormGroup({
        debentureId: new FormControl(''),
        documentNumber: new FormControl(''),
        debentureType: new FormControl(''),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        descriptionStatement: new FormControl(''),
        dateGregorian: new FormControl(''),
        dateHijri: new FormControl(null),
        sourceValue: new FormControl(null),
        balance: new FormControl(null),
        amountPaid: new FormControl(null),
        amountAdjustmentCurrency: new FormControl(null),
        differenceValue: new FormControl(null),
      })
    ]);

    if (this.adjustment) {
      code = this.adjustment.code;
      gregorianDate = this.adjustment.gregorianDate;
      hijriDate = this.adjustment.hijriDate;
      status = this.adjustment.status;
      adjustmentArabicDescription = this.adjustment.adjustmentArabicDescription;
      adjustmentEnglishDescription = this.adjustment.adjustmentEnglishDescription;
      adjustmentType = this.adjustment.adjustmentType;
      supplierId = this.adjustment.supplierId;
      customerId = this.adjustment.customerId;
      currencyId = this.adjustment.currencyId;
      exchangeRate = this.adjustment.exchangeRate;
      totalValue = this.adjustment.totalValue;
      isActive = this.adjustment.isActive;
      invoicesTableArray = new FormArray([]);
      for (const item of this.adjustment.invoicesTable) {
        invoicesTableArray.push(
          new FormGroup({
            invoiceId: new FormControl(item.invoiceId),
            documentNumber: new FormControl(item.documentNumber),
            invoiceType: new FormControl(item.invoiceType),
            currencyId: new FormControl(item.currencyId),
            exchangeRate: new FormControl(item.exchangeRate),
            descriptionStatement: new FormControl(item.descriptionStatement),
            dateGregorian: new FormControl(item.dateGregorian),
            dateHijri: new FormControl(item.dateHijri),
            sourceValue: new FormControl(item.sourceValue),
            balance: new FormControl(item.balance),
            amountPaid: new FormControl(item.amountPaid),
            amountAdjustmentCurrency: new FormControl(item.amountAdjustmentCurrency),
            differenceValue: new FormControl(item.differenceValue),
          })
        );
      }

      debenturesTableArray = new FormArray([]);
      for (const item of this.adjustment.debenturesTable) {
        invoicesTableArray.push(
          new FormGroup({
            debentureId: new FormControl(item.debentureId),
            documentNumber: new FormControl(item.documentNumber),
            debentureType: new FormControl(item.debentureType),
            currencyId: new FormControl(item.currencyId),
            exchangeRate: new FormControl(item.exchangeRate),
            descriptionStatement: new FormControl(item.descriptionStatement),
            dateGregorian: new FormControl(item.dateGregorian),
            dateHijri: new FormControl(item.dateHijri),
            sourceValue: new FormControl(item.sourceValue),
            balance: new FormControl(item.balance),
            amountPaid: new FormControl(item.amountPaid),
            amountAdjustmentCurrency: new FormControl(item.amountAdjustmentCurrency),
            differenceValue: new FormControl(item.differenceValue),
          })
        );
      }
    }

    this.adjustmentForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      dateGregorian: new FormControl(gregorianDate, Validators.required),
      dateHijri: new FormControl(hijriDate, Validators.required),
      status: new FormControl(status),
      adjustmentArabicDescription: new FormControl(adjustmentArabicDescription),
      adjustmentEnglishDescription: new FormControl(adjustmentEnglishDescription),
      adjustmentType: new FormControl(adjustmentType, Validators.required),
      supplierId: new FormControl(supplierId),
      customerId: new FormControl(customerId),
      currencyId: new FormControl(currencyId, Validators.required),
      exchangeRate: new FormControl(exchangeRate),
      totalValue: new FormControl(totalValue),
      invoicesTable: invoicesTableArray,
      debenturesTable: debenturesTableArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
