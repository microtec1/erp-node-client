import { Component, OnInit, OnDestroy } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { IDataRes } from "src/app/common/interfaces/IDataResponse.model";
import {
  pagingStart,
  pagingEnd,
} from "src/app/common/constants/general.constants";
import { FormGroup, FormControl } from "@angular/forms";
import { baseUrl, adjustmentApi } from "src/app/common/constants/api.constants";
import { UiService } from "src/app/common/services/ui/ui.service";
import { GeneralService } from "src/app/modules/general/services/general.service";
import { ConfirmModalComponent } from "src/app/common/components/confirm-modal/confirm-modal.component";
import { IAdjustment } from "../../../interfaces/IAdjustment";
import { DataService } from "src/app/common/services/shared/data.service";

@Component({
  selector: "app-adjustments",
  templateUrl: "./adjustments.component.html",
  styleUrls: ["./adjustments.component.scss"],
})
export class AdjustmentsComponent implements OnInit, OnDestroy {
  adjustments: IAdjustment[];
  bsModalRef: BsModalRef;
  adjustment: IAdjustment;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;
  pageCount = [];
  filteredAdjustments = [];

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) {}

  ngOnInit() {
    this.getAdjustmentsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(adjustmentApi, pageNumber).subscribe((res: IDataRes) => {
      this.adjustments = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === "asc" ? "desc" : "asc";
    this.sortValue = sortValue;
    this.data
      .dataSort(adjustmentApi, this.currentPage, sortValue, this.sortType)
      .subscribe((res: IDataRes) => {
        this.adjustments = res.results;
        this.uiService.isLoading.next(false);
      });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === "asc") {
        return "asc";
      } else if (this.sortType === "desc") {
        return "desc";
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(
      this.searchForm.value
    );
    if (this.generalService.isEmpty(searchValues)) {
      this.getAdjustmentsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data
          .get(adjustmentApi, null, searchValues)
          .subscribe((res: IDataRes) => {
            if (res.results.length) {
              this.adjustments = res.results;
              this.pageInfo = res;
              this.searchDone = true;
              this.uiService.isLoading.next(false);
            } else {
              this.uiService.showError("GENERAL.noRecords", "GENERAL.tryAgain");
              this.uiService.isLoading.next(false);
            }
          })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(""),
      adjustmentArabicDescription: new FormControl(""),
      adjustmentEnglishDescription: new FormControl(""),
    });
  }

  deleteModal(adjustment: IAdjustment) {
    const initialState = {
      code: adjustment.code,
      nameAr: adjustment.adjustmentArabicDescription,
      nameEn: adjustment.adjustmentEnglishDescription,
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, {
      initialState,
      class: "confirm-modal",
    });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe((confirmed) => {
        if (confirmed) {
          this.delete(adjustment._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(adjustmentApi, id).subscribe(
        (res) => {
          this.uiService.showSuccess("GENERAL.deletedSuccessfully", "");
          this.adjustments = this.generalService.removeItem(
            this.adjustments,
            id
          );
          this.uiService.isLoading.next(false);
        },
        (err) => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        }
      )
    );
  }

  getAdjustmentsFirstPage() {
    this.subscriptions.push(
      this.data.get(adjustmentApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.adjustments = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
