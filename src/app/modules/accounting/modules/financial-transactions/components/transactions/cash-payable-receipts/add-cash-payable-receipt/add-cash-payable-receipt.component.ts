import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { searchLength, companyId, branchId } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PaidTableComponent } from '../../paid-table/paid-table.component';
import { DocumentTableComponent } from '../../document-table/document-table.component';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { ICashPayableReceipt } from '../../../../interfaces/ICashPayableReceipt';
import { DataService } from 'src/app/common/services/shared/data.service';
import { cashPayableReceiptsApi, suppliersApi, customersApi, branchCurrenciesApi, companyCurrenciesApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-cash-payable-receipt',
  templateUrl: './add-cash-payable-receipt.component.html',
  styleUrls: ['./add-cash-payable-receipt.component.scss']
})
export class AddCashPayableReceiptComponent implements OnInit, OnDestroy {
  cashPayableReceiptForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  cashPayableReceipt: ICashPayableReceipt;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  bsModalRef: BsModalRef;
  detailsMode: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Company Currencies
  companyCurrencies: any[] = [];
  companyCurrenciesInputFocused: boolean;
  companyCurrenciesCount: number;
  noCompanyCurrencies: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(cashPayableReceiptsApi, null, null, id).subscribe((cashPayableReceipt: ICashPayableReceipt) => {
              this.cashPayableReceipt = cashPayableReceipt;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.cashPayableReceiptForm.disable({ onlySelf: true });
              }
              this.syncControls();
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.syncControls();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(companyCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.companyCurrency.length) {
            this.companyCurrencies.push(...res.companyCurrency);
            this.companyCurrenciesCount = res.companyCurrency.length;
          } else {
            this.noCompanyCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  fillRate(event, formGroup: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        exchangeRate: currency.exchangeRate
      });
    } else {
      this.cashPayableReceiptForm.patchValue({
        exchangeRate: currency.exchangeRate
      });
    }
  }

  fillCurrency(event, formGroup: FormGroup) {
    let data;
    if (this.cashPayableReceiptForm.value.targetAccountType === 'customer') {
      data = this.customers.find(item => item._id === event);
    } else {
      data = this.suppliers.find(item => item._id === event);
    }
    formGroup.patchValue({
      currencyId: data.currencyId,
      exchangeRate: data.exchangeRate
    });
  }

  syncControls() {
    this.subscriptions.push(
      this.cashPayableReceiptForm.controls.cashPayableReceiptDetailsList.valueChanges.subscribe(res => {
        const valuesArray = [];
        for (let i = 0; i <= res.length - 1; i++) {
          if (res[i]) {
            valuesArray.push(res[i].totalValue);
          }
        }
        const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);

        this.cashPayableReceiptForm.patchValue({
          totalValue: valuesSum
        });
      })
    );
  }

  paidTable(id, currencyId, exchangeRate, type) {
    const formGroup: any = this.getCashPayableReceiptDetailsListArray.controls.find(item => item.value.targetAccountId === id);
    const currencyName = this.currencies.find(item => item.currencyId === currencyId).currencyNameAr;
    let data;
    if (formGroup.controls.paidTable.controls.length) {
      data = formGroup.controls.paidTable.value;
    }
    const initialState = {
      data,
      detailsMode: this.detailsMode ? this.detailsMode : null,
      currencyName,
      exchangeRate,
      type
    };
    this.bsModalRef = this.modalService.show(PaidTableComponent, { initialState, class: 'big-modal' });
    this.bsModalRef.content.formValue.subscribe(res => {
      formGroup.controls.paidTable.controls = [];
      for (const item of res.paidTable) {
        formGroup.controls.paidTable.push(
          new FormGroup({
            paidMethod: new FormControl(item.paidMethod),
            accountPayments: new FormControl(item.accountPayments),
            currencyId: new FormControl(this.cashPayableReceiptForm.value.currencyId),
            exchangeRate: new FormControl(this.cashPayableReceiptForm.value.exchangeRate),
            paidValue: new FormControl(item.paidValue),
            dueDateGregorian: new FormControl(item.dueDateGregorian),
            dueDateHijri: new FormControl(item.dueDateHijri),
            paidDateGregorian: new FormControl(item.paidDateGregorian),
            paidDateHijri: new FormControl(item.paidDateHijri),
          })
        );
      }
    });
  }

  documentTable(id, exchangeRate, type) {
    let data;
    let tableData;
    if (this.cashPayableReceiptForm.value.targetAccountType === 'customer') {
      data = this.customers.find(item => item._id === id);
    } else {
      data = this.suppliers.find(item => item._id === id);
    }
    const formGroup: any = this.getCashPayableReceiptDetailsListArray.controls.find(item => item.value.targetAccountId === id);
    if (formGroup.controls.documentTable.value.documentTableList.length) {
      tableData = formGroup.controls.documentTable.value;
    }
    const initialState = {
      data,
      tableData,
      type,
      detailsMode: this.detailsMode ? this.detailsMode : null,
      companyCurrencies: this.companyCurrencies,
      originalExchangeRate: exchangeRate
    };
    this.bsModalRef = this.modalService.show(DocumentTableComponent, { initialState, class: 'big-modal' });
    this.bsModalRef.content.formValue.subscribe(res => {
      const target = formGroup.controls.documentTable;
      target.patchValue({
        distributedValue: res.documentTable.distributedValue
      });
      target.controls.documentTableList.controls = [];
      for (const item of res.documentTable.documentTableList) {
        if (type === 'receipt') {
          target.controls.documentTableList.push(
            new FormGroup({
              documentNumber: new FormControl(item.documentNumber),
              currencyId: new FormControl(item.currencyId),
              exchangeRate: new FormControl(item.exchangeRate),
              originalDocumentValue: new FormControl(item.originalDocumentValue),
              documentBalance: new FormControl(item.documentBalance),
              documentDateGregorian: new FormControl(item.documentDateGregorian),
              documentDateHijri: new FormControl(item.documentDateHijri),
              dueDocumentDateGregorian: new FormControl(item.dueDocumentDateGregorian),
              dueDocumentDateHijri: new FormControl(item.dueDocumentDateHijri),
              documentPaidValue: new FormControl(item.documentPaidValue),
              documentPaidValueReceiptCurrency: new FormControl(item.documentPaidValueReceiptCurrency),
            })
          );
        } else {
          target.controls.documentTableList.push(
            new FormGroup({
              documentNumber: new FormControl(item.documentNumber),
              currencyId: new FormControl(item.currencyId),
              exchangeRate: new FormControl(item.exchangeRate),
              originalDocumentValue: new FormControl(item.originalDocumentValue),
              documentDateGregorian: new FormControl(item.documentDateGregorian),
              documentDateHijri: new FormControl(item.documentDateHijri),
              documentPaidValue: new FormControl(item.documentPaidValue),
              documentPaidValueReceiptCurrency: new FormControl(item.documentPaidValueReceiptCurrency),
            })
          );
        }
      }
    });
    this.bsModalRef.content.documentPaidValue.subscribe(res => {
      formGroup.patchValue({
        value: res / this.cashPayableReceiptForm.value.exchangeRate
      });
    });

    this.bsModalRef.content.downPaymentValue.subscribe(res => {
      formGroup.patchValue({
        downPaymentValue: res,
        advancePaymentBalance: res,
        totalValue: res + formGroup.value.value
      });
    });
  }

  get form() {
    return this.cashPayableReceiptForm.controls;
  }

  get getCashPayableReceiptDetailsListArray() {
    return this.cashPayableReceiptForm.get('cashPayableReceiptDetailsList') as FormArray;
  }

  addNoteDetails() {
    const control = this.cashPayableReceiptForm.get('cashPayableReceiptDetailsList') as FormArray;
    control.push(
      new FormGroup({
        targetAccountId: new FormControl('', Validators.required),
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl(null, Validators.required),
        value: new FormControl(null, Validators.required),
        downPaymentValue: new FormControl(null),
        totalValue: new FormControl(0),
        advancePaymentBalance: new FormControl(0),
        receiptNumber: new FormControl(null),
        receiptOwner: new FormControl(null),
        description: new FormControl(null),
        documentTable: new FormGroup({
          distributedValue: new FormControl(null),
          documentTableList: new FormArray([]),
        }),
        paidTable: new FormArray([])
      })
    );
  }

  deleteNoteDetails(index) {
    const control = this.cashPayableReceiptForm.get('cashPayableReceiptDetailsList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setNoteHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.cashPayableReceiptForm.patchValue({
        hijriDate: {
          year: hijriDate.iYear(),
          month: hijriDate.iMonth() + 1,
          day: hijriDate.iDate()
        }
      });
    }
  }

  setNoteGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.cashPayableReceiptForm.patchValue({
        gregorianDate: this.generalService.format(
          new Date(
            gegorianDate.year(),
            gegorianDate.month(),
            gegorianDate.date()
          )
        )
      });
    }
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  calculateTotalValue(value, downPayment, group: FormGroup) {
    group.patchValue({
      totalValue: +value + +downPayment
    });
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.cashPayableReceiptForm.value.gregorianDate) {
      if (
        typeof this.cashPayableReceiptForm.value.gregorianDate !==
        'string'
      ) {
        this.cashPayableReceiptForm.value.gregorianDate = this.generalService.format(
          this.cashPayableReceiptForm.value.gregorianDate
        );
      }
      if (
        typeof this.cashPayableReceiptForm.value.hijriDate !==
        'string'
      ) {
        this.cashPayableReceiptForm.value.hijriDate = this.generalService.formatHijriDate(
          this.cashPayableReceiptForm.value.hijriDate
        );
      }
    }

    for (
      let i = 0;
      i <= this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList.length - 1;
      i++
    ) {
      if (this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable.length) {
        for (
          let x = 0;
          x <= this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable.length - 1;
          x++
        ) {
          if (this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].dueDateGregorian) {
            if (
              typeof this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].dueDateGregorian !==
              'string'
            ) {
              this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].dueDateGregorian = this.generalService.format(
                this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].dueDateGregorian
              );
            }
            if (
              typeof this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].dueDateHijri !==
              'string'
            ) {
              this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].dueDateHijri =
                this.generalService.formatHijriDate(
                  this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].dueDateHijri
                );
            }
          }
          if (this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].paidDateGregorian) {
            if (
              typeof this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].paidDateGregorian !==
              'string'
            ) {
              this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].paidDateGregorian =
                this.generalService.format(
                  this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].paidDateGregorian
                );
            }
            if (
              typeof this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].paidDateHijri !==
              'string'
            ) {
              this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].paidDateHijri =
                this.generalService.formatHijriDate(
                  this.cashPayableReceiptForm.value.cashPayableReceiptDetailsList[i].paidTable[x].paidDateHijri
                );
            }
          }
        }
      }
    }
    if (this.cashPayableReceipt) {
      if (this.cashPayableReceiptForm.valid) {
        const newCashPayableReceipt = {
          _id: this.cashPayableReceipt._id,
          ...this.generalService.checkEmptyFields(this.cashPayableReceiptForm.value),
          companyId,
          branchId
        };
        this.data.put(cashPayableReceiptsApi, newCashPayableReceipt).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/financialTransactions/cashPayableReceipts']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {

      if (this.cashPayableReceiptForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.cashPayableReceiptForm.value),
          companyId,
          branchId
        };

        this.subscriptions.push(
          this.data.post(cashPayableReceiptsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.cashPayableReceiptForm.reset();
            this.cashPayableReceiptForm.patchValue({
              isActive: true,
              targetAccountType: 'customer',
              status: 'unposted',
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let cashPayableReceiptArabicDescription = '';
    let cashPayableReceiptEnglishDescription = '';
    let fileNumber = null;
    let targetAccountType = 'customer';
    let currencyId = '';
    let exchangeRate = null;
    let totalValue = null;
    let currencyConversionDifference = null;
    let internalReference = '';
    let checkDispose = false;
    let cashPayableReceiptPeriodic = false;
    let status = 'unposted';
    let isActive = true;
    let cashPayableReceiptDetailsListArray = new FormArray([
      new FormGroup({
        targetAccountId: new FormControl('', Validators.required),
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl(null, Validators.required),
        value: new FormControl(0, Validators.required),
        downPaymentValue: new FormControl(0, Validators.required),
        totalValue: new FormControl(null, Validators.required),
        advancePaymentBalance: new FormControl(null, Validators.required),
        receiptNumber: new FormControl(null),
        receiptOwner: new FormControl(null),
        description: new FormControl(null),
        documentTable: new FormGroup({
          distributedValue: new FormControl(null),
          documentTableList: new FormArray([]),
        }),
        paidTable: new FormArray([])
      })
    ]);

    if (this.cashPayableReceipt) {
      code = this.cashPayableReceipt.code;
      gregorianDate = new Date(this.cashPayableReceipt.gregorianDate + 'UTC');
      hijriDate = this.cashPayableReceipt.hijriDate;
      status = this.cashPayableReceipt.status;
      cashPayableReceiptArabicDescription = this.cashPayableReceipt.cashPayableReceiptArabicDescription;
      cashPayableReceiptEnglishDescription = this.cashPayableReceipt.cashPayableReceiptEnglishDescription;
      fileNumber = this.cashPayableReceipt.fileNumber;
      targetAccountType = this.cashPayableReceipt.targetAccountType;
      currencyId = this.cashPayableReceipt.currencyId;
      exchangeRate = this.cashPayableReceipt.exchangeRate;
      totalValue = this.cashPayableReceipt.totalValue;
      currencyConversionDifference = this.cashPayableReceipt.currencyConversionDifference;
      internalReference = this.cashPayableReceipt.internalReference;
      checkDispose = this.cashPayableReceipt.checkDispose;
      cashPayableReceiptPeriodic = this.cashPayableReceipt.cashPayableReceiptPeriodic;
      isActive = this.cashPayableReceipt.isActive;
      cashPayableReceiptDetailsListArray = new FormArray([]);
      for (const item of this.cashPayableReceipt.cashPayableReceiptDetailsList) {
        const documentTableListArray = new FormArray([]);
        for (const innterItem of item.documentTable.documentTableList) {
          documentTableListArray.push(
            new FormGroup({
              documentNumber: new FormControl(innterItem.documentNumber),
              currencyId: new FormControl(innterItem.currencyId),
              exchangeRate: new FormControl(innterItem.exchangeRate),
              originalDocumentValue: new FormControl(innterItem.originalDocumentValue),
              documentBalance: new FormControl(innterItem.documentBalance),
              documentDateGregorian: new FormControl(innterItem.documentDateGregorian),
              documentDateHijri: new FormControl(innterItem.documentDateHijri),
              dueDocumentDateGregorian: new FormControl(innterItem.dueDocumentDateGregorian),
              dueDocumentDateHijri: new FormControl(innterItem.dueDocumentDateHijri),
              documentPaidValue: new FormControl(innterItem.documentPaidValue),
              documentPaidValueReceiptCurrency: new FormControl(innterItem.documentPaidValueReceiptCurrency),
            })
          );
        }
        const paidTableArray = new FormArray([]);
        for (const innterItem of item.paidTable) {
          paidTableArray.push(
            new FormGroup({
              paidMethod: new FormControl(innterItem.paidMethod),
              accountPayments: new FormControl(innterItem.accountPayments),
              currencyId: new FormControl(innterItem.currencyId),
              exchangeRate: new FormControl(innterItem.exchangeRate),
              paidValue: new FormControl(innterItem.paidValue),
              dueDateGregorian: new FormControl(new Date(innterItem.dueDateGregorian + 'UTC')),
              dueDateHijri: new FormControl(innterItem.dueDateHijri),
              paidDateGregorian: new FormControl(new Date(innterItem.paidDateGregorian + 'UTC')),
              paidDateHijri: new FormControl(innterItem.paidDateHijri),
            })
          );
        }
        cashPayableReceiptDetailsListArray.push(
          new FormGroup({
            targetAccountId: new FormControl(item.targetAccountId, Validators.required),
            currencyId: new FormControl(item.currencyId, Validators.required),
            exchangeRate: new FormControl(item.exchangeRate, Validators.required),
            value: new FormControl(item.value, Validators.required),
            downPaymentValue: new FormControl(item.downPaymentValue, Validators.required),
            totalValue: new FormControl(item.totalValue, Validators.required),
            advancePaymentBalance: new FormControl(item.advancePaymentBalance, Validators.required),
            receiptNumber: new FormControl(item.receiptNumber),
            receiptOwner: new FormControl(item.receiptOwner),
            description: new FormControl(item.description),
            documentTable: new FormGroup({
              distributedValue: new FormControl(item.documentTable.distributedValue),
              documentTableList: documentTableListArray,
            }),
            paidTable: paidTableArray
          })
        );
      }
    }

    this.cashPayableReceiptForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      status: new FormControl(status),
      cashPayableReceiptArabicDescription: new FormControl(cashPayableReceiptArabicDescription),
      cashPayableReceiptEnglishDescription: new FormControl(cashPayableReceiptEnglishDescription),
      fileNumber: new FormControl(fileNumber),
      targetAccountType: new FormControl(targetAccountType, Validators.required),
      currencyId: new FormControl(currencyId, Validators.required),
      exchangeRate: new FormControl(exchangeRate),
      totalValue: new FormControl(totalValue),
      currencyConversionDifference: new FormControl(currencyConversionDifference),
      internalReference: new FormControl(internalReference),
      checkDispose: new FormControl(checkDispose),
      cashPayableReceiptPeriodic: new FormControl(cashPayableReceiptPeriodic),
      cashPayableReceiptDetailsList: cashPayableReceiptDetailsListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
