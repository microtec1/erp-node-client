import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, cashPayableReceiptsApi, postCashPayableReceiptsApi, unPostCashPayableReceiptsApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { ICashPayableReceipt } from '../../../interfaces/ICashPayableReceipt';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-cash-payable-receipts',
  templateUrl: './cash-payable-receipts.component.html',
  styleUrls: ['./cash-payable-receipts.component.scss']
})

export class CashPayableReceiptsComponent implements OnInit, OnDestroy {
  cashPayableReceipts: ICashPayableReceipt[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getCashPayableReceiptsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(cashPayableReceiptsApi, pageNumber).subscribe((res: IDataRes) => {
      this.cashPayableReceipts = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(cashPayableReceiptsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.cashPayableReceipts = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCashPayableReceiptsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(cashPayableReceiptsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.cashPayableReceipts = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      cashPayableReceiptArabicDescription: new FormControl(''),
      cashPayableReceiptEnglishDescription: new FormControl('')
    });
  }

  deleteModal(cashPayableReceipt: ICashPayableReceipt) {
    const initialState = {
      code: cashPayableReceipt.code,
      nameAr: cashPayableReceipt.cashPayableReceiptArabicDescription,
      nameEn: cashPayableReceipt.cashPayableReceiptEnglishDescription
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(cashPayableReceipt._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(cashPayableReceiptsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.cashPayableReceipts = this.generalService.removeItem(this.cashPayableReceipts, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postCashPayableReceiptsApi}/${id}`, {}).subscribe(res => {
        this.cashPayableReceipts = this.generalService.changeStatus(this.cashPayableReceipts, id, 'posted', 'status');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostCashPayableReceiptsApi}/${id}`, {}).subscribe(res => {
        this.cashPayableReceipts = this.generalService.changeStatus(this.cashPayableReceipts, id, 'unposted', 'status');
        this.uiService.isLoading.next(false);
      })
    );
  }

  getCashPayableReceiptsFirstPage() {
    this.subscriptions.push(
      this.data.get(cashPayableReceiptsApi, 1).subscribe((res: IDataRes) => {
        this.cashPayableReceipts = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
