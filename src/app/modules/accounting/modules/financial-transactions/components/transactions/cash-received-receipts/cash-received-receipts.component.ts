import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, cashReceivedReceiptsApi, postCashReceivedReceiptsApi, unPostCashReceivedReceiptsApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { ICashReceivedReceipt } from '../../../interfaces/ICashReceivedReceipt';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-cash-received-receipts',
  templateUrl: './cash-received-receipts.component.html',
  styleUrls: ['./cash-received-receipts.component.scss']
})

export class CashReceivedReceiptsComponent implements OnInit, OnDestroy {
  cashReceivedReceipts: ICashReceivedReceipt[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getCashReceivedReceiptsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(cashReceivedReceiptsApi, pageNumber).subscribe((res: IDataRes) => {
      this.cashReceivedReceipts = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(cashReceivedReceiptsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.cashReceivedReceipts = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCashReceivedReceiptsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(cashReceivedReceiptsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.cashReceivedReceipts = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      cashReceivedReceiptArabicDescription: new FormControl(''),
      cashReceivedReceiptEnglishDescription: new FormControl('')
    });
  }

  deleteModal(cashReceivedReceipt: ICashReceivedReceipt) {
    const initialState = {
      code: cashReceivedReceipt.code,
      nameAr: cashReceivedReceipt.cashReceivedReceiptArabicDescription,
      nameEn: cashReceivedReceipt.cashReceivedReceiptEnglishDescription
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(cashReceivedReceipt._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(cashReceivedReceiptsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.cashReceivedReceipts = this.generalService.removeItem(this.cashReceivedReceipts, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postCashReceivedReceiptsApi}/${id}`, {}).subscribe(res => {
        this.cashReceivedReceipts = this.generalService.changeStatus(this.cashReceivedReceipts, id, 'posted', 'status');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostCashReceivedReceiptsApi}/${id}`, {}).subscribe(res => {
        this.cashReceivedReceipts = this.generalService.changeStatus(this.cashReceivedReceipts, id, 'unposted', 'status');
        this.uiService.isLoading.next(false);
      })
    );
  }

  getCashReceivedReceiptsFirstPage() {
    this.subscriptions.push(
      this.data.get(cashReceivedReceiptsApi, 1).subscribe((res: IDataRes) => {
        this.cashReceivedReceipts = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
