import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { searchLength, companyId, branchId } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICreditNote } from '../../../../interfaces/ICreditNote';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PaidTableComponent } from '../../paid-table/paid-table.component';
import { DocumentTableComponent } from '../../document-table/document-table.component';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { DataService } from 'src/app/common/services/shared/data.service';
import { creditNotesApi, branchCurrenciesApi, companyCurrenciesApi, customersApi, suppliersApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-credit-note',
  templateUrl: './add-credit-note.component.html',
  styleUrls: ['./add-credit-note.component.scss']
})
export class AddCreditNoteComponent implements OnInit, OnDestroy {
  creditNoteForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  creditNote: ICreditNote;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  bsModalRef: BsModalRef;
  detailsMode: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Company Currencies
  companyCurrencies: any[] = [];
  companyCurrenciesInputFocused: boolean;
  companyCurrenciesCount: number;
  noCompanyCurrencies: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(creditNotesApi, null, null, id).subscribe((creditNote: ICreditNote) => {
              this.creditNote = creditNote;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.creditNoteForm.disable({ onlySelf: true });
              }
              this.syncControls();
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.syncControls();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(companyCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.companyCurrency.length) {
            this.companyCurrencies.push(...res.companyCurrency);
            this.companyCurrenciesCount = res.companyCurrency.length;
          } else {
            this.noCompanyCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  fillRate(event, formGroup: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        exchangeRate: currency.exchangeRate
      });
    } else {
      this.creditNoteForm.patchValue({
        exchangeRate: currency.exchangeRate
      });
    }
  }

  fillCurrency(event, formGroup: FormGroup) {
    let data;
    if (this.creditNoteForm.value.targetAccountType === 'customer') {
      data = this.customers.find(item => item._id === event);
    } else {
      data = this.suppliers.find(item => item._id === event);
    }
    formGroup.patchValue({
      currencyId: data.currencyId,
      exchangeRate: data.exchangeRate
    });
  }

  syncControls() {
    this.creditNoteForm.controls.creditNotesDetailsList.valueChanges.subscribe(res => {
      const valuesArray = [];
      for (let i = 0; i <= res.length - 1; i++) {
        if (res[i]) {
          valuesArray.push(res[i].value);
        }
      }
      const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);

      this.creditNoteForm.patchValue({
        totalValue: valuesSum
      });
    });
  }

  paidTable(id, currencyId, exchangeRate) {
    const formGroup: any = this.getCreditNotesDetailsListArray.controls.find(item => item.value.targetAccountId === id);
    const currencyName = this.currencies.find(item => item.currencyId === currencyId).currencyNameAr;
    let data;
    if (formGroup.controls.paidTable.controls.length) {
      data = formGroup.controls.paidTable.value;
    }
    const initialState = {
      data,
      detailsMode: this.detailsMode ? this.detailsMode : null,
      currencyName,
      exchangeRate
    };
    this.bsModalRef = this.modalService.show(PaidTableComponent, { initialState });
    this.bsModalRef.content.formValue.subscribe(res => {
      formGroup.controls.paidTable.controls = [];
      for (const item of res.paidTable) {
        formGroup.controls.paidTable.push(
          new FormGroup({
            accountPayments: new FormControl(item.accountPayments),
            currencyId: new FormControl(this.creditNoteForm.value.currencyId),
            exchangeRate: new FormControl(this.creditNoteForm.value.exchangeRate),
            paidValue: new FormControl(item.paidValue),
          })
        );
      }
    });
  }

  documentTable(id, exchangeRate) {
    let data;
    let tableData;
    if (this.creditNoteForm.value.targetAccountType === 'customer') {
      data = this.customers.find(item => item._id === id);
    } else {
      data = this.suppliers.find(item => item._id === id);
    }
    const formGroup: any = this.getCreditNotesDetailsListArray.controls.find(item => item.value.targetAccountId === id);
    if (formGroup.controls.documentTable.value.documentTableList.length) {
      tableData = formGroup.controls.documentTable.value;
    }
    const initialState = {
      data,
      tableData,
      detailsMode: this.detailsMode ? this.detailsMode : null,
      companyCurrencies: this.companyCurrencies,
      originalExchangeRate: exchangeRate
    };
    this.bsModalRef = this.modalService.show(DocumentTableComponent, { initialState });
    this.bsModalRef.content.formValue.subscribe(res => {
      const target = formGroup.controls.documentTable;
      target.patchValue({
        distributedValue: res.documentTable.distributedValue
      });
      target.controls.documentTableList.controls = [];
      for (const item of res.documentTable.documentTableList) {
        target.controls.documentTableList.push(
          new FormGroup({
            documentNumber: new FormControl(item.documentNumber),
            currencyId: new FormControl(item.currencyId),
            exchangeRate: new FormControl(item.exchangeRate),
            originalDocumentValue: new FormControl(item.originalDocumentValue),
            documentDateGregorian: new FormControl(item.documentDateGregorian),
            documentDateHijri: new FormControl(item.documentDateHijri),
            documentPaidValue: new FormControl(item.documentPaidValue),
            documentPaidValueReceiptCurrency: new FormControl(item.documentPaidValueReceiptCurrency),
          })
        );
      }
    });

    this.bsModalRef.content.documentPaidValue.subscribe(res => {
      formGroup.patchValue({
        value: res / this.creditNoteForm.value.exchangeRate
      });
    });
  }

  get form() {
    return this.creditNoteForm.controls;
  }

  get getCreditNotesDetailsListArray() {
    return this.creditNoteForm.get('creditNotesDetailsList') as FormArray;
  }

  addNoteDetails() {
    const control = this.creditNoteForm.get('creditNotesDetailsList') as FormArray;
    control.push(
      new FormGroup({
        targetAccountId: new FormControl('', Validators.required),
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl(null, Validators.required),
        value: new FormControl(null, Validators.required),
        documentTable: new FormGroup({
          distributedValue: new FormControl(null),
          documentTableList: new FormArray([]),
        }),
        paidTable: new FormArray([])
      })
    );
  }

  deleteNoteDetails(index) {
    const control = this.creditNoteForm.get('creditNotesDetailsList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setNoteHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.creditNoteForm.patchValue({
        hijriDate: {
          year: hijriDate.iYear(),
          month: hijriDate.iMonth() + 1,
          day: hijriDate.iDate()
        }
      });
    }
  }

  setNoteGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.creditNoteForm.patchValue({
        gregorianDate: this.generalService.format(
          new Date(
            gegorianDate.year(),
            gegorianDate.month(),
            gegorianDate.date()
          )
        )
      });
    }
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.creditNoteForm.value.gregorianDate) {
      if (
        typeof this.creditNoteForm.value.gregorianDate !==
        'string'
      ) {
        this.creditNoteForm.value.gregorianDate = this.generalService.format(
          this.creditNoteForm.value.gregorianDate
        );
      }
      if (
        typeof this.creditNoteForm.value.hijriDate !==
        'string'
      ) {
        this.creditNoteForm.value.hijriDate = this.generalService.formatHijriDate(
          this.creditNoteForm.value.hijriDate
        );
      }
    }
    if (this.creditNote) {
      if (this.creditNoteForm.valid) {
        const newcreditNote = {
          _id: this.creditNote._id,
          ...this.generalService.checkEmptyFields(this.creditNoteForm.value),
          companyId,
          branchId
        };
        this.data.put(creditNotesApi, newcreditNote).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/financialTransactions/creditNotes']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {

      if (this.creditNoteForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.creditNoteForm.value),
          companyId,
          branchId
        };

        this.subscriptions.push(
          this.data.post(creditNotesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.creditNoteForm.reset();
            this.creditNoteForm.patchValue({
              isActive: true,
              targetAccountType: 'customer',
              status: 'unposted',
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let creditNotesArabicDescription = '';
    let creditNotesEnglishDescription = '';
    let fileNumber = null;
    let targetAccountType = 'customer';
    let currencyId = '';
    let exchangeRate = null;
    let totalValue = null;
    let currencyConversionDifference = null;
    let internalReference = '';
    let creditNotesPeriodic = false;
    let status = 'unposted';
    let isActive = true;
    let creditNotesDetailsListArray = new FormArray([
      new FormGroup({
        targetAccountId: new FormControl('', Validators.required),
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl(null, Validators.required),
        value: new FormControl(null, Validators.required),
        documentTable: new FormGroup({
          distributedValue: new FormControl(null),
          documentTableList: new FormArray([]),
        }),
        paidTable: new FormArray([])
      })
    ]);

    if (this.creditNote) {
      code = this.creditNote.code;
      gregorianDate = new Date(this.creditNote.gregorianDate + 'UTC');
      hijriDate = this.creditNote.hijriDate;
      status = this.creditNote.status;
      creditNotesArabicDescription = this.creditNote.creditNotesArabicDescription;
      creditNotesEnglishDescription = this.creditNote.creditNotesEnglishDescription;
      fileNumber = this.creditNote.fileNumber;
      targetAccountType = this.creditNote.targetAccountType;
      currencyId = this.creditNote.currencyId;
      exchangeRate = this.creditNote.exchangeRate;
      totalValue = this.creditNote.totalValue;
      currencyConversionDifference = this.creditNote.currencyConversionDifference;
      internalReference = this.creditNote.internalReference;
      creditNotesPeriodic = this.creditNote.creditNotesPeriodic;
      isActive = this.creditNote.isActive;
      creditNotesDetailsListArray = new FormArray([]);
      for (const item of this.creditNote.creditNotesDetailsList) {
        const documentTableListArray = new FormArray([]);
        for (const innterItem of item.documentTable.documentTableList) {
          documentTableListArray.push(
            new FormGroup({
              documentNumber: new FormControl(innterItem.documentNumber),
              currencyId: new FormControl(innterItem.currencyId),
              exchangeRate: new FormControl(innterItem.exchangeRate),
              originalDocumentValue: new FormControl(innterItem.originalDocumentValue),
              documentDateGregorian: new FormControl(innterItem.documentDateGregorian),
              documentDateHijri: new FormControl(innterItem.documentDateHijri),
              documentPaidValue: new FormControl(innterItem.documentPaidValue),
              documentPaidValueReceiptCurrency: new FormControl(innterItem.documentPaidValueReceiptCurrency),
            })
          );
        }
        const paidTableArray = new FormArray([]);
        for (const innterItem of item.paidTable) {
          paidTableArray.push(
            new FormGroup({
              accountPayments: new FormControl(innterItem.accountPayments),
              currencyId: new FormControl(innterItem.currencyId),
              exchangeRate: new FormControl(innterItem.exchangeRate),
              paidValue: new FormControl(innterItem.paidValue),
            })
          );
        }
        creditNotesDetailsListArray.push(
          new FormGroup({
            targetAccountId: new FormControl(item.targetAccountId, Validators.required),
            currencyId: new FormControl(item.currencyId, Validators.required),
            exchangeRate: new FormControl(item.exchangeRate, Validators.required),
            value: new FormControl(item.value, Validators.required),
            documentTable: new FormGroup({
              distributedValue: new FormControl(item.documentTable.distributedValue),
              documentTableList: documentTableListArray,
            }),
            paidTable: paidTableArray
          })
        );
      }
    }

    this.creditNoteForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      status: new FormControl(status),
      creditNotesArabicDescription: new FormControl(creditNotesArabicDescription),
      creditNotesEnglishDescription: new FormControl(creditNotesEnglishDescription),
      fileNumber: new FormControl(fileNumber),
      targetAccountType: new FormControl(targetAccountType, Validators.required),
      currencyId: new FormControl(currencyId, Validators.required),
      exchangeRate: new FormControl(exchangeRate),
      totalValue: new FormControl(totalValue),
      currencyConversionDifference: new FormControl(currencyConversionDifference),
      internalReference: new FormControl(internalReference),
      creditNotesPeriodic: new FormControl(creditNotesPeriodic),
      creditNotesDetailsList: creditNotesDetailsListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
