import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, creditNotesApi, postCreditNotesApi, unPostCreditNotesApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { ICreditNote } from '../../../interfaces/ICreditNote';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-credit-notes',
  templateUrl: './credit-notes.component.html',
  styleUrls: ['./credit-notes.component.scss']
})

export class CreditNotesComponent implements OnInit, OnDestroy {
  creditNotes: ICreditNote[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getCreditNotesFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(creditNotesApi, pageNumber).subscribe((res: IDataRes) => {
      this.creditNotes = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(creditNotesApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.creditNotes = res.results;
      this.uiService.isLoading.next(false);
    });
  }


  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCreditNotesFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(creditNotesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.creditNotes = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      creditNotesArabicDescription: new FormControl(''),
      creditNotesEnglishDescription: new FormControl('')
    });
  }

  deleteModal(creditNote: ICreditNote) {
    const initialState = {
      code: creditNote.code,
      nameAr: creditNote.creditNotesArabicDescription,
      nameEn: creditNote.creditNotesEnglishDescription
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(creditNote._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(creditNotesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.creditNotes = this.generalService.removeItem(this.creditNotes, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postCreditNotesApi}/${id}`, {}).subscribe(res => {
        this.creditNotes = this.generalService.changeStatus(this.creditNotes, id, 'posted', 'status');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostCreditNotesApi}/${id}`, {}).subscribe(res => {
        this.creditNotes = this.generalService.changeStatus(this.creditNotes, id, 'unposted', 'status');
        this.uiService.isLoading.next(false);
      })
    );
  }

  getCreditNotesFirstPage() {
    this.subscriptions.push(
      this.data.get(creditNotesApi, 1).subscribe((res: IDataRes) => {
        this.creditNotes = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
