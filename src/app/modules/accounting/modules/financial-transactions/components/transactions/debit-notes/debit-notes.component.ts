import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, debitNotesApi, postDebitNotesApi, unPostDebitNotesApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IDebitNote } from '../../../interfaces/IDebitNote';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-debit-notes',
  templateUrl: './debit-notes.component.html',
  styleUrls: ['./debit-notes.component.scss']
})

export class DebitNotesComponent implements OnInit, OnDestroy {
  debitNotes: IDebitNote[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getDebitNotesFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(debitNotesApi, pageNumber).subscribe((res: IDataRes) => {
      this.debitNotes = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(debitNotesApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.debitNotes = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getDebitNotesFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(debitNotesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.debitNotes = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      debitNotesArabicDescription: new FormControl(''),
      debitNotesEnglishDescription: new FormControl('')
    });
  }

  deleteModal(debitNote: IDebitNote) {
    const initialState = {
      code: debitNote.code,
      nameAr: debitNote.debitNotesArabicDescription,
      nameEn: debitNote.debitNotesEnglishDescription
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(debitNote._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(debitNotesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.debitNotes = this.generalService.removeItem(this.debitNotes, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postDebitNotesApi}/${id}`, {}).subscribe(res => {
        this.debitNotes = this.generalService.changeStatus(this.debitNotes, id, 'posted', 'status');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostDebitNotesApi}/${id}`, {}).subscribe(res => {
        this.debitNotes = this.generalService.changeStatus(this.debitNotes, id, 'unposted', 'status');
        this.uiService.isLoading.next(false);
      })
    );
  }

  getDebitNotesFirstPage() {
    this.subscriptions.push(
      this.data.get(debitNotesApi, 1).subscribe((res: IDataRes) => {
        this.debitNotes = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
