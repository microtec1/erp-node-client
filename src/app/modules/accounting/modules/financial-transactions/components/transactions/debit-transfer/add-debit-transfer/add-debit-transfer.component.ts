import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { IDebitTransfer } from '../../../../interfaces/IDebitTransfer';
import { Subscription } from 'rxjs';
import { companyId, branchId, searchLength } from 'src/app/common/constants/general.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, Params, RouterStateSnapshot } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { DataService } from 'src/app/common/services/shared/data.service';
import { debitTransfersApi, branchCurrenciesApi, customersApi, suppliersApi } from 'src/app/common/constants/api.constants';


@Component({
  selector: 'app-add-debit-transfer',
  templateUrl: './add-debit-transfer.component.html',
  styleUrls: ['./add-debit-transfer.component.scss']
})
export class AddDebitTransferComponent implements OnInit, OnDestroy {
  debitTransferForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  debitTransfer: IDebitTransfer;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  companyId = companyId;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  customersInputFocused2: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  suppliersInputFocused2: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) {

  }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(debitTransfersApi, null, null, id).subscribe((debitTransfer: IDebitTransfer) => {
              this.debitTransfer = debitTransfer;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.debitTransferForm.disable({ onlySelf: true });
              }
              this.syncControls();
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.syncControls();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  fillRate(event, formGroup: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        exchangeRate: currency.exchangeRate
      });
    } else {
      this.debitTransferForm.patchValue({
        exchangeRate: currency.exchangeRate
      });
    }
  }

  loadTransactions(value) {
    const array = this.debitTransferForm.get('debitTransferDetails') as FormArray;
    array.controls = [];
    let targetAccount;
    if (this.debitTransferForm.value.targetedAccount === 'customer') {
      targetAccount = this.customers.find(item => item._id === value);
    } else {
      targetAccount = this.suppliers.find(item => item._id === value);
    }
    for (const item of targetAccount.openingBalances) {
      array.push(
        new FormGroup({
          transactionNumber: new FormControl(null),
          transactionType: new FormControl('openingBalance'),
          transactionDescription: new FormControl(null),
          transactionDateGregorian: new FormControl(item.dueGregorianDate),
          transactionDateHijri: new FormControl(item.dueHijriDate),
          transactionValue: new FormControl(item.balance),
          transactionBalance: new FormControl(item.netBalance),
          transferredAmount: new FormControl(null, Validators.required),
          transferredBalance: new FormControl(null),
        })
      );
    }
  }

  fillTransferredBalance(value, group: FormGroup) {
    group.patchValue({
      transferredBalance: value
    });
  }

  syncControls() {
    this.debitTransferForm.controls.debitTransferDetails.valueChanges.subscribe(res => {
      const valuesArray = [];
      for (let i = 0; i <= res.length - 1; i++) {
        if (res[i]) {
          valuesArray.push(res[i].transferredAmount);
        }
      }
      const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);

      this.debitTransferForm.patchValue({
        totalValue: valuesSum,
        totalTransferredAmount: valuesSum,
        totalTransferredBalance: valuesSum
      });
    });
  }

  get form() {
    return this.debitTransferForm.controls;
  }

  get getDebitTransferDetailsArray() {
    return this.debitTransferForm.get('debitTransferDetails') as FormArray;
  }

  addTransferDetails() {
    const control = this.debitTransferForm.get('debitTransferDetails') as FormArray;
    control.push(
      new FormGroup({
        transactionNumber: new FormControl(null),
        transactionType: new FormControl(''),
        transactionDescription: new FormControl(''),
        transactionDateGregorian: new FormControl(''),
        transactionDateHijri: new FormControl(''),
        transactionValue: new FormControl(null),
        transactionBalance: new FormControl(null),
        transferredAmount: new FormControl(null, Validators.required),
        transferredBalance: new FormControl(null),
      })
    );
  }

  deleteTransferDetails(index) {
    const control = this.debitTransferForm.get('debitNotesDetailsList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setTransferHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.debitTransferForm.patchValue({
        hijriDate: {
          year: hijriDate.iYear(),
          month: hijriDate.iMonth() + 1,
          day: hijriDate.iDate()
        }
      });
    }
  }

  setTransferGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.debitTransferForm.patchValue({
        gregorianDate: this.generalService.format(
          new Date(
            gegorianDate.year(),
            gegorianDate.month(),
            gegorianDate.date()
          )
        )
      });
    }
  }


  setTransactionHijri(value: Date, group: FormGroup) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      group.patchValue({
        transactionDateHijri: {
          year: hijriDate.iYear(),
          month: hijriDate.iMonth() + 1,
          day: hijriDate.iDate()
        }
      });
    }
  }

  setTransactionGeorian(value, group: FormGroup) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      group.patchValue({
        transactionDateGregorian: this.generalService.format(
          new Date(
            gegorianDate.year(),
            gegorianDate.month(),
            gegorianDate.date()
          )
        )
      });
    }
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.debitTransferForm.value.gregorianDate) {
      if (
        typeof this.debitTransferForm.value.gregorianDate !==
        'string'
      ) {
        this.debitTransferForm.value.gregorianDate = this.generalService.format(
          this.debitTransferForm.value.gregorianDate
        );
      }
      if (
        typeof this.debitTransferForm.value.hijriDate !==
        'string'
      ) {
        this.debitTransferForm.value.hijriDate = this.generalService.formatHijriDate(
          this.debitTransferForm.value.hijriDate
        );
      }
    }
    if (this.debitTransfer) {
      if (this.debitTransferForm.valid) {
        const newdebitTransfer = {
          _id: this.debitTransfer._id,
          ...this.generalService.checkEmptyFields(this.debitTransferForm.value),
          companyId,
          branchId
        };
        this.data.put(debitTransfersApi, newdebitTransfer).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/financialTransactions/debitTransfers']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        })
        this.loadingButton = false;
      }
    } else {
      if (this.debitTransferForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.debitTransferForm.value),
          companyId,
          branchId
        };

        this.subscriptions.push(
          this.data.post(debitTransfersApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.debitTransferForm.reset();
            this.getDebitTransferDetailsArray.controls = [];
            this.debitTransferForm.patchValue({
              isActive: true,
              targetedAccount: 'customer',
              debitTransferStatus: 'unposted',
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let gregorianDate = new Date();
    let debitTransferDescriptionAr = '';
    let debitTransferDescriptionEn = '';
    let hijriDate = null;
    let targetedAccount = 'customer';
    let fromTargetedAccountId = '';
    let toTargetedAccountId = null;
    let currencyId = '';
    let exchangeRate = null;
    let totalValue = null;
    let internalReference = '';
    let totalTransferredBalance = null;
    let totalTransferredAmount = null;
    let debitTransferStatus = 'unposted';
    let isActive = true;
    let debitTransferDetailsArray = new FormArray([]);

    if (this.debitTransfer) {
      code = this.debitTransfer.code;
      gregorianDate = new Date(this.debitTransfer.gregorianDate + 'UTC');
      hijriDate = this.debitTransfer.hijriDate;
      debitTransferDescriptionAr = this.debitTransfer.debitTransferDescriptionAr;
      debitTransferDescriptionEn = this.debitTransfer.debitTransferDescriptionEn;
      debitTransferStatus = this.debitTransfer.debitTransferStatus;
      targetedAccount = this.debitTransfer.targetedAccount;
      fromTargetedAccountId = this.debitTransfer.fromTargetedAccountId;
      toTargetedAccountId = this.debitTransfer.toTargetedAccountId;
      currencyId = this.debitTransfer.currencyId;
      exchangeRate = this.debitTransfer.exchangeRate;
      totalValue = this.debitTransfer.totalValue;
      internalReference = this.debitTransfer.internalReference;
      totalTransferredBalance = this.debitTransfer.totalTransferredBalance;
      totalTransferredAmount = this.debitTransfer.totalTransferredAmount;
      isActive = this.debitTransfer.isActive;
      debitTransferDetailsArray = new FormArray([]);
      for (const item of this.debitTransfer.debitTransferDetails) {
        debitTransferDetailsArray.push(
          new FormGroup({
            transactionNumber: new FormControl(item.transactionNumber),
            transactionType: new FormControl(item.transactionType),
            transactionDescription: new FormControl(item.transactionDescription),
            transactionDateGregorian: new FormControl(item.transactionDateGregorian),
            transactionDateHijri: new FormControl(item.transactionDateHijri),
            transactionValue: new FormControl(item.transactionValue),
            transactionBalance: new FormControl(item.transactionBalance),
            transferredAmount: new FormControl(item.transferredAmount, Validators.required),
            transferredBalance: new FormControl(item.transferredBalance)
          })
        );
      }
    }

    this.debitTransferForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      debitTransferDescriptionAr: new FormControl(debitTransferDescriptionAr),
      debitTransferDescriptionEn: new FormControl(debitTransferDescriptionEn),
      debitTransferStatus: new FormControl(debitTransferStatus),
      targetedAccount: new FormControl(targetedAccount, Validators.required),
      fromTargetedAccountId: new FormControl(fromTargetedAccountId, Validators.required),
      toTargetedAccountId: new FormControl(toTargetedAccountId, Validators.required),
      currencyId: new FormControl(currencyId, Validators.required),
      exchangeRate: new FormControl(exchangeRate),
      totalValue: new FormControl(totalValue),
      internalReference: new FormControl(internalReference),
      totalTransferredBalance: new FormControl(totalTransferredBalance),
      totalTransferredAmount: new FormControl(totalTransferredAmount),
      debitTransferDetails: debitTransferDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
