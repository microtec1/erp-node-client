import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, debitTransfersApi, postDebitTransfersApi, unPostDebitTransfersApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IDebitTransfer } from '../../../interfaces/IDebitTransfer';
import { DataService } from 'src/app/common/services/shared/data.service';


@Component({
  selector: 'app-debit-transfer',
  templateUrl: './debit-transfer.component.html',
  styleUrls: ['./debit-transfer.component.scss']
})
export class DebitTransferComponent implements OnInit, OnDestroy {
  debitTransfers: IDebitTransfer[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getDebitTransfersFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(debitTransfersApi, pageNumber).subscribe((res: IDataRes) => {
      this.debitTransfers = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(debitTransfersApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.debitTransfers = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getDebitTransfersFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(debitTransfersApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.debitTransfers = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      debitTransferDescriptionAr: new FormControl(''),
      debitTransferDescriptionEn: new FormControl('')
    });
  }

  deleteModal(debitTransfer: IDebitTransfer) {
    const initialState = {
      code: debitTransfer.code,
      nameAr: debitTransfer.debitTransferDescriptionAr,
      nameEn: debitTransfer.debitTransferDescriptionEn,
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(debitTransfer._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(debitTransfersApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.debitTransfers = this.generalService.removeItem(this.debitTransfers, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getDebitTransfersFirstPage() {
    this.subscriptions.push(
      this.data.get(debitTransfersApi, 1).subscribe((res: IDataRes) => {
        this.debitTransfers = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }


  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postDebitTransfersApi}/${id}`, {}).subscribe(res => {
        this.debitTransfers = this.generalService.changeStatus(this.debitTransfers, id, 'posted', 'debitTransferStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostDebitTransfersApi}/${id}`, {}).subscribe(res => {
        this.debitTransfers = this.generalService.changeStatus(this.debitTransfers, id, 'unposted', 'debitTransferStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

}
