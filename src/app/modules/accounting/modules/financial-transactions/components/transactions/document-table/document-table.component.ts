import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';

@Component({
  selector: 'app-document-table',
  templateUrl: './document-table.component.html',
  styleUrls: ['./document-table.component.scss']
})

export class DocumentTableComponent implements OnInit, OnDestroy {
  data: ICustomer;
  tableData: any;
  detailsMode: boolean;
  type: string;
  companyCurrencies: any[];
  originalExchangeRate: number;
  documentTableForm: FormGroup;
  subscriptions: Subscription[] = [];
  distributedValue = 0;
  @Output() formValue: EventEmitter<any> = new EventEmitter();
  @Output() downPaymentValue: EventEmitter<any> = new EventEmitter();
  @Output() documentPaidValue: EventEmitter<any> = new EventEmitter();

  constructor(
    public bsModalRef: BsModalRef,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.initForm();
    if (this.tableData) {
      if (this.tableData.distributedValue > 0) {
        this.distributeValue(this.tableData.distributedValue);
      }
    }
    if (this.detailsMode) {
      this.documentTableForm.disable({ onlySelf: true });
    }
  }

  get getDocumentTableListArray() {
    return this.documentTableForm.controls.documentTable.get('documentTableList') as FormArray;
  }

  submit() {
    this.formValue.emit(this.documentTableForm.value);
    const valuesArray = [];
    for (const item of this.documentTableForm.value.documentTable.documentTableList) {
      valuesArray.push(item.documentPaidValue * item.exchangeRate);
    }
    const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);
    this.documentPaidValue.emit(valuesSum);
    if ((this.distributedValue * this.originalExchangeRate) > valuesSum) {
      this.downPaymentValue.emit((this.distributedValue - valuesSum) / this.originalExchangeRate );
    } else {
      this.downPaymentValue.emit(0);
    }
    this.bsModalRef.hide();
  }

  getReceiptCurrency(value, formGroup) {
    const valueExchangeRate = formGroup.value.exchangeRate;
    formGroup.patchValue({
      documentPaidValueReceiptCurrency: value * valueExchangeRate / this.originalExchangeRate
    });
  }

  distributeValue(value) {
    let disValue;
    disValue = value * this.originalExchangeRate;
    this.distributedValue = disValue;
    for (const item of this.getDocumentTableListArray.controls) {
      if (disValue >= (item.value.originalDocumentValue * item.value.exchangeRate)) {
        this.getReceiptCurrency(item.value.originalDocumentValue, item);
        disValue = disValue - (item.value.originalDocumentValue * item.value.exchangeRate);
        item.patchValue({
          documentPaidValue: item.value.originalDocumentValue
        });
      } else {
        this.getReceiptCurrency((disValue / item.value.exchangeRate), item);
        item.patchValue({
          documentPaidValue: disValue / item.value.exchangeRate
        });
      }
    }
  }

  private initForm() {
    let documentTableListArray = new FormArray([]);
    const dataSource = this.data.openingBalances;
    documentTableListArray = new FormArray([]);
    for (const [i, item] of dataSource.entries()) {
      let documentPaidValue;
      let documentPaidValueReceiptCurrency;
      if (this.tableData) {
        documentPaidValue = this.tableData.documentTableList[i].documentPaidValue;
        documentPaidValueReceiptCurrency = this.tableData.documentTableList[i].documentPaidValueReceiptCurrency;
      }
      if (this.type === 'receipt') {
        documentTableListArray.push(
          new FormGroup({
            documentNumber: new FormControl((i + 1).toString()),
            currencyId: new FormControl(item.currencyId),
            exchangeRate: new FormControl(item.exchangeRate),
            originalDocumentValue: new FormControl(item.balance),
            documentBalance: new FormControl(item.netBalance),
            documentDateGregorian: new FormControl(item.dueGregorianDate),
            documentDateHijri: new FormControl(item.dueHijriDate),
            dueDocumentDateGregorian: new FormControl(item.dueGregorianDate),
            dueDocumentDateHijri: new FormControl(item.dueHijriDate),
            documentPaidValue: new FormControl(documentPaidValue),
            documentPaidValueReceiptCurrency: new FormControl(documentPaidValueReceiptCurrency),
          })
        );
      } else {
        documentTableListArray.push(
          new FormGroup({
            documentNumber: new FormControl((i + 1).toString()),
            currencyId: new FormControl(item.currencyId),
            exchangeRate: new FormControl(item.exchangeRate),
            originalDocumentValue: new FormControl(item.balance),
            documentDateGregorian: new FormControl(item.dueGregorianDate),
            documentDateHijri: new FormControl(item.dueHijriDate),
            documentPaidValue: new FormControl(documentPaidValue),
            documentPaidValueReceiptCurrency: new FormControl(documentPaidValueReceiptCurrency),
          })
        );
      }
    }

    const documentTableGroup = new FormGroup({
      distributedValue: new FormControl(0),
      documentTableList: documentTableListArray
    });

    if (this.tableData) {
      documentTableGroup.patchValue({
        distributedValue: this.tableData.distributedValue
      });
    }

    this.documentTableForm = new FormGroup({
      documentTable: documentTableGroup
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
