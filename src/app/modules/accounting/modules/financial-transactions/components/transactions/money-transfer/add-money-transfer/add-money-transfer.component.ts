import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { searchLength, companyId, branchId } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IMoneyTransfer } from '../../../../interfaces/IMoneyTransfer';
import { IBank } from '../../../../interfaces/IBank';
import { IBox } from '../../../../interfaces/IBox';
import { DataService } from 'src/app/common/services/shared/data.service';
import { moneyTransfersApi, branchCurrenciesApi, banksApi, safeBoxesApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-money-transfer',
  templateUrl: './add-money-transfer.component.html',
  styleUrls: ['./add-money-transfer.component.scss']
})
export class AddMoneyTransferComponent implements OnInit, OnDestroy {
  moneyTransferForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  moneyTransfer: IMoneyTransfer;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  companyId = companyId;
  branchId = branchId;
  detailsMode: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;


  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(moneyTransfersApi, null, null, id).subscribe((moneyTransfer: IMoneyTransfer) => {
              this.moneyTransfer = moneyTransfer;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.moneyTransferForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoresafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.moneyTransferForm.controls;
  }

  get getMoneyTransferDetailsArray() {
    return this.moneyTransferForm.get('moneyTransferDetails') as FormArray;
  }

  addTransfer() {
    const control = this.moneyTransferForm.get('moneyTransferDetails') as FormArray;
    control.push(
      new FormGroup({
        accountType: new FormControl('bank', Validators.required),
        accountId: new FormControl('', Validators.required),
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl(null, Validators.required),
        amountOfMoney: new FormControl(null, Validators.required),
        transferredAccountType: new FormControl('bank', Validators.required),
        transferredAccountId: new FormControl('', Validators.required),
        transferredCurrencyId: new FormControl('', Validators.required),
        transferredExchangeRate: new FormControl(null, Validators.required),
        transferredValue: new FormControl(null, Validators.required),
        expensesBankValue: new FormControl(null),
        descriptionAr: new FormControl(''),
        descriptionEn: new FormControl(''),
      })
    );
  }

  deleteTransfer(index) {
    const control = this.moneyTransferForm.get('moneyTransferDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setTransferHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.moneyTransferForm.patchValue({
        hijriDate: {
          year: hijriDate.iYear(),
          month: hijriDate.iMonth() + 1,
          day: hijriDate.iDate()
        }
      });
    }
  }

  setTransferGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.moneyTransferForm.patchValue({
        gregorianDate: this.generalService.format(
          new Date(
            gegorianDate.year(),
            gegorianDate.month(),
            gegorianDate.date()
          )
        )
      });
    }
  }

  getCurrency(selected, formGroup: FormGroup, input, type) {
    if (input === 'bank') {
      const bank = this.banks.find(item => item._id === selected);
      const currency = this.currencies.find(item => item.currencyId === bank.currencyId);
      if (type === 'sender') {
        formGroup.patchValue({
          currencyId: currency.currencyId,
          exchangeRate: currency.exchangeRate
        });
      }
      if (type === 'receiver') {
        formGroup.patchValue({
          transferredCurrencyId: currency.currencyId,
          transferredExchangeRate: currency.exchangeRate
        });
      }
    }
    if (input === 'safeBox') {
      const safeBox = this.safeBoxes.find(item => item._id === selected);
      const currency = this.currencies.find(item => item.currencyId === safeBox.currencyId);
      if (type === 'sender') {
        formGroup.patchValue({
          currencyId: currency.currencyId,
          exchangeRate: currency.exchangeRate
        });
      }
      if (type === 'receiver') {
        formGroup.patchValue({
          transferredCurrencyId: currency.currencyId,
          transferredExchangeRate: currency.exchangeRate
        });
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.moneyTransferForm.value.gregorianDate) {
      if (
        typeof this.moneyTransferForm.value.gregorianDate !==
        'string'
      ) {
        this.moneyTransferForm.value.gregorianDate = this.generalService.format(
          this.moneyTransferForm.value.gregorianDate
        );
      }
      if (
        typeof this.moneyTransferForm.value.hijriDate !==
        'string'
      ) {
        this.moneyTransferForm.value.hijriDate = this.generalService.formatHijriDate(
          this.moneyTransferForm.value.hijriDate
        );
      }
    }
    if (this.moneyTransfer) {
      if (this.moneyTransferForm.valid) {
        const newMoneyTransfer = {
          _id: this.moneyTransfer._id,
          ...this.generalService.checkEmptyFields(this.moneyTransferForm.value)
        };
        this.data.put(moneyTransfersApi, newMoneyTransfer).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/financialTransactions/moneyTransfers']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {

      if (this.moneyTransferForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.moneyTransferForm.value)
        };
        this.subscriptions.push(
          this.data.post(moneyTransfersApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.moneyTransferForm.reset();
            this.moneyTransferForm.patchValue({
              isActive: true,
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let moneyTransferStatus = 'unposted';
    let moneyTransferDescriptionAr = '';
    let moneyTransferDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let moneyTransferDetailsArray = new FormArray([
      new FormGroup({
        accountType: new FormControl('bank', Validators.required),
        accountId: new FormControl('', Validators.required),
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl(null, Validators.required),
        amountOfMoney: new FormControl(null, Validators.required),
        transferredAccountType: new FormControl('bank', Validators.required),
        transferredAccountId: new FormControl('', Validators.required),
        transferredCurrencyId: new FormControl('', Validators.required),
        transferredExchangeRate: new FormControl(null, Validators.required),
        transferredValue: new FormControl(null, Validators.required),
        expensesBankValue: new FormControl(null),
        descriptionAr: new FormControl(''),
        descriptionEn: new FormControl(''),
      })
    ]);
    let isActive = true;

    if (this.moneyTransfer) {
      code = this.moneyTransfer.code;
      moneyTransferStatus = this.moneyTransfer.moneyTransferStatus;
      moneyTransferDescriptionAr = this.moneyTransfer.moneyTransferDescriptionAr;
      moneyTransferDescriptionEn = this.moneyTransfer.moneyTransferDescriptionEn;
      gregorianDate = new Date(this.moneyTransfer.gregorianDate + 'UTC');
      hijriDate = this.moneyTransfer.hijriDate;
      isActive = this.moneyTransfer.isActive;
      moneyTransferDetailsArray = new FormArray([]);
      for (const control of this.moneyTransfer.moneyTransferDetails) {
        moneyTransferDetailsArray.push(
          new FormGroup({
            accountType: new FormControl(control.accountType),
            accountId: new FormControl(control.accountId),
            currencyId: new FormControl(control.currencyId),
            exchangeRate: new FormControl(control.exchangeRate),
            amountOfMoney: new FormControl(control.amountOfMoney),
            transferredAccountType: new FormControl(control.transferredAccountType),
            transferredAccountId: new FormControl(control.transferredAccountId),
            transferredCurrencyId: new FormControl(control.transferredCurrencyId),
            transferredExchangeRate: new FormControl(control.transferredExchangeRate),
            transferredValue: new FormControl(control.transferredValue - control.expensesBankValue),
            expensesBankValue: new FormControl(control.expensesBankValue),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn)
          })
        );
      }
    }
    this.moneyTransferForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      moneyTransferStatus: new FormControl(moneyTransferStatus),
      moneyTransferDescriptionAr: new FormControl(moneyTransferDescriptionAr),
      moneyTransferDescriptionEn: new FormControl(moneyTransferDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      moneyTransferDetails: moneyTransferDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
