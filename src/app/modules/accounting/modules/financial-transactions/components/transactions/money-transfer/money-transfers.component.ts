import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { branchId, companyId } from 'src/app/common/constants/general.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, moneyTransfersApi, postMoneyTransfersApi, unPostMoneyTransfersApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IMoneyTransfer } from '../../../interfaces/IMoneyTransfer';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-money-transfers',
  templateUrl: './money-transfers.component.html',
  styleUrls: ['./money-transfers.component.scss']
})
export class MoneyTransfersComponent implements OnInit, OnDestroy {
  moneyTransfers: IMoneyTransfer[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;
  branchId = branchId;
  companyId = companyId;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getMoneyTransfersFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(moneyTransfersApi, pageNumber).subscribe((res: IDataRes) => {
      this.moneyTransfers = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(moneyTransfersApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.moneyTransfers = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postMoneyTransfersApi}/${id}`, {}).subscribe(res => {
        this.moneyTransfers = this.generalService.changeStatus(this.moneyTransfers, id, 'posted', 'moneyTransferStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostMoneyTransfersApi}/${id}`, {}).subscribe(res => {
        this.moneyTransfers = this.generalService.changeStatus(this.moneyTransfers, id, 'unposted', 'moneyTransferStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value),
      companyId,
      branchId
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getMoneyTransfersFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(moneyTransfersApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.moneyTransfers = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      moneyTransferDescriptionAr: new FormControl(''),
      moneyTransferDescriptionEn: new FormControl('')
    });
  }

  deleteModal(moneyTransfer: IMoneyTransfer) {
    const initialState = {
      code: moneyTransfer.code,
      nameAr: moneyTransfer.moneyTransferDescriptionAr,
      nameEn: moneyTransfer.moneyTransferDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(moneyTransfer._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(moneyTransfersApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.moneyTransfers = this.generalService.removeItem(this.moneyTransfers, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getMoneyTransfersFirstPage() {
    this.subscriptions.push(
      this.data.get(moneyTransfersApi, 1).subscribe((res: IDataRes) => {
        this.moneyTransfers = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
