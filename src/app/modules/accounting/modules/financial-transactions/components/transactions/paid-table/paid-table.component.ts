import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { IChartOfAccount } from '../../../../gl/interfaces/IChartOfAccount';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IBank } from '../../../interfaces/IBank';
import { IBox } from '../../../interfaces/IBox';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { searchLength, companyId } from 'src/app/common/constants/general.constants';
import { detailedChartOfAccountsApi, safeBoxesApi, banksApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-paid-table',
  templateUrl: './paid-table.component.html',
  styleUrls: ['./paid-table.component.scss']
})
export class PaidTableComponent implements OnInit, OnDestroy {
  data: any;
  detailsMode: boolean;
  currencyName: string;
  exchangeRate: number;
  paidTableForm: FormGroup;
  type: string;
  subscriptions: Subscription[] = [];
  @Output() formValue: EventEmitter<any> = new EventEmitter();

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;


  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  constructor(
    public bsModalRef: BsModalRef,
    private uiService: UiService,
    private dataService: DataService,
    private generalService: GeneralService,
  ) { }

  ngOnInit() {
    this.initForm();
    if (this.detailsMode) {
      this.paidTableForm.disable({ onlySelf: true });
    }
    this.subscriptions.push(
      this.dataService.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
    const searchValues = {
      isActive: true
    };
    this.subscriptions.push(
      this.dataService.get(safeBoxesApi, null, searchValues).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.dataService.get(banksApi, null, searchValues).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  get getPaidTableArray() {
    return this.paidTableForm.get('paidTable') as FormArray;
  }

  addPaidAccount() {
    const control = this.paidTableForm.get('paidTable') as FormArray;
    if (this.type === 'receipt') {
      control.push(
        new FormGroup({
          paidMethod: new FormControl('cash'),
          accountPayments: new FormControl(''),
          currencyId: new FormControl(''),
          exchangeRate: new FormControl(null),
          paidValue: new FormControl(null),
          dueDateGregorian: new FormControl(null),
          dueDateHijri: new FormControl(null),
          paidDateGregorian: new FormControl(null),
          paidDateHijri: new FormControl(null),
        })
      );
      return;
    }
    control.push(new FormGroup({
      accountPayments: new FormControl(''),
      currencyId: new FormControl(''),
      exchangeRate: new FormControl(null),
      paidValue: new FormControl(null)
    }));
  }

  deletePaidAccount(index) {
    const control = this.paidTableForm.get('paidTable') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.paidTableForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.paidTableForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }
    }
  }

  submit() {
    this.formValue.emit(this.paidTableForm.value);
    this.bsModalRef.hide();
  }

  private initForm() {
    let paidTableArray = new FormArray([
      new FormGroup({
        accountPayments: new FormControl(''),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        paidValue: new FormControl(null),
      })
    ]);

    if (this.type === 'receipt') {
      paidTableArray = new FormArray([
        new FormGroup({
          paidMethod: new FormControl('cash'),
          accountPayments: new FormControl(''),
          currencyId: new FormControl(''),
          exchangeRate: new FormControl(null),
          paidValue: new FormControl(null),
          dueDateGregorian: new FormControl(new Date()),
          dueDateHijri: new FormControl(null),
          paidDateGregorian: new FormControl(new Date()),
          paidDateHijri: new FormControl(null),
        })
      ]);
    }
    if (this.data) {
      if (this.type === 'receipt') {
        paidTableArray = new FormArray([]);
        for (const control of this.data) {
          paidTableArray.push(
            new FormGroup({
              paidMethod: new FormControl(control.paidMethod),
              accountPayments: new FormControl(control.accountPayments),
              currencyId: new FormControl(control.currencyId),
              exchangeRate: new FormControl(control.exchangeRate),
              paidValue: new FormControl(control.paidValue),
              dueDateGregorian: new FormControl(control.dueDateGregorian && new Date(control.dueDateGregorian + 'UTC')),
              dueDateHijri: new FormControl(control.dueDateHijri),
              paidDateGregorian: new FormControl(control.paidDateGregorian && new Date(control.paidDateGregorian + 'UTC')),
              paidDateHijri: new FormControl(control.paidDateHijri),
            })
          );
        }
      } else {
        paidTableArray = new FormArray([]);
        for (const control of this.data) {
          paidTableArray.push(
            new FormGroup({
              accountPayments: new FormControl(control.accountPayments),
              currencyId: new FormControl(control.currencyId),
              exchangeRate: new FormControl(control.exchangeRate),
              paidValue: new FormControl(control.paidValue),
            })
          );
        }
      }
    }
    this.paidTableForm = new FormGroup({
      paidTable: paidTableArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
