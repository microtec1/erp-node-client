import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BanksComponent } from './components/files/banks/banks.component';
import { AddBankComponent } from './components/files/banks/add-bank/add-bank.component';
import { SafeBoxesComponent } from './components/files/safe-boxes/safe-boxes.component';
import { AddSafeBoxComponent } from './components/files/safe-boxes/add-safe-box/add-safe-box.component';
import { MoneyTransfersComponent } from './components/transactions/money-transfer/money-transfers.component';
import { AddMoneyTransferComponent } from './components/transactions/money-transfer/add-money-transfer/add-money-transfer.component';
import { CreditNotesComponent } from './components/transactions/credit-notes/credit-notes.component';
import { AddCreditNoteComponent } from './components/transactions/credit-notes/add-credit-note/add-credit-note.component';
import { DebitNotesComponent } from './components/transactions/debit-notes/debit-notes.component';
import { AddDebitNoteComponent } from './components/transactions/debit-notes/add-debit-note/add-debit-note.component';
import { DebitTransferComponent } from './components/transactions/debit-transfer/debit-transfer.component';
import { AddDebitTransferComponent } from './components/transactions/debit-transfer/add-debit-transfer/add-debit-transfer.component';
import { CashPayableReceiptsComponent } from './components/transactions/cash-payable-receipts/cash-payable-receipts.component';
// tslint:disable-next-line:max-line-length
import { AddCashPayableReceiptComponent } from './components/transactions/cash-payable-receipts/add-cash-payable-receipt/add-cash-payable-receipt.component';
import { CashReceivedReceiptsComponent } from './components/transactions/cash-received-receipts/cash-received-receipts.component';
// tslint:disable-next-line:max-line-length
import { AddCashReceivedReceiptComponent } from './components/transactions/cash-received-receipts/add-cash-received-receipt/add-cash-received-receipt.component';
import { AdjustmentsComponent } from './components/transactions/adjustments/adjustments.component';
import { AddAdjustmentComponent } from './components/transactions/adjustments/add-adjustment/add-adjustment.component';


const routes: Routes = [
  { path: '', redirectTo: 'banks', pathMatch: 'full' },
  { path: 'banks', component: BanksComponent },
  { path: 'banks/add', component: AddBankComponent },
  { path: 'banks/edit/:id', component: AddBankComponent },
  { path: 'banks/details/:id', component: AddBankComponent },
  { path: 'safeBoxes', component: SafeBoxesComponent },
  { path: 'safeBoxes/add', component: AddSafeBoxComponent },
  { path: 'safeBoxes/edit/:id', component: AddSafeBoxComponent },
  { path: 'safeBoxes/details/:id', component: AddSafeBoxComponent },
  { path: 'moneyTransfers', component: MoneyTransfersComponent },
  { path: 'moneyTransfers/add', component: AddMoneyTransferComponent },
  { path: 'moneyTransfers/edit/:id', component: AddMoneyTransferComponent },
  { path: 'moneyTransfers/details/:id', component: AddMoneyTransferComponent },
  { path: 'creditNotes', component: CreditNotesComponent },
  { path: 'creditNotes/add', component: AddCreditNoteComponent },
  { path: 'creditNotes/edit/:id', component: AddCreditNoteComponent },
  { path: 'creditNotes/details/:id', component: AddCreditNoteComponent },
  { path: 'debitNotes', component: DebitNotesComponent },
  { path: 'debitNotes/add', component: AddDebitNoteComponent },
  { path: 'debitNotes/edit/:id', component: AddDebitNoteComponent },
  { path: 'debitNotes/details/:id', component: AddDebitNoteComponent },
  { path: 'debitTransfers', component: DebitTransferComponent },
  { path: 'debitTransfers/add', component: AddDebitTransferComponent },
  { path: 'debitTransfers/edit/:id', component: AddDebitTransferComponent },
  { path: 'debitTransfers/details/:id', component: AddDebitTransferComponent },
  { path: 'cashPayableReceipts', component: CashPayableReceiptsComponent },
  { path: 'cashPayableReceipts/add', component: AddCashPayableReceiptComponent },
  { path: 'cashPayableReceipts/edit/:id', component: AddCashPayableReceiptComponent },
  { path: 'cashPayableReceipts/details/:id', component: AddCashPayableReceiptComponent },
  { path: 'cashReceivedReceipts', component: CashReceivedReceiptsComponent },
  { path: 'cashReceivedReceipts/add', component: AddCashReceivedReceiptComponent },
  { path: 'cashReceivedReceipts/edit/:id', component: AddCashReceivedReceiptComponent },
  { path: 'cashReceivedReceipts/details/:id', component: AddCashReceivedReceiptComponent },
  { path: 'adjustments', component: AdjustmentsComponent },
  { path: 'adjustments/add', component: AddAdjustmentComponent },
  { path: 'adjustments/edit/:id', component: AddAdjustmentComponent },
  { path: 'adjustments/details/:id', component: AddAdjustmentComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancialTransactionsRoutingModule { }
