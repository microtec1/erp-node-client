import { NgModule } from '@angular/core';
import { BanksComponent } from './components/files/banks/banks.component';
import { AddBankComponent } from './components/files/banks/add-bank/add-bank.component';
import { SafeBoxesComponent } from './components/files/safe-boxes/safe-boxes.component';
import { AddSafeBoxComponent } from './components/files/safe-boxes/add-safe-box/add-safe-box.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { FinancialTransactionsRoutingModule } from './financial-transactions-routing.module';
import { MoneyTransfersComponent } from './components/transactions/money-transfer/money-transfers.component';
import { AddMoneyTransferComponent } from './components/transactions/money-transfer/add-money-transfer/add-money-transfer.component';
import { CreditNotesComponent } from './components/transactions/credit-notes/credit-notes.component';
import { AddCreditNoteComponent } from './components/transactions/credit-notes/add-credit-note/add-credit-note.component';
import { PaidTableComponent } from './components/transactions/paid-table/paid-table.component';
import { DocumentTableComponent } from './components/transactions/document-table/document-table.component';
import { DebitNotesComponent } from './components/transactions/debit-notes/debit-notes.component';
import { AddDebitNoteComponent } from './components/transactions/debit-notes/add-debit-note/add-debit-note.component';
import { DebitTransferComponent } from './components/transactions/debit-transfer/debit-transfer.component';
import { AddDebitTransferComponent } from './components/transactions/debit-transfer/add-debit-transfer/add-debit-transfer.component';
import { CashPayableReceiptsComponent } from './components/transactions/cash-payable-receipts/cash-payable-receipts.component';
// tslint:disable-next-line:max-line-length
import { AddCashPayableReceiptComponent } from './components/transactions/cash-payable-receipts/add-cash-payable-receipt/add-cash-payable-receipt.component';
import { CashReceivedReceiptsComponent } from './components/transactions/cash-received-receipts/cash-received-receipts.component';
// tslint:disable-next-line:max-line-length
import { AddCashReceivedReceiptComponent } from './components/transactions/cash-received-receipts/add-cash-received-receipt/add-cash-received-receipt.component';
import { AdjustmentsComponent } from './components/transactions/adjustments/adjustments.component';
import { AddAdjustmentComponent } from './components/transactions/adjustments/add-adjustment/add-adjustment.component';


@NgModule({
  declarations: [
    BanksComponent,
    AddBankComponent,
    SafeBoxesComponent,
    AddSafeBoxComponent,
    MoneyTransfersComponent,
    AddMoneyTransferComponent,
    CreditNotesComponent,
    AddCreditNoteComponent,
    PaidTableComponent,
    DocumentTableComponent,
    DebitNotesComponent,
    AddDebitNoteComponent,
    DebitTransferComponent,
    AddDebitTransferComponent,
    CashPayableReceiptsComponent,
    AddCashPayableReceiptComponent,
    CashReceivedReceiptsComponent,
    AddCashReceivedReceiptComponent,
    AdjustmentsComponent,
    AddAdjustmentComponent
  ],
  imports: [
    SharedModule,
    FinancialTransactionsRoutingModule
  ],
  entryComponents: [
    PaidTableComponent,
    DocumentTableComponent
  ]
})
export class FinancialTransactionsModule { }
