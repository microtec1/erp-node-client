export interface IAdjustment {
  _id: string;
  image: string;
  code: string;
  gregorianDate: string;
  hijriDate: string;
  status: string;
  adjustmentArabicDescription: string;
  adjustmentEnglishDescription: string;
  adjustmentType: string;
  supplierId: string;
  customerId: string;
  currencyId: string;
  exchangeRate: number;
  totalValue: number;
  isActive: boolean;
  invoicesTable: [
    {
      invoiceId: string;
      invoiceItemId: string;
      invoiceType: string;
      documentNumber: string;
      currencyId: string;
      exchangeRate: number;
      descriptionStatement: string;
      dateGregorian: string;
      dateHijri: string;
      sourceValue: number;
      balance: number;
      amountPaid: number;
      amountAdjustmentCurrency: number;
      differenceValue: number;
    }
  ];
  debenturesTable: [
    {
      debentureId: string;
      debentureItemId: string;
      debentureType: string;
      documentNumber: string;
      currencyId: string;
      exchangeRate: number;
      descriptionStatement: string;
      dateGregorian: string;
      dateHijri: string;
      sourceValue: number;
      balance: number;
      amountPaid: number;
      amountAdjustmentCurrency: number;
      differenceValue: number;
    }
  ];
}
