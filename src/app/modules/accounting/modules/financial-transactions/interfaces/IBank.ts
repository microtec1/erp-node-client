export interface IBank {
  _id: string;
  code: string;
  bankNameAr: string;
  bankNameEn: string;
  ibanAccountNumber: string;
  glAccountId: string;
  currencyId: string;
  allBranches: boolean;
  branchesId: string[];
  expensesAccountId: string;
  underCollectionChecksAccountId: string;
  underPaymentChecksAccountId: string;
  creditCardCommissionRatio: string;
  creditCardMaximumLimit: string;
  masterCardCommissionRatio: string;
  masterCardMaximumLimit: string;
  networkCommissionRatio: string;
  networkMaximumLimit: string;
  bankAddress: string;
  bankResponsible: string;
  telephoneNumber: string;
  faxNumber: string;
  isActive: boolean;
  openingBalances: [
    {
      accountId: string;
      currencyId: string;
      currencyName: string;
      code: string;
      exchangeRate: number;
      debit: number;
      credit: number;
    }
  ];
}
