export interface IBox {
  _id: string;
  code: string;
  safeBoxNameAr: string;
  safeBoxNameEn: string;
  glAccountId: string;
  currencyId: string;
  allBranches: boolean;
  branchesId: string[];
  isActive: boolean;
  openingBalances: [
    {
      currencyId: string;
      exchangeRate: number;
      debit: number;
      credit: number;
    }
  ];
}
