export interface ICashPayableReceipt {
  _id: string;
  targetAccountType: string;
  code: string;
  gregorianDate: string;
  hijriDate: string;
  cashPayableReceiptArabicDescription: string;
  cashPayableReceiptEnglishDescription: string;
  fileNumber: string;
  currencyId: string;
  exchangeRate: number;
  totalValue: number;
  currencyConversionDifference: number;
  internalReference: string;
  checkDispose: boolean;
  cashPayableReceiptPeriodic: boolean;
  status: string;
  isActive: boolean;
  cashPayableReceiptDetailsList: [
    {
      targetAccountId: string;
      currencyId: string;
      exchangeRate: number;
      value: number;
      downPaymentValue: number;
      totalValue: number;
      advancePaymentBalance: number;
      receiptNumber: string;
      receiptOwner: string;
      description: string;
      documentTable: {
        distributedValue: number;
        documentTableList: [
          {
            documentNumber: string;
            currencyId: string;
            exchangeRate: number;
            originalDocumentValue: number;
            documentBalance: number;
            documentDateGregorian: string;
            documentDateHijri: string;
            dueDocumentDateGregorian: string;
            dueDocumentDateHijri: string;
            documentPaidValue: number;
            documentPaidValueReceiptCurrency: number;
          }
        ]
      };
      paidTable: [
        {
          paidMethod: string;
          accountPayments: string;
          currencyId: string;
          exchangeRate: number;
          paidValue: number;
          dueDateGregorian: string;
          dueDateHijri: string;
          paidDateGregorian: string;
          paidDateHijri: string;
        }
      ]
    }
  ];
}
