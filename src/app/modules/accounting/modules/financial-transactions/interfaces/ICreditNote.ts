export interface ICreditNote {
  _id: string;
  code: string;
  gregorianDate: string;
  hijriDate: string;
  creditNotesArabicDescription: string;
  creditNotesEnglishDescription: string;
  fileNumber: string;
  targetAccountType: string;
  currencyId: string;
  exchangeRate: number;
  totalValue: number;
  currencyConversionDifference: number;
  internalReference: string;
  creditNotesPeriodic: boolean;
  status: string;
  isActive: boolean;
  creditNotesDetailsList: [
    {
      targetAccountId: string;
      currencyId: string;
      exchangeRate: number;
      value: number;
      documentTable: {
        distributedValue: number;
        documentTableList: [
          {
            documentNumber: string;
            currencyId: string;
            exchangeRate: number;
            originalDocumentValue: number;
            documentDateGregorian: string;
            documentDateHijri: string;
            documentPaidValue: number;
            documentPaidValueReceiptCurrency: number;
          }
        ]
      };
      paidTable: [
        {
          accountPayments: string;
          currencyId: string;
          exchangeRate: number;
          paidValue: number;
        }
      ]
    }
  ];
}
