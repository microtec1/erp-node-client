export interface IDebitNote {
  _id: string;
  code: string;
  gregorianDate: string;
  hijriDate: string;
  debitNotesArabicDescription: string;
  debitNotesEnglishDescription: string;
  fileNumber: string;
  targetAccountType: string;
  currencyId: string;
  exchangeRate: number;
  totalValue: number;
  currencyConversionDifference: number;
  internalReference: string;
  debitNotesPeriodic: boolean;
  status: string;
  isActive: boolean;
  debitNotesDetailsList: [
    {
      targetAccountId: string;
      currencyId: string;
      exchangeRate: number;
      value: number;
      documentTable: {
        distributedValue: number;
        documentTableList: [
          {
            documentNumber: string;
            currencyId: string;
            exchangeRate: number;
            originalDocumentValue: number;
            documentDateGregorian: string;
            documentDateHijri: string;
            documentPaidValue: number;
            documentPaidValueReceiptCurrency: number;
          }
        ]
      };
      paidTable: [
        {
          accountPayments: string;
          currencyId: string;
          exchangeRate: number;
          paidValue: number;
        }
      ]
    }
  ];
}
