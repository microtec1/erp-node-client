export interface IDebitTransfer {
  _id: string;
  code: string;
  gregorianDate: string;
  hijriDate: string;
  debitTransferDescriptionAr: string;
  debitTransferDescriptionEn: string;
  targetedAccount: string;
  fromTargetedAccountId: string;
  toTargetedAccountId: string;
  currencyId: string;
  exchangeRate: string;
  totalValue: number;
  internalReference: string;
  debitTransferStatus: string;
  isActive: boolean;
  debitTransferDetails: [
    {
      transactionNumber: number;
      transactionType: string;
      transactionDescription: string;
      transactionDateGregorian: string;
      transactionDateHijri: string;
      transactionValue: number;
      transactionBalance: number;
      transferredAmount: number;
      transferredBalance: number;
    }
  ];
  totalTransferredAmount: number;
  totalTransferredBalance: number;
}
