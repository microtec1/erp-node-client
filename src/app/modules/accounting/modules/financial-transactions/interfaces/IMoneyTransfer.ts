export interface IMoneyTransfer {
  _id: string;
  code: string;
  moneyTransferStatus: string;
  moneyTransferDescriptionAr: string;
  moneyTransferDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  isActive: boolean;
  moneyTransferDetails: [
    {
      accountType: string;
      accountId: string;
      currencyId: string;
      exchangeRate: number;
      amountOfMoney: number;
      transferredAccountType: string;
      transferredAccountId: string;
      transferredCurrencyId: string;
      transferredExchangeRate: number;
      transferredValue: number;
      expensesBankValue: number;
      descriptionAr: string;
      descriptionEn: string;
    }
  ];
}
