import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountStatementCostCentersComponent } from './account-statement-cost-centers.component';

describe('AccountStatementCostCentersComponent', () => {
  let component: AccountStatementCostCentersComponent;
  let fixture: ComponentFixture<AccountStatementCostCentersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountStatementCostCentersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountStatementCostCentersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
