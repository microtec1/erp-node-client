import { Component } from '@angular/core';
import { DataService } from 'src/app/common/services/shared/data.service';
import { AccountReportsService } from '../../services/account-reports.service';
import { GroupedAccountReportsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
@Component({
  selector: 'app-account-statement-cost-centers',
  templateUrl: './account-statement-cost-centers.component.html',
  styleUrls: ['./account-statement-cost-centers.component.scss']
})
export class AccountStatementCostCentersComponent  {

  constructor(public data: DataService,private general:GeneralService,private accountReports:AccountReportsService) { }

  filterConfig = {
    lockups: [

    ],
    inputs:[
      {
        name: "level",
        label: "AccountClassification.level",
        lockupName: "level",
      }
    ],
    date: {
      type: "ranged"
    }
  };

//   onAccountStatementChange(e){
//     this.form = e
//     this.noData = false;
//     this.spinner =true;
//     if(Object.entries(e).length===0 || !e.date ){
//       this.noData = true;
//       this.spinner = false;
//     }
    
//     else{
//       let minDate = this.general.formatDate(e.date[0])
//       let maxDate = this.general.formatDate(e.date[1])
//       console.log("test",e.level)
//       this.accountReports.removeObjEmptyAttributes(e);
//       this.filter = this.accountReports.shapingFilterObject(e);
//       console.log(this.filter)
//       this.filter.chartOfAccountsReportType = "balance";
//       let level = parseInt(e.level)
//       this.getDate({
//         "detailedAccountFilter":{
      
//         },
//         entriesFilter: {
//           minDate: minDate,
//           maxDate: maxDate
//         },
//           "mainAccountFilter":{
//           },
//           "groupOnLevel": level,
//           "groupToRoot" : true
//       },1)
      
//     }
//   }


//   tablesData = [];
//   spinner :boolean ;
//   noData :boolean = true;
//   pageTotal
//   currentPage
//   pageSize
//   form
//   filter:any

//   getDate(body = {},page?:number) {
//     let receivedData;
//     this.data.postWithoutSpinner(GroupedAccountReportsApi, body).subscribe((data:any[])=> {
//       // fetching data from server response
//       // reconstructing server response to match predefined table-headers criteria  (tablesData)
//       if(data.length !=0){
//         this.pageSize= 2;
//         this.pageTotal= data[0].count;
//         this.currentPage = data[0].currentPage;
//         receivedData = data[0].results;
//         this.tablesData=this.accountReports.shapingAccountStatmentCostCenters(receivedData)
//         console.log("tablesData : " ,this.tablesData);
//         if(this.tablesData.length == 0){
//           this.spinner=false;
//           this.noData = true
//         }else{
//           this.noData = false
//           this.spinner = false;
//         }
//       }else{
//         this.spinner=false;
//         this.noData = true
//       }
//     });
// }


}
