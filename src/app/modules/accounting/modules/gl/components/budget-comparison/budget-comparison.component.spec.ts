import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetComparisonComponent } from './budget-comparison.component';

describe('BudgetComparisonComponent', () => {
  let component: BudgetComparisonComponent;
  let fixture: ComponentFixture<BudgetComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
