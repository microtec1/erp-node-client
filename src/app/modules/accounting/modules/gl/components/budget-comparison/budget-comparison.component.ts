import { Component, OnInit } from '@angular/core';
import { AccountReportsService } from '../../services/account-reports.service';

@Component({
  selector: 'app-budget-comparison',
  templateUrl: './budget-comparison.component.html',
  styleUrls: ['./budget-comparison.component.scss']
})
export class BudgetComparisonComponent implements OnInit {

  constructor( private accountReportService:AccountReportsService) { 
    
  }
  ngOnInit(){
    this.columnDefs = {columns:[
      {
        field: "athlete",
        rowSpan: 2
      },
      {
        headerName: "test",
        children: [{ field: "age" }, { field: "country" }, { field: "year" }]
      }
    ]};
  }
  filterConfig = {
    lockups: [

    ],
    radios:[{
      name:"code",
      label:"AccountClassification.code",
      choices:[
        {
          name:"reportFrom",
          text:"AccountClassification.oneValue",
          value:"false"
        },
        {
          name:"reportTo",
          text:"AccountClassification.range",
          value:"true"
        }
      ]
    }

    ],
    date: {
      type: "ranged"
    }
  };

  tablesData = [];
  spinner :boolean ;
  noData :boolean = true;
  pageTotal
  currentPage
  pageSize
  form
  filter:any
  defaultColDef
  columnDefs

  
}
