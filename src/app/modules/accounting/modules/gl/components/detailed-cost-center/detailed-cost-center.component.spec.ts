import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedCostCenterComponent } from './detailed-cost-center.component';

describe('DetailedCostCenterComponent', () => {
  let component: DetailedCostCenterComponent;
  let fixture: ComponentFixture<DetailedCostCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedCostCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedCostCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
