import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { Router, ActivatedRoute, Params, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IBalanceSheetClassification } from '../../../../interfaces/IBalanceSheetClassification.model';
import { companyId } from 'src/app/common/constants/general.constants';
import { baseUrl, balanceSheetClassificationsApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-balance-sheet-classification',
  templateUrl: './add-balance-sheet-classification.component.html',
  styleUrls: ['./add-balance-sheet-classification.component.scss']
})
export class AddBalanceSheetClassificationComponent implements OnInit, OnDestroy {
  balanceSheetClassificationForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  balanceSheetClassification: IBalanceSheetClassification;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(balanceSheetClassificationsApi, null, null, id)
              .subscribe((balanceSheetClassification: IBalanceSheetClassification) => {
                this.balanceSheetClassification = balanceSheetClassification;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.balanceSheetClassificationForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  validateResult(event) {
    if (event === 'result') {
      this.balanceSheetClassificationForm.controls.balanceSheetClassificationResulting.setValidators([Validators.required]);
      this.balanceSheetClassificationForm.controls.balanceSheetClassificationResulting.updateValueAndValidity();
    } else {
      this.balanceSheetClassificationForm.controls.balanceSheetClassificationResulting.clearValidators();
      this.balanceSheetClassificationForm.controls.balanceSheetClassificationResulting.updateValueAndValidity();
      this.balanceSheetClassificationForm.value.balanceSheetClassificationResulting = null;

    }
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.balanceSheetClassificationForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.balanceSheetClassification.image = '';
    this.balanceSheetClassificationForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.balanceSheetClassificationForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.balanceSheetClassification) {
      if (this.balanceSheetClassificationForm.valid) {
        const newBalanceSheetClassification = {
          _id: this.balanceSheetClassification._id,
          ...this.generalService.checkEmptyFields(this.balanceSheetClassificationForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(balanceSheetClassificationsApi, newBalanceSheetClassification).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/gl/balanceSheetClassifications']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;

      }
    } else {
      if (this.balanceSheetClassificationForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.balanceSheetClassificationForm.value)
        };
        this.subscriptions.push(
          this.data.post(balanceSheetClassificationsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.balanceSheetClassificationForm.reset();
            this.balanceSheetClassificationForm.patchValue({
              balanceSheetClassificationSide: 'debit',
              balanceSheetClassificationType: 'detailed',
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let balanceSheetClassificationNameAr = '';
    let balanceSheetClassificationNameEn = '';
    let balanceSheetClassificationSide = 'debit';
    let balanceSheetClassificationType = 'detailed';
    let balanceSheetClassificationResulting = '';
    let balanceSheetClassificationCalcNetLossesProfits = false;
    let isActive = true;

    if (this.balanceSheetClassification) {
      image = this.balanceSheetClassification.image;
      code = this.balanceSheetClassification.code;
      balanceSheetClassificationNameAr = this.balanceSheetClassification.balanceSheetClassificationNameAr;
      balanceSheetClassificationNameEn = this.balanceSheetClassification.balanceSheetClassificationNameEn;
      balanceSheetClassificationSide = this.balanceSheetClassification.balanceSheetClassificationSide;
      balanceSheetClassificationType = this.balanceSheetClassification.balanceSheetClassificationType;
      balanceSheetClassificationResulting = this.balanceSheetClassification.balanceSheetClassificationResulting;
      balanceSheetClassificationCalcNetLossesProfits = this.balanceSheetClassification.balanceSheetClassificationCalcNetLossesProfits;
      isActive = this.balanceSheetClassification.isActive;
    }
    this.balanceSheetClassificationForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      balanceSheetClassificationNameAr: new FormControl(balanceSheetClassificationNameAr, Validators.required),
      balanceSheetClassificationNameEn: new FormControl(balanceSheetClassificationNameEn),
      balanceSheetClassificationSide: new FormControl(balanceSheetClassificationSide, Validators.required),
      balanceSheetClassificationType: new FormControl(balanceSheetClassificationType, Validators.required),
      balanceSheetClassificationResulting: new FormControl(balanceSheetClassificationResulting),
      balanceSheetClassificationCalcNetLossesProfits: new FormControl(balanceSheetClassificationCalcNetLossesProfits),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
