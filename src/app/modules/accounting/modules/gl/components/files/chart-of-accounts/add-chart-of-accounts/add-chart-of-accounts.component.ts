import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IChartOfAccount } from '../../../../interfaces/IChartOfAccount';
import { CustomValidators } from 'ng2-validation';
import { ICurrency } from 'src/app/modules/general/interfaces/ICurrency';
import { IBalanceSheetClassification } from '../../../../interfaces/IBalanceSheetClassification.model';
import { IIncomeStatementClassification } from '../../../../interfaces/IIncomeStatementClassification.model';
import { ICostCenter } from '../../../../interfaces/ICostCenter';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { DataService } from 'src/app/common/services/shared/data.service';
import {
  detailedCostCentersApi,
  currenciesApi,
  incomeStatementClassificationsApi,
  balanceSheetClassificationsApi,
  chartOfAccountsApi,
  chartOfAccountsTreeApi
} from 'src/app/common/constants/api.constants';
import { ChartOfAccountsService } from '../../../../services/chart-of-accounts.service';
import { CostCentersService } from '../../../../services/cost-centers.service';

@Component({
  selector: 'app-add-chart-of-accounts',
  templateUrl: './add-chart-of-accounts.component.html',
  styleUrls: ['./add-chart-of-accounts.component.scss']
})
export class AddChartOfAccountsComponent implements OnInit {
  @Input() item: IChartOfAccount;
  @Input() selectedItem: IChartOfAccount;
  @Input() sidebarMode: boolean;
  @Output() finished: EventEmitter<any> = new EventEmitter();
  chartOfAccountsForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  reportType = 'balance';
  categoriesInputFocused: boolean;
  chartOfAccount: IChartOfAccount;
  glSettings = JSON.parse(localStorage.getItem('glSettings'));

  // Cost Centers
  costCenters: ICostCenter[] = [];
  costCenterInputFocused: boolean;
  costCentersCount: number;
  noCostCenters: boolean;


  // Currencies
  currencies: ICurrency[] = [];
  currenciesInputFocused: boolean;
  hasMoreCurrencies: boolean;
  currenciesCount: number;
  selectedCurrenciesPage = 1;
  currenciesPagesNo: number;
  noCurrencies: boolean;

  // Income Statements
  incomeStatements: IIncomeStatementClassification[] = [];
  hasMoreIncomeStatements: boolean;
  incomeStatementsCount: number;
  selectedIncomeStatementsPage = 1;
  incomeStatementsPagesNo: number;
  noIncomeStatements: boolean;

  // Balance Sheets
  balanceSheets: IBalanceSheetClassification[] = [];
  hasMoreBalanceSheets: boolean;
  balanceSheetsCount: number;
  selectedBalanceSheetsPage = 1;
  balanceSheetsPagesNo: number;
  noBalanceSheets: boolean;

  constructor(
    private data: DataService,
    private COAService: ChartOfAccountsService,
    private costCentersService: CostCentersService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.initForm();
    if (this.glSettings.company.generateCodesOfAccountsAndCostCentersAutomatically) {
      this.chartOfAccountsForm.controls.code.clearValidators();
      this.chartOfAccountsForm.controls.code.updateValueAndValidity();
    }
    this.subscriptions.push(
      this.data.get(detailedCostCentersApi, null, null, companyId).subscribe((res: ICostCenter[]) => {
        if (res.length) {
          this.costCenters.push(...res);
          this.costCentersCount = res.length;
        } else {
          this.noCostCenters = true;
        }

        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(currenciesApi, 1).subscribe((res: IDataRes) => {
        this.currenciesPagesNo = res.pages;
        this.currenciesCount = res.count;
        if (this.currenciesPagesNo > this.selectedCurrenciesPage) {
          this.hasMoreCurrencies = true;
        }
        this.currencies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(incomeStatementClassificationsApi, 1).subscribe((res: IDataRes) => {
        this.incomeStatementsPagesNo = res.pages;
        this.incomeStatementsCount = res.count;
        if (this.incomeStatementsPagesNo > this.selectedIncomeStatementsPage) {
          this.hasMoreIncomeStatements = true;
        }

        if (res.results.length) {
          this.incomeStatements.push(...res.results);
        } else {
          this.noIncomeStatements = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(balanceSheetClassificationsApi, 1).subscribe((res: IDataRes) => {
        this.balanceSheetsPagesNo = res.pages;
        this.balanceSheetsCount = res.count;
        if (this.balanceSheetsPagesNo > this.selectedBalanceSheetsPage) {
          this.hasMoreBalanceSheets = true;
        }
        if (res.results.length) {
          this.balanceSheets.push(...res.results);
        } else {
          this.noBalanceSheets = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.COAService.fillFormChange.subscribe((res: IChartOfAccount) => {
        this.chartOfAccount = res;
        if (!res.addMode) {
          if (this.chartOfAccount.chartOfAccountsCostCenter.length) {
            const chartOfAccountsCostCenterArray = new FormArray([]);
            for (const control of this.chartOfAccount.chartOfAccountsCostCenter) {
              chartOfAccountsCostCenterArray.push(
                new FormGroup({
                  costCenterId: new FormControl(control.costCenterId),
                  costCenterRate: new FormControl(control.costCenterRate, CustomValidators.number)
                })
              );
            }
            this.chartOfAccountsForm.setControl('chartOfAccountsCostCenter', chartOfAccountsCostCenterArray);
          }
          this.chartOfAccountsForm.patchValue({
            code: this.chartOfAccount.code,
            chartOfAccountsNameAr: this.chartOfAccount.chartOfAccountsNameAr,
            chartOfAccountsNameEn: this.chartOfAccount.chartOfAccountsNameEn,
            chartOfAccountsCategoryId: this.chartOfAccount.chartOfAccountsCategoryId,
            chartOfAccountsCurrency: {
              currencyId: this.chartOfAccount.chartOfAccountsCurrency.currencyId
            },
            chartOfAccountsCostCenterType: this.chartOfAccount.chartOfAccountsCostCenterType,
            chartOfAccountsCashFlow: this.chartOfAccount.chartOfAccountsCashFlow,
            chartOfAccountsType: this.chartOfAccount.chartOfAccountsType,
            chartOfAccountsSide: this.chartOfAccount.chartOfAccountsSide,
            chartOfAccountsReportType: this.chartOfAccount.chartOfAccountsReportType,
            isActive: this.chartOfAccount.isActive,
          });
        } else {
          this.chartOfAccountsForm.reset();
          const chartOfAccountsCostCenterArray = new FormArray([
            new FormGroup({
              costCenterId: new FormControl(''),
              costCenterRate: new FormControl('', CustomValidators.number)
            })
          ]);
          this.chartOfAccountsForm.setControl('chartOfAccountsCostCenter', chartOfAccountsCostCenterArray);
          this.chartOfAccountsForm.patchValue({
            chartOfAccountsCostCenterType: this.chartOfAccount.chartOfAccountsCostCenterType,
            chartOfAccountsCashFlow: this.chartOfAccount.chartOfAccountsCashFlow,
            chartOfAccountsType: this.chartOfAccount.chartOfAccountsType,
            chartOfAccountsSide: this.chartOfAccount.chartOfAccountsSide,
            chartOfAccountsReportType: this.chartOfAccount.chartOfAccountsReportType,
            isActive: true,
          });
        }
      })
    );
  }

  get form() {
    return this.chartOfAccountsForm.controls;
  }

  get getChartOfAccountsCostCenterArray() {
    return this.chartOfAccountsForm.get('chartOfAccountsCostCenter') as FormArray;
  }
  get getChartOfAccountsCurrencyGroup() {
    return this.chartOfAccountsForm.get('chartOfAccountsCurrency') as FormGroup;
  }

  get optionTextField() {
    if (this.item) {
      return this.item.chartOfAccountsReportType === 'balance' ? 'balanceSheetClassificationNameAr' : 'incomeStatementClassificationNameAr';
    } else {
      return this.reportType === 'balance' ? 'balanceSheetClassificationNameAr' : 'incomeStatementClassificationNameAr';
    }
  }

  get itemsArray() {
    if (this.item) {
      return this.item.chartOfAccountsReportType === 'balance' ? this.balanceSheets : this.incomeStatements;
    } else {
      return this.reportType === 'balance' ? this.balanceSheets : this.incomeStatements;
    }
  }

  addCostCenter() {
    const control = this.chartOfAccountsForm.get('chartOfAccountsCostCenter') as FormArray;
    control.push(new FormGroup({
      costCenterId: new FormControl(''),
      costCenterRate: new FormControl(null, [CustomValidators.number])
    }));
  }

  deleteCostCenter(index) {
    const control = this.chartOfAccountsForm.get('chartOfAccountsCostCenter') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  validateCostCenters(value) {
    if (value === 'mandatory') {
      for (let i = 0; i <= this.getChartOfAccountsCostCenterArray.controls.length - 1; i++) {
        const formGroup = this.getChartOfAccountsCostCenterArray.controls[i] as FormGroup;
        formGroup.controls.costCenterId.setValidators(Validators.required);
        formGroup.controls.costCenterRate
          .setValidators([Validators.required, CustomValidators.number]);
        formGroup.controls.costCenterId.updateValueAndValidity();
        formGroup.controls.costCenterRate.updateValueAndValidity();
      }

    } else {
      for (let i = 0; i <= this.getChartOfAccountsCostCenterArray.controls.length - 1; i++) {
        const formGroup = this.getChartOfAccountsCostCenterArray.controls[i] as FormGroup;
        formGroup.controls.costCenterId.clearValidators();
        formGroup.controls.costCenterRate
          .clearValidators();
        formGroup.controls.costCenterId.updateValueAndValidity();
        formGroup.controls.costCenterRate.updateValueAndValidity();
      }
    }
  }

  searchCurrencies(event) {
    const searchValue = event;
    const searchQuery = {
      currencyNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(currenciesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCurrencies = true;
          } else {
            this.noCurrencies = false;
            for (const item of res.results) {
              if (this.currencies.length) {
                const uniqueCurrencies = this.currencies.filter(x => x._id !== item._id);
                this.currencies = uniqueCurrencies;
              }
              this.currencies.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCurrencies() {
    this.selectedCurrenciesPage = this.selectedCurrenciesPage + 1;
    this.subscriptions.push(
      this.data.get(currenciesApi, this.selectedCurrenciesPage).subscribe((res: IDataRes) => {
        if (this.currenciesPagesNo > this.selectedCurrenciesPage) {
          this.hasMoreCurrencies = true;
        } else {
          this.hasMoreCurrencies = false;
        }
        for (const item of res.results) {
          if (this.currencies.length) {
            const uniqueCurrencies = this.currencies.filter(x => x._id !== item._id);
            this.currencies = uniqueCurrencies;
          }
          this.currencies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCategories(event) {
    const searchValue = event;
    if (this.item) {
      this.reportType = this.item.chartOfAccountsReportType;
    }
    if (this.reportType === 'balance') {
      const searchQuery = {
        balanceSheetClassificationNameAr: searchValue,
        balanceSheetClassificationType: 'detailed'
      };
      if (searchValue.length >= searchLength) {
        this.subscriptions.push(
          this.data.get(balanceSheetClassificationsApi, null, searchQuery).subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBalanceSheets = true;
            } else {
              this.noBalanceSheets = false;
              this.balanceSheetsCount = res.count;
              for (const item of res.results) {
                if (this.balanceSheets.length) {
                  const uniqueBalanceSheets = this.balanceSheets
                    .filter(x => x._id !== item._id);
                  this.balanceSheets = uniqueBalanceSheets;
                }
                this.balanceSheets.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
        );
      }
    } else {
      const searchQuery = {
        incomeStatementClassificationNameAr: searchValue,
        incomeStatementClassificationType: 'detailed'
      };
      if (searchValue.length >= searchLength) {
        this.subscriptions.push(
          this.data.get(incomeStatementClassificationsApi, null, searchQuery).subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noIncomeStatements = true;
            } else {
              this.noIncomeStatements = false;
              this.incomeStatementsCount = res.count;
              for (const item of res.results) {
                if (this.incomeStatements.length) {
                  const uniqueIncomeStatements = this.incomeStatements
                    .filter(x => x._id !== item._id);
                  this.incomeStatements = uniqueIncomeStatements;
                }
                this.incomeStatements.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
        );
      }
    }
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  cancel() {
    this.finished.emit(true);
  }

  private initForm() {
    let code = '';
    let chartOfAccountsNameAr = '';
    let chartOfAccountsNameEn = '';
    let chartOfAccountsCurrencyGroup = new FormGroup({
      currencyId: new FormControl('', Validators.required),
    });
    let chartOfAccountsCategoryId = '';
    let chartOfAccountsCostCenterType = 'optional';
    let chartOfAccountsCashFlow = 'optional';
    let chartOfAccountsType = 'main';
    let chartOfAccountsSide = 'debit';
    let chartOfAccountsReportType = 'balance';
    let isActive = true;
    let chartOfAccountsCostCenterArray = new FormArray([
      new FormGroup({
        costCenterId: new FormControl(''),
        costCenterRate: new FormControl(null, [CustomValidators.number])
      })
    ]);


    if (this.item) {
      code = this.item.code,
        chartOfAccountsNameAr = this.item.chartOfAccountsNameAr,
        chartOfAccountsNameEn = this.item.chartOfAccountsNameEn,
        chartOfAccountsCurrencyGroup = new FormGroup({
          currencyId: new FormControl(this.item.chartOfAccountsCurrency.currencyId, Validators.required)
        }),
        chartOfAccountsCategoryId = this.item.chartOfAccountsCategoryId,
        chartOfAccountsCostCenterType = this.item.chartOfAccountsCostCenterType,
        chartOfAccountsCashFlow = this.item.chartOfAccountsCashFlow,
        chartOfAccountsType = this.item.chartOfAccountsType,
        chartOfAccountsSide = this.item.chartOfAccountsSide,
        chartOfAccountsReportType = this.item.chartOfAccountsReportType,
        isActive = this.item.isActive;

      if (this.item.chartOfAccountsCostCenter.length) {
        chartOfAccountsCostCenterArray = new FormArray([]);
        for (const control of this.item.chartOfAccountsCostCenter) {
          chartOfAccountsCostCenterArray.push(
            new FormGroup({
              costCenterId: new FormControl(control.costCenterId),
              costCenterRate: new FormControl(control.costCenterRate, [CustomValidators.number])
            })
          );
        }
      }

    }
    this.chartOfAccountsForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      chartOfAccountsNameAr: new FormControl(chartOfAccountsNameAr, Validators.required),
      chartOfAccountsNameEn: new FormControl(chartOfAccountsNameEn),
      chartOfAccountsCurrency: chartOfAccountsCurrencyGroup,
      chartOfAccountsCategoryId: new FormControl(chartOfAccountsCategoryId),
      chartOfAccountsCostCenterType: new FormControl(chartOfAccountsCostCenterType, Validators.required),
      chartOfAccountsCashFlow: new FormControl(chartOfAccountsCashFlow, Validators.required),
      chartOfAccountsType: new FormControl(chartOfAccountsType, Validators.required),
      chartOfAccountsSide: new FormControl(chartOfAccountsSide, Validators.required),
      chartOfAccountsReportType: new FormControl(chartOfAccountsReportType, Validators.required),
      isActive: new FormControl(isActive, Validators.required),
      chartOfAccountsCostCenter: chartOfAccountsCostCenterArray
    });
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (this.chartOfAccount || this.item) {
      if (this.chartOfAccountsForm.valid) {
        const editItem = this.chartOfAccount && this.chartOfAccount || this.item && this.item;
        const newChartOfAccounts = {
          _id: editItem._id,
          ...this.generalService.checkEmptyFields(this.chartOfAccountsForm.value),
          companyId
        };
        const emptyObject = this.generalService.isEmpty(this.chartOfAccountsForm.value.chartOfAccountsCostCenter[0]);
        if (emptyObject) {
          newChartOfAccounts.chartOfAccountsCostCenter = [];
        }
        this.subscriptions.push(
          this.data.put(chartOfAccountsApi, newChartOfAccounts).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.costCentersService.fillFormChange.next(null);
            this.finished.emit();
            this.subscriptions.push(
              this.data.getByRoute(chartOfAccountsTreeApi).subscribe((response: IChartOfAccount[]) => {
                this.COAService.chartOfAccountsChanged.next(response);
                this.uiService.isLoading.next(false);
              }, err => {
                this.uiService.isLoading.next(false);
                this.uiService.showErrorMessage(err);
              })
            );
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            })
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      }
    } else {
      if (this.chartOfAccountsForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.chartOfAccountsForm.value),
          companyId,
          parentId: this.selectedItem ? this.selectedItem._id : null
        };
        const emptyObject = this.generalService.isEmpty(this.chartOfAccountsForm.value.chartOfAccountsCostCenter[0]);
        if (emptyObject) {
          formValue.chartOfAccountsCostCenter = [];
        }
        this.subscriptions.push(
          this.data.post(chartOfAccountsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.chartOfAccountsForm.reset();
            this.chartOfAccountsForm.patchValue({
              chartOfAccountsCostCenterType: 'optional',
              chartOfAccountsCashFlow: 'optional',
              chartOfAccountsType: 'main',
              chartOfAccountsSide: 'debit',
              chartOfAccountsReportType: 'balance',
              isActive: true,
            });
            this.subscriptions.push(
              this.data.getByRoute(chartOfAccountsTreeApi).subscribe((response: IChartOfAccount[]) => {
                this.COAService.chartOfAccountsChanged.next(response);
                this.uiService.isLoading.next(false);
              })
            );
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }
}
