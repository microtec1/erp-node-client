import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { IChartOfAccount } from '../../../../interfaces/IChartOfAccount';
import { ChartOfAccountsService } from '../../../../services/chart-of-accounts.service';
import { ICurrency } from 'src/app/modules/general/interfaces/ICurrency';
import { ICostCenter } from '../../../../interfaces/ICostCenter';
import { IBalanceSheetClassification } from '../../../../interfaces/IBalanceSheetClassification.model';
import { IIncomeStatementClassification } from '../../../../interfaces/IIncomeStatementClassification.model';
import { DataService } from 'src/app/common/services/shared/data.service';
import { currenciesApi, costCentersApi, balanceSheetClassificationsApi, incomeStatementClassificationsApi, chartOfAccountsApi, chartOfAccountsTreeApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';

@Component({
  selector: 'app-chart-of-accounts-tree',
  templateUrl: './chart-of-accounts-tree.component.html',
  styleUrls: ['./chart-of-accounts-tree.component.scss']
})
export class ChartOfAccountsTreeComponent implements OnInit, OnDestroy {
  @Input() items: IChartOfAccount[];
  selected: any;
  selectedChartOfAccounts: IChartOfAccount;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  initAddForm: boolean;
  initEditForm: boolean;

  constructor(
    private modalService: BsModalService,
    private COAService: ChartOfAccountsService,
    private generalService: GeneralService,
    private data: DataService,
    private uiService: UiService
  ) { }

  ngOnInit() {
  }

  actionFinished() {
    this.initAddForm = false;
    this.initEditForm = false;
    this.selectedChartOfAccounts = null;
    this.selected = null;
  }

  showDetails(item) {
    this.selected = item._id;
    this.selectedChartOfAccounts = item;
    this.subscriptions.push(
      this.data.get(currenciesApi, null, null, this.selectedChartOfAccounts.chartOfAccountsCurrency.currencyId)
        .subscribe((currency: ICurrency) => {
          this.selectedChartOfAccounts.chartOfAccountsCurrency.currencyNameAr = currency.currencyNameAr;
          this.uiService.isLoading.next(false);
        })
    );
    const chartOfAccountsCostCenterArray: any = [];
    for (let control of item.chartOfAccountsCostCenter) {
      this.subscriptions.push(
        this.data.get(costCentersApi, null, null, control.costCenterId).subscribe((res: ICostCenter) => {
          control = { ...control, costCenterNameAr: res.costCenterNameAr, code: res.code };
          chartOfAccountsCostCenterArray.push(control);
          this.uiService.isLoading.next(false);
        })
      );
    }
    if (this.selectedChartOfAccounts.chartOfAccountsCategoryId) {
      if (this.selectedChartOfAccounts.chartOfAccountsReportType === 'balance') {
        this.subscriptions.push(
          this.data.get(balanceSheetClassificationsApi, null, null, this.selectedChartOfAccounts.chartOfAccountsCategoryId)
            .subscribe((balanceSheet: IBalanceSheetClassification) => {
              this.selectedChartOfAccounts.chartOfAccountsCategoryName = balanceSheet.balanceSheetClassificationNameAr;
              this.uiService.isLoading.next(false);
            })
        );
      } else {
        this.subscriptions.push(
          this.data.get(incomeStatementClassificationsApi, null, null, this.selectedChartOfAccounts.chartOfAccountsCategoryId)
            .subscribe((incomeStatement: IIncomeStatementClassification) => {
              this.selectedChartOfAccounts.chartOfAccountsCategoryName = incomeStatement.incomeStatementClassificationNameAr;
              this.uiService.isLoading.next(false);
            })
        );
      }
    }

    this.selectedChartOfAccounts.chartOfAccountsCostCenter = chartOfAccountsCostCenterArray;
    this.initAddForm = false;
    this.initEditForm = false;
  }

  deleteModal(chartOfAccounts: IChartOfAccount) {
    const initialState = {
      code: chartOfAccounts.code,
      nameAr: chartOfAccounts.chartOfAccountsNameAr,
      nameEn: chartOfAccounts.chartOfAccountsNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(chartOfAccounts._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(chartOfAccountsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.subscriptions.push(
          this.data.getByRoute(chartOfAccountsTreeApi).subscribe((response: IChartOfAccount[]) => {
            this.actionFinished();
            this.COAService.chartOfAccountsChanged.next(response);
            this.uiService.isLoading.next(false);
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  addNewItem() {
    this.initEditForm = false;
    this.initAddForm = true;
    this.selectedChartOfAccounts.addMode = true;
    this.COAService.fillFormChange.next(this.selectedChartOfAccounts);
  }

  editItem() {
    this.initAddForm = false;
    this.initEditForm = true;
    this.COAService.fillFormChange.next({...this.selectedChartOfAccounts, addMode: false});
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
