import { Component, OnInit, OnDestroy } from '@angular/core';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { IChartOfAccount } from '../../../interfaces/IChartOfAccount';
import { ChartOfAccountsService } from '../../../services/chart-of-accounts.service';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/shared/data.service';
import { chartOfAccountsTreeApi, chartOfAccountsApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-chart-of-accounts',
  templateUrl: './chart-of-accounts.component.html',
  styleUrls: ['./chart-of-accounts.component.scss']
})
export class ChartOfAccountsComponent implements OnInit, OnDestroy {
  chartOfAccounts: IChartOfAccount[];
  subscriptions: Subscription[] = [];
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;

  constructor(
    private COAService: ChartOfAccountsService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
  ) { }


  ngOnInit() {
    this.getChartOfAccountsTree();
    this.subscriptions.push(
      this.COAService.chartOfAccountsChanged.subscribe((res: IChartOfAccount[]) => {
        this.chartOfAccounts = res;
      })
    );

    this.initSearchForm();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      chartOfAccountsNameAr: new FormControl(''),
      chartOfAccountsNameEn: new FormControl('')
    });
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getChartOfAccountsTree();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(chartOfAccountsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.chartOfAccounts = res.results;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  getChartOfAccountsTree() {
    this.subscriptions.push(
      this.data.getByRoute(chartOfAccountsTreeApi).subscribe((res: IChartOfAccount[]) => {
        this.chartOfAccounts = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
