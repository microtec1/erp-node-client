import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostCentersModalComponent } from './cost-centers-modal.component';

describe('CostCentersModalComponent', () => {
  let component: CostCentersModalComponent;
  let fixture: ComponentFixture<CostCentersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostCentersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostCentersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
