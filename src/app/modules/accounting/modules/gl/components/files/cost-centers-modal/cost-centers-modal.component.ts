import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ICostCenter } from '../../../interfaces/ICostCenter';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { chartOfAccountsApi, detailedCostCentersApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { companyId } from 'src/app/common/constants/general.constants';

@Component({
  selector: 'app-cost-centers-modal',
  templateUrl: './cost-centers-modal.component.html',
  styleUrls: ['./cost-centers-modal.component.scss']
})
export class CostCentersModalComponent implements OnInit, OnDestroy {
  data: IChartOfAccount;
  costCentersValue: any;
  value: number;
  costCenters: any[] = [];
  subscriptions: Subscription[] = [];
  costCentersForm: FormGroup;
  formReady: boolean;

  @Output() formValue: EventEmitter<any> = new EventEmitter();

  // Cost Centers
  allCostCenters: ICostCenter[] = [];
  costCenterInputFocused: boolean;
  costCentersCount: number;
  noCostCenters: boolean;


  constructor(
    public bsModalRef: BsModalRef,
    private dataService: DataService,
    private uiService: UiService,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.dataService.get(detailedCostCentersApi, null, null, companyId).subscribe((res: ICostCenter[]) => {
        if (res.length) {
          this.allCostCenters.push(...res);
          this.costCentersCount = res.length;
        } else {
          this.noCostCenters = true;
        }

        this.uiService.isLoading.next(false);
      })
    );
    this.subscriptions.push(
      this.dataService.get(chartOfAccountsApi, null, null, this.data._id).subscribe((account: IChartOfAccount) => {
        this.costCenters = account.chartOfAccountsCostCenter;
        this.formReady = true;
        this.initForm();
        if (this.costCentersValue) {
          this.getCostCenterListArray.controls = [];
          for (const item of this.costCentersValue) {
            this.getCostCenterListArray.push(
              new FormGroup({
                costCenterId: new FormControl(item.costCenterId),
                price: new FormControl(item.price),
              }));
          }
        }
        this.uiService.isLoading.next(false);
      })
    );

    if (this.costCenters.length) {
      this.getCostCenterListArray.controls = [];
      for (const control of this.costCenters) {
        this.getCostCenterListArray.push(
          new FormGroup({
            costCenterId: new FormControl(control.costCenterId),
            price: new FormControl(this.value * control.costCenterRate / 100),
          })
        );
      }
    }
  }

  get getCostCenterListArray() {
    return this.costCentersForm.get('costCenterList') as FormArray;
  }

  addCostCenter() {
    const control = this.costCentersForm.get('costCenterList') as FormArray;
    control.push(new FormGroup({
      costCenterId: new FormControl(''),
      price: new FormControl(null),
    }));
  }

  deleteCostCenter(index) {
    const control = this.costCentersForm.get('costCenterList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  submit() {
    if (this.costCentersForm.value) {
      this.formValue.emit(this.costCentersForm.value);
      this.bsModalRef.hide();
    }
  }

  private initForm() {
    let costCenterListArray = new FormArray([
      new FormGroup({
        costCenterId: new FormControl(''),
        price: new FormControl(null),
      })
    ]);

    if (this.costCenters.length) {
      costCenterListArray = new FormArray([]);
      for (const control of this.costCenters) {
        costCenterListArray.push(
          new FormGroup({
            costCenterId: new FormControl(control.costCenterId),
            price: new FormControl(this.value * control.costCenterRate / 100),
          })
        );
      }
    }
    this.costCentersForm = new FormGroup({
      costCenterList: costCenterListArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }


}
