import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CostCentersService } from '../../../../services/cost-centers.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { ICostCenter } from '../../../../interfaces/ICostCenter';
import { DataService } from 'src/app/common/services/shared/data.service';
import { costCentersApi, costCentersTreeApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-cost-center',
  templateUrl: './add-cost-center.component.html',
  styleUrls: ['./add-cost-center.component.scss']
})
export class AddCostCenterComponent implements OnInit {
  @Input() item: ICostCenter;
  @Input() selectedItem: ICostCenter;
  @Input() sidebarMode: boolean;
  @Output() finished: EventEmitter<any> = new EventEmitter();
  costCenterForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  costCenter: ICostCenter;
  glSettings = JSON.parse(localStorage.getItem('glSettings'));
  constructor(
    private costCentersService: CostCentersService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
  ) { }

  ngOnInit() {
    this.initForm();
    if (this.glSettings.company.generateCodesOfAccountsAndCostCentersAutomatically) {
      this.costCenterForm.controls.code.clearValidators();
      this.costCenterForm.controls.code.updateValueAndValidity();
    }
    this.subscriptions.push(
      this.costCentersService.fillFormChange.subscribe((res: ICostCenter) => {
        this.costCenter = res;
        if (this.costCenter) {
          this.costCenterForm.patchValue({
            code: this.costCenter.code,
            costCenterNameAr: this.costCenter.costCenterNameAr,
            costCenterNameEn: this.costCenter.costCenterNameEn,
            costCenterType: this.costCenter.costCenterType,
            isActive: this.costCenter.isActive,
          });
        } else {
          this.costCenterForm.patchValue({
            code: '',
            costCenterNameAr: '',
            costCenterNameEn: '',
            costCenterType: '',
            isActive: true,
          });
        }
      })
    );
  }

  get form() {
    return this.costCenterForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  cancel() {
    this.finished.emit(true);
  }

  private initForm() {
    let code = '';
    let costCenterNameAr = '';
    let costCenterNameEn = '';
    let costCenterType = 'main';
    let isActive = true;

    if (this.item) {
      code = this.item.code;
      costCenterNameAr = this.item.costCenterNameAr;
      costCenterNameEn = this.item.costCenterNameEn;
      costCenterType = this.item.costCenterType;
      isActive = this.item.isActive;
    }

    this.costCenterForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      costCenterNameAr: new FormControl(costCenterNameAr, Validators.required),
      costCenterNameEn: new FormControl(costCenterNameEn),
      costCenterType: new FormControl(costCenterType),
      isActive: new FormControl(isActive, Validators.required),
    });
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.costCenter || this.item) {
      if (this.costCenterForm.valid) {
        const editItem = this.costCenter && this.costCenter || this.item && this.item;
        const newCostCenter = {
          _id: editItem._id,
          ...this.generalService.checkEmptyFields(this.costCenterForm.value),
          companyId
        };
        this.subscriptions.push(
          this.data.put(costCentersApi, newCostCenter).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.costCentersService.fillFormChange.next(null);
            this.finished.emit();
            this.subscriptions.push(
              this.data.getByRoute(costCentersTreeApi).subscribe((response: ICostCenter[]) => {
                this.costCentersService.costCentersChanged.next(response);
                this.uiService.isLoading.next(false);
              })
            );
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.costCenterForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.costCenterForm.value),
          companyId,
          parentId: this.selectedItem ? this.selectedItem._id : null
        };
        this.subscriptions.push(
          this.data.post(costCentersApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.costCenterForm.reset();
            this.costCenterForm.patchValue({
              costCenterType: 'main',
              isActive: true,
            });
            this.subscriptions.push(
              this.data.getByRoute(costCentersTreeApi).subscribe((response: ICostCenter[]) => {
                this.costCentersService.costCentersChanged.next(response);
                this.uiService.isLoading.next(false);
              })
            );
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

}
