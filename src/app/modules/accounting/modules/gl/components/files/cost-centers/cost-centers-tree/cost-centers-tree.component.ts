import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ICostCenter } from '../../../../interfaces/ICostCenter';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { Subscription } from 'rxjs';
import { CostCentersService } from '../../../../services/cost-centers.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { costCentersApi, costCentersTreeApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-cost-centers-tree',
  templateUrl: './cost-centers-tree.component.html',
  styleUrls: ['./cost-centers-tree.component.scss']
})
export class CostCentersTreeComponent implements OnInit, OnDestroy {
  @Input() items: ICostCenter[];
  selected: any;
  selectedCostCenter: ICostCenter;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  initAddForm: boolean;
  initEditForm: boolean;

  constructor(
    private modalService: BsModalService,
    private costCentersService: CostCentersService,
    private data: DataService,
    private uiService: UiService
  ) { }

  ngOnInit() {
  }

  actionFinished() {
    this.initAddForm = false;
    this.initEditForm = false;
    this.selectedCostCenter = null;
    this.selected = null;
  }

  showDetails(item) {
    this.selected = item._id;
    this.selectedCostCenter = item;
    this.initAddForm = false;
    this.initEditForm = false;
  }

  deleteModal(costCenter: ICostCenter) {
    const initialState = {
      id: costCenter._id,
      code: costCenter.code,
      nameAr: costCenter.costCenterNameAr,
      nameEn: costCenter.costCenterNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(costCenter._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(costCentersApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.subscriptions.push(
          this.data.getByRoute(costCentersTreeApi).subscribe((response: ICostCenter[]) => {
            this.actionFinished();
            this.costCentersService.costCentersChanged.next(response);
            this.uiService.isLoading.next(false);
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  addNewItem() {
    this.initEditForm = false;
    this.initAddForm = true;
    this.costCentersService.fillFormChange.next(null);
  }

  editItem() {
    this.initAddForm = false;
    this.initEditForm = true;
    this.costCentersService.fillFormChange.next(this.selectedCostCenter);
  }


  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
