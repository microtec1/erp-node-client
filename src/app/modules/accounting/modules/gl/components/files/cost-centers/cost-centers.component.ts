import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICostCenter } from '../../../interfaces/ICostCenter';
import { CostCentersService } from '../../../services/cost-centers.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/shared/data.service';
import { costCentersTreeApi, costCentersApi } from 'src/app/common/constants/api.constants';
import { companyId } from 'src/app/common/constants/general.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-cost-centers',
  templateUrl: './cost-centers.component.html',
  styleUrls: ['./cost-centers.component.scss']
})
export class CostCentersComponent implements OnInit, OnDestroy {
  costCenters: ICostCenter[];
  subscriptions: Subscription[] = [];
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;

  constructor(
    private costCentersService: CostCentersService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
  ) { }


  ngOnInit() {
    this.getCostCentersTree();
    this.subscriptions.push(
      this.costCentersService.costCentersChanged.subscribe((data: ICostCenter[]) => {
        this.costCenters = data;
        this.uiService.isLoading.next(false);
      })
    );
    this.initSearchForm();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      costCenterNameAr: new FormControl(''),
      costCenterNameEn: new FormControl('')
    });
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCostCentersTree();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(costCentersApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.costCenters = res.results;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  getCostCentersTree() {
    this.subscriptions.push(
      this.data.getByRoute(costCentersTreeApi).subscribe((res: ICostCenter[]) => {
        this.costCenters = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
