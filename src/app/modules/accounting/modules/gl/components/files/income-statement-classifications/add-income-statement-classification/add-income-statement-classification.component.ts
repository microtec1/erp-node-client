import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IIncomeStatementClassification } from '../../../../interfaces/IIncomeStatementClassification.model';
import { baseUrl, incomeStatementClassificationsApi } from 'src/app/common/constants/api.constants';
import { companyId } from 'src/app/common/constants/general.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-income-statement-classification',
  templateUrl: './add-income-statement-classification.component.html',
  styleUrls: ['./add-income-statement-classification.component.scss']
})
export class AddIncomeStatementClassificationComponent
  implements OnInit, OnDestroy {
  incomeStatementClassificationForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  incomeStatementClassification: IIncomeStatementClassification;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(incomeStatementClassificationsApi, null, null, id).subscribe(
              (
                incomeStatementClassification: IIncomeStatementClassification
              ) => {
                this.incomeStatementClassification = incomeStatementClassification;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.incomeStatementClassificationForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              }
            )
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  validateResult(event) {
    if (event === 'result') {
      this.incomeStatementClassificationForm.controls.incomeStatementClassificationResulting.setValidators(
        [Validators.required]
      );
      this.incomeStatementClassificationForm.controls.incomeStatementClassificationResulting.updateValueAndValidity();
    } else {
      this.incomeStatementClassificationForm.controls.incomeStatementClassificationResulting.clearValidators();
      this.incomeStatementClassificationForm.controls.incomeStatementClassificationResulting.updateValueAndValidity();
      this.incomeStatementClassificationForm.value.incomeStatementClassificationResulting = null;
    }
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.incomeStatementClassificationForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.incomeStatementClassification.image = '';
    this.incomeStatementClassificationForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.incomeStatementClassificationForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.incomeStatementClassification) {
      if (this.incomeStatementClassificationForm.valid) {
        const newIncomeStatementClassification = {
          _id: this.incomeStatementClassification._id,
          ...this.generalService.checkEmptyFields(
            this.incomeStatementClassificationForm.value
          ),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(incomeStatementClassificationsApi, newIncomeStatementClassification).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/gl/incomeStatementClassifications']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.incomeStatementClassificationForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(
            this.incomeStatementClassificationForm.value
          ),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(incomeStatementClassificationsApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.incomeStatementClassificationForm.reset();
              this.incomeStatementClassificationForm.patchValue({
                incomeStatementClassificationSide: 'debit',
                incomeStatementClassificationType: 'detailed',
                isActive: true
              });
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let incomeStatementClassificationNameAr = '';
    let incomeStatementClassificationNameEn = '';
    let incomeStatementClassificationResulting = '';
    let incomeStatementClassificationSide = 'debit';
    let incomeStatementClassificationType = 'detailed';
    let isActive = true;

    if (this.incomeStatementClassification) {
      image = this.incomeStatementClassification
        .image;
      code = this.incomeStatementClassification
        .code;
      incomeStatementClassificationNameAr = this.incomeStatementClassification
        .incomeStatementClassificationNameAr;
      incomeStatementClassificationNameEn = this.incomeStatementClassification
        .incomeStatementClassificationNameEn;
      incomeStatementClassificationSide = this.incomeStatementClassification
        .incomeStatementClassificationSide;
      incomeStatementClassificationType = this.incomeStatementClassification
        .incomeStatementClassificationType;
      incomeStatementClassificationResulting = this
        .incomeStatementClassification.incomeStatementClassificationResulting;
      isActive = this.incomeStatementClassification.isActive;
    }
    this.incomeStatementClassificationForm = new FormGroup({
      image: new FormControl(
        image
      ),
      code: new FormControl(
        code,
        Validators.required
      ),
      incomeStatementClassificationNameAr: new FormControl(
        incomeStatementClassificationNameAr,
        Validators.required
      ),
      incomeStatementClassificationNameEn: new FormControl(
        incomeStatementClassificationNameEn
      ),
      incomeStatementClassificationSide: new FormControl(
        incomeStatementClassificationSide,
        Validators.required
      ),
      incomeStatementClassificationType: new FormControl(
        incomeStatementClassificationType,
        Validators.required
      ),
      incomeStatementClassificationResulting: new FormControl(
        incomeStatementClassificationResulting
      ),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
