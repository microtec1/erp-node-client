import { Component, OnInit, OnDestroy } from '@angular/core';
import { IIncomeStatementClassification } from '../../../interfaces/IIncomeStatementClassification.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { baseUrl, incomeStatementClassificationsApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-income-statement-classifications',
  templateUrl: './income-statement-classifications.component.html',
  styleUrls: ['./income-statement-classifications.component.scss']
})
export class IncomeStatementClassificationsComponent implements OnInit, OnDestroy {
  incomeStatementClassifications: IIncomeStatementClassification[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;
  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getIncomeStatementClassificationsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(incomeStatementClassificationsApi, pageNumber).subscribe((res: IDataRes) => {
      this.incomeStatementClassifications = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(incomeStatementClassificationsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.incomeStatementClassifications = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getIncomeStatementClassificationsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(incomeStatementClassificationsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.incomeStatementClassifications = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      incomeStatementClassificationNameAr: new FormControl(''),
      incomeStatementClassificationNameEn: new FormControl(''),
      incomeStatementClassificationType: new FormControl(''),
      incomeStatementClassificationSide: new FormControl(''),
    });
  }

  deleteModal(incomeStatementClassification: IIncomeStatementClassification) {
    const initialState = {
      code: incomeStatementClassification.code,
      nameAr: incomeStatementClassification.incomeStatementClassificationNameAr,
      nameEn: incomeStatementClassification.incomeStatementClassificationNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(incomeStatementClassification._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(incomeStatementClassificationsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.incomeStatementClassifications = this.generalService.removeItem(this.incomeStatementClassifications, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getIncomeStatementClassificationsFirstPage() {
    this.subscriptions.push(
      this.data.get(incomeStatementClassificationsApi, 1).subscribe((res: IDataRes) => {
        this.incomeStatementClassifications = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
