import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedAccountReportComponent } from './grouped-account-report.component';

describe('GroupedAccountReportComponent', () => {
  let component: GroupedAccountReportComponent;
  let fixture: ComponentFixture<GroupedAccountReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupedAccountReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedAccountReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
