import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/common/services/shared/data.service';
import { AccountReportsService } from '../../services/account-reports.service';
import { GroupedAccountReportsApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-grouped-account-report',
  templateUrl: './grouped-account-report.component.html',
  styleUrls: ['./grouped-account-report.component.scss']
})
export class GroupedAccountReportComponent implements OnInit{

  // ui components configuration
  filterConfig = {
    lockups: [
      {
        label: "REPORTS.glAccount",
        lockupName: "chartOfAccounts",
        placeholder:"AccountClassification.selectAccount",
        childs:[
          {
            label: "AccountClassification.from",
            lockupName: "from",
          },
          {
            label: "AccountClassification.to",
            lockupName: "to",
          }
        ]
      },
      {
        label: "REPORTS.glAccount",
        lockupName: "accountsGroup",
        placeholder:"AccountClassification.selectGroupOfAccounts",
        childs:[
          {
            label: "AccountClassification.accountsGroup",
            lockupName: "accountsGroup"
          }
        ]
      },
      {
        label: "REPORTS.glAccount",
        lockupName: "branchGroup",
        placeholder:"AccountClassification.selectBranch",
        childs:[
          {
            label: "AccountClassification.branchGroup",
            lockupName: "branch"
          }
        ]
      },
      {
        label: "REPORTS.glAccount",
        lockupName: "branch",
        placeholder:"AccountClassification.selectBranch",
        childs:[
          {
            label: "AccountClassification.from",
            lockupName: "fromBranch",
          },
          {
            label: "AccountClassification.to",
            lockupName: "toBranch",
          }
        ]
      },
    ],
    inputs:[
      {
        name: "level",
        label: "AccountClassification.level",
        lockupName: "level",
      }
    ],
    radios:[
      {
        name:"journalEntryStatus",
        label:"AccountClassification.journalEntryStatus",
        choices:[
          {
            name:"all",
            text:"AccountClassification.all",
            value:"all"
          },
          {
            name:"posted",
            text:"AccountClassification.posted",
            value:"posted"
          },
          {
            name:"unposted",
            text:"AccountClassification.unposted",
            value:"unposted"
          }
        ]
      },
      {
        name:"groupToRoot",
        label:"AccountClassification.level",
        choices:[
          {
            name:"currentLevel",
            text:"AccountClassification.currentLevel",
            value:"false"
          },
          {
            name:"toRoot",
            text:"AccountClassification.toRoot",
            value:"true"
          }
        ]
      },
      {
        name:"staticAccumulative",
        label:"AccountClassification.accumulative",
        choices:[
          {
            name:"accumulative",
            text:"AccountClassification.accumulative",
            value:"true"
          },
          {
            name:"period",
            text:"AccountClassification.period",
            value:"false"
          }
        ]
      }
    ],
    date: {
      type: "ranged"
    }
  };
    
  tablesData = [];
  spinner :boolean ;
  noData :boolean = true;
  pageTotal : number ;
  currentPage : number;
  pageSize : number;
  date:{minDate,maxDate}
  filter:any;
  entriesFilter:any={};
  level:number;
  groupToRoot:boolean;
  staticAccumulative:boolean;
  grouped=true;
  collapsed:boolean=true;

  // filter Form initalization 
  filterFormConfig = new FormGroup({
    date: new FormControl(''),
    level:new FormControl(''),
    groupToRoot :new FormControl(''),
    staticAccumulative : new FormControl(''),
    from: new FormControl(),
    to : new FormControl(),
    accountsGroup : new FormControl(),
    branch : new FormControl(),
    branchGroup : new FormControl(),
    fromBranch : new FormControl(),
    toBranch : new FormControl(),
    journalEntryStatus : new FormControl(),
  })

  defaultColDef = {
    sortable: true,
    resizable: true,
  };

  constructor(
    private data: DataService,
    private accountReports:AccountReportsService,
    private uiService:UiService
  ) { }

  ngOnInit(){
    this.uiService.isCollapsed.next(true);
    this.uiService.isCollapsed.subscribe(collapsed=>this.collapsed=collapsed)
  }


  // send form body to the endpoint
  onGroupedReportChange(e){
    this.noData = false;
    this.spinner =true;
    console.log(e.date)
    if(Object.entries(e).length===0 || (!e.date||!e.level) ){
      this.noData = true;
      this.spinner = false;
      let message = !e.date?"يجب ادخال تاريخ الفترة":"يجب ادخال المستوى"
      this.uiService.showError("",message)
    }
    
    else{
      this.date = this.accountReports.getDate(e.date)
      this.accountReports.removeObjEmptyAttributes(e);
      //this.filter = this.accountReports.shapingFilterObject(e);
      this.level = parseInt(e.level)
      this.groupToRoot = e.groupToRoot=='false'?false:true ;
      this.staticAccumulative = (e.staticAccumulative=='false'?false:true) ;
      this.entriesFilter.minDate = this.date.minDate
      this.entriesFilter.maxDate = this.date.maxDate
      if(e.accountsGroup||e.from || e.to){
        this.filter = this.accountReports.shapingFilterObject(e)
      }
      if(e.branchGroup || e.fromBranch || e.toBranch){
        this.entriesFilter =  this.accountReports.shapingBranchFilterObject(e)

      }
      if(e.journalEntryStatus && e.journalEntryStatus !== 'all' ){
        this.entriesFilter.journalEntryStatus = e.journalEntryStatus
      }

      this.getDate({
        "detailedAccountFilter":{
          
        },
        entriesFilter: this.entriesFilter,
        "mainAccountFilter":this.filter,
        groupOnLevel: this.level,
        "groupToRoot" : this.groupToRoot
      },1)
    }
  }

  getDate(body = {},page?:number) {
    let receivedData;
    this.data.postWithoutSpinner(GroupedAccountReportsApi+"?limit=10&page="+page, body).subscribe((data:any[])=> {
      // fetching data from server response
      // reconstructing server response to match predefined table-headers criteria  (tablesData)
      if(data.length !=0){
        this.pageSize= data[0].limit;
        this.pageTotal= data[0].count;
        this.currentPage = data[0].currentPage;
        receivedData = data[0].results;
        console.log("thisdata " ,data)
        this.tablesData=this.accountReports.shapingGroupedAccumulativeAccount(receivedData,this.staticAccumulative)
        console.log("tablesData : " ,this.tablesData);
        if(this.tablesData.length == 0){
          this.spinner=false;
          this.noData = true
        }else{
          this.noData = false
          this.spinner = false;
        }
      }else{
        this.spinner=false;
        this.noData = true
      }
    });
  }

  paginate(pageNumber){
    this.currentPage = pageNumber;
    this.spinner = true;

    //console.log(minDate , maxDate)
    this.getDate({
      "detailedAccountFilter":{
    
      },
      entriesFilter: this.entriesFilter,
      "mainAccountFilter":this.filter,
      groupOnLevel: this.level,
      "groupToRoot" : this.groupToRoot
    },pageNumber);    
  }

}
