import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedCostCenterComponent } from './grouped-cost-center.component';

describe('GroupedCostCenterComponent', () => {
  let component: GroupedCostCenterComponent;
  let fixture: ComponentFixture<GroupedCostCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupedCostCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedCostCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
