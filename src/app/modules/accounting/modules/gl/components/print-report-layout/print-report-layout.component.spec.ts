import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintReportLayoutComponent } from './print-report-layout.component';

describe('PrintReportLayoutComponent', () => {
  let component: PrintReportLayoutComponent;
  let fixture: ComponentFixture<PrintReportLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintReportLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintReportLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
