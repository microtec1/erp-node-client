import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { UiService } from 'src/app/common/services/ui/ui.service';

@Component({
  selector: 'app-print-report-layout',
  templateUrl: './print-report-layout.component.html',
  styleUrls: ['./print-report-layout.component.scss']
})
export class PrintReportLayoutComponent implements OnInit {


  constructor(
    private uiService:UiService,
  ) { }

  ngOnInit() {
  }

}
