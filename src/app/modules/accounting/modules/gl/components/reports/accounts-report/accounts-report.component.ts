import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { ReportsAccountStatementApi, GroupedAccountReportsApi } from "src/app/common/constants/api.constants";
import { AccountReportsService } from '../../../services/account-reports.service';
import { FormGroup, FormControl } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { DataService } from 'src/app/common/services/shared/data.service';



@Component({
  selector: "app-accounts-report",
  templateUrl: "./accounts-report.component.html",
  styleUrls: ["./accounts-report.component.scss"]
})
export class AccountsReportComponent implements OnInit{

  // ui components configuration
  filterConfig = {
    lockups: [
      {
        label: "REPORTS.glAccount",
        lockupName: "chartOfAccounts",
        placeholder:"AccountClassification.selectAccount",
        childs:[
          {
            label: "AccountClassification.from",
            lockupName: "from",
          },
          {
            label: "AccountClassification.to",
            lockupName: "to",
          }
        ]
      },
      {
        label: "REPORTS.glAccount",
        lockupName: "accountsGroup",
        placeholder:"AccountClassification.selectGroupOfAccounts",
        childs:[
          {
            label: "AccountClassification.accountsGroup",
            lockupName: "accountsGroup"
          }
        ]
      },
      {
        label: "REPORTS.glAccount",
        lockupName: "branchGroup",
        placeholder:"AccountClassification.selectBranch",
        childs:[
          {
            label: "AccountClassification.branchGroup",
            lockupName: "branch"
          }
        ]
      },
      {
        label: "REPORTS.glAccount",
        lockupName: "branch",
        placeholder:"AccountClassification.selectBranch",
        childs:[
          {
            label: "AccountClassification.from",
            lockupName: "fromBranch",
          },
          {
            label: "AccountClassification.to",
            lockupName: "toBranch",
          }
        ]
      },
      {
        label: "REPORTS.glAccount",
        lockupName: "costCenter",
        placeholder:"AccountClassification.costCenter",
        childs:[
          {
            label: "AccountClassification.costCenter",
            lockupName: "costCenter",
          }
        ]
      }
      
    ],
    radios: [
      {
        name:"journalEntryStatus",
        label:"AccountClassification.journalEntryStatus",
        choices:[
          {
            name:"all",
            text:"AccountClassification.all",
            value:"all"
          },
          {
            name:"posted",
            text:"AccountClassification.posted",
            value:"posted"
          },
          {
            name:"unposted",
            text:"AccountClassification.unposted",
            value:"unposted"
          }
        ]
      },
      {
        name:"grouped",
        label:"AccountClassification.displayedWay",
        choices:[
          {
            name:"detailed",
            text:"AccountClassification.detailed",
            value:"false"
          },
          {
            name:"grouped",
            text:"AccountClassification.grouped",
            value:"true"
          }
        ]
      },
      {
        name:"reactiveGroupToRoot",
        label:"AccountClassification.level",
        choices:[
          {
            name:"currentLevel",
            text:"AccountClassification.currentLevel",
            value:"false"
          },
          {
            name:"toRoot",
            text:"AccountClassification.toRoot",
            value:"true"
          }
        ]
      },
      {
        name:"accumulative",
        label:"AccountClassification.accumulative",
        choices:[
          {
            name:"accumulative",
            text:"AccountClassification.accumulative",
            value:"true"
          },
          {
            name:"period",
            text:"AccountClassification.period",
            value:"false"
          }
        ]
      },
      
    ],
    inputs:[
      {
        name: "reactiveLevel",
        label: "AccountClassification.level",
        lockupName: "level",
      }
    ],
    date: {
      type: "ranged"
      // other type if single
    }
  };

  tablesData = [];
  spinner :boolean ;
  noData :boolean = true;
  pageTotal
  currentPage
  pageSize
  date:{minDate,maxDate}
  filter:any
  entriesFilter :any={};
  costCenterFilter :any={};
  costCenter:boolean;
  grouped ;
  groupToRoot;
  level;
  accumulative;
  collapsed:boolean=true;

  // filter Form initalization 
  filterFormConfig = new FormGroup({
    date: new FormControl(''),
    grouped : new FormControl(''),
    reactiveLevel:new FormControl(''),
    reactiveGroupToRoot :new FormControl(''),
    accumulative:new FormControl(''),
    chartOfAccounts:new FormControl(''),
    from: new FormControl(),
    to : new FormControl(),
    accountsGroup : new FormControl(),
    branch : new FormControl(),
    branchGroup : new FormControl(),
    fromBranch : new FormControl(),
    toBranch : new FormControl(),
    journalEntryStatus : new FormControl('all'),
    costCenter : new FormControl(),
  })
  
  defaultColDef = {
    sortable: true,
    resizable: true,
  };

  constructor(
    private data: DataService,
    private accountReports:AccountReportsService,
    private uiService:UiService
  ) { }

  ngOnInit(){
    this.uiService.isCollapsed.next(true);
    this.uiService.isCollapsed.subscribe(collapsed=>this.collapsed=collapsed)
  }
  
  // send form body to the endpoint
  onChange(e){
    this.noData = false;
    this.spinner =true;
    this.grouped = (!e.grouped?false:(e.grouped=='false'?false:true))
    this.costCenter = !!e.costCenter ? true : false ;
    console.log(e)
    if(Object.entries(e).length===0 || !e.date ){
      this.noData = true;
      this.spinner = false;
      let message = "يجب ادخال تاريخ الفترة"
      this.uiService.showError("",message)
    }
    else{
      this.date=this.accountReports.getDate(e.date);
      
      if(this.grouped==true){
        this.level = parseInt(e.reactiveLevel);
        this.groupToRoot = (e.reactiveGroupToRoot=='false'?false:true) ;
        this.accumulative = (e.accumulative=='false'?false:true) ;
        this.filter = {}
        this.entriesFilter.minDate = this.date.minDate
        this.entriesFilter.maxDate = this.date.maxDate
        if(e.accountsGroup||e.from || e.to){
          this.filter = this.accountReports.shapingFilterObject(e)
        }
        if(e.branchGroup || e.fromBranch || e.toBranch){
          this.entriesFilter =  this.accountReports.shapingBranchFilterObject(e)
        } 
        if(e.journalEntryStatus !== 'all' ){
          this.entriesFilter.journalEntryStatus = e.journalEntryStatus
        }
        console.log("filter" ,this.filter),
        console.log("entries filter" ,this.entriesFilter)
        this.getDate({
          "detailedAccountFilter":{
        
          },
          entriesFilter: this.entriesFilter,
          mainAccountFilter:this.filter,
          groupOnLevel : this.level,
          groupToRoot : this.groupToRoot
        },1)
      }
      else{
        this.accountReports.removeObjEmptyAttributes(e);
        this.filter = this.accountReports.shapingFilterObject(e);
        this.entriesFilter =  this.accountReports.shapingBranchFilterObject(e)
        this.entriesFilter.minDate = this.date.minDate
        this.entriesFilter.maxDate = this.date.maxDate
        if(e.journalEntryStatus !== 'all' ){
          this.entriesFilter.journalEntryStatus = e.journalEntryStatus
        }
        if(!!e.costCenter){
          this.costCenterFilter = {
            "IN":{
              "costCenterId":e.costCenter
            }
          }
        }
        this.getDate({
          filter: this.filter,
          entriesFilter: this.entriesFilter,
          costCenterFilter : this.costCenterFilter
        },1 );
      }
    }
  }
  
  getDate(body = {},page?:number) {
    let receivedData;
    let url = this.grouped == true ? GroupedAccountReportsApi :ReportsAccountStatementApi
    let limit = this.grouped == true ? 10 : 1 ;
    this.data.postWithoutSpinner(url+"?limit="+limit+"&page="+page, body).subscribe((data:any[])=> {
      if(data.length !=0){
        this.pageSize= data[0].limit;
        this.pageTotal= data[0].count;
        this.currentPage = data[0].currentPage;
        // fetching data from server response
        receivedData = data[0].results;
        console.log("recieved Data : ", receivedData)
        // reconstructing server response to match predefined table-headers criteria  (tablesData)
        if(this.grouped==true){
          if(this.accumulative != undefined || this.accumulative != null){
            this.tablesData=this.accountReports.shapingGroupedAccumulativeAccount(receivedData,this.accumulative)
          }else{
            this.tablesData=this.accountReports.shapingGroupedAccount (receivedData)
          }
        }
        else{
          this.tablesData=this.accountReports.shapingAccountsReportTable (receivedData)
          console.log("teessst " ,this.tablesData)
        }
        if(this.tablesData.length == 0){
          this.spinner=false;
          this.noData = true
        }else{
          this.noData = false
          this.spinner = false;
        }
      }else{
        this.spinner=false;
        this.noData = true
      }
    });
    
  }

  paginate(pageNumber){
    this.currentPage = pageNumber;
    this.spinner = true;

    if(this.grouped){
      this.getDate({
        "detailedAccountFilter":{
      
        },
        entriesFilter: this.entriesFilter,
        "mainAccountFilter":this.filter,
        groupOnLevel : this.level,
        "groupToRoot" : this.groupToRoot
      },pageNumber);  
    }
    else{
      this.getDate({
        filter: this.filter,
        entriesFilter: this.entriesFilter,
        costCenterFilter : this.costCenterFilter

      },pageNumber); 
    }
     
  }

}
