import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenueListComparisonComponent } from './revenue-list-comparison.component';

describe('RevenueListComparisonComponent', () => {
  let component: RevenueListComparisonComponent;
  let fixture: ComponentFixture<RevenueListComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevenueListComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevenueListComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
