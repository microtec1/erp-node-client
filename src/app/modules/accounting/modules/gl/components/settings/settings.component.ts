import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { FormGroup, FormControl, } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { searchLength, companyId, branchId } from 'src/app/common/constants/general.constants';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { IGlAccountSettings } from '../../interfaces/ISettings';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { Router } from '@angular/router';
import { IChartOfAccount } from '../../interfaces/IChartOfAccount';
import { DataService } from 'src/app/common/services/shared/data.service';
import { getGlSettingsApi, safeBoxesApi, banksApi, detailedChartOfAccountsApi, glSettingsApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  settingsForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  settings: IGlAccountSettings;
  chartOfAccounts: IChartOfAccount[] = [];
  noChartOfAccounts: boolean;
  chartOfAccountsCount: number;
  netOfProfitAndLoseAccountsInputFocused: boolean;
  approximateCalculationAccountsInputFocused: boolean;
  currenciesTransferAccountsInputFocused: boolean;
  addedTaxesValueAccountsInputFocused: boolean;
  financialTransferAccountsInputFocused: boolean;
  bankCommissionsTaxAccountsInputFocused: boolean;
  downPaymentTaxAccountsInputFocused: boolean;
  underPaymentChequesAccountsInputFocused: boolean;
  underCollectingChequesAccountsInputFocused: boolean;
  formReady: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Safe Boxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.data.post(getGlSettingsApi, {}).subscribe((res: IGlAccountSettings) => {
        this.settings = res;
        this.initForm();
        this.formReady = true;
        this.uiService.isLoading.next(false);
      })
    );
    this.formReady = true;
    this.initForm();

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

  }

  get form() {
    return this.settingsForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  changeLevelLength(checked) {
    if (!checked) {
      this.settingsForm.patchValue({
        company: {
          levelLength: null
        }
      });
    } else {
      this.settingsForm.patchValue({
        company: {
          levelLength: this.settings.company.levelLength
        }
      });
    }

  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.settingsForm.valid) {
      const newSettings = {
        companyId,
        ...this.generalService.checkEmptyFields(this.settingsForm.value)
      };
      if (!newSettings.company.generateCodesOfAccountsAndCostCentersAutomatically) {
        delete newSettings.company.levelLength;
      }

      this.data.post(glSettingsApi, newSettings).subscribe(res => {
        this.uiService.isLoading.next(false);
        this.submitted = false;
        this.loadingButton = false;
        this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        this.subscriptions.push(
          this.data.post(getGlSettingsApi, {}).subscribe((data: IGlAccountSettings) => {
            const settings = JSON.stringify(data);
            localStorage.setItem('glSettings', settings);
            this.uiService.isLoading.next(false);
          })
        );
      },
        err => {
          this.loadingButton = false;
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        });

    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
    }
  }

  private initForm() {
    let linkCashWithAccounts = false;
    let linkInventoryWithAccounts = false;
    let paymentsAndTransactionsOfASpecificBranchWithOtherBranches = false;
    let linkAccountsWithIncomeStatementClassificationAndBalanceSheetClassification = false;
    let generateCodesOfAccountsAndCostCentersAutomatically = false;
    let levelLength = null;
    let netOfProfitAndLoseAccountId = '';
    let cashBoxId = '';
    let bankId = '';
    let linkPayableReceiptsWithActualFiles = false;
    let linkCashReceiptsWithActualFiles = false;
    let copyDescriptionOfPostPartnersFromTransactionDescriptions = false;
    let addAdditionalCostCentersWithItsEntries = false;
    let printPayableAndReceivableReceiptsDirectly = false;
    let notAllowedToEditInFinancialTransactions = false;
    let showDraftInformationAtJournalEntriesScreen = false;
    let notAllowToDuplicateInternalReferenceInJournalEntry = false;
    let approximateCalculationAccountId = '';
    let currenciesTransferAccountId = '';
    let addedTaxesValueAccountId = '';
    let financialTransferAccountId = '';
    let bankCommissionsTaxAccountId = '';
    let downPaymentTaxAccountId = '';
    let underPaymentChequesAccountId = '';
    let underCollectingChequesAccountsId = '';

    if (this.settings) {
      linkCashWithAccounts = this.settings.company.linkCashWithAccounts;
      linkInventoryWithAccounts = this.settings.company.linkInventoryWithAccounts;
      paymentsAndTransactionsOfASpecificBranchWithOtherBranches =
        this.settings.company.paymentsAndTransactionsOfASpecificBranchWithOtherBranches;
      linkAccountsWithIncomeStatementClassificationAndBalanceSheetClassification =
        this.settings.company.linkAccountsWithIncomeStatementClassificationAndBalanceSheetClassification;
      generateCodesOfAccountsAndCostCentersAutomatically = this.settings.company.generateCodesOfAccountsAndCostCentersAutomatically;
      levelLength = this.settings.company.levelLength;
      netOfProfitAndLoseAccountId = this.settings.company.netOfProfitAndLoseAccountId;
      cashBoxId = this.settings.branch.cashBoxId;
      bankId = this.settings.branch.bankId;
      linkPayableReceiptsWithActualFiles = this.settings.branch.linkPayableReceiptsWithActualFiles;
      linkCashReceiptsWithActualFiles = this.settings.branch.linkCashReceiptsWithActualFiles;
      copyDescriptionOfPostPartnersFromTransactionDescriptions =
        this.settings.branch.copyDescriptionOfPostPartnersFromTransactionDescriptions;
      addAdditionalCostCentersWithItsEntries = this.settings.branch.addAdditionalCostCentersWithItsEntries;
      printPayableAndReceivableReceiptsDirectly = this.settings.branch.printPayableAndReceivableReceiptsDirectly;
      notAllowedToEditInFinancialTransactions = this.settings.branch.notAllowedToEditInFinancialTransactions;
      showDraftInformationAtJournalEntriesScreen = this.settings.branch.showDraftInformationAtJournalEntriesScreen;
      notAllowToDuplicateInternalReferenceInJournalEntry = this.settings.branch.notAllowToDuplicateInternalReferenceInJournalEntry;
      approximateCalculationAccountId = this.settings.branch.approximateCalculationAccountId;
      currenciesTransferAccountId = this.settings.branch.currenciesTransferAccountId;
      addedTaxesValueAccountId = this.settings.branch.addedTaxesValueAccountId;
      financialTransferAccountId = this.settings.branch.financialTransferAccountId;
      bankCommissionsTaxAccountId = this.settings.branch.bankCommissionsTaxAccountId;
      downPaymentTaxAccountId = this.settings.branch.downPaymentTaxAccountId;
      underPaymentChequesAccountId = this.settings.branch.underPaymentChequesAccountId;
      underCollectingChequesAccountsId = this.settings.branch.underCollectingChequesAccountsId;
    }

    this.settingsForm = new FormGroup({
      company: new FormGroup({
        linkCashWithAccounts: new FormControl(linkCashWithAccounts),
        linkInventoryWithAccounts: new FormControl(linkInventoryWithAccounts),
        paymentsAndTransactionsOfASpecificBranchWithOtherBranches:
          new FormControl(paymentsAndTransactionsOfASpecificBranchWithOtherBranches),
        linkAccountsWithIncomeStatementClassificationAndBalanceSheetClassification:
          new FormControl(linkAccountsWithIncomeStatementClassificationAndBalanceSheetClassification),
        generateCodesOfAccountsAndCostCentersAutomatically: new FormControl(generateCodesOfAccountsAndCostCentersAutomatically),
        levelLength: new FormControl(levelLength, CustomValidators.number),
        netOfProfitAndLoseAccountId: new FormControl(netOfProfitAndLoseAccountId),
      }),
      branch: new FormGroup({
        branchId: new FormControl(branchId),
        cashBoxId: new FormControl(cashBoxId),
        bankId: new FormControl(bankId),
        linkPayableReceiptsWithActualFiles: new FormControl(linkPayableReceiptsWithActualFiles),
        linkCashReceiptsWithActualFiles: new FormControl(linkCashReceiptsWithActualFiles),
        copyDescriptionOfPostPartnersFromTransactionDescriptions:
          new FormControl(copyDescriptionOfPostPartnersFromTransactionDescriptions),
        addAdditionalCostCentersWithItsEntries: new FormControl(addAdditionalCostCentersWithItsEntries),
        printPayableAndReceivableReceiptsDirectly: new FormControl(printPayableAndReceivableReceiptsDirectly),
        notAllowedToEditInFinancialTransactions: new FormControl(notAllowedToEditInFinancialTransactions),
        showDraftInformationAtJournalEntriesScreen: new FormControl(showDraftInformationAtJournalEntriesScreen),
        notAllowToDuplicateInternalReferenceInJournalEntry: new FormControl(notAllowToDuplicateInternalReferenceInJournalEntry),
        approximateCalculationAccountId: new FormControl(approximateCalculationAccountId),
        currenciesTransferAccountId: new FormControl(currenciesTransferAccountId),
        addedTaxesValueAccountId: new FormControl(addedTaxesValueAccountId),
        financialTransferAccountId: new FormControl(financialTransferAccountId),
        bankCommissionsTaxAccountId: new FormControl(bankCommissionsTaxAccountId),
        downPaymentTaxAccountId: new FormControl(downPaymentTaxAccountId),
        underPaymentChequesAccountId: new FormControl(underPaymentChequesAccountId),
        underCollectingChequesAccountsId: new FormControl(underCollectingChequesAccountsId),
      })
    });
  }

  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true;
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(safeBoxesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSafeBoxes = true;
          } else {
            this.noSafeBoxes = false;
            for (const item of res.results) {
              if (this.safeBoxes.length) {
                const uniqueSafeBox = this.safeBoxes.filter(x => x.safeBoxNameAr !== item.safeBoxNameAr);
                this.safeBoxes = uniqueSafeBox;
              }
              this.safeBoxes.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreSafeboxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data.get(safeBoxesApi, this.selectedSafeBoxesPage).subscribe((res: IDataRes) => {
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        } else {
          this.hasMoreSafeBoxes = false;
        }
        for (const item of res.results) {
          if (this.safeBoxes.length) {
            const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
            this.safeBoxes = uniqueSafeBoxes;
          }
          this.safeBoxes.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(banksApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noBanks = true;
          } else {
            this.noBanks = false;
            for (const item of res.results) {
              if (this.banks.length) {
                const uniqueBank = this.banks.filter(x => x._id !== item._id);
                this.banks = uniqueBank;
              }
              this.banks.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data.get(banksApi, this.selectedBanksPage).subscribe((res: IDataRes) => {
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        } else {
          this.hasMoreBanks = false;
        }
        for (const item of res.results) {
          if (this.banks.length) {
            const uniqueBanks = this.banks.filter(x => x._id !== item._id);
            this.banks = uniqueBanks;
          }
          this.banks.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
