import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalTrailBalanceComponent } from './total-trail-balance.component';

describe('TotalTrailBalanceComponent', () => {
  let component: TotalTrailBalanceComponent;
  let fixture: ComponentFixture<TotalTrailBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalTrailBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalTrailBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
