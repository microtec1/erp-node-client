import { Component, OnInit, OnDestroy } from '@angular/core';
import { IOpeningBalance } from '../../../interfaces/IOpeningBalance';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup } from '@angular/forms';
import { baseUrl, accountsOpeningBalancesApi, unPostAccountsOpeningBalancesApi, postAccountsOpeningBalancesApi } from 'src/app/common/constants/api.constants';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-accounts-opening-balances',
  templateUrl: './accounts-opening-balances.component.html',
  styleUrls: ['./accounts-opening-balances.component.scss']
})
export class AccountsOpeningBalancesComponent implements OnInit, OnDestroy {
  openingBalances: IOpeningBalance[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;
  noAdd: boolean;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getOpeningBalancesFirstPage();
    if (this.openingBalances) {
      this.openingBalances.map(item => {
        if (item.accountsOpeningBalancesStatus === 'posted') {
          this.noAdd = true;
        } else {
          this.noAdd = false;
        }
      });
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postAccountsOpeningBalancesApi}/${id}`, {}).subscribe(res => {
        this.openingBalances = this.generalService.changeStatus(this.openingBalances, id, 'posted', 'accountsOpeningBalancesStatus');
        this.openingBalances.map(item => {
          if (item.accountsOpeningBalancesStatus === 'posted') {
            this.noAdd = true;
          } else {
            this.noAdd = false;
          }
        });
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostAccountsOpeningBalancesApi}/${id}`, {}).subscribe(res => {
        this.openingBalances = this.generalService.changeStatus(this.openingBalances, id, 'unposted', 'accountsOpeningBalancesStatus');
        this.openingBalances.map(item => {
          if (item.accountsOpeningBalancesStatus === 'posted') {
            this.noAdd = true;
          } else {
            this.noAdd = false;
          }
        });
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getOpeningBalancesFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(accountsOpeningBalancesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.openingBalances = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  // private initSearchForm() {
  //   this.searchForm = new FormGroup({
  //     openingBalanceCode: new FormControl(''),
  //     openingBalanceNameAr: new FormControl(''),
  //     openingBalanceNameEn: new FormControl('')
  //   });
  // }

  deleteModal(openingBalance: IOpeningBalance) {
    // const initialState = {
    //   code: openingBalance.openingBalanceCode,
    //   nameAr: openingBalance.openingBalanceNameAr,
    //   nameEn: openingBalance.openingBalanceNameEn
    // };
    // this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    // this.subscriptions.push(
    //   this.bsModalRef.content.confirmed.subscribe(confirmed => {
    //     if (confirmed) {
    //       this.delete(openingBalance._id);
    //       this.bsModalRef.hide();
    //     } else {
    //       this.bsModalRef.hide();
    //     }
    //   })
    // );
  }


  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(accountsOpeningBalancesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.openingBalances = this.generalService.removeItem(this.openingBalances, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getOpeningBalancesFirstPage() {
    this.subscriptions.push(
      this.data.getBasicData(accountsOpeningBalancesApi, 1).subscribe((res: IDataRes) => {
        this.openingBalances = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(accountsOpeningBalancesApi, pageNumber).subscribe((res: IDataRes) => {
      this.openingBalances = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
