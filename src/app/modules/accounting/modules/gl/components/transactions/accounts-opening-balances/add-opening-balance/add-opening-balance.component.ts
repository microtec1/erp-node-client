import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { baseUrl, accountsOpeningBalancesApi, branchCurrenciesApi, fiscalYearsApi, detailedCostCentersApi, detailedChartOfAccountsApi } from 'src/app/common/constants/api.constants';
import {
  companyId,
  branchId
} from 'src/app/common/constants/general.constants';
import { IOpeningBalance } from '../../../../interfaces/IOpeningBalance';
import { IFinancialYear } from 'src/app/modules/general/interfaces/IFinancialYear';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IChartOfAccount } from '../../../../interfaces/IChartOfAccount';
import { ICostCenter } from '../../../../interfaces/ICostCenter';
import { ICompany } from 'src/app/modules/general/interfaces/ICompany';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-opening-balance',
  templateUrl: './add-opening-balance.component.html',
  styleUrls: ['./add-opening-balance.component.scss']
})
export class AddOpeningBalanceComponent implements OnInit, OnDestroy {
  openingBalanceForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  openingBalance: IOpeningBalance;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  branchId = branchId;
  detailsMode: boolean;

  // Financial Years
  financialYears: IFinancialYear[] = [];
  financialYearInputFocused: boolean;
  financialYearsCount: number;
  noFinancialYears: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Cost Centers
  costCenters: ICostCenter[] = [];
  costCenterInputFocused: boolean;
  costCentersCount: number;
  noCostCenters: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  hasMoreCurrencies: boolean;
  currenciesCount: number;
  selectedCurrenciesPage = 1;
  currenciesPagesNo: number;
  noCurrencies: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(accountsOpeningBalancesApi, null, null, id).subscribe(
              (openingBalance: IOpeningBalance) => {
                this.openingBalance = openingBalance;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.openingBalanceForm.disable({ onlySelf: true });
                }
                this.syncControls();
                this.uiService.isLoading.next(false);
              }
            )
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.syncControls();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    const search = {
      companyId,
      fiscalYearStatus: 'opened'
    };
    this.subscriptions.push(
      this.data
        .get(fiscalYearsApi, null, search)
        .subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.financialYears = res.results;
            this.financialYearsCount = res.results.length;
          } else {
            this.noFinancialYears = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe(
        (res: IChartOfAccount[]) => {
          if (res.length) {
            this.chartOfAccounts.push(...res);
            this.chartOfAccountsCount = res.length;
          } else {
            this.noChartOfAccounts = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );

    this.subscriptions.push(
      this.data.get(detailedCostCentersApi, null, null, companyId).subscribe((res: ICostCenter[]) => {
        if (res.length) {
          this.costCenters.push(...res);
          this.costCentersCount = res.length;
        } else {
          this.noCostCenters = true;
        }

        this.uiService.isLoading.next(false);
      })
    );
  }

  syncControls() {
    this.openingBalanceForm.controls.accountsOpeningBalancesDetails.valueChanges.subscribe(res => {
      const debitِArray = [];
      const creditArray = [];
      for (let i = 0; i <= res.length - 1; i++) {
        if (res[i]) {
          if (res[i].debit !== null) {
            debitِArray.push(res[i].debit * res[i].exchangeRate);
          }
          if (res[i].credit !== null) {
            creditArray.push(res[i].credit * res[i].exchangeRate);
          }
        }
      }
      const debitSum = debitِArray.reduce((acc, cur) => acc + cur, 0);
      const creditSum = creditArray.reduce((acc, cur) => acc + cur, 0);

      this.openingBalanceForm.patchValue({
        accountsOpeningBalancesTotalDebit: debitSum,
        accountsOpeningBalancesTotalCredit: creditSum,
        accountsOpeningBalancesDifference: Math.abs(creditSum - debitSum)
      });
    });
  }

  fillRate(event, formGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        exchangeRate: currency.exchangeRate
      });
    } else {
      this.openingBalanceForm.patchValue({
        exchangeRate: currency.exchangeRate
      });
    }
  }

  clearValue(array: FormArray, index, fieldName, value) {
    const group = array.controls[index] as FormGroup;
    if (fieldName === 'debit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        credit: 0,
      });
    }
    if (fieldName === 'credit' && !group.controls[fieldName].errors) {
      array.controls[index].patchValue({
        debit: 0,
      });
    }
  }

  get form() {
    return this.openingBalanceForm.controls;
  }

  get getAccountsOpeningBalancesDetailsArray() {
    return this.openingBalanceForm.get('accountsOpeningBalancesDetails') as FormArray;
  }

  addBalance() {
    const control = this.openingBalanceForm.get('accountsOpeningBalancesDetails') as FormArray;
    control.push(
      new FormGroup({
        accountId: new FormControl(null),
        currencyId: new FormControl(null),
        exchangeRate: new FormControl(null),
        debit: new FormControl(null),
        credit: new FormControl(null),
        costCenterId: new FormControl(null)
      })
    );
  }

  deleteBalance(index) {
    const control = this.openingBalanceForm.get('accountsOpeningBalancesDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.openingBalance) {
      if (this.openingBalanceForm.valid) {
        const newOpeningBalance = {
          _id: this.openingBalance._id,
          ...this.generalService.checkEmptyFields(
            this.openingBalanceForm.value
          )
        };
        this.subscriptions.push(
          this.data.put(accountsOpeningBalancesApi, newOpeningBalance).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/gl/accountsOpeningBalances']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          }, err => {
            this.uiService.isLoading.next(false);
            this.loadingButton = false;
            this.uiService.showErrorMessage(err);
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.openingBalanceForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(
            this.openingBalanceForm.value
          )
        };
        this.subscriptions.push(
          this.data.post(accountsOpeningBalancesApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.openingBalanceForm.reset();
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let accountsOpeningBalancesFiscalYearId = '';
    let accountsOpeningBalancesTotalDebit = 0;
    let accountsOpeningBalancesTotalCredit = 0;
    let accountsOpeningBalancesDifference = 0;
    let isActive = true;
    let accountsOpeningBalancesDetailsArray = new FormArray([
      new FormGroup({
        accountId: new FormControl(null),
        currencyId: new FormControl(null),
        exchangeRate: new FormControl(null),
        debit: new FormControl(null),
        credit: new FormControl(null),
        costCenterId: new FormControl(null)
      })
    ]);

    if (this.openingBalance) {
      accountsOpeningBalancesDetailsArray = new FormArray([]);
      accountsOpeningBalancesFiscalYearId = this.openingBalance.accountsOpeningBalancesFiscalYearId;
      accountsOpeningBalancesTotalDebit = this.openingBalance.accountsOpeningBalancesTotalDebit;
      accountsOpeningBalancesTotalCredit = this.openingBalance.accountsOpeningBalancesTotalCredit;
      accountsOpeningBalancesDifference = this.openingBalance.accountsOpeningBalancesDifference;
      isActive = this.openingBalance.isActive;
      for (const item of this.openingBalance.accountsOpeningBalancesDetails) {
        accountsOpeningBalancesDetailsArray.push(
          new FormGroup({
            accountId: new FormControl(item.accountId),
            currencyId: new FormControl(item.currencyId),
            exchangeRate: new FormControl(item.exchangeRate),
            debit: new FormControl(item.debit),
            credit: new FormControl(item.credit),
            costCenterId: new FormControl(item.costCenterId)
          })
        );
      }
    }

    this.openingBalanceForm = new FormGroup({
      accountsOpeningBalancesFiscalYearId: new FormControl(
        accountsOpeningBalancesFiscalYearId
      ),
      accountsOpeningBalancesTotalDebit: new FormControl(
        accountsOpeningBalancesTotalDebit
      ),
      accountsOpeningBalancesTotalCredit: new FormControl(
        accountsOpeningBalancesTotalCredit
      ),
      accountsOpeningBalancesDifference: new FormControl(
        accountsOpeningBalancesDifference
      ),
      isActive: new FormControl(isActive),
      accountsOpeningBalancesDetails: accountsOpeningBalancesDetailsArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
