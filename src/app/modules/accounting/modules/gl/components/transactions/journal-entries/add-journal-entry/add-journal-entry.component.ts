import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { Router, ActivatedRoute, Params, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, branchId } from 'src/app/common/constants/general.constants';
import { baseUrl, journalEntriesApi, detailedCostCentersApi, detailedChartOfAccountsApi, branchCurrenciesApi, taxesApi } from 'src/app/common/constants/api.constants';
import { IJournalEntry } from '../../../../interfaces/IJournalEntry';
import { IChartOfAccount } from '../../../../interfaces/IChartOfAccount';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICostCenter } from '../../../../interfaces/ICostCenter';
import { ITax } from 'src/app/modules/sales/interfaces/ITax';
// tslint:disable-next-line:max-line-length
import { CostCentersModalComponent } from 'src/app/modules/accounting/modules/gl/components/files/cost-centers-modal/cost-centers-modal.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/app/common/services/shared/data.service';
import { EntriesModalComponent } from 'src/app/common/components/entries-modal/entries-modal.component';

@Component({
  selector: 'app-add-journal-entry',
  templateUrl: './add-journal-entry.component.html',
  styleUrls: ['./add-journal-entry.component.scss']
})

export class AddJournalEntryComponent implements OnInit, OnDestroy {
  journalEntryForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  journalEntry: IJournalEntry;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  bsModalRef: BsModalRef;
  detailsMode: boolean;
  companyId = companyId;
  reversedEntry: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Cost Centers
  costCenters: ICostCenter[] = [];
  costCenterInputFocused: boolean;
  costCenterInputFocused2: boolean;
  costCentersCount: number;
  noCostCenters: boolean;

  // Taxes
  taxes: ITax[] = [];
  taxesInputFocused: boolean;
  hasMoreTaxes: boolean;
  taxesCount: number;
  selectedTaxesPage = 1;
  taxesPagesNo: number;
  noTaxes: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(journalEntriesApi, null, null, id)
              .subscribe((journalEntry: IJournalEntry) => {
                this.journalEntry = journalEntry;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.journalEntryForm.disable({ onlySelf: true });
                }
                this.syncControls();
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.journalEntryForm.updateValueAndValidity();
          this.syncControls();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(detailedCostCentersApi, null, null, companyId).subscribe((res: ICostCenter[]) => {
        if (res.length) {
          this.costCenters.push(...res);
          this.costCentersCount = res.length;
        } else {
          this.noCostCenters = true;
        }

        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe(
        (res: IChartOfAccount[]) => {
          if (res.length) {
            this.chartOfAccounts.push(...res);
            this.chartOfAccountsCount = res.length;
          } else {
            this.noChartOfAccounts = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    const searchBody = {
      taxType: 'add',
      companyId
    };
    this.subscriptions.push(
      this.data
        .get(taxesApi, null, searchBody)
        .subscribe((res: IDataRes) => {
          this.taxesPagesNo = res.pages;
          this.taxesCount = res.count;
          if (this.taxesPagesNo > this.selectedTaxesPage) {
            this.hasMoreTaxes = true;
          }
          this.taxes.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.journalEntryForm.controls;
  }

  get getJournalEntryGuideAccountsListArray() {
    return this.journalEntryForm.get('journalEntryGuideAccountsList') as FormArray;
  }

  addGuideAccount() {
    const control = this.journalEntryForm.get('journalEntryGuideAccountsList') as FormArray;
    control.push(
      new FormGroup({
        accountId: new FormControl(''),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        debit: new FormControl(null),
        credit: new FormControl(null),
        description: new FormControl(''),
        tax: new FormControl(''),
        documentNumber: new FormControl(''),
        costCenterList: new FormArray([]),
        costCenter2: new FormControl(''),
        referenceNumber: new FormControl(''),
        documentDateGregorian: new FormControl(null),
        documentDateHijri: new FormControl(null),
      })
    );
  }

  deleteGuideAccount(index) {
    const control = this.journalEntryForm.get('journalEntryGuideAccountsList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setEntryHijri(value: Date, formGroup?: FormGroup) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          documentDateHijri: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.journalEntryForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setEntryGeorian(value, formGroup: FormGroup) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          documentDateGregorian: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.journalEntryForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  fillExchangeRate(value, formGroup: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === value);
    formGroup.patchValue({
      exchangeRate: currency.exchangeRate
    });
  }

  clearValue(formGroup: FormGroup, fieldName) {
    if (fieldName === 'debit' && !formGroup.controls[fieldName].errors) {
      formGroup.patchValue({
        credit: 0,
      });
    }
    if (fieldName === 'credit' && !formGroup.controls[fieldName].errors) {
      formGroup.patchValue({
        debit: 0,
      });
    }
  }

  distributeCostCenters(accountId, debitValue, creditValue, index) {
    const account = this.chartOfAccounts.find(item => item._id === accountId);
    const formGroup: any = this.getJournalEntryGuideAccountsListArray.controls[index];
    const initialState = {
      data: account,
      value: debitValue > 0 ? debitValue : creditValue,
      costCentersValue: formGroup.controls.costCenterList.controls.length ? formGroup.controls.costCenterList.value : null
    };
    this.bsModalRef = this.modalService.show(CostCentersModalComponent, { initialState });
    this.bsModalRef.content.formValue.subscribe(res => {
      formGroup.controls.costCenterList.controls = [];
      for (const item of res.costCenterList) {
        formGroup.controls.costCenterList.push(
          new FormGroup({
            costCenterId: new FormControl(item.costCenterId),
            price: new FormControl(item.price),
          })
        );
      }
    });
  }

  loadReversedEntry() {
    const searchValues = {
      journalEntryStatus: 'posted',
      journalEntrySourceName: 'directEntry'
    };
    this.subscriptions.push(
      this.data.get(journalEntriesApi, null, searchValues).subscribe((res: IDataRes) => {
        this.uiService.isLoading.next(false);
        const initialState = {
          entries: res.results
        };
        this.bsModalRef = this.modalService.show(EntriesModalComponent, { initialState });
        this.bsModalRef.content.confirmed.subscribe(data => {
          if (data) {
            this.reversedEntry = true;
            const selectedEntry: IJournalEntry = res.results[data];
            this.journalEntryForm.patchValue({
              journalEntryReverseNumber: selectedEntry.code,
              journalEntryReverseDateGregorian: selectedEntry.gregorianDate,
              journalEntryReverseDateHijri: selectedEntry.hijriDate,
              journalEntryReverse: true
            });

            this.getJournalEntryGuideAccountsListArray.controls = [];
            for (const item of selectedEntry.journalEntryGuideAccountsList) {
              const costCenterListArray = new FormArray([]);
              for (const control of item.costCenterList) {
                costCenterListArray.push(
                  new FormGroup({
                    costCenterId: new FormControl(control.costCenterId),
                    price: new FormControl(control.price),
                  })
                );
              }
              this.getJournalEntryGuideAccountsListArray.push(
                new FormGroup({
                  accountId: new FormControl(item.accountId),
                  currencyId: new FormControl(item.currencyId),
                  exchangeRate: new FormControl(item.exchangeRate),
                  debit: new FormControl(item.credit),
                  credit: new FormControl(item.debit),
                  description: new FormControl(item.description),
                  costCenterList: costCenterListArray,
                  tax: new FormControl(item.tax),
                  costCenter: new FormControl(item.costCenter),
                  costCenter2: new FormControl(item.costCenter2),
                  documentNumber: new FormControl(item.documentNumber),
                  referenceNumber: new FormControl(item.referenceNumber),
                  documentDateGregorian: new FormControl(new Date(item.documentDateGregorian + 'UTC')),
                  documentDateHijri: new FormControl(item.documentDateHijri),
                })
              );
            }
            this.bsModalRef.hide();
          } else {
            this.bsModalRef.hide();
          }
        });
      })
    );

  }

  loadPeriodEntry() {
    const searchValues = {
      journalEntryStatus: 'posted',
      journalEntrySourceName: 'directEntry',
      journalEntryPeriodic: true,
    };
    this.subscriptions.push(
      this.data.get(journalEntriesApi, null, searchValues).subscribe((res: IDataRes) => {
        this.uiService.isLoading.next(false);
        const initialState = {
          entries: res.results
        };
        this.bsModalRef = this.modalService.show(EntriesModalComponent, { initialState });
        this.bsModalRef.content.confirmed.subscribe(data => {
          if (data) {
            this.reversedEntry = false;
            const selectedEntry: IJournalEntry = res.results[data];
            this.journalEntryForm.patchValue({
              journalEntryReverseNumber: '',
              journalEntryReverseDateGregorian: '',
              journalEntryReverseDateHijri: '',
              journalEntryReverse: false
            });

            this.getJournalEntryGuideAccountsListArray.controls = [];
            for (const item of selectedEntry.journalEntryGuideAccountsList) {
              const costCenterListArray = new FormArray([]);
              for (const control of item.costCenterList) {
                costCenterListArray.push(
                  new FormGroup({
                    costCenterId: new FormControl(control.costCenterId),
                    price: new FormControl(control.price),
                  })
                );
              }
              this.getJournalEntryGuideAccountsListArray.push(
                new FormGroup({
                  accountId: new FormControl(item.accountId),
                  currencyId: new FormControl(item.currencyId),
                  exchangeRate: new FormControl(item.exchangeRate),
                  debit: new FormControl(item.debit),
                  credit: new FormControl(item.credit),
                  description: new FormControl(item.description),
                  costCenterList: costCenterListArray,
                  tax: new FormControl(item.tax),
                  costCenter: new FormControl(item.costCenter),
                  costCenter2: new FormControl(item.costCenter2),
                  documentNumber: new FormControl(item.documentNumber),
                  referenceNumber: new FormControl(item.referenceNumber),
                  documentDateGregorian: new FormControl(new Date(item.documentDateGregorian + 'UTC')),
                  documentDateHijri: new FormControl(item.documentDateHijri),
                })
              );
            }
            this.bsModalRef.hide();
          } else {
            this.bsModalRef.hide();
          }
        });
      })
    );

  }

  syncControls() {
    this.journalEntryForm.controls.journalEntryGuideAccountsList.valueChanges.subscribe(res => {
      const debitِArray = [];
      const creditArray = [];
      for (let i = 0; i <= res.length - 1; i++) {
        if (res[i]) {
          if (res[i].debit !== null) {
            debitِArray.push(res[i].debit * res[i].exchangeRate);
          }
          if (res[i].credit !== null) {
            creditArray.push(res[i].credit * res[i].exchangeRate);
          }
        }
      }
      const debitSum = debitِArray.reduce((acc, cur) => acc + cur, 0);
      const creditSum = creditArray.reduce((acc, cur) => acc + cur, 0);

      this.journalEntryForm.patchValue({
        journalEntryTotalDebit: debitSum,
        journalEntryTotalCredit: creditSum,
        journalEntryDifference: Math.abs(creditSum - debitSum)
      });
    });
  }
  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (!this.reversedEntry) {
      delete this.journalEntryForm.value.journalEntryReverseNumber;
      delete this.journalEntryForm.value.journalEntryReverse;
      delete this.journalEntryForm.value.journalEntryReverseDateGregorian;
      delete this.journalEntryForm.value.journalEntryReverseDateHijri;
    }
    if (
      typeof this.journalEntryForm.value.gregorianDate !==
      'string'
    ) {
      this.journalEntryForm.value.gregorianDate = this.generalService.format(
        this.journalEntryForm.value.gregorianDate
      );
      this.journalEntryForm.value.hijriDate = this.generalService.formatHijriDate(
        this.journalEntryForm.value.hijriDate
      );
    }

    for (
      let i = 0;
      i <= this.journalEntryForm.value.journalEntryGuideAccountsList.length - 1;
      i++
    ) {
      if (this.journalEntryForm.value.journalEntryGuideAccountsList[i].documentDateGregorian) {
        if (
          typeof this.journalEntryForm.value.journalEntryGuideAccountsList[i].documentDateGregorian !==
          'string'
        ) {
          this.journalEntryForm.value.journalEntryGuideAccountsList[
            i
          ].documentDateGregorian = this.generalService.format(
            this.journalEntryForm.value.journalEntryGuideAccountsList[i].documentDateGregorian
          );
        }
        this.journalEntryForm.value.journalEntryGuideAccountsList[i] = {
          ...this.journalEntryForm.value.journalEntryGuideAccountsList[i],
          documentDateHijri: this.generalService.formatHijriDate(
            this.journalEntryForm.value.journalEntryGuideAccountsList[i].documentDateHijri
          )
        };
      }
    }
    if (this.journalEntry) {
      if (this.journalEntryForm.valid) {
        const newjournalEntry = {
          _id: this.journalEntry._id,
          ...this.generalService.checkEmptyFields(this.journalEntryForm.value)
        };
        this.subscriptions.push(
          this.data.put(journalEntriesApi, newjournalEntry).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/gl/journalEntries']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
            this.loadingButton = false;
          })
        );
        this.loadingButton = false;

      }
    } else {
      if (this.journalEntryForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.journalEntryForm.value)
        };
        console.log(formValue);

        this.subscriptions.push(
          this.data.post(journalEntriesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.journalEntryForm.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        const invalid = [];
        const controls = this.journalEntryForm.controls;
        for (const name in controls) {
          if (controls[name].invalid) {
            invalid.push(name);
          }
        }
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let journalEntryPeriodic = false;
    let journalEntryStatus = 'unposted';
    let journalEntryDescription = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let journalEntryFileNumber = '';
    let journalEntrySourceName = 'directEntry';
    let journalEntryReverseNumber = '';
    let journalEntryReverse = false;
    let journalEntryReverseDateGregorian = null;
    let journalEntryReverseDateHijri = null;
    let journalEntryTotalDebit = null;
    let journalEntryTotalCredit = null;
    let journalEntryDifference = null;
    let isActive = true;
    let costCenterListArray = new FormArray([]);
    let journalEntryGuideAccountsListArray = new FormArray([
      new FormGroup({
        accountId: new FormControl('', Validators.required),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        debit: new FormControl(null),
        credit: new FormControl(null),
        description: new FormControl(''),
        tax: new FormControl(''),
        documentNumber: new FormControl(''),
        costCenterList: costCenterListArray,
        // costCenter: new FormControl(''),
        costCenter2: new FormControl(''),
        referenceNumber: new FormControl(''),
        documentDateGregorian: new FormControl(null),
        documentDateHijri: new FormControl(null),
      })
    ]);

    if (this.journalEntry) {
      code = this.journalEntry.code;
      journalEntryPeriodic = this.journalEntry.journalEntryPeriodic;
      journalEntryStatus = this.journalEntry.journalEntryStatus;
      journalEntryDescription = this.journalEntry.journalEntryDescription;
      gregorianDate = new Date(this.journalEntry.gregorianDate + 'UTC');
      hijriDate = this.journalEntry.hijriDate;
      journalEntryFileNumber = this.journalEntry.journalEntryFileNumber;
      journalEntrySourceName = this.journalEntry.journalEntrySourceName;
      journalEntryTotalDebit = this.journalEntry.journalEntryTotalDebit;
      journalEntryTotalCredit = this.journalEntry.journalEntryTotalCredit;
      journalEntryDifference = this.journalEntry.journalEntryDifference;
      journalEntryReverseNumber = this.journalEntry.journalEntryReverseNumber;
      journalEntryReverse = this.journalEntry.journalEntryReverse;
      journalEntryReverseDateGregorian = new Date(this.journalEntry.journalEntryReverseDateGregorian + 'UTC');
      journalEntryReverseDateHijri = this.journalEntry.journalEntryReverseDateHijri;
      isActive = this.journalEntry.isActive;
      journalEntryGuideAccountsListArray = new FormArray([]);
      for (const item of this.journalEntry.journalEntryGuideAccountsList) {
        if (item.costCenterList.length) {
          costCenterListArray = new FormArray([]);

          for (const item2 of item.costCenterList) {
            costCenterListArray.push(
              new FormGroup({
                costCenterId: new FormControl(item2.costCenterId),
                price: new FormControl(item2.price),
              })
            );
          }
        }

        journalEntryGuideAccountsListArray.push(
          new FormGroup({
            accountId: new FormControl(item.accountId),
            currencyId: new FormControl(item.currencyId),
            exchangeRate: new FormControl(item.exchangeRate),
            debit: new FormControl(item.debit),
            credit: new FormControl(item.credit),
            description: new FormControl(item.description),
            tax: new FormControl(item.tax),
            documentNumber: new FormControl(item.documentNumber),
            costCenterList: costCenterListArray,
            costCenter2: new FormControl(item.costCenter2),
            referenceNumber: new FormControl(item.referenceNumber),
            documentDateGregorian: new FormControl(item.documentDateGregorian && new Date(item.documentDateGregorian + 'UTC')),
            documentDateHijri: new FormControl(item.documentDateHijri),
          })
        );
      }
    }
    this.journalEntryForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      journalEntryPeriodic: new FormControl(journalEntryPeriodic),
      journalEntryStatus: new FormControl(journalEntryStatus),
      journalEntryDescription: new FormControl(journalEntryDescription),
      gregorianDate: new FormControl(gregorianDate),
      hijriDate: new FormControl(hijriDate),
      journalEntryFileNumber: new FormControl(journalEntryFileNumber),
      journalEntrySourceName: new FormControl(journalEntrySourceName),
      journalEntryReverseNumber: new FormControl(journalEntryReverseNumber),
      journalEntryReverse: new FormControl(journalEntryReverse),
      journalEntryReverseDateGregorian: new FormControl(journalEntryReverseDateGregorian),
      journalEntryReverseDateHijri: new FormControl(journalEntryReverseDateHijri),
      journalEntryTotalDebit: new FormControl(journalEntryTotalDebit),
      journalEntryTotalCredit: new FormControl(journalEntryTotalCredit),
      journalEntryDifference: new FormControl(journalEntryDifference),
      isActive: new FormControl(isActive),
      journalEntryGuideAccountsList: journalEntryGuideAccountsListArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
