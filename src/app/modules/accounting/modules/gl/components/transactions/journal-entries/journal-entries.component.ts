import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { pagingStart, pagingEnd } from 'src/app/common/constants/general.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { baseUrl, journalEntriesApi, postJournalEntriesApi, unPostjournalEntriesApi } from 'src/app/common/constants/api.constants';
import { IJournalEntry } from '../../../interfaces/IJournalEntry';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-journal-entries',
  templateUrl: './journal-entries.component.html',
  styleUrls: ['./journal-entries.component.scss']
})
export class JournalEntriesComponent implements OnInit, OnDestroy {
  journalEntries: IJournalEntry[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;
  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getJournalEntriesFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(journalEntriesApi, pageNumber).subscribe((res: IDataRes) => {
      this.journalEntries = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getJournalEntriesFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(journalEntriesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.journalEntries = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      journalEntryDescription: new FormControl('')
    });
  }

  deleteModal(journalEntry: IJournalEntry) {
    const initialState = {
      code: journalEntry.code,
      nameAr: journalEntry.journalEntryDescription
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(journalEntry._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(journalEntriesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.journalEntries = this.generalService.removeItem(this.journalEntries, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postJournalEntriesApi}/${id}`, {}).subscribe(res => {
        this.journalEntries = this.generalService.changeStatus(this.journalEntries, id, 'posted', 'journalEntryStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostjournalEntriesApi}/${id}`, {}).subscribe(res => {
        this.journalEntries = this.generalService.changeStatus(this.journalEntries, id, 'unposted', 'journalEntryStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  getJournalEntriesFirstPage() {
    this.subscriptions.push(
      this.data.get(journalEntriesApi, 1).subscribe((res: IDataRes) => {
        this.journalEntries = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
