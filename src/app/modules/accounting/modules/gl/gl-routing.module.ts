import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// tslint:disable-next-line:max-line-length
import { IncomeStatementClassificationsComponent } from './components/files/income-statement-classifications/income-statement-classifications.component';
// tslint:disable-next-line:max-line-length
import { AddIncomeStatementClassificationComponent } from './components/files/income-statement-classifications/add-income-statement-classification/add-income-statement-classification.component';
// tslint:disable-next-line:max-line-length
// tslint:disable-next-line:max-line-length
import { BalanceSheetClassificationsComponent } from './components/files/balance-sheet-classifications/balance-sheet-classifications.component';
// tslint:disable-next-line:max-line-length
import { AddBalanceSheetClassificationComponent } from './components/files/balance-sheet-classifications/add-balance-sheet-classification/add-balance-sheet-classification.component';
// tslint:disable-next-line:max-line-length
import { CostCentersComponent } from './components/files/cost-centers/cost-centers.component';
import { ChartOfAccountsComponent } from './components/files/chart-of-accounts/chart-of-accounts.component';
import { AddCostCenterComponent } from './components/files/cost-centers/add-cost-center/add-cost-center.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AddChartOfAccountsComponent } from './components/files/chart-of-accounts/add-chart-of-accounts/add-chart-of-accounts.component';
import { AccountsOpeningBalancesComponent } from './components/transactions/accounts-opening-balances/accounts-opening-balances.component';
// tslint:disable-next-line:max-line-length
import { AddOpeningBalanceComponent } from './components/transactions/accounts-opening-balances/add-opening-balance/add-opening-balance.component';
import { JournalEntriesComponent } from './components/transactions/journal-entries/journal-entries.component';
import { AddJournalEntryComponent } from './components/transactions/journal-entries/add-journal-entry/add-journal-entry.component';
import { AccountsReportComponent } from './components/reports/accounts-report/accounts-report.component';
import { BudgetReportComponent } from './components/budget-report/budget-report.component';
import { RevenueListComponent } from './components/revenue-list/revenue-list.component';
import { BudgetComparisonComponent } from './components/budget-comparison/budget-comparison.component';
import { RevenueListComparisonComponent } from './components/revenue-list-comparison/revenue-list-comparison.component';
import { GroupedAccountReportComponent } from './components/grouped-account-report/grouped-account-report.component';
import { AccountStatementCostCentersComponent } from './components/account-statement-cost-centers/account-statement-cost-centers.component';
import { PrintReportLayoutComponent } from './components/print-report-layout/print-report-layout.component';
import { TrailBalanceComponent } from './components/trail-balance/trail-balance.component';
import { TotalTrailBalanceComponent } from './components/total-trail-balance/total-trail-balance.component';
import { DetailedCostCenterComponent } from './components/detailed-cost-center/detailed-cost-center.component';
import { GroupedCostCenterComponent } from './components/grouped-cost-center/grouped-cost-center.component';


const routes: Routes = [
  { path: '', redirectTo: 'incomeStatementClassifications', pathMatch: 'full' },
  { path: 'incomeStatementClassifications', component: IncomeStatementClassificationsComponent },
  { path: 'incomeStatementClassifications/add', component: AddIncomeStatementClassificationComponent },
  { path: 'incomeStatementClassifications/edit/:id', component: AddIncomeStatementClassificationComponent },
  { path: 'incomeStatementClassifications/details/:id', component: AddIncomeStatementClassificationComponent },
  { path: 'balanceSheetClassifications', component: BalanceSheetClassificationsComponent },
  { path: 'balanceSheetClassifications/add', component: AddBalanceSheetClassificationComponent },
  { path: 'balanceSheetClassifications/edit/:id', component: AddBalanceSheetClassificationComponent },
  { path: 'balanceSheetClassifications/details/:id', component: AddBalanceSheetClassificationComponent },
  { path: 'costCenters', component: CostCentersComponent },
  { path: 'costCenters/add', component: AddCostCenterComponent },
  { path: 'chartOfAccounts', component: ChartOfAccountsComponent },
  { path: 'chartOfAccounts/add', component: AddChartOfAccountsComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'accountsOpeningBalances', component: AccountsOpeningBalancesComponent },
  { path: 'accountsOpeningBalances/add', component: AddOpeningBalanceComponent },
  { path: 'accountsOpeningBalances/edit/:id', component: AddOpeningBalanceComponent },
  { path: 'accountsOpeningBalances/details/:id', component: AddOpeningBalanceComponent },
  { path: 'journalEntries', component: JournalEntriesComponent },
  { path: 'journalEntries/add', component: AddJournalEntryComponent },
  { path: 'journalEntries/edit/:id', component: AddJournalEntryComponent },
  { path: 'journalEntries/details/:id', component: AddJournalEntryComponent },
 

  // Reports
  { path: 'accountsReport', component: AccountsReportComponent },
  { path :'budgetReport',component:BudgetReportComponent},
  { path :'revenueList',component:RevenueListComponent},
  { path :'budgetComparison',component:BudgetComparisonComponent},
  { path :'revenueListComparison',component:RevenueListComparisonComponent},
  { path :'groupedAccountReports',component:GroupedAccountReportComponent},
  { path :'accountStatementCostCenters',component:AccountStatementCostCentersComponent},
  { path :'printLayout',component:PrintReportLayoutComponent},
  { path :'trailBalance', component:TrailBalanceComponent},
  { path:'totalTrailBalance' , component:TotalTrailBalanceComponent},
  { path:'detailedCostCenter' , component:DetailedCostCenterComponent},
  { path:'groupedCostCenter' , component:GroupedCostCenterComponent},

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GlRoutingModule { }
