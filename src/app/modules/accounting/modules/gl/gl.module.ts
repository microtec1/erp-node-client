import { NgModule } from "@angular/core";
import { GlRoutingModule } from "./gl-routing.module";
// tslint:disable-next-line:max-line-length
import { IncomeStatementClassificationsComponent } from "./components/files/income-statement-classifications/income-statement-classifications.component";
// tslint:disable-next-line:max-line-length
import { AddIncomeStatementClassificationComponent } from "./components/files/income-statement-classifications/add-income-statement-classification/add-income-statement-classification.component";
// tslint:disable-next-line:max-line-length
import { BalanceSheetClassificationsComponent } from "./components/files/balance-sheet-classifications/balance-sheet-classifications.component";
// tslint:disable-next-line:max-line-length
import { AddBalanceSheetClassificationComponent } from "./components/files/balance-sheet-classifications/add-balance-sheet-classification/add-balance-sheet-classification.component";
// tslint:disable-next-line:max-line-length
import { CostCentersComponent } from "./components/files/cost-centers/cost-centers.component";
import { CostCentersTreeComponent } from "./components/files/cost-centers/cost-centers-tree/cost-centers-tree.component";
import { ChartOfAccountsComponent } from "./components/files/chart-of-accounts/chart-of-accounts.component";
import { ChartOfAccountsTreeComponent } from "./components/files/chart-of-accounts/chart-of-accounts-tree/chart-of-accounts-tree.component";
import { AddCostCenterComponent } from "./components/files/cost-centers/add-cost-center/add-cost-center.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { AddChartOfAccountsComponent } from "./components/files/chart-of-accounts/add-chart-of-accounts/add-chart-of-accounts.component";
import { SharedModule } from "src/app/modules/shared/shared.module";
import { AccountsOpeningBalancesComponent } from "./components/transactions/accounts-opening-balances/accounts-opening-balances.component";
// tslint:disable-next-line:max-line-length
import { AddOpeningBalanceComponent } from "./components/transactions/accounts-opening-balances/add-opening-balance/add-opening-balance.component";
import { JournalEntriesComponent } from "./components/transactions/journal-entries/journal-entries.component";
import { AddJournalEntryComponent } from "./components/transactions/journal-entries/add-journal-entry/add-journal-entry.component";
// tslint:disable-next-line:max-line-length
import { CostCentersModalComponent } from "src/app/modules/accounting/modules/gl/components/files/cost-centers-modal/cost-centers-modal.component";
import { AccountsReportComponent } from "./components/reports/accounts-report/accounts-report.component";

import { AgGridModule } from "ag-grid-angular";
import { BudgetReportComponent } from "./components/budget-report/budget-report.component";
import { RevenueListComponent } from "./components/revenue-list/revenue-list.component";
import { BudgetComparisonComponent } from "./components/budget-comparison/budget-comparison.component";
import { RevenueListComparisonComponent } from "./components/revenue-list-comparison/revenue-list-comparison.component";
import { GroupedAccountReportComponent } from "./components/grouped-account-report/grouped-account-report.component";
import { AccountStatementCostCentersComponent } from "./components/account-statement-cost-centers/account-statement-cost-centers.component";
import { PrintReportLayoutComponent } from "./components/print-report-layout/print-report-layout.component";
import { TrailBalanceComponent } from "./components/trail-balance/trail-balance.component";
import { TotalTrailBalanceComponent } from "./components/total-trail-balance/total-trail-balance.component";
import { DetailedCostCenterComponent } from "./components/detailed-cost-center/detailed-cost-center.component";
import { GroupedCostCenterComponent } from "./components/grouped-cost-center/grouped-cost-center.component";

@NgModule({
  declarations: [
    IncomeStatementClassificationsComponent,
    AddIncomeStatementClassificationComponent,
    BalanceSheetClassificationsComponent,
    AddBalanceSheetClassificationComponent,
    CostCentersComponent,
    CostCentersTreeComponent,
    ChartOfAccountsComponent,
    ChartOfAccountsTreeComponent,
    AddCostCenterComponent,
    AddChartOfAccountsComponent,
    SettingsComponent,
    AccountsOpeningBalancesComponent,
    AddOpeningBalanceComponent,
    JournalEntriesComponent,
    AddJournalEntryComponent,
    CostCentersModalComponent,
    AccountsReportComponent,
    BudgetReportComponent,
    RevenueListComponent,
    BudgetComparisonComponent,
    RevenueListComparisonComponent,
    GroupedAccountReportComponent,
    AccountStatementCostCentersComponent,
    PrintReportLayoutComponent,
    TrailBalanceComponent,
    TotalTrailBalanceComponent,
    DetailedCostCenterComponent,
    GroupedCostCenterComponent,
  ],
  imports: [
    SharedModule,
    GlRoutingModule,
    // AgGridModule.withComponents([]),
  ],
  entryComponents: [CostCentersModalComponent],
})
export class GlModule {}
