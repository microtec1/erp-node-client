export interface IBalanceSheetClassification {
  _id: string;
  image: string;
  code: string;
  balanceSheetClassificationNameAr: string;
  balanceSheetClassificationNameEn: string;
  balanceSheetClassificationSide: string;
  balanceSheetClassificationType: string;
  balanceSheetClassificationResulting: string;
  balanceSheetClassificationCalcNetLossesProfits: boolean;
  isActive: boolean;
}
