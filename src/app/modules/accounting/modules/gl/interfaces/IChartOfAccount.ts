export interface IChartOfAccount {
  _id: string;
  image: string;
  code: string;
  addMode: boolean;
  chartOfAccountsNameAr: string;
  chartOfAccountsNameEn: string;
  chartOfAccountsCurrency: {
    currencyId: string;
    currencyNameAr: string;
    exchangeRate: string;
    isActive: string;
  };
  chartOfAccountsType: string;
  chartOfAccountsReportType: string;
  chartOfAccountsCashFlow: string;
  chartOfAccountsCostCenterType: string;
  chartOfAccountsCategoryId: string;
  chartOfAccountsCategoryName: string;
  chartOfAccountsSide: string;
  chartOfAccountsCostCenter: [
    {
      costCenterId: string;
      code: string;
      costCenterNameAr: string;
      costCenterRate: number;
      isActive: boolean;
    }
  ];
  parentId: string;
  companyId: string;
  isActive: boolean;
  level?: number;
  children: IChartOfAccount[];
}
