export interface ICostCenter {
  _id: string;
  code: string;
  costCenterNameAr: string;
  costCenterNameEn: string;
  costCenterType: string;
  parentId: string;
  companyId: string;
  isActive: boolean;
  level?: number;
  children: ICostCenter[];
}
