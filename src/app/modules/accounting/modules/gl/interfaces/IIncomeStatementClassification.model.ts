export interface IIncomeStatementClassification {
  _id: string;
  image: string;
  code: string;
  incomeStatementClassificationNameAr: string;
  incomeStatementClassificationNameEn: string;
  incomeStatementClassificationSide: string;
  incomeStatementClassificationType: string;
  incomeStatementClassificationResulting: string;
  isActive: boolean;
}
