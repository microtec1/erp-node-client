export interface IJournalEntry {
  _id: string;
  code: string;
  journalEntryPeriodic: boolean;
  journalEntryStatus: string;
  journalEntryDescription: string;
  gregorianDate: string;
  hijriDate: string;
  journalEntryFileNumber: string;
  journalEntrySourceName: string;
  journalEntryReverseNumber: string;
  journalEntryReverse: boolean;
  journalEntryReverseDateGregorian: string;
  journalEntryReverseDateHijri: string;
  journalEntryGuideAccountsList: [
    {
      accountId: string;
      currencyId: string;
      exchangeRate: number;
      debit: number;
      credit: number;
      description: string;
      costCenterList: [
        {
          costCenterId: string;
          price: number;
        }
      ];
      tax: string;
      costCenter: string;
      costCenter2: string;
      documentNumber: string;
      referenceNumber: string;
      documentDateGregorian: string;
      documentDateHijri: string;
    }
  ];
  journalEntryTotalDebit: number;
  journalEntryTotalCredit: number;
  journalEntryDifference: number;
  isActive: boolean;
}
