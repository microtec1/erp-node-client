export interface IOpeningBalance {
  _id: string;
  accountsOpeningBalancesStatus: string;
  accountsOpeningBalancesFiscalYearId: string;
  fiscalYearName: string;
  accountsOpeningBalancesTotalDebit: number;
  accountsOpeningBalancesTotalCredit: number;
  accountsOpeningBalancesDifference: number;
  journalEntryId: string;
  accountsOpeningBalancesDetails: [
    {
      accountId: string;
      currencyId: string;
      exchangeRate: number;
      debit: number;
      credit: number;
      costCenterId: string;
    }
  ];
  isActive: boolean;
}
