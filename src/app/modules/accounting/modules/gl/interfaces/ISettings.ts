export interface IGlAccountSettings {
  companyId: string;
  _id: string;
  company: {
    linkCashWithAccounts: boolean;
    linkInventoryWithAccounts: boolean;
    paymentsAndTransactionsOfASpecificBranchWithOtherBranches: boolean;
    linkAccountsWithIncomeStatementClassificationAndBalanceSheetClassification: boolean;
    generateCodesOfAccountsAndCostCentersAutomatically: boolean;
    levelLength: number;
    netOfProfitAndLoseAccountId: string;
  };
  branch: {
    branchId: string;
    cashBoxId: string;
    bankId: string;
    linkPayableReceiptsWithActualFiles: boolean;
    linkCashReceiptsWithActualFiles: boolean;
    copyDescriptionOfPostPartnersFromTransactionDescriptions: boolean;
    addAdditionalCostCentersWithItsEntries: boolean;
    printPayableAndReceivableReceiptsDirectly: boolean;
    notAllowedToEditInFinancialTransactions: boolean;
    showDraftInformationAtJournalEntriesScreen: boolean;
    notAllowToDuplicateInternalReferenceInJournalEntry: boolean;
    approximateCalculationAccountId: string;
    currenciesTransferAccountId: string;
    addedTaxesValueAccountId: string;
    financialTransferAccountId: string;
    bankCommissionsTaxAccountId: string;
    downPaymentTaxAccountId: string;
    underPaymentChequesAccountId: string;
    underCollectingChequesAccountsId: string;
  };
}

