import { Injectable } from '@angular/core';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { map } from "rxjs/operators";
import { from, BehaviorSubject } from "rxjs";
import { costCentersApi }  from '../../../../../common/constants/api.constants';
import { ColDefUtil } from 'ag-grid-community';
@Injectable({
  providedIn: 'root'
})
export class AccountReportsService {

  constructor(private data: DataService,private general : GeneralService) { }

  private tableLayout = new BehaviorSubject({});
  currentTableLayout = this.tableLayout.asObservable();

  
  removeObjEmptyAttributes(obj) {
    for (var propName in obj) { 
      if (obj[propName] === null || obj[propName] === undefined || obj[propName] === "" || obj[propName].length === 0) {
        delete obj[propName];
      }
    }
  }
  
  shapingFilterObject(e){
    let code :any;
    console.log("this is e : " , e)
    this.removeObjEmptyAttributes(e)
    let filter;
    if(e.accountsGroup){
      return filter = {
        IN:{
          code:e.accountsGroup
        }
      }
    }

    else{

      if(!e.to && e.from){ 
        code = e.from;
        return filter = {code:code}
      } 

      if(!e.from && e.to ){ 
        code = e.to;
        return filter = {code:code}
      }

      if(!e.from&&!e.to){
        return filter = {}
      }

      if(!!e.from && !!e.to){
        code={min:e.from,max:e.to,maxEquality:true,minEquality:true}
        return filter = {
          RANGE:{
            code:code
          }
        }
      }

    }

    
  }


  shapingBranchFilterObject(e){
    let code :any;
    console.log("this is e : " , e)
    this.removeObjEmptyAttributes(e)
    let filter;
    if(e.branchGroup){
      return filter = {
        IN:{
          branchCode:e.branchGroup
        }
      }
    }

    else{
      
      if(!e.toBranch && e.fromBranch){ 
        code = e.fromBranch;
        return filter = {branchCode:code}
      }
      if(!e.fromBranch && e.toBranch ){ 
        code = e.toBranch;
        return filter = {branchCode:code}
      }
      if(!e.fromBranch&&!e.toBranch){
        return filter = {}
      }
      if(!!e.fromBranch && !!e.toBranch){
        code={min:e.fromBranch,max:e.toBranch,maxEquality:true,minEquality:true}
        return filter = {
          RANGE:{
            branchCode:code
          }
        }
      }
    }
  }

  shapingCostCentersFilterObject(e){
    let code :any;
    console.log("this is e : " , e)
    this.removeObjEmptyAttributes(e)
    let filter;
    if(e.costCentersGroup){
      return filter = {
        IN:{
          code:e.costCentersGroup
        }
      }
    }

    else{
      
      if(!e.toCostCenter && e.fromCostCenter){ 
        code = e.fromCostCenter;
        return filter = {code:code[0]}
      }
      if(!e.fromCostCenter && e.toCostCenter ){ 
        code = e.toCostCenter;
        return filter = {code:code[0]}
      }
      if(!e.fromCostCenter&&!e.toCostCenter){
        return filter = {}
      }
      if(!!e.fromCostCenter && !!e.toCostCenter){
        code={min:e.fromCostCenter,max:e.toCostCenter,maxEquality:true,minEquality:true}
        return filter = {
          RANGE:{
            code:code
          }
        }
      }
    }
  }


  
  //shaping  received data object to match ag grid table structure 
  //used in accountsReport , revenueList and budgetReport 
  shapingAccountsReportTable (receivedData){
    let shapedData :any = []
    console.log("this is recieved data" , receivedData)
    let source = from(receivedData);
      let mappedData = source.pipe(
        map((current:any) => {
          let eachTablePeriodEntries=[];
          current.periodEntries.forEach((eachEntry,index) => {
            let balance:number = this.getTotalBasedOnSide(index,
              current,
              eachTablePeriodEntries,
              eachEntry)

            let row={ 
              branch: eachEntry.branch.branchCode +" - "+ eachEntry.branch.branchNameAr , 
              registrationNumber: eachEntry.code,
              registrationDate: eachEntry.gregorianDate,
              description:eachEntry.journalEntryDescription,
              source:this.general.hasTranslation("EntrySource",eachEntry.journalEntrySourceName) ,
              creditor:eachEntry.credit,
              balance:balance,
              debtor:eachEntry.debit 
              };
          
            eachTablePeriodEntries.push(row)
          });

          let pinnedRowTop ={
            branch: "", 
            registrationNumber: "",
            registrationDate: "",
            description:this.general.getTranslation('TRANSACTIONS',"openingBalance"),
            source: "",
            creditor: parseFloat(current.open.credit.toFixed(4)),
            balance:parseFloat(current.open.balance.toFixed(4)),
            debtor: parseFloat(current.open.debit.toFixed(4))
          }

          eachTablePeriodEntries.splice(0,0,pinnedRowTop)

          let columnDefs = [
            {
            headerName:`
            ${this.general.getTranslation("AccountClassification","accountNumber")} :
            &nbsp;  ‎${current.code} &nbsp; &nbsp;

            &nbsp; &nbsp; ${this.general.getTranslation("AccountClassification","chartOfAccountName")} :
            &nbsp;${current.chartOfAccountsNameAr} &nbsp; &nbsp;

            &nbsp; &nbsp; ${this.general.getTranslation("AccountClassification","chartOfAccountsReportType")} :
            &nbsp;  ${this.general.getTranslation("AccountClassification",current.chartOfAccountsReportType)}&nbsp; &nbsp;

            &nbsp; &nbsp; ${this.general.getTranslation("AccountClassification","accountSide")} :
            &nbsp;  ${this.general.getTranslation("financialAnalysis",current.chartOfAccountsSide)}
            `
            ,
            headerClass: "medals-group",
            children:[
              {
                headerName:this.general.getTranslation("LOGIN","branch"),
                field:"branch",
                colId:"branch"
              },
              {
                headerName:this.general.getTranslation("GL","registrationNumber"),
                field:"registrationNumber",
                colId:"registrationNumber"
              },
              {
                headerName:this.general.getTranslation("GL","registrationDate"),
                field:"registrationDate",
                colId:"registrationDate"
              },
              {
                headerName:this.general.getTranslation("GL","source"),
                field:"source",
                colId:"source"
              },
              {
                headerName:this.general.getTranslation("GL","description"),
                field:"description",
                colId:"description"
              },
              {
                headerName:this.general.getTranslation("GL","debtor"),
                field:"debtor",
                colId:"debtor"
              },
              {
                headerName:this.general.getTranslation("GL","creditor"),
                field:"creditor",
                colId:"creditor"
              },
              {
                headerName:this.general.getTranslation("PURCHASES","balance"),
                field:"balance",
                colId:"balance"
              },
        
            ]
          }
        ];

          let pinnedRowBottom = [{
            branch:this.general.getTranslation("PURCHASES","total"),
            debtor: parseFloat(current.total.debit.toFixed(4)) ,
            creditor :parseFloat(current.total.credit.toFixed(4)) ,
            balance : parseFloat(current.total.balance.toFixed(4))
          }]
          
          shapedData.push({coulmns : columnDefs ,rows:eachTablePeriodEntries,pinnedRowBottom:pinnedRowBottom,pagination:true,pageSize:"5"});
          
          return shapedData;
        })
      );

      mappedData.subscribe(d => {
        shapedData = d ;
      });
      // shapedData[0].costCenter=this.shapingNestedCostCenterData(receivedData)
      return shapedData ;
  }
  //get balance(total) based on side whether it's debit of credit 
  getTotalBasedOnSide (index,current,eachTablePeriodEntries,eachEntry){
    let acc :number =0 ;
    if(eachTablePeriodEntries.length == 0){
              
      if(eachEntry.debit !=0 && current.chartOfAccountsSide == "debit" ){
        acc = parseFloat((current.open.balance + eachEntry.debit).toFixed(4));
        
      }
      if(eachEntry.credit != 0 && current.chartOfAccountsSide == "credit"){
        acc = parseFloat((current.open.balance + eachEntry.credit).toFixed(4));
      }
      if(eachEntry.debit !=0 && current.chartOfAccountsSide != "debit" ){
        acc = parseFloat((current.open.balance - eachEntry.debit).toFixed(4));
      }
      if(eachEntry.credit != 0 && current.chartOfAccountsSide != "credit"){
        acc = parseFloat((current.open.balance - eachEntry.credit).toFixed(4));
      }
      
    }
    if(eachTablePeriodEntries.length>0){
      
      if(eachEntry.debit !=0 && current.chartOfAccountsSide == "debit" ){
        acc = parseFloat((eachTablePeriodEntries[index-1].balance + eachEntry.debit).toFixed(4));
      }
      if(eachEntry.credit != 0 && current.chartOfAccountsSide == "credit"){
        acc = parseFloat((eachTablePeriodEntries[index-1].balance + eachEntry.credit).toFixed(4));
      }
      if(eachEntry.debit !=0 && current.chartOfAccountsSide != "debit" ){
        acc = parseFloat((eachTablePeriodEntries[index-1].balance - eachEntry.debit).toFixed(4));
      }
      if(eachEntry.credit != 0 && current.chartOfAccountsSide != "credit"){
        acc = parseFloat((eachTablePeriodEntries[index-1].balance - eachEntry.credit).toFixed(4));
      }
    }
    
    return acc;
  }

  // shaping  received data object for budgetComparison table
  shapingDataForBudgetComparison(){//receivedData){    
  }

  shapingAccountStatmentCostCenters(receivedData){}

  // shaping data for grouped account reports table
  shapingGroupedAccount(receivedData){
    let shapedData = []
    let eachTablePeriodEntries = [];
    receivedData.forEach(current=>{
      let row={ 
        account:current.code +"  -  "+current.chartOfAccountsNameAr  ,
        totalCredit:parseFloat(current.total.credit.toFixed(4)),
        totalDebit:parseFloat(current.total.debit.toFixed(4)),
        openCredit:parseFloat(current.openCredit.toFixed(4)),
        openDebit:parseFloat(current.openDebit.toFixed(4)),
        periodCredit:parseFloat(current.periodCredit.toFixed(4)),
        periodDebit:parseFloat(current.periodDebit.toFixed(4)),
        totalBalance:parseFloat(current.total.balance.toFixed(4))
      };
      eachTablePeriodEntries.push(row)
    })
    let columnDefs = [
          {
            headerName: this.general.getTranslation("AccountClassification","account"),
            field:"account",
          },
          {
            field:"openCredit",
            headerName: this.general.getTranslation("AccountClassification","openCredit"),
          },
          {
            field:"openDebit",
            headerName: this.general.getTranslation("AccountClassification","openDebit"),
          },
          {
            field:"periodCredit",
            headerName: this.general.getTranslation("AccountClassification","periodCredit"),
          },
          {
            field:"periodDebit",
            headerName: this.general.getTranslation("AccountClassification","periodDebit"),
          },
          {
            field:"totalCredit",
            headerName: this.general.getTranslation("AccountClassification","totalCredit"),
          },
          {
            field:"totalDebit",
            headerName: this.general.getTranslation("AccountClassification","totalDebit"),
          },
          {
            field:"totalBalance",
            headerName: this.general.getTranslation("AccountClassification","totalBalance"),
          },
      
    ];

    
    shapedData.push({coulmns : columnDefs, rows:eachTablePeriodEntries ,pagination:true,
    pageSize:eachTablePeriodEntries.length}); 
    console.log("x:",shapedData)     
    return shapedData ;
  }
  shapingGroupedAccumulativeAccount(receivedData,accumulative:boolean){
    let shapedData = []
    let tableRows = [];
    receivedData.forEach(current=>{
      let row={ 
        account: current.code +"  -  "+current.chartOfAccountsNameAr+ " ( " + this.general.getTranslation('AccountClassification',current.chartOfAccountsSide)  + " ) ",
        totalCredit:parseFloat(current.total.credit.toFixed(4)),
        totalDebit:parseFloat(current.total.debit.toFixed(4)),
        openCredit:parseFloat(current.openCredit.toFixed(4)),
        openDebit:parseFloat(current.openDebit.toFixed(4)),
        periodCredit:parseFloat(current.periodCredit.toFixed(4)),
        periodDebit:parseFloat(current.periodDebit.toFixed(4)),
        totalBalance:parseFloat(current.total.balance.toFixed(4))
      };
      if(accumulative){
        delete row.periodCredit
        delete row.periodDebit
      }else{
        delete row.openCredit
        delete row.openDebit
      }
      tableRows.push(row)
    })
    let columnDefs = [
          {
            headerName: this.general.getTranslation("AccountClassification","account"),
            field:"account",
          },
          {
            field:"openCredit",
            headerName: this.general.getTranslation("AccountClassification","openCredit"),
          },
          {
            field:"openDebit",
            headerName: this.general.getTranslation("AccountClassification","openDebit"),
          },
          {
            field:"periodCredit",
            headerName: this.general.getTranslation("AccountClassification","periodCredit"),
          },
          {
            field:"periodDebit",
            headerName: this.general.getTranslation("AccountClassification","periodDebit"),
          },
          {
            field:"totalCredit",
            headerName: this.general.getTranslation("AccountClassification","totalCredit"),
          },
          {
            field:"totalDebit",
            headerName: this.general.getTranslation("AccountClassification","totalDebit"),
          },
          {
            field:"totalBalance",
            headerName: this.general.getTranslation("AccountClassification","totalBalance"),
          },
      
    ];

    if(accumulative){
      columnDefs = columnDefs.filter(obj=>{
        return (obj.field !== 'periodCredit') && (obj.field !== 'periodDebit' ) 
      })
    }else{
      columnDefs = columnDefs.filter(obj=>{
        return (obj.field !== 'openCredit' ) && (obj.field !== 'openDebit')
        && (obj.field !== 'totalCredit') && (obj.field !== 'totalDebit')
      })
      
    }
    
    shapedData.push({coulmns : columnDefs, rows:tableRows ,pagination:true,
    pageSize:tableRows.length}); 
    console.log("x:",shapedData)     
    return shapedData ;
  }

  getDate(date){
    
    date.forEach((eachDate,i)=>{
      date[i]= this.general.formatDate(eachDate);
    })
    return {minDate:date[0],maxDate:date[1]}
  }

  shapingTrailBalance(receivedData,accumulative:boolean){
    let shapedData = []
    let tableRows = [];
    receivedData.forEach(current=>{
      let row={ 
        account: current.code +"  -  "+current.chartOfAccountsNameAr+ " ( " + this.general.getTranslation('AccountClassification',current.chartOfAccountsSide)  + " ) ",
        totalCredit:parseFloat(current.total.credit.toFixed(4)),
        totalDebit:parseFloat(current.total.debit.toFixed(4)),
        openCredit:parseFloat(current.openCredit.toFixed(4)),
        openDebit:parseFloat(current.openDebit.toFixed(4)),
        periodCredit:parseFloat(current.periodCredit.toFixed(4)),
        periodDebit:parseFloat(current.periodDebit.toFixed(4)),
        totalBalance:parseFloat(current.total.balance.toFixed(4)),
        debit: 0,
        credit: 0
      };
      if(accumulative){
        delete row.periodCredit
        delete row.periodDebit
      }else{
        delete row.openCredit
        delete row.openDebit
      }
      let total = parseFloat(Math.abs(current.total.balance).toFixed(4))
      if(current.total.debit > current.total.credit ){
        row.debit = total;
      }else{
        row.credit = total;
      }
      tableRows.push(row)
    })
    let columnDefs = [
          {
            headerName: this.general.getTranslation("AccountClassification","account"),
            field:"account",
          },
          {
            field:"openCredit",
            headerName: this.general.getTranslation("AccountClassification","openCredit"),
          },
          {
            field:"openDebit",
            headerName: this.general.getTranslation("AccountClassification","openDebit"),
          },
          {
            field:"periodCredit",
            headerName: this.general.getTranslation("AccountClassification","periodCredit"),
          },
          {
            field:"periodDebit",
            headerName: this.general.getTranslation("AccountClassification","periodDebit"),
          },
          {
            field:"totalCredit",
            headerName: this.general.getTranslation("AccountClassification","totalCredit"),
          },
          {
            field:"totalDebit",
            headerName: this.general.getTranslation("AccountClassification","totalDebit"),
          },
          {
            field:"totalBalance",
            headerName: this.general.getTranslation("AccountClassification","totalBalance"),
          },
          {
            field:"debit",
            headerName: this.general.getTranslation("AccountClassification","a"),
          },
          {
            field:"credit",
            headerName: this.general.getTranslation("AccountClassification","b"),
          },
      
      
    ];

    if(accumulative){
      columnDefs = columnDefs.filter(obj=>{
        return (obj.field !== 'periodCredit') && (obj.field !== 'periodDebit' ) 
      })
    }else{
      columnDefs = columnDefs.filter(obj=>{
        return (obj.field !== 'openCredit' ) && (obj.field !== 'openDebit')
        && (obj.field !== 'totalCredit') && (obj.field !== 'totalDebit')
      })
      
    }
    
    shapedData.push({coulmns : columnDefs, rows:tableRows ,pagination:true,
    pageSize:tableRows.length}); 
    return shapedData ;
  }

  setTableLayoutToPrint(data:{}){
    this.tableLayout.next(data)
  }

  // shapingNestedCostCenterData(receivedData){
  //   let shapedData = []
  //   console.log("this is recieved data" , receivedData)
  //   let source = from(receivedData);
  //     let mappedData = source.pipe(
  //       map((current:any) => {
  //         let eachTablePeriodEntries=[];
  //         let costCenterNameAr = current.CostCenters[0].costCenter.costCenterNameAr ;
  //         let costCenterRate = current.CostCenters[0].costCenterRate
  //         let code = current.CostCenters[0].costCenter.code
  //         let row={ 
  //           code:code,
  //           costCenterNameAr: costCenterNameAr  , 
  //           // creditor:parseFloat((eachEntry.credit * costCenterRate).toFixed(4)),
  //           // balance:parseFloat((balance * costCenterRate).toFixed(4)),
  //           // debtor:parseFloat((eachEntry.debit * costCenterRate).toFixed(4)),
  //           rate : costCenterRate
  //           };
        
  //         eachTablePeriodEntries.push(row)
  //         let columnDefs = [
  //           {
  //           headerName:costCenterNameAr
  //           ,
  //           headerClass: "medals-group",
  //           children:[
  //             {
  //               headerName:this.general.getTranslation("AccountClassification","code"),
  //               field:"code",
  //               colId:"code"
  //             },
  //             {
  //               headerName:this.general.getTranslation("AccountClassification","costCenter"),
  //               field:"costCenterNameAr",
  //               colId:"costCenterNameAr"
  //             },
  //             {
  //               headerName:this.general.getTranslation("AccountClassification","rate"),
  //               field:"rate",
  //               colId:"rate"
  //             },
  //             // {
  //             //   headerName:this.general.getTranslation("GL","debtor"),
  //             //   field:"debtor",
  //             //   colId:"debtor"
  //             // },
  //             // {
  //             //   headerName:this.general.getTranslation("GL","creditor"),
  //             //   field:"creditor",
  //             //   colId:"creditor"
  //             // },
  //             // {
  //             //   headerName:this.general.getTranslation("PURCHASES","balance"),
  //             //   field:"balance",
  //             //   colId:"balance"
  //             // }
  //           ]
  //         }
  //       ];
          
  //         shapedData.push({coulmns : columnDefs ,rows:eachTablePeriodEntries,pagination:true,pageSize:"5"});
          
  //         return shapedData;
  //       })
  //     );

  //     mappedData.subscribe(d => {
  //       shapedData = d ;
  //     });

  //     return shapedData ;
  // }


  shapingDetailedCostCenter(receivedData){
    let shapedData :any = []
    console.log("this is recieved data" , receivedData)
    let source = from(receivedData);
      let mappedData = source.pipe(
        map((current:any) => {
          let eachTablePeriodEntries=[];
          current.periodEntries.forEach((eachEntry,index) => {
            let row={ 
              branch: eachEntry.branch.branchCode , 
              registrationDate: eachEntry.gregorianDate,
              description:eachEntry.journalEntryDescription,
              creditor:eachEntry.credit,
              journalEntryStatus:this.general.hasTranslation("AccountClassification",eachEntry.journalEntryStatus),
              debtor:eachEntry.debit 
              };
          
            eachTablePeriodEntries.push(row)
          });

          let pinnedRowTop ={
            branch: "", 
            registrationDate: "",
            description:this.general.getTranslation('TRANSACTIONS',"openingBalance"),
            creditor: parseFloat(current.open.credit.toFixed(4)),
            debtor: parseFloat(current.open.debit.toFixed(4))
          }

          eachTablePeriodEntries.splice(0,0,pinnedRowTop)

          let columnDefs = [
            {
            headerName:`
            ${this.general.getTranslation("AccountClassification","costCenterCode")} :
            &nbsp;  ‎${current.code} &nbsp; &nbsp;
            &nbsp;${current.costCenterNameAr} &nbsp; &nbsp;
            `
            ,
            headerClass: "medals-group",
            children:[
              {
                headerName:this.general.getTranslation("AccountClassification","branchId"),
                field:"branch",
                colId:"branch"
              },
              {
                headerName:this.general.getTranslation("GL","registrationDate"),
                field:"registrationDate",
                colId:"registrationDate"
              },
              {
                headerName:this.general.getTranslation("AccountClassification","journalEntryStatus"),
                field:"journalEntryStatus",
                colId:"journalEntryStatus"
              },
              {
                headerName:this.general.getTranslation("GL","description"),
                field:"description",
                colId:"description"
              },
              {
                headerName:this.general.getTranslation("GL","debtor"),
                field:"debtor",
                colId:"debtor"
              },
              {
                headerName:this.general.getTranslation("GL","creditor"),
                field:"creditor",
                colId:"creditor"
              },
        
            ]
          }
        ];

          let pinnedRowBottom = [{
            branch:this.general.getTranslation("PURCHASES","total"),
            debtor: parseFloat(current.total.debit.toFixed(4)) ,
            creditor :parseFloat(current.total.credit.toFixed(4)) ,
          }]
          
          shapedData.push({coulmns : columnDefs ,rows:eachTablePeriodEntries,pinnedRowBottom:pinnedRowBottom,pagination:true,pageSize:"5"});
          
          return shapedData;
        })
      );

      mappedData.subscribe(d => {
        shapedData = d ;
      });
      // shapedData[0].costCenter=this.shapingNestedCostCenterData(receivedData)
      return shapedData ;
  }


  shapingGroupedAccumulativeCostCenter(receivedData,accumulative:boolean){
    let shapedData = []
    let tableRows = [];
    receivedData.forEach(current=>{
      let row={ 
        account: current.code +"  -  "+current.costCenterNameAr ,
        totalCredit:parseFloat(current.total.credit.toFixed(4)),
        totalDebit:parseFloat(current.total.debit.toFixed(4)),
        openCredit:parseFloat(current.openCredit.toFixed(4)),
        openDebit:parseFloat(current.openDebit.toFixed(4)),
        periodCredit:parseFloat(current.periodCredit.toFixed(4)),
        periodDebit:parseFloat(current.periodDebit.toFixed(4))
      };
      if(accumulative){
        delete row.periodCredit
        delete row.periodDebit
      }else{
        delete row.openCredit
        delete row.openDebit
      }
      tableRows.push(row)
    })
    let columnDefs = [
          {
            headerName: this.general.getTranslation("AccountClassification","costCenter"),
            field:"account",
          },
          {
            field:"openCredit",
            headerName: this.general.getTranslation("AccountClassification","openCredit"),
          },
          {
            field:"openDebit",
            headerName: this.general.getTranslation("AccountClassification","openDebit"),
          },
          {
            field:"periodCredit",
            headerName: this.general.getTranslation("AccountClassification","periodCredit"),
          },
          {
            field:"periodDebit",
            headerName: this.general.getTranslation("AccountClassification","periodDebit"),
          },
          {
            field:"totalCredit",
            headerName: this.general.getTranslation("AccountClassification","totalCredit"),
          },
          {
            field:"totalDebit",
            headerName: this.general.getTranslation("AccountClassification","totalDebit"),
          }
      
    ];

    if(accumulative){
      columnDefs = columnDefs.filter(obj=>{
        return (obj.field !== 'periodCredit') && (obj.field !== 'periodDebit' ) 
      })
    }else{
      columnDefs = columnDefs.filter(obj=>{
        return (obj.field !== 'openCredit' ) && (obj.field !== 'openDebit')
        && (obj.field !== 'totalCredit') && (obj.field !== 'totalDebit')
      })
      
    }
    
    shapedData.push({coulmns : columnDefs, rows:tableRows ,pagination:true,
    pageSize:tableRows.length}); 
    console.log("x:",shapedData)     
    return shapedData ;
  }
}
