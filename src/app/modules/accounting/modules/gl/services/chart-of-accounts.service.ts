import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IChartOfAccount } from '../interfaces/IChartOfAccount';

@Injectable({
  providedIn: 'root'
})
export class ChartOfAccountsService {
  chartOfAccountsChanged = new Subject<IChartOfAccount[]>();
  fillFormChange = new Subject<IChartOfAccount>();

  constructor() { }

}
