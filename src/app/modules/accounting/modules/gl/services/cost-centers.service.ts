import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ICostCenter } from '../interfaces/ICostCenter';

@Injectable({
  providedIn: 'root'
})
export class CostCentersService {
  costCentersChanged = new Subject<ICostCenter[]>();
  fillFormChange = new Subject<ICostCenter>();

  constructor() { }

}
