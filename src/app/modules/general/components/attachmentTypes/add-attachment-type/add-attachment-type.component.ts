import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from '../../../services/general.service';
import { IAttachmentType } from '../../../interfaces/IAttachmentType';
import { Router, ActivatedRoute, Params, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, attachmentTypesApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-attachment-type',
  templateUrl: './add-attachment-type.component.html',
  styleUrls: ['./add-attachment-type.component.scss']
})
export class AddAttachmentTypeComponent implements OnInit, OnDestroy {
  attachmentTypeForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  attachmentType: IAttachmentType;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(attachmentTypesApi, null, null, id).subscribe(
              (attachmentType: IAttachmentType) => {
                this.attachmentType = attachmentType;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.attachmentTypeForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              },
              err => {
                this.uiService.showErrorMessage(err);
              }
            )
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.attachmentTypeForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.attachmentType.image = '';
    this.attachmentTypeForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.attachmentTypeForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.attachmentType) {
      if (this.attachmentTypeForm.valid) {
        const newAttachmentType = {
          _id: this.attachmentType._id,
          ...this.generalService.checkEmptyFields(this.attachmentTypeForm.value)
        };
        this.subscriptions.push(
          this.data.put(attachmentTypesApi, newAttachmentType).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/attachmentTypes']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.attachmentTypeForm.valid) {
        const formValue = this.generalService.checkEmptyFields(
          this.attachmentTypeForm.value
        );
        this.subscriptions.push(
          this.data.post(attachmentTypesApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.attachmentTypeForm.reset();
              this.attachmentTypeForm.patchValue({
                isActive: true
              });
              this.submitted = false;
              this.dropzone.reset();
              this.filesAdded = false;
              this.uiService.isLoading.next(false);
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let attachmentTypeNameAr = '';
    let attachmentTypeNameEn = '';
    let isActive = true;

    if (this.attachmentType) {
      image = this.attachmentType.image;
      code = this.attachmentType.code;
      attachmentTypeNameAr = this.attachmentType.attachmentTypeNameAr;
      attachmentTypeNameEn = this.attachmentType.attachmentTypeNameEn;
      isActive = this.attachmentType.isActive;
    }
    this.attachmentTypeForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(
        code,
        Validators.required
      ),
      attachmentTypeNameAr: new FormControl(
        attachmentTypeNameAr,
        Validators.required
      ),
      attachmentTypeNameEn: new FormControl(attachmentTypeNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
