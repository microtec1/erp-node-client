import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ICity } from '../../../interfaces/ICity.model';
import { ICountry } from '../../../interfaces/ICountry.model';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from '../../../services/general.service';
import { searchLength } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { baseUrl, countriesApi, citiesApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';


@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.scss']
})
export class AddCityComponent implements OnInit, OnDestroy {
  cityForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  city: ICity;
  country: ICountry;
  filesAdded: boolean;
  subscriptions: Subscription[] = [];
  formReady: boolean;
  baseUrl = baseUrl;
  detailsMode: boolean;

  // Countries
  countries: ICountry[] = [];
  countriesInputFocused: boolean;
  hasMoreCountries: boolean;
  countriesCount: number;
  selectedCountriesPage = 1;
  countriesPagesNo: number;
  noCountries: boolean;

  @ViewChild('dropzone') dropzone: any;

  constructor(
    private uiService: UiService,
    private data: DataService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.data.get(countriesApi, 1).subscribe((res: IDataRes) => {
        this.countriesPagesNo = res.pages;
        this.countriesCount = res.count;
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        }
        this.countries.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(citiesApi, null, null, id).subscribe((city: ICity) => {
              this.city = city;
              this.subscriptions.push(
                this.data.get(countriesApi, city.countryId).subscribe((country: ICountry) => {
                  if (!this.generalService.checkItem(this.countries, country._id)) {
                    this.countries.push(country);
                  }
                  this.uiService.isLoading.next(false);
                })
              );
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.cityForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            },
              err => {
                this.uiService.showErrorMessage(err);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.cityForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.city.image = '';
    this.cityForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  searchCountries(event) {
    const searchValue = event;
    const searchQuery = {
      countryNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(countriesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCountries = true;
          } else {
            this.noCountries = false;
            for (const item of res.results) {
              if (this.countries.length) {
                const uniqueCountries = this.countries.filter(x => x._id !== item._id);
                this.countries = uniqueCountries;
              }
              this.countries.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        },
          err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
      );
    }
    if (searchValue.length <= 0) {
      this.noCountries = false;
      this.countriesInputFocused = true;
    } else {
      this.countriesInputFocused = false;
    }
  }

  loadMoreCountries() {
    this.selectedCountriesPage = this.selectedCountriesPage + 1;
    this.subscriptions.push(
      this.data.get(countriesApi, this.selectedCountriesPage).subscribe((res: IDataRes) => {
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        } else {
          this.hasMoreCountries = false;
        }
        for (const item of res.results) {
          if (this.countries.length) {
            const uniqueCountries = this.countries.filter(x => x._id !== item._id);
            this.countries = uniqueCountries;
          }
          this.countries.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  get form() {
    return this.cityForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.city) {
      if (this.cityForm.valid) {
        const newCity = { _id: this.city._id, ...this.generalService.checkEmptyFields(this.cityForm.value) };
        this.subscriptions.push(
          this.data.put(citiesApi, newCity).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/cities']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.cityForm.valid) {
        const formValue = this.generalService.checkEmptyFields(
          this.cityForm.value
        );
        this.subscriptions.push(
          this.data.post(citiesApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.cityForm.reset();
              this.dropzone.reset();
              this.cityForm.patchValue({
                isActive: true
              });
              this.filesAdded = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let countryId = '';
    let code = '';
    let cityNameAr = '';
    let cityNameEn = '';
    let isActive = true;

    if (this.city) {
      image = this.city.image;
      countryId = this.city.countryId;
      code = this.city.code;
      cityNameAr = this.city.cityNameAr;
      cityNameEn = this.city.cityNameEn;
      isActive = this.city.isActive;
    }
    this.cityForm = new FormGroup({
      image: new FormControl(image),
      countryId: new FormControl(countryId, Validators.required),
      code: new FormControl(code, Validators.required),
      cityNameAr: new FormControl(cityNameAr, Validators.required),
      cityNameEn: new FormControl(cityNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
