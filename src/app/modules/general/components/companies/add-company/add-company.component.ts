import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from '../../../services/general.service';
import { CustomValidators } from 'ng2-validation';
import { IRegion } from '../../../interfaces/IRegion.model';
import { ICity } from '../../../interfaces/ICity.model';
import { searchLength } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICompany } from '../../../interfaces/ICompany';
import { ICurrency } from '../../../interfaces/ICurrency';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { baseUrl, companiesApi, currenciesApi, citiesApi, regionsApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit, OnDestroy {
  companyForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  company: ICompany;
  filesAdded: boolean;
  subscriptions: Subscription[] = [];
  formReady: boolean;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  @ViewChild('dropzone') dropzone: any;
  baseUrl = baseUrl;
  detailsMode: boolean;

  // Currencies
  currencies: ICurrency[] = [];
  currenciesInputFocused: boolean;
  hasMoreCurrencies: boolean;
  currenciesCount: number;
  selectedCurrenciesPage = 1;
  currenciesPagesNo: number;
  noCurrencies: boolean;

  // Cities
  cities: ICity[] = [];
  citiesInputFocused: boolean;
  hasMoreCities: boolean;
  citiesCount: number;
  selectedCitiesPage = 1;
  citiesPagesNo: number;
  noCities: boolean;

  // Regions
  regions: IRegion[] = [];
  regionsInputFocused: boolean;
  hasMoreRegions: boolean;
  regionsCount: number;
  selectedRegionsPage = 1;
  regionsPagesNo: number;
  noRegions: boolean;

  constructor(
    private generalService: GeneralService,
    private data: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(companiesApi, null, null, id).subscribe((company: ICompany) => {
              this.company = company;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.companyForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            },
              err => {
                this.uiService.showErrorMessage(err);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(currenciesApi, 1).subscribe((res: IDataRes) => {
        this.currenciesPagesNo = res.pages;
        this.currenciesCount = res.count;
        if (this.currenciesPagesNo > this.selectedCurrenciesPage) {
          this.hasMoreCurrencies = true;
        }
        this.currencies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(citiesApi, 1).subscribe((res: IDataRes) => {
        this.citiesPagesNo = res.pages;
        this.citiesCount = res.count;
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        }
        this.cities.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(regionsApi, 1).subscribe((res: IDataRes) => {
        this.regionsPagesNo = res.pages;
        this.regionsCount = res.count;
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        }
        this.regions.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.companyForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.company.image = '';
    this.companyForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.companyForm.controls;
  }

  get getCompanyCurrencyArray() {
    return this.companyForm.get('companyCurrency') as FormArray;
  }

  get getBranchesArray() {
    return this.companyForm.get('branches') as FormArray;
  }

  companyCurrencyArray(i) {
    return (this.companyForm.get('companyCurrency') as FormArray).controls[i];
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  addCompanyCurrency() {
    const control = this.companyForm.get('companyCurrency') as FormArray;
    control.push(new FormGroup({
      currencyId: new FormControl(''),
      exchangeRate: new FormControl('', CustomValidators.number),
      isDefault: new FormControl(false),
    }));
  }

  deleteCompanyCurrency(index) {
    const control = this.companyForm.get('companyCurrency') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBranch() {
    const control = this.companyForm.get('branches') as FormArray;
    const newBranch = new FormGroup({
      regionId: new FormControl(),
      cityId: new FormControl(),
      branchCurrency: new FormArray([
        new FormGroup({
          currencyId: new FormControl('', Validators.required),
          exchangeRate: new FormControl('', [Validators.required, CustomValidators.number]),
          isDefault: new FormControl(false, Validators.required),
        })
      ]),
      BranchResponsible: new FormArray([
        new FormGroup({
          responsibleName: new FormControl(''),
          responsibleJob: new FormControl(''),
          responsiblePhone: new FormControl('', [CustomValidators.number]),
          responsibleInternalPhone: new FormControl('', [CustomValidators.number]),
          responsibleMobile: new FormControl('', [CustomValidators.number]),
          responsibleEmail: new FormControl('', [Validators.email]),
        })
      ]),
      code: new FormControl(''),
      branchNameAr: new FormControl(''),
      branchNameEn: new FormControl(),
      branchNotice: new FormControl(),
      branchAddress: new FormControl(),
      branchPhone: new FormControl('', [CustomValidators.number]),
      branchMobile: new FormControl('', [CustomValidators.number]),
      branchEmail: new FormControl('', [Validators.email])
    });
    control.push(newBranch);
  }

  deleteBranch(index) {
    const control = this.companyForm.get('branches') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBranchCurrency(control) {
    control.push(
      new FormGroup({
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl('', [Validators.required, CustomValidators.number]),
        isDefault: new FormControl(false, Validators.required),
      })
    );
  }

  deleteBranchCurrency(control, index) {
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBranchResponsible(control) {
    control.push(
      new FormGroup({
        responsibleName: new FormControl(''),
        responsibleJob: new FormControl(''),
        responsiblePhone: new FormControl('', [CustomValidators.number]),
        responsibleInternalPhone: new FormControl('', [CustomValidators.number]),
        responsibleMobile: new FormControl('', [CustomValidators.number]),
        responsibleEmail: new FormControl('', [Validators.email]),
      })
    );
  }

  deleteBranchResponsible(control, index) {
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  searchRegions(event) {
    const searchValue = event;
    const searchQuery = {
      regionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(regionsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noRegions = true;
          } else {
            this.noRegions = false;
            for (const item of res.results) {
              if (this.regions.length) {
                const uniqueRegions = this.regions.filter(x => x._id !== item._id);
                this.regions = uniqueRegions;
              }
              this.regions.push(item);
            }
          }
          this.regions = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreRegions() {
    this.selectedRegionsPage = this.selectedRegionsPage + 1;
    this.subscriptions.push(
      this.data.get(regionsApi, this.selectedRegionsPage).subscribe((res: IDataRes) => {
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        } else {
          this.hasMoreRegions = false;
        }
        for (const item of res.results) {
          if (this.regions.length) {
            const uniqueRegions = this.regions.filter(x => x._id !== item._id);
            this.regions = uniqueRegions;
          }
          this.regions.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCities(event) {
    const searchValue = event;
    const searchQuery = {
      cityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(citiesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCities = true;
          } else {
            this.noCities = false;
            for (const item of res.results) {
              if (this.cities.length) {
                const uniqueCities = this.cities.filter(x => x._id !== item._id);
                this.cities = uniqueCities;
              }
              this.cities.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCities() {
    this.selectedCitiesPage = this.selectedCitiesPage + 1;
    this.subscriptions.push(
      this.data.get(citiesApi, this.selectedCitiesPage).subscribe((res: IDataRes) => {
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        } else {
          this.hasMoreCities = false;
        }
        for (const item of res.results) {
          if (this.cities.length) {
            const uniqueCities = this.cities.filter(x => x._id !== item._id);
            this.cities = uniqueCities;
          }
          this.cities.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCurrencies(event) {
    const searchValue = event;
    const searchQuery = {
      currencyNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(currenciesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCurrencies = true;
          } else {
            this.noCurrencies = false;
            for (const item of res.results) {
              if (this.currencies.length) {
                const uniqueCurrencies = this.currencies.filter(x => x._id !== item._id);
                this.currencies = uniqueCurrencies;
              }
              this.currencies.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCurrencies() {
    this.selectedCurrenciesPage = this.selectedCurrenciesPage + 1;
    this.subscriptions.push(
      this.data.get(currenciesApi, this.selectedCurrenciesPage).subscribe((res: IDataRes) => {
        if (this.currenciesPagesNo > this.selectedCurrenciesPage) {
          this.hasMoreCurrencies = true;
        } else {
          this.hasMoreCurrencies = false;
        }
        for (const item of res.results) {
          if (this.currencies.length) {
            const uniqueCurrencies = this.currencies.filter(x => x._id !== item._id);
            this.currencies = uniqueCurrencies;
          }
          this.currencies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.company) {
      if (this.companyForm.valid) {
        const newBranches = [];
        for (let i = 0; i < this.companyForm.value.branches.length; i++) {
          if (this.company.branches[i]) {
            newBranches.push({
              _id: this.company.branches[i]._id,
              ...this.generalService.checkEmptyFields(this.companyForm.value.branches[i])
            });
          } else {
            newBranches.push({
              ...this.generalService.checkEmptyFields(this.companyForm.value.branches[i])
            });
          }

        }
        const newCompany = {
          _id: this.company._id,
          ...this.generalService.checkEmptyFields(this.companyForm.value),
          branches: newBranches
        };
        this.subscriptions.push(
          this.data.put(companiesApi, newCompany).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/companies']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
        this.loadingButton = false;
        this.submitted = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      }
    } else {
      if (this.companyForm.valid) {
        const formValue = this.generalService.checkEmptyFields(
          this.companyForm.value
        );
        this.subscriptions.push(
          this.data.post(companiesApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.companyForm.reset();
              this.companyForm.patchValue({
                isActive: true,
                calendarType: 'hijri'
              });
              this.submitted = false;
              this.dropzone.reset();
              this.filesAdded = false;
              this.uiService.isLoading.next(false);
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  fillRate(event, index, array) {
    const control = array;
    if (event.target.checked) {
      const controlLength = control.controls.length;
      if (controlLength > 1) {
        for (let i = 0; i <= controlLength - 1; i++) {
          control.controls[i].patchValue({
            isDefault: false
          });
        }
      }
      control.controls[index].patchValue({
        isDefault: true
      });
      control.controls[index].patchValue({
        exchangeRate: '1'
      });
    }
  }

  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true;
  }

  private initForm() {
    let image = '';
    let calendarType = 'hijri';
    let code = '';
    let companyNameAr = '';
    let companyNameEn = '';
    let companyActivity = '';
    let taxNumber = '';
    let isActive = true;
    let branchCurrencyArray = new FormArray([
      new FormGroup({
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl('', [Validators.required, CustomValidators.number]),
        isDefault: new FormControl(false, Validators.required),
      })
    ]);
    let BranchResponsibleArray = new FormArray([
      new FormGroup({
        responsibleName: new FormControl(''),
        responsibleJob: new FormControl(''),
        responsiblePhone: new FormControl('', [CustomValidators.number]),
        responsibleInternalPhone: new FormControl('', [CustomValidators.number]),
        responsibleMobile: new FormControl('', [CustomValidators.number]),
        responsibleEmail: new FormControl('', [Validators.email]),
      })
    ]);
    let companyCurrencyArray = new FormArray([
      new FormGroup({
        currencyId: new FormControl('', Validators.required),
        exchangeRate: new FormControl('', [Validators.required, CustomValidators.number]),
        isDefault: new FormControl(false, Validators.required)
      })
    ]);
    let branchesArray = new FormArray([
      new FormGroup({
        branchCurrency: branchCurrencyArray,
        BranchResponsible: BranchResponsibleArray,
        code: new FormControl('', Validators.required),
        branchNameAr: new FormControl('', Validators.required),
        branchNameEn: new FormControl(''),
        branchNotice: new FormControl(''),
        branchAddress: new FormControl(''),
        regionId: new FormControl(''),
        cityId: new FormControl(''),
        branchPhone: new FormControl('', [CustomValidators.number]),
        branchMobile: new FormControl('', [CustomValidators.number]),
        branchEmail: new FormControl('', [Validators.email])
      })
    ]);
    if (this.company) {
      image = this.company.image;
      calendarType = this.company.calendarType;
      code = this.company.code;
      companyNameAr = this.company.companyNameAr;
      companyNameEn = this.company.companyNameEn;
      companyActivity = this.company.companyActivity;
      taxNumber = this.company.taxNumber;
      isActive = this.company.isActive;
      companyCurrencyArray = new FormArray([]);
      branchesArray = new FormArray([]);
      for (const control of this.company.companyCurrency) {
        companyCurrencyArray.push(
          new FormGroup({
            currencyId: new FormControl(control.currencyId, Validators.required),
            exchangeRate: new FormControl(control.exchangeRate, [Validators.required, CustomValidators.number]),
            isDefault: new FormControl(control.isDefault, Validators.required)
          })
        );
      }

      for (const branch of this.company.branches) {
        branchCurrencyArray = new FormArray([]);
        for (const branchCurrency of branch.branchCurrency) {
          branchCurrencyArray.push(
            new FormGroup({
              currencyId: new FormControl(branchCurrency.currencyId, Validators.required),
              exchangeRate: new FormControl(branchCurrency.exchangeRate, [Validators.required, CustomValidators.number]),
              isDefault: new FormControl(branchCurrency.isDefault, Validators.required),
            })
          );
        }
        if (branch.BranchResponsible.length) {
          BranchResponsibleArray = new FormArray([]);
          for (const responsible of branch.BranchResponsible) {
            BranchResponsibleArray.push(
              new FormGroup({
                responsibleName: new FormControl(responsible.responsibleName),
                responsibleJob: new FormControl(responsible.responsibleJob),
                responsiblePhone: new FormControl(responsible.responsiblePhone, [CustomValidators.number]),
                responsibleInternalPhone: new FormControl(responsible.responsibleInternalPhone, [CustomValidators.number]),
                responsibleMobile: new FormControl(responsible.responsibleMobile, [CustomValidators.number]),
                responsibleEmail: new FormControl(responsible.responsibleEmail, [Validators.email]),
              })
            );
          }
        }

        branchesArray.push(
          new FormGroup({
            regionId: new FormControl(branch.regionId),
            cityId: new FormControl(branch.cityId),
            branchCurrency: branchCurrencyArray,
            BranchResponsible: BranchResponsibleArray,
            code: new FormControl(branch.code, Validators.required),
            branchNameAr: new FormControl(branch.branchNameAr, Validators.required),
            branchNameEn: new FormControl(branch.branchNameEn),
            branchNotice: new FormControl(branch.branchNotice),
            branchAddress: new FormControl(branch.branchAddress),
            branchPhone: new FormControl(branch.branchPhone),
            branchMobile: new FormControl(branch.branchMobile),
            branchEmail: new FormControl(branch.branchEmail, [Validators.email])
          })
        );
      }
    }

    this.companyForm = new FormGroup({
      image: new FormControl(image),
      calendarType: new FormControl(calendarType),
      code: new FormControl(code, Validators.required),
      companyNameAr: new FormControl(companyNameAr, Validators.required),
      companyNameEn: new FormControl(companyNameEn),
      companyActivity: new FormControl(companyActivity),
      taxNumber: new FormControl(taxNumber),
      isActive: new FormControl(isActive, Validators.required),
      companyCurrency: companyCurrencyArray,
      branches: branchesArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
