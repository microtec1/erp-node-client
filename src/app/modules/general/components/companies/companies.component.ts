import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { ICompany } from '../../interfaces/ICompany';
import { GeneralService } from '../../services/general.service';
import { baseUrl, companiesApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})

export class CompaniesComponent implements OnInit, OnDestroy {
  companies: ICompany[];
  subscriptions: Subscription[] = [];
  bsModalRef: BsModalRef;
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  baseUrl = baseUrl;
  sortType: string;
  sortValue: string;

  constructor(
    private uiService: UiService,
    private modalService: BsModalService,
    private data: DataService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getCompaniesFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(companiesApi, pageNumber).subscribe((res: IDataRes) => {
      this.companies = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(companiesApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.companies = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCompaniesFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(companiesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.companies = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/company.jpg';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      companyNameAr: new FormControl(''),
      companyNameEn: new FormControl('')
    });
  }

  deleteModal(company: ICompany) {
    const initialState = {
      code: company.code,
      nameAr: company.companyNameAr,
      nameEn: company.companyNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(company._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(companiesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.companies = this.generalService.removeItem(this.companies, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getCompaniesFirstPage() {
    this.subscriptions.push(
      this.data.get(companiesApi, 1).subscribe((res: IDataRes) => {
        this.companies = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
