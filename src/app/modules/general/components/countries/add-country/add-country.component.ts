import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ICountry } from '../../../interfaces/ICountry.model';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from '../../../services/general.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, countriesApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.scss']
})
export class AddCountryComponent implements OnInit, OnDestroy {
  countryForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  country: ICountry;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(countriesApi, null, null, id).subscribe((country: ICountry) => {
              this.country = country;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.countryForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.countryForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.country.image = '';
    this.countryForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.countryForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.country) {
      if (this.countryForm.valid) {
        const newCountry = { _id: this.country._id, ...this.generalService.checkEmptyFields(this.countryForm.value) };
        this.subscriptions.push(
          this.data.put(countriesApi, newCountry).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/countries']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.countryForm.valid) {
        const formValue = this.generalService.checkEmptyFields(this.countryForm.value);
        this.subscriptions.push(
          this.data.post(countriesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.countryForm.reset();
            this.countryForm.patchValue({
              isActive: true
            });
            this.dropzone.reset();
            this.filesAdded = false;
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let countryNameAr = '';
    let countryNameEn = '';
    let isActive = true;

    if (this.country) {
      image = this.country.image;
      code = this.country.code;
      countryNameAr = this.country.countryNameAr;
      countryNameEn = this.country.countryNameEn;
      isActive = this.country.isActive;
    }
    this.countryForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      countryNameAr: new FormControl(countryNameAr, Validators.required),
      countryNameEn: new FormControl(countryNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
