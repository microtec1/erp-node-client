import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ICurrency } from '../../../interfaces/ICurrency';
import { GeneralService } from '../../../services/general.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, Params, RouterStateSnapshot } from '@angular/router';
import { baseUrl, currenciesApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-currency',
  templateUrl: './add-currency.component.html',
  styleUrls: ['./add-currency.component.scss']
})
export class AddCurrencyComponent implements OnInit, OnDestroy {
  currencyForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  currency: ICurrency;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      if (id != null) {
        this.subscriptions.push(
          this.data.get(currenciesApi, null, null, id).subscribe((currency: ICurrency) => {
            this.currency = currency;
            this.formReady = true;
            this.initForm();
            if (this.detailsMode) {
              this.currencyForm.disable({ onlySelf: true });
            }
            this.uiService.isLoading.next(false);
          },
            err => {
              this.uiService.showErrorMessage(err);
            })
        );
      } else {
        this.formReady = true;
        this.initForm();
      }
    });
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.currencyForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.currency.image = '';
    this.currencyForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.currencyForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.currency) {
      if (this.currencyForm.valid) {
        const newCurrency = { _id: this.currency._id, ...this.generalService.checkEmptyFields(this.currencyForm.value) };
        this.subscriptions.push(
          this.data.put(currenciesApi, newCurrency).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/currencies']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.currencyForm.valid) {
        const formValue = this.generalService.checkEmptyFields(this.currencyForm.value);
        this.subscriptions.push(
          this.data.post(currenciesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.currencyForm.reset();
            this.currencyForm.patchValue({
              isActive: true
            });
            this.dropzone.reset();
            this.filesAdded = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let currencyNameAr = '';
    let currencyNameEn = '';
    let currencyDescriptionAr = '';
    let currencyDescriptionEn = '';
    let isActive = true;

    if (this.currency) {
      image = this.currency.image;
      code = this.currency.code;
      currencyNameAr = this.currency.currencyNameAr;
      currencyNameEn = this.currency.currencyNameEn;
      currencyDescriptionAr = this.currency.currencyDescriptionAr;
      currencyDescriptionEn = this.currency.currencyDescriptionEn;
      isActive = this.currency.isActive;
    }
    this.currencyForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      currencyNameAr: new FormControl(currencyNameAr, Validators.required),
      currencyNameEn: new FormControl(currencyNameEn),
      currencyDescriptionAr: new FormControl(currencyDescriptionAr),
      currencyDescriptionEn: new FormControl(currencyDescriptionEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
