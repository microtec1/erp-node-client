import { Component, OnInit, Output, EventEmitter, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from '../../../services/general.service';
import { IEmployee } from '../../../interfaces/IEmployee';
import { CustomValidators } from 'ng2-validation';
import { IRegion } from '../../../interfaces/IRegion.model';
import { ICity } from '../../../interfaces/ICity.model';
import { searchLength, companyId } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { Params, Router, ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { baseUrl, employeesApi, citiesApi, regionsApi, attachmentTypesApi, detailedChartOfAccountsApi, companiesApi } from 'src/app/common/constants/api.constants';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IAttachmentType } from '../../../interfaces/IAttachmentType';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit, OnDestroy {
  employeeForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  employee: IEmployee;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;
  @Output() newEmployees: EventEmitter<IEmployee[]> = new EventEmitter();

  // Cities
  cities: ICity[] = [];
  citiesInputFocused: boolean;
  hasMoreCities: boolean;
  citiesCount: number;
  selectedCitiesPage = 1;
  citiesPagesNo: number;
  noCities: boolean;

  // Regions
  regions: IRegion[] = [];
  regionsInputFocused: boolean;
  hasMoreRegions: boolean;
  regionsCount: number;
  selectedRegionsPage = 1;
  regionsPagesNo: number;
  noRegions: boolean;


  // Attachment Types
  attachmentTypes: IAttachmentType[] = [];
  attachmentTypesInputFocused: boolean;
  hasMoreAttachmentTypes: boolean;
  attachmentTypesCount: number;
  selectedAttachmentTypesPage = 1;
  attachmentTypesPagesNo: number;
  noAttachmentTypes: boolean;

  // Chart Of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  constructor(
    private generalService: GeneralService,
    private data: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(employeesApi, null, null, id).subscribe((employee: IEmployee) => {
              this.employee = employee;
              console.log(employee)
              if (this.employee.cityId) {
                this.subscriptions.push(
                  this.data.get(citiesApi, null, null, employee.cityId).subscribe((city: ICity) => {
                    if (!this.generalService.checkItem(this.cities, employee.cityId)) {
                      this.cities.push(city);
                    }
                    this.uiService.isLoading.next(false);
                  },
                    err => {
                      this.uiService.showErrorMessage(err);
                    })
                );
              }
              if (this.employee.regionId) {
                this.subscriptions.push(
                  this.data.get(regionsApi, null, null, employee.regionId).subscribe((region: IRegion) => {
                    if (!this.generalService.checkItem(this.regions, employee.regionId)) {
                      this.regions.push(region);
                    }
                    this.uiService.isLoading.next(false);
                  },
                    err => {
                      this.uiService.showErrorMessage(err);
                    })
                );
              }
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.employeeForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            },
              err => {
                this.uiService.showErrorMessage(err);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(citiesApi, 1).subscribe((res: IDataRes) => {
        this.citiesPagesNo = res.pages;
        this.citiesCount = res.count;
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        }
        this.cities.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(regionsApi, 1).subscribe((res: IDataRes) => {
        this.regionsPagesNo = res.pages;
        this.regionsCount = res.count;
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        }
        this.regions.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(attachmentTypesApi, 1).subscribe((res: IDataRes) => {
        this.attachmentTypesPagesNo = res.pages;
        this.attachmentTypesCount = res.count;
        if (this.attachmentTypesPagesNo > this.selectedAttachmentTypesPage) {
          this.hasMoreAttachmentTypes = true;
        }
        this.attachmentTypes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.employeeForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

   inputFileChanged(files: File[], formGroup: FormGroup) {
    files.forEach(item => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        formGroup.patchValue({
          file: content
        });
      };
      reader.readAsDataURL(item);
    });
  }

  resetFile(dropzoneFile, formGroup) {
    formGroup.patchValue({
      file: ''
    });
    dropzoneFile.reset();
  }

  removeImage() {
    this.employee.image = '';
    this.employeeForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.employeeForm.controls;
  }

  get filesArray() {
    return this.employeeForm.get('files') as FormArray;
  }

  addFile() {
    const control = this.employeeForm.get('files') as FormArray;
    control.push(
      new FormGroup({
        attachmentTypeId: new FormControl(''),
        file: new FormControl(''),
      })
    );
  }

  removeFile(index) {
    this.employee.files[index].file = null;
    this.filesArray.controls[index].patchValue({
      file: ''
    });
  }

  deleteFile(index) {
    const control = this.employeeForm.get('files') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  searchRegions(event) {
    const searchValue = event;
    const searchQuery = {
      regionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(regionsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noRegions = true;
          } else {
            this.noRegions = false;
            for (const item of res.results) {
              if (this.regions.length) {
                const uniqueRegions = this.regions.filter(x => x._id !== item._id);
                this.regions = uniqueRegions;
              }
              this.regions.push(item);
            }
          }
          this.regions = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreRegions() {
    this.selectedRegionsPage = this.selectedRegionsPage + 1;
    this.subscriptions.push(
      this.data.get(regionsApi, this.selectedRegionsPage).subscribe((res: IDataRes) => {
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        } else {
          this.hasMoreRegions = false;
        }
        for (const item of res.results) {
          if (this.regions.length) {
            const uniqueRegions = this.regions.filter(x => x._id !== item._id);
            this.regions = uniqueRegions;
          }
          this.regions.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCities(event) {
    const searchValue = event;
    const searchQuery = {
      cityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(citiesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCities = true;
          } else {
            this.noCities = false;
            for (const item of res.results) {
              if (this.cities.length) {
                const uniqueCities = this.cities.filter(x => x._id !== item._id);
                this.cities = uniqueCities;
              }
              this.cities.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCities() {
    this.selectedCitiesPage = this.selectedCitiesPage + 1;
    this.subscriptions.push(
      this.data.get(citiesApi, this.selectedCitiesPage).subscribe((res: IDataRes) => {
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        } else {
          this.hasMoreCities = false;
        }
        for (const item of res.results) {
          if (this.cities.length) {
            const uniqueCities = this.cities.filter(x => x._id !== item._id);
            this.cities = uniqueCities;
          }
          this.cities.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  getLastWord(link) {
    if (link) {
      const str = link;
      const n = str.lastIndexOf('/');
      const result = str.substring(n + 1);
      return result;
    }
  }

  searchAttachmentTypes(event) {
    const searchValue = event;
    const searchQuery = {
      attachmentTypeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(attachmentTypesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noAttachmentTypes = true;
          } else {
            this.noAttachmentTypes = false;
            for (const item of res.results) {
              if (this.attachmentTypes.length) {
                const uniqueAttachmentTypes = this.attachmentTypes.filter(x => x._id !== item._id);
                this.attachmentTypes = uniqueAttachmentTypes;
              }
              this.attachmentTypes.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreAttachmentTypes() {
    this.selectedAttachmentTypesPage = this.selectedAttachmentTypesPage + 1;
    this.subscriptions.push(
      this.data.get(attachmentTypesApi, this.selectedAttachmentTypesPage).subscribe((res: IDataRes) => {
        if (this.attachmentTypesPagesNo > this.selectedAttachmentTypesPage) {
          this.hasMoreAttachmentTypes = true;
        } else {
          this.hasMoreAttachmentTypes = false;
        }
        for (const item of res.results) {
          if (this.attachmentTypes.length) {
            const uniqueAttachmentTypes = this.attachmentTypes.filter(x => x._id !== item._id);
            this.attachmentTypes = uniqueAttachmentTypes;
          }
          this.attachmentTypes.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.employee) {
      if (this.employeeForm.valid) {
        const newEmployee = {
          _id: this.employee._id,
          ...this.generalService.checkEmptyFields(this.employeeForm.value)
        };
        const emptyObject = this.generalService.isEmpty(this.employeeForm.value.files[0]);
        if (emptyObject) {
          newEmployee.files = [];
        }
        this.subscriptions.push(
          this.data.put(employeesApi, newEmployee).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/employees']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.employeeForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(
            this.employeeForm.value
          )
        };
        const emptyObject = this.generalService.isEmpty(this.employeeForm.value.files[0]);
        if (emptyObject) {
          formValue.files = [];
        }
        this.subscriptions.push(
          this.data.post(employeesApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.employeeForm.reset();
              this.employeeForm.patchValue({
                isActive: true
              });
              this.dropzone.reset();
              this.filesAdded = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let employeeNameAr = '';
    let employeeNameEn = '';
    let gender = '';
    let qualification = '';
    let salesRepresentative = false;
    let accountId = '';
    let regionId = '';
    let cityId = '';
    let employeeAddress = '';
    let employeePhone = null;
    let employeeMobile = null;
    let employeeWhatsApp = '';
    let employeeEmail = '';
    let employeeNote = '';
    let isActive = true;
    let filesArray = new FormArray([
      new FormGroup({
        attachmentTypeId: new FormControl(''),
        file: new FormControl(''),
      })
    ]);

    if (this.employee) {
      image = this.employee.image;
      code = this.employee.code;
      employeeNameAr = this.employee.employeeNameAr;
      employeeNameEn = this.employee.employeeNameEn;
      gender = this.employee.gender;
      qualification = this.employee.qualification;
      salesRepresentative = this.employee.salesRepresentative;
      accountId = this.employee.accountId;
      regionId = this.employee.regionId;
      cityId = this.employee.cityId;
      employeeAddress = this.employee.employeeAddress;
      employeePhone = this.employee.employeePhone;
      employeeMobile = this.employee.employeeMobile;
      employeeWhatsApp = this.employee.employeeWhatsApp;
      employeeEmail = this.employee.employeeEmail;
      employeeNote = this.employee.employeeNote;
      isActive = this.employee.isActive;
      if (this.employee.files.length) {
        filesArray = new FormArray([]);
        for (const control of this.employee.files) {
          filesArray.push(
            new FormGroup({
              attachmentTypeId: new FormControl(control.attachmentTypeId),
              file: new FormControl(control.file),
            })
          );
        }
      }
    }
    this.employeeForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      employeeNameAr: new FormControl(employeeNameAr, Validators.required),
      employeeNameEn: new FormControl(employeeNameEn),
      gender: new FormControl(gender),
      qualification: new FormControl(qualification),
      salesRepresentative: new FormControl(salesRepresentative),
      accountId: new FormControl(accountId),
      regionId: new FormControl(regionId),
      cityId: new FormControl(cityId),
      employeeAddress: new FormControl(employeeAddress),
      employeePhone: new FormControl(employeePhone, [CustomValidators.number]),
      employeeMobile: new FormControl(employeeMobile, [CustomValidators.number]),
      employeeWhatsApp: new FormControl(employeeWhatsApp),
      employeeEmail: new FormControl(employeeEmail, Validators.email),
      employeeNote: new FormControl(employeeNote),
      isActive: new FormControl(isActive, Validators.required),
      files: filesArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
