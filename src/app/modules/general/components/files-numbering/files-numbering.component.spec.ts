import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilesNumberingComponent } from './files-numbering.component';

describe('FilesNumberingComponent', () => {
  let component: FilesNumberingComponent;
  let fixture: ComponentFixture<FilesNumberingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilesNumberingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilesNumberingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
