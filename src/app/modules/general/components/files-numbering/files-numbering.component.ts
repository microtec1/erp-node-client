import { Component, OnInit, OnDestroy } from '@angular/core';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { GeneralService } from '../../services/general.service';
import { FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { companyId } from 'src/app/common/constants/general.constants';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from 'src/app/common/services/shared/data.service';
import { filesNumberingApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-files-numbering',
  templateUrl: './files-numbering.component.html',
  styleUrls: ['./files-numbering.component.scss']
})
export class FilesNumberingComponent implements OnInit, OnDestroy {
  filesNumberingForm: FormGroup;
  filesNumbering: any;
  modules: any[] = [];
  manualMode: boolean;
  loadingButton: boolean;
  companyId = companyId;
  subscriptions: Subscription[] = [];
  constructor(
    private data: DataService,
    private generalService: GeneralService,
    private uiService: UiService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.data.getByRoute(filesNumberingApi).subscribe((res: IDataRes) => {
        // this.filesNumbering = res.results[0];
        console.log(res);

        // this.modules = this.generalService.getObjects(this.filesNumbering).array;

        // const componentsNames = [];
        // this.modules.map(item => {
        //   item.components = this.generalService.getObjects(item).array;
        //   const names = this.generalService.getObjects(item).objectKeys;
        //   names.pop();
        //   componentsNames.push(names);
        // });
        // this.initForm();
        // const modulesNames = this.generalService.getObjects(this.filesNumbering).objectKeys;

        // for (let i = 0; i <= modulesNames.length - 1; i++) {
        //   this.filesNumberingForm.setControl(modulesNames[i], new FormGroup({}));
        //   this.modules[i].moduleName = modulesNames[i];
        //   for (let k = 0; k <= componentsNames[i].length - 1; k++) {
        //     (this.filesNumberingForm.get(modulesNames[i]) as FormGroup).setControl(componentsNames[i][k], new FormGroup({
        //       numberingSystem: new FormControl('manual'),
        //       startNumber: new FormControl(0),
        //     }));
        //     this.modules[i].components[k].componentName = componentsNames[i][k];

        //   }
        // }
        this.uiService.isLoading.next(false);
      })
    );

  }

  checkType(moduleName, componentName, value) {
    const componentGroup = this.filesNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (value === 'manual') {
      this.manualMode = true;
      componentGroup.patchValue({
        startNumber: 0
      });
      componentGroup.controls.startNumber.clearValidators();
      componentGroup.controls.startNumber.updateValueAndValidity();
    } else {
      this.manualMode = false;
      componentGroup.patchValue({
        startNumber: 1
      });
      componentGroup.controls.startNumber.setValidators([Validators.required, CustomValidators.number, CustomValidators.notEqual(0)]);
      componentGroup.controls.startNumber.updateValueAndValidity();
    }
  }

  checkManualMode(moduleName, componentName) {
    const componentGroup = this.filesNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (componentGroup.controls.numberingSystem.value === 'manual') {
      return true;
    } else {
      return false;
    }
  }

  checkValidation(moduleName, componentName, validationName) {
    const componentGroup = this.filesNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (componentGroup.controls.startNumber.errors && componentGroup.controls.numberingSystem.value !== 'manual') {
      if (componentGroup.controls.startNumber.errors[validationName]) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkInvalidFeedback(moduleName, componentName) {
    const componentGroup = this.filesNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (componentGroup.controls.startNumber.errors && componentGroup.controls.numberingSystem.value !== 'manual') {
      return true;
    } else {
      return false;
    }
  }

  submit() {
    console.log(this.filesNumberingForm.value);
    this.loadingButton = true;
    const formValue = {
      ...this.filesNumberingForm.value,
      companyId: this.companyId
    };
    // this.subscriptions.push(
    //   this.filesNumberingService.saveFilesNumbering(formValue).subscribe(res => {
    //     this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
    //     this.loadingButton = false;
    //     this.uiService.isLoading.next(false);
    //   }, err => {
    //     this.loadingButton = false;
    //     this.uiService.isLoading.next(false);
    //     if (this.translate.currentLang === 'en') {
    //       this.uiService.showError(err.error.error.en, '');
    //     } else {
    //       this.uiService.showError(err.error.error.ar, '');
    //     }
    //   })
    // );
  }

  get form() {
    return this.filesNumberingForm.controls;
  }

  private initForm() {
    this.filesNumberingForm = new FormGroup({});
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
