import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from '../../../services/general.service';
import { CustomValidators } from 'ng2-validation';
import { IFinancialYear } from '../../../interfaces/IFinancialYear';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, fiscalYearsApi, nextFiscalYearDateApi } from 'src/app/common/constants/api.constants';
import { companyId } from 'src/app/common/constants/general.constants';
import { DataService } from 'src/app/common/services/shared/data.service';


@Component({
  selector: 'app-add-financial-year',
  templateUrl: './add-financial-year.component.html',
  styleUrls: ['./add-financial-year.component.scss']
})
export class AddFinancialYearComponent implements OnInit {
  financialYearForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  financialYear: IFinancialYear;
  filesAdded: boolean;
  financialYearSet: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  companyId = companyId;
  baseUrl = baseUrl;
  detailsMode: boolean;
  isFirstYear: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(fiscalYearsApi, null, null, id).subscribe((financialYear: IFinancialYear) => {
              this.financialYear = financialYear;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.financialYearForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
              this.financialYearSet = true;
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.subscriptions.push(
            this.data.getByRoute(nextFiscalYearDateApi).subscribe((res: any) => {
              if (res) {
                this.isFirstYear = false;
                this.financialYearForm.patchValue({
                  fiscalYearStartDateGregorian: new Date(res.nextFiscalYearStartGregorianDate + 'UTC'),
                  fiscalYearPeriodsCount: 1
                });
                this.setEndDate(new Date(res.nextFiscalYearStartGregorianDate + 'UTC'), 1);
              } else {
                this.isFirstYear = true;
              }
              this.uiService.isLoading.next(false);
            })
          );
        }
      })
    );
  }

  setEndDate(value: Date, fiscalYearPeriodsCount) {
    if (value) {
      const periods = parseInt(fiscalYearPeriodsCount, 0);
      const endDate = new Date(value.getFullYear(), value.getMonth() + periods, 0);
      const startHijriDate = this.generalService.convertToHijri(value);
      const endHijriDate = this.generalService.convertToHijriWithFormat(endDate);

      this.financialYearForm.patchValue({
        fiscalYearStartDateHijri: { year: startHijriDate.iYear(), month: startHijriDate.iMonth() + 1, day: startHijriDate.iDate() },
        fiscalYearEndDateGregorian: this.generalService.format(endDate),
        fiscalYearEndDateHijri: endHijriDate
      });
    }
  }

  onHijriDateSelect(event) {
    const startDate = this.generalService.convertToGregorian(event);
    this.financialYearForm.patchValue({
      fiscalYearStartDateGregorian: this.generalService.format(new Date(startDate.year(), startDate.month(), startDate.date()))
    });
  }

  disablePeriodStatus(array, index) {
    if (array.controls[index].controls.periodStatus.value === 'opened') {
      this.financialYearForm.patchValue({
        fiscalYearStatus: 'opened'
      });
    }
    if (array.controls[index].controls.periodStatus.value === 'closed' && index !== array.controls.length - 1) {
      if (array.controls[index + 1].controls.periodStatus.value === 'opened') {
        return false;
      }
      return true;
    } else {
      if (index === 0 || array.controls[index - 1].controls.periodStatus.value === 'opened' ||
        array.controls[index - 1].controls.periodStatus.value === 'closed') {
        return !this.financialYearSet;
      } else {
        return true;
      }
    }
  }

  disablePeriodStatusOption(array, index) {
    if (this.financialYearSet) {
      if (index === 0) {
        if (array.controls[index].controls.periodStatus.value === 'opened') {
          return false;
        } else {
          return true;
        }
      } else {
        if (array.controls[index - 1].controls.periodStatus.value === 'closed') {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  addFinancialYear(fiscalYearPeriodsCount, startDate: Date) {
    this.financialYearSet = true;
    this.financialYearForm.patchValue({
      fiscalYearStatus: 'planned'
    });
    const control = this.financialYearForm.get('accountingPeriods') as FormArray;
    const startYear = new Date(startDate).getFullYear();
    const startMonth = new Date(startDate).getMonth();
    const startDay = new Date(startDate).getDate();
    control.clear();
    control.push(
      new FormGroup({
        periodCode: new FormControl('1', [Validators.required, CustomValidators.number]),
        periodStatus: new FormControl('planned'),
        periodStartDateGregorian: new FormControl(this.generalService.format(new Date(startYear, startMonth, startDay))),
        periodStartDateHijri: new FormControl(this.generalService.convertToHijriWithFormat(new Date(startDate))),
        periodEndDateGregorian: new FormControl(this.generalService.format(new Date(startYear, startMonth + 1, 0))),
        periodEndDateHijri: new FormControl(this.generalService.convertToHijriWithFormat(new Date(startYear, startMonth + 1, 0)))
      })
    );
    for (let i = 1; i <= fiscalYearPeriodsCount - 1; i++) {
      control.push(
        new FormGroup({
          periodCode: new FormControl((i + 1).toString(), [Validators.required, CustomValidators.number]),
          periodStatus: new FormControl('planned'),
          periodStartDateGregorian: new FormControl(this.generalService.format(new Date(startYear, startMonth + i, 1))),
          periodStartDateHijri: new FormControl(this.generalService.convertToHijriWithFormat(new Date(startYear, startMonth + i, 1))),
          periodEndDateGregorian: new FormControl(this.generalService.format(new Date(startYear, (startMonth + i) + 1, 0))),
          periodEndDateHijri: new FormControl(this.generalService.convertToHijriWithFormat(new Date(startYear, (startMonth + i) + 1, 0)))
        })
      );
    }
  }

  checkValidation(no, date) {
    if (no !== '' && no !== null && date !== '' && date !== null) {
      return false;
    } else {
      return true;
    }
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.financialYearForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.financialYear.image = '';
    this.financialYearForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.financialYearForm.controls;
  }

  get getAccountingPeriodsArray() {
    return this.financialYearForm.get('accountingPeriods') as FormArray;
  }

  get fiscalYearEndDateGregorian() {
    return this.financialYearForm.controls.fiscalYearStartDateGregorian.value;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.financialYear) {
      if (this.financialYearForm.valid) {
        if (typeof this.financialYearForm.value.fiscalYearStartDateGregorian !== 'string') {
          this.financialYearForm.value.fiscalYearStartDateGregorian =
            this.generalService.format(this.financialYearForm.value.fiscalYearStartDateGregorian);
        }
        const newFinancialYear = {
          _id: this.financialYear._id,
          ...this.generalService.checkEmptyFields(this.financialYearForm.value),
          fiscalYearStartDateHijri: this.generalService.formatHijriDate(this.financialYearForm.value.fiscalYearStartDateHijri)
        };
        console.log(newFinancialYear);

        this.subscriptions.push(
          this.data.put(fiscalYearsApi, newFinancialYear).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/financialYears']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.financialYearForm.valid) {
        const formValue = this.generalService.checkEmptyFields(this.financialYearForm.value);
        if (typeof formValue.fiscalYearStartDateGregorian !== 'string') {
          formValue.fiscalYearStartDateGregorian = this.generalService.format(formValue.fiscalYearStartDateGregorian)
        }
        const formValueModified = {
          ...formValue,
          companyId: this.companyId,
          fiscalYearStartDateHijri: this.generalService.formatHijriDate(this.financialYearForm.value.fiscalYearStartDateHijri)
        };
        this.data.post(fiscalYearsApi, formValueModified).subscribe(res => {
          this.loadingButton = false;
          this.financialYearForm.reset();
          this.financialYearForm.patchValue({
            isActive: true
          });
          this.submitted = false;
          this.uiService.isLoading.next(false);
          this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
        },
          err => {
            this.loadingButton = false;
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          }
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let fiscalYearName = '';
    let fiscalYearPeriodsCount = '';
    let fiscalYearStatus = '';
    let isActive = true;
    let fiscalYearStartDateGregorian = null;
    let fiscalYearStartDateHijri = null;
    const fiscalYearEndDateGregorian = '';
    const fiscalYearEndDateHijri = '';
    let accountingPeriodsArray = new FormArray([
      new FormGroup({
        periodCode: new FormControl(''),
        periodStatus: new FormControl(''),
        periodStartDateGregorian: new FormControl(''),
        periodStartDateHijri: new FormControl(''),
        periodEndDateGregorian: new FormControl(''),
        periodEndDateHijri: new FormControl('')
      })
    ]);

    if (this.financialYear) {
      image = this.financialYear.image;
      code = this.financialYear.code;
      fiscalYearName = this.financialYear.fiscalYearName;
      fiscalYearPeriodsCount = this.financialYear.fiscalYearPeriodsCount;
      fiscalYearStatus = this.financialYear.fiscalYearStatus;
      fiscalYearStartDateGregorian = new Date(this.financialYear.fiscalYearStartDateGregorian + 'UTC');
      isActive = this.financialYear.isActive;
      accountingPeriodsArray = new FormArray([]);
      for (const period of this.financialYear.accountingPeriods) {
        accountingPeriodsArray.push(
          new FormGroup({
            periodCode: new FormControl(period.periodCode),
            periodStatus: new FormControl(period.periodStatus),
            periodStartDateGregorian: new FormControl(period.periodStartDateGregorian),
            periodStartDateHijri: new FormControl(period.periodStartDateHijri),
            periodEndDateGregorian: new FormControl(period.periodEndDateGregorian),
            periodEndDateHijri: new FormControl(period.periodEndDateHijri)
          })
        );
      }
    }

    this.financialYearForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, [Validators.required, CustomValidators.number]),
      fiscalYearName: new FormControl(fiscalYearName, Validators.required),
      fiscalYearPeriodsCount: new FormControl(fiscalYearPeriodsCount, [Validators.required, CustomValidators.number, Validators.min(1)]),
      fiscalYearStatus: new FormControl(fiscalYearStatus, Validators.required),
      isActive: new FormControl(isActive, Validators.required),
      fiscalYearStartDateGregorian: new FormControl(fiscalYearStartDateGregorian, Validators.required),
      fiscalYearStartDateHijri: new FormControl(fiscalYearStartDateHijri, Validators.required),
      fiscalYearEndDateGregorian: new FormControl(fiscalYearEndDateGregorian),
      fiscalYearEndDateHijri: new FormControl(fiscalYearEndDateHijri),
      accountingPeriods: accountingPeriodsArray,
    });
  }
}
