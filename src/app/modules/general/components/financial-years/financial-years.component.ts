import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IFinancialYear } from '../../interfaces/IFinancialYear';
import { GeneralService } from '../../services/general.service';
import { baseUrl, fiscalYearsApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-financial-years',
  templateUrl: './financial-years.component.html',
  styleUrls: ['./financial-years.component.scss']
})

export class FinancialYearsComponent implements OnInit, OnDestroy {
  financialYears: IFinancialYear[];
  subscriptions: Subscription[] = [];
  bsModalRef: BsModalRef;
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private uiService: UiService,
    private modalService: BsModalService,
    private data: DataService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getFinancialYearsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(fiscalYearsApi, pageNumber).subscribe((res: IDataRes) => {
      this.financialYears = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getFinancialYearsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(fiscalYearsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.financialYears = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      financialYearName: new FormControl('')
    });
  }
  deleteModal(financialYear: IFinancialYear) {
    const initialState = {
      code: financialYear.code,
      nameAr: financialYear.fiscalYearName,
      nameEn: financialYear.fiscalYearStatus
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(financialYear._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(fiscalYearsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.financialYears = this.generalService.removeItem(this.financialYears, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getFinancialYearsFirstPage() {
    this.subscriptions.push(
      this.data.get(fiscalYearsApi, 1).subscribe((res: IDataRes) => {
        this.financialYears = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
