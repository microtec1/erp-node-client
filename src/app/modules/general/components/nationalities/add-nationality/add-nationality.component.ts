import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { INationality } from '../../../interfaces/INationality';
import { GeneralService } from '../../../services/general.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, Params, RouterStateSnapshot } from '@angular/router';
import { baseUrl, nationalitiesApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-nationality',
  templateUrl: './add-nationality.component.html',
  styleUrls: ['./add-nationality.component.scss']
})
export class AddNationalityComponent implements OnInit, OnDestroy {
  nationalityForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  nationality: INationality;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(nationalitiesApi, null, null, id).subscribe((nationality: INationality) => {
              this.nationality = nationality;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.nationalityForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            },
              err => {
                this.uiService.showErrorMessage(err);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.nationalityForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.nationality.image = '';
    this.nationalityForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.nationalityForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.nationality) {
      if (this.nationalityForm.valid) {
        const newNationality = { _id: this.nationality._id, ...this.generalService.checkEmptyFields(this.nationalityForm.value) };
        this.subscriptions.push(
          this.data.put(nationalitiesApi, newNationality).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/nationalities']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.nationalityForm.valid) {
        const formValue = this.generalService.checkEmptyFields(this.nationalityForm.value);
        this.subscriptions.push(
          this.data.post(nationalitiesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.nationalityForm.reset();
            this.nationalityForm.patchValue({
              isActive: true
            });
            this.dropzone.reset();
            this.filesAdded = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let nationalityNameAr = '';
    let nationalityNameEn = '';
    let isActive = true;

    if (this.nationality) {
      image = this.nationality.image;
      code = this.nationality.code;
      nationalityNameAr = this.nationality.nationalityNameAr;
      nationalityNameEn = this.nationality.nationalityNameEn;
      isActive = this.nationality.isActive;
    }
    this.nationalityForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      nationalityNameAr: new FormControl(nationalityNameAr, Validators.required),
      nationalityNameEn: new FormControl(nationalityNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
