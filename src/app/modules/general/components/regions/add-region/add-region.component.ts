import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ICity } from '../../../interfaces/ICity.model';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { IRegion } from '../../../interfaces/IRegion.model';
import { GeneralService } from '../../../services/general.service';
import { searchLength } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { baseUrl, citiesApi, countriesApi, regionsApi } from 'src/app/common/constants/api.constants';
import { ICountry } from '../../../interfaces/ICountry.model';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-region',
  templateUrl: './add-region.component.html',
  styleUrls: ['./add-region.component.scss']
})
export class AddRegionComponent implements OnInit, OnDestroy {
  regionForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  region: IRegion;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Cities
  cities: ICity[] = [];
  citiesInputFocused: boolean;
  hasMoreCities: boolean;
  citiesCount: number;
  selectedCitiesPage = 1;
  citiesPagesNo: number;
  noCities: boolean;

  // Countries
  countries: ICountry[] = [];
  countriesInputFocused: boolean;
  hasMoreCountries: boolean;
  countriesCount: number;
  selectedCountriesPage = 1;
  countriesPagesNo: number;
  noCountries: boolean;

  constructor(
    private generalService: GeneralService,
    private data: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.data.get(citiesApi, 1).subscribe((res: IDataRes) => {
        this.citiesPagesNo = res.pages;
        this.citiesCount = res.count;
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        }
        this.cities.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(countriesApi, 1).subscribe((res: IDataRes) => {
        this.countriesPagesNo = res.pages;
        this.countriesCount = res.count;
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        }
        this.countries.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(regionsApi, null, null, id).subscribe((region: IRegion) => {
              this.region = region;
              this.subscriptions.push(
                this.data.get(citiesApi, null, null, region.cityId).subscribe((city: ICity) => {
                  if (!this.generalService.checkItem(this.cities, region.cityId)) {
                    this.cities.push(city);
                  }
                  this.uiService.isLoading.next(false);
                },
                  err => {
                    this.uiService.showErrorMessage(err);
                  })
              );
              this.subscriptions.push(
                this.data.get(countriesApi, null, null, region.countryId).subscribe((city: ICity) => {
                  if (!this.generalService.checkItem(this.countries, region.countryId)) {
                    this.countries.push(city);
                  }
                  this.uiService.isLoading.next(false);
                },
                  err => {
                    this.uiService.showErrorMessage(err);
                  })
              );
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.regionForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            },
              err => {
                this.uiService.showErrorMessage(err);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.regionForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.region.image = '';
    this.regionForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  searchCities(event) {
    const searchValue = event;
    const searchQuery = {
      cityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(citiesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCities = true;
          } else {
            this.noCities = false;
            for (const item of res.results) {
              if (this.cities.length) {
                const uniqueCities = this.cities.filter(x => x._id !== item._id);
                this.cities = uniqueCities;
              }
              this.cities.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        },
          err => {
            this.uiService.showErrorMessage(err);
          })
      );
    }
    if (searchValue.length <= 0) {
      this.noCities = false;
      this.citiesInputFocused = true;
    } else {
      this.citiesInputFocused = false;
    }
  }

  loadMoreCities() {
    this.selectedCitiesPage = this.selectedCitiesPage + 1;
    this.subscriptions.push(
      this.data.get(citiesApi, this.selectedCitiesPage).subscribe((res: IDataRes) => {
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        } else {
          this.hasMoreCities = false;
        }
        for (const item of res.results) {
          if (this.cities.length) {
            const uniqueCities = this.cities.filter(x => x._id !== item._id);
            this.cities = uniqueCities;
          }
          this.cities.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }
  searchCountries(event) {
    const searchValue = event;
    const searchQuery = {
      countryNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(countriesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCountries = true;
          } else {
            this.noCountries = false;
            for (const item of res.results) {
              if (this.countries.length) {
                const uniqueCountries = this.countries.filter(x => x._id !== item._id);
                this.countries = uniqueCountries;
              }
              this.countries.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        },
          err => {
            this.uiService.showErrorMessage(err);
          })
      );
    }
    if (searchValue.length <= 0) {
      this.noCountries = false;
      this.countriesInputFocused = true;
    } else {
      this.countriesInputFocused = false;
    }
  }

  loadMoreCountries() {
    this.selectedCountriesPage = this.selectedCountriesPage + 1;
    this.subscriptions.push(
      this.data.get(countriesApi, this.selectedCountriesPage).subscribe((res: IDataRes) => {
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        } else {
          this.hasMoreCountries = false;
        }
        for (const item of res.results) {
          if (this.countries.length) {
            const uniqueCountries = this.countries.filter(x => x._id !== item._id);
            this.countries = uniqueCountries;
          }
          this.countries.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }


  get form() {
    return this.regionForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.region) {
      if (this.regionForm.valid) {
        const newRegion = { _id: this.region._id, ...this.generalService.checkEmptyFields(this.regionForm.value) };
        this.subscriptions.push(
          this.data.put(regionsApi, newRegion).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/general/regions']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.regionForm.valid) {
        const formValue = this.generalService.checkEmptyFields(
          this.regionForm.value
        );
        this.subscriptions.push(
          this.data.post(regionsApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.regionForm.reset();
              this.regionForm.patchValue({
                isActive: true
              });
              this.dropzone.reset();
              this.filesAdded = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let cityId = '';
    let countryId = '';
    let code = '';
    let regionNameAr = '';
    let regionNameEn = '';
    let isActive = true;

    if (this.region) {
      image = this.region.image;
      cityId = this.region.cityId;
      countryId = this.region.countryId;
      code = this.region.code;
      regionNameAr = this.region.regionNameAr;
      regionNameEn = this.region.regionNameEn;
      isActive = this.region.isActive;
    }
    this.regionForm = new FormGroup({
      image: new FormControl(image),
      cityId: new FormControl(cityId, Validators.required),
      countryId: new FormControl(countryId, Validators.required),
      code: new FormControl(code, Validators.required),
      regionNameAr: new FormControl(regionNameAr, Validators.required),
      regionNameEn: new FormControl(regionNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
