import { Component, OnInit, OnDestroy } from '@angular/core';
import { IRegion } from '../../interfaces/IRegion.model';
import { FormGroup, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { GeneralService } from '../../services/general.service';
import { baseUrl, regionsApi } from 'src/app/common/constants/api.constants';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.scss']
})
export class RegionsComponent implements OnInit, OnDestroy {
  regions: IRegion[];
  subscriptions: Subscription[] = [];
  bsModalRef: BsModalRef;
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  baseUrl = baseUrl;
  sortType: string;
  sortValue: string;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.getRegionsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(regionsApi, pageNumber).subscribe((res: IDataRes) => {
      this.regions = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(regionsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.regions = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getRegionsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(regionsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.regions = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      regionNameAr: new FormControl(''),
      regionNameEn: new FormControl('')
    });
  }
  deleteModal(region: IRegion) {
    const initialState = {
      code: region.code,
      nameAr: region.regionNameAr,
      nameEn: region.regionNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(region._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(regionsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.regions = this.generalService.removeItem(this.regions, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getRegionsFirstPage() {
    this.subscriptions.push(
      this.data.get(regionsApi, 1).subscribe((res: IDataRes) => {
        this.regions = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
