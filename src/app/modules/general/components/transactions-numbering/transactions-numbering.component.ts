import { Component, OnInit, OnDestroy } from '@angular/core';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { GeneralService } from '../../services/general.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { companyId } from 'src/app/common/constants/general.constants';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { TransactionsNumberingService } from '../../services/transactions-numbering.service';
import { IFinancialYear } from '../../interfaces/IFinancialYear';
import { fiscalYearsApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-transactions-numbering',
  templateUrl: './transactions-numbering.component.html',
  styleUrls: ['./transactions-numbering.component.scss']
})
export class TransactionsNumberingComponent implements OnInit, OnDestroy {
  transactionsNumberingForm: FormGroup;
  transactionsNumbering: any;
  modules: any[] = [];
  manualMode = true;
  accountingPeriodsOn: boolean;
  loadingButton: boolean;
  companyId = companyId;
  subscriptions: Subscription[] = [];
  financialYears: IFinancialYear[];

  constructor(
    private transactionsNumberingService: TransactionsNumberingService,
    private generalService: GeneralService,
    private data: DataService,
    private uiService: UiService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.transactionsNumberingService.getTransactionsNumbering().subscribe((res: IDataRes) => {
        this.transactionsNumbering = res.results[0];
        this.modules = this.generalService.getObjects(this.transactionsNumbering).array;

        const componentsNames = [];
        this.modules.map(item => {
          item.components = this.generalService.getObjects(item).array;
          const names = this.generalService.getObjects(item).objectKeys;
          names.pop();
          componentsNames.push(names);
        });
        this.initForm();
        const modulesNames = this.generalService.getObjects(this.transactionsNumbering).objectKeys;

        for (let i = 0; i <= modulesNames.length - 1; i++) {
          this.transactionsNumberingForm.setControl(modulesNames[i], new FormGroup({}));
          this.modules[i].moduleName = modulesNames[i];
          for (let k = 0; k <= componentsNames[i].length - 1; k++) {
            (this.transactionsNumberingForm.get(modulesNames[i]) as FormGroup).setControl(componentsNames[i][k], new FormGroup({
              numberingSystem: new FormControl('manual'),
              numberingType: new FormControl('connected'),
              startNumber: new FormControl(0),
            }));
            this.modules[i].components[k].componentName = componentsNames[i][k];

          }
        }
        console.log(this.transactionsNumbering);

        this.uiService.isLoading.next(false);
      })
    );

  }

  checkType(moduleName, componentName, value) {
    const componentGroup = this.transactionsNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (value === 'manual') {
      this.manualMode = true;
      componentGroup.patchValue({
        startNumber: 0
      });
      componentGroup.controls.startNumber.clearValidators();
      componentGroup.controls.startNumber.updateValueAndValidity();
    } else {
      this.manualMode = false;
      componentGroup.patchValue({
        startNumber: 1
      });
      componentGroup.controls.startNumber.setValidators([Validators.required, CustomValidators.number, CustomValidators.notEqual(0)]);
      componentGroup.controls.startNumber.updateValueAndValidity();
    }
  }

  checkManualMode(moduleName, componentName) {
    const componentGroup = this.transactionsNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (componentGroup.controls.numberingSystem.value === 'manual') {
      return true;
    } else {
      return false;
    }
  }
  showFiscalYears(moduleName, componentName) {
    const componentGroup = this.transactionsNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (componentGroup.controls.numberingType.value !== 'connected' && componentGroup.controls.numberingSystem.value !== 'manual') {
      return true;
    } else {
      return false;
    }
  }

  checkValidation(moduleName, componentName, validationName) {
    const componentGroup = this.transactionsNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (componentGroup.controls.startNumber.errors && componentGroup.controls.numberingSystem.value !== 'manual') {
      if (componentGroup.controls.startNumber.errors[validationName]) {
        return true;
      } else {
        return false;
      }
    }
  }

  checkInvalidFeedback(moduleName, componentName) {
    const componentGroup = this.transactionsNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (componentGroup.controls.startNumber.errors && componentGroup.controls.numberingSystem.value !== 'manual') {
      return true;
    } else {
      return false;
    }
  }

  numberingType(moduleName, componentName, value) {
    if (value !== 'connected') {
      if (value === 'financial_year') {
        this.accountingPeriodsOn = false;
        const componentGroup = this.transactionsNumberingForm.get(moduleName).get(componentName) as  FormGroup;
        componentGroup.setControl('fiscalYear', new FormGroup({
          code: new FormControl()
        }));
      } else {
        this.accountingPeriodsOn = true;
      }
      const search = {
        companyId,
        fiscalYearStatus: 'opened'
      };
      this.subscriptions.push(
        this.data.get(fiscalYearsApi, null, search).subscribe((res: IDataRes) => {
          this.financialYears = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  checkNumberingType(moduleName, componentName) {
    const componentGroup = this.transactionsNumberingForm.get(moduleName).get(componentName) as FormGroup;
    if (componentGroup.controls.numberingType.value !== 'connected') {
      return false;
    } else {
      return true;
    }
  }

  submit() {
    console.log(this.transactionsNumberingForm.value);

    this.loadingButton = true;
    const formValue = {
      ...this.transactionsNumberingForm.value,
      companyId: this.companyId
    };
    this.subscriptions.push(
      this.transactionsNumberingService.saveTransactionsNumbering(formValue).subscribe(res => {
        console.log(res);
        this.uiService.isLoading.next(false);
      }, err => {
        this.loadingButton = false;
        this.uiService.isLoading.next(false);
        if (this.translate.currentLang === 'en') {
          this.uiService.showError(err.error.error.en, '');
        } else {
          this.uiService.showError(err.error.error.ar, '');
        }
      })
    );
  }

  get form() {
    return this.transactionsNumberingForm.controls;
  }

  private initForm() {
    this.transactionsNumberingForm = new FormGroup({});
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
