import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from '../../../services/general.service';
import { IUser } from '../../../interfaces/IUser.model';
import { CustomValidators } from 'ng2-validation';
import { ICompany } from '../../../interfaces/ICompany';
import { searchLength } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, Params, RouterStateSnapshot } from '@angular/router';
import { baseUrl, usersApi, companiesApi } from 'src/app/common/constants/api.constants';
import { IBranch } from '../../../interfaces/IBranch';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit, OnDestroy {
  userForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  user: IUser;
  filesAdded: boolean;
  companyBranches: IBranch[] = [];
  subscriptions: Subscription[] = [];
  formReady: boolean;
  baseUrl = baseUrl;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Companies
  companies: any[] = [];
  companiesInputFocused: boolean;
  hasMoreCompanies: boolean;
  companiesCount: number;
  selectedCompaniesPage = 1;
  companiesPagesNo: number;
  noCompanies: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private localeService: BsLocaleService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(usersApi, null, null, id).subscribe(
              (user: IUser) => {
                this.user = user;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.userForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              },
              err => {
                this.uiService.showErrorMessage(err);
              }
            )
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(companiesApi, 1).subscribe((res: IDataRes) => {
        this.companiesPagesNo = res.pages;
        this.companiesCount = res.count;
        if (this.companiesPagesNo > this.selectedCompaniesPage) {
          this.hasMoreCompanies = true;
        }
        this.companies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    const userLang = localStorage.getItem('userLang');
    this.localeService.use(userLang);
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.userForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.user.image = '';
    this.userForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  searchCompanies(event) {
    const searchValue = event;
    const searchQuery = {
      companyNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.data
        .get(companiesApi, null, searchQuery)
        .subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCompanies = true;
          } else {
            this.noCompanies = false;
            for (const item of res.results) {
              if (this.companies.length) {
                const uniqueCompany = this.companies.filter(
                  x => x._id !== item._id
                );
                this.companies = uniqueCompany;
              }
              this.companies.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        });
    }
  }

  loadMoreCompanies() {
    this.selectedCompaniesPage = this.selectedCompaniesPage + 1;
    this.subscriptions.push(
      this.data
        .get(companiesApi, this.selectedCompaniesPage)
        .subscribe((res: IDataRes) => {
          if (this.companiesPagesNo > this.selectedCompaniesPage) {
            this.hasMoreCompanies = true;
          } else {
            this.hasMoreCompanies = false;
          }
          for (const item of res.results) {
            if (this.companies.length) {
              const uniqueCompanies = this.companies.filter(
                x => x._id !== item._id
              );
              this.companies = uniqueCompanies;
            }
            this.companies.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  fillBranches(event) {
    if (event) {
      const company = this.companies.find(item => item._id === event);
      if (company) {
        return company.branches;
      }
    }
  }

  get form() {
    return this.userForm.controls;
  }

  get userCompanies() {
    return this.userForm.get('userCompanies') as FormArray;
  }

  addUserCompany() {
    const control = this.userForm.get('userCompanies') as FormArray;
    control.push(
      new FormGroup({
        companyId: new FormControl('', Validators.required),
        branches: new FormArray([
          new FormGroup({
            branchId: new FormControl('', Validators.required)
          })
        ])
      })
    );
  }
  deleteUserCompany(index) {
    const control = this.userForm.get('userCompanies') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBranch(control) {
    control.push(
      new FormGroup({
        branchId: new FormControl('', Validators.required)
      })
    );
  }
  deleteBranch(control, index) {
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.user) {
      this.userForm.controls.password.clearValidators();
      this.userForm.controls.password.updateValueAndValidity();
      if (this.userForm.valid) {
        delete this.userForm.value.password;
        delete this.userForm.value.confirmPassword;
        const newUser = {
          _id: this.user._id,
          ...this.generalService.checkEmptyFields(this.userForm.value)
        };
        this.data.put(usersApi, newUser).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/general/users']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      }
    } else {
      if (this.userForm.valid) {
        const formValue = this.generalService.checkEmptyFields(
          this.userForm.value
        );
        this.subscriptions.push(
          this.data.post(usersApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.userForm.reset();
              this.userForm.patchValue({
                isActive: true
              });
              this.dropzone.reset();
              this.filesAdded = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let userName = '';
    const password = '';
    const confirmPassword = '';
    let image = '';
    let fullName = '';
    let userNotes = '';
    let mobile: number;
    let email = '';
    let isActive = true;
    let branchesArray = new FormArray([
      new FormGroup({
        branchId: new FormControl('', Validators.required)
      })
    ]);
    let userCompaniesArray = new FormArray([
      new FormGroup({
        companyId: new FormControl('', Validators.required),
        branches: branchesArray
      })
    ]);

    const passwordControl = new FormControl(password, [
      Validators.required,
      Validators.minLength(6)
    ]);
    const confirmPasswordControl = new FormControl(
      confirmPassword,
      CustomValidators.equalTo(passwordControl)
    );

    if (this.user) {
      userName = this.user.userName;
      email = this.user.email;
      mobile = this.user.mobile;
      image = this.user.image;
      fullName = this.user.fullName;
      isActive = this.user.isActive;
      userNotes = this.user.userNotes;
      userCompaniesArray = new FormArray([]);
      for (const control of this.user.userCompanies) {
        branchesArray = new FormArray([]);
        for (const branch of control.branches) {
          branchesArray.push(
            new FormGroup({
              branchId: new FormControl(branch.branchId, Validators.required)
            })
          );
        }
        userCompaniesArray.push(
          new FormGroup({
            companyId: new FormControl(control.companyId, Validators.required),
            branches: branchesArray
          })
        );
      }
    }
    this.userForm = new FormGroup({
      userName: new FormControl(userName, Validators.required),
      email: new FormControl(email, [Validators.required, Validators.email]),
      password: passwordControl,
      confirmPassword: confirmPasswordControl,
      mobile: new FormControl(mobile, CustomValidators.number),
      image: new FormControl(image),
      fullName: new FormControl(fullName),
      userNotes: new FormControl(userNotes),
      isActive: new FormControl(isActive),
      userCompanies: userCompaniesArray
    });
  }
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
