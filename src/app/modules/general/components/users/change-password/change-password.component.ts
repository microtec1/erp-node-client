import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthService } from 'src/app/common/services/auth/auth.service';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  changePasswordForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  constructor(
    private authervice: AuthService,
    private translate: TranslateService,
    private uiService: UiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initForm();
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  get form() {
    return this.changePasswordForm.controls;
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.changePasswordForm.valid) {
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.subscriptions.push(
        this.authervice.changePassword(this.changePasswordForm.value, currentUser.id).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/home']);
        },
          err => {
            this.loadingButton = false;
            this.uiService.isLoading.next(false);
            if (this.translate.currentLang === 'en') {
              this.uiService.showError(err.error.error.en, '');
            } else {
              this.uiService.showError(err.error.error.ar, '');
            }
          })
      );
    }
  }

  private initForm() {
    const passwordControl = new FormControl('', Validators.required);
    const confirmPasswordControl = new FormControl('', CustomValidators.equalTo(passwordControl));
    this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl('', Validators.required),
      newPassword: passwordControl,
      confirmNewPassword: confirmPasswordControl,
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
