import { Component, OnInit, OnDestroy } from '@angular/core';
import { IUser } from '../../../interfaces/IUser.model';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { usersApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  user: IUser;
  subscriptions: Subscription[] = [];
  constructor(
    private data: DataService,
    private uiService: UiService
    ) { }

  ngOnInit() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.subscriptions.push(
      this.data.get(usersApi, null, null, currentUser.id).subscribe((user: IUser) => {
        this.user = user;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
