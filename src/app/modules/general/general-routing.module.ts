import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CurrenciesComponent } from './components/currencies/currencies.component';
import { CitiesComponent } from './components/cities/cities.component';
import { CountriesComponent } from './components/countries/countries.component';
import { RegionsComponent } from './components/regions/regions.component';
import { NationalitiesComponent } from './components/nationalities/nationalities.component';
import { AttachmentTypesComponent } from './components/attachmentTypes/attachment-types.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { CompaniesComponent } from './components/companies/companies.component';
import { FinancialYearsComponent } from './components/financial-years/financial-years.component';
import { UsersComponent } from './components/users/users.component';
import { AddCountryComponent } from './components/countries/add-country/add-country.component';
import { AddCityComponent } from './components/cities/add-city/add-city.component';
import { AddRegionComponent } from './components/regions/add-region/add-region.component';
import { AddCurrencyComponent } from './components/currencies/add-currency/add-currency.component';
import { AddNationalityComponent } from './components/nationalities/add-nationality/add-nationality.component';
import { AddUserComponent } from './components/users/add-user/add-user.component';
import { AddEmployeeComponent } from './components/employees/add-employee/add-employee.component';
import { AddAttachmentTypeComponent } from './components/attachmentTypes/add-attachment-type/add-attachment-type.component';
import { AddCompanyComponent } from './components/companies/add-company/add-company.component';
import { AddFinancialYearComponent } from './components/financial-years/add-financial-year/add-financial-year.component';
import { UserProfileComponent } from './components/users/user-profile/user-profile.component';
import { ChangePasswordComponent } from './components/users/change-password/change-password.component';
import { FilesNumberingComponent } from './components/files-numbering/files-numbering.component';
import { TransactionsNumberingComponent } from './components/transactions-numbering/transactions-numbering.component';


const routes: Routes = [
  { path: '', redirectTo: 'currencies', pathMatch: 'full' },
  { path: 'countries', component: CountriesComponent },
  { path: 'countries/add', component: AddCountryComponent },
  { path: 'countries/edit/:id', component: AddCountryComponent },
  { path: 'countries/details/:id', component: AddCountryComponent },
  { path: 'cities', component: CitiesComponent },
  { path: 'cities/add', component: AddCityComponent },
  { path: 'cities/edit/:id', component: AddCityComponent },
  { path: 'cities/details/:id', component: AddCityComponent },
  { path: 'regions', component: RegionsComponent },
  { path: 'regions/add', component: AddRegionComponent },
  { path: 'regions/edit/:id', component: AddRegionComponent },
  { path: 'regions/details/:id', component: AddRegionComponent },
  { path: 'currencies', component: CurrenciesComponent },
  { path: 'currencies/add', component: AddCurrencyComponent },
  { path: 'currencies/edit/:id', component: AddCurrencyComponent },
  { path: 'currencies/details/:id', component: AddCurrencyComponent },
  { path: 'nationalities', component: NationalitiesComponent },
  { path: 'nationalities/add', component: AddNationalityComponent },
  { path: 'nationalities/edit/:id', component: AddNationalityComponent },
  { path: 'nationalities/details/:id', component: AddNationalityComponent },
  { path: 'users', component: UsersComponent },
  { path: 'users/add', component: AddUserComponent },
  { path: 'users/edit/:id', component: AddUserComponent },
  { path: 'users/details/:id', component: AddUserComponent },
  { path: 'users/user/profile', component: UserProfileComponent },
  { path: 'users/user/changePassword', component: ChangePasswordComponent },
  { path: 'employees', component: EmployeesComponent },
  { path: 'employees/add', component: AddEmployeeComponent },
  { path: 'employees/edit/:id', component: AddEmployeeComponent },
  { path: 'employees/details/:id', component: AddEmployeeComponent },
  { path: 'attachmentTypes', component: AttachmentTypesComponent },
  { path: 'attachmentTypes/add', component: AddAttachmentTypeComponent },
  { path: 'attachmentTypes/edit/:id', component: AddAttachmentTypeComponent },
  { path: 'attachmentTypes/details/:id', component: AddAttachmentTypeComponent },
  { path: 'companies', component: CompaniesComponent },
  { path: 'companies/add', component: AddCompanyComponent },
  { path: 'companies/edit/:id', component: AddCompanyComponent },
  { path: 'companies/details/:id', component: AddCompanyComponent },
  { path: 'financialYears', component: FinancialYearsComponent },
  { path: 'financialYears/add', component: AddFinancialYearComponent },
  { path: 'financialYears/edit/:id', component: AddFinancialYearComponent },
  { path: 'financialYears/details/:id', component: AddFinancialYearComponent },
  { path: 'filesNumbering', component: FilesNumberingComponent },
  { path: 'transactionsNumbering', component: TransactionsNumberingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeneralRoutingModule { }
