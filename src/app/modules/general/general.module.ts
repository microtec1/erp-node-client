import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { GeneralRoutingModule } from './general-routing.module';
import { CurrenciesComponent } from './components/currencies/currencies.component';
import { CitiesComponent } from './components/cities/cities.component';
import { CountriesComponent } from './components/countries/countries.component';
import { AddCountryComponent } from './components/countries/add-country/add-country.component';
import { AddCityComponent } from './components/cities/add-city/add-city.component';
import { RegionsComponent } from './components/regions/regions.component';
import { AddRegionComponent } from './components/regions/add-region/add-region.component';
import { AddNationalityComponent } from './components/nationalities/add-nationality/add-nationality.component';
import { NationalitiesComponent } from './components/nationalities/nationalities.component';
import { AddCurrencyComponent } from './components/currencies/add-currency/add-currency.component';
import { AttachmentTypesComponent } from './components/attachmentTypes/attachment-types.component';
import { AddAttachmentTypeComponent } from './components/attachmentTypes/add-attachment-type/add-attachment-type.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { AddEmployeeComponent } from './components/employees/add-employee/add-employee.component';
import { CompaniesComponent } from './components/companies/companies.component';
import { AddCompanyComponent } from './components/companies/add-company/add-company.component';
import { FinancialYearsComponent } from './components/financial-years/financial-years.component';
import { AddFinancialYearComponent } from './components/financial-years/add-financial-year/add-financial-year.component';
import { UsersComponent } from './components/users/users.component';
import { AddUserComponent } from './components/users/add-user/add-user.component';
import { ChangePasswordComponent } from './components/users/change-password/change-password.component';
import { UserProfileComponent } from './components/users/user-profile/user-profile.component';
import { FilesNumberingComponent } from './components/files-numbering/files-numbering.component';
import { TransactionsNumberingComponent } from './components/transactions-numbering/transactions-numbering.component';

@NgModule({
  declarations: [
    CitiesComponent,
    CountriesComponent,
    AddCountryComponent,
    AddCityComponent,
    RegionsComponent,
    AddRegionComponent,
    NationalitiesComponent,
    AddNationalityComponent,
    CurrenciesComponent,
    AddCurrencyComponent,
    AttachmentTypesComponent,
    AddAttachmentTypeComponent,
    EmployeesComponent,
    AddEmployeeComponent,
    CompaniesComponent,
    AddCompanyComponent,
    FinancialYearsComponent,
    AddFinancialYearComponent,
    UsersComponent,
    AddUserComponent,
    ChangePasswordComponent,
    UserProfileComponent,
    FilesNumberingComponent,
    TransactionsNumberingComponent,
     
  ],
  imports: [SharedModule, GeneralRoutingModule]
})
export class GeneralModule { }
