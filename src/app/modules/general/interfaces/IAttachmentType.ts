export interface IAttachmentType {
  _id: string;
  image: string;
  code: string;
  attachmentTypeLogo: string;
  attachmentTypeNameAr: string;
  attachmentTypeNameEn?: string;
  isActive: boolean;
}
