export interface IBranch {
  regionId: string;
  cityId: string;
  cityName?: string;
  regionName?: string;
  branchCurrency: [
    {
      currencyId: string;
      currencyNameAr?: string;
      exchangeRate: number;
      isDefault: boolean;
    }
  ];
  BranchResponsible: [
    {
      responsibleName: string;
      responsibleJob: string;
      responsiblePhone: string;
      responsibleInternalPhone: string;
      responsibleMobile: string;
      responsibleEmail: string;
    }
  ];
  _id: string;
  code: string;
  branchNameAr: string;
  branchNameEn?: string;
  branchNotice?: string;
  branchAddress?: string;
  branchPhone: string;
  branchMobile: string;
  branchEmail?: string;
}
