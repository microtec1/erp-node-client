export interface ICity {
  _id: string;
  image: string;
  countryId: string;
  countryNameAr: string;
  code: string;
  cityNameAr: string;
  cityNameEn?: string;
  isActive: boolean;
}
