import { IBranch } from './IBranch';

export interface ICompany {
  _id: string;
  image: string;
  calendarType: string;
  code: string;
  companyNameAr: string;
  companyNameEn: string;
  companyActivity: string;
  taxNumber: string;
  companyCurrency: [
    {
      currencyId: string,
      currencyNameAr?: string;
      exchangeRate: number,
      isDefault: boolean
    }
  ];
  branches: IBranch[];
  isActive: boolean;
}
