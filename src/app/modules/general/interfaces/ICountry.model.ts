export interface ICountry {
  _id: string;
  image: string;
  code: string;
  countryNameAr: string;
  countryNameEn?: string;
  isActive: boolean;
}
