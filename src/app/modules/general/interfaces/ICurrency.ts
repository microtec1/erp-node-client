export interface ICurrency {
  _id: string;
  image: string;
  code: string;
  currencyLogo: string;
  currencyNameAr: string;
  currencyNameEn?: string;
  currencyDescriptionAr?: string;
  currencyDescriptionEn?: string;
  isActive: boolean;
}
