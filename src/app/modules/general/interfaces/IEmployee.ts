export interface IEmployee {
  _id: string;
  image: string;
  code: string;
  employeeNameAr: string;
  employeeNameEn?: string;
  gender?: string;
  qualification?: string;
  salesRepresentative: boolean;
  accountId?: string;
  regionId?: string;
  cityId?: string;
  employeeAddress?: string;
  employeeNationalId: number;
  employeePhone: number;
  employeeMobile: number;
  employeeWhatsApp?: string;
  employeeEmail?: string;
  employeeFacebook?: string;
  employeeTwitter?: string;
  employeeNote?: string;
  isActive: boolean;
  files: [
    {
      attachmentTypeId: string;
      attachmentTypeNameAr: string;
      file: string;
    }
  ];
}
