export interface IFileName {
  _id: string;
  code: string;
  fileNameDescriptionAr?: string;
  fileNameDescriptionEn?: string;
  isActive: boolean;
}
