export interface IFinancialYear {
  _id: string;
  image: string;
  fiscalYearStartDateGregorian: string;
  fiscalYearStartDateHijri: string;
  fiscalYearEndDateGregorian: string;
  fiscalYearEndDateHijri: string;
  fiscalYearStatus: string;
  code: string;
  fiscalYearName: string;
  fiscalYearPeriodsCount: string;
  isActive: boolean;
  accountingPeriods: [
    {
      periodStartDateGregorian: string;
      periodStartDateHijri: string;
      periodEndDateGregorian: string;
      periodEndDateHijri: string;
      periodStatus: string;
      periodCode: string;
    }
  ];
}



