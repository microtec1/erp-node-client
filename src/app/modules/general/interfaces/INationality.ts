export interface INationality {
  _id: string;
  image: string;
  code: string;
  nationalityNameAr: string;
  nationalityNameEn?: string;
  isActive: boolean;
}
