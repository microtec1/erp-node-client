export interface IRegion {
  _id: string;
  image: string;
  cityId: string;
  countryId: string;
  countryNameAr: string;
  cityNameAr: string;
  code: string;
  regionNameAr: string;
  regionNameEn?: string;
  isActive: boolean;
}
