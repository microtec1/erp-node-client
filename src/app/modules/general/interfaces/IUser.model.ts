export interface IUser {
  _id: string;
  image: string;
  userName: string;
  email: string;
  isActive: boolean;
  password: string;
  confirmPassword: string;
  mobile?: number;
  fullName?: string;
  userNotes?: string;
  userCompanies: [
    {
      companyId: string;
      companyNameAr: string;
      branches: [
        {
          branchId: string
          branchNameAr: string
        }
      ]
    }
  ];
}
