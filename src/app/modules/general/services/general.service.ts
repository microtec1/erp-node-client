import { Injectable } from '@angular/core';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment-hijri';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  constructor(
    private translate: TranslateService
  ) {

  }

  // Find Index of Item to Update
  findIndexToUpdate(item) {
    return item._id === this;
  }

  // Get Translation
  getTranslation(location, value) {
    const word = `${location}.${value}`;
    let translation;
    this.translate.get(word).subscribe(res => {
      translation = res;
    });
    return translation;
  }

  hasTranslation(location: string, key: string) {
    const translation = this.getTranslation(location, key)
    if (translation === `${location}.${key}`) {
      return key;
    } else {
      return translation;
    }
  }

  // Check Empty Fields
  checkEmptyFields(form) {
    const formValue = { ...form };
    const keys = Object.keys(formValue);
    for (let i = 0; i <= keys.length; i++) {
      if (this.isObject(formValue[keys[i]]) && !this.isEmpty(formValue[keys[i]])) {
        const item = formValue[keys[i]];
        const itemKeys = Object.keys(item);
        for (let k = 0; k <= itemKeys.length; k++) {
          if (item[itemKeys[k]] === null || item[itemKeys[k]] === '') {
            delete item[itemKeys[k]];
          }
          if (item[itemKeys[k]] instanceof Array) {
            item[itemKeys[k]].map(innerItem => {
              const innerItemKeys = Object.keys(innerItem);
              for (let j = 0; j <= innerItemKeys.length; j++) {
                if (innerItem[innerItemKeys[j]] === null || innerItem[innerItemKeys[j]] === '') {
                  delete innerItem[innerItemKeys[j]];
                }
              }
            });
          }
          if (this.isObject(item[itemKeys[k]]) && !this.isEmpty(item[itemKeys[k]])) {
            const item2 = item[itemKeys[k]];
            const itemKeys2 = Object.keys(item2);

            for (let j = 0; j <= itemKeys2.length; j++) {
              if (item2[itemKeys2[j]] === null || item2[itemKeys2[j]] === '') {
                delete item2[itemKeys2[j]];
              }
            }
          }
          if (this.isObject(item[itemKeys[k]]) && this.isEmpty(item[itemKeys[k]])) {
            delete item[itemKeys[k]];
          }
        }
      }
      if (formValue[keys[i]] instanceof Array) {
        formValue[keys[i]].map(item => {
          const itemKeys = Object.keys(item);
          for (let k = 0; k <= itemKeys.length; k++) {
            if (item[itemKeys[k]] === null || item[itemKeys[k]] === '') {
              delete item[itemKeys[k]];
            }
            if (this.isObject(item[itemKeys[k]]) && this.isEmpty(item[itemKeys[k]])) {
              delete item[itemKeys[k]];
            }
            if (item[itemKeys[k]] instanceof Array) {
              item[itemKeys[k]].map(innerItem => {
                const innerItemKeys = Object.keys(innerItem);
                for (let j = 0; j <= innerItemKeys.length; j++) {
                  if (innerItem[innerItemKeys[j]] === null || innerItem[innerItemKeys[j]] === '') {
                    delete innerItem[innerItemKeys[j]];
                  }
                }
              });
            }
          }
        });
      }
      if (this.isObject(formValue[keys[i]]) && this.isEmpty(formValue[keys[i]])) {
        delete formValue[keys[i]];
      }
      if (formValue[keys[i]] === null || formValue[keys[i]] === '') {
        delete formValue[keys[i]];
      }
    }
    return formValue;
  }

  // Check Type of Object
  isObject(value) {
    return value && typeof value === 'object' && value.constructor === Object;
  }


  // Check Empty Object
  isEmpty(obj) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      } else {
      }
    }
    return true;
  }

  // Remove Dublicates
  removeDuplicates(array, key) {
    const filtered = [];
    array.forEach(element => {
      if (filtered.length) {
        filtered.map(x => {
          if (x[key] !== element[key]) {
            filtered.push(element);
          }
        });
      } else {
        filtered.push(element);
      }
    });
    return filtered;
  }

  // Format Date
  format(date: Date) {
    const d = date.getDate();
    const m = date.getMonth() + 1;
    const y = date.getFullYear();
    return y + '/' + (m <= 9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d);
  }

  // Date Convertion
  convertToGregorian(date: NgbDate) {
    const year = date.year;
    const month = date.month;
    const day = date.day;
    const gregorianDate = moment(year + '/' + month + '/' + day, 'iYYYY/iMM/iDD');
    gregorianDate.format('YYYY/MM/DD');
    return gregorianDate;
  }

  convertToHijri(date: Date) {
    const hijriDate = moment(date.toLocaleDateString(), 'MM/D/YYYY');
    hijriDate.format('iYYYY/iMM/iDD');
    return hijriDate;
  }

  convertToHijriWithFormat(date: Date) {
    const d = date.getDate();
    const m = date.getMonth() + 1;
    const y = date.getFullYear();
    moment.locale('en');
    const hijriDate = moment(m + '/' + d + '/' + y, 'MM/DD/YYYY');
    return hijriDate.format('iYYYY/iMM/iDD');
  }

  formatDate(date: string) {
    const dateFormat = new Date(date);
    moment.locale('en');
    const hijriDate = moment(dateFormat);
    return hijriDate.format('YYYY/MM/DD');
  }

  formatHijriDate(hijriDate) {
    const date = hijriDate.year + '/' + hijriDate.month + '/' + hijriDate.day;
    moment.locale('en');
    const hijri = moment(date, 'iYYYY/iM/iD');
    return hijri.format('iYYYY/iMM/iDD');
  }

  // Get Only Objects
  getObjects(obj) {
    const values = { ...obj };
    const keys = Object.keys(values);
    const objectKeys = [];
    const array = [];
    for (let i = 0; i <= keys.length; i++) {
      if (typeof values[keys[i]] === ('object')) {
        objectKeys.push(keys[i]);
        array.push(values[keys[i]]);
      }
    }
    return {
      array,
      objectKeys
    };
  }

  // Delete Item From Array
  removeItem(array, id) {
    const target = array.find(item => item._id === id);
    const index = array.indexOf(target);
    array.splice(index, 1);
    return array;
  }

  // Get Index by ID
  getIndex(array, id, fieldName?) {
    const target = array.find(item => item[fieldName] === id);
    const index = array.indexOf(target);
    return index;
  }

  // Changing Status of item in Array
  changeStatus(array, id, newStatus, oldStatus?) {
    const target = array.find(item => item._id === id);
    const index = array.indexOf(target);
    if (oldStatus) {
      array[index][oldStatus] = newStatus;
    } else {
      array[index].transactionStatus = newStatus;
    }
    return array;
  }

  // Check on Item
  checkItem(array, id) {
    const target = array.find(item => item._id === id);
    if (target) {
      return true;
    } else {
      return false;
    }
  }

}
