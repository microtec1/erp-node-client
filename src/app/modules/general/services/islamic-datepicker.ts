import { Injectable } from '@angular/core';
import {
  NgbDateStruct, NgbDatepickerI18n
} from '@ng-bootstrap/ng-bootstrap';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

let WEEKDAYS = ['ن', 'ث', 'ر', 'خ', 'ج', 'س', 'ح'];
let MONTHS = ['محرم', 'صفر', 'ربيع الأول', 'ربيع الآخر', 'جمادى الأولى', 'جمادى الآخرة', 'رجب', 'شعبان', 'رمضان', 'شوال',
  'ذو القعدة', 'ذو الحجة'];

@Injectable({
  providedIn: 'root'
})
export class IslamicI18n extends NgbDatepickerI18n {

  constructor(private translate: TranslateService) {
    super();
    if (translate.currentLang === 'en') {
      WEEKDAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
      MONTHS = ['Muḥarram',
        'Ṣafar', 'Rabīʿ al-Awwal',
        'Rabī’ al-Ākhir',
        'Jumādá al-Ūlá', 'Jumādá al-Ākhirah', 'Rajab', 'Sha‘bān', 'Ramaḍān', 'Shawwāl',
        'Dhū al-Qa‘dah', 'Dhū al-Ḥijjah'];
    }
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      if (event.lang === 'en') {
        WEEKDAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        MONTHS = ['Muḥarram',
          'Ṣafar', 'Rabīʿ al-Awwal',
          'Rabī’ al-Ākhir',
          'Jumādá al-Ūlá', 'Jumādá al-Ākhirah', 'Rajab', 'Sha‘bān', 'Ramaḍān', 'Shawwāl',
          'Dhū al-Qa‘dah', 'Dhū al-Ḥijjah'];
      }
    });
  }

  getWeekdayShortName(weekday: number) {
    return WEEKDAYS[weekday - 1];
  }

  getMonthShortName(month: number) {
    return MONTHS[month - 1];
  }

  getMonthFullName(month: number) {
    return MONTHS[month - 1];
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}
