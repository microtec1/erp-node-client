import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl } from 'src/app/common/constants/api.constants';
import { companyId } from 'src/app/common/constants/general.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionsNumberingService {

  constructor(
    private http: HttpClient,
    private uiService: UiService,
  ) { }

  getTransactionsNumbering() {
    this.uiService.isLoading.next(true);
    const search = {
      companyId
    };
    return this.http.post(apiUrl + 'transactionsNumbering/search', search);
  }

  saveTransactionsNumbering(value) {
    this.uiService.isLoading.next(true);
    return this.http.post(apiUrl + 'transactionsNumbering', value);
  }
}
