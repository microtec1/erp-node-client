import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  view: any[] = [600, 200];

  // options for the chart
  showXAxis = false;
  showYAxis = false;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = 'Country';
  showYAxisLabel = false;
  yAxisLabel = 'Sales';
  timeline = false;

  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  // pie
  showLabels = true;

  // data goes here
  public single = [
    {
      name: 'China',
      value: 2243772
    },
    {
      name: 'USA',
      value: 1126000
    },
    {
      name: 'Norway',
      value: 296215
    },
    {
      name: 'Japan',
      value: 257363
    },
    {
      name: 'Germany',
      value: 196750
    },
    {
      name: 'France',
      value: 204617
    }
  ];

  public multi = [
    {
      name: 'Cyprus',
      series: [
        {
          value: 5668,
          name: '2016-09-22T14:32:49.909Z'
        },
        {
          value: 5179,
          name: '2016-09-23T00:49:36.714Z'
        },
        {
          value: 3618,
          name: '2016-09-23T01:46:55.241Z'
        },
        {
          value: 5690,
          name: '2016-09-13T11:57:54.305Z'
        },
        {
          value: 5335,
          name: '2016-09-24T00:35:16.252Z'
        }
      ]
    },
    {
      name: 'Moldova',
      series: [
        {
          value: 2139,
          name: '2016-09-22T14:32:49.909Z'
        },
        {
          value: 2597,
          name: '2016-09-23T00:49:36.714Z'
        },
        {
          value: 5184,
          name: '2016-09-23T01:46:55.241Z'
        },
        {
          value: 3460,
          name: '2016-09-13T11:57:54.305Z'
        },
        {
          value: 6398,
          name: '2016-09-24T00:35:16.252Z'
        }
      ]
    },
    {
      name: 'Romania',
      series: [
        {
          value: 6218,
          name: '2016-09-22T14:32:49.909Z'
        },
        {
          value: 4826,
          name: '2016-09-23T00:49:36.714Z'
        },
        {
          value: 4173,
          name: '2016-09-23T01:46:55.241Z'
        },
        {
          value: 3057,
          name: '2016-09-13T11:57:54.305Z'
        },
        {
          value: 3253,
          name: '2016-09-24T00:35:16.252Z'
        }
      ]
    },
    {
      name: 'Ireland',
      series: [
        {
          value: 6551,
          name: '2016-09-22T14:32:49.909Z'
        },
        {
          value: 5505,
          name: '2016-09-23T00:49:36.714Z'
        },
        {
          value: 5621,
          name: '2016-09-23T01:46:55.241Z'
        },
        {
          value: 6682,
          name: '2016-09-13T11:57:54.305Z'
        },
        {
          value: 3520,
          name: '2016-09-24T00:35:16.252Z'
        }
      ]
    },
    {
      name: 'Lithuania',
      series: [
        {
          value: 4007,
          name: '2016-09-22T14:32:49.909Z'
        },
        {
          value: 4140,
          name: '2016-09-23T00:49:36.714Z'
        },
        {
          value: 6056,
          name: '2016-09-23T01:46:55.241Z'
        },
        {
          value: 2631,
          name: '2016-09-13T11:57:54.305Z'
        },
        {
          value: 5413,
          name: '2016-09-24T00:35:16.252Z'
        }
      ]
    }
  ];
  constructor() { }

  ngOnInit() {
  }
}
