import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { HomeRoutingModule } from './home-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';

@NgModule({
  imports: [SharedModule, HomeRoutingModule, NgxChartsModule],
  declarations: [DashboardComponent]
})
export class HomeModule {}
