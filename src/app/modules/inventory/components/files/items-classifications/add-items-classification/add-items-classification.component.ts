import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, itemsClassificationsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IItemsClassification } from '../../../../interfaces/IItemsClassification';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-items-classification',
  templateUrl: './add-items-classification.component.html',
  styleUrls: ['./add-items-classification.component.scss']
})

export class AddItemsClassificationComponent implements OnInit, OnDestroy {
  itemsClassificationForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  itemsClassification: IItemsClassification;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(itemsClassificationsApi, null, null, id)
              .subscribe((itemsClassification: IItemsClassification) => {
                this.itemsClassification = itemsClassification;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.itemsClassificationForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.itemsClassificationForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.itemsClassification.image = '';
    this.itemsClassificationForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.itemsClassificationForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.itemsClassification) {
      if (this.itemsClassificationForm.valid) {
        const newitemsClassification = {
          _id: this.itemsClassification._id,
          ...this.generalService.checkEmptyFields(this.itemsClassificationForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(itemsClassificationsApi, newitemsClassification).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/itemsClassifications']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.itemsClassificationForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.itemsClassificationForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(itemsClassificationsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.itemsClassificationForm.reset();
            this.itemsClassificationForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let itemsClassificationNameAr = '';
    let itemsClassificationNameEn = '';
    let isActive = true;

    if (this.itemsClassification) {
      image = this.itemsClassification.image;
      code = this.itemsClassification.code;
      itemsClassificationNameAr = this.itemsClassification.itemsClassificationNameAr;
      itemsClassificationNameEn = this.itemsClassification.itemsClassificationNameEn;
      isActive = this.itemsClassification.isActive;
    }
    this.itemsClassificationForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      itemsClassificationNameAr: new FormControl(itemsClassificationNameAr, Validators.required),
      itemsClassificationNameEn: new FormControl(itemsClassificationNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
