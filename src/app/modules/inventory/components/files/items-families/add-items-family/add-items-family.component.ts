import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, itemsFamiliesApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IItemsFamily } from '../../../../interfaces/IItemsFamily';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-items-family',
  templateUrl: './add-items-family.component.html',
  styleUrls: ['./add-items-family.component.scss']
})

export class AddItemsFamilyComponent implements OnInit, OnDestroy {
  itemsFamilyForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  itemsFamily: IItemsFamily;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(itemsFamiliesApi, null, null, id)
              .subscribe((itemsFamily: IItemsFamily) => {
                this.itemsFamily = itemsFamily;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.itemsFamilyForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.itemsFamilyForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.itemsFamily.image = '';
    this.itemsFamilyForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.itemsFamilyForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.itemsFamily) {
      if (this.itemsFamilyForm.valid) {
        const newitemsFamily = {
          _id: this.itemsFamily._id,
          ...this.generalService.checkEmptyFields(this.itemsFamilyForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(itemsFamiliesApi, newitemsFamily).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/itemsFamilies']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.itemsFamilyForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.itemsFamilyForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(itemsFamiliesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.itemsFamilyForm.reset();
            this.itemsFamilyForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let itemsFamilyNameAr = '';
    let itemsFamilyNameEn = '';
    let isActive = true;

    if (this.itemsFamily) {
      image = this.itemsFamily.image;
      code = this.itemsFamily.code;
      itemsFamilyNameAr = this.itemsFamily.itemsFamilyNameAr;
      itemsFamilyNameEn = this.itemsFamily.itemsFamilyNameEn;
      isActive = this.itemsFamily.isActive;
    }
    this.itemsFamilyForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      itemsFamilyNameAr: new FormControl(itemsFamilyNameAr, Validators.required),
      itemsFamilyNameEn: new FormControl(itemsFamilyNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
