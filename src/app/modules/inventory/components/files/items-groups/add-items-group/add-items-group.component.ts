import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, itemsGroupsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IItemsGroup } from '../../../../interfaces/IItemsGroup';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-items-group',
  templateUrl: './add-items-group.component.html',
  styleUrls: ['./add-items-group.component.scss']
})

export class AddItemsGroupComponent implements OnInit, OnDestroy {
  itemsGroupForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  itemsGroup: IItemsGroup;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(itemsGroupsApi, null, null, id)
              .subscribe((itemsGroup: IItemsGroup) => {
                this.itemsGroup = itemsGroup;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.itemsGroupForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.itemsGroupForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.itemsGroup.image = '';
    this.itemsGroupForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.itemsGroupForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.itemsGroup) {
      if (this.itemsGroupForm.valid) {
        const newitemsGroup = {
          _id: this.itemsGroup._id,
          ...this.generalService.checkEmptyFields(this.itemsGroupForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(itemsGroupsApi, newitemsGroup).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/itemsGroups']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.itemsGroupForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.itemsGroupForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(itemsGroupsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.itemsGroupForm.reset();
            this.itemsGroupForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let itemsGroupNameAr = '';
    let itemsGroupNameEn = '';
    let isActive = true;

    if (this.itemsGroup) {
      image = this.itemsGroup.image;
      code = this.itemsGroup.code;
      itemsGroupNameAr = this.itemsGroup.itemsGroupNameAr;
      itemsGroupNameEn = this.itemsGroup.itemsGroupNameEn;
      isActive = this.itemsGroup.isActive;
    }
    this.itemsGroupForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      itemsGroupNameAr: new FormControl(itemsGroupNameAr, Validators.required),
      itemsGroupNameEn: new FormControl(itemsGroupNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
