import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, itemsSectionsApi, companiesApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IItemsSection } from '../../../../interfaces/IItemsSection';
import { IBranch } from 'src/app/modules/general/interfaces/IBranch';
import { ICompany } from 'src/app/modules/general/interfaces/ICompany';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-items-section',
  templateUrl: './add-items-section.component.html',
  styleUrls: ['./add-items-section.component.scss']
})

export class AddItemsSectionComponent implements OnInit, OnDestroy {
  itemsSectionForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  itemsSection: IItemsSection;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Branches
  branches: IBranch[] = [];
  branchesInputFocused: boolean;
  branchesCount: number;
  noBranches: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(itemsSectionsApi, null, null, id)
              .subscribe((itemsSection: IItemsSection) => {
                this.itemsSection = itemsSection;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.itemsSectionForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(companiesApi, null, null, companyId).subscribe((res: ICompany) => {
        if (res.branches.length) {
          this.branchesCount = res.branches.length;
          this.branches.push(...res.branches);
        } else {
          this.noBranches = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.itemsSectionForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.itemsSection.image = '';
    this.itemsSectionForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.itemsSectionForm.controls;
  }

  get getBranchPrintersArray() {
    return this.itemsSectionForm.get('branchPrinters') as FormArray;
  }

  addPrinter() {
    const control = this.itemsSectionForm.get('branchPrinters') as FormArray;
    control.push(
      new FormGroup({
        branchId: new FormControl(''),
        printerPath: new FormControl(''),
      })
    );
  }

  deletePrinter(index) {
    const control = this.itemsSectionForm.get('branchPrinters') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.itemsSection) {
      if (this.itemsSectionForm.valid) {
        const newitemsSection = {
          _id: this.itemsSection._id,
          ...this.generalService.checkEmptyFields(this.itemsSectionForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(itemsSectionsApi, newitemsSection).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/itemsSections']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.itemsSectionForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.itemsSectionForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(itemsSectionsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.itemsSectionForm.reset();
            this.itemsSectionForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let itemsSectionNameAr = '';
    let itemsSectionNameEn = '';
    let branchPrintersArray = new FormArray([
      new FormGroup({
        branchId: new FormControl(''),
        printerPath: new FormControl(''),
      })
    ]);
    let isActive = true;

    if (this.itemsSection) {
      image = this.itemsSection.image;
      code = this.itemsSection.code;
      itemsSectionNameAr = this.itemsSection.itemsSectionNameAr;
      itemsSectionNameEn = this.itemsSection.itemsSectionNameEn;
      isActive = this.itemsSection.isActive;
      branchPrintersArray = new FormArray([]);
      for (const control of this.itemsSection.branchPrinters) {
        branchPrintersArray.push(
          new FormGroup({
            branchId: new FormControl(control.branchId),
            printerPath: new FormControl(control.printerPath)
          })
        );
      }
    }
    this.itemsSectionForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      itemsSectionNameAr: new FormControl(itemsSectionNameAr, Validators.required),
      itemsSectionNameEn: new FormControl(itemsSectionNameEn),
      isActive: new FormControl(isActive, Validators.required),
      branchPrinters: branchPrintersArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
