import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, itemsTypesApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IItemsType } from '../../../../interfaces/IItemsType';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-items-type',
  templateUrl: './add-items-type.component.html',
  styleUrls: ['./add-items-type.component.scss']
})

export class AddItemsTypeComponent implements OnInit, OnDestroy {
  itemsTypeForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  itemsType: IItemsType;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(itemsTypesApi, null, null, id)
              .subscribe((itemsType: IItemsType) => {
                this.itemsType = itemsType;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.itemsTypeForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.itemsTypeForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.itemsType.image = '';
    this.itemsTypeForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.itemsTypeForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.itemsType) {
      if (this.itemsTypeForm.valid) {
        const newitemsType = {
          _id: this.itemsType._id,
          ...this.generalService.checkEmptyFields(this.itemsTypeForm.value),
          companyId: this.companyId
        };
        this.data.put(itemsTypesApi, newitemsType).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/inventory/itemsTypes']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        })
        this.loadingButton = false;
      }
    } else {
      if (this.itemsTypeForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.itemsTypeForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(itemsTypesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.itemsTypeForm.reset();
            this.itemsTypeForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let itemsTypeNameAr = '';
    let itemsTypeNameEn = '';
    let isActive = true;

    if (this.itemsType) {
      image = this.itemsType.image;
      code = this.itemsType.code;
      itemsTypeNameAr = this.itemsType.itemsTypeNameAr;
      itemsTypeNameEn = this.itemsType.itemsTypeNameEn;
      isActive = this.itemsType.isActive;
    }
    this.itemsTypeForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      itemsTypeNameAr: new FormControl(itemsTypeNameAr, Validators.required),
      itemsTypeNameEn: new FormControl(itemsTypeNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
