import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, itemsUnitsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from '../../../../interfaces/IItemsUnit';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-items-unit',
  templateUrl: './add-items-unit.component.html',
  styleUrls: ['./add-items-unit.component.scss']
})

export class AddItemsUnitComponent implements OnInit, OnDestroy {
  itemsUnitForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  itemsUnit: IItemsUnit;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
    private data: DataService,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(itemsUnitsApi, null, null, id)
              .subscribe((itemsUnit: IItemsUnit) => {
                this.itemsUnit = itemsUnit;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.itemsUnitForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.itemsUnitForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.itemsUnit.image = '';
    this.itemsUnitForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.itemsUnitForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.itemsUnit) {
      if (this.itemsUnitForm.valid) {
        const newitemsUnit = {
          _id: this.itemsUnit._id,
          ...this.generalService.checkEmptyFields(this.itemsUnitForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(itemsUnitsApi, newitemsUnit).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/itemsUnits']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.itemsUnitForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.itemsUnitForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(itemsUnitsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.itemsUnitForm.reset();
            this.itemsUnitForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let itemsUnitNameAr = '';
    let itemsUnitNameEn = '';
    let isActive = true;

    if (this.itemsUnit) {
      image = this.itemsUnit.image;
      code = this.itemsUnit.code;
      itemsUnitNameAr = this.itemsUnit.itemsUnitNameAr;
      itemsUnitNameEn = this.itemsUnit.itemsUnitNameEn;
      isActive = this.itemsUnit.isActive;
    }
    this.itemsUnitForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      itemsUnitNameAr: new FormControl(itemsUnitNameAr, Validators.required),
      itemsUnitNameEn: new FormControl(itemsUnitNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
