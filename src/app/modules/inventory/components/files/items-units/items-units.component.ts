import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, itemsUnitsApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IItemsUnit } from '../../../interfaces/IItemsUnit';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-items-units',
  templateUrl: './items-units.component.html',
  styleUrls: ['./items-units.component.scss']
})

export class ItemsUnitsComponent implements OnInit, OnDestroy {
  itemsUnits: IItemsUnit[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService
  ) { }

  ngOnInit() {
    this.getItemsUnitsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(itemsUnitsApi, pageNumber).subscribe((res: IDataRes) => {
      this.itemsUnits = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(itemsUnitsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.itemsUnits = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getItemsUnitsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(itemsUnitsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.itemsUnits = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      itemsUnitNameAr: new FormControl(''),
      itemsUnitNameEn: new FormControl('')
    });
  }

  deleteModal(itemsUnit: IItemsUnit) {
    const initialState = {
      code: itemsUnit.code,
      nameAr: itemsUnit.itemsUnitNameAr,
      nameEn: itemsUnit.itemsUnitNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(itemsUnit._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(itemsUnitsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.itemsUnits = this.generalService.removeItem(this.itemsUnits, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getItemsUnitsFirstPage() {
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, 1).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
