import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, itemsVariablesApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IItemsVariable } from '../../../../interfaces/IItemsVariable';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-items-variable',
  templateUrl: './add-items-variable.component.html',
  styleUrls: ['./add-items-variable.component.scss']
})

export class AddItemsVariableComponent implements OnInit, OnDestroy {
  itemsVariableForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  itemsVariable: IItemsVariable;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(itemsVariablesApi, null, null, id)
              .subscribe((itemsVariable: IItemsVariable) => {
                this.itemsVariable = itemsVariable;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.itemsVariableForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.itemsVariableForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.itemsVariable.image = '';
    this.itemsVariableForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.itemsVariableForm.controls;
  }

  get getItemsVariablesArray() {
    return this.itemsVariableForm.get('itemsVariables') as FormArray;
  }

  addItemsVariable() {
    const control = this.itemsVariableForm.get('itemsVariables') as FormArray;
    control.push(
      new FormGroup({
        name: new FormControl('', Validators.required)
      })
    );
  }

  deleteItemsVariable(index) {
    const control = this.itemsVariableForm.get('itemsVariables') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.itemsVariable) {
      if (this.itemsVariableForm.valid) {
        const newitemsVariable = {
          _id: this.itemsVariable._id,
          ...this.generalService.checkEmptyFields(this.itemsVariableForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(itemsVariablesApi, newitemsVariable).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/itemsVariables']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.itemsVariableForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.itemsVariableForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(itemsVariablesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.itemsVariableForm.reset();
            this.itemsVariableForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let itemsVariableNameAr = '';
    let itemsVariableNameEn = '';
    let itemsVariablesArray = new FormArray([
      new FormGroup({
        name: new FormControl('', Validators.required)
      })
    ]);
    let isActive = true;

    if (this.itemsVariable) {
      image = this.itemsVariable.image;
      code = this.itemsVariable.code;
      itemsVariableNameAr = this.itemsVariable.itemsVariableNameAr;
      itemsVariableNameEn = this.itemsVariable.itemsVariableNameEn;
      itemsVariablesArray = new FormArray([]);
      for (const variable of this.itemsVariable.itemsVariables) {
        itemsVariablesArray.push(
          new FormGroup({
            name: new FormControl(variable.name, Validators.required)
          })
        );
      }
      isActive = this.itemsVariable.isActive;
    }
    this.itemsVariableForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      itemsVariableNameAr: new FormControl(itemsVariableNameAr, Validators.required),
      itemsVariableNameEn: new FormControl(itemsVariableNameEn),
      itemsVariables: itemsVariablesArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
