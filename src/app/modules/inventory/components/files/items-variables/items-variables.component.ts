import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, itemsVariablesApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IItemsVariable } from '../../../interfaces/IItemsVariable';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-items-variables',
  templateUrl: './items-variables.component.html',
  styleUrls: ['./items-variables.component.scss']
})

export class ItemsVariablesComponent implements OnInit, OnDestroy {
  itemsVariables: IItemsVariable[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService,
  ) { }

  ngOnInit() {
    this.getItemsVariablesFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(itemsVariablesApi, pageNumber).subscribe((res: IDataRes) => {
      this.itemsVariables = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(itemsVariablesApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.itemsVariables = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getItemsVariablesFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.itemsVariables = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      itemsVariableNameAr: new FormControl(''),
      itemsVariableNameEn: new FormControl('')
    });
  }

  deleteModal(itemsVariable: IItemsVariable) {
    const initialState = {
      code: itemsVariable.code,
      nameAr: itemsVariable.itemsVariableNameAr,
      nameEn: itemsVariable.itemsVariableNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(itemsVariable._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(itemsVariablesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.itemsVariables = this.generalService.removeItem(this.itemsVariables, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getItemsVariablesFirstPage() {
    this.subscriptions.push(
      this.data.get(itemsVariablesApi, 1).subscribe((res: IDataRes) => {
        this.itemsVariables = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
