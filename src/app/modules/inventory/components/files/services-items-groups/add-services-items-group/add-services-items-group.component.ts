import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, servicesItemsGroupsApi, detailedChartOfAccountsApi, servicesItemsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IServicesItemsGroup } from '../../../../interfaces/IServicesItemsGroup';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-services-items-group',
  templateUrl: './add-services-items-group.component.html',
  styleUrls: ['./add-services-items-group.component.scss']
})

export class AddServicesItemsGroupComponent implements OnInit, OnDestroy {
  servicesItemsGroupForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  servicesItemsGroup: IServicesItemsGroup;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  noChartOfAccounts: boolean;
  chartOfAccountsCount: number;
  salesAccountInputFocused: boolean;
  salesRevenueAccountInputFocused: boolean;
  purchaseAccountInputFocused: boolean;
  purchaseRevenueAccountInputFocused: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(servicesItemsGroupsApi, null, null, id)
              .subscribe((servicesItemsGroup: IServicesItemsGroup) => {
                this.servicesItemsGroup = servicesItemsGroup;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.servicesItemsGroupForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.servicesItemsGroupForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.servicesItemsGroup.image = '';
    this.servicesItemsGroupForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.servicesItemsGroupForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.servicesItemsGroup) {
      if (this.servicesItemsGroupForm.valid) {
        const newservicesItemsGroup = {
          _id: this.servicesItemsGroup._id,
          ...this.generalService.checkEmptyFields(this.servicesItemsGroupForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(servicesItemsGroupsApi, newservicesItemsGroup).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/servicesItemsGroups']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.servicesItemsGroupForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.servicesItemsGroupForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(servicesItemsGroupsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.servicesItemsGroupForm.reset();
            this.servicesItemsGroupForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let servicesItemsGroupNameAr = '';
    let servicesItemsGroupNameEn = '';
    let accountsGroup = new FormGroup({
      salesAccountId: new FormControl(''),
      salesRevenueAccountId: new FormControl(''),
      purchaseAccountId: new FormControl(''),
      purchaseRevenueAccountId: new FormControl(''),
    });
    let isActive = true;

    if (this.servicesItemsGroup) {
      image = this.servicesItemsGroup.image;
      code = this.servicesItemsGroup.code;
      servicesItemsGroupNameAr = this.servicesItemsGroup.servicesItemsGroupNameAr;
      servicesItemsGroupNameEn = this.servicesItemsGroup.servicesItemsGroupNameEn;
      isActive = this.servicesItemsGroup.isActive;
      accountsGroup = new FormGroup({
        salesAccountId: new FormControl(this.servicesItemsGroup.accounts.salesAccountId),
        salesRevenueAccountId: new FormControl(this.servicesItemsGroup.accounts.salesRevenueAccountId),
        purchaseAccountId: new FormControl(this.servicesItemsGroup.accounts.purchaseAccountId),
        purchaseRevenueAccountId: new FormControl(this.servicesItemsGroup.accounts.purchaseRevenueAccountId),
      });
    }
    this.servicesItemsGroupForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      servicesItemsGroupNameAr: new FormControl(servicesItemsGroupNameAr, Validators.required),
      servicesItemsGroupNameEn: new FormControl(servicesItemsGroupNameEn),
      isActive: new FormControl(isActive, Validators.required),
      accounts: accountsGroup
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
