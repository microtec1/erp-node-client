import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, servicesItemsApi, servicesItemsGroupsApi, detailedChartOfAccountsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IServicesItem } from '../../../../interfaces/IServicesItem';
import { IServicesItemsGroup } from '../../../../interfaces/IServicesItemsGroup';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-services-items',
  templateUrl: './add-services-items.component.html',
  styleUrls: ['./add-services-items.component.scss']
})

export class AddServicesItemComponent implements OnInit, OnDestroy {
  servicesItemForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  servicesItem: IServicesItem;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Services Items Groups
  servicesItemsGroups: IServicesItemsGroup[] = [];
  servicesItemsGroupsInputFocused: boolean;
  hasMoreServicesItemsGroups: boolean;
  servicesItemsGroupsCount: number;
  selectedServicesItemsGroupsPage = 1;
  servicesItemsGroupsPagesNo: number;
  noServicesItemsGroups: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  noChartOfAccounts: boolean;
  chartOfAccountsCount: number;
  salesAccountInputFocused: boolean;
  salesRevenueAccountInputFocused: boolean;
  purchaseAccountInputFocused: boolean;
  purchaseRevenueAccountInputFocused: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(servicesItemsApi, null, null, id)
              .subscribe((servicesItem: IServicesItem) => {
                this.servicesItem = servicesItem;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.servicesItemForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(servicesItemsGroupsApi, 1).subscribe((res: IDataRes) => {
        this.servicesItemsGroupsPagesNo = res.pages;
        this.servicesItemsGroupsCount = res.count;
        if (this.servicesItemsGroupsPagesNo > this.selectedServicesItemsGroupsPage) {
          this.hasMoreServicesItemsGroups = true;
        }
        this.servicesItemsGroups.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchServicesItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      servicesItemsGroupNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(servicesItemsGroupsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noServicesItemsGroups = true;
          } else {
            this.noServicesItemsGroups = false;
            for (const item of res.results) {
              if (this.servicesItemsGroups.length) {
                const uniqueServicesItemsGroups = this.servicesItemsGroups.filter(x => x._id !== item._id);
                this.servicesItemsGroups = uniqueServicesItemsGroups;
              }
              this.servicesItemsGroups.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreServicesItemsGroups() {
    this.selectedServicesItemsGroupsPage = this.selectedServicesItemsGroupsPage + 1;
    this.subscriptions.push(
      this.data.get(servicesItemsGroupsApi, this.selectedServicesItemsGroupsPage).subscribe((res: IDataRes) => {
        if (this.servicesItemsGroupsPagesNo > this.selectedServicesItemsGroupsPage) {
          this.hasMoreServicesItemsGroups = true;
        } else {
          this.hasMoreServicesItemsGroups = false;
        }
        for (const item of res.results) {
          if (this.servicesItemsGroups.length) {
            const uniqueServicesItemsGroups = this.servicesItemsGroups.filter(x => x._id !== item._id);
            this.servicesItemsGroups = uniqueServicesItemsGroups;
          }
          this.servicesItemsGroups.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.servicesItemForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.servicesItem.image = '';
    this.servicesItemForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.servicesItemForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.servicesItem) {
      if (this.servicesItemForm.valid) {
        const newservicesItem = {
          _id: this.servicesItem._id,
          ...this.generalService.checkEmptyFields(this.servicesItemForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(servicesItemsApi, newservicesItem).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/servicesItems']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.servicesItemForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.servicesItemForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(servicesItemsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.servicesItemForm.reset();
            this.servicesItemForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let servicesItemNameAr = '';
    let servicesItemNameEn = '';
    let servicesItemGroupId = '';
    let accountsGroup = new FormGroup({
      salesAccountId: new FormControl(''),
      salesRevenueAccountId: new FormControl(''),
      purchaseAccountId: new FormControl(''),
      purchaseRevenueAccountId: new FormControl(''),
    });
    let isActive = true;

    if (this.servicesItem) {
      image = this.servicesItem.image;
      code = this.servicesItem.code;
      servicesItemNameAr = this.servicesItem.servicesItemNameAr;
      servicesItemNameEn = this.servicesItem.servicesItemNameEn;
      servicesItemGroupId = this.servicesItem.servicesItemGroupId;
      isActive = this.servicesItem.isActive;
      accountsGroup = new FormGroup({
        salesAccountId: new FormControl(this.servicesItem.accounts.salesAccountId),
        salesRevenueAccountId: new FormControl(this.servicesItem.accounts.salesRevenueAccountId),
        purchaseAccountId: new FormControl(this.servicesItem.accounts.purchaseAccountId),
        purchaseRevenueAccountId: new FormControl(this.servicesItem.accounts.purchaseRevenueAccountId),
      });
    }
    this.servicesItemForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      servicesItemNameAr: new FormControl(servicesItemNameAr, Validators.required),
      servicesItemNameEn: new FormControl(servicesItemNameEn),
      servicesItemGroupId: new FormControl(servicesItemGroupId),
      isActive: new FormControl(isActive, Validators.required),
      accounts: accountsGroup
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
