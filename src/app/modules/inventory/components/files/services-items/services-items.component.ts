import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, servicesItemsApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IServicesItem } from '../../../interfaces/IServicesItem';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-services-items',
  templateUrl: './services-items.component.html',
  styleUrls: ['./services-items.component.scss']
})

export class ServicesItemsComponent implements OnInit, OnDestroy {
  servicesItems: IServicesItem[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService,
  ) { }

  ngOnInit() {
    this.getServicesItemsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(servicesItemsApi, pageNumber).subscribe((res: IDataRes) => {
      this.servicesItems = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(servicesItemsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.servicesItems = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getServicesItemsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(servicesItemsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.servicesItems = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      servicesItemNameAr: new FormControl(''),
      servicesItemNameEn: new FormControl('')
    });
  }

  deleteModal(servicesItem: IServicesItem) {
    const initialState = {
      code: servicesItem.code,
      nameAr: servicesItem.servicesItemNameAr,
      nameEn: servicesItem.servicesItemNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(servicesItem._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(servicesItemsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.servicesItems = this.generalService.removeItem(this.servicesItems, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getServicesItemsFirstPage() {
    this.subscriptions.push(
      this.data.get(servicesItemsApi, 1).subscribe((res: IDataRes) => {
        this.servicesItems = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
