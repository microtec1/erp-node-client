import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, storesItemsApi, itemsGroupsApi, warehousesApi, itemsFamiliesApi, itemsUnitsApi, itemsTypesApi, itemsVariablesApi, itemsClassificationsApi, taxesApi, detailedChartOfAccountsApi, detailedCostCentersApi, suppliersApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IStoreItem } from '../../../../interfaces/IStoreItem';
import { IItemsGroup } from '../../../../interfaces/IItemsGroup';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICostCenter } from 'src/app/modules/accounting/modules/gl/interfaces/ICostCenter';
import { ITax } from 'src/app/modules/sales/interfaces/ITax';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IItemsUnit } from '../../../../interfaces/IItemsUnit';
import { IItemsVariable } from '../../../../interfaces/IItemsVariable';
import { IWarehouse } from '../../../../interfaces/IWarehouse';
import { DataService } from 'src/app/common/services/shared/data.service';
import { VariablesModalComponent } from 'src/app/common/components/variables-modal/variables-modal.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { IItemsClassification } from 'src/app/modules/inventory/interfaces/IItemsClassification';
import { IItemsType } from 'src/app/modules/inventory/interfaces/IItemsType';
import { IItemsFamily } from 'src/app/modules/inventory/interfaces/IItemsFamily';

@Component({
  selector: 'app-add-store-item',
  templateUrl: './add-store-item.component.html',
  styleUrls: ['./add-store-item.component.scss']
})

export class AddStoreItemComponent implements OnInit, OnDestroy {
  storeItemForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  storeItem: IStoreItem;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  specifications: string[] = [];
  companyId = companyId;
  detailsMode: boolean;
  bsModalRef: BsModalRef;
  @ViewChild('dropzone') dropzone: any;


  // Items Groups
  itemsGroups: IItemsGroup[] = [];
  itemsGroupsInputFocused: boolean;
  hasMoreItemsGroups: boolean;
  itemsGroupsCount: number;
  selectedItemsGroupsPage = 1;
  itemsGroupsPagesNo: number;
  noItemsGroups: boolean;

  // Items Families
  itemsFamilies: IItemsFamily[] = [];
  itemsFamiliesInputFocused: boolean;
  hasMoreItemsFamilies: boolean;
  itemsFamiliesCount: number;
  selectedItemsFamiliesPage = 1;
  itemsFamiliesPagesNo: number;
  noItemsFamilies: boolean;

  // Items Units
  itemsUnits: IItemsUnit[] = [];
  itemsUnitsInputFocused: boolean;
  hasMoreItemsUnits: boolean;
  itemsUnitsCount: number;
  selectedItemsUnitsPage = 1;
  itemsUnitsPagesNo: number;
  noItemsUnits: boolean;

  // Items Units Without Main
  filteredItemsUnits: IItemsUnit[] = [];
  filteredItemsUnitsInputFocused: boolean;
  hasMoreFilteredItemsUnits: boolean;
  filteredItemsUnitsCount: number;
  selectedFilteredItemsUnitsPage = 1;
  filteredItemsUnitsPagesNo: number;
  noFilteredItemsUnits: boolean;

  // Items Types
  itemsTypes: IItemsType[] = [];
  itemsTypesInputFocused: boolean;
  hasMoreItemsTypes: boolean;
  itemsTypesCount: number;
  selectedItemsTypesPage = 1;
  itemsTypesPagesNo: number;
  noItemsTypes: boolean;

  // Items Classifications
  itemsClassifications: IItemsClassification[] = [];
  itemsClassificationsInputFocused: boolean;
  hasMoreItemsClassifications: boolean;
  itemsClassificationsCount: number;
  selectedItemsClassificationsPage = 1;
  itemsClassificationsPagesNo: number;
  noItemsClassifications: boolean;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Taxes
  taxes: ITax[] = [];
  taxesInputFocused: boolean;
  hasMoreTaxes: boolean;
  taxesCount: number;
  selectedTaxesPage = 1;
  taxesPagesNo: number;
  noTaxes: boolean;

  // Cost Centers
  costCenters: ICostCenter[] = [];
  costCentersInputFocused: boolean;
  costCentersCount: number;
  noCostCenters: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;
  salesAccountInputFocused: boolean;
  salesRevenueAccountInputFocused: boolean;
  purchaseAccountInputFocused: boolean;
  purchaseRevenueAccountInputFocused: boolean;
  inventoryAccountInputFocused: boolean;
  goodsOnRoadAccountInputFocused: boolean;
  adjustmentByIncreaseAccountInputFocused: boolean;
  adjustmentByDecreaseAccountInputFocused: boolean;
  costAdjustmentByIncreaseAccountInputFocused: boolean;
  costAdjustmentByDecreaseAccountInputFocused: boolean;
  consignmentAccountInputFocused: boolean;
  giftsSamplesAccountInputFocused: boolean;
  displayGoodsAccountInputFocused: boolean;
  reservedGoodsAccountInputFocused: boolean;
  underExchangeGoodsAccountInputFocused: boolean;
  withoutInvoicesGoodsAccountInputFocused: boolean;
  outgoingGoodsAccountInputFocused: boolean;
  receivedGoodsAccountInputFocused: boolean;
  inventoryCostAccountInputFocused: boolean;
  inventoryDamageAccountInputFocused: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;


  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(storesItemsApi, null, null, id)
              .subscribe((storeItem: IStoreItem) => {
                this.storeItem = storeItem;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.storeItemForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsGroupsApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsGroupsPagesNo = res.pages;
          this.itemsGroupsCount = res.count;
          if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
            this.hasMoreItemsGroups = true;
          }
          this.itemsGroups.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          this.warehouses.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsFamiliesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsFamiliesPagesNo = res.pages;
          this.itemsFamiliesCount = res.count;
          if (this.itemsFamiliesPagesNo > this.selectedItemsFamiliesPage) {
            this.hasMoreItemsFamilies = true;
          }
          this.itemsFamilies.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsUnitsApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsUnitsPagesNo = res.pages;
          this.itemsUnitsCount = res.count;
          if (this.itemsUnitsPagesNo > this.selectedItemsUnitsPage) {
            this.hasMoreItemsUnits = true;
          }
          this.itemsUnits.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsTypesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsTypesPagesNo = res.pages;
          this.itemsTypesCount = res.count;
          if (this.itemsTypesPagesNo > this.selectedItemsTypesPage) {
            this.hasMoreItemsTypes = true;
          }
          this.itemsTypes.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(itemsClassificationsApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsClassificationsPagesNo = res.pages;
          this.itemsClassificationsCount = res.count;
          if (this.itemsClassificationsPagesNo > this.selectedItemsClassificationsPage) {
            this.hasMoreItemsClassifications = true;
          }
          this.itemsClassifications.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    const searchBody = {
      taxType: 'add',
      companyId
    };
    this.subscriptions.push(
      this.data
        .get(taxesApi, null, searchBody)
        .subscribe((res: IDataRes) => {
          this.taxesPagesNo = res.pages;
          this.taxesCount = res.count;
          if (this.taxesPagesNo > this.selectedTaxesPage) {
            this.hasMoreTaxes = true;
          }
          this.taxes.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(detailedCostCentersApi, null, null, companyId).subscribe((res: ICostCenter[]) => {
        if (res.length) {
          this.costCenters.push(...res);
          this.costCentersCount = res.length;
        } else {
          this.noCostCenters = true;
        }

        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(suppliersApi, 1)
        .subscribe((res: IDataRes) => {
          this.suppliersPagesNo = res.pages;
          this.suppliersCount = res.count;
          if (this.suppliersPagesNo > this.selectedSuppliersPage) {
            this.hasMoreSuppliers = true;
          }
          this.suppliers.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe(
        (res: IChartOfAccount[]) => {
          if (res.length) {
            this.chartOfAccounts.push(...res);
            this.chartOfAccountsCount = res.length;
          } else {
            this.noChartOfAccounts = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );

    this.subscriptions.push(
      this.data
        .get(storesItemsApi, 1)
        .subscribe((res: IDataRes) => {
          this.storeItemsPagesNo = res.pages;
          this.storeItemsCount = res.count;
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          }
          this.storeItems.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              const itemsArray = [];
              for (const item of res.results) {
                if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                  itemsArray.push(item);
                }
              }
              for (const item of itemsArray) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
            const itemsArray = [];
            for (const item of res.results) {
              if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                itemsArray.push(item);
              }
            }
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(
                  x => x._id !== item._id
                );
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      itemsGroupNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsGroups = true;
            } else {
              this.noItemsGroups = false;
              for (const item of res.results) {
                if (this.itemsGroups.length) {
                  const uniqueItemsGroups = this.itemsGroups.filter(
                    x => x._id !== item._id
                  );
                  this.itemsGroups = uniqueItemsGroups;
                }
                this.itemsGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsGroups() {
    this.selectedItemsGroupsPage = this.selectedItemsGroupsPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsGroupsApi, this.selectedItemsGroupsPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
            this.hasMoreItemsGroups = true;
          } else {
            this.hasMoreItemsGroups = false;
          }
          for (const item of res.results) {
            if (this.itemsGroups.length) {
              const uniqueItemsGroups = this.itemsGroups.filter(
                x => x._id !== item._id
              );
              this.itemsGroups = uniqueItemsGroups;
            }
            this.itemsGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsVariables(event) {
    const searchValue = event;
    const searchQuery = {
      itemsVariableNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsVariablesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsVariables = true;
            } else {
              this.noItemsVariables = false;
              for (const item of res.results) {
                if (this.itemsVariables.length) {
                  const uniqueItemsVariables = this.itemsVariables.filter(
                    x => x._id !== item._id
                  );
                  this.itemsVariables = uniqueItemsVariables;
                }
                this.itemsVariables.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsVariables() {
    this.selectedItemsVariablesPage = this.selectedItemsVariablesPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, this.selectedItemsVariablesPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          } else {
            this.hasMoreItemsVariables = false;
          }
          for (const item of res.results) {
            if (this.itemsVariables.length) {
              const uniqueItemsVariables = this.itemsVariables.filter(
                x => x._id !== item._id
              );
              this.itemsVariables = uniqueItemsVariables;
            }
            this.itemsVariables.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsTypes(event) {
    const searchValue = event;
    const searchQuery = {
      itemsTypeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsTypesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsTypes = true;
            } else {
              this.noItemsTypes = false;
              for (const item of res.results) {
                if (this.itemsTypes.length) {
                  const uniqueItemsTypes = this.itemsTypes.filter(
                    x => x._id !== item._id
                  );
                  this.itemsTypes = uniqueItemsTypes;
                }
                this.itemsTypes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsTypes() {
    this.selectedItemsTypesPage = this.selectedItemsTypesPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsTypesApi, this.selectedItemsTypesPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsTypesPagesNo > this.selectedItemsTypesPage) {
            this.hasMoreItemsTypes = true;
          } else {
            this.hasMoreItemsTypes = false;
          }
          for (const item of res.results) {
            if (this.itemsTypes.length) {
              const uniqueItemsTypes = this.itemsTypes.filter(
                x => x._id !== item._id
              );
              this.itemsTypes = uniqueItemsTypes;
            }
            this.itemsTypes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsUnits(event) {
    const searchValue = event;
    const searchQuery = {
      itemsUnitNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsUnitsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsUnits = true;
            } else {
              this.noItemsUnits = false;
              for (const item of res.results) {
                if (this.itemsUnits.length) {
                  const uniqueItemsUnits = this.itemsUnits.filter(
                    x => x._id !== item._id
                  );
                  this.itemsUnits = uniqueItemsUnits;
                }
                this.itemsUnits.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsUnits() {
    this.selectedItemsUnitsPage = this.selectedItemsUnitsPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsUnitsApi, this.selectedItemsUnitsPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsUnitsPagesNo > this.selectedItemsUnitsPage) {
            this.hasMoreItemsUnits = true;
          } else {
            this.hasMoreItemsUnits = false;
          }
          for (const item of res.results) {
            if (this.itemsUnits.length) {
              const uniqueItemsUnits = this.itemsUnits.filter(
                x => x._id !== item._id
              );
              this.itemsUnits = uniqueItemsUnits;
            }
            this.itemsUnits.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsClassifications(event) {
    const searchValue = event;
    const searchQuery = {
      itemsClassificationNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsClassificationsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsClassifications = true;
            } else {
              this.noItemsClassifications = false;
              for (const item of res.results) {
                if (this.itemsClassifications.length) {
                  const uniqueItemsClassifications = this.itemsClassifications.filter(
                    x => x._id !== item._id
                  );
                  this.itemsClassifications = uniqueItemsClassifications;
                }
                this.itemsClassifications.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsClassifications() {
    this.selectedItemsClassificationsPage = this.selectedItemsClassificationsPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsClassificationsApi, this.selectedItemsClassificationsPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsClassificationsPagesNo > this.selectedItemsClassificationsPage) {
            this.hasMoreItemsClassifications = true;
          } else {
            this.hasMoreItemsClassifications = false;
          }
          for (const item of res.results) {
            if (this.itemsClassifications.length) {
              const uniqueItemsClassifications = this.itemsClassifications.filter(
                x => x._id !== item._id
              );
              this.itemsClassifications = uniqueItemsClassifications;
            }
            this.itemsClassifications.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsFamilies(event) {
    const searchValue = event;
    const searchQuery = {
      itemsFamilyNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(itemsFamiliesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsFamilies = true;
            } else {
              this.noItemsFamilies = false;
              for (const item of res.results) {
                if (this.itemsFamilies.length) {
                  const uniqueItemsFamilies = this.itemsFamilies.filter(
                    x => x._id !== item._id
                  );
                  this.itemsFamilies = uniqueItemsFamilies;
                }
                this.itemsFamilies.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsFamilies() {
    this.selectedItemsFamiliesPage = this.selectedItemsFamiliesPage + 1;
    this.subscriptions.push(
      this.data
        .get(itemsFamiliesApi, this.selectedItemsFamiliesPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsFamiliesPagesNo > this.selectedItemsFamiliesPage) {
            this.hasMoreItemsFamilies = true;
          } else {
            this.hasMoreItemsFamilies = false;
          }
          for (const item of res.results) {
            if (this.itemsFamilies.length) {
              const uniqueItemsFamilies = this.itemsFamilies.filter(
                x => x._id !== item._id
              );
              this.itemsFamilies = uniqueItemsFamilies;
            }
            this.itemsFamilies.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(suppliersApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSuppliers = true;
            } else {
              this.noSuppliers = false;
              for (const item of res.results) {
                if (this.suppliers.length) {
                  const uniqueSuppliers = this.suppliers.filter(
                    x => x._id !== item._id
                  );
                  this.suppliers = uniqueSuppliers;
                }
                this.suppliers.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data
        .get(suppliersApi, this.selectedSuppliersPage)
        .subscribe((res: IDataRes) => {
          if (this.suppliersPagesNo > this.selectedSuppliersPage) {
            this.hasMoreSuppliers = true;
          } else {
            this.hasMoreSuppliers = false;
          }
          for (const item of res.results) {
            if (this.suppliers.length) {
              const uniqueSuppliers = this.suppliers.filter(
                x => x._id !== item._id
              );
              this.suppliers = uniqueSuppliers;
            }
            this.suppliers.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.storeItemForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.storeItem.image = '';
    this.storeItemForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.storeItemForm.controls;
  }

  get getSuppliersArray() {
    return this.storeItemForm.controls.otherInformation.get('suppliers') as FormArray;
  }

  get getAlternativeItemsArray() {
    return this.storeItemForm.get('alternativeItems') as FormArray;
  }

  get getItemDetailsArray() {
    return this.storeItemForm.get('itemDetails') as FormArray;
  }

  get getRelatedItemUnitsArray() {
    return this.storeItemForm.get('relatedItemUnits') as FormArray;
  }

  get getBarcodeArray() {
    return this.storeItemForm.get('barCode') as FormArray;
  }

  get getWarehousesArray() {
    return this.storeItemForm.get('warehouses') as FormArray;
  }

  get getItemBalanceArray() {
    return this.storeItemForm.get('itemBalance') as FormArray;
  }

  addWarehouse() {
    const control = this.storeItemForm.get('warehouses') as FormArray;
    control.push(
      new FormGroup({
        warehouseId: new FormControl(''),
        reorderQuantity: new FormControl(''),
        maxQuantity: new FormControl(''),
        minQuantity: new FormControl('')
      })
    );
  }

  deleteWarehouse(index) {
    const control = this.storeItemForm.get('warehouses') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBarcode() {
    const control = this.storeItemForm.get('barCode') as FormArray;
    control.push(
      new FormGroup({
        barCode: new FormControl(''),
        unitId: new FormControl(''),
        conversionFactor: new FormControl(null),
        price: new FormControl(null),
        status: new FormControl('active'),
        variables: new FormArray([]),
        variablesControl: new FormControl([]),
        discountPercentage: new FormControl(''),
        discountValue: new FormControl(''),
      })
    );
  }

  deleteBarcode(index) {
    const control = this.storeItemForm.get('barCode') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }


  addRelatedItemUnit() {
    const control = this.storeItemForm.get('relatedItemUnits') as FormArray;
    control.push(
      new FormGroup({
        unitId: new FormControl(''),
        conversionFactor: new FormControl(null),
        isMain: new FormControl(false),
      })
    );
  }

  deleteRelatedItemUnit(index) {
    const control = this.storeItemForm.get('relatedItemUnits') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addItemDetail() {
    const control = this.storeItemForm.get('itemDetails') as FormArray;
    control.push(
      new FormGroup({
        itemId: new FormControl(''),
        unitId: new FormControl(''),
        amount: new FormControl(null),
      })
    );
  }

  deleteItemDetail(index) {
    const control = this.storeItemForm.get('itemDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addSupplier() {
    const control = this.storeItemForm.controls.otherInformation.get('suppliers') as FormArray;
    control.push(
      new FormGroup({
        supplierId: new FormControl(''),
      })
    );
  }

  deleteSupplier(index) {
    const control = this.storeItemForm.controls.otherInformation.get('suppliers') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addAlternativeItem() {
    const control = this.storeItemForm.get('alternativeItems') as FormArray;
    control.push(
      new FormGroup({
        itemId: new FormControl(''),
      })
    );
  }

  deleteAlternativeItem(index) {
    const control = this.storeItemForm.get('alternativeItems') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  setPurchaseHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.storeItemForm.patchValue({
        otherInformation: {
          lastPurchaseHijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        }
      });
    }
  }

  setPurchaseGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.storeItemForm.patchValue({
        otherInformation: {
          lastPurchaseGregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        }
      });
    }
  }

  setSaleHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.storeItemForm.patchValue({
        otherInformation: {
          lastSaleHijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        }
      });
    }
  }

  setSaleGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.storeItemForm.patchValue({
        otherInformation: {
          lastSaleGregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        }
      });
    }
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  addSpecification(event) {
    this.specifications = [];
    this.specifications.push(
      event
    );
  }

  getRelatedItemsUnits(units) {
    const array = [];
    for (const unit of units) {
      const choosed = this.itemsUnits.find(item => item._id === unit.unitId)
      array.push(choosed);
    }
    return array;
  }

  filterItemsUnits(event) {
    this.filteredItemsUnits = this.itemsUnits.filter(item => item._id !== event);
    this.getRelatedItemUnitsArray.at(0).patchValue({
      unitId: event,
      conversionFactor: 1,
      isMain: true
    });
  }

  validateConversionFactor(formGroup) {
    const group = formGroup as FormGroup;
    group.controls.conversionFactor.setValidators(Validators.required);
    group.controls.conversionFactor.updateValueAndValidity();
  }

  getConversionFactor(event, formGroup: FormGroup) {
    const selected = this.storeItemForm.value.relatedItemUnits.find(item => item.unitId === event);
    formGroup.patchValue({
      conversionFactor: selected.conversionFactor
    });
  }

  addVariables(code: FormGroup) {
    const initialState = {
      formData: code.value.variables.length ? code.value.variables : null
    };
    this.bsModalRef = this.modalService.show(VariablesModalComponent, { initialState });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          const array = code.get('variables') as FormArray;
          if (confirmed.variables.length) {
            array.controls = [];
            for (const item of confirmed.variables) {
              array.push(
                new FormGroup({
                  variableId: new FormControl(item.variableId),
                  itemVariableNameId: new FormControl(item.itemVariableNameId)
                })
              );
            }
          }
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  getVariables(group: FormGroup) {
    const variables = [];
    const variablesControl = group.get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        if (value) {
          variables.push(
            { name: variable.itemsVariableNameAr, value: value.name }
          );
        }
      }
    }
    return variables;
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    delete this.storeItemForm.value.itemBalance;
    if (!this.storeItemForm.value.otherInformation.workWithBatch) {
      delete this.storeItemForm.value.otherInformation.workBatchType;
    }
    for (const code of this.storeItemForm.value.barCode) {
      delete code.variablesControl;
    }
    if (this.storeItemForm.value.otherInformation) {
      if (this.storeItemForm.value.otherInformation.lastPurchaseGregorianDate) {
        if (
          typeof this.storeItemForm.value.otherInformation.lastPurchaseGregorianDate !==
          'string'
        ) {
          this.storeItemForm.value.otherInformation.lastPurchaseGregorianDate = this.generalService.format(
            this.storeItemForm.value.otherInformation.lastPurchaseGregorianDate
          );
        }
        if (
          typeof this.storeItemForm.value.otherInformation.lastPurchaseHijriDate !==
          'string'
        ) {
          this.storeItemForm.value.otherInformation.lastPurchaseHijriDate = this.generalService.formatHijriDate(
            this.storeItemForm.value.otherInformation.lastPurchaseHijriDate
          );
        }
      }
      if (this.storeItemForm.value.otherInformation.lastSaleGregorianDate) {
        if (
          typeof this.storeItemForm.value.otherInformation.lastSaleGregorianDate !==
          'string'
        ) {
          this.storeItemForm.value.otherInformation.lastSaleGregorianDate = this.generalService.format(
            this.storeItemForm.value.otherInformation.lastSaleGregorianDate
          );
        }
        if (
          typeof this.storeItemForm.value.otherInformation.lastSaleHijriDate !==
          'string'
        ) {
          this.storeItemForm.value.otherInformation.lastSaleHijriDate = this.generalService.formatHijriDate(
            this.storeItemForm.value.otherInformation.lastSaleHijriDate
          );
        }
      }
    }
    if (this.storeItem) {
      if (this.storeItemForm.valid) {
        const newStoreItem = {
          _id: this.storeItem._id,
          ...this.generalService.checkEmptyFields(this.storeItemForm.value),
          companyId
        };
        const emptyObject = this.generalService.isEmpty(newStoreItem.warehouses[0]);
        if (emptyObject) {
          newStoreItem.warehouses = [];
        }
        const emptyObject2 = this.generalService.isEmpty(newStoreItem.relatedItemUnits[0]);
        if (emptyObject2) {
          newStoreItem.relatedItemUnits = [];
        }
        const emptyObject3 = this.generalService.isEmpty(newStoreItem.otherInformation.suppliers[0]);
        if (emptyObject3) {
          newStoreItem.otherInformation.suppliers = [];
        }
        const emptyObject4 = this.generalService.isEmpty(newStoreItem.itemDetails[0]);
        if (emptyObject4) {
          newStoreItem.itemDetails = [];
        }
        const emptyObject6 = this.generalService.isEmpty(newStoreItem.alternativeItems[0]);
        if (emptyObject6) {
          newStoreItem.alternativeItems = [];
        }
        delete newStoreItem.itemsUnitId;
        this.subscriptions.push(
          this.data.put(storesItemsApi, newStoreItem).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/inventory/storeItems']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          }, err => {
            this.loadingButton = false;
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.storeItemForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.storeItemForm.value),
          companyId
        };
        const emptyObject = this.generalService.isEmpty(formValue.warehouses[0]);
        if (emptyObject) {
          formValue.warehouses = [];
        }
        const emptyObject2 = this.generalService.isEmpty(formValue.relatedItemUnits[0]);
        if (emptyObject2) {
          formValue.relatedItemUnits = [];
        }
        const emptyObject3 = this.generalService.isEmpty(formValue.otherInformation.suppliers[0]);
        if (emptyObject3) {
          formValue.otherInformation.suppliers = [];
        }
        const emptyObject4 = this.generalService.isEmpty(formValue.itemDetails[0]);
        if (emptyObject4) {
          formValue.itemDetails = [];
        }
        const emptyObject6 = this.generalService.isEmpty(formValue.alternativeItems[0]);
        if (emptyObject6) {
          formValue.alternativeItems = [];
        }
        delete formValue.itemsUnitId;
        this.subscriptions.push(
          this.data.post(storesItemsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.storeItemForm.reset();
            this.storeItemForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let itemsNameAr = '';
    let itemsNameEn = '';
    let itemsGroupId = '';
    let itemsUnitId = '';
    let itemsFamilyId = '';
    let itemsTypeId = '';
    let itemsCategoryId = '';
    let suppliersArray = new FormArray([
      new FormGroup({
        supplierId: new FormControl('')
      })
    ]);
    let otherInformationGroup = new FormGroup({
      maximumDiscount: new FormControl(null),
      maximumDiscountValue: new FormControl(null),
      externalReference: new FormControl(''),
      initialDisassemblyPrice: new FormControl(null),
      lastPurchaseGregorianDate: new FormControl(''),
      lastPurchaseHijriDate: new FormControl(null),
      lastPurchasePrice: new FormControl(null),
      lastSaleGregorianDate: new FormControl(''),
      lastSaleHijriDate: new FormControl(null),
      lastSalePrice: new FormControl(null),
      defaultCost: new FormControl(null),
      warrantyPeriod: new FormControl(null),
      vatId: new FormControl(''),
      suppliers: suppliersArray,
      costCenterId: new FormControl(null),
      priceIncludesVAT: new FormControl(null),
      balanceCategory: new FormControl(null),
      nonReturnableItem: new FormControl(null),
      consignmentCategory: new FormControl(null),
      workWithBatch: new FormControl(null),
      workBatchType: new FormControl('fifo'),
      productFormation: new FormControl('independent')
    });

    let accountsGroup = new FormGroup({
      salesAccountId: new FormControl(''),
      salesRevenueAccountId: new FormControl(''),
      purchaseAccountId: new FormControl(''),
      purchaseRevenueAccountId: new FormControl(''),
      inventoryAccountId: new FormControl(''),
      goodsOnRoadAccountId: new FormControl(''),
      adjustmentByIncreaseAccountId: new FormControl(''),
      adjustmentByDecreaseAccountId: new FormControl(''),
      costAdjustmentByIncreaseAccountId: new FormControl(''),
      costAdjustmentByDecreaseAccountId: new FormControl(''),
      consignmentAccountId: new FormControl(''),
      giftsSamplesAccountId: new FormControl(''),
      displayGoodsAccountId: new FormControl(''),
      reservedGoodsAccountId: new FormControl(''),
      underExchangeGoodsAccountId: new FormControl(''),
      withoutInvoicesGoodsAccountId: new FormControl(''),
      outgoingGoodsAccountId: new FormControl(''),
      receivedGoodsAccountId: new FormControl(''),
      inventoryCostAccountId: new FormControl(''),
      inventoryDamageAccountId: new FormControl('')
    });

    let warehousesArray = new FormArray([
      new FormGroup({
        warehouseId: new FormControl(''),
        reorderQuantity: new FormControl(''),
        maxQuantity: new FormControl(''),
        minQuantity: new FormControl('')
      })
    ]);

    let alternativeItemsArray = new FormArray([
      new FormGroup({
        itemId: new FormControl('')
      })
    ]);
    let specificationsArray = [];
    let additionalSpecificationsGroup = new FormGroup({
      specifications: new FormControl(specificationsArray),
      foodAuthorityNumber: new FormControl(''),
      itemBtnColor: new FormControl('')
    });

    let itemDetailsArray = new FormArray([
      new FormGroup({
        itemId: new FormControl(''),
        unitId: new FormControl(''),
        amount: new FormControl(null),
      })
    ]);

    let relatedItemUnitsArray = new FormArray([
      new FormGroup({
        unitId: new FormControl(''),
        conversionFactor: new FormControl(null),
        isMain: new FormControl(true)
      }),
      new FormGroup({
        unitId: new FormControl(''),
        conversionFactor: new FormControl(null),
        isMain: new FormControl(false)
      })
    ]);

    let variablesArray = new FormArray([]);

    let barCodeArray = new FormArray([
      new FormGroup({
        barCode: new FormControl(''),
        unitId: new FormControl(''),
        conversionFactor: new FormControl(null),
        price: new FormControl(null),
        status: new FormControl('active'),
        variables: variablesArray,
        discountPercentage: new FormControl(''),
        discountValue: new FormControl(''),
      })
    ]);

    let itemBalanceBarCodeArray = new FormArray([
      new FormGroup({
        barCode: new FormControl(''),
        unitId: new FormControl(''),
        price: new FormControl(null),
      })
    ]);

    let itemBalanceArray = new FormArray([
      new FormGroup({
        barCode: itemBalanceBarCodeArray,
        warehouseId: new FormControl(''),
        balance: new FormControl(null),
        averageCost: new FormControl(null),
      })
    ]);

    let isActive = true;

    if (this.storeItem) {
      image = this.storeItem.image;
      code = this.storeItem.code;
      itemsNameAr = this.storeItem.itemsNameAr;
      itemsNameEn = this.storeItem.itemsNameEn;
      isActive = this.storeItem.isActive;
      itemsGroupId = this.storeItem.itemsGroupId;
      itemsUnitId = this.storeItem.relatedItemUnits[0].unitId;
      itemsFamilyId = this.storeItem.itemsFamilyId;
      itemsTypeId = this.storeItem.itemsTypeId;
      itemsCategoryId = this.storeItem.itemsCategoryId;
      if (this.storeItem.otherInformation.suppliers.length) {
        suppliersArray = new FormArray([]);
        for (const control of this.storeItem.otherInformation.suppliers) {
          suppliersArray.push(
            new FormGroup({
              supplierId: new FormControl(control.supplierId)
            })
          );
        }
      }
      if (this.storeItem.otherInformation) {
        let lastPurchaseGregorianDate = null;
        let lastSaleGregorianDate = null;
        if (this.storeItem.otherInformation.lastPurchaseGregorianDate) {
          lastPurchaseGregorianDate = new Date(this.storeItem.otherInformation.lastPurchaseGregorianDate + 'UTC');
        }
        if (this.storeItem.otherInformation.lastSaleGregorianDate) {
          lastSaleGregorianDate = new Date(this.storeItem.otherInformation.lastSaleGregorianDate + 'UTC');
        }
        otherInformationGroup = new FormGroup({
          maximumDiscount: new FormControl(this.storeItem.otherInformation.maximumDiscount),
          maximumDiscountValue: new FormControl(this.storeItem.otherInformation.maximumDiscountValue),
          externalReference: new FormControl(this.storeItem.otherInformation.externalReference),
          initialDisassemblyPrice: new FormControl(this.storeItem.otherInformation.initialDisassemblyPrice),
          lastPurchaseGregorianDate: new FormControl(lastPurchaseGregorianDate),
          lastPurchaseHijriDate: new FormControl(this.storeItem.otherInformation.lastPurchaseHijriDate),
          lastPurchasePrice: new FormControl(this.storeItem.otherInformation.lastPurchasePrice),
          lastSaleGregorianDate: new FormControl(lastSaleGregorianDate),
          lastSaleHijriDate: new FormControl(this.storeItem.otherInformation.lastSaleHijriDate),
          lastSalePrice: new FormControl(this.storeItem.otherInformation.lastSalePrice),
          defaultCost: new FormControl(this.storeItem.otherInformation.defaultCost),
          warrantyPeriod: new FormControl(this.storeItem.otherInformation.warrantyPeriod),
          vatId: new FormControl(this.storeItem.otherInformation.vatId),
          suppliers: suppliersArray,
          costCenterId: new FormControl(this.storeItem.otherInformation.costCenterId),
          priceIncludesVAT: new FormControl(this.storeItem.otherInformation.priceIncludesVAT),
          balanceCategory: new FormControl(this.storeItem.otherInformation.balanceCategory),
          nonReturnableItem: new FormControl(this.storeItem.otherInformation.nonReturnableItem),
          consignmentCategory: new FormControl(this.storeItem.otherInformation.consignmentCategory),
          workWithBatch: new FormControl(this.storeItem.otherInformation.workWithBatch),
          workBatchType: new FormControl(this.storeItem.otherInformation.workBatchType),
          productFormation: new FormControl(this.storeItem.otherInformation.productFormation)
        });
      }
      if (this.storeItem.accounts) {
        accountsGroup = new FormGroup({
          salesAccountId: new FormControl(this.storeItem.accounts.salesAccountId),
          salesRevenueAccountId: new FormControl(this.storeItem.accounts.salesRevenueAccountId),
          purchaseAccountId: new FormControl(this.storeItem.accounts.purchaseAccountId),
          purchaseRevenueAccountId: new FormControl(this.storeItem.accounts.purchaseRevenueAccountId),
          inventoryAccountId: new FormControl(this.storeItem.accounts.inventoryAccountId),
          goodsOnRoadAccountId: new FormControl(this.storeItem.accounts.goodsOnRoadAccountId),
          adjustmentByIncreaseAccountId: new FormControl(this.storeItem.accounts.adjustmentByIncreaseAccountId),
          adjustmentByDecreaseAccountId: new FormControl(this.storeItem.accounts.adjustmentByDecreaseAccountId),
          costAdjustmentByIncreaseAccountId: new FormControl(this.storeItem.accounts.costAdjustmentByIncreaseAccountId),
          costAdjustmentByDecreaseAccountId: new FormControl(this.storeItem.accounts.costAdjustmentByDecreaseAccountId),
          consignmentAccountId: new FormControl(this.storeItem.accounts.consignmentAccountId),
          giftsSamplesAccountId: new FormControl(this.storeItem.accounts.giftsSamplesAccountId),
          displayGoodsAccountId: new FormControl(this.storeItem.accounts.displayGoodsAccountId),
          reservedGoodsAccountId: new FormControl(this.storeItem.accounts.reservedGoodsAccountId),
          underExchangeGoodsAccountId: new FormControl(this.storeItem.accounts.underExchangeGoodsAccountId),
          withoutInvoicesGoodsAccountId: new FormControl(this.storeItem.accounts.withoutInvoicesGoodsAccountId),
          outgoingGoodsAccountId: new FormControl(this.storeItem.accounts.outgoingGoodsAccountId),
          receivedGoodsAccountId: new FormControl(this.storeItem.accounts.receivedGoodsAccountId),
          inventoryCostAccountId: new FormControl(this.storeItem.accounts.inventoryCostAccountId),
          inventoryDamageAccountId: new FormControl(this.storeItem.accounts.inventoryDamageAccountId)
        });
      }
      if (this.storeItem.warehouses.length) {
        warehousesArray = new FormArray([]);
        for (const control of this.storeItem.warehouses) {
          warehousesArray.push(
            new FormGroup({
              warehouseId: new FormControl(control.warehouseId),
              reorderQuantity: new FormControl(control.reorderQuantity),
              maxQuantity: new FormControl(control.maxQuantity),
              minQuantity: new FormControl(control.minQuantity)
            })
          );
        }
      }
      if (this.storeItem.alternativeItems.length) {
        alternativeItemsArray = new FormArray([]);
        for (const control of this.storeItem.alternativeItems) {
          alternativeItemsArray.push(
            new FormGroup({
              itemId: new FormControl(control.itemId)
            })
          );
        }
      }

      if (this.storeItem.additionalSpecifications.specifications.length) {
        specificationsArray = [...this.storeItem.additionalSpecifications.specifications];
      }
      if (this.storeItem.additionalSpecifications) {
        additionalSpecificationsGroup = new FormGroup({
          specifications: new FormControl(specificationsArray),
          foodAuthorityNumber: new FormControl(this.storeItem.additionalSpecifications.foodAuthorityNumber),
          itemBtnColor: new FormControl(this.storeItem.additionalSpecifications.itemBtnColor)
        });
      }

      if (this.storeItem.itemDetails.length) {
        itemDetailsArray = new FormArray([]);
        for (const control of this.storeItem.itemDetails) {
          itemDetailsArray.push(
            new FormGroup({
              itemId: new FormControl(control.itemId),
              unitId: new FormControl(control.unitId),
              amount: new FormControl(control.amount),
            })
          );
        }
      }

      if (this.storeItem.relatedItemUnits.length) {
        relatedItemUnitsArray = new FormArray([]);
        for (const control of this.storeItem.relatedItemUnits) {
          relatedItemUnitsArray.push(
            new FormGroup({
              unitId: new FormControl(control.unitId),
              conversionFactor: new FormControl(control.conversionFactor),
              isMain: new FormControl(control.isMain),
            })
          );
        }
      }

      if (this.storeItem.barCode.length) {
        barCodeArray = new FormArray([]);
        for (const control of this.storeItem.barCode) {
          variablesArray = new FormArray([]);
          for (const item of control.variables) {
            variablesArray.push(
              new FormGroup({
                variableId: new FormControl(item.variableId),
                itemVariableNameId: new FormControl(item.itemVariableNameId)
              })
            );
          }
          barCodeArray.push(
            new FormGroup({
              barCode: new FormControl(control.barCode),
              unitId: new FormControl(control.unitId),
              conversionFactor: new FormControl(control.conversionFactor),
              price: new FormControl(control.price),
              status: new FormControl(control.status),
              variables: variablesArray,
              discountPercentage: new FormControl(control.discountPercentage),
              discountValue: new FormControl(control.discountValue),
            })
          );
        }
      }

      if (this.storeItem.itemBalance.length) {
        itemBalanceArray = new FormArray([]);
        for (const control of this.storeItem.itemBalance) {
          if (control.barCode.length) {
            itemBalanceBarCodeArray = new FormArray([]);
            for (const item of control.barCode) {
              itemBalanceBarCodeArray.push(
                new FormGroup({
                  barCode: new FormControl(''),
                  unitId: new FormControl(''),
                  price: new FormControl(null),
                })
              );
            }
          }
          itemBalanceArray.push(
            new FormGroup({
              barCode: itemBalanceBarCodeArray,
              warehouseId: new FormControl(''),
              balance: new FormControl(null),
              averageCost: new FormControl(null),
            })
          );
        }
      }

    }

    this.storeItemForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      itemsNameAr: new FormControl(itemsNameAr, Validators.required),
      itemsNameEn: new FormControl(itemsNameEn),
      isActive: new FormControl(isActive, Validators.required),
      itemsGroupId: new FormControl(itemsGroupId, Validators.required),
      itemsFamilyId: new FormControl(itemsFamilyId),
      itemsUnitId: new FormControl(itemsUnitId, Validators.required),
      itemsTypeId: new FormControl(itemsTypeId),
      itemsCategoryId: new FormControl(itemsCategoryId),
      otherInformation: otherInformationGroup,
      accounts: accountsGroup,
      warehouses: warehousesArray,
      alternativeItems: alternativeItemsArray,
      additionalSpecifications: additionalSpecificationsGroup,
      itemDetails: itemDetailsArray,
      relatedItemUnits: relatedItemUnitsArray,
      barCode: barCodeArray,
      itemBalance: itemBalanceArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
