import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IWarehouse } from '../../../../interfaces/IWarehouse';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IRegion } from 'src/app/modules/general/interfaces/IRegion.model';
import { ICity } from 'src/app/modules/general/interfaces/ICity.model';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { DataService } from 'src/app/common/services/shared/data.service';
import { WarehousesService } from 'src/app/modules/inventory/services/warehouses.service';
import { citiesApi, regionsApi, detailedChartOfAccountsApi, warehousesApi, warehousesTreeApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-warehouse',
  templateUrl: './add-warehouse.component.html',
  styleUrls: ['./add-warehouse.component.scss']
})
export class AddWarehouseComponent implements OnInit {
  @Input() item: IWarehouse;
  @Input() selectedItem: IWarehouse;
  @Input() sidebarMode: boolean;
  @Output() finished: EventEmitter<any> = new EventEmitter();
  warehouseForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  warehouse: IWarehouse;
  inventorySettings = JSON.parse(localStorage.getItem('inventorySettings'));


  // Cities
  cities: ICity[] = [];
  citiesInputFocused: boolean;
  hasMoreCities: boolean;
  citiesCount: number;
  selectedCitiesPage = 1;
  citiesPagesNo: number;
  noCities: boolean;

  // Regions
  regions: IRegion[] = [];
  regionsInputFocused: boolean;
  hasMoreRegions: boolean;
  regionsCount: number;
  selectedRegionsPage = 1;
  regionsPagesNo: number;
  noRegions: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private warehousesService: WarehousesService
  ) { }

  ngOnInit() {
    this.initForm();
    if (this.inventorySettings.company.automaticGenerateWarehouseCode) {
      this.warehouseForm.controls.code.clearValidators();
      this.warehouseForm.controls.code.updateValueAndValidity();
    }
    this.subscriptions.push(
      this.warehousesService.fillFormChange.subscribe((res: IWarehouse) => {
        this.warehouse = res;
        if (this.warehouse) {
          this.warehouseForm.patchValue({
            code: this.warehouse.code,
            warehouseNameAr: this.warehouse.warehouseNameAr,
            warehouseNameEn: this.warehouse.warehouseNameEn,
            warehouseType: this.warehouse.warehouseType,
            isActive: this.warehouse.isActive
          });
        } else {
          this.warehouseForm.patchValue({
            code: '',
            warehouseNameAr: '',
            warehouseNameEn: '',
            warehouseType: '',
            isActive: true
          });
        }
      })
    );

    this.subscriptions.push(
      this.data.get(citiesApi, 1).subscribe((res: IDataRes) => {
        this.citiesPagesNo = res.pages;
        this.citiesCount = res.count;
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        }
        this.cities.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(regionsApi, 1).subscribe((res: IDataRes) => {
        this.regionsPagesNo = res.pages;
        this.regionsCount = res.count;
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        }
        this.regions.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchRegions(event) {
    const searchValue = event;
    const searchQuery = {
      regionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(regionsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noRegions = true;
          } else {
            this.noRegions = false;
            for (const item of res.results) {
              if (this.regions.length) {
                const uniqueRegions = this.regions.filter(x => x._id !== item._id);
                this.regions = uniqueRegions;
              }
              this.regions.push(item);
            }
          }
          this.regions = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreRegions() {
    this.selectedRegionsPage = this.selectedRegionsPage + 1;
    this.subscriptions.push(
      this.data.get(regionsApi, this.selectedRegionsPage).subscribe((res: IDataRes) => {
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        } else {
          this.hasMoreRegions = false;
        }
        for (const item of res.results) {
          if (this.regions.length) {
            const uniqueRegions = this.regions.filter(x => x._id !== item._id);
            this.regions = uniqueRegions;
          }
          this.regions.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCities(event) {
    const searchValue = event;
    const searchQuery = {
      cityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(citiesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCities = true;
          } else {
            this.noCities = false;
            for (const item of res.results) {
              if (this.cities.length) {
                const uniqueCities = this.cities.filter(x => x._id !== item._id);
                this.cities = uniqueCities;
              }
              this.cities.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCities() {
    this.selectedCitiesPage = this.selectedCitiesPage + 1;
    this.subscriptions.push(
      this.data.get(citiesApi, this.selectedCitiesPage).subscribe((res: IDataRes) => {
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        } else {
          this.hasMoreCities = false;
        }
        for (const item of res.results) {
          if (this.cities.length) {
            const uniqueCities = this.cities.filter(x => x._id !== item._id);
            this.cities = uniqueCities;
          }
          this.cities.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }


  get form() {
    return this.warehouseForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  cancel() {
    this.finished.emit(true);
  }

  private initForm() {
    let code = '';
    let warehouseNameAr = '';
    let warehouseNameEn = '';
    let warehousePositionType = 'warehouse';
    let warehouseType = 'warehouse';
    let glAccountId = '';
    let regionId = '';
    let cityId = '';
    let address = '';
    let telephone = null;
    let internalNumber = null;
    let mobile = null;
    let fax = null;
    let mailBox = '';
    let postalCode = '';
    let email = '';
    let responsiblePerson = '';
    let isActive = true;

    if (this.item) {
      code = this.item.code;
      warehouseNameAr = this.item.warehouseNameAr;
      warehouseNameEn = this.item.warehouseNameEn;
      warehousePositionType = this.item.warehousePositionType;
      warehouseType = this.item.warehouseType;
      glAccountId = this.item.glAccountId;
      regionId = this.item.regionId;
      cityId = this.item.cityId;
      address = this.item.address;
      telephone = this.item.telephone;
      internalNumber = this.item.internalNumber;
      mobile = this.item.mobile;
      fax = this.item.fax;
      mailBox = this.item.mailBox;
      postalCode = this.item.postalCode;
      email = this.item.email;
      responsiblePerson = this.item.responsiblePerson;
      isActive = this.item.isActive;
    }

    this.warehouseForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      warehouseNameAr: new FormControl(warehouseNameAr, Validators.required),
      warehouseNameEn: new FormControl(warehouseNameEn),
      warehousePositionType: new FormControl(
        warehousePositionType,
        Validators.required
      ),
      warehouseType: new FormControl(warehouseType, Validators.required),
      glAccountId: new FormControl(glAccountId),
      regionId: new FormControl(regionId),
      cityId: new FormControl(cityId),
      address: new FormControl(address),
      telephone: new FormControl(telephone),
      internalNumber: new FormControl(internalNumber),
      mobile: new FormControl(mobile),
      fax: new FormControl(fax),
      mailBox: new FormControl(mailBox),
      postalCode: new FormControl(postalCode),
      email: new FormControl(email),
      responsiblePerson: new FormControl(responsiblePerson),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.warehouse || this.item) {
      if (this.warehouseForm.valid) {
        const editItem =
          (this.warehouse && this.warehouse) || (this.item && this.item);
        const newWarehouse = {
          _id: editItem._id,
          ...this.generalService.checkEmptyFields(this.warehouseForm.value),
          companyId
        };
        this.data
          .put(warehousesApi, newWarehouse)
          .subscribe(
            res => {
              this.loadingButton = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.warehousesService.fillFormChange.next(null);
              this.finished.emit();
              this.subscriptions.push(
                this.data.getByRoute(warehousesTreeApi).subscribe((response: IWarehouse[]) => {
                  this.warehousesService.warehousesChanged.next(response);
                  this.uiService.isLoading.next(false);
                }, err => {
                  this.uiService.isLoading.next(false);
                  this.uiService.showErrorMessage(err);
                })
              );
              this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
            },
            err => {
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          );
        this.loadingButton = false;
      }
    } else {
      if (this.warehouseForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.warehouseForm.value),
          companyId,
          parentId: this.selectedItem ? this.selectedItem._id : null
        };
        this.subscriptions.push(
          this.data.post(warehousesApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.warehouseForm.reset();
              this.warehouseForm.patchValue({
                isActive: true,
                warehousePositionType: 'warehouse',
                warehouseType: 'warehouse'
              });
              this.subscriptions.push(
                this.data.getByRoute(warehousesTreeApi).subscribe((response: IWarehouse[]) => {
                  this.warehousesService.warehousesChanged.next(response);
                  this.uiService.isLoading.next(false);
                }, err => {
                  this.uiService.isLoading.next(false);
                  this.uiService.showErrorMessage(err);
                })
              );
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }
}
