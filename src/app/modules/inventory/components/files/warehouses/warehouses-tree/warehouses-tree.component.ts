import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { IWarehouse } from '../../../../interfaces/IWarehouse';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { ICity } from 'src/app/modules/general/interfaces/ICity.model';
import { IRegion } from 'src/app/modules/general/interfaces/IRegion.model';
import { DataService } from 'src/app/common/services/shared/data.service';
import { WarehousesService } from 'src/app/modules/inventory/services/warehouses.service';
import { chartOfAccountsApi, citiesApi, regionsApi, warehousesApi, warehousesTreeApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';

@Component({
  selector: 'app-warehouses-tree',
  templateUrl: './warehouses-tree.component.html',
  styleUrls: ['./warehouses-tree.component.scss']
})
export class WarehousesTreeComponent implements OnInit, OnDestroy {
  @Input() items: IWarehouse[];
  selected: any;
  selectedWarehouse: IWarehouse;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  initAddForm: boolean;
  initEditForm: boolean;

  constructor(
    private modalService: BsModalService,
    private warehousesService: WarehousesService,
    private data: DataService,
    public generalService: GeneralService,
    private uiService: UiService
  ) { }

  ngOnInit() {
  }

  actionFinished() {
    this.initAddForm = false;
    this.initEditForm = false;
    this.selectedWarehouse = null;
    this.selected = null;
  }

  showDetails(item) {
    this.selected = item._id;
    this.selectedWarehouse = item;

    if (this.selectedWarehouse.glAccountId) {
      this.data.get(chartOfAccountsApi, null, null, this.selectedWarehouse.glAccountId).subscribe((res: IChartOfAccount) => {
        this.selectedWarehouse.glAccountName = res.chartOfAccountsNameAr;
        this.uiService.isLoading.next(false);
      });
    }

    if (this.selectedWarehouse.cityId) {
      this.data.get(citiesApi, null, null, this.selectedWarehouse.cityId).subscribe((res: ICity) => {
        this.selectedWarehouse.cityName = res.cityNameAr;
        this.uiService.isLoading.next(false);
      });
    }
    if (this.selectedWarehouse.regionId) {
      this.data.get(regionsApi, null, null, this.selectedWarehouse.regionId).subscribe((res: IRegion) => {
        this.selectedWarehouse.regionName = res.regionNameAr;
        this.uiService.isLoading.next(false);
      });
    }

    this.initAddForm = false;
    this.initEditForm = false;
  }

  deleteModal(warehouse: IWarehouse) {
    const initialState = {
      code: warehouse.code,
      nameAr: warehouse.warehouseNameAr,
      nameEn: warehouse.warehouseNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(warehouse._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(warehousesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.subscriptions.push(
          this.data.getByRoute(warehousesTreeApi).subscribe((response: IWarehouse[]) => {
            this.actionFinished();
            this.warehousesService.warehousesChanged.next(response);
            this.uiService.isLoading.next(false);
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  addNewItem() {
    this.initEditForm = false;
    this.initAddForm = true;
    this.warehousesService.fillFormChange.next(null);
  }

  editItem() {
    this.initAddForm = false;
    this.initEditForm = true;
    this.warehousesService.fillFormChange.next(this.selectedWarehouse);
  }


  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
