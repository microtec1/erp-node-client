import { Component, OnInit, OnDestroy } from '@angular/core';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { Subscription } from 'rxjs';
import { IWarehouse } from '../../../interfaces/IWarehouse';
import { WarehousesService } from '../../../services/warehouses.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { warehousesTreeApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-warehouses',
  templateUrl: './warehouses.component.html',
  styleUrls: ['./warehouses.component.scss']
})
export class WarehousesComponent implements OnInit, OnDestroy {
  warehouses: IWarehouse[];
  subscriptions: Subscription[] = [];
  constructor(
    private warehousesService: WarehousesService,
    private data: DataService,
    private uiService: UiService
  ) { }


  ngOnInit() {
    this.subscriptions.push(
      this.warehousesService.warehousesChanged.subscribe((res: IWarehouse[]) => {
        this.warehouses = res;
      })
    );
    this.subscriptions.push(
      this.data.getByRoute(warehousesTreeApi).subscribe((res: IWarehouse[]) => {
        this.warehouses = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
