import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { companyId, branchId, searchLength } from 'src/app/common/constants/general.constants';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IInventorySettings } from '../../interfaces/IInventorySettings';
import { IWarehouse } from '../../interfaces/IWarehouse';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { DataService } from 'src/app/common/services/shared/data.service';
import { getInventorySettingsApi, warehousesApi, inventorySettingsApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit, OnDestroy {
  settingsForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  settings: IInventorySettings;
  formReady: boolean;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  constructor(
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.data.post(getInventorySettingsApi, {}).subscribe((res: IInventorySettings) => {
        this.settings = res;
        this.initForm();
        this.formReady = true;
        this.uiService.isLoading.next(false);
      })
    );
    this.formReady = true;
    this.initForm();

    this.subscriptions.push(
      this.data.get(warehousesApi, 1).subscribe((res: IDataRes) => {
        this.warehousesPagesNo = res.pages;
        this.warehousesCount = res.count;
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        }
        this.warehouses.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(warehousesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noWarehouses = true;
          } else {
            this.noWarehouses = false;
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data.get(warehousesApi, this.selectedWarehousesPage).subscribe((res: IDataRes) => {
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        } else {
          this.hasMoreWarehouses = false;
        }
        for (const item of res.results) {
          if (this.warehouses.length) {
            const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
            this.warehouses = uniqueWarehouses;
          }
          this.warehouses.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  get form() {
    return this.settingsForm.controls;
  }

  get getBranchesArray() {
    return this.settingsForm.get('branches') as FormArray;
  }

  get getBranchGroup() {
    return this.getBranchesArray.controls[0] as FormGroup;
  }
  get getCompanyGroup() {
    return this.settingsForm.get('company') as FormGroup;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  changeLevelLength(checked) {
    if (!checked) {
      this.settingsForm.patchValue({
        company: {
          levelLength: null
        }
      });
    } else {
      this.settingsForm.patchValue({
        company: {
          levelLength: this.settings.company.levelLength
        }
      });
    }
  }

  changeWorkWithBatchOptions(checked) {
    if (!checked) {
      this.settingsForm.patchValue({
        company: {
          workWithBatchOptions: null
        }
      });
    } else {
      this.settingsForm.patchValue({
        company: {
          workWithBatchOptions: this.settings.company.workWithBatchOptions
        }
      });
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.settingsForm.valid) {
      const newSettings = { companyId, ...this.generalService.checkEmptyFields(this.settingsForm.value) };
      if (!newSettings.company.automaticGeneratecode) {
        delete newSettings.company.levelLength;
      }
      if (!newSettings.company.workWithBatch) {
        delete newSettings.company.workWithBatchOptions;
      }

      this.data.post(inventorySettingsApi, newSettings).subscribe(res => {
        this.loadingButton = false;
        this.submitted = false;
        this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        this.uiService.isLoading.next(false);
        this.subscriptions.push(
          this.data.post(getInventorySettingsApi, {}).subscribe((data: IInventorySettings) => {
            const settings = JSON.stringify(data);
            localStorage.setItem('inventorySettings', settings);
            this.uiService.isLoading.next(false);
          })
        );
      },
        err => {
          this.loadingButton = false;
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        });
    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
    }
  }

  private initForm() {
    let saleMethod = 'averageCost';
    let activateLocation = false;
    let showBarCode = false;
    let automaticGenerateWarehouseCode = false;
    let levelLength = null;
    let workWithBatch = false;
    let workWithBatchOptions = '';
    let warehouseId = '';
    let itemType = 'store';
    let storeConversionNeedsTransferRequestFromBranch = false;

    if (this.settings) {
      saleMethod = this.settings.company.saleMethod;
      activateLocation = this.settings.company.activateLocation;
      showBarCode = this.settings.company.showBarCode;
      automaticGenerateWarehouseCode = this.settings.company.automaticGenerateWarehouseCode;
      levelLength = this.settings.company.levelLength;
      workWithBatch = this.settings.company.workWithBatch;
      workWithBatchOptions = this.settings.company.workWithBatchOptions;
      warehouseId = this.settings.branch.warehouseId;
      itemType = this.settings.branch.itemType;
      storeConversionNeedsTransferRequestFromBranch = this.settings.branch.storeConversionNeedsTransferRequestFromBranch;
    }

    this.settingsForm = new FormGroup({
      company: new FormGroup({
        saleMethod: new FormControl(saleMethod),
        activateLocation: new FormControl(activateLocation),
        showBarCode: new FormControl(showBarCode),
        automaticGenerateWarehouseCode: new FormControl(automaticGenerateWarehouseCode),
        levelLength: new FormControl(levelLength),
        workWithBatch: new FormControl(workWithBatch),
        workWithBatchOptions: new FormControl(workWithBatchOptions),
      }),
      branch: new FormGroup({
        branchId: new FormControl(branchId),
        warehouseId: new FormControl(warehouseId),
        itemType: new FormControl(itemType),
        storeConversionNeedsTransferRequestFromBranch: new FormControl(storeConversionNeedsTransferRequestFromBranch),
      })
    });
  }

  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
