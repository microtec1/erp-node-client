import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAssemblingItemsComponent } from './add-assembling-items.component';

describe('AddAssemblingItemsComponent', () => {
  let component: AddAssemblingItemsComponent;
  let fixture: ComponentFixture<AddAssemblingItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAssemblingItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAssemblingItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
