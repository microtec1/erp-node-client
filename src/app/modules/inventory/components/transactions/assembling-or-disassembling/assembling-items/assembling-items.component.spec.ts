import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssemblingItemsComponent } from './assembling-items.component';

describe('AssemblingItemsComponent', () => {
  let component: AssemblingItemsComponent;
  let fixture: ComponentFixture<AssemblingItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssemblingItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssemblingItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
