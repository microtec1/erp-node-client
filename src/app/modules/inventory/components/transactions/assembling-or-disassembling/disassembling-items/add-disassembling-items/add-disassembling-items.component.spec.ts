import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDisassemblingItemsComponent } from './add-disassembling-items.component';

describe('AddDisassemblingItemsComponent', () => {
  let component: AddDisassemblingItemsComponent;
  let fixture: ComponentFixture<AddDisassemblingItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDisassemblingItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDisassemblingItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
