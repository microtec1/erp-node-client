import { Component, OnInit, OnDestroy } from '@angular/core';
import { IDisassemblingItem } from 'src/app/modules/inventory/interfaces/inventory-transactions/IDisassemblingItem';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  disassemblingItemsApi,
  warehousesApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { BatchDataModalComponent } from 'src/app/common/components/batch-data-modal/batch-data-modal.component';

@Component({
  selector: 'app-add-disassembling-items',
  templateUrl: './add-disassembling-items.component.html',
  styleUrls: ['./add-disassembling-items.component.scss']
})
export class AddDisassemblingItemsComponent implements OnInit, OnDestroy {
  disassemblingItemForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  disassemblingItem: IDisassemblingItem;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;
  showDetails: boolean;
  selectedItem: number;
  inventorySettings = JSON.parse(localStorage.getItem('inventorySettings'));

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  warehousesInputFocused2: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Collected Store Items
  collectedStoreItems: IStoreItem[] = [];
  collectedStoreItemsInputFocused: boolean;
  hasMoreCollectedStoreItems: boolean;
  collectedStoreItemsCount: number;
  selectedCollectedStoreItemsPage = 1;
  collectedStoreItemsPagesNo: number;
  noCollectedStoreItems: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(disassemblingItemsApi, null, null, id).subscribe((disassemblingItem: IDisassemblingItem) => {
              this.disassemblingItem = disassemblingItem;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.disassemblingItemForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          for (const item of res.results) {
            if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    const searchValues = {
      'otherInformation.productFormation': 'summarized'
    };

    this.subscriptions.push(
      this.data.get(storesItemsApi, null, searchValues).subscribe((res: IDataRes) => {
        this.collectedStoreItemsPagesNo = res.pages;
        this.collectedStoreItemsCount = res.count;
        this.collectedStoreItems = res.results;
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              const itemsArray = [];
              for (const item of res.results) {
                if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                  itemsArray.push(item);
                }
              }
              for (const item of itemsArray) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
            const itemsArray = [];
            for (const item of res.results) {
              if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                itemsArray.push(item);
              }
            }
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(
                  x => x._id !== item._id
                );
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  getItem(event, formGroup: FormGroup) {
    this.subscriptions.push(
      this.data.get(storesItemsApi, null, null, event).subscribe((res: any) => {
        if (res.barCode.length > 1) {
          const initialState = {
            barCodes: res.barCode
          };
          this.bsModalRef = this.modalService.show(BarcodeModalComponent, { initialState });
          this.subscriptions.push(
            this.bsModalRef.content.confirmed.subscribe(confirmed => {
              if (confirmed) {
                this.searchBarcodes(confirmed.barCode, formGroup);
                this.bsModalRef.hide();
              } else {
                this.bsModalRef.hide();
              }
            })
          );
        } else {
          this.searchBarcodes(res.barCode[0].barCode, formGroup);
        }
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  checkBatch(id) {
    if (id) {
      const item = this.storeItems.find(x => x._id === id);
      if (item) {
        if (item.otherInformation.workWithBatch) {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  checkProductFormation(id) {
    if (id) {
      const item = this.storeItems.find(x => x._id === id);
      if (item) {
        if (item.otherInformation.productFormation !== 'summarized') {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  details(id, formGroup: FormGroup, index) {
    this.showDetails = true;
    this.selectedItem = index;
  }

  addDetails() {
    this.showDetails = false;
  }

  get getItemDetails() {
    const control = (this.getInventoryDisassemblingItemsDetailsArray.controls[this.selectedItem] as FormGroup).get('details') as FormArray;
    return control.controls;
  }

  openBatchModal(id, formGroup: FormGroup, index) {
    if (id) {
      const initialState = {
        itemId: id,
        request: formGroup.value.itemBatchDetails
      };
      this.bsModalRef = this.modalService.show(BatchDataModalComponent, { initialState, class: 'big-modal' });
      this.subscriptions.push(
        this.bsModalRef.content.formValue.subscribe(formValue => {
          if (formValue) {
            const array = formGroup.controls.itemBatchDetails as FormArray;
            array.controls = [];
            for (const item of formValue.itemBatchDetails) {
              array.push(
                new FormGroup({
                  batchNumber: new FormControl(item.batchNumber),
                  quantity: new FormControl(item.quantity),
                  batchDateGregorian: new FormControl(item.batchDateGregorian),
                  batchDateHijri: new FormControl(item.batchDateHijri),
                  expiryDateGregorian: new FormControl(item.expiryDateGregorian),
                  expiryDateHijri: new FormControl(item.expiryDateHijri)
                })
              );
            }
            this.bsModalRef.hide();
          } else {
            this.bsModalRef.hide();
          }
        })
      );
    }
  }

  fillRow(item, formGroup) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    for (const x of item.barCode.variables) {
      variablesControl.push(
        new FormGroup({
          variableId: new FormControl(x.variableId),
          itemVariableNameId: new FormControl(x.itemVariableNameId)
        })
      );
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
    formGroup.patchValue({
      barCode: item.barCode.barCode,
      itemUnitId: item.barCode.unitId
    });
  }

  getVariables(index, childIndex?) {
    const variables = [];
    let variablesControl;
    if (this.showDetails) {
      variablesControl = ((this.getInventoryDisassemblingItemsDetailsArray.controls[index] as FormGroup).controls.details as FormArray).controls[childIndex].get('variables') as FormArray;
    } else {
      variablesControl = this.getInventoryDisassemblingItemsDetailsArray.controls[index].get('variables') as FormArray;
    }
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  fillRowWarehouse(event) {
    for (const control of this.getInventoryDisassemblingItemsDetailsArray.controls) {
      control.patchValue({
        warehouseId: event
      });
      const array = control.get('details') as FormArray;
      for (const item of array.controls) {
        item.patchValue({
          warehouseId: event
        });
      }
    }
  }

  calculateTotalCost(value, group: FormGroup) {
    group.patchValue({
      totalCost: group.value.quantity * group.value.cost
    });
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.disassemblingItemForm.controls;
  }

  get getInventoryDisassemblingItemsDetailsArray() {
    return this.disassemblingItemForm.get('inventoryDisassemblingItemsDetails') as FormArray;
  }

  addItem(group: FormGroup, target?) {
    if (group.valid) {
      const variablesArray = new FormArray([]);
      for (const item of group.value.variables) {
        variablesArray.push(
          new FormGroup({
            variableId: new FormControl(item.variableId),
            itemVariableNameId: new FormControl(item.itemVariableNameId)
          })
        );
      }
      const itemBatchDetailsArray = new FormArray([]);
      const detailsArray = new FormArray([
        new FormGroup({
          barCode: new FormControl(''),
          itemId: new FormControl(''),
          variables: new FormArray([]),
          quantity: new FormControl(null),
          itemUnitId: new FormControl(''),
          cost: new FormControl(0),
          totalCost: new FormControl(0),
          warehouseId: new FormControl(''),
          descriptionAr: new FormControl(''),
          descriptionEn: new FormControl(''),
          itemBatchDetails: itemBatchDetailsArray
        })
      ]);
      if (target) {
        const control = target.get('details') as FormArray;
        control.push(
          new FormGroup({
            barCode: new FormControl(group.value.barCode, Validators.required),
            itemId: new FormControl(group.value.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(group.value.quantity, Validators.required),
            itemUnitId: new FormControl(group.value.itemUnitId),
            cost: new FormControl(group.value.cost),
            totalCost: new FormControl(group.value.totalCost),
            warehouseId: new FormControl(group.value.warehouseId),
            descriptionAr: new FormControl(group.value.descriptionAr),
            descriptionEn: new FormControl(group.value.descriptionEn),
            itemBatchDetails: itemBatchDetailsArray
          })
        );
      } else {
        this.getInventoryDisassemblingItemsDetailsArray.push(
          new FormGroup({
            barCode: new FormControl(group.value.barCode, Validators.required),
            itemId: new FormControl(group.value.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(group.value.quantity, Validators.required),
            itemUnitId: new FormControl(group.value.itemUnitId),
            cost: new FormControl(group.value.cost),
            totalCost: new FormControl(group.value.totalCost),
            warehouseId: new FormControl(group.value.warehouseId),
            descriptionAr: new FormControl(group.value.descriptionAr),
            descriptionEn: new FormControl(group.value.descriptionEn),
            itemBatchDetails: itemBatchDetailsArray,
            details: detailsArray
          })
        );
      }
      group.reset();
      group.patchValue({
        cost: 0,
        warehouseId: this.disassemblingItemForm.value.warehouseId
      });
      // group.updateValueAndValidity();
    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      this.uiService.isLoading.next(false);
    }

  }

  deleteItem(index, formGroup: FormGroup) {
    let control;
    if (formGroup) {
      control = formGroup.get('details') as FormArray;
    } else {
      control = this.disassemblingItemForm.get('inventoryDisassemblingItemsDetails') as FormArray;
    }
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.disassemblingItemForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.disassemblingItemForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  validateField(value) {
    if (value) {
      if (value !== 'without') {
        this.disassemblingItemForm.controls.sourceNumberId.setValidators(Validators.required);
        this.disassemblingItemForm.controls.sourceNumberId.updateValueAndValidity();
      } else {
        this.disassemblingItemForm.controls.sourceNumberId.clearValidators();
        this.disassemblingItemForm.controls.sourceNumberId.updateValueAndValidity();
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    const detailsArray = (this.disassemblingItemForm.get('inventoryDisassemblingItemsDetails') as FormArray).controls;
    if (detailsArray.length > 1) {
      (detailsArray[0] as FormGroup).controls.barCode.clearValidators();
      (detailsArray[0] as FormGroup).controls.barCode.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.itemId.clearValidators();
      (detailsArray[0] as FormGroup).controls.itemId.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.quantity.clearValidators();
      (detailsArray[0] as FormGroup).controls.quantity.updateValueAndValidity();
    } else {
      this.submitted = false;
      this.loadingButton = false;
      this.uiService.showError('GENERAL.rowMustBeAdded', '');
      return;
    }

    if (typeof this.disassemblingItemForm.value.gregorianDate !== 'string') {
      this.disassemblingItemForm.value.gregorianDate =
        this.generalService.format(this.disassemblingItemForm.value.gregorianDate);
    }
    if (typeof this.disassemblingItemForm.value.hijriDate !== 'string') {
      this.disassemblingItemForm.value.hijriDate =
        this.generalService.formatHijriDate(this.disassemblingItemForm.value.hijriDate);
    }

    for (
      let i = 0;
      i <= this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails.length - 1;
      i++
    ) {

      if (this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails) {
        for (
          let x = 0;
          x <= this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails.length - 1;
          x++
        ) {
          if (this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].batchDateGregorian) {
            if (
              typeof this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].batchDateGregorian !==
              'string'
            ) {
              this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].batchDateGregorian = this.generalService.format(
                this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].batchDateGregorian
              );
            }
            if (typeof this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].batchDateHijri !==
              'string') {
              this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].batchDateHijri = this.generalService.formatHijriDate(
                this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].batchDateHijri
              );
            }
          }
          if (this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].expiryDateGregorian) {
            if (
              typeof this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].expiryDateGregorian !==
              'string'
            ) {
              this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].expiryDateGregorian = this.generalService.format(
                this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].expiryDateGregorian
              );
            }
            if (typeof this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].expiryDateHijri !==
              'string') {
              this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].expiryDateHijri = this.generalService.formatHijriDate(
                this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].itemBatchDetails[x].expiryDateHijri
              );
            }
          }
        }
      }

      if (this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details) {
        for (
          let x = 0;
          x <= this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details.length - 1;
          x++
        ) {
          for (
            let k = 0;
            k <= this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails.length - 1;
            k++
          ) {
            if (this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].batchDateGregorian) {
              if (
                typeof this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].batchDateGregorian !==
                'string'
              ) {
                this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].batchDateGregorian = this.generalService.format(
                  this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].batchDateGregorian
                );
              }
              if (typeof this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].batchDateHijri !==
                'string') {
                this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].batchDateHijri = this.generalService.formatHijriDate(
                  this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].batchDateHijri
                );
              }
            }
            if (this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].expiryDateGregorian) {
              if (
                typeof this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].expiryDateGregorian !==
                'string'
              ) {
                this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].expiryDateGregorian = this.generalService.format(
                  this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].expiryDateGregorian
                );
              }
              if (typeof this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].expiryDateHijri !==
                'string') {
                this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].expiryDateHijri = this.generalService.formatHijriDate(
                  this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails[i].details[x].itemBatchDetails[k].expiryDateHijri
                );
              }
            }
          }
        }
      }

    }

    if (this.disassemblingItem) {
      this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails.splice(0, 1);
      for (const item of this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails) {
        item.details.splice(0, 1);
      }
      if (this.disassemblingItemForm.valid) {
        const newdisassemblingItem = {
          _id: this.disassemblingItem._id,
          ...this.generalService.checkEmptyFields(this.disassemblingItemForm.value)
        };
        this.data.put(disassemblingItemsApi, newdisassemblingItem).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/inventory/disassemblingItems']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.disassemblingItemForm.valid) {
        this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails.splice(0, 1);
        for (const item of this.disassemblingItemForm.value.inventoryDisassemblingItemsDetails) {
          item.details.splice(0, 1);
        }
        const formValue = {
          ...this.generalService.checkEmptyFields(this.disassemblingItemForm.value)
        };
        this.subscriptions.push(
          this.data.post(disassemblingItemsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.disassemblingItemForm.reset();
            this.disassemblingItemForm.patchValue({
              isActive: true,
              transactionSourceType: 'without',
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let inventoryDisassemblingItemsStatus = 'unposted';
    let inventoryDisassemblingItemsDescriptionAr = '';
    let inventoryDisassemblingItemsDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let warehouseId = this.inventorySettings.branch.warehouseId;
    let referenceNumber = '';
    let variablesArray = new FormArray([]);
    let detailsVariablesArray = new FormArray([]);
    let itemBatchDetailsArray = new FormArray([
      new FormGroup({
        batchNumber: new FormControl(''),
        quantity: new FormControl(null),
        batchDateGregorian: new FormControl(''),
        batchDateHijri: new FormControl(''),
        expiryDateGregorian: new FormControl(''),
        expiryDateHijri: new FormControl('')
      })
    ]);
    let detailsItemBatchDetailsArray = new FormArray([
      new FormGroup({
        batchNumber: new FormControl(''),
        quantity: new FormControl(null),
        batchDateGregorian: new FormControl(''),
        batchDateHijri: new FormControl(''),
        expiryDateGregorian: new FormControl(''),
        expiryDateHijri: new FormControl('')
      })
    ]);
    let detailsArray = new FormArray([
      new FormGroup({
        barCode: new FormControl(''),
        itemId: new FormControl(''),
        variables: detailsVariablesArray,
        quantity: new FormControl(null),
        itemUnitId: new FormControl(''),
        cost: new FormControl(0),
        totalCost: new FormControl(0),
        warehouseId: new FormControl(''),
        descriptionAr: new FormControl(''),
        descriptionEn: new FormControl(''),
        itemBatchDetails: detailsItemBatchDetailsArray
      })
    ]);
    let inventoryDisassemblingItemsDetailsArray = new FormArray([
      new FormGroup({
        barCode: new FormControl('', Validators.required),
        itemId: new FormControl('', Validators.required),
        variables: variablesArray,
        quantity: new FormControl(null, Validators.required),
        itemUnitId: new FormControl(''),
        cost: new FormControl(0),
        totalCost: new FormControl(0),
        warehouseId: new FormControl(''),
        descriptionAr: new FormControl(''),
        descriptionEn: new FormControl(''),
        itemBatchDetails: itemBatchDetailsArray,
        details: detailsArray,
      })
    ]);
    let isActive = true;

    if (this.disassemblingItem) {
      code = this.disassemblingItem.code;
      inventoryDisassemblingItemsStatus = this.disassemblingItem.inventoryDisassemblingItemsStatus;
      inventoryDisassemblingItemsDescriptionAr = this.disassemblingItem.inventoryDisassemblingItemsDescriptionAr;
      inventoryDisassemblingItemsDescriptionEn = this.disassemblingItem.inventoryDisassemblingItemsDescriptionEn;
      gregorianDate = new Date(this.disassemblingItem.gregorianDate + 'UTC');
      hijriDate = this.disassemblingItem.hijriDate;
      warehouseId = this.disassemblingItem.warehouseId;
      referenceNumber = this.disassemblingItem.referenceNumber;
      isActive = this.disassemblingItem.isActive;
      inventoryDisassemblingItemsDetailsArray = new FormArray([]);
      for (const control of this.disassemblingItem.inventoryDisassemblingItemsDetails) {
        variablesArray = new FormArray([]);
        itemBatchDetailsArray = new FormArray([]);
        detailsArray = new FormArray([
          new FormGroup({
            barCode: new FormControl(''),
            itemId: new FormControl(''),
            variables: detailsVariablesArray,
            quantity: new FormControl(null),
            itemUnitId: new FormControl(''),
            cost: new FormControl(0),
            totalCost: new FormControl(0),
            warehouseId: new FormControl(this.disassemblingItem.warehouseId),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
            itemBatchDetails: detailsItemBatchDetailsArray
          })
        ]);
        inventoryDisassemblingItemsDetailsArray.push(
          new FormGroup({
            barCode: new FormControl('', Validators.required),
            itemId: new FormControl('', Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(null, Validators.required),
            itemUnitId: new FormControl(''),
            cost: new FormControl(0),
            totalCost: new FormControl(0),
            warehouseId: new FormControl(this.disassemblingItem.warehouseId),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
            itemBatchDetails: itemBatchDetailsArray,
            details: detailsArray
          })
        );
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        for (const item of control.itemBatchDetails) {
          itemBatchDetailsArray.push(
            new FormGroup({
              batchNumber: new FormControl(item.batchNumber),
              quantity: new FormControl(item.quantity),
              batchDateGregorian: new FormControl(new Date(item.batchDateGregorian + 'UTC')),
              batchDateHijri: new FormControl(item.batchDateHijri),
              expiryDateGregorian: new FormControl(new Date(item.expiryDateGregorian + 'UTC')),
              expiryDateHijri: new FormControl(item.expiryDateHijri)
            })
          );
        }
        for (const item of control.details) {
          detailsVariablesArray = new FormArray([]);
          detailsItemBatchDetailsArray = new FormArray([]);
          for (const item2 of item.variables) {
            detailsVariablesArray.push(
              new FormGroup({
                variableId: new FormControl(item2.variableId),
                itemVariableNameId: new FormControl(item2.itemVariableNameId)
              })
            );
          }
          for (const item2 of item.itemBatchDetails) {
            detailsItemBatchDetailsArray.push(
              new FormGroup({
                batchNumber: new FormControl(item2.batchNumber),
                quantity: new FormControl(item2.quantity),
                batchDateGregorian: new FormControl(new Date(item2.batchDateGregorian + 'UTC')),
                batchDateHijri: new FormControl(item2.batchDateHijri),
                expiryDateGregorian: new FormControl(new Date(item2.expiryDateGregorian + 'UTC')),
                expiryDateHijri: new FormControl(item2.expiryDateHijri)
              })
            );
          }
          detailsArray.push(
            new FormGroup({
              barCode: new FormControl(item.barCode),
              itemId: new FormControl(item.itemId),
              variables: detailsVariablesArray,
              quantity: new FormControl(item.quantity),
              itemUnitId: new FormControl(item.itemUnitId),
              cost: new FormControl(0),
              totalCost: new FormControl(0),
              warehouseId: new FormControl(item.warehouseId),
              descriptionAr: new FormControl(item.descriptionAr),
              descriptionEn: new FormControl(item.descriptionEn),
              itemBatchDetails: detailsItemBatchDetailsArray
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        inventoryDisassemblingItemsDetailsArray.push(
          new FormGroup({
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(control.cost),
            totalCost: new FormControl(control.totalCost),
            warehouseId: new FormControl(control.warehouseId),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            itemBatchDetails: itemBatchDetailsArray,
            details: detailsArray
          })
        );
      }
    }
    this.disassemblingItemForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      inventoryDisassemblingItemsStatus: new FormControl(inventoryDisassemblingItemsStatus),
      inventoryDisassemblingItemsDescriptionAr: new FormControl(inventoryDisassemblingItemsDescriptionAr),
      inventoryDisassemblingItemsDescriptionEn: new FormControl(inventoryDisassemblingItemsDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      warehouseId: new FormControl(warehouseId, Validators.required),
      referenceNumber: new FormControl(referenceNumber),
      inventoryDisassemblingItemsDetails: inventoryDisassemblingItemsDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
