import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisassemblingItemsComponent } from './disassembling-items.component';

describe('DisassemblingItemsComponent', () => {
  let component: DisassemblingItemsComponent;
  let fixture: ComponentFixture<DisassemblingItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisassemblingItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisassemblingItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
