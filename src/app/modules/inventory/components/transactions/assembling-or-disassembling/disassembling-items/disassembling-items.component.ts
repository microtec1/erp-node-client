import { Component, OnInit, OnDestroy } from '@angular/core';
import { IDisassemblingItem } from 'src/app/modules/inventory/interfaces/inventory-transactions/IDisassemblingItem';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, disassemblingItemsApi, postDisassemblingItemsApi, unPostDisassemblingItemsApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-disassembling-items',
  templateUrl: './disassembling-items.component.html',
  styleUrls: ['./disassembling-items.component.scss']
})
export class DisassemblingItemsComponent implements OnInit, OnDestroy {
  disassemblingItems: IDisassemblingItem[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getDisassemblingItemsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(disassemblingItemsApi, pageNumber).subscribe((res: IDataRes) => {
      this.disassemblingItems = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(disassemblingItemsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.disassemblingItems = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postDisassemblingItemsApi}/${id}`, {}).subscribe(res => {
        this.disassemblingItems = this.generalService.changeStatus(this.disassemblingItems, id, 'posted', 'inventoryDisassemblingItemsStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostDisassemblingItemsApi}/${id}`, {}).subscribe(res => {
        this.disassemblingItems = this.generalService.changeStatus(this.disassemblingItems, id, 'unposted', 'inventoryDisassemblingItemsStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getDisassemblingItemsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(disassemblingItemsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.disassemblingItems = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryDisassemblingItemsDescriptionAr: new FormControl(''),
      inventoryDisassemblingItemsDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IDisassemblingItem) {
    const initialState = {
      code: item.code,
      nameAr: item.inventoryDisassemblingItemsDescriptionAr,
      nameEn: item.inventoryDisassemblingItemsDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(disassemblingItemsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.disassemblingItems = this.generalService.removeItem(this.disassemblingItems, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getDisassemblingItemsFirstPage() {
    this.subscriptions.push(
      this.data.get(disassemblingItemsApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.disassemblingItems = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
