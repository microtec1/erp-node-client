import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDamageStoreItemsComponent } from './add-damage-store-items.component';

describe('AddDamageStoreItemsComponent', () => {
  let component: AddDamageStoreItemsComponent;
  let fixture: ComponentFixture<AddDamageStoreItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDamageStoreItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDamageStoreItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
