import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamageStoreItemsComponent } from './damage-store-items.component';

describe('DamageStoreItemsComponent', () => {
  let component: DamageStoreItemsComponent;
  let fixture: ComponentFixture<DamageStoreItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamageStoreItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamageStoreItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
