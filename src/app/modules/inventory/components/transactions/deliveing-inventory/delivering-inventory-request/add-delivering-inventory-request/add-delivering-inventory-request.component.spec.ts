import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDeliveringInventoryRequestComponent } from './add-delivering-inventory-request.component';

describe('AddDeliveringInventoryRequestComponent', () => {
  let component: AddDeliveringInventoryRequestComponent;
  let fixture: ComponentFixture<AddDeliveringInventoryRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDeliveringInventoryRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDeliveringInventoryRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
