import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveringInventoryRequestComponent } from './delivering-inventory-request.component';

describe('DeliveringInventoryRequestComponent', () => {
  let component: DeliveringInventoryRequestComponent;
  let fixture: ComponentFixture<DeliveringInventoryRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveringInventoryRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveringInventoryRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
