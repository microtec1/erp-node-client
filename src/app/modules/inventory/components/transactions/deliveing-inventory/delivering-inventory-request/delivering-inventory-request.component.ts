import { Component, OnInit, OnDestroy } from '@angular/core';
import { IDeliveringInventoryRequest } from 'src/app/modules/inventory/interfaces/inventory-transactions/delivering-inventory/IDeliveringInventoryRequest';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, deliveringInventoryRequestApi, postDeliveringInventoryRequestApi, unPostDeliveringInventoryRequestApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-delivering-inventory-request',
  templateUrl: './delivering-inventory-request.component.html',
  styleUrls: ['./delivering-inventory-request.component.scss']
})
export class DeliveringInventoryRequestComponent implements OnInit, OnDestroy {
  deliveringInventoryRequest: IDeliveringInventoryRequest[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getDeliveringInventoryRequestFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(deliveringInventoryRequestApi, pageNumber).subscribe((res: IDataRes) => {
      this.deliveringInventoryRequest = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(deliveringInventoryRequestApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.deliveringInventoryRequest = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postDeliveringInventoryRequestApi}/${id}`, {}).subscribe(res => {
        this.deliveringInventoryRequest = this.generalService.changeStatus(this.deliveringInventoryRequest, id, 'posted', 'deliveringInventoryRequestStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostDeliveringInventoryRequestApi}/${id}`, {}).subscribe(res => {
        this.deliveringInventoryRequest = this.generalService.changeStatus(this.deliveringInventoryRequest, id, 'unposted', 'deliveringInventoryRequestStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getDeliveringInventoryRequestFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(deliveringInventoryRequestApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.deliveringInventoryRequest = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      deliveringInventoryRequestDescriptionAr: new FormControl(''),
      deliveringInventoryRequestDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IDeliveringInventoryRequest) {
    const initialState = {
      code: item.code,
      nameAr: item.deliveringInventoryRequestDescriptionAr,
      nameEn: item.deliveringInventoryRequestDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(deliveringInventoryRequestApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.deliveringInventoryRequest = this.generalService.removeItem(this.deliveringInventoryRequest, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getDeliveringInventoryRequestFirstPage() {
    this.subscriptions.push(
      this.data.get(deliveringInventoryRequestApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.deliveringInventoryRequest = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
