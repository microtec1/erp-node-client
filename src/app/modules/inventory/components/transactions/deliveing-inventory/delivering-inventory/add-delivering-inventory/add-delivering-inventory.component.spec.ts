import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDeliveringInventoryComponent } from './add-delivering-inventory.component';

describe('AddDeliveringInventoryComponent', () => {
  let component: AddDeliveringInventoryComponent;
  let fixture: ComponentFixture<AddDeliveringInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDeliveringInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDeliveringInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
