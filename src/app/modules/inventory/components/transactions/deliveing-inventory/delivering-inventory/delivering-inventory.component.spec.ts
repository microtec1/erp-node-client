import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveringInventoryComponent } from './delivering-inventory.component';

describe('DeliveringInventoryComponent', () => {
  let component: DeliveringInventoryComponent;
  let fixture: ComponentFixture<DeliveringInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveringInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveringInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
