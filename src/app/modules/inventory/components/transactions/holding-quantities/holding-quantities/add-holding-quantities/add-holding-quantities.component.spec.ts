import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddHoldingQuantitiesComponent } from './add-holding-quantities.component';

describe('AddHoldingQuantitiesComponent', () => {
  let component: AddHoldingQuantitiesComponent;
  let fixture: ComponentFixture<AddHoldingQuantitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddHoldingQuantitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHoldingQuantitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
