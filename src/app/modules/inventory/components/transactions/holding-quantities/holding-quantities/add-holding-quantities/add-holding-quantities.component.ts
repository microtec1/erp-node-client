
import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IHoldingQuantity } from 'src/app/modules/inventory/interfaces/inventory-transactions/holding-quantities/IHoldingQuantity';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  projectsApi,
  warehousesApi,
  customersApi,
  storesItemsApi,
  itemsVariablesApi,
  itemsUnitsApi,
  holdingQuantitiesApi,
  holdingQuantitiesRequestApi,
  getStoreItemViaBarcodeApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { BatchDataModalComponent } from 'src/app/common/components/batch-data-modal/batch-data-modal.component';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IRequestHoldingQuantity } from 'src/app/modules/inventory/interfaces/inventory-transactions/holding-quantities/IRequestHoldingQuantity';
import { IProject } from 'src/app/modules/purchases/interfaces/IProject.model';
@Component({
  selector: 'app-add-holding-quantities',
  templateUrl: './add-holding-quantities.component.html',
  styleUrls: ['./add-holding-quantities.component.scss']
})

export class AddHoldingQuantitiesComponent implements OnInit, OnDestroy {
  holdingQuantityForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  holdingQuantity: IHoldingQuantity;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;
  inventorySettings = JSON.parse(localStorage.getItem('inventorySettings'));

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Projects
  projects: IProject[] = [];
  projectsInputFocused: boolean;
  hasMoreProjects: boolean;
  projectsCount: number;
  selectedProjectsPage = 1;
  projectsPagesNo: number;
  noProjects: boolean;

  // Holding Requests
  holdingRequests: IRequestHoldingQuantity[] = [];
  holdingRequestsInputFocused: boolean;
  hasMoreHoldingRequests: boolean;
  holdingRequestsCount: number;
  selectedHoldingRequestsPage = 1;
  holdingRequestsPagesNo: number;
  noHoldingRequests: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(holdingQuantitiesApi, null, null, id).subscribe((holdingQuantity: IHoldingQuantity) => {
              this.holdingQuantity = holdingQuantity;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.holdingQuantityForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .get(projectsApi, 1)
        .subscribe((res: IDataRes) => {
          this.projectsPagesNo = res.pages;
          this.projectsCount = res.count;
          if (this.projectsPagesNo > this.selectedProjectsPage) {
            this.hasMoreProjects = true;
          }
          this.projects.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          for (const item of res.results) {
            if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.getHoldingRequests();

  }

  getHoldingRequests(id?: string) {
    const searchValues: any = {
      inventoryHldQtyReqStatus: 'posted'
    };
    if (id) {
      searchValues.customerId = id;
    }
    this.subscriptions.push(
      this.data
        .get(holdingQuantitiesRequestApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.holdingRequests = res.results;
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              const itemsArray = [];
              for (const item of res.results) {
                if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                  itemsArray.push(item);
                }
              }
              for (const item of itemsArray) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
            const itemsArray = [];
            for (const item of res.results) {
              if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                itemsArray.push(item);
              }
            }
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(
                  x => x._id !== item._id
                );
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchProjects(event) {
    const searchValue = event;
    const searchQuery = {
      projectNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noProjects = true;
            } else {
              this.noProjects = false;
              for (const item of res.results) {
                if (this.projects.length) {
                  const uniqueprojects = this.projects.filter(
                    x => x._id !== item._id
                  );
                  this.projects = uniqueprojects;
                }
                this.projects.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreProjects() {
    this.selectedProjectsPage = this.selectedProjectsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedProjectsPage)
        .subscribe((res: IDataRes) => {
          if (this.projectsPagesNo > this.selectedProjectsPage) {
            this.hasMoreProjects = true;
          } else {
            this.hasMoreProjects = false;
          }
          for (const item of res.results) {
            if (this.projects.length) {
              const uniqueProjects = this.projects.filter(
                x => x._id !== item._id
              );
              this.projects = uniqueProjects;
            }
            this.projects.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  getItem(event, formGroup: FormGroup) {
    this.subscriptions.push(
      this.data.get(storesItemsApi, null, null, event).subscribe((res: any) => {
        if (res.barCode.length > 1) {
          const initialState = {
            barCodes: res.barCode
          };
          this.bsModalRef = this.modalService.show(BarcodeModalComponent, { initialState });
          this.subscriptions.push(
            this.bsModalRef.content.confirmed.subscribe(confirmed => {
              if (confirmed) {
                this.searchBarcodes(confirmed.barCode, formGroup);
                this.bsModalRef.hide();
              } else {
                this.bsModalRef.hide();
              }
            })
          );
        } else {
          this.searchBarcodes(res.barCode[0].barCode, formGroup);
        }
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  checkBatch(id) {
    if (id) {
      const item = this.storeItems.find(x => x._id === id);
      if (item) {
        if (item.otherInformation.workWithBatch) {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  openBatchModal(id, formGroup: FormGroup, index) {
    if (id) {
      const initialState = {
        itemId: id,
        request: this.holdingQuantity ? this.holdingQuantity.inventoryHoldingQuantitiesDetails[index - 1].itemBatchDetails : formGroup.value.itemBatchDetails
      };
      this.bsModalRef = this.modalService.show(BatchDataModalComponent, { initialState, class: 'big-modal' });
      this.subscriptions.push(
        this.bsModalRef.content.formValue.subscribe(formValue => {
          if (formValue) {
            const array = formGroup.controls.itemBatchDetails as FormArray;
            array.controls = [];
            for (const item of formValue.itemBatchDetails) {
              array.push(
                new FormGroup({
                  batchNumber: new FormControl(item.batchNumber),
                  quantity: new FormControl(item.quantity),
                  batchDateGregorian: new FormControl(item.batchDateGregorian),
                  batchDateHijri: new FormControl(item.batchDateHijri),
                  expiryDateGregorian: new FormControl(item.expiryDateGregorian),
                  expiryDateHijri: new FormControl(item.expiryDateHijri)
                })
              );
            }
            this.bsModalRef.hide();
          } else {
            this.bsModalRef.hide();
          }
        })
      );
    }
  }

  fillRow(item, formGroup) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    for (const x of item.barCode.variables) {
      variablesControl.push(
        new FormGroup({
          variableId: new FormControl(x.variableId),
          itemVariableNameId: new FormControl(x.itemVariableNameId)
        })
      );
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
    formGroup.patchValue({
      barCode: item.barCode.barCode,
      itemUnitId: item.barCode.unitId
    });
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getInventoryHoldingQuantitiesDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  fillRowWarehouse(event) {
    for (const control of this.getInventoryHoldingQuantitiesDetailsArray.controls) {
      control.patchValue({
        warehouseId: event
      });
    }
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.holdingQuantityForm.controls;
  }

  get getInventoryHoldingQuantitiesDetailsArray() {
    return this.holdingQuantityForm.get('inventoryHoldingQuantitiesDetails') as FormArray;
  }

  addItem(group: FormGroup) {
    if (this.getInventoryHoldingQuantitiesDetailsArray.valid) {
      const variablesArray = new FormArray([]);
      for (const item of group.value.variables) {
        variablesArray.push(
          new FormGroup({
            variableId: new FormControl(item.variableId),
            itemVariableNameId: new FormControl(item.itemVariableNameId)
          })
        );
      }

      const itemBatchDetailsArray = new FormArray([]);

      this.getInventoryHoldingQuantitiesDetailsArray.push(
        new FormGroup({
          barCode: new FormControl(group.value.barCode, Validators.required),
          itemId: new FormControl(group.value.itemId, Validators.required),
          variables: variablesArray,
          quantity: new FormControl(group.value.quantity, Validators.required),
          itemUnitId: new FormControl(group.value.itemUnitId),
          warehouseBalance: new FormControl(group.value.warehouseBalance),
          warehouseId: new FormControl(group.value.warehouseId),
          descriptionAr: new FormControl(group.value.descriptionAr),
          descriptionEn: new FormControl(group.value.descriptionEn),
          itemBatchDetails: itemBatchDetailsArray
        })
      );
      group.reset();
      // group.updateValueAndValidity();
    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      this.uiService.isLoading.next(false);
    }

  }

  deleteItem(index) {
    const control = this.holdingQuantityForm.get('inventoryHoldingQuantitiesDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.holdingQuantityForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.holdingQuantityForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  fillSourceNumberList(value) {
    // if (value) {
    //   if (value === ط)
    // }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    const detailsArray = (this.holdingQuantityForm.get('inventoryHoldingQuantitiesDetails') as FormArray).controls;
    if (detailsArray.length > 1) {
      (detailsArray[0] as FormGroup).controls.barCode.clearValidators();
      (detailsArray[0] as FormGroup).controls.barCode.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.itemId.clearValidators();
      (detailsArray[0] as FormGroup).controls.itemId.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.quantity.clearValidators();
      (detailsArray[0] as FormGroup).controls.quantity.updateValueAndValidity();
    } else {
      this.submitted = false;
      this.loadingButton = false;
      this.uiService.showError('GENERAL.rowMustBeAdded', '');
      return;
    }

    if (typeof this.holdingQuantityForm.value.gregorianDate !== 'string') {
      this.holdingQuantityForm.value.gregorianDate =
        this.generalService.format(this.holdingQuantityForm.value.gregorianDate);
    }
    if (typeof this.holdingQuantityForm.value.hijriDate !== 'string') {
      this.holdingQuantityForm.value.hijriDate =
        this.generalService.formatHijriDate(this.holdingQuantityForm.value.hijriDate);
    }
    for (
      let i = 0;
      i <= this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails.length - 1;
      i++
    ) {

      for (
        let x = 0;
        x <= this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails.length - 1;
        x++
      ) {
        if (this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateGregorian) {
          if (
            typeof this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateGregorian !==
            'string'
          ) {
            this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateGregorian = this.generalService.format(
              this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateGregorian
            );
          }
          if (typeof this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateHijri !==
            'string') {
            this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateHijri = this.generalService.formatHijriDate(
              this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateHijri
            );
          }
        }
        if (this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateGregorian) {
          if (
            typeof this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateGregorian !==
            'string'
          ) {
            this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateGregorian = this.generalService.format(
              this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateGregorian
            );
          }
          if (typeof this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateHijri !==
            'string') {
            this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateHijri = this.generalService.formatHijriDate(
              this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateHijri
            );
          }
        }
      }

    }

    if (this.holdingQuantity) {
      this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails.splice(0, 1);
      if (this.holdingQuantityForm.valid) {
        const newHoldingQuantity = {
          _id: this.holdingQuantity._id,
          ...this.generalService.checkEmptyFields(this.holdingQuantityForm.value)
        };
        this.data.put(holdingQuantitiesApi, newHoldingQuantity).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/inventory/holdingQuantities']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.holdingQuantityForm.valid) {
        this.holdingQuantityForm.value.inventoryHoldingQuantitiesDetails.splice(0, 1);
        const formValue = {
          ...this.generalService.checkEmptyFields(this.holdingQuantityForm.value)
        };
        this.subscriptions.push(
          this.data.post(holdingQuantitiesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.holdingQuantityForm.reset();
            this.holdingQuantityForm.patchValue({
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let inventoryHoldingQuantitiesStatus = 'unposted';
    let inventoryHoldingQuantitiesDescriptionAr = '';
    let inventoryHoldingQuantitiesDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let warehouseId = this.inventorySettings.branch.warehouseId;
    let referenceNumber = '';
    let transactionSourceType = 'without';
    let sourceNumberId = '';
    let customerId = '';
    let variablesArray = new FormArray([]);
    let itemBatchDetailsArray = new FormArray([
      new FormGroup({
        batchNumber: new FormControl(''),
        quantity: new FormControl(null),
        batchDateGregorian: new FormControl(''),
        batchDateHijri: new FormControl(''),
        expiryDateGregorian: new FormControl(''),
        expiryDateHijri: new FormControl('')
      })
    ]);
    let inventoryHoldingQuantitiesDetailsArray = new FormArray([
      new FormGroup({
        barCode: new FormControl('', Validators.required),
        itemId: new FormControl('', Validators.required),
        variables: variablesArray,
        quantity: new FormControl(null, Validators.required),
        itemUnitId: new FormControl(''),
        warehouseBalance: new FormControl(null),
        warehouseId: new FormControl(null),
        descriptionAr: new FormControl(''),
        descriptionEn: new FormControl(''),
        itemBatchDetails: itemBatchDetailsArray
      })
    ]);
    let isActive = true;

    if (this.holdingQuantity) {
      code = this.holdingQuantity.code;
      inventoryHoldingQuantitiesStatus = this.holdingQuantity.inventoryHoldingQuantitiesStatus;
      inventoryHoldingQuantitiesDescriptionAr = this.holdingQuantity.inventoryHoldingQuantitiesDescriptionAr;
      inventoryHoldingQuantitiesDescriptionEn = this.holdingQuantity.inventoryHoldingQuantitiesDescriptionEn;
      gregorianDate = new Date(this.holdingQuantity.gregorianDate + 'UTC');
      hijriDate = this.holdingQuantity.hijriDate;
      warehouseId = this.holdingQuantity.warehouseId;
      transactionSourceType = this.holdingQuantity.transactionSourceType;
      customerId = this.holdingQuantity.customerId;
      sourceNumberId = this.holdingQuantity.sourceNumberId;
      referenceNumber = this.holdingQuantity.referenceNumber;
      isActive = this.holdingQuantity.isActive;
      inventoryHoldingQuantitiesDetailsArray = new FormArray([]);
      for (const control of this.holdingQuantity.inventoryHoldingQuantitiesDetails) {
        variablesArray = new FormArray([]);
        itemBatchDetailsArray = new FormArray([]);
        inventoryHoldingQuantitiesDetailsArray.push(
          new FormGroup({
            barCode: new FormControl('', Validators.required),
            itemId: new FormControl('', Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(null, Validators.required),
            itemUnitId: new FormControl(''),
            warehouseBalance: new FormControl(null),
            warehouseId: new FormControl(''),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
            itemBatchDetails: new FormArray([])
          })
        );
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        for (const item of control.itemBatchDetails) {
          itemBatchDetailsArray.push(
            new FormGroup({
              batchNumber: new FormControl(item.batchNumber),
              quantity: new FormControl(item.quantity),
              batchDateGregorian: new FormControl(new Date(item.batchDateGregorian + 'UTC')),
              batchDateHijri: new FormControl(item.batchDateHijri),
              expiryDateGregorian: new FormControl(new Date(item.expiryDateGregorian + 'UTC')),
              expiryDateHijri: new FormControl(item.expiryDateHijri)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        inventoryHoldingQuantitiesDetailsArray.push(
          new FormGroup({
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            warehouseBalance: new FormControl(control.warehouseBalance),
            warehouseId: new FormControl(control.warehouseId),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            itemBatchDetails: itemBatchDetailsArray
          })
        );
      }
    }
    this.holdingQuantityForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      inventoryHoldingQuantitiesStatus: new FormControl(inventoryHoldingQuantitiesStatus),
      inventoryHoldingQuantitiesDescriptionAr: new FormControl(inventoryHoldingQuantitiesDescriptionAr),
      inventoryHoldingQuantitiesDescriptionEn: new FormControl(inventoryHoldingQuantitiesDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      warehouseId: new FormControl(warehouseId, Validators.required),
      referenceNumber: new FormControl(referenceNumber),
      transactionSourceType: new FormControl(transactionSourceType),
      sourceNumberId: new FormControl(sourceNumberId),
      customerId: new FormControl(customerId, Validators.required),
      inventoryHoldingQuantitiesDetails: inventoryHoldingQuantitiesDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}


