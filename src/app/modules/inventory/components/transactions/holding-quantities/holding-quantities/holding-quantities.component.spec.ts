import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoldingQuantitiesComponent } from './holding-quantities.component';

describe('HoldingQuantitiesComponent', () => {
  let component: HoldingQuantitiesComponent;
  let fixture: ComponentFixture<HoldingQuantitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoldingQuantitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoldingQuantitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
