import { Component, OnInit, OnDestroy } from '@angular/core';
import { IHoldingQuantity } from 'src/app/modules/inventory/interfaces/inventory-transactions/holding-quantities/IHoldingQuantity';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, holdingQuantitiesApi, postHoldingQuantitiesApi, unPostHoldingQuantitiesApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
@Component({
  selector: 'app-holding-quantities',
  templateUrl: './holding-quantities.component.html',
  styleUrls: ['./holding-quantities.component.scss']
})
export class HoldingQuantitiesComponent implements OnInit, OnDestroy {
  holdingQuantities: IHoldingQuantity[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getHoldingQuantitiesFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(holdingQuantitiesApi, pageNumber).subscribe((res: IDataRes) => {
      this.holdingQuantities = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(holdingQuantitiesApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.holdingQuantities = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postHoldingQuantitiesApi}/${id}`, {}).subscribe(res => {
        this.holdingQuantities = this.generalService.changeStatus(this.holdingQuantities, id, 'posted', 'inventoryHoldingQuantitiesStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostHoldingQuantitiesApi}/${id}`, {}).subscribe(res => {
        this.holdingQuantities = this.generalService.changeStatus(this.holdingQuantities, id, 'unposted', 'inventoryHoldingQuantitiesStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getHoldingQuantitiesFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(holdingQuantitiesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.holdingQuantities = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryHoldingQuantitiesDescriptionAr: new FormControl(''),
      inventoryHoldingQuantitiesDescriptionEn: new FormControl('')
    });
  }

  deleteModal(request: IHoldingQuantity) {
    const initialState = {
      code: request.code,
      nameAr: request.inventoryHoldingQuantitiesDescriptionAr,
      nameEn: request.inventoryHoldingQuantitiesDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(request._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(holdingQuantitiesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.holdingQuantities = this.generalService.removeItem(this.holdingQuantities, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getHoldingQuantitiesFirstPage() {
    this.subscriptions.push(
      this.data.get(holdingQuantitiesApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.holdingQuantities = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

