import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRequestHoldingQuantitiesComponent } from './add-request-holding-quantities.component';

describe('AddRequestHoldingQuantitiesComponent', () => {
  let component: AddRequestHoldingQuantitiesComponent;
  let fixture: ComponentFixture<AddRequestHoldingQuantitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRequestHoldingQuantitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRequestHoldingQuantitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
