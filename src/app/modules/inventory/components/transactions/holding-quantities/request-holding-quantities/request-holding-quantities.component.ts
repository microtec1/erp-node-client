import { Component, OnInit, OnDestroy } from '@angular/core';
import { IRequestHoldingQuantity } from 'src/app/modules/inventory/interfaces/inventory-transactions/holding-quantities/IRequestHoldingQuantity';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, holdingQuantitiesRequestApi, postHoldingQuantitiesRequestApi, unPostHoldingQuantitiesRequestApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-request-holding-quantities',
  templateUrl: './request-holding-quantities.component.html',
  styleUrls: ['./request-holding-quantities.component.scss']
})
export class RequestHoldingQuantitiesComponent implements OnInit, OnDestroy {
  holdingQuantitiesRequests: IRequestHoldingQuantity[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getHoldingQuantitiesRequestsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(holdingQuantitiesRequestApi, pageNumber).subscribe((res: IDataRes) => {
      this.holdingQuantitiesRequests = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(holdingQuantitiesRequestApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.holdingQuantitiesRequests = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postHoldingQuantitiesRequestApi}/${id}`, {}).subscribe(res => {
        this.holdingQuantitiesRequests = this.generalService.changeStatus(this.holdingQuantitiesRequests, id, 'posted', 'inventoryHldQtyReqStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostHoldingQuantitiesRequestApi}/${id}`, {}).subscribe(res => {
        this.holdingQuantitiesRequests = this.generalService.changeStatus(this.holdingQuantitiesRequests, id, 'unposted', 'inventoryHldQtyReqStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getHoldingQuantitiesRequestsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(holdingQuantitiesRequestApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.holdingQuantitiesRequests = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryHldQtyReqDescriptionAr: new FormControl(''),
      inventoryHldQtyReqDescriptionEn: new FormControl('')
    });
  }

  deleteModal(request: IRequestHoldingQuantity) {
    const initialState = {
      code: request.code,
      nameAr: request.inventoryHldQtyReqDescriptionAr,
      nameEn: request.inventoryHldQtyReqDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(request._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(holdingQuantitiesRequestApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.holdingQuantitiesRequests = this.generalService.removeItem(this.holdingQuantitiesRequests, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getHoldingQuantitiesRequestsFirstPage() {
    this.subscriptions.push(
      this.data.get(holdingQuantitiesRequestApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.holdingQuantitiesRequests = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
