import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUnholdingQuantitiesComponent } from './add-unholding-quantities.component';

describe('AddUnholdingQuantitiesComponent', () => {
  let component: AddUnholdingQuantitiesComponent;
  let fixture: ComponentFixture<AddUnholdingQuantitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUnholdingQuantitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUnholdingQuantitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
