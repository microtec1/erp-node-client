import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IUnholdingQuantity } from 'src/app/modules/inventory/interfaces/inventory-transactions/holding-quantities/IUnholdingQuantity';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  warehousesApi,
  customersApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi,
  unHoldingQuantitiesApi,
  holdingQuantitiesApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IHoldingQuantity } from 'src/app/modules/inventory/interfaces/inventory-transactions/holding-quantities/IHoldingQuantity';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { BatchDataModalComponent } from 'src/app/common/components/batch-data-modal/batch-data-modal.component';

@Component({
  selector: 'app-add-unholding-quantities',
  templateUrl: './add-unholding-quantities.component.html',
  styleUrls: ['./add-unholding-quantities.component.scss']
})
export class AddUnholdingQuantitiesComponent implements OnInit, OnDestroy {
  unHoldingQuantityForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  unHoldingQuantity: IUnholdingQuantity;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;
  inventorySettings = JSON.parse(localStorage.getItem('inventorySettings'));


  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Holding Quantities
  holdingQuantities: IHoldingQuantity[] = [];
  holdingQuantitiesInputFocused: boolean;
  hasMoreHoldingQuantities: boolean;
  holdingQuantitiesCount: number;
  selectedHoldingQuantitiesPage = 1;
  holdingQuantitiesPagesNo: number;
  noHoldingQuantities: boolean;
  selectedHoldingQuantity: IHoldingQuantity;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(unHoldingQuantitiesApi, null, null, id).subscribe((unHoldingQuantity: IUnholdingQuantity) => {
              this.unHoldingQuantity = unHoldingQuantity;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.unHoldingQuantityForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          for (const item of res.results) {
            if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.getHoldingQuantities();
  }

  openBatchModal(id, formGroup: FormGroup, index) {
    if (id) {
      const initialState = {
        itemId: id,
        request: this.selectedHoldingQuantity ? this.selectedHoldingQuantity.inventoryHoldingQuantitiesDetails[index].itemBatchDetails : formGroup.value.itemBatchDetails
      };
      this.bsModalRef = this.modalService.show(BatchDataModalComponent, { initialState, class: 'big-modal' });
      this.subscriptions.push(
        this.bsModalRef.content.formValue.subscribe(formValue => {
          if (formValue) {
            const array = formGroup.controls.itemBatchDetails as FormArray;
            array.controls = [];
            for (const item of formValue.itemBatchDetails) {
              array.push(
                new FormGroup({
                  batchNumber: new FormControl(item.batchNumber),
                  quantity: new FormControl(item.quantity),
                  batchDateGregorian: new FormControl(item.batchDateGregorian),
                  batchDateHijri: new FormControl(item.batchDateHijri),
                  expiryDateGregorian: new FormControl(item.expiryDateGregorian),
                  expiryDateHijri: new FormControl(item.expiryDateHijri)
                })
              );
            }
            this.bsModalRef.hide();
          } else {
            this.bsModalRef.hide();
          }
        })
      );
    }
  }

  getHoldingQuantities(id?: string) {
    const searchValues: any = {
      inventoryHoldingQuantitiesStatus: 'posted'
    };
    if (id) {
      searchValues.customerId = id;
    }
    this.subscriptions.push(
      this.data
        .get(holdingQuantitiesApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.holdingQuantities = res.results;
          this.uiService.isLoading.next(false);
        })
    );
  }

  fillTable(value) {
    this.selectedHoldingQuantity = this.holdingQuantities.find(x => x._id === value);
    if (this.selectedHoldingQuantity) {
      this.getInventoryUnHoldingQuantitiesDetailsArray.controls = [];
      for (const control of this.selectedHoldingQuantity.inventoryHoldingQuantitiesDetails) {
        const variablesArray = new FormArray([]);
        const itemBatchDetailsArray = new FormArray([]);
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        for (const item of control.itemBatchDetails) {
          itemBatchDetailsArray.push(
            new FormGroup({
              batchNumber: new FormControl(item.batchNumber),
              quantity: new FormControl(item.quantity),
              batchDateGregorian: new FormControl(item.batchDateGregorian),
              batchDateHijri: new FormControl(item.batchDateHijri),
              expiryDateGregorian: new FormControl(item.expiryDateGregorian),
              expiryDateHijri: new FormControl(item.expiryDateHijri)
            })
          );
        }
        this.getInventoryUnHoldingQuantitiesDetailsArray.push(
          new FormGroup({
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            warehouseBalance: new FormControl(control.warehouseBalance),
            warehouseId: new FormControl(control.warehouseId),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            itemBatchDetails: itemBatchDetailsArray
          })
        );
      }
    }
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              const itemsArray = [];
              for (const item of res.results) {
                if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                  itemsArray.push(item);
                }
              }
              for (const item of itemsArray) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
            const itemsArray = [];
            for (const item of res.results) {
              if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                itemsArray.push(item);
              }
            }
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(
                  x => x._id !== item._id
                );
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  checkBatch(id) {
    if (id) {
      const item = this.storeItems.find(x => x._id === id);
      if (item) {
        if (item.otherInformation.workWithBatch) {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  fillRow(item, formGroup) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    for (const x of item.barCode.variables) {
      variablesControl.push(
        new FormGroup({
          variableId: new FormControl(x.variableId),
          itemVariableNameId: new FormControl(x.itemVariableNameId)
        })
      );
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
    formGroup.patchValue({
      barCode: item.barCode.barCode,
      itemUnitId: item.barCode.unitId
    });
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getInventoryUnHoldingQuantitiesDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.unHoldingQuantityForm.controls;
  }

  get getInventoryUnHoldingQuantitiesDetailsArray() {
    return this.unHoldingQuantityForm.get('inventoryUnHoldingQuantitiesDetails') as FormArray;
  }

  deleteItem(index) {
    const control = this.unHoldingQuantityForm.get('inventoryUnHoldingQuantitiesDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.unHoldingQuantityForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.unHoldingQuantityForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.unHoldingQuantityForm.value.gregorianDate !== 'string') {
      this.unHoldingQuantityForm.value.gregorianDate =
        this.generalService.format(this.unHoldingQuantityForm.value.gregorianDate);
    }
    if (typeof this.unHoldingQuantityForm.value.hijriDate !== 'string') {
      this.unHoldingQuantityForm.value.hijriDate =
        this.generalService.formatHijriDate(this.unHoldingQuantityForm.value.hijriDate);
    }
    for (
      let i = 0;
      i <= this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails.length - 1;
      i++
    ) {

      for (
        let x = 0;
        x <= this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails.length - 1;
        x++
      ) {
        if (this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateGregorian) {
          if (
            typeof this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateGregorian !==
            'string'
          ) {
            this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateGregorian = this.generalService.format(
              this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateGregorian
            );
          }
          if (typeof this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateHijri !==
            'string') {
            this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateHijri = this.generalService.formatHijriDate(
              this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].batchDateHijri
            );
          }
        }
        if (this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateGregorian) {
          if (
            typeof this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateGregorian !==
            'string'
          ) {
            this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateGregorian = this.generalService.format(
              this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateGregorian
            );
          }
          if (typeof this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateHijri !==
            'string') {
            this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateHijri = this.generalService.formatHijriDate(
              this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails[i].itemBatchDetails[x].expiryDateHijri
            );
          }
        }
      }

    }

    if (this.unHoldingQuantity) {
      this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails.splice(0, 1);
      if (this.unHoldingQuantityForm.valid) {
        const newHoldingQuantity = {
          _id: this.unHoldingQuantity._id,
          ...this.generalService.checkEmptyFields(this.unHoldingQuantityForm.value)
        };
        this.data.put(holdingQuantitiesApi, newHoldingQuantity).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/inventory/unholdingQuantities']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.unHoldingQuantityForm.valid) {
        this.unHoldingQuantityForm.value.inventoryUnHoldingQuantitiesDetails.splice(0, 1);
        const formValue = {
          ...this.generalService.checkEmptyFields(this.unHoldingQuantityForm.value)
        };
        this.subscriptions.push(
          this.data.post(holdingQuantitiesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.unHoldingQuantityForm.reset();
            this.unHoldingQuantityForm.patchValue({
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let inventoryUnHoldingQuantitiesStatus = 'unposted';
    let inventoryUnHoldingQuantitiesDescriptionAr = '';
    let inventoryUnHoldingQuantitiesDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let warehouseId = this.inventorySettings.branch.warehouseId;
    let referenceNumber = '';
    let transactionSourceType = 'holdingQuantities';
    let sourceNumberId = '';
    let customerId = '';
    let variablesArray = new FormArray([]);
    let itemBatchDetailsArray = new FormArray([
      new FormGroup({
        batchNumber: new FormControl(''),
        quantity: new FormControl(null),
        batchDateGregorian: new FormControl(''),
        batchDateHijri: new FormControl(''),
        expiryDateGregorian: new FormControl(''),
        expiryDateHijri: new FormControl('')
      })
    ]);
    let inventoryUnHoldingQuantitiesDetailsArray = new FormArray([]);
    let isActive = true;

    if (this.unHoldingQuantity) {
      code = this.unHoldingQuantity.code;
      inventoryUnHoldingQuantitiesStatus = this.unHoldingQuantity.inventoryUnHoldingQuantitiesStatus;
      inventoryUnHoldingQuantitiesDescriptionAr = this.unHoldingQuantity.inventoryUnHoldingQuantitiesDescriptionAr;
      inventoryUnHoldingQuantitiesDescriptionEn = this.unHoldingQuantity.inventoryUnHoldingQuantitiesDescriptionEn;
      gregorianDate = new Date(this.unHoldingQuantity.gregorianDate + 'UTC');
      hijriDate = this.unHoldingQuantity.hijriDate;
      warehouseId = this.unHoldingQuantity.warehouseId;
      transactionSourceType = this.unHoldingQuantity.transactionSourceType;
      customerId = this.unHoldingQuantity.customerId;
      sourceNumberId = this.unHoldingQuantity.sourceNumberId;
      referenceNumber = this.unHoldingQuantity.referenceNumber;
      isActive = this.unHoldingQuantity.isActive;
      inventoryUnHoldingQuantitiesDetailsArray = new FormArray([]);
      for (const control of this.unHoldingQuantity.inventoryUnHoldingQuantitiesDetails) {
        variablesArray = new FormArray([]);
        itemBatchDetailsArray = new FormArray([]);
        inventoryUnHoldingQuantitiesDetailsArray.push(
          new FormGroup({
            barCode: new FormControl('', Validators.required),
            itemId: new FormControl('', Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(null, Validators.required),
            itemUnitId: new FormControl(''),
            warehouseBalance: new FormControl(null),
            warehouseId: new FormControl(''),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
            itemBatchDetails: new FormArray([])
          })
        );
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        for (const item of control.itemBatchDetails) {
          itemBatchDetailsArray.push(
            new FormGroup({
              batchNumber: new FormControl(item.batchNumber),
              quantity: new FormControl(item.quantity),
              batchDateGregorian: new FormControl(new Date(item.batchDateGregorian + 'UTC')),
              batchDateHijri: new FormControl(item.batchDateHijri),
              expiryDateGregorian: new FormControl(new Date(item.expiryDateGregorian + 'UTC')),
              expiryDateHijri: new FormControl(item.expiryDateHijri)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        inventoryUnHoldingQuantitiesDetailsArray.push(
          new FormGroup({
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            warehouseBalance: new FormControl(control.warehouseBalance),
            warehouseId: new FormControl(control.warehouseId),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            itemBatchDetails: itemBatchDetailsArray
          })
        );
      }
    }
    this.unHoldingQuantityForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      inventoryUnHoldingQuantitiesStatus: new FormControl(inventoryUnHoldingQuantitiesStatus),
      inventoryUnHoldingQuantitiesDescriptionAr: new FormControl(inventoryUnHoldingQuantitiesDescriptionAr),
      inventoryUnHoldingQuantitiesDescriptionEn: new FormControl(inventoryUnHoldingQuantitiesDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      warehouseId: new FormControl(warehouseId, Validators.required),
      referenceNumber: new FormControl(referenceNumber),
      transactionSourceType: new FormControl(transactionSourceType),
      sourceNumberId: new FormControl(sourceNumberId),
      customerId: new FormControl(customerId, Validators.required),
      inventoryUnHoldingQuantitiesDetails: inventoryUnHoldingQuantitiesDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
