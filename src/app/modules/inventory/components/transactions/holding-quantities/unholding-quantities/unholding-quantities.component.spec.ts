import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnholdingQuantitiesComponent } from './unholding-quantities.component';

describe('UnholdingQuantitiesComponent', () => {
  let component: UnholdingQuantitiesComponent;
  let fixture: ComponentFixture<UnholdingQuantitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnholdingQuantitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnholdingQuantitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
