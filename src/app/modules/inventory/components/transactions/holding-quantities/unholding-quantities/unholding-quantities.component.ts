import { Component, OnInit, OnDestroy } from '@angular/core';
import { IUnholdingQuantity } from 'src/app/modules/inventory/interfaces/inventory-transactions/holding-quantities/IUnholdingQuantity';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, unHoldingQuantitiesApi, postUnHoldingQuantitiesApi, unPostUnHoldingQuantitiesApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-unholding-quantities',
  templateUrl: './unholding-quantities.component.html',
  styleUrls: ['./unholding-quantities.component.scss']
})
export class UnholdingQuantitiesComponent implements OnInit, OnDestroy {
  unHoldingQuantitiesRequests: IUnholdingQuantity[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getunHoldingQuantitiesRequestsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(unHoldingQuantitiesApi, pageNumber).subscribe((res: IDataRes) => {
      this.unHoldingQuantitiesRequests = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(unHoldingQuantitiesApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.unHoldingQuantitiesRequests = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postUnHoldingQuantitiesApi}/${id}`, {}).subscribe(res => {
        this.unHoldingQuantitiesRequests = this.generalService.changeStatus(this.unHoldingQuantitiesRequests, id, 'posted', 'inventoryHoldingQuantitiesStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostUnHoldingQuantitiesApi}/${id}`, {}).subscribe(res => {
        this.unHoldingQuantitiesRequests = this.generalService.changeStatus(this.unHoldingQuantitiesRequests, id, 'unposted', 'inventoryHoldingQuantitiesStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getunHoldingQuantitiesRequestsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(unHoldingQuantitiesApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.unHoldingQuantitiesRequests = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryUnHoldingQuantitiesDescriptionAr: new FormControl(''),
      inventoryUnHoldingQuantitiesDescriptionEn: new FormControl('')
    });
  }

  deleteModal(request: IUnholdingQuantity) {
    const initialState = {
      code: request.code,
      nameAr: request.inventoryUnHoldingQuantitiesDescriptionAr,
      nameEn: request.inventoryUnHoldingQuantitiesDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(request._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(unHoldingQuantitiesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.unHoldingQuantitiesRequests = this.generalService.removeItem(this.unHoldingQuantitiesRequests, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getunHoldingQuantitiesRequestsFirstPage() {
    this.subscriptions.push(
      this.data.get(unHoldingQuantitiesApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.unHoldingQuantitiesRequests = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
