import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { searchLength } from 'src/app/common/constants/general.constants';
import { IItemsGroup } from '../../../interfaces/IItemsGroup';
import { IItemsType } from '../../../interfaces/IItemsType';
import { IItemsClassification } from '../../../interfaces/IItemsClassification';
import { IItemsFamily } from '../../../interfaces/IItemsFamily';
import { IItemsUnit } from '../../../interfaces/IItemsUnit';
import { itemsGroupsApi, itemsTypesApi, itemsClassificationsApi, itemsFamiliesApi, itemsUnitsApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-inventory-filteration',
  templateUrl: './inventory-filteration.component.html',
  styleUrls: ['./inventory-filteration.component.scss']
})
export class InventoryFilterationComponent implements OnInit, OnDestroy {
  filterationForm: FormGroup;
  subscriptions: Subscription[] = [];
  showFilteration: boolean;
  @Output() formSubmitted: EventEmitter<any> = new EventEmitter();

  // Items Groups
  itemsGroups: IItemsGroup[] = [];
  itemsGroupsInputFocused: boolean;
  hasMoreItemsGroups: boolean;
  itemsGroupsCount: number;
  selectedItemsGroupsPage = 1;
  itemsGroupsPagesNo: number;
  noItemsGroups: boolean;

  // Items Types
  itemsTypes: IItemsType[] = [];
  itemsTypesInputFocused: boolean;
  hasMoreItemsTypes: boolean;
  itemsTypesCount: number;
  selectedItemsTypesPage = 1;
  itemsTypesPagesNo: number;
  noItemsTypes: boolean;


  // Items Classifications
  itemsClassifications: IItemsClassification[] = [];
  itemsClassificationsInputFocused: boolean;
  itemsClassificationsInputFocused2: boolean;
  hasMoreItemsClassifications: boolean;
  itemsClassificationsCount: number;
  selectedItemsClassificationsPage = 1;
  itemsClassificationsPagesNo: number;
  noItemsClassifications: boolean;

  // Items Families
  itemsFamilies: IItemsFamily[] = [];
  itemsFamiliesInputFocused: boolean;
  hasMoreItemsFamilies: boolean;
  itemsFamiliesCount: number;
  selectedItemsFamiliesPage = 1;
  itemsFamiliesPagesNo: number;
  noItemsFamilies: boolean;

  // Items Units
  itemsUnits: IItemsUnit[] = [];
  itemsUnitsInputFocused: boolean;
  hasMoreItemsUnits: boolean;
  itemsUnitsCount: number;
  selectedItemsUnitsPage = 1;
  itemsUnitsPagesNo: number;
  noItemsUnits: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.data.get(itemsGroupsApi, 1).subscribe((res: IDataRes) => {
        this.itemsGroupsPagesNo = res.pages;
        this.itemsGroupsCount = res.count;
        if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
          this.hasMoreItemsGroups = true;
        }
        this.itemsGroups.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(itemsTypesApi, 1).subscribe((res: IDataRes) => {
        this.itemsTypesPagesNo = res.pages;
        this.itemsTypesCount = res.count;
        if (this.itemsTypesPagesNo > this.selectedItemsTypesPage) {
          this.hasMoreItemsTypes = true;
        }
        this.itemsTypes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(itemsClassificationsApi, 1).subscribe((res: IDataRes) => {
        this.itemsClassificationsPagesNo = res.pages;
        this.itemsClassificationsCount = res.count;
        if (this.itemsClassificationsPagesNo > this.selectedItemsClassificationsPage) {
          this.hasMoreItemsClassifications = true;
        }
        this.itemsClassifications.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(itemsFamiliesApi, 1).subscribe((res: IDataRes) => {
        this.itemsFamiliesPagesNo = res.pages;
        this.itemsFamiliesCount = res.count;
        if (this.itemsFamiliesPagesNo > this.selectedItemsFamiliesPage) {
          this.hasMoreItemsFamilies = true;
        }
        this.itemsFamilies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(itemsUnitsApi, 1).subscribe((res: IDataRes) => {
        this.itemsUnitsPagesNo = res.pages;
        this.itemsUnitsCount = res.count;
        if (this.itemsUnitsPagesNo > this.selectedItemsUnitsPage) {
          this.hasMoreItemsUnits = true;
        }
        this.itemsUnits.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.filteration();
  }

  private filteration() {
    this.filterationForm = new FormGroup({
      itemsNameAr: new FormControl(null),
      itemsGroupId: new FormControl(null),
      itemsFamilyId: new FormControl(null),
      itemsTypeId: new FormControl(null),
      itemsUnitId: new FormControl(null),
      itemsCategoryId: new FormControl(null),
      productFormation: new FormControl(null)
    });
  }

  clear() {
    this.filterationForm.reset();
    const formValue = this.generalService.checkEmptyFields(this.filterationForm.value);
    this.formSubmitted.emit(formValue);
  }

  filterChecks() {
    const formValue = this.generalService.checkEmptyFields(this.filterationForm.value);
    this.formSubmitted.emit(formValue);
  }

  searchItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      itemsGroupNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(itemsGroupsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noItemsGroups = true;
          } else {
            this.noItemsGroups = false;
            for (const item of res.results) {
              if (this.itemsGroups.length) {
                const uniqueitemsGroups = this.itemsGroups.filter(x => x._id !== item._id);
                this.itemsGroups = uniqueitemsGroups;
              }
              this.itemsGroups.push(item);
            }
          }
          this.itemsGroups = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreItemsGroups() {
    this.selectedItemsGroupsPage = this.selectedItemsGroupsPage + 1;
    this.subscriptions.push(
      this.data.get(itemsGroupsApi, this.selectedItemsGroupsPage).subscribe((res: IDataRes) => {
        if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
          this.hasMoreItemsGroups = true;
        } else {
          this.hasMoreItemsGroups = false;
        }
        for (const item of res.results) {
          if (this.itemsGroups.length) {
            const uniqueitemsGroups = this.itemsGroups.filter(x => x._id !== item._id);
            this.itemsGroups = uniqueitemsGroups;
          }
          this.itemsGroups.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchItemsTypes(event) {
    const searchValue = event;
    const searchQuery = {
      itemsTypeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(itemsTypesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noItemsTypes = true;
          } else {
            this.noItemsTypes = false;
            for (const item of res.results) {
              if (this.itemsTypes.length) {
                const uniqueitemsTypes = this.itemsTypes.filter(x => x._id !== item._id);
                this.itemsTypes = uniqueitemsTypes;
              }
              this.itemsTypes.push(item);
            }
          }
          this.itemsTypes = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreItemsTypes() {
    this.selectedItemsTypesPage = this.selectedItemsTypesPage + 1;
    this.subscriptions.push(
      this.data.get(itemsTypesApi, this.selectedItemsTypesPage).subscribe((res: IDataRes) => {
        if (this.itemsTypesPagesNo > this.selectedItemsTypesPage) {
          this.hasMoreItemsTypes = true;
        } else {
          this.hasMoreItemsTypes = false;
        }
        for (const item of res.results) {
          if (this.itemsTypes.length) {
            const uniqueitemsTypes = this.itemsTypes.filter(x => x._id !== item._id);
            this.itemsTypes = uniqueitemsTypes;
          }
          this.itemsTypes.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchItemsClassifications(event) {
    const searchValue = event;
    const searchQuery = {
      itemsClassificationNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(itemsClassificationsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noItemsClassifications = true;
          } else {
            this.noItemsClassifications = false;
            for (const item of res.results) {
              if (this.itemsClassifications.length) {
                const uniqueitemsClassifications = this.itemsClassifications.filter(x => x._id !== item._id);
                this.itemsClassifications = uniqueitemsClassifications;
              }
              this.itemsClassifications.push(item);
            }
          }
          this.itemsClassifications = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreItemsClassifications() {
    this.selectedItemsClassificationsPage = this.selectedItemsClassificationsPage + 1;
    this.subscriptions.push(
      this.data.get(itemsClassificationsApi, this.selectedItemsClassificationsPage).subscribe((res: IDataRes) => {
        if (this.itemsClassificationsPagesNo > this.selectedItemsClassificationsPage) {
          this.hasMoreItemsClassifications = true;
        } else {
          this.hasMoreItemsClassifications = false;
        }
        for (const item of res.results) {
          if (this.itemsClassifications.length) {
            const uniqueitemsClassifications = this.itemsClassifications.filter(x => x._id !== item._id);
            this.itemsClassifications = uniqueitemsClassifications;
          }
          this.itemsClassifications.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchItemsFamilies(event) {
    const searchValue = event;
    const searchQuery = {
      itemsFamilyNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(itemsFamiliesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noItemsFamilies = true;
          } else {
            this.noItemsFamilies = false;
            for (const item of res.results) {
              if (this.itemsFamilies.length) {
                const uniqueitemsFamilies = this.itemsFamilies.filter(x => x._id !== item._id);
                this.itemsFamilies = uniqueitemsFamilies;
              }
              this.itemsFamilies.push(item);
            }
          }
          this.itemsFamilies = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreItemsFamilies() {
    this.selectedItemsFamiliesPage = this.selectedItemsFamiliesPage + 1;
    this.subscriptions.push(
      this.data.get(itemsFamiliesApi, this.selectedItemsFamiliesPage).subscribe((res: IDataRes) => {
        if (this.itemsFamiliesPagesNo > this.selectedItemsFamiliesPage) {
          this.hasMoreItemsFamilies = true;
        } else {
          this.hasMoreItemsFamilies = false;
        }
        for (const item of res.results) {
          if (this.itemsFamilies.length) {
            const uniqueitemsFamilies = this.itemsFamilies.filter(x => x._id !== item._id);
            this.itemsFamilies = uniqueitemsFamilies;
          }
          this.itemsFamilies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchItemsUnits(event) {
    const searchValue = event;
    const searchQuery = {
      itemsUnitNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(itemsUnitsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noItemsUnits = true;
          } else {
            this.noItemsUnits = false;
            for (const item of res.results) {
              if (this.itemsUnits.length) {
                const uniqueitemsUnits = this.itemsUnits.filter(x => x._id !== item._id);
                this.itemsUnits = uniqueitemsUnits;
              }
              this.itemsUnits.push(item);
            }
          }
          this.itemsUnits = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreItemsUnits() {
    this.selectedItemsUnitsPage = this.selectedItemsUnitsPage + 1;
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, this.selectedItemsUnitsPage).subscribe((res: IDataRes) => {
        if (this.itemsUnitsPagesNo > this.selectedItemsUnitsPage) {
          this.hasMoreItemsUnits = true;
        } else {
          this.hasMoreItemsUnits = false;
        }
        for (const item of res.results) {
          if (this.itemsUnits.length) {
            const uniqueitemsUnits = this.itemsUnits.filter(x => x._id !== item._id);
            this.itemsUnits = uniqueitemsUnits;
          }
          this.itemsUnits.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
