import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInventoryOpeningBalanceComponent } from './add-inventory-opening-balance.component';

describe('AddInventoryOpeningBalanceComponent', () => {
  let component: AddInventoryOpeningBalanceComponent;
  let fixture: ComponentFixture<AddInventoryOpeningBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInventoryOpeningBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInventoryOpeningBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
