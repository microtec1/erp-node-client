import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryOpeningBalanceComponent } from './inventory-opening-balance.component';

describe('InventoryOpeningBalanceComponent', () => {
  let component: InventoryOpeningBalanceComponent;
  let fixture: ComponentFixture<InventoryOpeningBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryOpeningBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryOpeningBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
