import { Component, OnInit, OnDestroy } from '@angular/core';
import { IInventoryOpeningBalance } from '../../../interfaces/inventory-transactions/inventory-opening-balance/IInventoryOpeningBalance';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, inventoryOpeningBalanceApi, postInventoryOpeningBalanceApi, unPostInventoryOpeningBalanceApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-inventory-opening-balance',
  templateUrl: './inventory-opening-balance.component.html',
  styleUrls: ['./inventory-opening-balance.component.scss']
})
export class InventoryOpeningBalanceComponent implements OnInit, OnDestroy {
  openingBalance: IInventoryOpeningBalance[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getInventoryOpeningBalanceFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(inventoryOpeningBalanceApi, pageNumber).subscribe((res: IDataRes) => {
      this.openingBalance = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(inventoryOpeningBalanceApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.openingBalance = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postInventoryOpeningBalanceApi}/${id}`, {}).subscribe(res => {
        this.openingBalance = this.generalService.changeStatus(this.openingBalance, id, 'posted', 'inventoryOpeningBalanceStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostInventoryOpeningBalanceApi}/${id}`, {}).subscribe(res => {
        this.openingBalance = this.generalService.changeStatus(this.openingBalance, id, 'unposted', 'inventoryOpeningBalanceStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getInventoryOpeningBalanceFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(inventoryOpeningBalanceApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.openingBalance = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryOpeningBalanceDescriptionAr: new FormControl(''),
      inventoryOpeningBalanceDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IInventoryOpeningBalance) {
    const initialState = {
      code: item.code,
      nameAr: item.inventoryOpeningBalanceDescriptionAr,
      nameEn: item.inventoryOpeningBalanceDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(inventoryOpeningBalanceApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.openingBalance = this.generalService.removeItem(this.openingBalance, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getInventoryOpeningBalanceFirstPage() {
    this.subscriptions.push(
      this.data.get(inventoryOpeningBalanceApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.openingBalance = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
