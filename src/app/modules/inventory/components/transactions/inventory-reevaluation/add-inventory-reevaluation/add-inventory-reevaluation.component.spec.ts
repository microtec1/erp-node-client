import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInventoryReevaluationComponent } from './add-inventory-reevaluation.component';

describe('AddInventoryReevaluationComponent', () => {
  let component: AddInventoryReevaluationComponent;
  let fixture: ComponentFixture<AddInventoryReevaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInventoryReevaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInventoryReevaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
