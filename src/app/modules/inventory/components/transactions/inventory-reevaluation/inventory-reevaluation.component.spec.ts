import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryReevaluationComponent } from './inventory-reevaluation.component';

describe('InventoryReevaluationComponent', () => {
  let component: InventoryReevaluationComponent;
  let fixture: ComponentFixture<InventoryReevaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryReevaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryReevaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
