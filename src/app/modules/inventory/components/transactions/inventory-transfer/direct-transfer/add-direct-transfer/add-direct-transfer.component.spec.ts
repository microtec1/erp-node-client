import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDirectTransferComponent } from './add-direct-transfer.component';

describe('AddDirectTransferComponent', () => {
  let component: AddDirectTransferComponent;
  let fixture: ComponentFixture<AddDirectTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDirectTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDirectTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
