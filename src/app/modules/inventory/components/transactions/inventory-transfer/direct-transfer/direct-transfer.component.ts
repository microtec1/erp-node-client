import { Component, OnInit, OnDestroy } from '@angular/core';
import { IDirectTransfer } from 'src/app/modules/inventory/interfaces/inventory-transactions/inventory-transfer/IDirectTransfer';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, directTransferApi, postDirectTransferApi, unPostDirectTransferApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-direct-transfer',
  templateUrl: './direct-transfer.component.html',
  styleUrls: ['./direct-transfer.component.scss']
})
export class DirectTransferComponent implements OnInit, OnDestroy {
  directTransfers: IDirectTransfer[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getDirectTransfersFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(directTransferApi, pageNumber).subscribe((res: IDataRes) => {
      this.directTransfers = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(directTransferApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.directTransfers = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postDirectTransferApi}/${id}`, {}).subscribe(res => {
        this.directTransfers = this.generalService.changeStatus(this.directTransfers, id, 'posted', 'inventoryDirectTransferStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostDirectTransferApi}/${id}`, {}).subscribe(res => {
        this.directTransfers = this.generalService.changeStatus(this.directTransfers, id, 'unposted', 'inventoryDirectTransferStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getDirectTransfersFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(directTransferApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.directTransfers = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryDirectTransferDescriptionAr: new FormControl(''),
      inventoryDirectTransferDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IDirectTransfer) {
    const initialState = {
      code: item.code,
      nameAr: item.inventoryDirectTransferDescriptionAr,
      nameEn: item.inventoryDirectTransferDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(directTransferApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.directTransfers = this.generalService.removeItem(this.directTransfers, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getDirectTransfersFirstPage() {
    this.subscriptions.push(
      this.data.get(directTransferApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.directTransfers = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
