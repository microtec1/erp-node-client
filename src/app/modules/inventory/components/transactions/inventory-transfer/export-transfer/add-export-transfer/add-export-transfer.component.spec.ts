import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddExportTransferComponent } from './add-export-transfer.component';

describe('AddExportTransferComponent', () => {
  let component: AddExportTransferComponent;
  let fixture: ComponentFixture<AddExportTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddExportTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddExportTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
