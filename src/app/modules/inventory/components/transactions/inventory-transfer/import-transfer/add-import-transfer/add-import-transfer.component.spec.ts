import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddImportTransferComponent } from './add-import-transfer.component';

describe('AddImportTransferComponent', () => {
  let component: AddImportTransferComponent;
  let fixture: ComponentFixture<AddImportTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddImportTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddImportTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
