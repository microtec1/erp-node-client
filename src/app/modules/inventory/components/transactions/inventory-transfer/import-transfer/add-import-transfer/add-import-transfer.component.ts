import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IImportTransfer } from 'src/app/modules/inventory/interfaces/inventory-transactions/inventory-transfer/IImportTransfer';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  importTransferApi,
  warehousesApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi,
  exportTransferApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { ITransferRequest } from 'src/app/modules/inventory/interfaces/inventory-transactions/inventory-transfer/ITransferRequest';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { BatchDataModalComponent } from 'src/app/common/components/batch-data-modal/batch-data-modal.component';
import { IExportTransfer } from 'src/app/modules/inventory/interfaces/inventory-transactions/inventory-transfer/IExportTransfer';


@Component({
  selector: 'app-add-import-transfer',
  templateUrl: './add-import-transfer.component.html',
  styleUrls: ['./add-import-transfer.component.scss']
})
export class AddImportTransferComponent implements OnInit, OnDestroy {
  importTransferForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  importTransfer: IImportTransfer;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  warehousesInputFocused2: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Warehouses By Road
  warehousesByRoad: IWarehouse[] = [];
  warehousesByRoadInputFocused: boolean;
  hasMoreWarehousesByRoad: boolean;
  warehousesByRoadCount: number;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Export Transfers
  exportTransfers: IExportTransfer[] = [];
  exportTransfersInputFocused: boolean;
  hasMoreExportTransfers: boolean;
  exportTransfersCount: number;
  selectedExportTransfersPage = 1;
  exportTransfersPagesNo: number;
  noExportTransfers: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(importTransferApi, null, null, id).subscribe((importTransfer: IImportTransfer) => {
              this.importTransfer = importTransfer;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.importTransferForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          for (const item of res.results) {
            if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    const searchValues = {
      warehouseType: 'goodsByRoad'
    };

    this.subscriptions.push(
      this.data
        .get(warehousesApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.warehousesByRoadCount = res.count;
          this.warehousesByRoad = res.results;
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.getExportTransfers();

  }

  getExportTransfers() {
    const searchValues: any = {
      inventoryExportTransferStatus: 'posted'
    };
    this.subscriptions.push(
      this.data
        .get(exportTransferApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.exportTransfers = res.results;
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              const itemsArray = [];
              for (const item of res.results) {
                if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                  itemsArray.push(item);
                }
              }
              for (const item of itemsArray) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
            const itemsArray = [];
            for (const item of res.results) {
              if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                itemsArray.push(item);
              }
            }
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(
                  x => x._id !== item._id
                );
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.warehousesByRoad = this.warehouses.filter(x => x.warehouseType === 'goodsByRoad');
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  getItem(event, formGroup: FormGroup) {
    this.subscriptions.push(
      this.data.get(storesItemsApi, null, null, event).subscribe((res: any) => {
        if (res.barCode.length > 1) {
          const initialState = {
            barCodes: res.barCode
          };
          this.bsModalRef = this.modalService.show(BarcodeModalComponent, { initialState });
          this.subscriptions.push(
            this.bsModalRef.content.confirmed.subscribe(confirmed => {
              if (confirmed) {
                this.searchBarcodes(confirmed.barCode, formGroup);
                this.bsModalRef.hide();
              } else {
                this.bsModalRef.hide();
              }
            })
          );
        } else {
          this.searchBarcodes(res.barCode[0].barCode, formGroup);
        }
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  checkBatch(id) {
    if (id) {
      const item = this.storeItems.find(x => x._id === id);
      if (item) {
        if (item.otherInformation.workWithBatch) {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  openBatchModal(id, formGroup: FormGroup, index) {
    if (id) {
      const initialState = {
        itemId: id,
        request: this.importTransfer ? this.importTransfer.inventoryImportTransferDetails[index - 1].itemBatchDetails : formGroup.value.itemBatchDetails
      };
      this.bsModalRef = this.modalService.show(BatchDataModalComponent, { initialState, class: 'big-modal' });
      this.subscriptions.push(
        this.bsModalRef.content.formValue.subscribe(formValue => {
          if (formValue) {
            const array = formGroup.controls.itemBatchDetails as FormArray;
            array.controls = [];
            for (const item of formValue.itemBatchDetails) {
              array.push(
                new FormGroup({
                  batchNumber: new FormControl(item.batchNumber),
                  quantity: new FormControl(item.quantity),
                  batchDateGregorian: new FormControl(item.batchDateGregorian),
                  batchDateHijri: new FormControl(item.batchDateHijri),
                  expiryDateGregorian: new FormControl(item.expiryDateGregorian),
                  expiryDateHijri: new FormControl(item.expiryDateHijri)
                })
              );
            }
            this.bsModalRef.hide();
          } else {
            this.bsModalRef.hide();
          }
        })
      );
    }
  }

  fillRow(item, formGroup) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    for (const x of item.barCode.variables) {
      variablesControl.push(
        new FormGroup({
          variableId: new FormControl(x.variableId),
          itemVariableNameId: new FormControl(x.itemVariableNameId)
        })
      );
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
    formGroup.patchValue({
      barCode: item.barCode.barCode,
      itemUnitId: item.barCode.unitId
    });
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getInventoryImportTransferDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  fillRowWarehouse(event, fieldName) {
    for (const control of this.getInventoryImportTransferDetailsArray.controls) {
      control.patchValue({
        [fieldName]: event
      });
    }
  }

  calculateTotalCost(value, group: FormGroup) {
    // group.patchValue({
    //   totalCost: group.value.quantity * group.value.cost
    // });
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.importTransferForm.controls;
  }

  get getInventoryImportTransferDetailsArray() {
    return this.importTransferForm.get('inventoryImportTransferDetails') as FormArray;
  }

  addItem(group: FormGroup) {
    if (this.getInventoryImportTransferDetailsArray.valid) {
      const variablesArray = new FormArray([]);
      for (const item of group.value.variables) {
        variablesArray.push(
          new FormGroup({
            variableId: new FormControl(item.variableId),
            itemVariableNameId: new FormControl(item.itemVariableNameId)
          })
        );
      }

      const itemBatchDetailsArray = new FormArray([]);

      this.getInventoryImportTransferDetailsArray.push(
        new FormGroup({
          barCode: new FormControl(group.value.barCode, Validators.required),
          itemId: new FormControl(group.value.itemId, Validators.required),
          variables: variablesArray,
          quantity: new FormControl(group.value.quantity, Validators.required),
          itemUnitId: new FormControl(group.value.itemUnitId),
          cost: new FormControl(group.value.cost),
          totalCost: new FormControl(group.value.totalCost),
          receivedQuantity: new FormControl(group.value.receivedQuantity, Validators.required),
          fromWarehouseId: new FormControl(group.value.fromWarehouseId),
          toWarehouseId: new FormControl(group.value.toWarehouseId),
          descriptionAr: new FormControl(group.value.descriptionAr),
          descriptionEn: new FormControl(group.value.descriptionEn),
          itemBatchDetails: itemBatchDetailsArray
        })
      );
      group.reset();
      // group.updateValueAndValidity();
    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      this.uiService.isLoading.next(false);
    }

  }

  deleteItem(index) {
    const control = this.importTransferForm.get('inventoryImportTransferDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.importTransferForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.importTransferForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  validateField(value) {
    if (value) {
      if (value !== 'without') {
        this.importTransferForm.controls.sourceNumberId.setValidators(Validators.required);
        this.importTransferForm.controls.sourceNumberId.updateValueAndValidity();
      } else {
        this.importTransferForm.controls.sourceNumberId.clearValidators();
        this.importTransferForm.controls.sourceNumberId.updateValueAndValidity();
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    const detailsArray = (this.importTransferForm.get('inventoryImportTransferDetails') as FormArray).controls;
    if (detailsArray.length > 1) {
      (detailsArray[0] as FormGroup).controls.barCode.clearValidators();
      (detailsArray[0] as FormGroup).controls.barCode.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.itemId.clearValidators();
      (detailsArray[0] as FormGroup).controls.itemId.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.quantity.clearValidators();
      (detailsArray[0] as FormGroup).controls.quantity.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.receivedQuantity.clearValidators();
      (detailsArray[0] as FormGroup).controls.receivedQuantity.updateValueAndValidity();
    } else {
      this.submitted = false;
      this.loadingButton = false;
      this.uiService.showError('GENERAL.rowMustBeAdded', '');
      return;
    }

    if (typeof this.importTransferForm.value.gregorianDate !== 'string') {
      this.importTransferForm.value.gregorianDate =
        this.generalService.format(this.importTransferForm.value.gregorianDate);
    }
    if (typeof this.importTransferForm.value.hijriDate !== 'string') {
      this.importTransferForm.value.hijriDate =
        this.generalService.formatHijriDate(this.importTransferForm.value.hijriDate);
    }

    for (
      let i = 0;
      i <= this.importTransferForm.value.inventoryImportTransferDetails.length - 1;
      i++
    ) {

      for (
        let x = 0;
        x <= this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails.length - 1;
        x++
      ) {
        if (this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].batchDateGregorian) {
          if (
            typeof this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].batchDateGregorian !==
            'string'
          ) {
            this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].batchDateGregorian = this.generalService.format(
              this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].batchDateGregorian
            );
          }
          if (typeof this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].batchDateHijri !==
            'string') {
            this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].batchDateHijri = this.generalService.formatHijriDate(
              this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].batchDateHijri
            );
          }
        }
        if (this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].expiryDateGregorian) {
          if (
            typeof this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].expiryDateGregorian !==
            'string'
          ) {
            this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].expiryDateGregorian = this.generalService.format(
              this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].expiryDateGregorian
            );
          }
          if (typeof this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].expiryDateHijri !==
            'string') {
            this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].expiryDateHijri = this.generalService.formatHijriDate(
              this.importTransferForm.value.inventoryImportTransferDetails[i].itemBatchDetails[x].expiryDateHijri
            );
          }
        }
      }
    }

    if (this.importTransfer) {
      this.importTransferForm.value.inventoryImportTransferDetails.splice(0, 1);
      if (this.importTransferForm.valid) {
        const newimportTransfer = {
          _id: this.importTransfer._id,
          ...this.generalService.checkEmptyFields(this.importTransferForm.value)
        };
        this.data.put(importTransferApi, newimportTransfer).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/inventory/importTransfer']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.importTransferForm.valid) {
        this.importTransferForm.value.inventoryImportTransferDetails.splice(0, 1);
        const formValue = {
          ...this.generalService.checkEmptyFields(this.importTransferForm.value)
        };
        this.subscriptions.push(
          this.data.post(importTransferApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.importTransferForm.reset();
            this.importTransferForm.patchValue({
              isActive: true,
              transactionSourceType: 'exportTransfer',
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let inventoryImportTransferStatus = 'unposted';
    let inventoryImportTransferDescriptionAr = '';
    let inventoryImportTransferDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let fromWarehouseId = '';
    let toWarehouseId = '';
    let referenceNumber = '';
    let transactionSourceType = 'exportTransfer';
    let sourceNumberId = '';
    let variablesArray = new FormArray([]);
    let itemBatchDetailsArray = new FormArray([
      new FormGroup({
        batchNumber: new FormControl(''),
        quantity: new FormControl(null),
        batchDateGregorian: new FormControl(''),
        batchDateHijri: new FormControl(''),
        expiryDateGregorian: new FormControl(''),
        expiryDateHijri: new FormControl('')
      })
    ]);
    let inventoryImportTransferDetailsArray = new FormArray([
      new FormGroup({
        barCode: new FormControl('', Validators.required),
        itemId: new FormControl('', Validators.required),
        variables: variablesArray,
        quantity: new FormControl(null, Validators.required),
        itemUnitId: new FormControl(''),
        cost: new FormControl(0),
        totalCost: new FormControl(0),
        receivedQuantity: new FormControl(null, Validators.required),
        fromWarehouseId: new FormControl(''),
        toWarehouseId: new FormControl(''),
        descriptionAr: new FormControl(''),
        descriptionEn: new FormControl(''),
        itemBatchDetails: itemBatchDetailsArray
      })
    ]);
    let isActive = true;

    if (this.importTransfer) {
      code = this.importTransfer.code;
      inventoryImportTransferStatus = this.importTransfer.inventoryImportTransferStatus;
      inventoryImportTransferDescriptionAr = this.importTransfer.inventoryImportTransferDescriptionAr;
      inventoryImportTransferDescriptionEn = this.importTransfer.inventoryImportTransferDescriptionEn;
      gregorianDate = new Date(this.importTransfer.gregorianDate + 'UTC');
      hijriDate = this.importTransfer.hijriDate;
      fromWarehouseId = this.importTransfer.fromWarehouseId;
      toWarehouseId = this.importTransfer.toWarehouseId;
      referenceNumber = this.importTransfer.referenceNumber;
      transactionSourceType = this.importTransfer.transactionSourceType;
      sourceNumberId = this.importTransfer.sourceNumberId;
      isActive = this.importTransfer.isActive;
      inventoryImportTransferDetailsArray = new FormArray([]);
      for (const control of this.importTransfer.inventoryImportTransferDetails) {
        variablesArray = new FormArray([]);
        itemBatchDetailsArray = new FormArray([]);
        inventoryImportTransferDetailsArray.push(
          new FormGroup({
            barCode: new FormControl('', Validators.required),
            itemId: new FormControl('', Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(null, Validators.required),
            itemUnitId: new FormControl(''),
            cost: new FormControl(0),
            totalCost: new FormControl(0),
            receivedQuantity: new FormControl(null, Validators.required),
            fromWarehouseId: new FormControl(''),
            toWarehouseId: new FormControl(''),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl('')
          })
        );
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        for (const item of control.itemBatchDetails) {
          itemBatchDetailsArray.push(
            new FormGroup({
              batchNumber: new FormControl(item.batchNumber),
              quantity: new FormControl(item.quantity),
              batchDateGregorian: new FormControl(new Date(item.batchDateGregorian + 'UTC')),
              batchDateHijri: new FormControl(item.batchDateHijri),
              expiryDateGregorian: new FormControl(new Date(item.expiryDateGregorian + 'UTC')),
              expiryDateHijri: new FormControl(item.expiryDateHijri)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        inventoryImportTransferDetailsArray.push(
          new FormGroup({
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(control.cost),
            totalCost: new FormControl(control.totalCost),
            receivedQuantity: new FormControl(control.receivedQuantity, Validators.required),
            fromWarehouseId: new FormControl(control.fromWarehouseId),
            toWarehouseId: new FormControl(control.toWarehouseId),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            itemBatchDetails: itemBatchDetailsArray
          })
        );
      }
    }
    this.importTransferForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      inventoryImportTransferStatus: new FormControl(inventoryImportTransferStatus),
      inventoryImportTransferDescriptionAr: new FormControl(inventoryImportTransferDescriptionAr),
      inventoryImportTransferDescriptionEn: new FormControl(inventoryImportTransferDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      fromWarehouseId: new FormControl(fromWarehouseId, Validators.required),
      toWarehouseId: new FormControl(toWarehouseId, Validators.required),
      referenceNumber: new FormControl(referenceNumber),
      transactionSourceType: new FormControl(transactionSourceType),
      sourceNumberId: new FormControl(sourceNumberId),
      inventoryImportTransferDetails: inventoryImportTransferDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
