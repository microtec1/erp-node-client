import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportTransferComponent } from './import-transfer.component';

describe('ImportTransferComponent', () => {
  let component: ImportTransferComponent;
  let fixture: ComponentFixture<ImportTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
