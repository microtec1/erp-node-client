import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTransferRequestComponent } from './add-transfer-request.component';

describe('AddTransferRequestComponent', () => {
  let component: AddTransferRequestComponent;
  let fixture: ComponentFixture<AddTransferRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTransferRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTransferRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
