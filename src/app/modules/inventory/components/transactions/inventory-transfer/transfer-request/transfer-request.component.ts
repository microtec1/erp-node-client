import { Component, OnInit, OnDestroy } from '@angular/core';
import { ITransferRequest } from 'src/app/modules/inventory/interfaces/inventory-transactions/inventory-transfer/ITransferRequest';
import { transferRequestApi, baseUrl, postTransferRequestApi, unPostTransferRequestApi } from 'src/app/common/constants/api.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-transfer-request',
  templateUrl: './transfer-request.component.html',
  styleUrls: ['./transfer-request.component.scss']
})
export class TransferRequestComponent implements OnInit, OnDestroy {
  transfersRequests: ITransferRequest[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getTransfersRequestsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(transferRequestApi, pageNumber).subscribe((res: IDataRes) => {
      this.transfersRequests = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(transferRequestApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.transfersRequests = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postTransferRequestApi}/${id}`, {}).subscribe(res => {
        this.transfersRequests = this.generalService.changeStatus(this.transfersRequests, id, 'posted', 'inventoryTransferRequestStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostTransferRequestApi}/${id}`, {}).subscribe(res => {
        this.transfersRequests = this.generalService.changeStatus(this.transfersRequests, id, 'unposted', 'inventoryTransferRequestStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getTransfersRequestsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(transferRequestApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.transfersRequests = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryTransferRequestDescriptionAr: new FormControl(''),
      inventoryTransferRequestDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: ITransferRequest) {
    const initialState = {
      code: item.code,
      nameAr: item.inventoryTransferRequestDescriptionAr,
      nameEn: item.inventoryTransferRequestDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(transferRequestApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.transfersRequests = this.generalService.removeItem(this.transfersRequests, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getTransfersRequestsFirstPage() {
    this.subscriptions.push(
      this.data.get(transferRequestApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.transfersRequests = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
