import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReceivingInventoryComponent } from './add-receiving-inventory.component';

describe('AddReceivingInventoryComponent', () => {
  let component: AddReceivingInventoryComponent;
  let fixture: ComponentFixture<AddReceivingInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReceivingInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReceivingInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
