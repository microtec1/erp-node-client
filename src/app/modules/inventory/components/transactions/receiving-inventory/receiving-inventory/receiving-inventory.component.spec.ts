import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivingInventoryComponent } from './receiving-inventory.component';

describe('ReceivingInventoryComponent', () => {
  let component: ReceivingInventoryComponent;
  let fixture: ComponentFixture<ReceivingInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivingInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivingInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
