import { Component, OnInit, OnDestroy } from '@angular/core';
import { IReceivingInventory } from 'src/app/modules/inventory/interfaces/inventory-transactions/receiving-inventory/receiving-inventory/IReceivingInventory';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, receivingInventoryApi, postReceivingInventoryApi, unPostReceivingInventoryApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-receiving-inventory',
  templateUrl: './receiving-inventory.component.html',
  styleUrls: ['./receiving-inventory.component.scss']
})
export class ReceivingInventoryComponent implements OnInit, OnDestroy {
  receivingInventory: IReceivingInventory[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getreceivingInventoryFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(receivingInventoryApi, pageNumber).subscribe((res: IDataRes) => {
      this.receivingInventory = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(receivingInventoryApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.receivingInventory = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postReceivingInventoryApi}/${id}`, {}).subscribe(res => {
        this.receivingInventory = this.generalService.changeStatus(this.receivingInventory, id, 'posted', 'receivingInventoryStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostReceivingInventoryApi}/${id}`, {}).subscribe(res => {
        this.receivingInventory = this.generalService.changeStatus(this.receivingInventory, id, 'unposted', 'receivingInventoryStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getreceivingInventoryFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(receivingInventoryApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.receivingInventory = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      receivingInventoryDescriptionAr: new FormControl(''),
      receivingInventoryDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IReceivingInventory) {
    const initialState = {
      code: item.code,
      nameAr: item.receivingInventoryDescriptionAr,
      nameEn: item.receivingInventoryDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(receivingInventoryApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.receivingInventory = this.generalService.removeItem(this.receivingInventory, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getreceivingInventoryFirstPage() {
    this.subscriptions.push(
      this.data.get(receivingInventoryApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.receivingInventory = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
