import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCountingReportComponent } from './add-counting-report.component';

describe('AddCountingReportComponent', () => {
  let component: AddCountingReportComponent;
  let fixture: ComponentFixture<AddCountingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCountingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCountingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
