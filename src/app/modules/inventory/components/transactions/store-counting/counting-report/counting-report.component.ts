import { Component, OnInit, OnDestroy } from '@angular/core';
import { ICountingReport } from 'src/app/modules/inventory/interfaces/inventory-transactions/store-counting/ICountingReport';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, countingReportApi, postCountingReportApi, unPostCountingReportApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-counting-report',
  templateUrl: './counting-report.component.html',
  styleUrls: ['./counting-report.component.scss']
})
export class CountingReportComponent implements OnInit, OnDestroy {
  countingReports: ICountingReport[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getCountingReportsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(countingReportApi, pageNumber).subscribe((res: IDataRes) => {
      this.countingReports = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(countingReportApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.countingReports = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postCountingReportApi}/${id}`, {}).subscribe(res => {
        this.countingReports = this.generalService.changeStatus(this.countingReports, id, 'posted', 'inventoryCountingReportStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostCountingReportApi}/${id}`, {}).subscribe(res => {
        this.countingReports = this.generalService.changeStatus(this.countingReports, id, 'unposted', 'inventoryCountingReportStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getCountingReportsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(countingReportApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.countingReports = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryCountingReportDescriptionAr: new FormControl(''),
      inventoryCountingReportDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: ICountingReport) {
    const initialState = {
      code: item.code,
      nameAr: item.inventoryCountingReportDescriptionAr,
      nameEn: item.inventoryCountingReportDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(countingReportApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.countingReports = this.generalService.removeItem(this.countingReports, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getCountingReportsFirstPage() {
    this.subscriptions.push(
      this.data.get(countingReportApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.countingReports = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
