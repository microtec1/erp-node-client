import { Component, OnInit, OnDestroy } from '@angular/core';
import { IInventoryAdjustment } from 'src/app/modules/inventory/interfaces/inventory-transactions/store-counting/IInventoryAdjustment';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  inventoryAdjustmentApi,
  warehousesApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi,
  countingReportApi,
  mergingCountingReportApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { BatchDataModalComponent } from 'src/app/common/components/batch-data-modal/batch-data-modal.component';

@Component({
  selector: 'app-add-inventory-adjustment',
  templateUrl: './add-inventory-adjustment.component.html',
  styleUrls: ['./add-inventory-adjustment.component.scss']
})
export class AddInventoryAdjustmentComponent implements OnInit, OnDestroy {
  inventoryAdjustmentForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  inventoryAdjustment: IInventoryAdjustment;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;
  inventorySettings = JSON.parse(localStorage.getItem('inventorySettings'));

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Source Items
  sourceItems: any[] = [];
  sourceItemsInputFocused: boolean;
  hasMoreSourceItems: boolean;
  sourceItemsCount: number;
  selectedSourceItemsPage = 1;
  sourceItemsPagesNo: number;
  noSourceItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(inventoryAdjustmentApi, null, null, id).subscribe((item: IInventoryAdjustment) => {
              this.inventoryAdjustment = item;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.inventoryAdjustmentForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          for (const item of res.results) {
            if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.getCountingReports();

  }

  getCountingReports() {
    const searchValues: any = {
      inventoryCountingReportStatus: 'posted'
    };
    this.subscriptions.push(
      this.data
        .get(countingReportApi, null, searchValues)
        .subscribe((res: IDataRes) => {

          this.sourceItems = res.results;
          console.log(this.sourceItems);
          this.uiService.isLoading.next(false);
        })
    );
  }

  getMergingCountings() {
    const searchValues: any = {
      inventoryMergingCountingReportStatus: 'posted'
    };
    this.subscriptions.push(
      this.data
        .get(mergingCountingReportApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              const itemsArray = [];
              for (const item of res.results) {
                if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                  itemsArray.push(item);
                }
              }
              for (const item of itemsArray) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
            const itemsArray = [];
            for (const item of res.results) {
              if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                itemsArray.push(item);
              }
            }
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(
                  x => x._id !== item._id
                );
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  getItem(event, formGroup: FormGroup) {
    this.subscriptions.push(
      this.data.get(storesItemsApi, null, null, event).subscribe((res: any) => {
        if (res.barCode.length > 1) {
          const initialState = {
            barCodes: res.barCode
          };
          this.bsModalRef = this.modalService.show(BarcodeModalComponent, { initialState });
          this.subscriptions.push(
            this.bsModalRef.content.confirmed.subscribe(confirmed => {
              if (confirmed) {
                this.searchBarcodes(confirmed.barCode, formGroup);
                this.bsModalRef.hide();
              } else {
                this.bsModalRef.hide();
              }
            })
          );
        } else {
          this.searchBarcodes(res.barCode[0].barCode, formGroup);
        }
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  checkBatch(id) {
    if (id) {
      const item = this.storeItems.find(x => x._id === id);
      if (item) {
        if (item.otherInformation.workWithBatch) {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  openBatchModal(id, formGroup: FormGroup, index) {
    if (id) {
      const initialState = {
        itemId: id,
        request: formGroup.value.itemBatchDetails
      };
      this.bsModalRef = this.modalService.show(BatchDataModalComponent, { initialState, class: 'big-modal' });
      this.subscriptions.push(
        this.bsModalRef.content.formValue.subscribe(formValue => {
          if (formValue) {
            const array = formGroup.controls.itemBatchDetails as FormArray;
            array.controls = [];
            for (const item of formValue.itemBatchDetails) {
              array.push(
                new FormGroup({
                  batchNumber: new FormControl(item.batchNumber),
                  quantity: new FormControl(item.quantity),
                  batchDateGregorian: new FormControl(item.batchDateGregorian),
                  batchDateHijri: new FormControl(item.batchDateHijri),
                  expiryDateGregorian: new FormControl(item.expiryDateGregorian),
                  expiryDateHijri: new FormControl(item.expiryDateHijri)
                })
              );
            }
            this.bsModalRef.hide();
          } else {
            this.bsModalRef.hide();
          }
        })
      );
    }
  }

  fillRow(item, formGroup, sourceType?) {
    console.log(item);

    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    if (sourceType) {
      for (const x of item.variables) {
        variablesControl.push(
          new FormGroup({
            variableId: new FormControl(x.variableId),
            itemVariableNameId: new FormControl(x.itemVariableNameId)
          })
        );
      }
    } else {
      for (const x of item.barCode.variables) {
        variablesControl.push(
          new FormGroup({
            variableId: new FormControl(x.variableId),
            itemVariableNameId: new FormControl(x.itemVariableNameId)
          })
        );
      }
      formGroup.patchValue({
        barCode: item.barCode.barCode,
        itemUnitId: item.barCode.unitId
      });
    }

    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getInventoryAdjustmentDetailsArray.controls[index].get('variables') as FormArray;

    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  fillRowWarehouse(event) {
    for (const control of this.getInventoryAdjustmentDetailsArray.controls) {
      control.patchValue({
        warehouseId: event
      });
    }
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.inventoryAdjustmentForm.controls;
  }

  get getInventoryAdjustmentDetailsArray() {
    return this.inventoryAdjustmentForm.get('inventoryAdjustmentDetails') as FormArray;
  }

  addItem(group: FormGroup) {
    if (this.getInventoryAdjustmentDetailsArray.valid) {
      const variablesArray = new FormArray([]);
      for (const item of group.value.variables) {
        variablesArray.push(
          new FormGroup({
            variableId: new FormControl(item.variableId),
            itemVariableNameId: new FormControl(item.itemVariableNameId)
          })
        );
      }

      const itemBatchDetailsArray = new FormArray([]);

      this.getInventoryAdjustmentDetailsArray.push(
        new FormGroup({
          barCode: new FormControl(group.value.barCode, Validators.required),
          itemId: new FormControl(group.value.itemId, Validators.required),
          variables: variablesArray,
          quantity: new FormControl(group.value.quantity, Validators.required),
          itemUnitId: new FormControl(group.value.itemUnitId),
          warehouseId: new FormControl(group.value.warehouseId),
          descriptionAr: new FormControl(group.value.descriptionAr),
          descriptionEn: new FormControl(group.value.descriptionEn),
          itemBatchDetails: itemBatchDetailsArray
        })
      );
      group.reset();
    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      this.uiService.isLoading.next(false);
    }

  }

  deleteItem(index) {
    const control = this.inventoryAdjustmentForm.get('inventoryAdjustmentDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.inventoryAdjustmentForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.inventoryAdjustmentForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  loadSourceItems(value) {
    if (value) {
      if (value === 'countingReport') {
        this.getCountingReports();
      } else {
        this.getMergingCountings();
      }
    }
  }

  fillTable(id) {
    const transaction = this.sourceItems.find(x => x._id === id);
    this.addPurchasingDetails(transaction);
  }

  addPurchasingDetails(item) {
    if (item) {
      for (const [index, control] of item.inventoryCountingReportDetails.entries()) {
        const itemBatchDetailsArray = new FormArray([]);

        for (const batch of control.itemBatchDetails) {
          itemBatchDetailsArray.push(
            new FormGroup({
              batchNumber: new FormControl(batch.batchNumber),
              quantity: new FormControl(batch.quantity),
              batchDateGregorian: new FormControl(new Date(batch.batchDateGregorian + 'UTC')),
              batchDateHijri: new FormControl(batch.batchDateHijri),
              expiryDateGregorian: new FormControl(new Date(batch.expiryDateGregorian + 'UTC')),
              expiryDateHijri: new FormControl(batch.expiryDateHijri)
            })
          );
        }
        this.getInventoryAdjustmentDetailsArray.push(
          new FormGroup({
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(control.quantity, Validators.required),
            systemQuantity: new FormControl(0, Validators.required),
            difference: new FormControl(0, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(0),
            totalCost: new FormControl(0),
            warehouseId: new FormControl(control.warehouseId),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
            itemBatchDetails: itemBatchDetailsArray
          })
        );
        this.fillRow(control, (this.getInventoryAdjustmentDetailsArray.controls[index] as FormGroup), 'sourceItem');
      }
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.inventoryAdjustmentForm.value.gregorianDate !== 'string') {
      this.inventoryAdjustmentForm.value.gregorianDate =
        this.generalService.format(this.inventoryAdjustmentForm.value.gregorianDate);
    }
    if (typeof this.inventoryAdjustmentForm.value.hijriDate !== 'string') {
      this.inventoryAdjustmentForm.value.hijriDate =
        this.generalService.formatHijriDate(this.inventoryAdjustmentForm.value.hijriDate);
    }
    for (
      let i = 0;
      i <= this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails.length - 1;
      i++
    ) {

      for (
        let x = 0;
        x <= this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails.length - 1;
        x++
      ) {
        if (this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].batchDateGregorian) {
          if (
            typeof this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].batchDateGregorian !==
            'string'
          ) {
            this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].batchDateGregorian = this.generalService.format(
              this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].batchDateGregorian
            );
          }
          if (typeof this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].batchDateHijri !==
            'string') {
            this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].batchDateHijri = this.generalService.formatHijriDate(
              this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].batchDateHijri
            );
          }
        }
        if (this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].expiryDateGregorian) {
          if (
            typeof this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].expiryDateGregorian !==
            'string'
          ) {
            this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].expiryDateGregorian = this.generalService.format(
              this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].expiryDateGregorian
            );
          }
          if (typeof this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].expiryDateHijri !==
            'string') {
            this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].expiryDateHijri = this.generalService.formatHijriDate(
              this.inventoryAdjustmentForm.value.inventoryAdjustmentDetails[i].itemBatchDetails[x].expiryDateHijri
            );
          }
        }
      }

    }

    if (this.inventoryAdjustment) {
      if (this.inventoryAdjustmentForm.valid) {
        const newinventoryAdjustment = {
          _id: this.inventoryAdjustment._id,
          ...this.generalService.checkEmptyFields(this.inventoryAdjustmentForm.value)
        };
        this.data.put(inventoryAdjustmentApi, newinventoryAdjustment).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/inventory/inventoryAdjustment']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.inventoryAdjustmentForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.inventoryAdjustmentForm.value)
        };
        this.subscriptions.push(
          this.data.post(inventoryAdjustmentApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.inventoryAdjustmentForm.reset();
            this.inventoryAdjustmentForm.patchValue({
              isActive: true
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let inventoryAdjustmentStatus = 'unposted';
    let inventoryAdjustmentDescriptionAr = '';
    let inventoryAdjustmentDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let warehouseId = this.inventorySettings.branch.warehouseId;
    let referenceNumber = '';
    let transactionSourceType = 'countingReport';
    let transactionSourceId = '';
    let variablesArray = new FormArray([]);
    let itemBatchDetailsArray = new FormArray([
      new FormGroup({
        batchNumber: new FormControl(''),
        quantity: new FormControl(null),
        batchDateGregorian: new FormControl(''),
        batchDateHijri: new FormControl(''),
        expiryDateGregorian: new FormControl(''),
        expiryDateHijri: new FormControl('')
      })
    ]);
    let inventoryAdjustmentDetailsArray = new FormArray([]);
    let isActive = true;

    if (this.inventoryAdjustment) {
      code = this.inventoryAdjustment.code;
      inventoryAdjustmentStatus = this.inventoryAdjustment.inventoryAdjustmentStatus;
      inventoryAdjustmentDescriptionAr = this.inventoryAdjustment.inventoryAdjustmentDescriptionAr;
      inventoryAdjustmentDescriptionEn = this.inventoryAdjustment.inventoryAdjustmentDescriptionEn;
      gregorianDate = new Date(this.inventoryAdjustment.gregorianDate + 'UTC');
      hijriDate = this.inventoryAdjustment.hijriDate;
      warehouseId = this.inventoryAdjustment.warehouseId;
      transactionSourceType = this.inventoryAdjustment.transactionSourceType;
      transactionSourceId = this.inventoryAdjustment.transactionSourceId;
      referenceNumber = this.inventoryAdjustment.referenceNumber;
      isActive = this.inventoryAdjustment.isActive;
      inventoryAdjustmentDetailsArray = new FormArray([]);
      for (const control of this.inventoryAdjustment.inventoryAdjustmentDetails) {
        variablesArray = new FormArray([]);
        itemBatchDetailsArray = new FormArray([]);
        inventoryAdjustmentDetailsArray.push(
          new FormGroup({
            barCode: new FormControl('', Validators.required),
            itemId: new FormControl('', Validators.required),
            itemsGroupId: new FormControl(''),
            variables: new FormArray([]),
            quantity: new FormControl(0, Validators.required),
            systemQuantity: new FormControl(0),
            difference: new FormControl(0),
            itemUnitId: new FormControl(''),
            cost: new FormControl(0),
            totalCost: new FormControl(0),
            warehouseId: new FormControl(null),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
            itemBatchDetails: new FormArray([])
          })
        );
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        for (const item of control.itemBatchDetails) {
          itemBatchDetailsArray.push(
            new FormGroup({
              batchNumber: new FormControl(item.batchNumber),
              quantity: new FormControl(item.quantity),
              batchDateGregorian: new FormControl(new Date(item.batchDateGregorian + 'UTC')),
              batchDateHijri: new FormControl(item.batchDateHijri),
              expiryDateGregorian: new FormControl(new Date(item.expiryDateGregorian + 'UTC')),
              expiryDateHijri: new FormControl(item.expiryDateHijri)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        inventoryAdjustmentDetailsArray.push(
          new FormGroup({
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            itemsGroupId: new FormControl(control.itemsGroupId),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            systemQuantity: new FormControl(control.systemQuantity),
            difference: new FormControl(control.difference),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(control.cost),
            totalCost: new FormControl(control.totalCost),
            warehouseId: new FormControl(control.warehouseId),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            itemBatchDetails: itemBatchDetailsArray
          })
        );
      }
    }
    this.inventoryAdjustmentForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      inventoryAdjustmentStatus: new FormControl(inventoryAdjustmentStatus),
      inventoryAdjustmentDescriptionAr: new FormControl(inventoryAdjustmentDescriptionAr),
      inventoryAdjustmentDescriptionEn: new FormControl(inventoryAdjustmentDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      warehouseId: new FormControl(warehouseId, Validators.required),
      referenceNumber: new FormControl(referenceNumber),
      transactionSourceType: new FormControl(transactionSourceType),
      transactionSourceId: new FormControl(transactionSourceId),
      inventoryAdjustmentDetails: inventoryAdjustmentDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
