import { Component, OnInit, OnDestroy } from '@angular/core';
import { IInventoryAdjustment } from 'src/app/modules/inventory/interfaces/inventory-transactions/store-counting/IInventoryAdjustment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, inventoryAdjustmentApi, postInventoryAdjustmentApi, unPostInventoryAdjustmentApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-inventory-adjustment',
  templateUrl: './inventory-adjustment.component.html',
  styleUrls: ['./inventory-adjustment.component.scss']
})
export class InventoryAdjustmentComponent implements OnInit, OnDestroy {
  inventoryAdjustments: IInventoryAdjustment[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getInventoryAdjustmentsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(inventoryAdjustmentApi, pageNumber).subscribe((res: IDataRes) => {
      this.inventoryAdjustments = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(inventoryAdjustmentApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.inventoryAdjustments = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postInventoryAdjustmentApi}/${id}`, {}).subscribe(res => {
        this.inventoryAdjustments = this.generalService.changeStatus(this.inventoryAdjustments, id, 'posted', 'inventoryAdjustmentStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostInventoryAdjustmentApi}/${id}`, {}).subscribe(res => {
        this.inventoryAdjustments = this.generalService.changeStatus(this.inventoryAdjustments, id, 'unposted', 'inventoryAdjustmentStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getInventoryAdjustmentsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(inventoryAdjustmentApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.inventoryAdjustments = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryAdjustmentDescriptionAr: new FormControl(''),
      inventoryAdjustmentDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IInventoryAdjustment) {
    const initialState = {
      code: item.code,
      nameAr: item.inventoryAdjustmentDescriptionAr,
      nameEn: item.inventoryAdjustmentDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(inventoryAdjustmentApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.inventoryAdjustments = this.generalService.removeItem(this.inventoryAdjustments, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getInventoryAdjustmentsFirstPage() {
    this.subscriptions.push(
      this.data.get(inventoryAdjustmentApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.inventoryAdjustments = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
