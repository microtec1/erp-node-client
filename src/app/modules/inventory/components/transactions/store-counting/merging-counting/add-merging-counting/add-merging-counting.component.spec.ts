import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMergingCountingComponent } from './add-merging-counting.component';

describe('AddMergingCountingComponent', () => {
  let component: AddMergingCountingComponent;
  let fixture: ComponentFixture<AddMergingCountingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMergingCountingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMergingCountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
