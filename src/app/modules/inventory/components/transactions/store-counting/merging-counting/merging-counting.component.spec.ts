import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergingCountingComponent } from './merging-counting.component';

describe('MergingCountingComponent', () => {
  let component: MergingCountingComponent;
  let fixture: ComponentFixture<MergingCountingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergingCountingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergingCountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
