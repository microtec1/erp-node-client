import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStoreCountingComponent } from './add-store-counting.component';

describe('AddStoreCountingComponent', () => {
  let component: AddStoreCountingComponent;
  let fixture: ComponentFixture<AddStoreCountingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddStoreCountingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStoreCountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
