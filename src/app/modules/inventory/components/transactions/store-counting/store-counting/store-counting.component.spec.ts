import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreCountingComponent } from './store-counting.component';

describe('StoreCountingComponent', () => {
  let component: StoreCountingComponent;
  let fixture: ComponentFixture<StoreCountingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreCountingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreCountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
