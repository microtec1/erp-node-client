export interface IInventorySettings {
  company: {
    saleMethod: string;
    activateLocation: boolean;
    showBarCode: boolean;
    automaticGenerateWarehouseCode: boolean;
    levelLength: number;
    workWithBatch: boolean;
    workWithBatchOptions: string;
  };
  branch: {
    branchId: string;
    warehouseId: string;
    itemType: string;
    storeConversionNeedsTransferRequestFromBranch: boolean;
  };
}
