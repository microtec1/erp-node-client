export interface IItemsClassification {
  _id: string;
  image: string;
  code: string;
  itemsClassificationNameAr: string;
  itemsClassificationNameEn?: string;
  isActive: boolean;
}
