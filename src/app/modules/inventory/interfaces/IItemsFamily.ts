export interface IItemsFamily {
  _id: string;
  image: string;
  code: string;
  itemsFamilyNameAr: string;
  itemsFamilyNameEn?: string;
  isActive: boolean;
}
