export interface IItemsGroup {
  _id: string;
  image: string;
  code: string;
  itemsGroupNameAr: string;
  itemsGroupNameEn?: string;
  accounts: {
    salesAccountId: string,
    salesRevenueAccountId: string,
    purchaseAccountId: string,
    purchaseRevenueAccountId: string,
    inventoryAccountId: string,
    goodsOnRoadAccountId: string,
    adjustmentByIncreaseAccountId: string,
    adjustmentByDecreaseAccountId: string,
    costAdjustmentByIncreaseAccountId: string,
    costAdjustmentByDecreaseAccountId: string,
    consignmentAccountId: string,
    giftsSamplesAccountId: string,
    displayGoodsAccountId: string,
    reservedGoodsAccountId: string,
    underExchangeGoodsAccountId: string,
    withoutInvoicesGoodsAccountId: string,
    outgoingGoodsAccountId: string,
    receivedGoodsAccountId: string,
    inventoryCostAccountId: string,
    inventoryDamageAccountId: string,
  };
  isActive: boolean;
}
