export interface IItemsSection {
  _id: string;
  image: string;
  code: string;
  itemsSectionNameAr: string;
  itemsSectionNameEn?: string;
  isActive: boolean;
  branchPrinters: [
    {
      branchId: string;
      printerPath: string;
    }
  ];
}
