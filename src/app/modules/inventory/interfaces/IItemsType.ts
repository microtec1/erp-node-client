export interface IItemsType {
  _id: string;
  image: string;
  code: string;
  itemsTypeNameAr: string;
  itemsTypeNameEn?: string;
  isActive: boolean;
}
