export interface IItemsUnit {
  _id: string;
  image: string;
  code: string;
  itemsUnitNameAr: string;
  itemsUnitNameEn?: string;
  isActive: boolean;
}
