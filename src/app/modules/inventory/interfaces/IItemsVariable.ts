export interface IItemsVariable {
  _id: string;
  image: string;
  code: string;
  itemsVariableNameAr: string;
  itemsVariableNameEn?: string;
  itemsVariables: [
    {
      _id: string;
      name: string;
    }
  ];
  isActive: boolean;
}
