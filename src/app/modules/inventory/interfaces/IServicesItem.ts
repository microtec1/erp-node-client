export interface IServicesItem {
  _id: string;
  image: string;
  code: string;
  servicesItemNameAr: string;
  servicesItemNameEn?: string;
  isActive: boolean;
  servicesItemGroupId: string;
  accounts: {
    salesAccountId: string;
    salesRevenueAccountId: string;
    purchaseAccountId: string;
    purchaseRevenueAccountId: string;
  };
}
