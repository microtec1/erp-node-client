export interface IServicesItemsGroup {
  _id: string;
  image: string;
  code: string;
  servicesItemsGroupNameAr: string;
  servicesItemsGroupNameEn?: string;
  isActive: boolean;
  accounts: {
    salesAccountId: string;
    salesRevenueAccountId: string;
    purchaseAccountId: string;
    purchaseRevenueAccountId: string;
  };
}
