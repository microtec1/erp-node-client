export interface IStoreItem {
  _id: string;
  image: string;
  code: string;
  itemsNameAr: string;
  itemsNameEn: string;
  itemsGroupId: string;
  itemsFamilyId: string;
  itemsTypeId: string;
  itemsUnitId: string;
  itemsCategoryId: string;
  otherInformation: {
    maximumDiscount: number;
    maximumDiscountValue: number;
    externalReference: string;
    initialDisassemblyPrice: number;
    lastPurchaseGregorianDate: string;
    lastPurchaseHijriDate: string;
    lastPurchasePrice: number;
    lastSaleGregorianDate: string;
    lastSaleHijriDate: string;
    lastSalePrice: number;
    defaultCost: number;
    warrantyPeriod: number;
    vatId: string;
    suppliers: [
      {
        supplierId: string
      }
    ];
    costCenterId: string;
    priceIncludesVAT: boolean;
    balanceCategory: boolean;
    nonReturnableItem: boolean;
    consignmentCategory: boolean;
    workWithBatch: boolean;
    workBatchType: string;
    productFormation: string;
  };
  accounts: {
    salesAccountId: string
    salesRevenueAccountId: string
    purchaseAccountId: string
    purchaseRevenueAccountId: string
    inventoryAccountId: string
    goodsOnRoadAccountId: string
    adjustmentByIncreaseAccountId: string
    adjustmentByDecreaseAccountId: string
    costAdjustmentByIncreaseAccountId: string
    costAdjustmentByDecreaseAccountId: string
    consignmentAccountId: string
    giftsSamplesAccountId: string
    displayGoodsAccountId: string
    reservedGoodsAccountId: string
    underExchangeGoodsAccountId: string
    withoutInvoicesGoodsAccountId: string
    outgoingGoodsAccountId: string
    receivedGoodsAccountId: string
    inventoryCostAccountId: string
    inventoryDamageAccountId: string
  };
  warehouses: [
    {
      warehouseId: string;
      reorderQuantity: number;
      maxQuantity: number;
      minQuantity: number;
    }
  ];
  alternativeItems: [
    {
      itemId: string;
    }
  ];
  additionalSpecifications: {
    specifications: [];
    foodAuthorityNumber: string;
    itemBtnColor: string;
  };
  itemDetails: [
    {
      _id: string,
      itemId: string,
      unitId: string,
      amount: number
    }
  ];
  relatedItemUnits: [
    {
      unitId: string;
      conversionFactor: number,
      isMain: boolean
    }
  ];
  barCode: [
    {
      barCode: string,
      unitId: string,
      conversionFactor: number,
      price: number,
      status: string;
      variables: [
        {
          variableId: string;
          itemVariableNameId: string;
        }
      ],
      discountPercentage: number,
      discountValue: number
    }
  ];
  itemBalance: [
    {
      barCode: [{
        barCode: string,
        unitId: string,
        price: number
      }],
      warehouseId: string,
      balance: number,
      averageCost: number
    }
  ];
  isActive: boolean;
}
