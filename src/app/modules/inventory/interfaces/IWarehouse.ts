export interface IWarehouse {
  _id: string;
  image: string;
  code: string;
  warehouseNameAr: string;
  warehouseNameEn: string;
  warehousePositionType: string;
  warehouseType: string;
  glAccountId: string;
  glAccountName: string;
  regionId: string;
  regionName: string;
  cityId: string;
  cityName: string;
  address: string;
  telephone: number;
  internalNumber: number;
  mobile: number;
  fax: number;
  mailBox: string;
  postalCode: string;
  email: string;
  responsiblePerson: string;
  isActive: boolean;
}
