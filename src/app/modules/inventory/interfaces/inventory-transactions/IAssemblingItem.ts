
export interface IAssemblingItem {
  _id: string;
  code: string;
  inventoryAssemblingItemsStatus: string;
  inventoryAssemblingItemsDescriptionAr: string;
  inventoryAssemblingItemsDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  isActive: boolean;
  inventoryAssemblingItemsDetails: [{
    barCode: string;
    itemId: string;
    variables: [{
      variableId: string;
      itemVariableNameId: string;
    }];
    quantity: number;
    itemUnitId: string;
    cost: number;
    warehouseId: string;
    locationId: string;
    descriptionEn: string;
    descriptionAr: string;
    itemBatchDetails: [{
      batchNumber: string;
      quantity: number;
      batchDateGregorian: string;
      batchDateHijri: string;
      expiryDateGregorian: string;
      expiryDateHijri: string;
    }],
    details: [{
      barCode: string;
      itemId: string;
      variables: [{
        variableId: string;
        itemVariableNameId: string;
      }];
      quantity: number;
      itemUnitId: string;
      cost: number;
      warehouseId: string;
      locationId: string;
      descriptionEn: string;
      descriptionAr: string;
      itemBatchDetails: [{
        batchNumber: string;
        quantity: number;
        batchDateGregorian: string;
        batchDateHijri: string;
        expiryDateGregorian: string;
        expiryDateHijri: string;
      }]
    }]
  }];
}
