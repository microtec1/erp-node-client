
export interface IDisassemblingItem {
  _id: string;
  code: string;
  inventoryDisassemblingItemsStatus: string;
  inventoryDisassemblingItemsDescriptionAr: string;
  inventoryDisassemblingItemsDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  isActive: boolean;
  inventoryDisassemblingItemsDetails: [{
    barCode: string;
    itemId: string;
    variables: [{
      variableId: string;
      itemVariableNameId: string;
    }];
    quantity: number;
    itemUnitId: string;
    cost: number;
    totalCost: number;
    warehouseId: string;
    locationId: string;
    descriptionEn: string;
    descriptionAr: string;
    itemBatchDetails: [{
      batchNumber: string;
      quantity: number;
      batchDateGregorian: string;
      batchDateHijri: string;
      expiryDateGregorian: string;
      expiryDateHijri: string;
    }],
    details: [{
      barCode: string;
      itemId: string;
      variables: [{
        variableId: string;
        itemVariableNameId: string;
      }];
      quantity: number;
      itemUnitId: string;
      cost: number;
      totalCost: number;
      warehouseId: string;
      locationId: string;
      descriptionEn: string;
      descriptionAr: string;
      itemBatchDetails: [{
        batchNumber: string;
        quantity: number;
        batchDateGregorian: string;
        batchDateHijri: string;
        expiryDateGregorian: string;
        expiryDateHijri: string;
      }]
    }]
  }];
}
