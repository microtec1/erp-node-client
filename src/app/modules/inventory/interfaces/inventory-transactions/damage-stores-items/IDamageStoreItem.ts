export interface IDamageStoreItem {
    _id: string;
    code: string;
    journalEntryId: string;
    inventoryDamageStoreItemsStatus: string;
    inventoryDamageStoreItemsDescriptionAr: string;
    inventoryDamageStoreItemsDescriptionEn: string;
    gregorianDate: string;
    hijriDate: string;
    warehouseId: string;
    referenceNumber: string;
    isActive: boolean;
    inventoryDamageStoreItemsDetails: [{
        barCode: string;
        itemId: string;
        itemsGroupId: string;
        variables: [{
            variableId: string,
            itemVariableNameId: string
        }];
        quantity: number;
        itemUnitId: string;
        cost: string;
        totalCost: string;
        warehouseId: string;
        locationId: string;
        descriptionAr: string;
        descriptionEn: string;
        itemBatchDetails: [{
            batchNumber: string;
            quantity: number;
            batchDateGregorian: string;
            batchDateHijri: string;
            expiryDateGregorian: string;
            expiryDateHijri: string;
        }]
    }];
}
