export interface IDeliveringInventory {
  _id: string;
  code: string;
  deliveringInventoryStatus: string;
  deliveringInventoryDescriptionAr: string;
  deliveringInventoryDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  transactionSourceType: string;
  sourceNumberId: string;
  exchangeTo: string;
  exchangeToId: string;
  isActive: boolean;
  deliveringInventoryDetails: [{
    barCode: string;
    itemId: string;
    variables: [{
      variableId: string,
      itemVariableNameId: string
    }];
    quantity: number;
    itemUnitId: string;
    cost: number;
    totalCost: number;
    warehouseId: string;
    locationId: string;
    descriptionAr: string;
    descriptionEn: string;
    itemBatchDetails: [{
      batchNumber: string;
      quantity: number;
      batchDateGregorian: string;
      batchDateHijri: string;
      expiryDateGregorian: string;
      expiryDateHijri: string;
    }]
  }];
}
