export interface IDeliveringInventoryRequest {
  _id: string;
  code: string;
  deliveringInventoryRequestStatus: string;
  deliveringInventoryRequestDescriptionAr: string;
  deliveringInventoryRequestDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  transactionSourceType: string;
  sourceNumberId: string;
  exchangeTo: string;
  exchangeToId: string;
  isActive: boolean;
  deliveringInventoryRequestDetails: [{
    barCode: string;
    itemId: string;
    variables: [{
      variableId: string,
      itemVariableNameId: string
    }];
    quantity: number;
    itemUnitId: string;
    warehouseId: string;
    descriptionAr: string;
    descriptionEn: string;
  }];
}
