export interface IRequestHoldingQuantity {
    _id: string;
    code: string;
    inventoryHldQtyReqStatus: string;
    inventoryHldQtyReqDescriptionAr: string;
    inventoryHldQtyReqDescriptionEn: string;
    gregorianDate: string;
    hijriDate: string;
    warehouseId: string;
    referenceNumber: string;
    transactionSourceType: string;
    sourceNumberId: string;
    customerId: string;
    inventoryHldQtyReqDetails: [
      {
        barCode: string;
        itemId: string;
        variables: [{
            variableId: string;
            itemVariableNameId: string;
        }];
        quantity: number;
        itemUnitId: string;
        warehouseBalance: number;
        warehouseId: string;
        locationId: string;
        descriptionAr: string;
        descriptionEn: string;
        itemBatchDetails: [{
            batchNumber: string;
            quantity: number;
            batchDateGregorian: string;
            batchDateHijri: string;
            expiryDateGregorian: string;
            expiryDateHijri: string;
        }]
    }];
    isActive: boolean;
}
