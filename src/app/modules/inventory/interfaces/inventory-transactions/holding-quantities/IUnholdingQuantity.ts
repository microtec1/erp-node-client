export interface IUnholdingQuantity {
  _id: string;
  image: string;
  code: string;
  inventoryUnHoldingQuantitiesStatus: string;
  inventoryUnHoldingQuantitiesDescriptionAr: string;
  inventoryUnHoldingQuantitiesDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  transactionSourceType: string;
  sourceNumberId: string;
  customerId: string;
  isActive: boolean;
  inventoryUnHoldingQuantitiesDetails: [{
    barCode: string;
    itemId: string;
    variables: [{
      variableId: string;
      itemVariableNameId: string;
    }];
    quantity: number;
    itemUnitId: string;
    warehouseBalance: number;
    warehouseId: string;
    locationId: string;
    descriptionAr: string;
    descriptionEn: string;
    itemBatchDetails: [{
      batchNumber: string;
      quantity: number;
      batchDateGregorian: string;
      batchDateHijri: string;
      expiryDateGregorian: string;
      expiryDateHijri: string;
    }]
  }];
}
