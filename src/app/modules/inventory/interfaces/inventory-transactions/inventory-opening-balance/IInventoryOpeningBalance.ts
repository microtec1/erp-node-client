
export interface IInventoryOpeningBalance {
    _id: string;
    code: string;
    inventoryOpeningBalanceStatus: string;
    inventoryOpeningBalanceDescriptionAr: string;
    inventoryOpeningBalanceDescriptionEn: string;
    gregorianDate: string;
    hijriDate: string;
    warehouseId: string;
    referenceNumber: string;
    isActive: boolean;
    inventoryOpeningBalanceDetails: [{
        barCode: string;
        itemId: string;
        variables: [{
            variableId: string;
            itemVariableNameId: string;
        }];
        quantity: number;
        itemUnitId: string;
        cost: number;
        totalCost: number;
        warehouseId: string;
        locationId: string;
        descriptionEn: string;
        descriptionAr: string;
        itemBatchDetails: [{
            batchNumber: string;
            quantity: number;
            batchDateGregorian: string;
            batchDateHijri: string;
            expiryDateGregorian: string;
            expiryDateHijri: string;
        }]
    }];

  }
