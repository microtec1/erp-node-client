export interface IInventoryReevaluation {
  _id: string;
  code: string;
  inventoryReEvaluationStatus: string;
  inventoryReEvaluationDescriptionAr: string;
  inventoryReEvaluationDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  isActive: boolean;
  inventoryReEvaluationDetails: [{
    barCode: string;
    itemId: string;
    itemsGroupId: string;
    variables: [{
      variableId: string,
      itemVariableNameId: string
    }];
    quantity: number;
    itemUnitId: string;
    oldAverageCost: number;
    newAverageCost: number;
    difference: number;
    warehouseId: string;
    locationId: string;
    descriptionAr: string;
    descriptionEn: string;
  }];
}
