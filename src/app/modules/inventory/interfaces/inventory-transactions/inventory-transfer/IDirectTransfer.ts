export interface IDirectTransfer {
  _id: string;
  code: string;
  inventoryDirectTransferStatus: string;
  inventoryDirectTransferDescriptionAr: string;
  inventoryDirectTransferDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  transactionSourceType: string;
  sourceNumberId: string;
  fromWarehouseId: string;
  toWarehouseId: string;
  referenceNumber: string;
  isActive: boolean;
  inventoryDirectTransferDetails: [
    {
      barCode: string;
      itemId: string;
      variables: [
        {
          variableId: string;
          itemVariableNameId: string;
        }
      ];
      quantity: number;
      itemUnitId: string;
      cost: number;
      totalCost: number;
      fromWarehouseId: string;
      toWarehouseId: string;
      fromLocation: string;
      toLocation: string;
      descriptionAr: string;
      descriptionEn: string;
      itemBatchDetails: [{
        batchNumber: string;
        quantity: number;
        batchDateGregorian: string;
        batchDateHijri: string;
        expiryDateGregorian: string;
        expiryDateHijri: string;
      }]
    }
  ];


}
