export interface IExportTransfer {
  _id: string;
  code: string;
  inventoryExportTransferStatus: string;
  inventoryExportTransferDescriptionAr: string;
  inventoryExportTransferDescriptionEn: string;
  gregorianDate: any;
  hijriDate: string;
  transactionSourceType: string;
  sourceNumberId: string;
  fromWarehouseId: string;
  toWarehouseId: string;
  referenceNumber: string;
  isActive: boolean;
  inventoryExportTransferDetails: [{
    barCode: string;
    itemId: string;
    variables: [{
      variableId: string;
      itemVariableNameId: string;
    }];
    quantity: number;
    itemUnitId: string;
    cost: number;
    totalCost: number;
    fromWarehouseId: string;
    fromLocationId: string;
    toWarehouseId: string;
    descriptionAr: string;
    descriptionEn: string;
    itemBatchDetails: [{
      batchNumber: string;
      quantity: number;
      batchDateGregorian: string;
      batchDateHijri: string;
      expiryDateGregorian: string;
      expiryDateHijri: string;
    }]
  }];
}

