export interface IImportTransfer {
    _id: string;
    code: string;
    inventoryImportTransferStatus: string;
    inventoryImportTransferDescriptionAr: string;
    inventoryImportTransferDescriptionEn: string;
    gregorianDate: string;
    hijriDate: string;
    transactionSourceType: string;
    sourceNumberId: string;
    fromWarehouseId: string;
    toWarehouseId: string;
    referenceNumber: string;
    isActive: boolean;
    inventoryImportTransferDetails: [{
        barCode: string;
        itemId: string;
        variables: [{
            variableId: string;
            itemVariableNameId: string;
        }];
        quantity: number;
        itemUnitId: string;
        cost: number;
        totalCost: number;
        receivedQuantity: number;
        fromWarehouseId: number;
        toWarehouseId: string;
        toLocationId: string;
        descriptionAr: string;
        descriptionEn: string;
        itemBatchDetails: [{
            batchNumber: string;
            quantity: number;
            batchDateGregorian: string;
            batchDateHijri: string;
            expiryDateGregorian: string;
            expiryDateHijri: string;
        }]
    }]

}
