export interface IInternalTransfer {
  _id: string;
  code: string;
  inventoryInternalTransferStatus: string;
  inventoryInternalTransferDescriptionAr: string;
  inventoryInternalTransferDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  fromWarehouseId: string;
  referenceNumber: string;
  isActive: boolean;
  inventoryInternalTransferDetails: [
    {
      barCode: string;
      itemId: string;
      variables: [
        {
          variableId: string;
          itemVariableNameId: string;
        }
      ];
      quantity: number;
      itemUnitId: string;
      fromLocationId: string;
      toLocationId: string;
      descriptionAr: string;
      descriptionEn: string;
      itemBatchDetails: [{
        batchNumber: string;
        quantity: number;
        batchDateGregorian: string;
        batchDateHijri: string;
        expiryDateGregorian: string;
        expiryDateHijri: string;
      }]
    }
  ];

}
