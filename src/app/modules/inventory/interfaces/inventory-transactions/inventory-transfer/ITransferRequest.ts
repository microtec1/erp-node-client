export interface ITransferRequest {
  _id: string;
  code: string;
  inventoryTransferRequestStatus: string;
  inventoryTransferRequestDescriptionAr: string;
  inventoryTransferRequestDescriptionEn: string;
  gregorianDate: any;
  hijriDate: any;
  transactionSourceType: string;
  sourceNumberId: string;
  fromWarehouseId: string;
  toWarehouseId: string;
  referenceNumber: string;
  isActive: boolean;
  inventoryTransferRequestDetails: [
    {
      barCode: string;
      itemId: string;
      variables: [
        {
          variableId: string;
          itemVariableNameId: string;
        }
      ];
      quantity: string;
      itemUnitId: string;
      warehouseBalance: number;
      fromWarehouseId: string;
      toWarehouseId: string;
      descriptionAr: string;
      descriptionEn: string;
    }
  ];

}
