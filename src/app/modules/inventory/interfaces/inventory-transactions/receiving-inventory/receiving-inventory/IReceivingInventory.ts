export interface IReceivingInventory {
  _id: string;
  code: string;
  receivingInventoryStatus: string;
  receivingInventoryDescriptionAr: string;
  receivingInventoryDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referencenumber: string;
  transactionSourceType: string;
  sourceNumberId: string;
  isActive: boolean;
  receivingInventoryDetails: [{
    barCode: string;
    itemId: string;
    itemsGroupId: string;
    variables: [{
      variableId: string;
      itemVariableNameId: string;
    }]
    quantity: number;
    itemUnitId: string;
    cost: number;
    totalCost: number;
    warehouseId: string;
    locationId: string;
    descriptionAr: string;
    descriptionEn: string;
    itemBatchDetails: [{
      batchNumber: string;
      quantity: number;
      batchDateGregorian: string;
      batchDateHijri: string;
      expiryDateGregorian: string;
      expiryDateHijri: string;
    }]
  }];
}
