export interface ICountingReport {
  _id: string;
  image: string;
  code: string;
  inventoryCountingReportStatus: string;
  inventoryCountingReportDescriptionAr: string;
  inventoryCountingReportDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  transactionSourceType: string;
  transactionSourceId: string;
  isActive: boolean;
  inventoryCountingReportDetails: [
    {
      barCode: string;
      itemId: string;
      variables: [{
        variableId: string,
        itemVariableNameId: string
      }];
      quantity: number;
      itemUnitId: string;
      warehouseId: string;
      locationId: string;
      descriptionAr: string;
      descriptionEn: string;
      itemBatchDetails: [{
        batchNumber: string;
        quantity: number;
        batchDateGregorian: string;
        batchDateHijri: string;
        expiryDateGregorian: string;
        expiryDateHijri: string;
      }]
    }
  ];
}
