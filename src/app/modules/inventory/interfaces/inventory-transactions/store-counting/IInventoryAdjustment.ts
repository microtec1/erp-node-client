export interface IInventoryAdjustment {
  _id: string;
  code: string;
  inventoryAdjustmentStatus: string;
  inventoryAdjustmentDescriptionAr: string;
  inventoryAdjustmentDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  transactionSourceType: string;
  transactionSourceId: string;
  inventoryAdjustmentDetails: [{
    barCode: string;
    itemId: string;
    itemsGroupId: string;
    variables: [{
      variableId: string;
      itemVariableNameId: string;
    }];
    quantity: number;
    systemQuantity: number;
    difference: number;
    itemUnitId: string;
    cost: number;
    totalCost: number;
    warehouseId: string;
    locationId: string;
    descriptionAr: string;
    descriptionEn: string;
    itemBatchDetails: [{
      batchNumber: string;
      quantity: number;
      systemQuantity: number;
      batchDateGregorian: string;
      batchDateHijri: string;
      expiryDateGregorian: string;
      expiryDateHijri: string;
    }]
  }];
  isActive: boolean;
}
