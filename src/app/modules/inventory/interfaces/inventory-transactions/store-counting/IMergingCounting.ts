export interface IMergingCounting {
  _id: string;
  code: string;
  inventoryMergingCountingReportStatus: string;
  inventoryMergingCountingReportDescriptionAr: string;
  inventoryMergingCountingReportDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  warehouseId: string;
  referenceNumber: string;
  transactionSourceType: string;
  countingReports: [{
    transactionId: string
  }];
  isActive: boolean;
  inventoryMergingCountingReportDetails: [{
    barCode: string;
    itemId: string;
    variables: [{
      variableId: string,
      itemVariableNameId: string
    }];
    quantity: number;
    itemUnitId: string;
    locationId: string;
    itemBatchDetails: [{
      batchNumber: string;
      quantity: number;
      batchDateGregorian: string;
      batchDateHijri: string;
      expiryDateGregorian: string;
      expiryDateHijri: string;
    }]
  }];
}
