export interface IStoreCounting {
    _id: string;
    image: string;
    code: string;
    inventoryStoreCountingStatus: string;
    inventoryStoreCountingDescriptionAr: string;
    inventoryStoreCountingDescriptionEn: string;
    gregorianDate: string;
    hijriDate: string;
    warehouseId: string;
    referenceNumber: string;
    isActive: boolean;
    inventoryStoreCountingDetails: [{
        barCode: string;
        itemId: string;
        variables: [{
            variableId: string;
            itemVariableNameId: string;
        }];
        itemUnitId: string;
        notes: string;
    }];
}
