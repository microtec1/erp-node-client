import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddItemsTypeComponent } from './components/files/items-types/add-items-type/add-items-type.component';
import { ItemsTypesComponent } from './components/files/items-types/items-types.component';
import { ItemsUnitsComponent } from './components/files/items-units/items-units.component';
import { AddItemsUnitComponent } from './components/files/items-units/add-items-unit/add-items-unit.component';
import { ItemsGroupsComponent } from './components/files/items-groups/items-groups.component';
import { AddItemsGroupComponent } from './components/files/items-groups/add-items-group/add-items-group.component';
import { ItemsFamiliesComponent } from './components/files/items-families/items-families.component';
import { AddItemsFamilyComponent } from './components/files/items-families/add-items-family/add-items-family.component';
import { ItemsClassificationsComponent } from './components/files/items-classifications/items-classifications.component';
// tslint:disable-next-line:max-line-length
import { AddItemsClassificationComponent } from './components/files/items-classifications/add-items-classification/add-items-classification.component';
// tslint:disable-next-line:max-line-length
import { ItemsVariablesComponent } from './components/files/items-variables/items-variables.component';
import { AddItemsVariableComponent } from './components/files/items-variables/add-items-variable/add-items-variable.component';
import { ItemsSectionsComponent } from './components/files/items-sections/items-sections.component';
import { AddItemsSectionComponent } from './components/files/items-sections/add-items-section/add-items-section.component';
import { ServicesItemsGroupsComponent } from './components/files/services-items-groups/services-items-groups.component';
// tslint:disable-next-line:max-line-length
import { AddServicesItemsGroupComponent } from './components/files/services-items-groups/add-services-items-group/add-services-items-group.component';
// tslint:disable-next-line:max-line-length
import { ServicesItemsComponent } from './components/files/services-items/services-items.component';
import { AddServicesItemComponent } from './components/files/services-items/add-services-items/add-services-items.component';
import { WarehousesComponent } from './components/files/warehouses/warehouses.component';
import { AddWarehouseComponent } from './components/files/warehouses/add-warehouse/add-warehouse.component';
import { StoreItemsComponent } from './components/files/store-items/store-items.component';
import { AddStoreItemComponent } from './components/files/store-items/add-store-item/add-store-item.component';
import { SettingsComponent } from './components/inventory-settings/settings.component';
import { TransferRequestComponent } from './components/transactions/inventory-transfer/transfer-request/transfer-request.component';
// tslint:disable-next-line: max-line-length
import { AddTransferRequestComponent } from './components/transactions/inventory-transfer/transfer-request/add-transfer-request/add-transfer-request.component';
// tslint:disable-next-line: max-line-length
import { ExportTransferComponent } from './components/transactions/inventory-transfer/export-transfer/export-transfer.component';
import { AddExportTransferComponent } from './components/transactions/inventory-transfer/export-transfer/add-export-transfer/add-export-transfer.component';
import { ImportTransferComponent } from './components/transactions/inventory-transfer/import-transfer/import-transfer.component';
import { AddImportTransferComponent } from './components/transactions/inventory-transfer/import-transfer/add-import-transfer/add-import-transfer.component';
import { DirectTransferComponent } from './components/transactions/inventory-transfer/direct-transfer/direct-transfer.component';
import { AddDirectTransferComponent } from './components/transactions/inventory-transfer/direct-transfer/add-direct-transfer/add-direct-transfer.component';
// tslint:disable-next-line: max-line-length
import { InternalTransferComponent } from './components/transactions/inventory-transfer/internal-transfer/internal-transfer.component';
import { AddInternalTransferComponent } from './components/transactions/inventory-transfer/internal-transfer/add-internal-transfer/add-internal-transfer.component';
import { RequestHoldingQuantitiesComponent } from './components/transactions/holding-quantities/request-holding-quantities/request-holding-quantities.component';
// tslint:disable-next-line: max-line-length
import { AddRequestHoldingQuantitiesComponent } from './components/transactions/holding-quantities/request-holding-quantities/add-request-holding-quantities/add-request-holding-quantities.component';
// tslint:disable-next-line: max-line-length
import { HoldingQuantitiesComponent } from './components/transactions/holding-quantities/holding-quantities/holding-quantities.component';
import { AddHoldingQuantitiesComponent } from './components/transactions/holding-quantities/holding-quantities/add-holding-quantities/add-holding-quantities.component';
import { UnholdingQuantitiesComponent } from './components/transactions/holding-quantities/unholding-quantities/unholding-quantities.component';
import { AddUnholdingQuantitiesComponent } from './components/transactions/holding-quantities/unholding-quantities/add-unholding-quantities/add-unholding-quantities.component';
import { StoreCountingComponent } from './components/transactions/store-counting/store-counting/store-counting.component';
import { AddStoreCountingComponent } from './components/transactions/store-counting/store-counting/add-store-counting/add-store-counting.component';
import { CountingReportComponent } from './components/transactions/store-counting/counting-report/counting-report.component';
import { AddCountingReportComponent } from './components/transactions/store-counting/counting-report/add-counting-report/add-counting-report.component';
import { MergingCountingComponent } from './components/transactions/store-counting/merging-counting/merging-counting.component';
import { AddMergingCountingComponent } from './components/transactions/store-counting/merging-counting/add-merging-counting/add-merging-counting.component';
import { InventoryAdjustmentComponent } from './components/transactions/store-counting/inventory-adjustment/inventory-adjustment.component';
import { AddInventoryAdjustmentComponent } from './components/transactions/store-counting/inventory-adjustment/add-inventory-adjustment/add-inventory-adjustment.component';
import { InventoryOpeningBalanceComponent } from './components/transactions/inventory-opening-balance/inventory-opening-balance.component';
import { AddInventoryOpeningBalanceComponent } from './components/transactions/inventory-opening-balance/add-inventory-opening-balance/add-inventory-opening-balance.component';
import { InventoryReevaluationComponent } from './components/transactions/inventory-reevaluation/inventory-reevaluation.component';
import { AddInventoryReevaluationComponent } from './components/transactions/inventory-reevaluation/add-inventory-reevaluation/add-inventory-reevaluation.component';
import { ReceivingInventoryComponent } from './components/transactions/receiving-inventory/receiving-inventory/receiving-inventory.component';
import { AddReceivingInventoryComponent } from './components/transactions/receiving-inventory/receiving-inventory/add-receiving-inventory/add-receiving-inventory.component';
import { DeliveringInventoryRequestComponent } from './components/transactions/deliveing-inventory/delivering-inventory-request/delivering-inventory-request.component';
// tslint:disable-next-line: max-line-length
import { AddDeliveringInventoryRequestComponent } from './components/transactions/deliveing-inventory/delivering-inventory-request/add-delivering-inventory-request/add-delivering-inventory-request.component';
// tslint:disable-next-line: max-line-length
import { AssemblingItemsComponent } from './components/transactions/assembling-or-disassembling/assembling-items/assembling-items.component';
import { AddAssemblingItemsComponent } from './components/transactions/assembling-or-disassembling/assembling-items/add-assembling-items/add-assembling-items.component';
import { DisassemblingItemsComponent } from './components/transactions/assembling-or-disassembling/disassembling-items/disassembling-items.component';
import { AddDisassemblingItemsComponent } from './components/transactions/assembling-or-disassembling/disassembling-items/add-disassembling-items/add-disassembling-items.component';
import { DeliveringInventoryComponent } from './components/transactions/deliveing-inventory/delivering-inventory/delivering-inventory.component';
import { AddDeliveringInventoryComponent } from './components/transactions/deliveing-inventory/delivering-inventory/add-delivering-inventory/add-delivering-inventory.component';
import { DamageStoreItemsComponent } from './components/transactions/damage-store-items/damage-store-items.component';
import { AddDamageStoreItemsComponent } from './components/transactions/damage-store-items/add-damage-store-items/add-damage-store-items.component';

const routes: Routes = [
  { path: '', redirectTo: 'itemsTypes', pathMatch: 'full' },
  { path: 'itemsTypes', component: ItemsTypesComponent },
  { path: 'itemsTypes/add', component: AddItemsTypeComponent },
  { path: 'itemsTypes/edit/:id', component: AddItemsTypeComponent },
  { path: 'itemsTypes/details/:id', component: AddItemsTypeComponent },
  { path: 'itemsUnits', component: ItemsUnitsComponent },
  { path: 'itemsUnits/add', component: AddItemsUnitComponent },
  { path: 'itemsUnits/edit/:id', component: AddItemsUnitComponent },
  { path: 'itemsUnits/details/:id', component: AddItemsUnitComponent },
  { path: 'itemsGroups', component: ItemsGroupsComponent },
  { path: 'itemsGroups/add', component: AddItemsGroupComponent },
  { path: 'itemsGroups/edit/:id', component: AddItemsGroupComponent },
  { path: 'itemsGroups/details/:id', component: AddItemsGroupComponent },
  { path: 'itemsFamilies', component: ItemsFamiliesComponent },
  { path: 'itemsFamilies/add', component: AddItemsFamilyComponent },
  { path: 'itemsFamilies/edit/:id', component: AddItemsFamilyComponent },
  { path: 'itemsFamilies/details/:id', component: AddItemsFamilyComponent },
  { path: 'itemsClassifications', component: ItemsClassificationsComponent },
  { path: 'itemsClassifications/add', component: AddItemsClassificationComponent },
  { path: 'itemsClassifications/edit/:id', component: AddItemsClassificationComponent },
  { path: 'itemsClassifications/details/:id', component: AddItemsClassificationComponent },
  { path: 'itemsVariables', component: ItemsVariablesComponent },
  { path: 'itemsVariables/add', component: AddItemsVariableComponent },
  { path: 'itemsVariables/edit/:id', component: AddItemsVariableComponent },
  { path: 'itemsVariables/details/:id', component: AddItemsVariableComponent },
  { path: 'itemsSections', component: ItemsSectionsComponent },
  { path: 'itemsSections/add', component: AddItemsSectionComponent },
  { path: 'itemsSections/edit/:id', component: AddItemsSectionComponent },
  { path: 'itemsSections/details/:id', component: AddItemsSectionComponent },
  { path: 'servicesItemsGroups', component: ServicesItemsGroupsComponent },
  { path: 'servicesItemsGroups/add', component: AddServicesItemsGroupComponent },
  { path: 'servicesItemsGroups/edit/:id', component: AddServicesItemsGroupComponent },
  { path: 'servicesItemsGroups/details/:id', component: AddServicesItemsGroupComponent },
  { path: 'servicesItems', component: ServicesItemsComponent },
  { path: 'servicesItems/add', component: AddServicesItemComponent },
  { path: 'servicesItems/edit/:id', component: AddServicesItemComponent },
  { path: 'servicesItems/details/:id', component: AddServicesItemComponent },
  { path: 'warehouses', component: WarehousesComponent },
  { path: 'warehouses/add', component: AddWarehouseComponent },
  { path: 'storeItems', component: StoreItemsComponent },
  { path: 'storeItems/add', component: AddStoreItemComponent },
  { path: 'storeItems/edit/:id', component: AddStoreItemComponent },
  { path: 'storeItems/details/:id', component: AddStoreItemComponent },
  { path: 'settings', component: SettingsComponent }
  ,

  { path: 'transferRequest', component: TransferRequestComponent },
  { path: 'transferRequest/add', component: AddTransferRequestComponent },
  { path: 'transferRequest/edit/:id', component: AddTransferRequestComponent },
  { path: 'transferRequest/details/:id', component: AddTransferRequestComponent },

  { path: 'exportTransfer', component: ExportTransferComponent },
  { path: 'exportTransfer/add', component: AddExportTransferComponent },
  { path: 'exportTransfer/edit/:id', component: AddExportTransferComponent },
  { path: 'exportTransfer/details/:id', component: AddExportTransferComponent },


  { path: 'importTransfer', component: ImportTransferComponent },
  { path: 'importTransfer/add', component: AddImportTransferComponent },
  { path: 'importTransfer/edit/:id', component: AddImportTransferComponent },
  { path: 'importTransfer/details/:id', component: AddImportTransferComponent },


  { path: 'directTransfer', component: DirectTransferComponent },
  { path: 'directTransfer/add', component: AddDirectTransferComponent },
  { path: 'directTransfer/edit/:id', component: AddDirectTransferComponent },
  { path: 'directTransfer/details/:id', component: AddDirectTransferComponent },

  { path: 'internalTransfer', component: InternalTransferComponent },
  { path: 'internalTransfer/add', component: AddInternalTransferComponent },
  { path: 'internalTransfer/edit/:id', component: AddInternalTransferComponent },
  { path: 'internalTransfer/details/:id', component: AddInternalTransferComponent },

  { path: 'requestHoldingQuantities', component: RequestHoldingQuantitiesComponent },
  { path: 'requestHoldingQuantities/add', component: AddRequestHoldingQuantitiesComponent },
  { path: 'requestHoldingQuantities/edit/:id', component: AddRequestHoldingQuantitiesComponent },
  { path: 'requestHoldingQuantities/details/:id', component: AddRequestHoldingQuantitiesComponent },

  { path: 'holdingQuantities', component: HoldingQuantitiesComponent },
  { path: 'holdingQuantities/add', component: AddHoldingQuantitiesComponent },
  { path: 'holdingQuantities/edit/:id', component: AddHoldingQuantitiesComponent },
  { path: 'holdingQuantities/details/:id', component: AddHoldingQuantitiesComponent },

  { path: 'unholdingQuantities', component: UnholdingQuantitiesComponent },
  { path: 'unholdingQuantities/add', component: AddUnholdingQuantitiesComponent },
  { path: 'unholdingQuantities/edit/:id', component: AddUnholdingQuantitiesComponent },
  { path: 'unholdingQuantities/details/:id', component: AddUnholdingQuantitiesComponent },

  { path: 'storeCounting', component: StoreCountingComponent },
  { path: 'storeCounting/add', component: AddStoreCountingComponent },
  { path: 'storeCounting/edit/:id', component: AddStoreCountingComponent },
  { path: 'storeCounting/details/:id', component: AddStoreCountingComponent },

  { path: 'countingReport', component: CountingReportComponent },
  { path: 'countingReport/add', component: AddCountingReportComponent },
  { path: 'countingReport/edit/:id', component: AddCountingReportComponent },
  { path: 'countingReport/details/:id', component: AddCountingReportComponent },

  { path: 'mergingCounting', component: MergingCountingComponent },
  { path: 'mergingCounting/add', component: AddMergingCountingComponent },
  { path: 'mergingCounting/edit/:id', component: AddMergingCountingComponent },
  { path: 'mergingCounting/details/:id', component: AddMergingCountingComponent },

  { path: 'inventoryAdjustment', component: InventoryAdjustmentComponent },
  { path: 'inventoryAdjustment/add', component: AddInventoryAdjustmentComponent },
  { path: 'inventoryAdjustment/edit/:id', component: AddInventoryAdjustmentComponent },
  { path: 'inventoryAdjustment/details/:id', component: AddInventoryAdjustmentComponent },

  { path: 'inventoryOpeningBalance', component: InventoryOpeningBalanceComponent },
  { path: 'inventoryOpeningBalance/add', component: AddInventoryOpeningBalanceComponent },
  { path: 'inventoryOpeningBalance/edit/:id', component: AddInventoryOpeningBalanceComponent },
  { path: 'inventoryOpeningBalance/details/:id', component: AddInventoryOpeningBalanceComponent },

  { path: 'inventoryReevaluation', component: InventoryReevaluationComponent },
  { path: 'inventoryReevaluation/add', component: AddInventoryReevaluationComponent },
  { path: 'inventoryReevaluation/edit/:id', component: AddInventoryReevaluationComponent },
  { path: 'inventoryReevaluation/details/:id', component: AddInventoryReevaluationComponent },

  { path: 'receivingInventory', component: ReceivingInventoryComponent },
  { path: 'receivingInventory/add', component: AddReceivingInventoryComponent },
  { path: 'receivingInventory/edit/:id', component: AddReceivingInventoryComponent },
  { path: 'receivingInventory/details/:id', component: AddReceivingInventoryComponent },

  { path: 'damageStoreItems', component: DamageStoreItemsComponent },
  { path: 'damageStoreItems/add', component: AddDamageStoreItemsComponent },
  { path: 'damageStoreItems/edit/:id', component: AddDamageStoreItemsComponent },
  { path: 'damageStoreItems/details/:id', component: AddDamageStoreItemsComponent },

  { path: 'deliveringInventory', component: DeliveringInventoryComponent },
  { path: 'deliveringInventory/add', component: AddDeliveringInventoryComponent },
  { path: 'deliveringInventory/edit/:id', component: AddDeliveringInventoryComponent },
  { path: 'deliveringInventory/details/:id', component: AddDeliveringInventoryComponent },

  { path: 'deliveringInventoryRequest', component: DeliveringInventoryRequestComponent },
  { path: 'deliveringInventoryRequest/add', component: AddDeliveringInventoryRequestComponent },
  { path: 'deliveringInventoryRequest/edit/:id', component: AddDeliveringInventoryRequestComponent },
  { path: 'deliveringInventoryRequest/details/:id', component: AddDeliveringInventoryRequestComponent },

  { path: 'assemblingItems', component: AssemblingItemsComponent },
  { path: 'assemblingItems/add', component: AddAssemblingItemsComponent },
  { path: 'assemblingItems/edit/:id', component: AddAssemblingItemsComponent },
  { path: 'assemblingItems/details/:id', component: AddAssemblingItemsComponent },

  { path: 'disassemblingItems', component: DisassemblingItemsComponent },
  { path: 'disassemblingItems/add', component: AddDisassemblingItemsComponent },
  { path: 'disassemblingItems/edit/:id', component: AddDisassemblingItemsComponent },
  { path: 'disassemblingItems/details/:id', component: AddDisassemblingItemsComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
