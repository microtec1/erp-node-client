import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { InventoryRoutingModule } from './inventory-routing.module';
import { ItemsTypesComponent } from './components/files/items-types/items-types.component';
import { AddItemsTypeComponent } from './components/files/items-types/add-items-type/add-items-type.component';
import { ItemsUnitsComponent } from './components/files/items-units/items-units.component';
import { AddItemsUnitComponent } from './components/files/items-units/add-items-unit/add-items-unit.component';
import { ItemsGroupsComponent } from './components/files/items-groups/items-groups.component';
import { AddItemsGroupComponent } from './components/files/items-groups/add-items-group/add-items-group.component';
import { ItemsFamiliesComponent } from './components/files/items-families/items-families.component';
import { AddItemsFamilyComponent } from './components/files/items-families/add-items-family/add-items-family.component';
import { ItemsClassificationsComponent } from './components/files/items-classifications/items-classifications.component';
// tslint:disable-next-line:max-line-length
import { AddItemsClassificationComponent } from './components/files/items-classifications/add-items-classification/add-items-classification.component';
// tslint:disable-next-line:max-line-length
import { ItemsVariablesComponent } from './components/files/items-variables/items-variables.component';
import { AddItemsVariableComponent } from './components/files/items-variables/add-items-variable/add-items-variable.component';
import { ItemsSectionsComponent } from './components/files/items-sections/items-sections.component';
import { AddItemsSectionComponent } from './components/files/items-sections/add-items-section/add-items-section.component';
// tslint:disable-next-line:max-line-length
import { ServicesItemsGroupsComponent } from './components/files/services-items-groups/services-items-groups.component';
// tslint:disable-next-line:max-line-length
import { AddServicesItemsGroupComponent } from './components/files/services-items-groups/add-services-items-group/add-services-items-group.component';
import { ServicesItemsComponent } from './components/files/services-items/services-items.component';
import { AddServicesItemComponent } from './components/files/services-items/add-services-items/add-services-items.component';
import { WarehousesComponent } from './components/files/warehouses/warehouses.component';
import { AddWarehouseComponent } from './components/files/warehouses/add-warehouse/add-warehouse.component';
import { WarehousesTreeComponent } from './components/files/warehouses/warehouses-tree/warehouses-tree.component';
import { StoreItemsComponent } from './components/files/store-items/store-items.component';
import { AddStoreItemComponent } from './components/files/store-items/add-store-item/add-store-item.component';
import { SettingsComponent } from './components/inventory-settings/settings.component';
import { TransferRequestComponent } from './components/transactions/inventory-transfer/transfer-request/transfer-request.component';
import { AddTransferRequestComponent } from './components/transactions/inventory-transfer/transfer-request/add-transfer-request/add-transfer-request.component';
import { ExportTransferComponent } from './components/transactions/inventory-transfer/export-transfer/export-transfer.component';
import { AddExportTransferComponent } from './components/transactions/inventory-transfer/export-transfer/add-export-transfer/add-export-transfer.component';
import { ImportTransferComponent } from './components/transactions/inventory-transfer/import-transfer/import-transfer.component';
import { AddImportTransferComponent } from './components/transactions/inventory-transfer/import-transfer/add-import-transfer/add-import-transfer.component';
import { DirectTransferComponent } from './components/transactions/inventory-transfer/direct-transfer/direct-transfer.component';
import { AddDirectTransferComponent } from './components/transactions/inventory-transfer/direct-transfer/add-direct-transfer/add-direct-transfer.component';
import { InternalTransferComponent } from './components/transactions/inventory-transfer/internal-transfer/internal-transfer.component';
import { AddInternalTransferComponent } from './components/transactions/inventory-transfer/internal-transfer/add-internal-transfer/add-internal-transfer.component';
import { RequestHoldingQuantitiesComponent } from './components/transactions/holding-quantities/request-holding-quantities/request-holding-quantities.component';
// tslint:disable-next-line:max-line-length
import { AddRequestHoldingQuantitiesComponent } from './components/transactions/holding-quantities/request-holding-quantities/add-request-holding-quantities/add-request-holding-quantities.component';
import { HoldingQuantitiesComponent } from './components/transactions/holding-quantities/holding-quantities/holding-quantities.component';
import { AddHoldingQuantitiesComponent } from './components/transactions/holding-quantities/holding-quantities/add-holding-quantities/add-holding-quantities.component';
import { UnholdingQuantitiesComponent } from './components/transactions/holding-quantities/unholding-quantities/unholding-quantities.component';
import { AddUnholdingQuantitiesComponent } from './components/transactions/holding-quantities/unholding-quantities/add-unholding-quantities/add-unholding-quantities.component';
import { StoreCountingComponent } from './components/transactions/store-counting/store-counting/store-counting.component';
import { AddStoreCountingComponent } from './components/transactions/store-counting/store-counting/add-store-counting/add-store-counting.component';
import { CountingReportComponent } from './components/transactions/store-counting/counting-report/counting-report.component';
import { AddCountingReportComponent } from './components/transactions/store-counting/counting-report/add-counting-report/add-counting-report.component';
import { MergingCountingComponent } from './components/transactions/store-counting/merging-counting/merging-counting.component';
import { AddMergingCountingComponent } from './components/transactions/store-counting/merging-counting/add-merging-counting/add-merging-counting.component';
import { InventoryAdjustmentComponent } from './components/transactions/store-counting/inventory-adjustment/inventory-adjustment.component';
import { AddInventoryAdjustmentComponent } from './components/transactions/store-counting/inventory-adjustment/add-inventory-adjustment/add-inventory-adjustment.component';
import {InventoryOpeningBalanceComponent} from './components/transactions/inventory-opening-balance/inventory-opening-balance.component';
import { AddInventoryOpeningBalanceComponent } from './components/transactions/inventory-opening-balance/add-inventory-opening-balance/add-inventory-opening-balance.component';
import { InventoryReevaluationComponent } from './components/transactions/inventory-reevaluation/inventory-reevaluation.component';
import { AddInventoryReevaluationComponent } from './components/transactions/inventory-reevaluation/add-inventory-reevaluation/add-inventory-reevaluation.component';
import { ReceivingInventoryComponent } from './components/transactions/receiving-inventory/receiving-inventory/receiving-inventory.component';
import { AddReceivingInventoryComponent } from './components/transactions/receiving-inventory/receiving-inventory/add-receiving-inventory/add-receiving-inventory.component';
import { DeliveringInventoryRequestComponent } from './components/transactions/deliveing-inventory/delivering-inventory-request/delivering-inventory-request.component';
import { DeliveringInventoryComponent } from './components/transactions/deliveing-inventory/delivering-inventory/delivering-inventory.component';
import { AddDeliveringInventoryComponent } from './components/transactions/deliveing-inventory/delivering-inventory/add-delivering-inventory/add-delivering-inventory.component';
import { AddDeliveringInventoryRequestComponent } from './components/transactions/deliveing-inventory/delivering-inventory-request/add-delivering-inventory-request/add-delivering-inventory-request.component';
import { DamageStoreItemsComponent } from './components/transactions/damage-store-items/damage-store-items.component';
import { AddDamageStoreItemsComponent } from './components/transactions/damage-store-items/add-damage-store-items/add-damage-store-items.component';
import { AssemblingItemsComponent } from './components/transactions/assembling-or-disassembling/assembling-items/assembling-items.component';
import { AddAssemblingItemsComponent } from './components/transactions/assembling-or-disassembling/assembling-items/add-assembling-items/add-assembling-items.component';
import { DisassemblingItemsComponent } from './components/transactions/assembling-or-disassembling/disassembling-items/disassembling-items.component';
import { AddDisassemblingItemsComponent } from './components/transactions/assembling-or-disassembling/disassembling-items/add-disassembling-items/add-disassembling-items.component';
import { InventoryFilterationComponent } from './components/transactions/inventory-filteration/inventory-filteration.component';


@NgModule({
  declarations: [
    ItemsTypesComponent,
    AddItemsTypeComponent,
    ItemsUnitsComponent,
    AddItemsUnitComponent,
    ItemsGroupsComponent,
    AddItemsGroupComponent,
    ItemsFamiliesComponent,
    AddItemsFamilyComponent,
    ItemsClassificationsComponent,
    AddItemsClassificationComponent,
    ItemsVariablesComponent,
    AddItemsVariableComponent,
    ItemsSectionsComponent,
    AddItemsSectionComponent,
    ServicesItemsGroupsComponent,
    AddServicesItemsGroupComponent,
    ServicesItemsComponent,
    AddServicesItemComponent,
    WarehousesComponent,
    AddWarehouseComponent,
    WarehousesTreeComponent,
    StoreItemsComponent,
    AddStoreItemComponent,
    SettingsComponent,
    TransferRequestComponent,
    AddTransferRequestComponent,
    ExportTransferComponent,
    AddExportTransferComponent,
    ImportTransferComponent,
    AddImportTransferComponent,
    DirectTransferComponent,
    AddDirectTransferComponent,
    InternalTransferComponent,
    AddInternalTransferComponent,
    RequestHoldingQuantitiesComponent,
    AddRequestHoldingQuantitiesComponent,
    HoldingQuantitiesComponent,
    AddHoldingQuantitiesComponent,
    UnholdingQuantitiesComponent,
    AddUnholdingQuantitiesComponent,
    StoreCountingComponent,
    AddStoreCountingComponent,
    CountingReportComponent,
    AddCountingReportComponent,
    MergingCountingComponent,
    AddMergingCountingComponent,
    InventoryAdjustmentComponent,
    AddInventoryAdjustmentComponent,
    InventoryOpeningBalanceComponent,
    AddInventoryOpeningBalanceComponent,
    InventoryReevaluationComponent,
    AddInventoryReevaluationComponent,
    ReceivingInventoryComponent,
    AddReceivingInventoryComponent,
    DeliveringInventoryRequestComponent,
    DeliveringInventoryComponent,
    AddDeliveringInventoryComponent,
    AddDeliveringInventoryRequestComponent,
    DamageStoreItemsComponent,
    AddDamageStoreItemsComponent,
    AssemblingItemsComponent,
    AddAssemblingItemsComponent,
    DisassemblingItemsComponent,
    AddDisassemblingItemsComponent,
    InventoryFilterationComponent
  ],
  imports: [
    SharedModule,
    InventoryRoutingModule
  ]
})
export class InventoryModule { }
