import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IWarehouse } from '../interfaces/IWarehouse';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Injectable({
  providedIn: 'root'
})
export class WarehousesService {
  warehousesChanged = new Subject<IWarehouse[]>();
  fillFormChange = new Subject<IWarehouse>();

  constructor() { }

}
