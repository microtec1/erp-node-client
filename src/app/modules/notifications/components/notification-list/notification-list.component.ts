import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationsSearchModalComponent } from '../notifications-search-modal/notifications-search-modal.component';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss']
})
export class NotificationListComponent implements OnInit {
  bsModalRef: BsModalRef;
  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }

  openModal() {
    const initialState = {};
    this.bsModalRef = this.modalService.show(NotificationsSearchModalComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Close';
  }


}
