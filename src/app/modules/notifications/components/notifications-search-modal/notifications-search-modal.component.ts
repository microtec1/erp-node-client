import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-notifications-search-modal',
  templateUrl: './notifications-search-modal.component.html',
  styleUrls: ['./notifications-search-modal.component.scss']
})
export class NotificationsSearchModalComponent implements OnInit {
  searchForm: FormGroup;
  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {
    this.initForm();
  }

  get form() {
    return this.searchForm;
  }

  submit() {
    this.bsModalRef.hide();
  }

  clear() {
    this.searchForm.reset();
  }

  private initForm() {
    this.searchForm = new FormGroup({
      system: new FormControl(null),
      user: new FormControl(null),
      message: new FormControl(null),
      value: new FormControl(null),
      dateFrom: new FormGroup({
        day: new FormControl(null),
        month: new FormControl(null),
        year: new FormControl(null)
      }),
      dateTo: new FormGroup({
        day: new FormControl(null),
        month: new FormControl(null),
        year: new FormControl(null)
      }),
      maxNotifications: new FormControl(null)
    });
  }
}
