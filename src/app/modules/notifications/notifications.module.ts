import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { NotificationListComponent } from './components/notification-list/notification-list.component';
import { NotificationsRoutingModule } from './notifications-routing.module';
import { NotificationsSearchModalComponent } from './components/notifications-search-modal/notifications-search-modal.component';

@NgModule({
  declarations: [NotificationListComponent, NotificationsSearchModalComponent],
  imports: [
    SharedModule,
    NotificationsRoutingModule
  ],
  entryComponents: [NotificationsSearchModalComponent]
})
export class NotificationsModule { }
