import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDefinitionOfPosComponent } from './add-definition-of-pos.component';

describe('AddDefinitionOfPosComponent', () => {
  let component: AddDefinitionOfPosComponent;
  let fixture: ComponentFixture<AddDefinitionOfPosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDefinitionOfPosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDefinitionOfPosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
