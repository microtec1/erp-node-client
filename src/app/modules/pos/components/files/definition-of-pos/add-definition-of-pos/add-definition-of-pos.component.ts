import { Component, OnInit, ViewChild } from '@angular/core';
import {
  baseUrl,
  PosDefinitionApi,
  warehousesApi,
  safeBoxesApi,
  servicesItemsGroupsApi,
  itemsGroupsApi,
  banksApi
} from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import {
  ActivatedRoute,
  Router,
  RouterStateSnapshot,
  Params
} from '@angular/router';
import { IDefinitionOfPos } from 'src/app/modules/pos/interfaces/files/IDefinitionOfPos';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { searchLength } from 'src/app/common/constants/general.constants';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IServicesItemsGroup } from 'src/app/modules/inventory/interfaces/IServicesItemsGroup';
import { IItemsGroup } from 'src/app/modules/inventory/interfaces/IItemsGroup';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';

@Component({
  selector: 'app-add-definition-of-pos',
  templateUrl: './add-definition-of-pos.component.html',
  styleUrls: ['./add-definition-of-pos.component.scss']
})
export class AddDefinitionOfPosComponent implements OnInit {
  item: IDefinitionOfPos = this.restFields();
  pointType: any = 'main';
  filesAdded = false;
  showEdit = false;

  url = PosDefinitionApi;
  baseUrl = baseUrl;

  subscriptions: Subscription[] = [];
  itemsGroup: any = null;
  servicesItemsGroup: any = {
    servicesItemsGroupId: null,
    notes: false
  };


  subPointLength: number = 0;


  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Warehouses
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Services Items Groups
  servicesItemsGroups: IServicesItemsGroup[] = [];
  servicesItemsGroupsInputFocused: boolean;
  hasMoreServicesItemsGroups: boolean;
  servicesItemsGroupsCount: number;
  selectedServicesItemsGroupsPage = 1;
  servicesItemsGroupsPagesNo: number;
  noServicesItemsGroups: boolean;

  // Items Groups
  itemsGroups: IItemsGroup[] = [];
  itemsGroupsInputFocused: boolean;
  hasMoreItemsGroups: boolean;
  itemsGroupsCount: number;
  selectedItemsGroupsPage = 1;
  itemsGroupsPagesNo: number;
  noItemsGroups: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  @ViewChild('dropzone') dropzone: any;
  detailsMode: boolean;

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.getData();
    this.subscriptions.push(
      this.dataService.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);

        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.dataService
        .get(warehousesApi, 1).subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          this.warehouses.push(...res.results);

          this.uiService.isLoading.next(false);
        })
    );


    this.subscriptions.push(
      this.dataService
        .get(servicesItemsGroupsApi, 1)
        .subscribe((res: IDataRes) => {
          this.servicesItemsGroupsPagesNo = res.pages;
          this.servicesItemsGroupsCount = res.count;
          if (this.servicesItemsGroupsPagesNo > this.selectedServicesItemsGroupsPage) {
            this.hasMoreServicesItemsGroups = true;
          }
          this.servicesItemsGroups.push(...res.results);

          this.uiService.isLoading.next(false);
        })
    );

  }
  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.item.image = content as string;
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.item.image = '';
    this.filesAdded = false;
  }

  restFields() {
    return {
      code: '01',
      pointOfSaleNameAr: '',
      pointOfSaleNameEn: '',
      warehouseId: '',
      safeBoxId: '',
      servicesItemsGroupList: [],
      itemsGroupList: [],
      subPointList: [
        {
          isActive: true,
          code: '',
          subPointNameAr: '',
          subPointNameEn: '',
          safeBoxId: null,
          bankId: null,
          paidType: ['payCash'],
          printerRequests: '',
          printerExtraRequests: '',
          numberPrintCopies: 0
        }
      ],
      image: '',
      isActive: true
    };
  }
  submit() {
    if (this.item._id) {
      this.dataService.put(`${this.url}`, this.item).subscribe(
        data => {
          this.item = this.restFields();
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');

          this.uiService.isLoading.next(false);

          this.router.navigate(['/pos/definitionOfPos/']);
        },
        err => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        }
      );
    } else {
      if (this.item.image === '') {
        delete this.item.image;
      }
      delete this.item._id;
      this.dataService.post(`${this.url}`, this.item).subscribe(
        (data: IDefinitionOfPos) => {
          this.item = this.restFields();
          this.filesAdded = false;
          this.uiService.showSuccess('GENERAL.addedSuccessfully', '');

          this.uiService.isLoading.next(false);
          this.router.navigate(['/pos/definitionOfPos/']);
        },
        err => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        }
      );
    }
  }
  getData() {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      if (id) {
        this.dataService
          .getByID(this.url, id)
          .subscribe((data: IDefinitionOfPos) => {
            this.item = data;

            this.searchByIDSServicesItemsGroups(
              this.item.servicesItemsGroupList
            );
            this.searchByIDSItemsGroups(this.item.itemsGroupList);

            if (data.image) {
              this.item.image = this.baseUrl + data.image;
            }
            this.subPointLength = this.item.subPointList.length;
            this.uiService.isLoading.next(false);
          });
        this.showEdit = true;
      } else {
        this.showEdit = false;
      }
    });
  }


  deleteItemFromArray(i) {
    if (i >= this.subPointLength) {
      this.item.subPointList.splice(i, 1)
    } else {
      this.item.subPointList.splice(i, 1);
      this.subPointLength-=1
    }
  }


  updateCheckedOptions(i, val) {
    if (this.item.subPointList[i].paidType.indexOf(val) >= 0) {
      this.item.subPointList[i].paidType.splice(
        this.item.subPointList[i].paidType.indexOf(val),
        1
      );
    } else {
      this.item.subPointList[i].paidType.push(val);
    }
  }
  addSub() {
    this.item.subPointList.push({
      isActive: true,
      code: '',
      subPointNameAr: '',
      subPointNameEn: '',
      safeBoxId: null,
      bankId: null,
      paidType: ['payCash'],
      printerRequests: '',
      printerExtraRequests: '',
      numberPrintCopies: 0
    });
  }

  public itemGroups(id) {
    return this.itemsGroups.find(el => {
      return el._id === id;
    });
  }


  addServicesItemsGroup(val) {
    if (val.servicesItemsGroupId) {
      this.item.servicesItemsGroupList.push(val)
      this.servicesItemsGroup = {
        servicesItemsGroupId: null,
        notes: false
      };
    }


  }

  servicesItemGroups(id) {
    return this.servicesItemsGroups.find(el => {
      return el._id === id;
    });
  }

  searchByIDSServicesItemsGroups(list) {
    const idsList = list.map(el => {
      return el.servicesItemsGroupId;
    });
    const searchQuery = {
      _id: {
        $in: idsList
      }
    };
    this.dataService
      .get(servicesItemsGroupsApi, null, searchQuery)
      .subscribe((res: IDataRes) => {
        this.servicesItemsGroups = res.results;

        this.uiService.isLoading.next(false);
      });
  }

  searchByIDSItemsGroups(idsList) {
    const searchQuery = {
      _id: {
        $in: idsList
      }
    };
    this.dataService
      .get(itemsGroupsApi, null, searchQuery)
      .subscribe((res: IDataRes) => {
        this.itemsGroups = res.results;

        this.uiService.isLoading.next(false);
      });
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              for (const item of res.results) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMorewarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
          }
          for (const item of res.results) {
            if (this.warehouses.length) {
              const uniqueWarehouses = this.warehouses.filter(
                x => x._id !== item._id
              );
              this.warehouses = uniqueWarehouses;
            }
            this.warehouses.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(
                x => x._id !== item._id
              );
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchServicesItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      servicesItemsGroupNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService
          .get(servicesItemsGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noServicesItemsGroups = true;
            } else {
              this.noServicesItemsGroups = false;
              for (const item of res.results) {
                if (this.servicesItemsGroups.length) {
                  const uniqueServicesItemsGroups = this.servicesItemsGroups.filter(
                    x => x._id !== item._id
                  );
                  this.servicesItemsGroups = uniqueServicesItemsGroups;
                }
                this.servicesItemsGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreServicesItemsGroups() {
    this.selectedServicesItemsGroupsPage =
      this.selectedServicesItemsGroupsPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(servicesItemsGroupsApi, this.selectedServicesItemsGroupsPage)
        .subscribe((res: IDataRes) => {
          if (
            this.servicesItemsGroupsPagesNo >
            this.selectedServicesItemsGroupsPage
          ) {
            this.hasMoreServicesItemsGroups = true;
          } else {
            this.hasMoreServicesItemsGroups = false;
          }
          for (const item of res.results) {
            if (this.servicesItemsGroups.length) {
              const uniqueServicesItemsGroups = this.servicesItemsGroups.filter(
                x => x._id !== item._id
              );
              this.servicesItemsGroups = uniqueServicesItemsGroups;
            }
            this.servicesItemsGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      itemsGroupNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService
          .get(itemsGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsGroups = true;
            } else {
              this.noItemsGroups = false;
              for (const item of res.results) {
                if (this.itemsGroups.length) {
                  const uniqueItemsGroups = this.itemsGroups.filter(
                    x => x._id !== item._id
                  );
                  this.itemsGroups = uniqueItemsGroups;
                }
                this.itemsGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsGroups() {
    this.selectedItemsGroupsPage = this.selectedItemsGroupsPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(itemsGroupsApi, this.selectedItemsGroupsPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
            this.hasMoreItemsGroups = true;
          } else {
            this.hasMoreItemsGroups = false;
          }
          for (const item of res.results) {
            if (this.itemsGroups.length) {
              const uniqueItemsGroups = this.itemsGroups.filter(
                x => x._id !== item._id
              );
              this.itemsGroups = uniqueItemsGroups;
            }
            this.itemsGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }
}
