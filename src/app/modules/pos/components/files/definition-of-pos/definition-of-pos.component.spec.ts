import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinitionOfPosComponent } from './definition-of-pos.component';

describe('DefinitionOfPosComponent', () => {
  let component: DefinitionOfPosComponent;
  let fixture: ComponentFixture<DefinitionOfPosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefinitionOfPosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinitionOfPosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
