import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IDefinitionOfPos } from '../../../interfaces/files/IDefinitionOfPos';
import { PosDefinitionApi, baseUrl } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-definition-of-pos',
  templateUrl: './definition-of-pos.component.html',
  styleUrls: ['./definition-of-pos.component.scss']
})
export class DefinitionOfPosComponent implements OnInit {
  bsModalRef: BsModalRef;
  pageInfo;
  showSearch: boolean;
  results = [];
  count;
  searchItem: IDefinitionOfPos = {
    code: '',
    pointOfSaleNameAr: '',
    pointOfSaleNameEn: ''
  };
  sortType = 'asc';
  sortValue: string;
  formReady = false;
  selected = 1;
  url = PosDefinitionApi;
  baseUrl = baseUrl;

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.getAllData();
  }
  getAllData() {
    this.dataService.post(`${this.url}/search`, {}).subscribe((data: any) => {
      this.count = data.count;
      this.pageInfo = data;
      this.results = data.results;
      this.formReady = true;
      this.uiService.isLoading.next(false);
    }, (err) => {
      this.uiService.isLoading.next(false);
    });
  }

  deleteModal(data: IDefinitionOfPos) {
    const initialState = {
      code: data.code,
      nameAr: data.pointOfSaleNameAr,
      nameEn: data.pointOfSaleNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, {
      initialState,
      class: 'confirm-modal'
    });
    this.bsModalRef.content.confirmed.subscribe(confirmed => {
      if (confirmed) {
        this.dataService
          .delete(`${this.url}`, data._id)
          .subscribe(result => {
            this.getAllData();
            this.bsModalRef.hide();
          });
      } else {
        this.bsModalRef.hide();
      }
    });
  }
  clear() {
    this.searchItem = {
      code: '',
      pointOfSaleNameAr: '',
      pointOfSaleNameEn: ''
    };
  }
  search() {
    this.dataService.post(`${this.url}/search`, this.searchItem).subscribe((data: any) => {
      this.results = data.results;
      this.sortType = 'asc';
      this.formReady = true;

      this.uiService.isLoading.next(false);
    }, (err) => {
      this.uiService.isLoading.next(false);
    });
  }
  openSearch() {
    this.showSearch = !this.showSearch;
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    const sort = this.sortValue + '-' + this.sortType;
    this.dataService
      .post(`${this.url}/search?page=${this.selected}&sort=${sort}`, {})
      .subscribe((data: any) => {
        this.results = data.results;
        this.uiService.isLoading.next(false);
      }, (err) => {
        this.uiService.isLoading.next(false);
      });
  }
  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }
  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }
  pageChanged(data) {
    this.dataService
      .post(`${this.url}/search?page=${data}`, {})
      // tslint:disable-next-line: no-shadowed-variable
      .subscribe((data: IDataRes) => {
        this.results = data.results;
        this.uiService.isLoading.next(false);
      }, (err) => {
        this.uiService.isLoading.next(false);
      });
  }
}

