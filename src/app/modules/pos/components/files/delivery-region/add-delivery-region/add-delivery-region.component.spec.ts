import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDeliveryRegionComponent } from './add-delivery-region.component';

describe('AddDeliveryRegionComponent', () => {
  let component: AddDeliveryRegionComponent;
  let fixture: ComponentFixture<AddDeliveryRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDeliveryRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDeliveryRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
