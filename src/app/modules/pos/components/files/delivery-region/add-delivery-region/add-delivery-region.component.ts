import { Component, OnInit, ViewChild } from '@angular/core';
import { IDeliveryRegion } from 'src/app/modules/pos/interfaces/files/IDeliveryRegion';
import { DeliveryRegionApi, baseUrl, citiesApi, regionsApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { ICity } from 'src/app/modules/general/interfaces/ICity.model';
import { Subscription } from 'rxjs';
import { searchLength } from 'src/app/common/constants/general.constants';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IRegion } from 'src/app/modules/general/interfaces/IRegion.model';

@Component({
  selector: 'app-add-delivery-region',
  templateUrl: './add-delivery-region.component.html',
  styleUrls: ['./add-delivery-region.component.scss']
})
export class AddDeliveryRegionComponent implements OnInit {
  item: IDeliveryRegion = this.restFields();
  filesAdded = false;
  showEdit = false;

  url = DeliveryRegionApi;
  baseUrl = baseUrl;



  // Cities

  subscriptions: Subscription[] = [];
  cities: ICity[] = [];
  citiesInputFocused: boolean;
  hasMoreCities: boolean;
  citiesCount: number;
  selectedCitiesPage = 1;
  citiesPagesNo: number;
  noCities: boolean;

  // Regions
  regions: IRegion[] = [];
  regionsInputFocused: boolean;
  hasMoreRegions: boolean;
  regionsCount: number;
  selectedRegionsPage = 1;
  regionsPagesNo: number;
  noRegions: boolean;

  @ViewChild("dropzone") dropzone: any;
  detailsMode: boolean;

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.getData();
  }
  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.item.image = content as string;
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.item.image = "";
    this.filesAdded = false;
  }


  restFields() {
    return {
      _id: '',
      code: '',
      deliveryRegionNameAr: '',
      deliveryRegionNameEn: '',
      cityId: '',
      regionId: '',
      image: '',
      isActive: true
    };
  }
  submit() {
    if (this.item._id) {
      this.dataService
        .put(`${this.url}`, this.item)
        .subscribe(data => {
          this.item = this.restFields()
          this.uiService.showSuccess("GENERAL.updatedSuccessfully", "");

          this.uiService.isLoading.next(false);

          this.router.navigate(['/pos/deliveryRegion/']);
        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    } else {
      if (this.item.image == '') delete this.item.image
      delete this.item._id;
      this.dataService
        .post(`${this.url}`, this.item)
        .subscribe((data: IDeliveryRegion) => {
          this.item = this.restFields();
          this.filesAdded = false;
          this.uiService.showSuccess("GENERAL.addedSuccessfully", "");

          this.uiService.isLoading.next(false);

        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    }
  }
  getData() {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      if (id) {

        this.dataService.getByID(this.url, id).subscribe((data: IDeliveryRegion) => {
          this.item = data;
          this.getCities();
          this.getRegions();
          if (data.image) {
            this.item.image = this.baseUrl + data.image
          }
          this.uiService.isLoading.next(false);
        });
        this.showEdit = true;
      } else this.showEdit = false;

    });
  }







  getCities() {

    const searchQuery = {
      _id: this.item.cityId
    };

    this.dataService
      .get(citiesApi, null, searchQuery)
      .subscribe((res: IDataRes) => {
        if (!res.results.length) {
          this.noCities = true;
        } else {
          this.noCities = false;
          for (const item of res.results) {
            if (this.cities.length) {
              const uniqueCities = this.cities.filter(x => x._id !== item._id);
              this.cities = uniqueCities;
            }
            this.cities.push(item);
          }
        }
        this.uiService.isLoading.next(false);
      },
        err => {
          this.uiService.showErrorMessage(err);
        })



  }



  getRegions() {
    const searchQuery = {
      _id: this.item.regionId,
      cityId: this.item.cityId
    };

    this.dataService
      .get(regionsApi, null, searchQuery)
      .subscribe((res: IDataRes) => {
        if (!res.results.length) {
          this.noRegions = true;
        } else {
          this.noRegions = false;
          for (const item of res.results) {
            if (this.regions.length) {
              const uniqueRegions = this.regions.filter(x => x._id !== item._id);
              this.regions = uniqueRegions;
            }
            this.regions.push(item);
          }
        }
        this.regions = res.results;
        this.uiService.isLoading.next(false);
      })


  }













  searchCities(event) {
    const searchValue = event;
    const searchQuery = {
      cityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService
          .get(citiesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noCities = true;
            } else {
              this.noCities = false;
              for (const item of res.results) {
                if (this.cities.length) {
                  const uniqueCities = this.cities.filter(x => x._id !== item._id);
                  this.cities = uniqueCities;
                }
                this.cities.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          },
            err => {
              this.uiService.showErrorMessage(err);
            })
      );
    }
    if (searchValue.length <= 0) {
      this.noCities = false;
      this.citiesInputFocused = true;
    } else {
      this.citiesInputFocused = false;
    }
  }

  loadMoreCities() {
    this.selectedCitiesPage = this.selectedCitiesPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(citiesApi, this.selectedCitiesPage)
        .subscribe((res: IDataRes) => {
          if (this.citiesPagesNo > this.selectedCitiesPage) {
            this.hasMoreCities = true;
          } else {
            this.hasMoreCities = false;
          }
          for (const item of res.results) {
            if (this.cities.length) {
              const uniqueCities = this.cities.filter(x => x._id !== item._id);
              this.cities = uniqueCities;
            }
            this.cities.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }




  searchRegions(event) {
    const searchValue = event;
    const searchQuery = {
      regionNameAr: searchValue,
      cityId: this.item.cityId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService
          .get(regionsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noRegions = true;
            } else {
              this.noRegions = false;
              for (const item of res.results) {
                if (this.regions.length) {
                  const uniqueRegions = this.regions.filter(x => x._id !== item._id);
                  this.regions = uniqueRegions;
                }
                this.regions.push(item);
              }
            }
            this.regions = res.results;
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreRegions() {
    this.selectedRegionsPage = this.selectedRegionsPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(regionsApi, this.selectedRegionsPage)
        .subscribe((res: IDataRes) => {
          if (this.regionsPagesNo > this.selectedRegionsPage) {
            this.hasMoreRegions = true;
          } else {
            this.hasMoreRegions = false;
          }
          for (const item of res.results) {
            if (this.regions.length) {
              const uniqueRegions = this.regions.filter(x => x._id !== item._id);
              this.regions = uniqueRegions;
            }
            this.regions.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

}
