import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryRegionComponent } from './delivery-region.component';

describe('DeliveryRegionComponent', () => {
  let component: DeliveryRegionComponent;
  let fixture: ComponentFixture<DeliveryRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
