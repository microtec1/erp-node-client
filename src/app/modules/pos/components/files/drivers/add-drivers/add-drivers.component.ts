import { Component, OnInit, ViewChild } from '@angular/core';
import { Params, ActivatedRoute, RouterStateSnapshot, Router } from '@angular/router';
import { IDriver } from 'src/app/modules/pos/interfaces/files/IDriver';
import { baseUrl, DriversApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';

@Component({
  selector: 'app-add-drivers',
  templateUrl: './add-drivers.component.html',
  styleUrls: ['./add-drivers.component.scss']
})
export class AddDriversComponent implements OnInit {

  item: IDriver = this.restFields();
  filesAdded = false;
  showEdit = false;

  url = DriversApi;
  baseUrl = baseUrl;

  @ViewChild('dropzone') dropzone: any;

  detailsMode: boolean;

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.getData();
  }
  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.item.image = content as string;
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.item.image = '';
    this.filesAdded = false;
  }


  restFields() {
    return {
      _id: '',
      code: '',
      driverNameAr: '',
      driverNameEn: '',
      cellPhone: '',
      phone: '',
      address: '',
      image: '',
      isActive: true
    };
  }
  submit() {
    if (this.item._id) {
      this.dataService
        .put(`${this.url}`, this.item)
        .subscribe(data => {
          this.item = this.restFields();
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');

          this.uiService.isLoading.next(false);

          this.router.navigate(['/pos/drivers/']);
        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    } else {
      if (this.item.image === '') { delete this.item.image; }
      delete this.item._id;
      this.dataService
        .post(`${this.url}`, this.item)
        .subscribe((data: IDriver) => {
          this.item = this.restFields();
          this.filesAdded = false;
          this.uiService.showSuccess('GENERAL.addedSuccessfully', '');

          this.uiService.isLoading.next(false);
        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    }
  }
  getData() {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      if (id) {
        this.dataService.getByID(this.url, id).subscribe((data: IDriver) => {
          this.item = data;
          if (data.image) {
            this.item.image = this.baseUrl + data.image;
          }
          this.uiService.isLoading.next(false);
        });
        this.showEdit = true;
      } else { this.showEdit = false; }

    });
  }
}
