import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFloorSectionComponent } from './add-floor-section.component';

describe('AddFloorSectionComponent', () => {
  let component: AddFloorSectionComponent;
  let fixture: ComponentFixture<AddFloorSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFloorSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFloorSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
