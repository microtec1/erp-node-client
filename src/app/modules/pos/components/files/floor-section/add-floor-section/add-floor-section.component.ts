import { Component, OnInit, ViewChild } from '@angular/core';
import { IFloorSection } from 'src/app/modules/pos/interfaces/files/IFloorSection';
import { baseUrl, SectionsApi, companiesApi } from 'src/app/common/constants/api.constants';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import {
  ActivatedRoute,
  Router,
  RouterStateSnapshot,
  Params
} from '@angular/router';
import { companyId } from 'src/app/common/constants/general.constants';

@Component({
  selector: 'app-add-floor-section',
  templateUrl: './add-floor-section.component.html',
  styleUrls: ['./add-floor-section.component.scss']
})
export class AddFloorSectionComponent implements OnInit {
  item: IFloorSection = this.restFields();

  pointType: any = 'main';
  filesAdded = false;
  showEdit = false;

  url = SectionsApi;
  baseUrl = baseUrl;

  subscriptions: Subscription[] = [];

  @ViewChild('dropzone') dropzone: any;
  detailsMode: boolean;
  branchList: any[];
  tableListLength: number = 0;

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.getData();
    this.getBranches();
  }
  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.item.image = content as string;
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.item.image = '';
    this.filesAdded = false;
  }

  restFields() {
    return {
      _id: '',
      code: '',
      sectionNameAr: '',
      sectionNameEn: '',
      branchId: '',
      tableList: [
        {
          code: '',
          tableNameAr: '',
          tableNameEn: '',
          numberOfChairs: 0,
          isActive: true
        }
      ],
      image: '',
      isActive: true
    };
  }
  submit() {
    if (this.item._id) {
      this.dataService.put(`${this.url}`, this.item).subscribe(
        data => {
          this.item = this.restFields();
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');

          this.uiService.isLoading.next(false);

          this.router.navigate(['/pos/floorOrSection/']);
        },
        err => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        }
      );
    } else {
      if (this.item.image === '') {
        delete this.item.image;
      }
      delete this.item._id;
      this.dataService.post(`${this.url}`, this.item).subscribe(
        (data: IFloorSection) => {
          this.item = this.restFields();
          this.filesAdded = false;
          this.uiService.showSuccess('GENERAL.addedSuccessfully', '');

          this.uiService.isLoading.next(false);
        },
        err => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        }
      );
    }
  }
  getData() {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      if (id) {
        this.dataService
          .getByID(this.url, id)
          .subscribe((data: IFloorSection) => {
            this.item = data;

            if (data.image) {
              this.item.image = this.baseUrl + data.image;
            }
            this.tableListLength = this.item.tableList.length;
            this.uiService.isLoading.next(false);
          });
        this.showEdit = true;
      } else { this.showEdit = false; }
    });
  }

  addSub() {
    this.item.tableList.push({
      isActive: true,
      code: '',
      tableNameAr: '',
      tableNameEn: '',
      numberOfChairs: 0
    });
  }
  deleteItemFromArray(i) {

    if (i >= this.tableListLength) {
      this.item.tableList.splice(i, 1)
    } else {
      this.item.tableList.splice(i, 1);
      this.tableListLength-=1
    }

  }





  getBranches() {
    this.dataService.http
      .get(`${companiesApi}/${companyId}`)
      .subscribe((data: any) => {
        this.branchList = data.branches;
      });
  }
}
