import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMediaMethodComponent } from './add-media-method.component';

describe('AddMediaMethodComponent', () => {
  let component: AddMediaMethodComponent;
  let fixture: ComponentFixture<AddMediaMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMediaMethodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMediaMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
