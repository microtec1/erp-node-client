import { Component, OnInit, ViewChild } from '@angular/core';
import { IMediaMethod } from 'src/app/modules/pos/interfaces/files/IMediaMethod';
import { MediaMethodApi, baseUrl } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';

@Component({
  selector: 'app-add-media-method',
  templateUrl: './add-media-method.component.html',
  styleUrls: ['./add-media-method.component.scss']
})
export class AddMediaMethodComponent implements OnInit {


  item: IMediaMethod = this.restFields();
  filesAdded: boolean = false;
  showEdit = false;

  url = MediaMethodApi;
  baseUrl = baseUrl;


  detailsMode: boolean;


  @ViewChild("dropzone") dropzone: any;

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.getData();
  }
  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.item.image = content as string;
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.item.image = "";
    this.filesAdded = false;
  }


  restFields() {
    return {
      _id: '',
      code: '',
      mediaMethodNameAr: '',
      mediaMethodNameEn: '',
      image: '',
      isActive: true
    };
  }
  submit() {
    if (this.item._id) {
      this.dataService
        .put(`${this.url}`, this.item)
        .subscribe(data => {
          this.item = this.restFields()
          this.uiService.showSuccess("GENERAL.updatedSuccessfully", "");

          this.uiService.isLoading.next(false);
          this.router.navigate(['/pos/mediaMethods/']);
        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    } else {
      if (this.item.image == '') delete this.item.image
      delete this.item._id;
      this.dataService
        .post(`${this.url}`, this.item)
        .subscribe((data: IMediaMethod) => {
          this.item = this.restFields();
          this.filesAdded = false;
          this.uiService.showSuccess("GENERAL.addedSuccessfully", "");

          this.uiService.isLoading.next(false);

        }, (err) => {
          this.uiService.showErrorMessage(err);

          this.uiService.isLoading.next(false);
        });
    }
  }
  getData() {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      if (id) {
        this.dataService.getByID(this.url, id).subscribe((data: IMediaMethod) => {
          this.item = data;

          if( data.image){
            this.item.image =this.baseUrl + data.image
          }
          this.uiService.isLoading.next(false);
        });
        this.showEdit = true;
      } else this.showEdit = false;

    });
  }
}
