import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaMethodComponent } from './media-method.component';

describe('MediaMethodComponent', () => {
  let component: MediaMethodComponent;
  let fixture: ComponentFixture<MediaMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaMethodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
