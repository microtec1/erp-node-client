import { Component, OnInit, ViewChild } from '@angular/core';
import { OperatorsApi, baseUrl, detailedChartOfAccountsApi, usersApi } from 'src/app/common/constants/api.constants';
import { IOperators } from 'src/app/modules/pos/interfaces/files/IOperators';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IUser } from 'src/app/modules/general/interfaces/IUser.model';
import { companyId } from 'src/app/common/constants/general.constants';

@Component({
  selector: 'app-add-operators',
  templateUrl: './add-operators.component.html',
  styleUrls: ['./add-operators.component.scss']
})
export class AddOperatorsComponent implements OnInit {


  item: IOperators = this.restFields();
  filesAdded: boolean = false;
  showEdit = false;

  url = OperatorsApi;
  baseUrl = baseUrl;

  @ViewChild("dropzone") dropzone: any;



  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;


  // Chart of Accounts
  userList: IUser[] = [];
  userListInputFocused: boolean;
  userListCount: number;
  noUserList: boolean;


  detailsMode: boolean;

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }

    this.chartOfAccountsList();
    this.usersList();
    this.getData();

  }

  chartOfAccountsList() {
    this.dataService.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
      if (res.length) {
        this.chartOfAccounts.push(...res);
        this.chartOfAccountsCount = res.length;
      } else {
        this.noChartOfAccounts = true;
      }
      this.uiService.isLoading.next(false);
    })
  }


  usersList() {
    this.dataService.get(usersApi).subscribe((res: any) => {
      if (res.results.length) {
        this.userList.push(...res.results);
        this.userListCount = res.results.length;
      } else {
        this.noUserList = true;
      }
      this.uiService.isLoading.next(false);
    })
  }


  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.item.image = content as string;
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.item.image = "";
    this.filesAdded = false;
  }


  restFields() {
    return {
      _id: '',
      code: '',
      operatorNameAr: '',
      operatorNameEn: '',
      glAccountId: null,
      userId: '',
      image: '',
      isActive: true
    };
  }
  submit() {
    if (this.item._id) {
      this.dataService
        .put(`${this.url}`, this.item)
        .subscribe(data => {
          this.item = this.restFields()
          this.uiService.showSuccess("GENERAL.updatedSuccessfully", "");

          this.uiService.isLoading.next(false);
          this.router.navigate(['/pos/operators/']);
        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    } else {
      if (this.item.image == '') delete this.item.image
      delete this.item._id;
      this.dataService
        .post(`${this.url}`, this.item)
        .subscribe((data: IOperators) => {
          this.item = this.restFields();
          this.filesAdded = false;
          this.uiService.showSuccess("GENERAL.addedSuccessfully", "");

          this.uiService.isLoading.next(false);

        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    }
  }
  getData() {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      if (id) {
        this.dataService.getByID(this.url, id).subscribe((data: IOperators) => {
          this.item = data;

          if( data.image){
            this.item.image =this.baseUrl + data.image
          }
          this.uiService.isLoading.next(false);
        });
        this.showEdit = true;
      } else {
        this.showEdit = false
      };

    });
  }
}
