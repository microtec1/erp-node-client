import { Component, OnInit, ViewChild } from '@angular/core';
import { IPromotions } from 'src/app/modules/pos/interfaces/files/IPromotions';
import { PromotionsApi, baseUrl, storesItemsApi, itemsGroupsApi } from 'src/app/common/constants/api.constants';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';

import { FormGroup } from '@angular/forms';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IItemsGroup } from 'src/app/modules/inventory/interfaces/IItemsGroup';
import { GeneralService } from 'src/app/modules/general/services/general.service';
@Component({
  selector: 'app-add-promotions',
  templateUrl: './add-promotions.component.html',
  styleUrls: ['./add-promotions.component.scss']
})
export class AddPromotionsComponent implements OnInit {

  promotionsForm: FormGroup;
  item: IPromotions = this.restFields();

  pointType: any = 'main';
  filesAdded = false;
  showEdit = false;

  // Items Groups
  itemsGroups: IItemsGroup[] = [];
  itemsGroupsInputFocused: boolean;
  hasMoreItemsGroups: boolean;
  itemsGroupsCount: number;
  selectedItemsGroupsPage = 1;
  itemsGroupsPagesNo: number;
  noItemsGroups: boolean;


  // Items Groups
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;


  // ExtraPointsOnItems
  selectedItemExtraPointsOnItems: any;











  url = PromotionsApi;
  baseUrl = baseUrl;

  subscriptions: Subscription[] = [];


  @ViewChild('dropzone') dropzone: any;
  detailsMode: boolean;
  branchList: any;
  customersList: any;
  itemGroupList: [];

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private route: ActivatedRoute,
    private generalService: GeneralService,
    private router: Router
  ) { }

  ngOnInit() {

    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.getData();
  }

  searchItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      itemsGroupNameAr: searchValue
    };
   
    if (searchValue.length >= searchLength) {
    
      this.subscriptions.push(
        this.dataService
          .get(itemsGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noItemsGroups = true;
            } else {
              this.noItemsGroups = false;
              for (const item of res.results) {
                if (this.itemsGroups.length) {
                  const uniqueItemsGroups = this.itemsGroups.filter(
                    x => x._id !== item._id
                  );
                  this.itemsGroups = uniqueItemsGroups;
                }
                this.itemsGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreItemsGroups() {
    this.selectedItemsGroupsPage = this.selectedItemsGroupsPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(itemsGroupsApi, this.selectedItemsGroupsPage)
        .subscribe((res: IDataRes) => {
          if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
            this.hasMoreItemsGroups = true;
          } else {
            this.hasMoreItemsGroups = false;
          }
          for (const item of res.results) {
            if (this.itemsGroups.length) {
              const uniqueItemsGroups = this.itemsGroups.filter(
                x => x._id !== item._id
              );
              this.itemsGroups = uniqueItemsGroups;
            }
            this.itemsGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }










  searchIStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      '$or': [{
        itemsNameAr: searchValue,
        code: searchValue,
        'barCode.barCode': searchValue,
      }]
    };
    if (searchValue.length >= searchLength) {
      console.log(111111111111);
      
      this.subscriptions.push(
        this.dataService
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  console.log(uniqueStoreItems)
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedItemsGroupsPage = this.selectedItemsGroupsPage + 1;
    this.subscriptions.push(
      this.dataService
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }








  // Types

  addToList(type, itemId) {
    const found = this.item[type].itemsList.find(el => {
      return el.itemId == itemId[0].value
    });


    if (itemId.length > 0 && typeof found == 'undefined' &&  itemId[0].value != null) {

      this.item[type].itemsList.push(
        {
          typeOfProduct: 'store',
          itemId: itemId[0].value,
          points: 0,
          maximumQuantity: 0,
        }
      )

    }

  }







  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.item.image = content as string;
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }


  removeImage() {
    this.item.image = '';
    this.filesAdded = false;
  }

  restFields() {
    return {
      _id: '',
      code: '',
      promotionNameAr: '',
      promotionNameEn: '',
      fromGregorianDate: new Date(),
      fromHijriDate: '',
      toGregorianDate: new Date(),
      toHijriDate: '',
      allBranches: true,
      branchesList: [],
      allCustomers: true,
      customersList: [],
      promotionType: '',
      discountOnInvoice: {
        fromPrice: 0,
        toPrice: 0,
        discountType: 'percentage',
        discount: 0
      },
      extraPointsOnItems: {
        points: 0,
        maximumQuantity: 0,
        itemsList: []
      },
      freeItemOnInvoiceValue: {
        fromPrice: 0,
        toPrice: 0,
        itemsList: [{
          typeOfProduct: 'store',
          itemId: null,
          freeQuantity: 0,
        }]
      },
      pricing: {
        discountPercentage: 0,
        discountValue: 0,
        maximumQuantity: 0,
        specificTiming: true,
        from: '',
        to: '',
        itemsList: [{
          typeOfProduct: 'store',
          itemId: null,
          discountPercentage: 0,
          discountValue: 0,
          netValue: 0,
          maximumQuantity: 0,
        }]
      },
      freeQuantity: {
        limitQuantity: 0,
        freeQuantity: 0,
        maximumQuantity: 0,
        itemsList: [{
          typeOfProduct: 'store',
          itemId: null,
          limitQuantity: 0,
          freeQuantity: 0,
          maximumQuantity: 0,
        }]
      },
      conditionalPricingInQuantity: {
        discountPercentage: 0,
        limitQuantity: 0,
        reducedQuantity: 0,
        maximumQuantity: 0,
        itemsList: [{
          typeOfProduct: 'store',
          itemId: null,
          discountPercentage: 0,
          limitQuantity: 0,
          reducedQuantity: 0,
          maximumQuantity: 0,
        }]
      },
      freeCouponOnTheBillValue: {
        fromPrice: 0,
        toPrice: 0,
        couponValue: 0,
        fromGregorianDate: new Date(),
        fromHijriDate: '',
        toGregorianDate: new Date(),
        toHijriDate: '',
      },
      image: '',
      isActive: true

    };
  }

  restPromotionData() {
    this.item.discountOnInvoice = {
      fromPrice: 0,
      toPrice: 0,
      discountType: 'percentage',
      discount: 0
    };

    this.item.extraPointsOnItems = {
      points: 0,
      maximumQuantity: 0,
      itemsList: []
    };
    this.item.freeItemOnInvoiceValue = {
      fromPrice: 0,
      toPrice: 0,
      itemsList: [{
        typeOfProduct: 'store',
        itemId: null,
        freeQuantity: 0,
      }]
    };
    this.item.pricing = {
      discountPercentage: 0,
      discountValue: 0,
      maximumQuantity: 0,
      specificTiming: true,
      from: '',
      to: '',
      itemsList: [{
        typeOfProduct: 'store',
        itemId: null,
        discountPercentage: 0,
        discountValue: 0,
        netValue: 0,
        maximumQuantity: 0,
      }]
    };
    this.item.freeQuantity = {
      limitQuantity: 0,
      freeQuantity: 0,
      maximumQuantity: 0,
      itemsList: [{
        typeOfProduct: 'store',
        itemId: null,
        limitQuantity: 0,
        freeQuantity: 0,
        maximumQuantity: 0,
      }]
    };
    this.item.conditionalPricingInQuantity = {
      discountPercentage: 0,
      limitQuantity: 0,
      reducedQuantity: 0,
      maximumQuantity: 0,
      itemsList: [{
        typeOfProduct: 'store',
        itemId: null,
        discountPercentage: 0,
        limitQuantity: 0,
        reducedQuantity: 0,
        maximumQuantity: 0,
      }]
    };
    this.item.freeCouponOnTheBillValue = {
      fromPrice: 0,
      toPrice: 0,
      couponValue: 0,
      fromGregorianDate: new Date(),
      fromHijriDate: '',
      toGregorianDate: new Date(),
      toHijriDate: '',
    };

  }

  changeGregorianDate(date) {
    const fullDate = `${new Date(date).getFullYear()}/${new Date(date).getMonth()}/${new Date(date).getDate()}`;
    return fullDate;
  }

  changeHijriDate(date) {
    const fullDate = `${date.year}/${date.month}/${date.day}`;
    return fullDate;
  }


  submit() {
    // this.item.fromGregorianDate=this.changeGregorianDate(this.item.fromGregorianDate)
    // this.item.fromHijriDate=this.changeHijriDate(this.item.fromHijriDate)
    // this.item.toGregorianDate=this.changeGregorianDate(this.item.toGregorianDate)
    // this.item.toHijriDate=this.changeHijriDate(this.item.toHijriDate)

    this.item.fromGregorianDate = "2019/09/17";
    this.item.fromHijriDate = "1441/01/18";
    this.item.toGregorianDate = "2019/09/18";
    this.item.toHijriDate = "1441/01/19";


    if (this.item._id) {
      this.dataService
        .put(`${this.url}`, this.item)
        .subscribe(data => {
          this.item = this.restFields();
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          this.uiService.isLoading.next(false);
          this.router.navigate(['/pos/promotions/']);
        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    } else {
      if (this.item.image === '') { delete this.item.image; }
      delete this.item._id;
      this.dataService
        .post(`${this.url}`, this.item)
        .subscribe((data: IPromotions) => {
          this.item = data;
          this.filesAdded = false;
          this.uiService.showSuccess('GENERAL.addedSuccessfully', '');

          this.uiService.isLoading.next(false);
        }, (err) => {
          this.uiService.showErrorMessage(err);
          this.uiService.isLoading.next(false);
        });
    }
  }

  getData() {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      if (id) {

        this.dataService.getByID(this.url, id).subscribe((data: IPromotions) => {
          this.item = data;

          this.item.image = data.image === '' ? data.image : this.baseUrl + data.image;
          this.uiService.isLoading.next(false);
        });
        this.showEdit = true;
      } else { this.showEdit = false; }

    });
  }



  setFromHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);

      this.item.fromHijriDate = {
        year: hijriDate.iYear(),
        month: hijriDate.iMonth() + 1,
        day: hijriDate.iDate()
      }

    }
  }


  setFromGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.item.fromGregorianDate = this.generalService.format(
        new Date(
          gegorianDate.year(),
          gegorianDate.month(),
          gegorianDate.date()
        )
      )
    }
  }


  setToHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);

      this.item.toHijriDate = {
        year: hijriDate.iYear(),
        month: hijriDate.iMonth() + 1,
        day: hijriDate.iDate()
      }

    }
  }


  setToGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.item.toGregorianDate = this.generalService.format(
        new Date(
          gegorianDate.year(),
          gegorianDate.month(),
          gegorianDate.date()
        )
      )
    }
  }















  getBranches() {
    this.dataService.http.get(`${this.baseUrl}api/general/files/companies/${companyId}`).subscribe((data: any) => {
      this.branchList = data.branches;
    });
  }

  getAllCustomers() {
    this.dataService.http.get(`${this.baseUrl}api/sales/files/customersGroups/${companyId}`).subscribe((data: any) => {
      this.customersList = data.results;
    });
  }

  getAllItemsGroup() {
    this.dataService.http.get(`${this.baseUrl}api/inventory/files/itemsGroups/${companyId}`).subscribe((data: any) => {
      this.itemGroupList = data.results;
    });
  }

}


