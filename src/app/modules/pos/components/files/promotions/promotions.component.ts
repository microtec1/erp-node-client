import { Component, OnInit } from '@angular/core';
import { IPromotions } from '../../../interfaces/files/IPromotions';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PromotionsApi, baseUrl } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { IDriver } from '../../../interfaces/files/IDriver';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.scss']
})
export class PromotionsComponent implements OnInit {
  bsModalRef: BsModalRef;
  pageInfo;
  showSearch: boolean;
  results = [];
  count;
  searchItem: any = {
    code: '',
    promotionNameAr: '',
    promotionNameEn: ''
  };
  sortType = 'asc';
  sortValue: string;
  formReady = false;
  selected = 1;
  url = PromotionsApi;
  baseUrl = baseUrl;

  constructor(
    private dataService: DataService,
    private uiService: UiService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.getAllData();
  }

  getAllData() {
    this.dataService.post(`${this.url}/search`, {}).subscribe((data: any) => {
      this.count = data.count;
      this.pageInfo = data;
      this.results = data.results;
      this.formReady = true;
      this.uiService.isLoading.next(false);
    });
  }

  deleteModal(data: IDriver) {
    const initialState = {
      code: data.code,
      nameAr: data.driverNameAr,
      nameEn: data.driverNameEn
    };

    this.bsModalRef = this.modalService.show(ConfirmModalComponent, {
      initialState,
      class: 'confirm-modal'
    });
    this.bsModalRef.content.confirmed.subscribe(confirmed => {
      if (confirmed) {
        this.dataService
          .delete(`${this.url}`, data._id)
          .subscribe(result => {
            this.getAllData();
            this.bsModalRef.hide();
          });
      } else {
        this.bsModalRef.hide();
      }
    });
  }

  clear() {
    this.searchItem = {
      code: '',
      romotionNameAr: '',
      promotionNameEn: ''
    };
  }

  search(data) {
    this.dataService.post(`${this.url}/search`, data).subscribe((data: any) => {
      this.results = data.results;
      this.sortType = 'asc';
      this.formReady = true;
      this.uiService.isLoading.next(false);
    });
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    const sort = this.sortValue + '-' + this.sortType;
    this.dataService
      .post(`${this.url}/search?page=${this.selected}&sort=${sort}`, {})
      .subscribe((data: any) => {
        this.results = data.results;
        this.uiService.isLoading.next(false);
      });
  }
  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }
  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }
  pageChanged(data) {
    this.dataService
      .post(`${this.url}/search?page=${data}`, {})
      .subscribe((data: any) => {
        this.results = data.results;
        this.uiService.isLoading.next(false);
      });
  }
}
