import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRewardsProgramCustomerComponent } from './add-rewards-program-customer.component';

describe('AddRewardsProgramCustomerComponent', () => {
  let component: AddRewardsProgramCustomerComponent;
  let fixture: ComponentFixture<AddRewardsProgramCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRewardsProgramCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRewardsProgramCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
