import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardsProgramCustomerComponent } from './rewards-program-customer.component';

describe('RewardsProgramCustomerComponent', () => {
  let component: RewardsProgramCustomerComponent;
  let fixture: ComponentFixture<RewardsProgramCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardsProgramCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsProgramCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
