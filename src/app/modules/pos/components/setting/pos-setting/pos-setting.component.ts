import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/common/services/shared/data.service';
import { IPosSetting } from '../../../interfaces/setting/IPosSetting';
import { baseUrl, PosSettingApi, customersApi } from 'src/app/common/constants/api.constants';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { searchLength } from 'src/app/common/constants/general.constants';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pos-setting',
  templateUrl: './pos-setting.component.html',
  styleUrls: ['./pos-setting.component.scss']
})
export class PosSettingComponent implements OnInit {

  item: IPosSetting;
  url = PosSettingApi;
  baseUrl = baseUrl;

  subscriptions: Subscription[] = [];

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  constructor(
    private dataService: DataService,
    private uiService: UiService
  ) { }

  ngOnInit() {
    this.dataService.post(`${this.url}/getSettings`, {}).subscribe((data: IPosSetting) => {

      this.item = {
        branch: {
          branchId: data.branch.branchId,
          endDayTime: data.branch.endDayTime,
          requestType: data.branch.requestType,
          approximationFinalSum: data.branch.approximationFinalSum,
          maximumReturnTime: data.branch.maximumReturnTime,
          defaultPOSCustomer: data.branch.defaultPOSCustomer,
          typePos: data.branch.typePos,
          priceOfProductsInInvoicePrinting: data.branch.priceOfProductsInInvoicePrinting,
          activateRewardsProgram: data.branch.activateRewardsProgram,
          alwaysAskWhyCanceled: data.branch.alwaysAskWhyCanceled,
          cancellationRequiresCustomerInformation: data.branch.cancellationRequiresCustomerInformation,
          mandatoryTableSelectionAndNumberPeopleInLocalOrders: data.branch.mandatoryTableSelectionAndNumberPeopleInLocalOrders,
          activateEmailEndOfDayReport: data.branch.activateEmailEndOfDayReport,
          EmailEndOfDayReport: data.branch.EmailEndOfDayReport,
          activateEmailEndOfDurationReport: data.branch.activateEmailEndOfDurationReport,
          EmailEndOfDurationReport: data.branch.EmailEndOfDurationReport,
        }
      };
      this.searchCustomers2(data.branch.defaultPOSCustomer)



      this.uiService.isLoading.next(false);
    }, (err) => {
      this.uiService.showErrorMessage(err);
      this.uiService.isLoading.next(false);
    });
  }
  submit() {

    if (!this.item.branch.activateEmailEndOfDayReport) delete this.item.branch.EmailEndOfDayReport
    if (!this.item.branch.activateEmailEndOfDurationReport) delete this.item.branch.EmailEndOfDurationReport
    this.dataService.post(`${this.url}`, this.item).subscribe((data: IPosSetting) => {
      this.item = {
        branch: {
          branchId: data.branch.branchId,
          endDayTime: data.branch.endDayTime,
          requestType: data.branch.requestType,
          approximationFinalSum: data.branch.approximationFinalSum,
          maximumReturnTime: data.branch.maximumReturnTime,
          defaultPOSCustomer: data.branch.defaultPOSCustomer,
          typePos: data.branch.typePos,
          priceOfProductsInInvoicePrinting: data.branch.priceOfProductsInInvoicePrinting,
          activateRewardsProgram: data.branch.activateRewardsProgram,
          alwaysAskWhyCanceled: data.branch.alwaysAskWhyCanceled,
          cancellationRequiresCustomerInformation: data.branch.cancellationRequiresCustomerInformation,
          mandatoryTableSelectionAndNumberPeopleInLocalOrders: data.branch.mandatoryTableSelectionAndNumberPeopleInLocalOrders,
          activateEmailEndOfDayReport: data.branch.activateEmailEndOfDayReport,
          EmailEndOfDayReport: data.branch.EmailEndOfDayReport,
          activateEmailEndOfDurationReport: data.branch.activateEmailEndOfDurationReport,
          EmailEndOfDurationReport: data.branch.EmailEndOfDurationReport,
        }
      };
      this.uiService.showSuccess("GENERAL.updatedSuccessfully", "");
      this.uiService.isLoading.next(false);
    }, (err) => {
      this.uiService.showErrorMessage(err);
      this.uiService.isLoading.next(false);
    });
  }



  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }




  searchCustomers2(event) {
    const searchValue = event;
    const searchQuery = {
      _id: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.dataService.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.dataService.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }
}
