export interface IDefinitionOfPos {
  _id?: string;
  code: string;
  pointOfSaleNameAr: string;
  pointOfSaleNameEn: string;
  warehouseId?: string;
  safeBoxId?: string;
  servicesItemsGroupList?: {
    servicesItemsGroupId: string;
    notes: boolean;
  }[];
  itemsGroupList?: string[];
  subPointList?: {
    isActive: boolean;
    code: string;
    subPointNameAr: string;
    subPointNameEn: string;
    safeBoxId: string;
    bankId: string;
    paidType: string[];
    printerRequests: string;
    printerExtraRequests: string;
    numberPrintCopies: number;
  }[];
  image?: string;
  isActive?: boolean;
}
