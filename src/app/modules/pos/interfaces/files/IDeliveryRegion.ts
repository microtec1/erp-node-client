export interface IDeliveryRegion {
  _id?: string;
  code: string;
  deliveryRegionNameAr: string;
  deliveryRegionNameEn: string;
  cityId?: string;
  regionId?: string;
  image?: string;
  isActive?: boolean;
}
