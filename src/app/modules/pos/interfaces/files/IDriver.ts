export interface IDriver {
  _id: string;
  code: string;
  driverNameAr: string;
  driverNameEn: string;
  cellPhone: string;
  phone: string;
  address: string;
  image: string;
  isActive: boolean;
}
