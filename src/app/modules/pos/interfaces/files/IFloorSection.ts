export interface IFloorSection {
  _id: string;
  code: string;
  sectionNameAr: string;
  sectionNameEn: string;
  branchId: string;
  tableList: {
    code: string,
    tableNameAr: string,
    tableNameEn: string,
    numberOfChairs: number,
    isActive: boolean
  }[];
  image: string;
  isActive: boolean;
}
