export interface IMediaMethod {
  _id?: string;
  code: string;
  mediaMethodNameAr: string;
  mediaMethodNameEn: string;
  image?: string;
  isActive?: boolean;
}
