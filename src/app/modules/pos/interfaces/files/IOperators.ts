export interface IOperators {
  _id?: string;
  code: string;
  operatorNameAr: string;
  operatorNameEn: string;
  glAccountId?: string;
  userId?: string;
  image?: string;
  isActive?: boolean;
}
