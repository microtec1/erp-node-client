export interface IPromotions {
  _id?: string;
  code: string;
  promotionNameAr: string;
  promotionNameEn: string;
  fromGregorianDate?: any;
  fromHijriDate?: any;
  toGregorianDate?: any;
  toHijriDate?: any;
  allBranches?: boolean;
  branchesList?: string[];
  allCustomers?: boolean;
  customersList?: string[];
  promotionType?: any;
  discountOnInvoice?: {
    fromPrice: number,
    toPrice: number,
    discountType: string,
    discount: number
  };
  extraPointsOnItems?: {
    points: number,

    maximumQuantity: number,
    itemsList: {
      typeOfProduct: string,
      itemId: string,
      points: number,
      maximumQuantity: number,
    }[]
  };
  freeItemOnInvoiceValue?: {
    fromPrice: number,
    toPrice: number,
    itemsList: {
      typeOfProduct: string,
      itemId: string,
      freeQuantity: number,
    }[]
  };
  pricing?: {
    discountPercentage: number,
    discountValue: number,
    maximumQuantity: number,
    specificTiming: boolean,
    from: string,
    to: string,
    itemsList: {
      typeOfProduct: string,
      itemId: string,
      discountPercentage: number,
      discountValue: number,
      netValue: number,
      maximumQuantity: number,
    }[]
  };
  freeQuantity?: {
    limitQuantity: number,
    freeQuantity: number,
    maximumQuantity: number,
    itemsList: {
      typeOfProduct: string,
      itemId: string,
      limitQuantity: number,
      freeQuantity: number,
      maximumQuantity: number,
    }[]
  };
  conditionalPricingInQuantity?: {
    discountPercentage: number,
    limitQuantity: number,
    reducedQuantity: number,
    maximumQuantity: number,
    itemsList: {
      typeOfProduct: string,
      itemId: string,
      discountPercentage: number,
      limitQuantity: number,
      reducedQuantity: number,
      maximumQuantity: number,
    }[]
  };
  freeCouponOnTheBillValue?: {
    fromPrice: number,
    toPrice: number,
    couponValue: number,
    fromGregorianDate: any,
    fromHijriDate: any,
    toGregorianDate: any,
    toHijriDate: any,
  };
  image?: string;
  isActive?: boolean;
}
