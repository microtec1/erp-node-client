export interface IRewardsProgram {
  _id?: string;
  code: string;
  rewardsProgramNameAr: string;
  rewardsProgramNameEn: string;
  profitPrice?: number;
  profitPoint?: number;
  profitDay?: number;
  exchangePrice?: number;
  exchangePoint?: number;
  exchangeLimitPoint?: number;
  image?: string;
  isActive?: boolean;
}
