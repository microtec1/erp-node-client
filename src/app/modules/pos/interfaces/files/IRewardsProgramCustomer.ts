export interface IRewardsProgramCustomer {
  _id?: string;
  code: string;
  rewardsProgramCustomerNameAr: string;
  rewardsProgramCustomerNameEn: string;
  phone?: string;
  points?: number;
  image?: string;
  isActive?: boolean;
}
