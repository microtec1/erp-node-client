export interface IPosSetting {
  branch: {
    branchId: String;
    endDayTime: String;
    requestType: String;
    approximationFinalSum: String;
    maximumReturnTime: Number;
    defaultPOSCustomer: String;
    typePos: String;
    priceOfProductsInInvoicePrinting: String;
    activateRewardsProgram: Boolean;
    alwaysAskWhyCanceled: Boolean;
    cancellationRequiresCustomerInformation: Boolean;
    mandatoryTableSelectionAndNumberPeopleInLocalOrders: Boolean;
    activateEmailEndOfDayReport: Boolean;
    EmailEndOfDayReport: String;
    activateEmailEndOfDurationReport: Boolean;
    EmailEndOfDurationReport: String;
  };
}
