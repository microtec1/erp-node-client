import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriversComponent } from './components/files/drivers/drivers.component';
import { AddDriversComponent } from './components/files/drivers/add-drivers/add-drivers.component';
import { OperatorsComponent } from './components/files/operators/operators.component';
import { AddOperatorsComponent } from './components/files/operators/add-operators/add-operators.component';
import { MediaMethodComponent } from './components/files/media-method/media-method.component';
import { AddMediaMethodComponent } from './components/files/media-method/add-media-method/add-media-method.component';
import { RewardsProgramCustomerComponent } from './components/files/rewards-program-customer/rewards-program-customer.component';
import { AddRewardsProgramCustomerComponent } from './components/files/rewards-program-customer/add-rewards-program-customer/add-rewards-program-customer.component';
import { DeliveryRegionComponent } from './components/files/delivery-region/delivery-region.component';
import { AddDeliveryRegionComponent } from './components/files/delivery-region/add-delivery-region/add-delivery-region.component';
import { PosSettingComponent } from './components/setting/pos-setting/pos-setting.component';
import { DefinitionOfPosComponent } from './components/files/definition-of-pos/definition-of-pos.component';
import { AddDefinitionOfPosComponent } from './components/files/definition-of-pos/add-definition-of-pos/add-definition-of-pos.component';
import { PromotionsComponent } from './components/files/promotions/promotions.component';
import { AddPromotionsComponent } from './components/files/promotions/add-promotions/add-promotions.component';
import { RewardsProgramComponent } from './components/files/rewards-program/rewards-program.component';
import { AddRewardsProgramComponent } from './components/files/rewards-program/add-rewards-program/add-rewards-program.component';
import { FloorSectionComponent } from './components/files/floor-section/floor-section.component';
import { AddFloorSectionComponent } from './components/files/floor-section/add-floor-section/add-floor-section.component';


const routes: Routes = [

  { path: '', redirectTo: 'drivers', pathMatch: 'full' },
  { path: 'drivers', component: DriversComponent },
  { path: 'drivers/add', component: AddDriversComponent },
  { path: 'drivers/edit/:id', component: AddDriversComponent },
  { path: 'drivers/details/:id', component: AddDriversComponent },


  { path: 'operators', component: OperatorsComponent },
  { path: 'operators/add', component: AddOperatorsComponent },
  { path: 'operators/edit/:id', component: AddOperatorsComponent },
  { path: 'operators/details/:id', component: AddOperatorsComponent },

  { path: 'mediaMethods', component: MediaMethodComponent },
  { path: 'mediaMethods/add', component: AddMediaMethodComponent },
  { path: 'mediaMethods/edit/:id', component: AddMediaMethodComponent },
  { path: 'mediaMethods/details/:id', component: AddMediaMethodComponent },


  { path: 'rewardsProgramCustomer', component: RewardsProgramCustomerComponent },
  { path: 'rewardsProgramCustomer/add', component: AddRewardsProgramCustomerComponent },
  { path: 'rewardsProgramCustomer/edit/:id', component: AddRewardsProgramCustomerComponent },
  { path: 'rewardsProgramCustomer/details/:id', component: AddRewardsProgramCustomerComponent },


  { path: 'rewardsProgram', component: RewardsProgramComponent },
  { path: 'rewardsProgram/add', component: AddRewardsProgramComponent },
  { path: 'rewardsProgram/edit/:id', component: AddRewardsProgramComponent },
  { path: 'rewardsProgram/details/:id', component: AddRewardsProgramComponent },


  { path: 'deliveryRegion', component: DeliveryRegionComponent },
  { path: 'deliveryRegion/add', component: AddDeliveryRegionComponent },
  { path: 'deliveryRegion/edit/:id', component: AddDeliveryRegionComponent },
  { path: 'deliveryRegion/details/:id', component: AddDeliveryRegionComponent },

  { path: 'setting', component: PosSettingComponent },


  { path: 'definitionOfPos', component: DefinitionOfPosComponent },
  { path: 'definitionOfPos/add', component: AddDefinitionOfPosComponent },
  { path: 'definitionOfPos/edit/:id', component: AddDefinitionOfPosComponent },
  { path: 'definitionOfPos/details/:id', component: AddDefinitionOfPosComponent },

  { path: 'promotions', component: PromotionsComponent },
  { path: 'promotions/add', component: AddPromotionsComponent },
  { path: 'promotions/edit/:id', component: AddPromotionsComponent },
  { path: 'promotions/details/:id', component: AddPromotionsComponent },

  { path: 'floorOrSection', component: FloorSectionComponent },
  { path: 'floorOrSection/add', component: AddFloorSectionComponent },
  { path: 'floorOrSection/edit/:id', component: AddFloorSectionComponent },
  { path: 'floorOrSection/details/:id', component: AddFloorSectionComponent },





];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PosRoutingModule { }
