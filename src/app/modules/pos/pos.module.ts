import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PosRoutingModule } from './pos-routing.module';
import { DriversComponent } from './components/files/drivers/drivers.component';
import { AddDriversComponent } from './components/files/drivers/add-drivers/add-drivers.component';
import { OperatorsComponent } from './components/files/operators/operators.component';
import { AddOperatorsComponent } from './components/files/operators/add-operators/add-operators.component';
import { MediaMethodComponent } from './components/files/media-method/media-method.component';
import { AddMediaMethodComponent } from './components/files/media-method/add-media-method/add-media-method.component';
import { AddDeliveryRegionComponent } from './components/files/delivery-region/add-delivery-region/add-delivery-region.component';
import { DeliveryRegionComponent } from './components/files/delivery-region/delivery-region.component';
import { RewardsProgramCustomerComponent } from './components/files/rewards-program-customer/rewards-program-customer.component';
import { AddRewardsProgramCustomerComponent } from './components/files/rewards-program-customer/add-rewards-program-customer/add-rewards-program-customer.component';
import { PosSettingComponent } from './components/setting/pos-setting/pos-setting.component';
import { DefinitionOfPosComponent } from './components/files/definition-of-pos/definition-of-pos.component';
import { AddDefinitionOfPosComponent } from './components/files/definition-of-pos/add-definition-of-pos/add-definition-of-pos.component';
import { PromotionsComponent } from './components/files/promotions/promotions.component';
import { AddPromotionsComponent } from './components/files/promotions/add-promotions/add-promotions.component';
import { FloorSectionComponent } from './components/files/floor-section/floor-section.component';
import { AddFloorSectionComponent } from './components/files/floor-section/add-floor-section/add-floor-section.component';
import { RewardsProgramComponent } from './components/files/rewards-program/rewards-program.component';
import { AddRewardsProgramComponent } from './components/files/rewards-program/add-rewards-program/add-rewards-program.component';

@NgModule({
  declarations: [
    DriversComponent,
    AddDriversComponent,
    OperatorsComponent,
    AddOperatorsComponent,
    MediaMethodComponent,
    AddMediaMethodComponent,
    AddDeliveryRegionComponent,
    DeliveryRegionComponent,
    RewardsProgramCustomerComponent,
    AddRewardsProgramCustomerComponent,
    RewardsProgramComponent,
    AddRewardsProgramComponent,
    PosSettingComponent,
    DefinitionOfPosComponent,
    AddDefinitionOfPosComponent,
    PromotionsComponent,
    AddPromotionsComponent,
    FloorSectionComponent,
    AddFloorSectionComponent
  ],
  imports: [SharedModule, PosRoutingModule]
})
export class PosModule {}
