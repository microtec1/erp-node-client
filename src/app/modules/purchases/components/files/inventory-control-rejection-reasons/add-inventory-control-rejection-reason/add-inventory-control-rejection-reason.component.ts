import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, inventoryControlRejectionReasonsApi } from 'src/app/common/constants/api.constants';
import { IInventoryRejectionReason } from '../../../../interfaces/IInventoryRejectionReason.model';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-inventory-control-rejection-reason',
  templateUrl: './add-inventory-control-rejection-reason.component.html',
  styleUrls: ['./add-inventory-control-rejection-reason.component.scss']
})
export class AddInventoryControlRejectionReasonComponent implements OnInit, OnDestroy {
  inventoryControlRejectionReasonForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  inventoryControlRejectionReason: IInventoryRejectionReason;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(inventoryControlRejectionReasonsApi, null, null, id)
              .subscribe((inventoryControlRejectionReason: IInventoryRejectionReason) => {
                this.inventoryControlRejectionReason = inventoryControlRejectionReason;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.inventoryControlRejectionReasonForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.inventoryControlRejectionReasonForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.inventoryControlRejectionReason.image = '';
    this.inventoryControlRejectionReasonForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.inventoryControlRejectionReasonForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.inventoryControlRejectionReason) {
      if (this.inventoryControlRejectionReasonForm.valid) {
        const newInventoryControlRejectionReason = {
          _id: this.inventoryControlRejectionReason._id,
          ...this.generalService.checkEmptyFields(this.inventoryControlRejectionReasonForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(inventoryControlRejectionReasonsApi, newInventoryControlRejectionReason).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/purchases/inventoryControlRejectionReasons']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.inventoryControlRejectionReasonForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.inventoryControlRejectionReasonForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(inventoryControlRejectionReasonsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.inventoryControlRejectionReasonForm.reset();
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let inventoryControlRejectionReasonNameAr = '';
    let inventoryControlRejectionReasonNameEn = '';
    let isActive = true;

    if (this.inventoryControlRejectionReason) {
      image = this.inventoryControlRejectionReason.image;
      code = this.inventoryControlRejectionReason.code;
      inventoryControlRejectionReasonNameAr = this.inventoryControlRejectionReason.inventoryControlRejectionReasonNameAr;
      inventoryControlRejectionReasonNameEn = this.inventoryControlRejectionReason.inventoryControlRejectionReasonNameEn;
      isActive = this.inventoryControlRejectionReason.isActive;
    }
    this.inventoryControlRejectionReasonForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      inventoryControlRejectionReasonNameAr: new FormControl(inventoryControlRejectionReasonNameAr, Validators.required),
      inventoryControlRejectionReasonNameEn: new FormControl(inventoryControlRejectionReasonNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
