import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, inventoryControlRejectionReasonsApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { IInventoryRejectionReason } from '../../../interfaces/IInventoryRejectionReason.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-inventory-control-rejection-reasons',
  templateUrl: './inventory-control-rejection-reasons.component.html',
  styleUrls: ['./inventory-control-rejection-reasons.component.scss']
})
export class InventoryControlRejectionReasonsComponent implements OnInit, OnDestroy {
  inventoryRejectionReasons: IInventoryRejectionReason[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService,
  ) { }

  ngOnInit() {
    this.getInventoryRejectionReasonsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(inventoryControlRejectionReasonsApi, pageNumber).subscribe((res: IDataRes) => {
      this.inventoryRejectionReasons = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(inventoryControlRejectionReasonsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.inventoryRejectionReasons = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getInventoryRejectionReasonsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(inventoryControlRejectionReasonsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.inventoryRejectionReasons = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      inventoryControlRejectionReasonNameAr: new FormControl(''),
      inventoryControlRejectionReasonNameEn: new FormControl('')
    });
  }

  deleteModal(inventoryControlRejectionReason: IInventoryRejectionReason) {
    const initialState = {
      code: inventoryControlRejectionReason.code,
      nameAr: inventoryControlRejectionReason.inventoryControlRejectionReasonNameAr,
      nameEn: inventoryControlRejectionReason.inventoryControlRejectionReasonNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(inventoryControlRejectionReason._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }


  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(inventoryControlRejectionReasonsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.inventoryRejectionReasons = this.generalService.removeItem(this.inventoryRejectionReasons, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getInventoryRejectionReasonsFirstPage() {
    this.subscriptions.push(
      this.data.get(inventoryControlRejectionReasonsApi, 1).subscribe((res: IDataRes) => {
        this.inventoryRejectionReasons = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
