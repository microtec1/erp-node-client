import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, projectsApi, detailedChartOfAccountsApi, detailedCostCentersApi, customersApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IProject } from '../../../../interfaces/IProject.model';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { ICostCenter } from 'src/app/modules/accounting/modules/gl/interfaces/ICostCenter';
import { DataService } from 'src/app/common/services/shared/data.service';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent implements OnInit, OnDestroy {
  projectForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  project: IProject;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  glCostAccountsInputFocused: boolean;
  glRevenueAccountsInputFocused: boolean;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Cost Centers
  costCenters: ICostCenter[] = [];
  costCenterInputFocused: boolean;
  costCentersCount: number;
  noCostCenters: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(projectsApi, null, null, id)
              .subscribe((project: IProject) => {
                this.project = project;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.projectForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedCostCentersApi, null, null, companyId).subscribe((res: ICostCenter[]) => {
        if (res.length) {
          this.costCenters.push(...res);
          this.costCentersCount = res.length;
        } else {
          this.noCostCenters = true;
        }

        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.projectForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.project.image = '';
    this.projectForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.projectForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.project) {
      if (this.projectForm.valid) {
        const newproject = {
          _id: this.project._id,
          ...this.generalService.checkEmptyFields(this.projectForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(projectsApi, newproject).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/purchases/projects']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.projectForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.projectForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(projectsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.projectForm.reset();
            this.projectForm.patchValue({
              isActive: true
            });
            this.dropzone.reset();
            this.filesAdded = false;
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let projectNameAr = '';
    let projectNameEn = '';
    let glCostAccountId = '';
    let costCenterId = '';
    let glRevenueAccountId = '';
    let projectOwnerId = '';
    let isActive = true;

    if (this.project) {
      image = this.project.image;
      code = this.project.code;
      projectNameAr = this.project.projectNameAr;
      projectNameEn = this.project.projectNameEn;
      glCostAccountId = this.project.glCostAccountId;
      costCenterId = this.project.costCenterId;
      glRevenueAccountId = this.project.glRevenueAccountId;
      projectOwnerId = this.project.projectOwnerId;
      isActive = this.project.isActive;
    }
    this.projectForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      projectNameAr: new FormControl(projectNameAr, Validators.required),
      projectNameEn: new FormControl(projectNameEn),
      glCostAccountId: new FormControl(glCostAccountId),
      costCenterId: new FormControl(costCenterId),
      glRevenueAccountId: new FormControl(glRevenueAccountId),
      projectOwnerId: new FormControl(projectOwnerId),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
