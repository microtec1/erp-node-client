import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { ISupplierCLassification } from '../../../../interfaces/ISupplierCLassification.model';
import { SuppliersClassificationsService } from '../../../../services/suppliers-classifications.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { suppliersClassificationsApi, suppliersClassificationsTreeApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-suppliers-classification',
  templateUrl: './add-suppliers-classification.component.html',
  styleUrls: ['./add-suppliers-classification.component.scss']
})
export class AddSuppliersClassificationComponent implements OnInit {
  @Input() item: ISupplierCLassification;
  @Input() selectedItem: ISupplierCLassification;
  @Input() sidebarMode: boolean;
  @Output() finished: EventEmitter<any> = new EventEmitter();
  suppliersClassificationForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  suppliersClassification: ISupplierCLassification;
  constructor(
    private SCService: SuppliersClassificationsService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService,
  ) { }

  ngOnInit() {
    this.initForm();
    this.subscriptions.push(
      this.SCService.fillFormChange.subscribe((res: ISupplierCLassification) => {
        this.suppliersClassification = res;
        if (this.suppliersClassification) {
          this.suppliersClassificationForm.patchValue({
            code: this.suppliersClassification.code,
            suppliersClassificationNameAr: this.suppliersClassification.suppliersClassificationNameAr,
            suppliersClassificationNameEn: this.suppliersClassification.suppliersClassificationNameEn,
            classificationType: this.suppliersClassification.classificationType,
            isActive: this.suppliersClassification.isActive,
          });
        } else {
          this.suppliersClassificationForm.patchValue({
            code: '',
            suppliersClassificationNameAr: '',
            suppliersClassificationNameEn: '',
            classificationType: '',
            isActive: true,
          });
        }
      })
    );
  }

  get form() {
    return this.suppliersClassificationForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  cancel() {
    this.finished.emit(true);
  }

  private initForm() {
    let code = '';
    let suppliersClassificationNameAr = '';
    let suppliersClassificationNameEn = '';
    let classificationType = 'main';
    let isActive = true;

    if (this.item) {
      code = this.item.code;
      suppliersClassificationNameAr = this.item.suppliersClassificationNameAr;
      suppliersClassificationNameEn = this.item.suppliersClassificationNameEn;
      classificationType = this.item.classificationType;
      isActive = this.item.isActive;
    }

    this.suppliersClassificationForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      suppliersClassificationNameAr: new FormControl(suppliersClassificationNameAr, Validators.required),
      suppliersClassificationNameEn: new FormControl(suppliersClassificationNameEn),
      classificationType: new FormControl(classificationType),
      isActive: new FormControl(isActive, Validators.required),
    });
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.suppliersClassification || this.item) {
      if (this.suppliersClassificationForm.valid) {
        const editItem = this.suppliersClassification && this.suppliersClassification || this.item && this.item;
        const newSuppliersClassification = {
          _id: editItem._id,
          ...this.generalService.checkEmptyFields(this.suppliersClassificationForm.value),
          companyId
        };
        this.data.put(suppliersClassificationsApi, newSuppliersClassification).subscribe(res => {
          this.loadingButton = false;
          this.submitted = false;
          this.uiService.isLoading.next(false);
          this.SCService.fillFormChange.next(null);
          this.finished.emit();
          this.subscriptions.push(
            this.data.getByRoute(suppliersClassificationsTreeApi).subscribe((response: ISupplierCLassification[]) => {
              this.SCService.suppliersClassificationsChanged.next(response);
              this.uiService.isLoading.next(false);
            }, err => {
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            })
          );
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        },
          err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          });
        this.loadingButton = false;
      }
    } else {
      if (this.suppliersClassificationForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.suppliersClassificationForm.value),
          companyId,
          parentId: this.selectedItem ? this.selectedItem._id : null
        };
        this.subscriptions.push(
          this.data.post(suppliersClassificationsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.suppliersClassificationForm.reset();
            this.suppliersClassificationForm.patchValue({
              isActive: true,
              classificationType: 'main'
            });
            this.subscriptions.push(
              this.data.getByRoute(suppliersClassificationsTreeApi).subscribe((response: ISupplierCLassification[]) => {
                this.SCService.suppliersClassificationsChanged.next(response);
                this.uiService.isLoading.next(false);
              }, err => {
                this.uiService.isLoading.next(false);
                this.uiService.showErrorMessage(err);
              })
            );
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

}
