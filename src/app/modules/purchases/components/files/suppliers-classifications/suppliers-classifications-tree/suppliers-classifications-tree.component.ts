import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ISupplierCLassification } from '../../../../interfaces/ISupplierCLassification.model';
import { SuppliersClassificationsService } from '../../../../services/suppliers-classifications.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { suppliersClassificationsApi, suppliersClassificationsTreeApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-suppliers-classifications-tree',
  templateUrl: './suppliers-classifications-tree.component.html',
  styleUrls: ['./suppliers-classifications-tree.component.scss']
})
export class SuppliersClassificationsTreeComponent implements OnInit, OnDestroy {
  @Input() items: ISupplierCLassification[];
  selected: any;
  selectedSuppliersClassification: ISupplierCLassification;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  initAddForm: boolean;
  initEditForm: boolean;

  constructor(
    private modalService: BsModalService,
    private SCService: SuppliersClassificationsService,
    private data: DataService,
    private uiService: UiService
  ) { }

  ngOnInit() {
  }

  actionFinished() {
    this.initAddForm = false;
    this.initEditForm = false;
    this.selectedSuppliersClassification = null;
    this.selected = null;
  }

  showDetails(item) {
    this.selected = item._id;
    this.selectedSuppliersClassification = item;
    this.initAddForm = false;
    this.initEditForm = false;
  }

  deleteModal(suppliersClassification: ISupplierCLassification) {
    const initialState = {
      code: suppliersClassification.code,
      nameAr: suppliersClassification.suppliersClassificationNameAr,
      nameEn: suppliersClassification.suppliersClassificationNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(suppliersClassification._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }


  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(suppliersClassificationsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.subscriptions.push(
          this.data.getByRoute(suppliersClassificationsTreeApi).subscribe((response: ISupplierCLassification[]) => {
            this.actionFinished();
            this.SCService.suppliersClassificationsChanged.next(response);
            this.uiService.isLoading.next(false);
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  addNewItem() {
    this.initEditForm = false;
    this.initAddForm = true;
    this.SCService.fillFormChange.next(null);
  }

  editItem() {
    this.initAddForm = false;
    this.initEditForm = true;
    this.SCService.fillFormChange.next(this.selectedSuppliersClassification);
  }


  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
