import { Component, OnInit, OnDestroy } from '@angular/core';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { Subscription } from 'rxjs';
import { ISupplierCLassification } from '../../../interfaces/ISupplierCLassification.model';
import { SuppliersClassificationsService } from '../../../services/suppliers-classifications.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { suppliersClassificationsTreeApi, suppliersClassificationsApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-suppliers-classifications',
  templateUrl: './suppliers-classifications.component.html',
  styleUrls: ['./suppliers-classifications.component.scss']
})
export class SuppliersClassificationsComponent implements OnInit, OnDestroy {
  suppliersClassifications: ISupplierCLassification[];
  subscriptions: Subscription[] = [];
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;


  constructor(
    private SCService: SuppliersClassificationsService,
    private data: DataService,
    private generalService: GeneralService,
    private uiService: UiService
  ) { }


  ngOnInit() {
    this.subscriptions.push(
      this.SCService.suppliersClassificationsChanged.subscribe((res: ISupplierCLassification[]) => {
        this.suppliersClassifications = res;
      })
    );
    this.getSuppliersClassificationsTree();
    this.initSearchForm();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      suppliersClassificationNameAr: new FormControl(''),
      suppliersClassificationNameEn: new FormControl('')
    });
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getSuppliersClassificationsTree();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(suppliersClassificationsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.suppliersClassifications = res.results;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  getSuppliersClassificationsTree() {
    this.subscriptions.push(
      this.data.getByRoute(suppliersClassificationsTreeApi).subscribe((res: ISupplierCLassification[]) => {
        this.suppliersClassifications = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
