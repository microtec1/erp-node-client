import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, suppliersGroupsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { ISuppliersGroup } from '../../../../interfaces/ISuppliersGroup.model';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-suppliers-group',
  templateUrl: './add-suppliers-group.component.html',
  styleUrls: ['./add-suppliers-group.component.scss']
})

export class AddSuppliersGroupComponent implements OnInit, OnDestroy {
  suppliersGroupForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  suppliersGroup: ISuppliersGroup;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(suppliersGroupsApi, null, null, id)
              .subscribe((suppliersGroup: ISuppliersGroup) => {
                this.suppliersGroup = suppliersGroup;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.suppliersGroupForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.suppliersGroupForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.suppliersGroup.image = '';
    this.suppliersGroupForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.suppliersGroupForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.suppliersGroup) {
      if (this.suppliersGroupForm.valid) {
        const newsuppliersGroup = {
          _id: this.suppliersGroup._id,
          ...this.generalService.checkEmptyFields(this.suppliersGroupForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(suppliersGroupsApi, newsuppliersGroup).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/purchases/suppliersGroups']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.suppliersGroupForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.suppliersGroupForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(suppliersGroupsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.suppliersGroupForm.reset();
            this.suppliersGroupForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let suppliersGroupNameAr = '';
    let suppliersGroupNameEn = '';
    let isActive = true;

    if (this.suppliersGroup) {
      image = this.suppliersGroup.image;
      code = this.suppliersGroup.code;
      suppliersGroupNameAr = this.suppliersGroup.suppliersGroupNameAr;
      suppliersGroupNameEn = this.suppliersGroup.suppliersGroupNameEn;
      isActive = this.suppliersGroup.isActive;
    }
    this.suppliersGroupForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      suppliersGroupNameAr: new FormControl(suppliersGroupNameAr, Validators.required),
      suppliersGroupNameEn: new FormControl(suppliersGroupNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
