import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  suppliersApi,
  companiesApi,
  companyCurrenciesApi,
  attachmentTypesApi,
  banksApi,
  nationalitiesApi,
  citiesApi,
  regionsApi,
  suppliersGroupsApi,
  detailedSuppliersClassificationsApi,
  detailedChartOfAccountsApi
} from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import {
  companyId,
  searchLength
} from 'src/app/common/constants/general.constants';
import { ISupplier } from '../../../../interfaces/ISupplier';
import { IRegion } from 'src/app/modules/general/interfaces/IRegion.model';
import { ICity } from 'src/app/modules/general/interfaces/ICity.model';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ISupplierCLassification } from '../../../../interfaces/ISupplierCLassification.model';
import { INationality } from 'src/app/modules/general/interfaces/INationality';
import { ISuppliersGroup } from '../../../../interfaces/ISuppliersGroup.model';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IAttachmentType } from 'src/app/modules/general/interfaces/IAttachmentType';
import { IBranch } from 'src/app/modules/general/interfaces/IBranch';
import { ICompany } from 'src/app/modules/general/interfaces/ICompany';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.scss']
})
export class AddSupplierComponent implements OnInit, OnDestroy {
  supplierForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  supplier: ISupplier;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;

  @ViewChild('dropzone') dropzone: any;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  hasMoreCurrencies: boolean;
  currenciesCount: number;
  selectedCurrenciesPage = 1;
  currenciesPagesNo: number;
  noCurrencies: boolean;

  // Cities
  cities: ICity[] = [];
  citiesInputFocused: boolean;
  hasMoreCities: boolean;
  citiesCount: number;
  selectedCitiesPage = 1;
  citiesPagesNo: number;
  noCities: boolean;

  // Regions
  regions: IRegion[] = [];
  regionsInputFocused: boolean;
  hasMoreRegions: boolean;
  regionsCount: number;
  selectedRegionsPage = 1;
  regionsPagesNo: number;
  noRegions: boolean;

  // Suppliers Groups
  suppliersGroups: ISuppliersGroup[] = [];
  suppliersGroupsInputFocused: boolean;
  hasMoreSuppliersGroups: boolean;
  suppliersGroupsCount: number;
  selectedSuppliersGroupsPage = 1;
  suppliersGroupsPagesNo: number;
  noSuppliersGroups: boolean;

  // Nationalities
  nationalities: INationality[] = [];
  nationalitiesInputFocused: boolean;
  hasMoreNationalities: boolean;
  nationalitiesCount: number;
  selectedNationalitiesPage = 1;
  nationalitiesPagesNo: number;
  noNationalities: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Attachment Types
  attachmentTypes: IAttachmentType[] = [];
  attachmentTypesInputFocused: boolean;
  hasMoreAttachmentTypes: boolean;
  attachmentTypesCount: number;
  selectedAttachmentTypesPage = 1;
  attachmentTypesPagesNo: number;
  noAttachmentTypes: boolean;

  // Suppliers Classifications
  suppliersClassifications: ISupplierCLassification[] = [];
  suppliersClassificationsInputFocused: boolean;
  suppliersClassificationsCount: number;
  noSuppliersClassifications: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Branches
  branches: IBranch[] = [];
  branchesInputFocused: boolean;
  branchesCount: number;
  noBranches: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data
              .get(suppliersApi, null, null, id)
              .subscribe((supplier: ISupplier) => {
                this.supplier = supplier;
                this.formReady = true;
                this.initForm();
                this.syncControls();
                if (this.detailsMode) {
                  this.supplierForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.syncControls();
          this.subscriptions.push(
            this.data.get(companiesApi, null, null, companyId).subscribe((res: any) => {
              for (const item of res.companyCurrency) {
                if (item.isDefault) {
                  this.supplierForm.patchValue({
                    currencyId: item.currencyId
                  });
                }
              }
              this.uiService.isLoading.next(false);
            })
          );
        }
      })
    );


    this.subscriptions.push(
      this.data.get(companiesApi, null, null, companyId).subscribe((res: ICompany) => {
        if (res.branches.length) {
          this.branchesCount = res.branches.length;
          this.branches.push(...res.branches);
        } else {
          this.noBranches = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .post(companyCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.companyCurrency.length) {
            this.currencies.push(...res.companyCurrency);
            this.currenciesCount = res.companyCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(attachmentTypesApi, 1).subscribe((res: IDataRes) => {
        this.attachmentTypesPagesNo = res.pages;
        this.attachmentTypesCount = res.count;
        if (this.attachmentTypesPagesNo > this.selectedAttachmentTypesPage) {
          this.hasMoreAttachmentTypes = true;
        }
        this.attachmentTypes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(nationalitiesApi, 1)
        .subscribe((res: IDataRes) => {
          this.nationalitiesPagesNo = res.pages;
          this.nationalitiesCount = res.count;
          if (this.nationalitiesPagesNo > this.selectedNationalitiesPage) {
            this.hasMoreNationalities = true;
          }
          this.nationalities.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(citiesApi, 1).subscribe((res: IDataRes) => {
        this.citiesPagesNo = res.pages;
        this.citiesCount = res.count;
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        }
        this.cities.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(regionsApi, 1).subscribe((res: IDataRes) => {
        this.regionsPagesNo = res.pages;
        this.regionsCount = res.count;
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        }
        this.regions.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(suppliersGroupsApi, 1)
        .subscribe((res: IDataRes) => {
          this.suppliersGroupsPagesNo = res.pages;
          this.suppliersGroupsCount = res.count;
          if (this.suppliersGroupsPagesNo > this.selectedSuppliersGroupsPage) {
            this.hasMoreSuppliersGroups = true;
          }
          this.suppliersGroups.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.getByRoute(detailedSuppliersClassificationsApi).subscribe(
        (res: IDataRes) => {
          if (res.results.length) {
            this.suppliersClassifications.push(...res.results);
            this.suppliersClassificationsCount = res.count;
          } else {
            this.noSuppliersClassifications = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe(
        (res: IChartOfAccount[]) => {
          if (res.length) {
            this.chartOfAccounts.push(...res);
            this.chartOfAccountsCount = res.length;
          } else {
            this.noChartOfAccounts = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );


  }

  syncControls() {
    this.subscriptions.push(
      this.supplierForm.controls.openingBalances.valueChanges.subscribe(res => {
        const debitValues = [];
        const creditValues = [];
        for (const item of this.getOpeningBalancesArray.controls) {
          if (item.value.accountSide === 'credit') {
            creditValues.push(parseFloat(item.value.balance) * item.value.exchangeRate);
          }
          if (item.value.accountSide === 'debit') {
            debitValues.push(parseFloat(item.value.balance) * item.value.exchangeRate);
          }
        }
        const creditValuesSum = creditValues.reduce((acc, cur) => acc + cur, 0);
        const debitValuesSum = debitValues.reduce((acc, cur) => acc + cur, 0);

        this.supplierForm.controls.accountingInfo.patchValue({
          netOpeningBalance: Math.abs((creditValuesSum - debitValuesSum) / this.supplierForm.value.exchangeRate)
        });
      })
    );
  }

  fillRate(event, formGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        exchangeRate: currency.exchangeRate
      });
    } else {
      this.supplierForm.patchValue({
        exchangeRate: currency.exchangeRate
      });
    }
  }

  searchRegions(event) {
    const searchValue = event;
    const searchQuery = {
      regionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(regionsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noRegions = true;
            } else {
              this.noRegions = false;
              for (const item of res.results) {
                if (this.regions.length) {
                  const uniqueRegions = this.regions.filter(
                    x => x._id !== item._id
                  );
                  this.regions = uniqueRegions;
                }
                this.regions.push(item);
              }
            }
            this.regions = res.results;
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreRegions() {
    this.selectedRegionsPage = this.selectedRegionsPage + 1;
    this.subscriptions.push(
      this.data
        .get(regionsApi, this.selectedRegionsPage)
        .subscribe((res: IDataRes) => {
          if (this.regionsPagesNo > this.selectedRegionsPage) {
            this.hasMoreRegions = true;
          } else {
            this.hasMoreRegions = false;
          }
          for (const item of res.results) {
            if (this.regions.length) {
              const uniqueRegions = this.regions.filter(
                x => x._id !== item._id
              );
              this.regions = uniqueRegions;
            }
            this.regions.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCities(event) {
    const searchValue = event;
    const searchQuery = {
      cityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(citiesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noCities = true;
            } else {
              this.noCities = false;
              for (const item of res.results) {
                if (this.cities.length) {
                  const uniqueCities = this.cities.filter(
                    x => x._id !== item._id
                  );
                  this.cities = uniqueCities;
                }
                this.cities.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreCities() {
    this.selectedCitiesPage = this.selectedCitiesPage + 1;
    this.subscriptions.push(
      this.data
        .get(citiesApi, this.selectedCitiesPage)
        .subscribe((res: IDataRes) => {
          if (this.citiesPagesNo > this.selectedCitiesPage) {
            this.hasMoreCities = true;
          } else {
            this.hasMoreCities = false;
          }
          for (const item of res.results) {
            if (this.cities.length) {
              const uniqueCities = this.cities.filter(x => x._id !== item._id);
              this.cities = uniqueCities;
            }
            this.cities.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchNationalities(event) {
    const searchValue = event;
    const searchQuery = {
      nationalityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(nationalitiesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noNationalities = true;
            } else {
              this.noNationalities = false;
              for (const item of res.results) {
                if (this.nationalities.length) {
                  const uniqueNationalities = this.nationalities.filter(
                    x => x._id !== item._id
                  );
                  this.nationalities = uniqueNationalities;
                }
                this.nationalities.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreNationalities() {
    this.selectedNationalitiesPage = this.selectedNationalitiesPage + 1;
    this.subscriptions.push(
      this.data
        .get(nationalitiesApi, this.selectedNationalitiesPage)
        .subscribe((res: IDataRes) => {
          if (this.nationalitiesPagesNo > this.selectedNationalitiesPage) {
            this.hasMoreNationalities = true;
          } else {
            this.hasMoreNationalities = false;
          }
          for (const item of res.results) {
            if (this.nationalities.length) {
              const uniqueNationalities = this.nationalities.filter(
                x => x._id !== item._id
              );
              this.nationalities = uniqueNationalities;
            }
            this.nationalities.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSuppliersGroups(event) {
    const searchValue = event;
    const searchQuery = {
      suppliersGroupNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(suppliersGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSuppliersGroups = true;
            } else {
              this.noSuppliersGroups = false;
              for (const item of res.results) {
                if (this.suppliersGroups.length) {
                  const uniqueSuppliersGroups = this.suppliersGroups.filter(
                    x => x._id !== item._id
                  );
                  this.suppliersGroups = uniqueSuppliersGroups;
                }
                this.suppliersGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSuppliersGroups() {
    this.selectedSuppliersGroupsPage = this.selectedSuppliersGroupsPage + 1;
    this.subscriptions.push(
      this.data
        .get(suppliersGroupsApi, this.selectedSuppliersGroupsPage)
        .subscribe((res: IDataRes) => {
          if (this.suppliersGroupsPagesNo > this.selectedSuppliersGroupsPage) {
            this.hasMoreSuppliersGroups = true;
          } else {
            this.hasMoreSuppliersGroups = false;
          }
          for (const item of res.results) {
            if (this.suppliersGroups.length) {
              const uniqueSuppliersGroups = this.suppliersGroups.filter(
                x => x._id !== item._id
              );
              this.suppliersGroups = uniqueSuppliersGroups;
            }
            this.suppliersGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  getLastWord(link) {
    if (link) {
      const str = link;
      const n = str.lastIndexOf('/');
      const result = str.substring(n + 1);
      return result;
    }
  }

  removeFile(index) {
    this.supplier.files[index].file = null;
    this.getFilesArray.controls[index].patchValue({
      file: ''
    });
  }

  inputFileChanged(files: File[], formGroup) {
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        formGroup.patchValue({
          file: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetFile(dropzoneFile, formGroup) {
    formGroup.patchValue({
      file: ''
    });
    dropzoneFile.reset();
  }

  searchAttachmentTypes(event) {
    const searchValue = event;
    const searchQuery = {
      attachmentTypeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(attachmentTypesApi, null, searchQuery).subscribe(
          (res: IDataRes) => {
            if (!res.results.length) {
              this.noAttachmentTypes = true;
            } else {
              this.noAttachmentTypes = false;
              for (const item of res.results) {
                if (this.attachmentTypes.length) {
                  const uniqueAttachmentTypes = this.attachmentTypes.filter(
                    x => x._id !== item._id
                  );
                  this.attachmentTypes = uniqueAttachmentTypes;
                }
                this.attachmentTypes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          }
        )
      );
    }
  }

  loadMoreAttachmentTypes() {
    this.selectedAttachmentTypesPage = this.selectedAttachmentTypesPage + 1;
    this.subscriptions.push(
      this.data.get(
        attachmentTypesApi, this.selectedAttachmentTypesPage
      ).subscribe((res: IDataRes) => {
        if (this.attachmentTypesPagesNo > this.selectedAttachmentTypesPage) {
          this.hasMoreAttachmentTypes = true;
        } else {
          this.hasMoreAttachmentTypes = false;
        }
        for (const item of res.results) {
          if (this.attachmentTypes.length) {
            const uniqueAttachmentTypes = this.attachmentTypes.filter(
              x => x._id !== item._id
            );
            this.attachmentTypes = uniqueAttachmentTypes;
          }
          this.attachmentTypes.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.supplierForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.supplier.image = '';
    this.supplierForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.supplierForm.controls;
  }

  get getOpeningBalancesArray() {
    return this.supplierForm.get('openingBalances') as FormArray;
  }

  get getBanksArray() {
    return this.supplierForm.controls.accountingInfo.get('banks') as FormArray;
  }

  get getBranchesArray() {
    return this.supplierForm.get('branches') as FormArray;
  }

  get getFilesArray() {
    return this.supplierForm.get('files') as FormArray;
  }

  get getSupplierTransactionsArray() {
    return this.supplierForm.get('supplierTransactions') as FormArray;
  }

  addSupplierTransaction() {
    const control = this.supplierForm.get('supplierTransactions') as FormArray;
    control.push(
      new FormGroup({
        branchId: new FormControl('')
      })
    );
  }

  deleteSupplierTransaction(index) {
    const control = this.supplierForm.get('supplierTransactions') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addFile() {
    const control = this.supplierForm.get('files') as FormArray;
    control.push(
      new FormGroup({
        attachmentTypeId: new FormControl(''),
        file: new FormControl('')
      })
    );
  }

  deleteFile(index) {
    const control = this.supplierForm.get('files') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addOfficial(control) {
    control.push(
      new FormGroup({
        name: new FormControl(''),
        job: new FormControl(''),
        phone: new FormControl(null),
        internalNumber: new FormControl(null),
        mobile: new FormControl(null),
        email: new FormControl('')
      })
    );
  }

  deleteOfficial(control, index) {
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBranch() {
    const control = this.supplierForm.get('branches') as FormArray;
    control.push(
      new FormGroup({
        code: new FormControl(''),
        branchName: new FormControl(''),
        cityId: new FormControl(''),
        regionId: new FormControl(''),
        address: new FormControl(''),
        phone: new FormControl(null),
        fax: new FormControl(null),
        mobile: new FormControl(null),
        email: new FormControl(''),
        officials: new FormArray([
          new FormGroup({
            name: new FormControl(''),
            job: new FormControl(''),
            phone: new FormControl(null),
            internalNumber: new FormControl(null),
            mobile: new FormControl(null),
            email: new FormControl('')
          })
        ])
      })
    );
  }

  deleteBranch(index) {
    const control = this.supplierForm.get('branches') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addOpeningBalance() {
    const control = this.supplierForm.get('openingBalances') as FormArray;
    control.push(
      new FormGroup({
        balance: new FormControl(null),
        netBalance: new FormControl(null),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        accountSide: new FormControl(''),
        dueGregorianDate: new FormControl(''),
        dueHijriDate: new FormControl(''),
        referenceNumber: new FormControl(''),
        notes: new FormControl('')
      })
    );
  }

  deleteOpeningBalance(index) {
    const control = this.supplierForm.get('openingBalances') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBank() {
    const control = this.supplierForm.controls.accountingInfo.get(
      'banks'
    ) as FormArray;
    control.push(
      new FormGroup({
        bankId: new FormControl('', Validators.required),
        bankAccountNumber: new FormControl(null)
      })
    );
  }

  deleteBank(index) {
    const control = this.supplierForm.controls.accountingInfo.get(
      'banks'
    ) as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  setReleaseHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.supplierForm.patchValue({
        identity: {
          releaseHijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        }
      });
    }
  }

  setReleaseGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.supplierForm.patchValue({
        identity: {
          releaseGregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        }
      });
    }
  }

  setStartBusinessHijri(value: Date) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.supplierForm.patchValue({
        officialDocuments: {
          commercialRegistration: {
            startBusinessHijriDate: {
              year: hijriDate.iYear(),
              month: hijriDate.iMonth() + 1,
              day: hijriDate.iDate()
            }
          }
        }
      });
    }
  }

  setStartBusinessGeorian(value) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.supplierForm.patchValue({
        officialDocuments: {
          commercialRegistration: {
            startBusinessGregorianDate: this.generalService.format(
              new Date(
                gegorianDate.year(),
                gegorianDate.month(),
                gegorianDate.date()
              )
            )
          }
        }
      });
    }
  }

  setDueHijri(value: Date, index) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      this.getOpeningBalancesArray.controls[index].patchValue({
        dueHijriDate: {
          year: hijriDate.iYear(),
          month: hijriDate.iMonth() + 1,
          day: hijriDate.iDate()
        }
      });
    }
  }

  setDueGeorian(value, index) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      this.getOpeningBalancesArray.controls[index].patchValue({
        dueGregorianDate: this.generalService.format(
          new Date(
            gegorianDate.year(),
            gegorianDate.month(),
            gegorianDate.date()
          )
        )
      });
    }
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  validateCurrency(value, formGroup: FormGroup) {
    formGroup.patchValue({
      netBalance: value
    });

    if (value) {
      formGroup.controls.currencyId.setValidators(Validators.required);
      formGroup.controls.currencyId.updateValueAndValidity();
    } else {
      formGroup.controls.currencyId.clearValidators();
      formGroup.controls.currencyId.updateValueAndValidity();
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.supplierForm.value.identity) {
      if (this.supplierForm.value.identity.releaseGregorianDate) {
        if (
          typeof this.supplierForm.value.identity.releaseGregorianDate !==
          'string'
        ) {
          this.supplierForm.value.identity.releaseGregorianDate = this.generalService.format(
            this.supplierForm.value.identity.releaseGregorianDate
          );
        }
        this.supplierForm.value.identity = {
          ...this.supplierForm.value.identity,
          releaseHijriDate: this.generalService.formatHijriDate(
            this.supplierForm.value.identity.releaseHijriDate
          )
        };
      }
    }

    if (this.supplierForm.value.officialDocuments.commercialRegistration) {
      if (
        this.supplierForm.value.officialDocuments.commercialRegistration
          .startBusinessGregorianDate
      ) {
        if (
          typeof this.supplierForm.value.officialDocuments.commercialRegistration
            .startBusinessGregorianDate !== 'string'
        ) {
          this.supplierForm.value.officialDocuments.commercialRegistration.startBusinessGregorianDate = this.generalService.format(
            this.supplierForm.value.officialDocuments.commercialRegistration
              .startBusinessGregorianDate
          );
        }
        if (
          typeof this.supplierForm.value.officialDocuments.commercialRegistration
            .startBusinessHijriDate !== 'string'
        ) {
          this.supplierForm.value.officialDocuments.commercialRegistration.startBusinessHijriDate = this.generalService.formatHijriDate(
            this.supplierForm.value.officialDocuments.commercialRegistration
              .startBusinessHijriDate
          );
        }
      }
    }

    for (
      let i = 0;
      i <= this.supplierForm.value.openingBalances.length - 1;
      i++
    ) {
      if (this.supplierForm.value.openingBalances[i].dueGregorianDate) {
        if (
          typeof this.supplierForm.value.openingBalances[i].dueGregorianDate !==
          'string'
        ) {
          this.supplierForm.value.openingBalances[
            i
          ].dueGregorianDate = this.generalService.format(
            this.supplierForm.value.openingBalances[i].dueGregorianDate
          );
        }
        if (
          typeof this.supplierForm.value.openingBalances[i].dueHijriDate !==
          'string'
        ) {
          this.supplierForm.value.openingBalances[
            i
          ].dueHijriDate = this.generalService.formatHijriDate(
            this.supplierForm.value.openingBalances[i].dueHijriDate
          );
        }
      }
    }

    if (this.supplier) {
      if (this.supplierForm.valid) {
        const newSupplier = {
          _id: this.supplier._id,
          ...this.generalService.checkEmptyFields(this.supplierForm.value),
          companyId: this.companyId
        };
        const emptyObject = this.generalService.isEmpty(this.supplierForm.value.files[0]);
        if (emptyObject) {
          newSupplier.files = [];
        }
        const emptyObject2 = this.generalService.isEmpty(this.supplierForm.value.supplierTransactions[0]);
        if (emptyObject2) {
          newSupplier.supplierTransactions = [];
        }
        this.subscriptions.push(
          this.data.put(suppliersApi, newSupplier).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/purchases/suppliers']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.supplierForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.supplierForm.value),
          companyId
        };

        const emptyObject = this.generalService.isEmpty(this.supplierForm.value.files[0]);
        if (emptyObject) {
          formValue.files = [];
        }
        const emptyObject2 = this.generalService.isEmpty(this.supplierForm.value.supplierTransactions[0]);
        if (emptyObject2) {
          formValue.supplierTransactions = [];
        }
        this.subscriptions.push(
          this.data.post(suppliersApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.supplierForm.reset();
              this.filesAdded = false;
              this.dropzone.reset();
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        const invalid = [];
        const controls = this.supplierForm.controls;
        for (const name in controls) {
          if (controls[name].invalid) {
            invalid.push(name);
          }
        }

        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let supplierNameAr = '';
    let supplierNameEn = '';
    let isActive = true;
    let suppliersGroupId = '';
    let currencyId = '';
    let exchangeRate = null;
    let suppliersClassificationId = '';
    let nationalityId = '';
    let identityNumber = null;
    let identitySource = '';
    let releaseGregorianDate = '';
    let releaseHijriDate = null;
    let taxRegistrationNumber = null;
    let taxCardNumber = null;
    let missionName = '';
    let businessAddress = '';
    let fileNumber = '';
    let businessName = '';
    let financierName = '';
    let commercialName = '';
    let commercialTrait = '';
    let commercialBrand = '';
    let traderName = '';
    let commercialRegistrationNationalityId = '';
    let financialSystem = '';
    let commercialEligibility = '';
    let tradeType = '';
    let startBusinessGregorianDate = '';
    let startBusinessHijriDate = null;
    let openingBalancesArray = new FormArray([
      new FormGroup({
        balance: new FormControl(null),
        netBalance: new FormControl(null),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(null),
        accountSide: new FormControl(''),
        dueGregorianDate: new FormControl(''),
        dueHijriDate: new FormControl(null),
        referenceNumber: new FormControl(''),
        notes: new FormControl('')
      })
    ]);
    let purchasesDuePeriod = null;
    let glAccountId = '';
    let netOpeningBalance = null;
    let cannotPassCreditLimit = false;
    let creditLimit = null;
    let salesPersonCommission = '';
    let banksArray = new FormArray([
      new FormGroup({
        bankId: new FormControl(''),
        bankAccountNumber: new FormControl(null)
      })
    ]);
    let cityId = '';
    let regionId = '';
    let address = '';
    let email = '';
    let phone = '';
    let internalNumber = null;
    let fax = null;
    let mobile = null;
    let postalCode = null;
    let postalBox = '';
    let website = '';
    let officialsArray = new FormArray([
      new FormGroup({
        name: new FormControl(''),
        job: new FormControl(''),
        phone: new FormControl(null),
        internalNumber: new FormControl(null),
        mobile: new FormControl(null),
        email: new FormControl('')
      })
    ]);
    let branchesArray = new FormArray([
      new FormGroup({
        code: new FormControl(''),
        branchName: new FormControl(''),
        cityId: new FormControl(''),
        regionId: new FormControl(''),
        address: new FormControl(''),
        phone: new FormControl(null),
        fax: new FormControl(null),
        mobile: new FormControl(null),
        email: new FormControl(''),
        officials: officialsArray
      })
    ]);

    let filesArray = new FormArray([
      new FormGroup({
        attachmentTypeId: new FormControl(''),
        file: new FormControl('')
      })
    ]);

    let supplierTransactionsArray = new FormArray([
      new FormGroup({
        branchId: new FormControl('')
      })
    ]);

    if (this.supplier) {
      image = this.supplier.image;
      code = this.supplier.code;
      supplierNameAr = this.supplier.supplierNameAr;
      supplierNameEn = this.supplier.supplierNameEn;
      isActive = this.supplier.isActive;
      suppliersGroupId = this.supplier.suppliersGroupId;
      currencyId = this.supplier.currencyId;
      exchangeRate = this.supplier.exchangeRate;
      suppliersClassificationId = this.supplier.suppliersClassificationId;
      if (this.supplier.identity) {
        nationalityId = this.supplier.identity.nationalityId;
        identityNumber = this.supplier.identity.identityNumber;
        identitySource = this.supplier.identity.identitySource;
        releaseGregorianDate = this.supplier.identity.releaseGregorianDate;
        releaseHijriDate = this.supplier.identity.releaseHijriDate;
      }
      if (this.supplier.officialDocuments) {
        if (this.supplier.officialDocuments.tax) {
          taxRegistrationNumber = this.supplier.officialDocuments.tax.taxRegistrationNumber;
          taxCardNumber = this.supplier.officialDocuments.tax.taxCardNumber;
          missionName = this.supplier.officialDocuments.tax.missionName;
          businessAddress = this.supplier.officialDocuments.tax.businessAddress;
          fileNumber = this.supplier.officialDocuments.tax.fileNumber;
          businessName = this.supplier.officialDocuments.tax.businessName;
          financierName = this.supplier.officialDocuments.tax.financierName;
        }
        if (this.supplier.officialDocuments.commercialRegistration) {
          commercialName = this.supplier.officialDocuments.commercialRegistration.commercialName;
          commercialTrait = this.supplier.officialDocuments.commercialRegistration.commercialTrait;
          commercialBrand = this.supplier.officialDocuments.commercialRegistration.commercialBrand;
          traderName = this.supplier.officialDocuments.commercialRegistration.traderName;
          commercialRegistrationNationalityId = this.supplier.officialDocuments.commercialRegistration.nationalityId;
          financialSystem = this.supplier.officialDocuments.commercialRegistration.financialSystem;
          commercialEligibility = this.supplier.officialDocuments.commercialRegistration.commercialEligibility;
          tradeType = this.supplier.officialDocuments.commercialRegistration.tradeType;
          startBusinessGregorianDate = this.supplier.officialDocuments.commercialRegistration.startBusinessGregorianDate;
          startBusinessHijriDate = this.supplier.officialDocuments.commercialRegistration.startBusinessHijriDate;
        }
      }
      if (this.supplier.openingBalances.length) {
        openingBalancesArray = new FormArray([]);
        for (const balanceItem of this.supplier.openingBalances) {
          openingBalancesArray.push(
            new FormGroup({
              balance: new FormControl(balanceItem.balance),
              netBalance: new FormControl(balanceItem.netBalance),
              currencyId: new FormControl(
                balanceItem.currencyId
              ),
              exchangeRate: new FormControl(balanceItem.exchangeRate),
              accountSide: new FormControl(balanceItem.accountSide),
              dueGregorianDate: new FormControl(balanceItem.dueGregorianDate && new Date(balanceItem.dueGregorianDate + 'UTC')),
              dueHijriDate: new FormControl(balanceItem.dueHijriDate),
              referenceNumber: new FormControl(balanceItem.referenceNumber),
              notes: new FormControl(balanceItem.notes)
            })
          );
        }
      }

      purchasesDuePeriod = this.supplier.accountingInfo.purchasesDuePeriod;
      glAccountId = this.supplier.accountingInfo.glAccountId;
      netOpeningBalance = this.supplier.accountingInfo.netOpeningBalance;
      cannotPassCreditLimit = this.supplier.accountingInfo.cannotPassCreditLimit;
      creditLimit = this.supplier.accountingInfo.creditLimit;
      salesPersonCommission = this.supplier.accountingInfo.salesPersonCommission;
      if (this.supplier.accountingInfo.banks.length) {
        banksArray = new FormArray([]);
        for (const bankItem of this.supplier.accountingInfo.banks) {
          banksArray.push(
            new FormGroup({
              bankId: new FormControl(bankItem.bankId),
              bankAccountNumber: new FormControl(bankItem.bankAccountNumber)
            })
          );
        }
      }
      if (this.supplier.contact) {
        if (this.supplier.contact.address) {
          cityId = this.supplier.contact.address.cityId;
          regionId = this.supplier.contact.address.regionId;
          address = this.supplier.contact.address.address;
        }
        if (this.supplier.contact.contact) {
          phone = this.supplier.contact.contact.phone;
          internalNumber = this.supplier.contact.contact.internalNumber;
          fax = this.supplier.contact.contact.fax;
          mobile = this.supplier.contact.contact.mobile;
        }
        if (this.supplier.contact.mail) {
          postalCode = this.supplier.contact.mail.postalCode;
          postalBox = this.supplier.contact.mail.postalBox;
        }
        if (this.supplier.contact.socialAccounts) {
          website = this.supplier.contact.socialAccounts.website;
        }
      }

      if (this.supplier.branches.length) {
        branchesArray = new FormArray([]);
        for (const branchItem of this.supplier.branches) {
          officialsArray = new FormArray([]);
          for (const officialItem of branchItem.officials) {
            officialsArray.push(
              new FormGroup({
                name: new FormControl(officialItem.name),
                job: new FormControl(officialItem.job),
                phone: new FormControl(officialItem.phone),
                internalNumber: new FormControl(officialItem.internalNumber),
                mobile: new FormControl(officialItem.mobile),
                email: new FormControl(officialItem.email)
              })
            );
          }
          branchesArray.push(
            new FormGroup({
              code: new FormControl(branchItem.code),
              branchName: new FormControl(branchItem.branchName),
              cityId: new FormControl(branchItem.cityId),
              regionId: new FormControl(branchItem.regionId),
              address: new FormControl(branchItem.address),
              phone: new FormControl(branchItem.phone),
              fax: new FormControl(branchItem.fax),
              mobile: new FormControl(branchItem.mobile),
              email: new FormControl(branchItem.email),
              officials: officialsArray
            })
          );
        }
      }

      if (this.supplier.files.length) {
        filesArray = new FormArray([]);
        for (const fileItem of this.supplier.files) {
          filesArray = new FormArray([
            new FormGroup({
              attachmentTypeId: new FormControl(fileItem.attachmentTypeId),
              file: new FormControl(fileItem.file)
            })
          ]);
        }
      }

      if (this.supplier.supplierTransactions.length) {
        supplierTransactionsArray = new FormArray([]);
        for (const supplierTransactionItem of this.supplier
          .supplierTransactions) {
          supplierTransactionsArray.push(
            new FormGroup({
              branchId: new FormControl(supplierTransactionItem.branchId)
            })
          );
        }
      }
    }
    this.supplierForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      supplierNameAr: new FormControl(supplierNameAr, Validators.required),
      supplierNameEn: new FormControl(supplierNameEn),
      suppliersGroupId: new FormControl(suppliersGroupId),
      currencyId: new FormControl(currencyId, Validators.required),
      exchangeRate: new FormControl(exchangeRate),
      suppliersClassificationId: new FormControl(suppliersClassificationId),
      identity: new FormGroup({
        nationalityId: new FormControl(nationalityId),
        identityNumber: new FormControl(identityNumber),
        identitySource: new FormControl(identitySource),
        releaseGregorianDate: new FormControl(releaseGregorianDate),
        releaseHijriDate: new FormControl(releaseHijriDate)
      }),
      officialDocuments: new FormGroup({
        tax: new FormGroup({
          taxRegistrationNumber: new FormControl(taxRegistrationNumber),
          taxCardNumber: new FormControl(taxCardNumber),
          missionName: new FormControl(missionName),
          businessAddress: new FormControl(businessAddress),
          fileNumber: new FormControl(fileNumber),
          businessName: new FormControl(businessName),
          financierName: new FormControl(financierName)
        }),
        commercialRegistration: new FormGroup({
          commercialName: new FormControl(commercialName),
          commercialTrait: new FormControl(commercialTrait),
          commercialBrand: new FormControl(commercialBrand),
          traderName: new FormControl(traderName),
          nationalityId: new FormControl(commercialRegistrationNationalityId),
          financialSystem: new FormControl(financialSystem),
          commercialEligibility: new FormControl(commercialEligibility),
          tradeType: new FormControl(tradeType),
          startBusinessGregorianDate: new FormControl(
            startBusinessGregorianDate
          ),
          startBusinessHijriDate: new FormControl(startBusinessHijriDate)
        })
      }),
      openingBalances: openingBalancesArray,
      accountingInfo: new FormGroup({
        purchasesDuePeriod: new FormControl(purchasesDuePeriod),
        glAccountId: new FormControl(glAccountId),
        netOpeningBalance: new FormControl(netOpeningBalance),
        cannotPassCreditLimit: new FormControl(cannotPassCreditLimit),
        creditLimit: new FormControl(creditLimit),
        salesPersonCommission: new FormControl(salesPersonCommission),
        banks: banksArray
      }),
      contact: new FormGroup({
        address: new FormGroup({
          cityId: new FormControl(cityId),
          regionId: new FormControl(regionId),
          address: new FormControl(address)
        }),
        contact: new FormGroup({
          phone: new FormControl(phone),
          internalNumber: new FormControl(internalNumber),
          fax: new FormControl(fax),
          mobile: new FormControl(mobile)
        }),
        mail: new FormGroup({
          postalCode: new FormControl(postalCode),
          postalBox: new FormControl(postalBox)
        }),
        socialAccounts: new FormGroup({
          website: new FormControl(website)
        })
      }),
      branches: branchesArray,
      supplierTransactions: supplierTransactionsArray,
      files: filesArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
