import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { companyId, branchId, searchLength } from 'src/app/common/constants/general.constants';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IPurchasesSettings } from '../../interfaces/IPurchasesSettings';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { DataService } from 'src/app/common/services/shared/data.service';
import { getPurchasesSettingsApi, detailedChartOfAccountsApi, safeBoxesApi, banksApi, purchasesSettingsApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit, OnDestroy {
  settingsForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  settings: IPurchasesSettings;
  formReady: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;


  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;
  constructor(
    private uiService: UiService,
    private data: DataService,
    private generalService: GeneralService,
  ) { }

ngOnInit() {
    this.subscriptions.push(
      this.data.post(getPurchasesSettingsApi, {}).subscribe((res: IPurchasesSettings) => {
        this.settings = res;
        this.initForm();
        this.formReady = true;
        this.uiService.isLoading.next(false);
      })
    );
    this.formReady = true;
    this.initForm();
    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoresafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  get form() {
    return this.settingsForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  activateChoice(checked, ...fieldNames) {
    let activate = false;
    activate = checked;
    const group = this.settingsForm.controls.branch.get('purchasingCycleTransactions') as FormGroup;
    if (fieldNames.length > 1) {
      for (const field of fieldNames) {
        group.patchValue({
          [field]: activate
        });
      }
    } else {
      group.patchValue({
        [fieldNames[0]]: activate
      });
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.settingsForm.valid) {
      const newSettings = {
        companyId,
        ...this.generalService.checkEmptyFields(this.settingsForm.value)
      };
      if (newSettings.branch.directInfluenceOnTheInventory) {
        delete newSettings.branch.directInfluenceOnTheInventoryOptions;
      }
      if (!newSettings.branch.loadLastPurchasingPrice) {
        delete newSettings.branch.loadLastPurchasingPriceOptions;
      }
      this.data.post(purchasesSettingsApi, newSettings).subscribe(res => {
        this.uiService.isLoading.next(false);
        this.submitted = false;
        this.loadingButton = false;
        this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        this.subscriptions.push(
          this.data.post(getPurchasesSettingsApi, {}).subscribe((data: IPurchasesSettings) => {
            const settings = JSON.stringify(data);
            localStorage.setItem('purchasesSettings', settings);
            this.uiService.isLoading.next(false);
          })
        );
      },
        err => {
          this.loadingButton = false;
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        });

    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
    }
  }

  private initForm() {
    let directInfluenceOnTheInventory = false;
    let directInfluenceOnTheInventoryOptions = '';
    let displayFreeQuantitiesInPurchasesAndInventoryReceiving = false;
    let defaultPaymentsType = 'cash';
    let defaultPaymentsAccount = '';
    let loadLastPurchasingPrice = false;
    let cannotExceedMaximumLimitPurchasesOnThePurchaseInvoice = false;
    let useAddedCostInPurchases = false;
    let loadLastPurchasingPriceOptions = 'accordingItem';
    let purchaseRequest = false;
    let inventoryControl = false;
    let purchaseOfferRequest = false;
    let purchaseOffer = false;
    let comparePrices = false;
    let purchaseOrder = false;
    let purchaseInvoice = false;
    let purchaseReturns = false;

    if (this.settings) {
      directInfluenceOnTheInventory = this.settings.branch.directInfluenceOnTheInventory;
      directInfluenceOnTheInventoryOptions = this.settings.branch.directInfluenceOnTheInventoryOptions;
      displayFreeQuantitiesInPurchasesAndInventoryReceiving = this.settings.branch.displayFreeQuantitiesInPurchasesAndInventoryReceiving;
      defaultPaymentsType = this.settings.branch.defaultPaymentsType;
      defaultPaymentsAccount = this.settings.branch.defaultPaymentsAccount;
      loadLastPurchasingPrice = this.settings.branch.loadLastPurchasingPrice;
      cannotExceedMaximumLimitPurchasesOnThePurchaseInvoice = this.settings.branch.cannotExceedMaximumLimitPurchasesOnThePurchaseInvoice;
      useAddedCostInPurchases = this.settings.branch.useAddedCostInPurchases;
      loadLastPurchasingPriceOptions = this.settings.branch.loadLastPurchasingPriceOptions;
      purchaseRequest = this.settings.branch.purchasingCycleTransactions.purchaseRequest;
      inventoryControl = this.settings.branch.purchasingCycleTransactions.inventoryControl;
      purchaseOfferRequest = this.settings.branch.purchasingCycleTransactions.purchaseOfferRequest;
      purchaseOffer = this.settings.branch.purchasingCycleTransactions.purchaseOffer;
      comparePrices = this.settings.branch.purchasingCycleTransactions.comparePrices;
      purchaseOrder = this.settings.branch.purchasingCycleTransactions.purchaseOrder;
      purchaseInvoice = this.settings.branch.purchasingCycleTransactions.purchaseInvoice;
      purchaseReturns = this.settings.branch.purchasingCycleTransactions.purchaseReturns;
    }

    this.settingsForm = new FormGroup({
      branch: new FormGroup({
        branchId: new FormControl(branchId),
        directInfluenceOnTheInventory: new FormControl(directInfluenceOnTheInventory),
        directInfluenceOnTheInventoryOptions: new FormControl(directInfluenceOnTheInventoryOptions),
        displayFreeQuantitiesInPurchasesAndInventoryReceiving: new FormControl(displayFreeQuantitiesInPurchasesAndInventoryReceiving),
        defaultPaymentsType: new FormControl(defaultPaymentsType),
        defaultPaymentsAccount: new FormControl(defaultPaymentsAccount),
        loadLastPurchasingPrice: new FormControl(loadLastPurchasingPrice),
        cannotExceedMaximumLimitPurchasesOnThePurchaseInvoice: new FormControl(cannotExceedMaximumLimitPurchasesOnThePurchaseInvoice),
        useAddedCostInPurchases: new FormControl(useAddedCostInPurchases),
        loadLastPurchasingPriceOptions: new FormControl(loadLastPurchasingPriceOptions),
        purchasingCycleTransactions: new FormGroup({
          purchaseRequest: new FormControl(purchaseRequest),
          inventoryControl: new FormControl(inventoryControl),
          purchaseOfferRequest: new FormControl(purchaseOfferRequest),
          purchaseOffer: new FormControl(purchaseOffer),
          comparePrices: new FormControl(comparePrices),
          purchaseOrder: new FormControl(purchaseOrder),
          purchaseInvoice: new FormControl(purchaseInvoice),
          purchaseReturns: new FormControl(purchaseReturns),
        })
      })
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
