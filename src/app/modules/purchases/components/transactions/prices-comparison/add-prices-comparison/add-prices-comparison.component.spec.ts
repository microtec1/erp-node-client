import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPricesComparisonComponent } from './add-prices-comparison.component';

describe('AddPricesComparisonComponent', () => {
  let component: AddPricesComparisonComponent;
  let fixture: ComponentFixture<AddPricesComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPricesComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPricesComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
