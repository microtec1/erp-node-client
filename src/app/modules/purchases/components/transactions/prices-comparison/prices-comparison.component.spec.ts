import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricesComparisonComponent } from './prices-comparison.component';

describe('PricesComparisonComponent', () => {
  let component: PricesComparisonComponent;
  let fixture: ComponentFixture<PricesComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricesComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
