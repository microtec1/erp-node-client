import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPurchasingPriceComparison } from '../../../interfaces/IPurchasingPriceComparison';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, purchasingPriceComparisonApi, postPurchasingPriceComparisonApi, unPostPurchasingPriceComparisonApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-prices-comparison',
  templateUrl: './prices-comparison.component.html',
  styleUrls: ['./prices-comparison.component.scss']
})
export class PricesComparisonComponent implements OnInit, OnDestroy {
  purchasingPriceComparison: IPurchasingPriceComparison[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getPurchasingPriceComparisonFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(purchasingPriceComparisonApi, pageNumber).subscribe((res: IDataRes) => {
      this.purchasingPriceComparison = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(purchasingPriceComparisonApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.purchasingPriceComparison = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postPurchasingPriceComparisonApi}/${id}`, {}).subscribe(res => {
        this.purchasingPriceComparison = this.generalService.changeStatus(this.purchasingPriceComparison, id, 'posted', 'purchasingPriceComparisonStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostPurchasingPriceComparisonApi}/${id}`, {}).subscribe(res => {
        this.purchasingPriceComparison = this.generalService.changeStatus(this.purchasingPriceComparison, id, 'unposted', 'purchasingPriceComparisonStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getPurchasingPriceComparisonFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(purchasingPriceComparisonApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.purchasingPriceComparison = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      purchasingPriceComparisonDescriptionAr: new FormControl(''),
      purchasingPriceComparisonDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IPurchasingPriceComparison) {
    const initialState = {
      code: item.code,
      nameAr: item.purchasingPriceComparisonDescriptionAr,
      nameEn: item.purchasingPriceComparisonDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(purchasingPriceComparisonApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.purchasingPriceComparison = this.generalService.removeItem(this.purchasingPriceComparison, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getPurchasingPriceComparisonFirstPage() {
    this.subscriptions.push(
      this.data.get(purchasingPriceComparisonApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.purchasingPriceComparison = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
