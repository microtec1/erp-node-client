import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPurchaseOrder } from '../../../interfaces/IPurchaseOrder';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, purchaseOrderApi, postPurchaseOrderApi, unPostPurchaseOrderApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit, OnDestroy {
  purchaseOrder: IPurchaseOrder[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getPurchaseOrderFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(purchaseOrderApi, pageNumber).subscribe((res: IDataRes) => {
      this.purchaseOrder = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(purchaseOrderApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.purchaseOrder = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postPurchaseOrderApi}/${id}`, {}).subscribe(res => {
        this.purchaseOrder = this.generalService.changeStatus(this.purchaseOrder, id, 'posted', 'purchaseOrderStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostPurchaseOrderApi}/${id}`, {}).subscribe(res => {
        this.purchaseOrder = this.generalService.changeStatus(this.purchaseOrder, id, 'unposted', 'purchaseOrderStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getPurchaseOrderFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(purchaseOrderApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.purchaseOrder = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      purchaseOrderDescriptionAr: new FormControl(''),
      purchaseOrderDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IPurchaseOrder) {
    const initialState = {
      code: item.code,
      nameAr: item.purchaseOrderDescriptionAr,
      nameEn: item.purchaseOrderDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(purchaseOrderApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.purchaseOrder = this.generalService.removeItem(this.purchaseOrder, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getPurchaseOrderFirstPage() {
    this.subscriptions.push(
      this.data.get(purchaseOrderApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.purchaseOrder = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
