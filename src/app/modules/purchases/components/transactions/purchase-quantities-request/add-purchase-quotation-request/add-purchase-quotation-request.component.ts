import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IPurchaseQuotationRequest } from 'src/app/modules/purchases/interfaces/IPurchaseQuotationRequest';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  purchaseQuotationRequestApi,
  warehousesApi,
  projectsApi,
  suppliersApi,
  storesItemsApi,
  itemsVariablesApi,
  itemsUnitsApi,
  purchasingRequestApi,
  countriesApi,
  purchasingStockControlApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IProject } from 'src/app/modules/purchases/interfaces/IProject.model';
import { IPurchasingRequest } from 'src/app/modules/purchases/interfaces/IPurchasingRequest';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ICountry } from 'src/app/modules/general/interfaces/ICountry.model';

@Component({
  selector: 'app-add-purchase-quotation-request',
  templateUrl: './add-purchase-quotation-request.component.html',
  styleUrls: ['./add-purchase-quotation-request.component.scss']
})
export class AddPurchaseQuotationRequestComponent implements OnInit, OnDestroy {
  purchaseQuotationRequestForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  purchaseQuotationRequest: IPurchaseQuotationRequest;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  // Supplier Branches
  supplierBranches: any[] = [];
  supplierBranchesInputFocused: boolean;
  supplierBranchesCount: number;
  noSupplierBranches: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Projects
  projects: IProject[] = [];
  projectsInputFocused: boolean;
  hasMoreProjects: boolean;
  projectsCount: number;
  selectedProjectsPage = 1;
  projectsPagesNo: number;
  noProjects: boolean;

  // Countries
  countries: ICountry[] = [];
  countriesInputFocused: boolean;
  hasMoreCountries: boolean;
  countriesCount: number;
  selectedCountriesPage = 1;
  countriesPagesNo: number;
  noCountries: boolean;

  // Source Items
  sourceItems: IPurchasingRequest[] = [];

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(purchaseQuotationRequestApi, null, null, id).subscribe((purchaseQuotationRequest: IPurchaseQuotationRequest) => {
              this.purchaseQuotationRequest = purchaseQuotationRequest;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.purchaseQuotationRequestForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(warehousesApi, 1).subscribe((res: IDataRes) => {
        this.warehousesPagesNo = res.pages;
        this.warehousesCount = res.count;
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        }
        this.warehouses.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(projectsApi, 1).subscribe((res: IDataRes) => {
        this.projectsPagesNo = res.pages;
        this.projectsCount = res.count;
        if (this.projectsPagesNo > this.selectedProjectsPage) {
          this.hasMoreProjects = true;
        }
        this.projects.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(countriesApi, 1).subscribe((res: IDataRes) => {
        this.countriesPagesNo = res.pages;
        this.countriesCount = res.count;
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        }
        this.countries.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  fillBranches(value) {
    const supplier = this.suppliers.find(x => x._id === value);
    this.supplierBranches = supplier.branches;
  }

  searchCountries(event) {
    const searchValue = event;
    const searchQuery = {
      countryNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(countriesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCountries = true;
          } else {
            this.noCountries = false;
            for (const item of res.results) {
              if (this.countries.length) {
                const uniqueCountries = this.countries.filter(x => x._id !== item._id);
                this.countries = uniqueCountries;
              }
              this.countries.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        },
          err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
      );
    }
    if (searchValue.length <= 0) {
      this.noCountries = false;
      this.countriesInputFocused = true;
    } else {
      this.countriesInputFocused = false;
    }
  }

  loadMoreCountries() {
    this.selectedCountriesPage = this.selectedCountriesPage + 1;
    this.subscriptions.push(
      this.data.get(countriesApi, this.selectedCountriesPage).subscribe((res: IDataRes) => {
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        } else {
          this.hasMoreCountries = false;
        }
        for (const item of res.results) {
          if (this.countries.length) {
            const uniqueCountries = this.countries.filter(x => x._id !== item._id);
            this.countries = uniqueCountries;
          }
          this.countries.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(warehousesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noWarehouses = true;
          } else {
            this.noWarehouses = false;
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data.get(warehousesApi, this.selectedWarehousesPage).subscribe((res: IDataRes) => {
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        } else {
          this.hasMoreWarehouses = false;
        }
        for (const item of res.results) {
          if (this.warehouses.length) {
            const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
            this.warehouses = uniqueWarehouses;
          }
          this.warehouses.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchProjects(event) {
    const searchValue = event;
    const searchQuery = {
      projectNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noProjects = true;
            } else {
              this.noProjects = false;
              for (const item of res.results) {
                if (this.projects.length) {
                  const uniqueprojects = this.projects.filter(
                    x => x._id !== item._id
                  );
                  this.projects = uniqueprojects;
                }
                this.projects.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreProjects() {
    this.selectedProjectsPage = this.selectedProjectsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedProjectsPage)
        .subscribe((res: IDataRes) => {
          if (this.projectsPagesNo > this.selectedProjectsPage) {
            this.hasMoreProjects = true;
          } else {
            this.hasMoreProjects = false;
          }
          for (const item of res.results) {
            if (this.projects.length) {
              const uniqueProjects = this.projects.filter(
                x => x._id !== item._id
              );
              this.projects = uniqueProjects;
            }
            this.projects.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }


  filterItems(formValue) {
    console.log(formValue);
  }


  fillRow(item, formGroup) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    for (const x of item.variables) {
      variablesControl.push(
        new FormGroup({
          variableId: new FormControl(x.variableId),
          itemVariableNameId: new FormControl(x.itemVariableNameId)
        })
      );
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getPurchasingDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        if (value) {
          variables.push(
            { name: variable.itemsVariableNameAr, value: value.name }
          );
        }
      }
    }
    return variables;
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.purchaseQuotationRequestForm.controls;
  }

  get getPurchasingDetailsArray() {
    return this.purchaseQuotationRequestForm.get('purchasingDetails') as FormArray;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.purchaseQuotationRequestForm.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.purchaseQuotationRequestForm.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  loadSourceItems(value) {
    if (value === 'purchaseRequest') {
      this.getPurchasingDetailsArray.controls = [];
      const searchValues = {
        purchasingRequestStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchasingRequestApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillSourceItems();
          this.uiService.isLoading.next(false);
        })
      );
    }
    if (value === 'purchaseStockControl') {
      this.getPurchasingDetailsArray.controls = [];
      const searchValues = {
        purchasingStockControlStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchasingStockControlApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillSourceItems();
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  fillSourceItems() {
    for (const item of this.sourceItems) {
      for (const [index, control] of item.purchasingDetails.entries()) {
        this.getPurchasingDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(item._id, Validators.required),
            itemType: new FormControl(control.itemType, Validators.required),
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            countryId: new FormControl(control.countryId)
          })
        );
        this.fillRow(control, (this.getPurchasingDetailsArray.controls[index] as FormGroup));
      }
    }
  }

  deleteSourceItem(id) {
    if (this.sourceItems.length > 1) {
      const index = this.generalService.getIndex(this.purchaseQuotationRequestForm.value.purchasingDetails, id, 'requestId');
      this.getPurchasingDetailsArray.removeAt(index);
      this.sourceItems = this.sourceItems.filter(d => d._id !== id);
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.purchaseQuotationRequestForm.value.gregorianDate !== 'string') {
      this.purchaseQuotationRequestForm.value.gregorianDate =
        this.generalService.format(this.purchaseQuotationRequestForm.value.gregorianDate);
    }
    if (typeof this.purchaseQuotationRequestForm.value.hijriDate !== 'string') {
      this.purchaseQuotationRequestForm.value.hijriDate =
        this.generalService.formatHijriDate(this.purchaseQuotationRequestForm.value.hijriDate);
    }


    if (this.purchaseQuotationRequestForm.value.targetType === 'warehouse') {
      delete this.purchaseQuotationRequestForm.value.projectId;
    } else {
      delete this.purchaseQuotationRequestForm.value.warehouseId;
    }


    if (this.purchaseQuotationRequest) {
      if (this.purchaseQuotationRequestForm.valid) {
        const newpurchaseQuotationRequest = {
          _id: this.purchaseQuotationRequest._id,
          ...this.generalService.checkEmptyFields(this.purchaseQuotationRequestForm.value)
        };
        this.data.put(purchaseQuotationRequestApi, newpurchaseQuotationRequest).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/purchases/purchaseQuotationRequest']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.purchaseQuotationRequestForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.purchaseQuotationRequestForm.value)
        };
        this.subscriptions.push(
          this.data.post(purchaseQuotationRequestApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.purchaseQuotationRequestForm.reset();
            this.purchaseQuotationRequestForm.patchValue({
              isActive: true,
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let purchaseQuotationRequestStatus = 'unposted';
    let purchaseQuotationRequestDescriptionAr = '';
    let purchaseQuotationRequestDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let supplierId = '';
    let supplierBranchId = '';
    let referenceNumber = '';
    let sourceType = 'without';
    let deliveryOptions = 'buyerShop';
    let variablesArray = new FormArray([]);
    let purchasingDetailsArray = new FormArray([]);
    let isActive = true;

    if (this.purchaseQuotationRequest) {
      code = this.purchaseQuotationRequest.code;
      purchaseQuotationRequestStatus = this.purchaseQuotationRequest.purchaseQuotationRequestStatus;
      purchaseQuotationRequestDescriptionAr = this.purchaseQuotationRequest.purchaseQuotationRequestDescriptionAr;
      purchaseQuotationRequestDescriptionEn = this.purchaseQuotationRequest.purchaseQuotationRequestDescriptionEn;
      gregorianDate = new Date(this.purchaseQuotationRequest.gregorianDate + 'UTC');
      hijriDate = this.purchaseQuotationRequest.hijriDate;
      supplierId = this.purchaseQuotationRequest.supplierId;
      supplierBranchId = this.purchaseQuotationRequest.supplierBranchId;
      referenceNumber = this.purchaseQuotationRequest.referenceNumber;
      sourceType = this.purchaseQuotationRequest.sourceType;
      deliveryOptions = this.purchaseQuotationRequest.deliveryOptions;
      isActive = this.purchaseQuotationRequest.isActive;
      purchasingDetailsArray = new FormArray([]);
      for (const control of this.purchaseQuotationRequest.purchasingDetails) {
        variablesArray = new FormArray([]);
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        purchasingDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(control.requestId, Validators.required),
            itemType: new FormControl(control.itemType, Validators.required),
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            countryId: new FormControl(control.countryId, Validators.required),
          })
        );
      }
    }
    this.purchaseQuotationRequestForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      purchaseQuotationRequestStatus: new FormControl(purchaseQuotationRequestStatus),
      purchaseQuotationRequestDescriptionAr: new FormControl(purchaseQuotationRequestDescriptionAr),
      purchaseQuotationRequestDescriptionEn: new FormControl(purchaseQuotationRequestDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      supplierId: new FormControl(supplierId, Validators.required),
      supplierBranchId: new FormControl(supplierBranchId),
      referenceNumber: new FormControl(referenceNumber),
      sourceType: new FormControl(sourceType),
      deliveryOptions: new FormControl(deliveryOptions, Validators.required),
      purchasingDetails: purchasingDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
