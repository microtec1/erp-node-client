import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IPurchaseQuotation } from 'src/app/modules/purchases/interfaces/IPurchaseQuotation';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  purchaseQuotationApi,
  detailedChartOfAccountsApi,
  banksApi,
  safeBoxesApi,
  warehousesApi,
  branchCurrenciesApi,
  taxesApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi,
  suppliersApi,
  purchasingStockControlApi,
  purchasingRequestApi,
  purchaseQuotationRequestApi,
  countriesApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { ITax } from 'src/app/modules/sales/interfaces/ITax';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { IPurchasingRequest } from 'src/app/modules/purchases/interfaces/IPurchasingRequest';
import { ICountry } from 'src/app/modules/general/interfaces/ICountry.model';

@Component({
  selector: 'app-add-purchase-quotation',
  templateUrl: './add-purchase-quotation.component.html',
  styleUrls: ['./add-purchase-quotation.component.scss']
})
export class AddPurchaseQuotationComponent implements OnInit, OnDestroy {
  purchaseQuotationForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  purchaseQuotation: IPurchaseQuotation;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Supplier Branches
  supplierBranches: any[] = [];
  supplierBranchesInputFocused: boolean;
  supplierBranchesCount: number;
  noSupplierBranches: boolean;

  // Taxes
  taxes: ITax[] = [];
  taxesInputFocused: boolean;
  hasMoreTaxes: boolean;
  taxesCount: number;
  selectedTaxesPage = 1;
  taxesPagesNo: number;
  noTaxes: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Countries
  countries: ICountry[] = [];
  countriesInputFocused: boolean;
  hasMoreCountries: boolean;
  countriesCount: number;
  selectedCountriesPage = 1;
  countriesPagesNo: number;
  noCountries: boolean;

  // Source Items
  sourceItems: IPurchasingRequest[] = [];


  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(purchaseQuotationApi, null, null, id).subscribe((purchaseQuotation: IPurchaseQuotation) => {
              this.purchaseQuotation = purchaseQuotation;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.purchaseQuotationForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          for (const item of res.results) {
            if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    const searchValues = {
      taxType: 'discount'
    };

    this.subscriptions.push(
      this.data
        .get(taxesApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.taxesPagesNo = res.pages;
          this.taxesCount = res.count + 1;
          if (this.taxesPagesNo > this.selectedTaxesPage) {
            this.hasMoreTaxes = true;
          }
          this.taxes.push(...res.results);
          const withoutItem = {
            _id: 'without',
            image: null,
            code: null,
            taxNameAr: 'بدون',
            taxNameEn: null,
            ratio: null,
            taxClassification: null,
            taxType: null,
            appliedOnAllItems: null,
            isActive: null
          };
          this.taxes.unshift(withoutItem);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(countriesApi, 1).subscribe((res: IDataRes) => {
        this.countriesPagesNo = res.pages;
        this.countriesCount = res.count;
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        }
        this.countries.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
  }

  fillBranches(value) {
    const supplier = this.suppliers.find(x => x._id === value);
    this.supplierBranches = supplier.branches;
  }

  fillRate(event) {
    const currency = this.currencies.find(item => item.currencyId === event);
    this.purchaseQuotationForm.patchValue({
      currencyExchangeRate: currency.exchangeRate
    });
  }

  searchCountries(event) {
    const searchValue = event;
    const searchQuery = {
      countryNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(countriesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCountries = true;
          } else {
            this.noCountries = false;
            for (const item of res.results) {
              if (this.countries.length) {
                const uniqueCountries = this.countries.filter(x => x._id !== item._id);
                this.countries = uniqueCountries;
              }
              this.countries.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        },
          err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
      );
    }
    if (searchValue.length <= 0) {
      this.noCountries = false;
      this.countriesInputFocused = true;
    } else {
      this.countriesInputFocused = false;
    }
  }

  loadMoreCountries() {
    this.selectedCountriesPage = this.selectedCountriesPage + 1;
    this.subscriptions.push(
      this.data.get(countriesApi, this.selectedCountriesPage).subscribe((res: IDataRes) => {
        if (this.countriesPagesNo > this.selectedCountriesPage) {
          this.hasMoreCountries = true;
        } else {
          this.hasMoreCountries = false;
        }
        for (const item of res.results) {
          if (this.countries.length) {
            const uniqueCountries = this.countries.filter(x => x._id !== item._id);
            this.countries = uniqueCountries;
          }
          this.countries.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              const itemsArray = [];
              for (const item of res.results) {
                if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                  itemsArray.push(item);
                }
              }
              for (const item of itemsArray) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
            const itemsArray = [];
            for (const item of res.results) {
              if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                itemsArray.push(item);
              }
            }
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(
                  x => x._id !== item._id
                );
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchTaxes(event) {
    const searchValue = event;
    const searchQuery = {
      taxNameAr: searchValue,
      taxType: 'discount'
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(taxesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noTaxes = true;
          } else {
            this.noTaxes = false;
            for (const item of res.results) {
              if (this.taxes.length) {
                const uniqueTaxes = this.taxes.filter(x => x._id !== item._id);
                this.taxes = uniqueTaxes;
              }
              this.taxes.push(item);
            }
          }
          this.taxes = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  checkValue(value, group: FormGroup) {
    if (value === 'POSTPONE') {
      const supplier = this.suppliers.find(item => item._id === this.purchaseQuotationForm.value.supplierId);
      if (supplier) {
        group.patchValue({
          accountId: supplier.accountingInfo.glAccountId
        });
      }
    }
  }
  getItem(event, formGroup: FormGroup) {
    this.subscriptions.push(
      this.data.get(storesItemsApi, null, null, event).subscribe((res: any) => {
        this.subscriptions.push(
          this.data.get(taxesApi, null, null, res.otherInformation.vatId).subscribe((data: ITax) => {
            formGroup.patchValue({
              vatPercentage: data.ratio
            });
            this.calculateVatValue(formGroup);
          })
        );

        if (res.barCode.length > 1) {
          const initialState = {
            barCodes: res.barCode
          };
          this.bsModalRef = this.modalService.show(BarcodeModalComponent, { initialState });
          this.subscriptions.push(
            this.bsModalRef.content.confirmed.subscribe(confirmed => {
              if (confirmed) {
                this.searchBarcodes(confirmed.barCode, formGroup);
                this.bsModalRef.hide();
              } else {
                this.bsModalRef.hide();
              }
            })
          );
        } else {
          this.searchBarcodes(res.barCode[0].barCode, formGroup);
        }
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  fillRow(item, formGroup, sourceType?) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    if (sourceType) {
      for (const x of item.variables) {
        variablesControl.push(
          new FormGroup({
            variableId: new FormControl(x.variableId),
            itemVariableNameId: new FormControl(x.itemVariableNameId)
          })
        );
      }
    } else {
      for (const x of item.barCode.variables) {
        variablesControl.push(
          new FormGroup({
            variableId: new FormControl(x.variableId),
            itemVariableNameId: new FormControl(x.itemVariableNameId)
          })
        );
      }
      formGroup.patchValue({
        barCode: item.barCode.barCode,
        itemUnitId: item.barCode.unitId
      });
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getPurchasingDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.purchaseQuotationForm.controls;
  }

  get getPurchasingDetailsArray() {
    return this.purchaseQuotationForm.get('purchasingDetails') as FormArray;
  }

  addItem(group: FormGroup) {
    if (this.getPurchasingDetailsArray.valid) {
      const variablesArray = new FormArray([]);
      for (const item of group.value.variables) {
        variablesArray.push(
          new FormGroup({
            variableId: new FormControl(item.variableId),
            itemVariableNameId: new FormControl(item.itemVariableNameId)
          })
        );
      }

      this.getPurchasingDetailsArray.push(
        new FormGroup({
          itemType: new FormControl(group.value.itemType, Validators.required),
          barCode: new FormControl(group.value.barCode, Validators.required),
          itemId: new FormControl(group.value.itemId, Validators.required),
          variables: variablesArray,
          quantity: new FormControl(group.value.quantity, Validators.required),
          itemUnitId: new FormControl(group.value.itemUnitId),
          cost: new FormControl(group.value.cost, Validators.required),
          discountPercentage: new FormControl(group.value.discountPercentage),
          discountValue: new FormControl(group.value.discountValue),
          totalCost: new FormControl(group.value.totalCost),
          vatPercentage: new FormControl(group.value.vatPercentage),
          vatValue: new FormControl(group.value.vatValue),
          netValue: new FormControl(group.value.netValue, Validators.required),
          descriptionAr: new FormControl(group.value.descriptionAr),
          descriptionEn: new FormControl(group.value.descriptionEn),
          countryId: new FormControl(group.value.countryId),
        })
      );
      group.reset();
    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      this.uiService.isLoading.next(false);
    }

  }

  deleteItem(index) {
    const control = this.purchaseQuotationForm.get('purchasingDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      if (transactionDate) {
        if (this.purchaseQuotationForm.value.offerValidity) {
          this.calculateExpiryDate(this.purchaseQuotationForm.value.offerValidity, value);
        }
      }
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.purchaseQuotationForm.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.purchaseQuotationForm.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  fillRatio(value) {
    if (value !== 'without') {
      const tax = this.taxes.find(x => x._id === value);
      this.purchaseQuotationForm.patchValue({
        discountTaxRate: tax.ratio
      });
      this.calculateTotalItemsDiscount();
    }
  }

  calculateTotalCost(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const cost = group.value.cost ? group.value.cost : 0;
    const quantity = group.value.quantity ? group.value.quantity : 0;
    // const freeQuantity = group.value.freeQuantity ? group.value.freeQuantity : 0;
    // const totalDiscount = group.value.totalDiscount ? group.value.totalDiscount : 0;
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const value = (cost * quantity) - discountValue;
    group.patchValue({
      totalCost: +(value.toFixed(4))
    });
    if (totalCost !== value) {
      this.calculateVatValue(group);
      this.calculateAdditionalDiscount(group);
      this.calculateNetValue(group);
      this.calculateDiscountValue(group);
    }
    this.calculateTotal('totalCost', 'totalCost', 'purchasingDetails');
    this.calculateTotalItemsDiscount();
  }

  calculateVatValue(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const vatPercentage = group.value.vatPercentage ? group.value.vatPercentage : 0;
    group.patchValue({
      vatValue: +(((totalCost * vatPercentage) / 100).toFixed(4))
    });
    this.calculateTotal('vatValue', 'totalVat', 'purchasingDetails');
    this.calculateNetValue(group);
  }

  calculateExpiryDate(value, date?: Date) {
    const days = +value;
    let transactionDate;
    if (date) {
      transactionDate = date;
    } else {
      transactionDate = this.purchaseQuotationForm.value.gregorianDate;
    }
    const expiryDate = new Date();
    expiryDate.setDate(transactionDate.getDate() + days);
    this.purchaseQuotationForm.patchValue({
      expiryDateGregorian: expiryDate
    });
  }

  calculateTotal(fieldName, totalName, arrayName, group?: FormGroup) {
    const array = this.purchaseQuotationForm.get(arrayName) as FormArray;
    const valuesArray = [];
    for (const item of array.value) {
      valuesArray.push(+item[fieldName]);
    }
    const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);
    this.purchaseQuotationForm.patchValue({
      [totalName]: valuesSum
    });

    if (group) {
      if (fieldName === 'discountValue') {
        const totalCost = group.value.totalCost ? group.value.totalCost : 0;
        group.patchValue({
          discountPercentage: +(((group.value[fieldName] * 100) / totalCost).toFixed(4))
        });
      }
      this.calculateAdditionalDiscount(group);
    }
  }

  calculateAdditionalDiscount(group: FormGroup) {
    const additionalDiscountTable = group.value.additionalDiscount ? group.value.additionalDiscount : 0;
    const cost = group.value.cost ? group.value.cost : 0;
    const quantity = group.value.quantity ? group.value.quantity : 0;
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const totalCost = this.purchaseQuotationForm.value.totalCost ? this.purchaseQuotationForm.value.totalCost : 0;
    const discountItems = this.purchaseQuotationForm.value.discountItems ? this.purchaseQuotationForm.value.discountItems : 0;
    const additionalDiscount = this.purchaseQuotationForm.value.additionalDiscount ? this.purchaseQuotationForm.value.additionalDiscount : 0;
    const value = (((cost * quantity) - discountValue) / (totalCost + discountItems)) * additionalDiscount;

    group.patchValue({
      additionalDiscount: +(value.toFixed(4))
    });
    if (value !== additionalDiscountTable) {
      this.calculateTotalDiscount(group);
    }
  }

  calculateTotalDiscount(group: FormGroup) {
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const additionalDiscount = group.value.additionalDiscount ? group.value.additionalDiscount : 0;
    group.patchValue({
      totalDiscount: +((discountValue + additionalDiscount).toFixed(4))
    });
    this.calculateTotalCost(group);
  }

  calculateDiscountValue(group: FormGroup) {
    const discountPercentage = group.value.discountPercentage ? group.value.discountPercentage : 0;
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    group.patchValue({
      discountValue: +(((discountPercentage * totalCost) / 100).toFixed(4))
    });
    this.calculateAdditionalDiscount(group);
    this.calculateTotalCost(group);
    this.calculateTotal('discountValue', 'totalDiscount', 'purchasingDetails');
  }

  calculateTotalItemsDiscount() {
    const totalCost = this.purchaseQuotationForm.value.totalCost ? this.purchaseQuotationForm.value.totalCost : 0;
    const discountTaxRate = this.purchaseQuotationForm.value.discountTaxRate ? this.purchaseQuotationForm.value.discountTaxRate : 0;
    this.purchaseQuotationForm.patchValue({
      totalDiscountTax: +(((totalCost * discountTaxRate) / 100).toFixed(4))
    });
  }

  calculateNetValue(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const vatValue = group.value.vatValue ? group.value.vatValue : 0;
    group.patchValue({
      netValue: +((totalCost + vatValue).toFixed(4))
    });
    this.calculateTotal('netValue', 'totalNet', 'purchasingDetails');
  }

  calculateTable() {
    for (const item of this.getPurchasingDetailsArray.controls) {
      const group = item as FormGroup;
      this.calculateAdditionalDiscount(group);
    }
  }

  loadSourceItems(value) {
    this.sourceItems = [];
    if (value === 'purchaseRequest') {
      this.getPurchasingDetailsArray.controls = [];
      const searchValues = {
        purchasingRequestStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchasingRequestApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillSourceItems();
          this.uiService.isLoading.next(false);
        })
      );
    }
    if (value === 'purchaseStockControl') {
      this.getPurchasingDetailsArray.controls = [];
      const searchValues = {
        purchasingStockControlStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchasingStockControlApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillSourceItems();
          this.uiService.isLoading.next(false);
        })
      );
    }
    if (value === 'purchaseQuotationRequest') {
      this.getPurchasingDetailsArray.controls = [];
      const searchValues = {
        purchaseQuotationRequestStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchaseQuotationRequestApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillSourceItems();
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  fillSourceItems() {
    console.log(this.sourceItems);

    for (const item of this.sourceItems) {
      for (const [index, control] of item.purchasingDetails.entries()) {
        this.getPurchasingDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(item._id, Validators.required),
            itemType: new FormControl(control.itemType, Validators.required),
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(0, Validators.required),
            discountPercentage: new FormControl(0),
            discountValue: new FormControl(0),
            totalCost: new FormControl(0),
            vatPercentage: new FormControl(0),
            vatValue: new FormControl(0),
            netValue: new FormControl(0, Validators.required),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
            countryId: new FormControl(''),
          })
        );
        this.fillRow(control, (this.getPurchasingDetailsArray.controls[index] as FormGroup), 'sourceItem');
      }
    }
  }

  deleteSourceItem(id) {
    if (this.sourceItems.length > 1) {
      const index = this.generalService.getIndex(this.purchaseQuotationForm.value.purchasingDetails, id, 'requestId');
      this.getPurchasingDetailsArray.removeAt(index);
      this.sourceItems = this.sourceItems.filter(d => d._id !== id);
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.purchaseQuotationForm.value.gregorianDate !== 'string') {
      this.purchaseQuotationForm.value.gregorianDate =
        this.generalService.format(this.purchaseQuotationForm.value.gregorianDate);
    }
    if (typeof this.purchaseQuotationForm.value.hijriDate !== 'string') {
      this.purchaseQuotationForm.value.hijriDate =
        this.generalService.formatHijriDate(this.purchaseQuotationForm.value.hijriDate);
    }

    if (this.purchaseQuotationForm.value.expiryDateGregorian) {
      if (typeof this.purchaseQuotationForm.value.expiryDateGregorian !== 'string') {
        this.purchaseQuotationForm.value.expiryDateGregorian =
          this.generalService.format(this.purchaseQuotationForm.value.expiryDateGregorian);
      }
      if (typeof this.purchaseQuotationForm.value.expiryDateHijri !== 'string') {
        this.purchaseQuotationForm.value.expiryDateHijri =
          this.generalService.formatHijriDate(this.purchaseQuotationForm.value.expiryDateHijri);
      }
    }

    if (this.purchaseQuotation) {
      if (this.purchaseQuotationForm.valid) {
        const newpurchaseQuotation = {
          _id: this.purchaseQuotation._id,
          ...this.generalService.checkEmptyFields(this.purchaseQuotationForm.value)
        };
        this.data.put(purchaseQuotationApi, newpurchaseQuotation).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/purchases/purchaseQuotation']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.purchaseQuotationForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.purchaseQuotationForm.value)
        };
        this.subscriptions.push(
          this.data.post(purchaseQuotationApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.purchaseQuotationForm.reset();
            this.purchaseQuotationForm.patchValue({
              isActive: true,
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let purchaseQuotationPeriodic = false;
    let purchaseQuotationStatus = 'unposted';
    let purchaseQuotationDescriptionAr = '';
    let purchaseQuotationDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let supplierId = '';
    let supplierBranchId = '';
    let referenceNumber = '';
    let sourceType = 'without';
    let currencyId = '';
    let currencyExchangeRate = 0;
    let offerValidity = null;
    let expiryDateGregorian = null;
    let expiryDateHijri = null;
    let discountTaxType = 'without';
    let discountTaxRate = 0;
    let totalDiscount = 0;
    let totalCost = 0;
    let totalDiscountTax = 0;
    let totalVat = 0;
    let totalNet = 0;
    let variablesArray = new FormArray([]);
    let purchasingDetailsArray = new FormArray([]);
    let isActive = true;

    if (this.purchaseQuotation) {
      code = this.purchaseQuotation.code;
      purchaseQuotationPeriodic = this.purchaseQuotation.purchaseQuotationPeriodic;
      purchaseQuotationStatus = this.purchaseQuotation.purchaseQuotationStatus;
      purchaseQuotationDescriptionAr = this.purchaseQuotation.purchaseQuotationDescriptionAr;
      purchaseQuotationDescriptionEn = this.purchaseQuotation.purchaseQuotationDescriptionEn;
      gregorianDate = new Date(this.purchaseQuotation.gregorianDate + 'UTC');
      hijriDate = this.purchaseQuotation.hijriDate;
      supplierId = this.purchaseQuotation.supplierId;
      supplierBranchId = this.purchaseQuotation.supplierBranchId;
      referenceNumber = this.purchaseQuotation.referenceNumber;
      sourceType = this.purchaseQuotation.sourceType;
      currencyId = this.purchaseQuotation.currencyId;
      currencyExchangeRate = this.purchaseQuotation.currencyExchangeRate;
      offerValidity = this.purchaseQuotation.offerValidity;
      expiryDateGregorian = new Date(this.purchaseQuotation.expiryDateGregorian + 'UTC');
      expiryDateHijri = this.purchaseQuotation.expiryDateHijri;
      discountTaxType = this.purchaseQuotation.discountTaxType;
      discountTaxRate = this.purchaseQuotation.discountTaxRate;
      isActive = this.purchaseQuotation.isActive;
      totalDiscount = this.purchaseQuotation.totalDiscount;
      totalCost = this.purchaseQuotation.totalCost;
      totalDiscountTax = this.purchaseQuotation.totalDiscountTax;
      totalVat = this.purchaseQuotation.totalVat;
      totalNet = this.purchaseQuotation.totalNet;
      purchasingDetailsArray = new FormArray([]);
      for (const control of this.purchaseQuotation.purchasingDetails) {
        variablesArray = new FormArray([]);
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        purchasingDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(control.requestId, Validators.required),
            itemType: new FormControl(control.itemType, Validators.required),
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(control.cost, Validators.required),
            discountPercentage: new FormControl(control.discountPercentage),
            discountValue: new FormControl(control.discountValue),
            totalCost: new FormControl(control.totalCost),
            vatPercentage: new FormControl(control.vatPercentage),
            vatValue: new FormControl(control.vatValue),
            netValue: new FormControl(control.netValue, Validators.required),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            countryId: new FormControl(control.countryId),
          })
        );
      }
    }
    this.purchaseQuotationForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      purchaseQuotationPeriodic: new FormControl(purchaseQuotationPeriodic),
      purchaseQuotationStatus: new FormControl(purchaseQuotationStatus),
      purchaseQuotationDescriptionAr: new FormControl(purchaseQuotationDescriptionAr),
      purchaseQuotationDescriptionEn: new FormControl(purchaseQuotationDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      supplierId: new FormControl(supplierId, Validators.required),
      supplierBranchId: new FormControl(supplierBranchId),
      referenceNumber: new FormControl(referenceNumber),
      sourceType: new FormControl(sourceType, Validators.required),
      currencyId: new FormControl(currencyId, Validators.required),
      currencyExchangeRate: new FormControl(currencyExchangeRate),
      offerValidity: new FormControl(offerValidity),
      expiryDateGregorian: new FormControl(expiryDateGregorian),
      expiryDateHijri: new FormControl(expiryDateHijri),
      discountTaxType: new FormControl(discountTaxType, Validators.required),
      discountTaxRate: new FormControl(discountTaxRate, Validators.required),
      totalDiscount: new FormControl(totalDiscount, Validators.required),
      totalCost: new FormControl(totalCost, Validators.required),
      totalDiscountTax: new FormControl(totalDiscountTax, Validators.required),
      totalVat: new FormControl(totalVat, Validators.required),
      totalNet: new FormControl(totalNet, Validators.required),
      purchasingDetails: purchasingDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
