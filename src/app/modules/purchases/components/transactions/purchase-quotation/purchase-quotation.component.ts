import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPurchaseQuotation } from '../../../interfaces/IPurchaseQuotation';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, purchaseQuotationApi, postPurchaseQuotationApi, unPostPurchaseQuotationApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-purchase-quotation',
  templateUrl: './purchase-quotation.component.html',
  styleUrls: ['./purchase-quotation.component.scss']
})
export class PurchaseQuotationComponent implements OnInit, OnDestroy {
  purchaseQuotation: IPurchaseQuotation[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getPurchaseQuotationFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(purchaseQuotationApi, pageNumber).subscribe((res: IDataRes) => {
      this.purchaseQuotation = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(purchaseQuotationApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.purchaseQuotation = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postPurchaseQuotationApi}/${id}`, {}).subscribe(res => {
        this.purchaseQuotation = this.generalService.changeStatus(this.purchaseQuotation, id, 'posted', 'purchaseQuotationStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostPurchaseQuotationApi}/${id}`, {}).subscribe(res => {
        this.purchaseQuotation = this.generalService.changeStatus(this.purchaseQuotation, id, 'unposted', 'purchaseQuotationStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getPurchaseQuotationFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(purchaseQuotationApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.purchaseQuotation = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      purchaseQuotationDescriptionAr: new FormControl(''),
      purchaseQuotationDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IPurchaseQuotation) {
    const initialState = {
      code: item.code,
      nameAr: item.purchaseQuotationDescriptionAr,
      nameEn: item.purchaseQuotationDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(purchaseQuotationApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.purchaseQuotation = this.generalService.removeItem(this.purchaseQuotation, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getPurchaseQuotationFirstPage() {
    this.subscriptions.push(
      this.data.get(purchaseQuotationApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.purchaseQuotation = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
