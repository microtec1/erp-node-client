import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPurchasingInvoiceComponent } from './add-purchasing-invoice.component';

describe('AddPurchasingInvoiceComponent', () => {
  let component: AddPurchasingInvoiceComponent;
  let fixture: ComponentFixture<AddPurchasingInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPurchasingInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPurchasingInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
