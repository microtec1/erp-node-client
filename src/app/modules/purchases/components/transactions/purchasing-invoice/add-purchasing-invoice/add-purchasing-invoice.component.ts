import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IPurchasingInvoice } from 'src/app/modules/purchases/interfaces/IPurchasingInvoice';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  purchasingInvoiceApi,
  detailedChartOfAccountsApi,
  banksApi,
  servicesItemsApi,
  safeBoxesApi,
  warehousesApi,
  projectsApi,
  branchCurrenciesApi,
  taxesApi,
  suppliersApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi,
  purchasingRequestApi,
  purchasingStockControlApi,
  purchaseQuotationApi,
  purchasingPriceComparisonApi,
  purchaseOrderApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { ITax } from 'src/app/modules/sales/interfaces/ITax';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IProject } from 'src/app/modules/purchases/interfaces/IProject.model';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IServicesItem } from 'src/app/modules/inventory/interfaces/IServicesItem';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { BatchDataModalComponent } from 'src/app/common/components/batch-data-modal/batch-data-modal.component';

@Component({
  selector: 'app-add-purchasing-invoice',
  templateUrl: './add-purchasing-invoice.component.html',
  styleUrls: ['./add-purchasing-invoice.component.scss']
})
export class AddPurchasingInvoiceComponent implements OnInit, OnDestroy {
  purchasingInvoiceForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  purchasingInvoice: IPurchasingInvoice;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  suppliersInputFocused2: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // supplier Branches
  supplierBranches: any[] = [];
  supplierBranchesInputFocused: boolean;
  supplierBranchesCount: number;
  nosupplierBranches: boolean;

  // Taxes
  taxes: ITax[] = [];
  taxesInputFocused: boolean;
  hasMoreTaxes: boolean;
  taxesCount: number;
  selectedTaxesPage = 1;
  taxesPagesNo: number;
  noTaxes: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Projects
  projects: IProject[] = [];
  projectsInputFocused: boolean;
  hasMoreProjects: boolean;
  projectsCount: number;
  selectedProjectsPage = 1;
  projectsPagesNo: number;
  noProjects: boolean;

  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Service Items
  servicesItems: IServicesItem[] = [];
  servicesItemsInputFocused: boolean;
  hasMoreServicesItems: boolean;
  servicesItemsCount: number;
  selectedServicesItemsPage = 1;
  servicesItemsPagesNo: number;
  noServicesItems: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Source Items
  sourceItems: any[] = [];

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(purchasingInvoiceApi, null, null, id).subscribe((purchasingInvoice: IPurchasingInvoice) => {
              this.purchasingInvoice = purchasingInvoice;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.purchasingInvoiceForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(servicesItemsApi, 1).subscribe((res: IDataRes) => {
        this.servicesItemsPagesNo = res.pages;
        this.servicesItemsCount = res.count;
        if (this.servicesItemsPagesNo > this.selectedServicesItemsPage) {
          this.hasMoreServicesItems = true;
        }
        this.servicesItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(warehousesApi, 1).subscribe((res: IDataRes) => {
        this.warehousesPagesNo = res.pages;
        this.warehousesCount = res.count;
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        }
        this.warehouses.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(projectsApi, 1).subscribe((res: IDataRes) => {
        this.projectsPagesNo = res.pages;
        this.projectsCount = res.count;
        if (this.projectsPagesNo > this.selectedProjectsPage) {
          this.hasMoreProjects = true;
        }
        this.projects.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    const searchValues = {
      taxType: 'discount'
    };

    this.subscriptions.push(
      this.data
        .get(taxesApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.taxesPagesNo = res.pages;
          this.taxesCount = res.count + 1;
          if (this.taxesPagesNo > this.selectedTaxesPage) {
            this.hasMoreTaxes = true;
          }
          this.taxes.push(...res.results);
          const withoutItem = {
            _id: 'without',
            image: null,
            code: null,
            taxNameAr: 'بدون',
            taxNameEn: null,
            ratio: null,
            taxClassification: null,
            taxType: null,
            appliedOnAllItems: null,
            isActive: null
          };
          this.taxes.unshift(withoutItem);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
  }

  fillBranches(value) {
    const supplier = this.suppliers.find(x => x._id === value);
    this.supplierBranches = supplier.branches;
    for (const item of this.getPaymentsArray.controls) {
      item.patchValue({
        supplierId: value
      });
    }
  }

  fillRate(event, group: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (group) {
      group.patchValue({
        currencyExchangeRate: currency.exchangeRate
      });
    } else {
      this.purchasingInvoiceForm.patchValue({
        currencyExchangeRate: currency.exchangeRate
      });
    }
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(warehousesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noWarehouses = true;
          } else {
            this.noWarehouses = false;
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data.get(warehousesApi, this.selectedWarehousesPage).subscribe((res: IDataRes) => {
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        } else {
          this.hasMoreWarehouses = false;
        }
        for (const item of res.results) {
          if (this.warehouses.length) {
            const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
            this.warehouses = uniqueWarehouses;
          }
          this.warehouses.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchServicesItems(event) {
    const searchValue = event;
    const searchQuery = {
      servicesItemNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(servicesItemsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noServicesItems = true;
          } else {
            this.noServicesItems = false;
            for (const item of res.results) {
              if (this.servicesItems.length) {
                const uniqueServicesItems = this.servicesItems.filter(x => x._id !== item._id);
                this.servicesItems = uniqueServicesItems;
              }
              this.servicesItems.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreservicesItems() {
    this.selectedServicesItemsPage = this.selectedServicesItemsPage + 1;
    this.subscriptions.push(
      this.data.get(servicesItemsApi, this.selectedServicesItemsPage).subscribe((res: IDataRes) => {
        if (this.servicesItemsPagesNo > this.selectedServicesItemsPage) {
          this.hasMoreServicesItems = true;
        } else {
          this.hasMoreServicesItems = false;
        }
        for (const item of res.results) {
          if (this.servicesItems.length) {
            const uniqueServicesItems = this.servicesItems.filter(x => x._id !== item._id);
            this.servicesItems = uniqueServicesItems;
          }
          this.servicesItems.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchProjects(event) {
    const searchValue = event;
    const searchQuery = {
      projectNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noProjects = true;
            } else {
              this.noProjects = false;
              for (const item of res.results) {
                if (this.projects.length) {
                  const uniqueprojects = this.projects.filter(
                    x => x._id !== item._id
                  );
                  this.projects = uniqueprojects;
                }
                this.projects.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreProjects() {
    this.selectedProjectsPage = this.selectedProjectsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedProjectsPage)
        .subscribe((res: IDataRes) => {
          if (this.projectsPagesNo > this.selectedProjectsPage) {
            this.hasMoreProjects = true;
          } else {
            this.hasMoreProjects = false;
          }
          for (const item of res.results) {
            if (this.projects.length) {
              const uniqueProjects = this.projects.filter(
                x => x._id !== item._id
              );
              this.projects = uniqueProjects;
            }
            this.projects.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchTaxes(event) {
    const searchValue = event;
    const searchQuery = {
      taxNameAr: searchValue,
      taxType: 'discount'
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(taxesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noTaxes = true;
          } else {
            this.noTaxes = false;
            for (const item of res.results) {
              if (this.taxes.length) {
                const uniqueTaxes = this.taxes.filter(x => x._id !== item._id);
                this.taxes = uniqueTaxes;
              }
              this.taxes.push(item);
            }
          }
          this.taxes = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoresafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  checkValue(value, group: FormGroup) {
    if (value === 'POSTPONE') {
      const supplier = this.suppliers.find(item => item._id === this.purchasingInvoiceForm.value.supplierId);
      if (supplier) {
        group.patchValue({
          accountId: supplier.accountingInfo.glAccountId
        });
      }
    }
  }

  getItem(event, formGroup: FormGroup) {
    this.subscriptions.push(
      this.data.get(storesItemsApi, null, null, event).subscribe((res: any) => {
        this.subscriptions.push(
          this.data.get(taxesApi, null, null, res.otherInformation.vatId).subscribe((data: ITax) => {
            formGroup.patchValue({
              vatPercentage: data.ratio
            });
            this.calculateVatValue(formGroup);
          })
        );

        if (res.barCode.length > 1) {
          const initialState = {
            barCodes: res.barCode
          };
          this.bsModalRef = this.modalService.show(BarcodeModalComponent, { initialState });
          this.subscriptions.push(
            this.bsModalRef.content.confirmed.subscribe(confirmed => {
              if (confirmed) {
                this.searchBarcodes(confirmed.barCode, formGroup);
                this.bsModalRef.hide();
              } else {
                this.bsModalRef.hide();
              }
            })
          );
        } else {
          this.searchBarcodes(res.barCode[0].barCode, formGroup);
        }
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  checkBatch(id) {
    if (id) {
      const item = this.storeItems.find(x => x._id === id);
      if (item) {
        if (item.otherInformation.workWithBatch) {
          return false;
        } else {
          return true;
        }
      }
    }
  }

  openBatchModal(id, formGroup: FormGroup, index) {
    if (id) {
      const initialState = {
        itemId: id,
        request: formGroup.value.itemBatchDetails
      };
      this.bsModalRef = this.modalService.show(BatchDataModalComponent, { initialState, class: 'big-modal' });
      this.subscriptions.push(
        this.bsModalRef.content.formValue.subscribe(formValue => {
          if (formValue) {
            const array = formGroup.controls.itemBatchDetails as FormArray;
            array.controls = [];
            for (const item of formValue.itemBatchDetails) {
              array.push(
                new FormGroup({
                  batchNumber: new FormControl(item.batchNumber),
                  quantity: new FormControl(item.quantity),
                  batchDateGregorian: new FormControl(item.batchDateGregorian),
                  batchDateHijri: new FormControl(item.batchDateHijri),
                  expiryDateGregorian: new FormControl(item.expiryDateGregorian),
                  expiryDateHijri: new FormControl(item.expiryDateHijri)
                })
              );
            }
            this.bsModalRef.hide();
          } else {
            this.bsModalRef.hide();
          }
        })
      );
    }
  }

  fillRow(item, formGroup, sourceType?) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    if (sourceType) {
      for (const x of item.variables) {
        variablesControl.push(
          new FormGroup({
            variableId: new FormControl(x.variableId),
            itemVariableNameId: new FormControl(x.itemVariableNameId)
          })
        );
      }
    } else {
      for (const x of item.barCode.variables) {
        variablesControl.push(
          new FormGroup({
            variableId: new FormControl(x.variableId),
            itemVariableNameId: new FormControl(x.itemVariableNameId)
          })
        );
      }
      formGroup.patchValue({
        barCode: item.barCode.barCode,
        itemUnitId: item.barCode.unitId
      });
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getPurchasingDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.purchasingInvoiceForm.controls;
  }

  get getPurchasingDetailsArray() {
    return this.purchasingInvoiceForm.get('purchasingDetails') as FormArray;
  }

  get getPaymentsArray() {
    return this.purchasingInvoiceForm.get('payments') as FormArray;
  }

  get getAdditionalCostsArray() {
    return this.purchasingInvoiceForm.get('additionalCosts') as FormArray;
  }

  addPayment() {
    const control = this.purchasingInvoiceForm.get('payments') as FormArray;
    control.push(
      new FormGroup({
        paymentMethod: new FormControl('CASH', Validators.required),
        accountId: new FormControl('', Validators.required),
        description: new FormControl(''),
        value: new FormControl(0, Validators.required),
        balance: new FormControl(0, Validators.required),
        dueDateGregorian: new FormControl(null),
        dueDateHijri: new FormControl(null),
        documentNumber: new FormControl(0),
        documentDateGregorian: new FormControl(null),
        documentDateHijri: new FormControl(null),
        supplierId: new FormControl('', Validators.required)
      })
    );
  }

  deletePayment(index) {
    const control = this.purchasingInvoiceForm.get('payments') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addAdditionCost() {
    const control = this.purchasingInvoiceForm.get('additionalCosts') as FormArray;
    control.push(
      new FormGroup({
        itemId: new FormControl(''),
        value: new FormControl(0),
        taxRate: new FormControl(0),
        taxValue: new FormControl(0),
        netValue: new FormControl(0),
        supplierId: new FormControl(''),
        paymentMethod: new FormControl('CASH'),
        accountId: new FormControl(''),
        currencyId: new FormControl(''),
        currencyExchangeRate: new FormControl(null),
        serviceInvoiceNumber: new FormControl(0)
      })
    );
  }

  deleteAdditionCost(index) {
    const control = this.purchasingInvoiceForm.get('additionalCosts') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      if (transactionDate) {
        if (this.purchasingInvoiceForm.value.offerValidity) {
          this.calculateExpiryDate(this.purchasingInvoiceForm.value.offerValidity, value);
        }
        for (const control of this.getPaymentsArray.controls) {
          control.patchValue({
            dueDateGregorian: value
          });
        }
      }
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.purchasingInvoiceForm.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (transactionDate) {
        for (const control of this.getPaymentsArray.controls) {
          control.patchValue({
            dueDateGregorian: this.generalService.format(
              new Date(
                gegorianDate.year(),
                gegorianDate.month(),
                gegorianDate.date()
              )
            )
          });
        }
      }
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.purchasingInvoiceForm.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  fillRatio(value) {
    if (value !== 'without') {
      const tax = this.taxes.find(x => x._id === value);
      this.purchasingInvoiceForm.patchValue({
        discountTaxRate: tax.ratio
      });
      this.calculateTotalItemsDiscount();
    }
  }

  calculateTotalCost(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const cost = group.value.cost ? group.value.cost : 0;
    const quantity = group.value.quantity ? group.value.quantity : 0;
    const freeQuantity = group.value.freeQuantity ? group.value.freeQuantity : 0;
    const totalDiscount = group.value.totalDiscount ? group.value.totalDiscount : 0;
    const value = (cost * (quantity + freeQuantity)) - totalDiscount;
    group.patchValue({
      totalCost: +(value.toFixed(4))
    });
    if (totalCost !== value) {
      this.calculateVatValue(group);
      this.calculateAdditionalDiscount(group);
      this.calculateNetValue(group);
      this.calculateDiscountValue(group);
    }
    this.calculateTotal('totalCost', 'totalCost', 'purchasingDetails');
    this.calculateTotalItemsDiscount();
  }

  calculateVatValue(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const vatPercentage = group.value.vatPercentage ? group.value.vatPercentage : 0;
    group.patchValue({
      vatValue: +(((totalCost * vatPercentage) / 100).toFixed(4))
    });
    this.calculateTotal('vatValue', 'totalVat', 'purchasingDetails');
    this.calculateNetValue(group);
  }

  calculateExpiryDate(value, date?: Date) {
    const days = +value;
    let transactionDate;
    if (date) {
      transactionDate = date;
    } else {
      transactionDate = this.purchasingInvoiceForm.value.gregorianDate;
    }
    const expiryDate = new Date();
    expiryDate.setDate(transactionDate.getDate() + days);
    this.purchasingInvoiceForm.patchValue({
      expiryDateGregorian: expiryDate
    });
  }

  calculateTotal(fieldName, totalName, arrayName, group?: FormGroup) {
    const array = this.purchasingInvoiceForm.get(arrayName) as FormArray;
    const valuesArray = [];
    for (const item of array.value) {
      valuesArray.push(+item[fieldName]);
    }
    const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);
    this.purchasingInvoiceForm.patchValue({
      [totalName]: valuesSum
    });

    if (group) {
      if (fieldName === 'discountValue') {
        const totalCost = group.value.totalCost ? group.value.totalCost : 0;
        group.patchValue({
          discountPercentage: +(((group.value[fieldName] * 100) / totalCost).toFixed(4))
        });
      }
      this.calculateAdditionalDiscount(group);
    }
  }

  calculateAdditionalDiscount(group: FormGroup) {
    const additionalDiscountTable = group.value.additionalDiscount ? group.value.additionalDiscount : 0;
    const cost = group.value.cost ? group.value.cost : 0;
    const quantity = group.value.quantity ? group.value.quantity : 0;
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const totalCost = this.purchasingInvoiceForm.value.totalCost ? this.purchasingInvoiceForm.value.totalCost : 0;
    const discountItems = this.purchasingInvoiceForm.value.discountItems ? this.purchasingInvoiceForm.value.discountItems : 0;
    const additionalDiscount = this.purchasingInvoiceForm.value.additionalDiscount ? this.purchasingInvoiceForm.value.additionalDiscount : 0;
    const value = (((cost * quantity) - discountValue) / (totalCost + discountItems)) * additionalDiscount;

    group.patchValue({
      additionalDiscount: +(value.toFixed(4))
    });
    if (value !== additionalDiscountTable) {
      this.calculateTotalDiscount(group);
    }
  }

  calculateTotalDiscount(group: FormGroup) {
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const additionalDiscount = group.value.additionalDiscount ? group.value.additionalDiscount : 0;
    group.patchValue({
      totalDiscount: +((discountValue + additionalDiscount).toFixed(4))
    });
    this.calculateTotalCost(group);
    this.calculateTotal('totalDiscount', 'totalDiscount', 'purchasingDetails');
  }

  calculateDiscountValue(group: FormGroup) {
    const discountPercentage = group.value.discountPercentage ? group.value.discountPercentage : 0;
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    group.patchValue({
      discountValue: +(((discountPercentage * totalCost) / 100).toFixed(4))
    });
    this.calculateAdditionalDiscount(group);
    this.calculateTotal('discountValue', 'discountItems', 'purchasingDetails');
  }

  calculateTotalItemsDiscount() {
    const totalCost = this.purchasingInvoiceForm.value.totalCost ? this.purchasingInvoiceForm.value.totalCost : 0;
    const discountTaxRate = this.purchasingInvoiceForm.value.discountTaxRate ? this.purchasingInvoiceForm.value.discountTaxRate : 0;
    this.purchasingInvoiceForm.patchValue({
      totalDiscountTax: +(((totalCost * discountTaxRate) / 100).toFixed(4))
    });
  }

  calculateNetValue(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const vatValue = group.value.vatValue ? group.value.vatValue : 0;
    group.patchValue({
      netValue: +((totalCost + vatValue).toFixed(4))
    });
    this.calculateTotal('netValue', 'totalNet', 'purchasingDetails');
  }

  calculateTable() {
    for (const item of this.getPurchasingDetailsArray.controls) {
      const group = item as FormGroup;
      this.calculateAdditionalDiscount(group);
    }
  }

  loadSourceItems(value) {
    if (value === 'purchaseRequest') {
      const searchValues = {
        purchasingRequestStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchasingRequestApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillTables(this.sourceItems);
          this.uiService.isLoading.next(false);
        })
      );
    }
    if (value === 'purchaseStockControl') {
      const searchValues = {
        purchasingStockControlStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchasingStockControlApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillTables(this.sourceItems);
          this.uiService.isLoading.next(false);
        })
      );
    }
    if (value === 'purchaseQuotation') {
      const searchValues = {
        purchaseQuotationStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchaseQuotationApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillTables(this.sourceItems);
          this.uiService.isLoading.next(false);
        })
      );
    }
    if (value === 'purchasePriceComparison') {
      const searchValues = {
        purchasingPriceComparisonStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchasingPriceComparisonApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillTables(this.sourceItems);
          this.uiService.isLoading.next(false);
        })
      );
    }

    if (value === 'purchaseOrder') {
      const searchValues = {
        purchaseOrderStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(purchaseOrderApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillTables(this.sourceItems);
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  fillTables(array: any[]) {
    if (array.length) {
      this.getPurchasingDetailsArray.controls = [];
      this.getPaymentsArray.controls = [];
      for (const item of array) {
        for (const control of item.purchasingDetails) {
          const variablesArray = new FormArray([]);
          for (const item2 of control.variables) {
            variablesArray.push(
              new FormGroup({
                variableId: new FormControl(item2.variableId),
                itemVariableNameId: new FormControl(item2.itemVariableNameId)
              })
            );
          }
          const itemBatchDetailsArray = new FormArray([]);
          this.getPurchasingDetailsArray.push(
            new FormGroup({
              requestId: new FormControl(item._id, Validators.required),
              itemType: new FormControl(control.itemType, Validators.required),
              barCode: new FormControl(control.barCode, Validators.required),
              itemId: new FormControl(control.itemId, Validators.required),
              variables: variablesArray,
              quantity: new FormControl(control.quantity, Validators.required),
              freeQuantity: new FormControl(control.freeQuantity),
              itemUnitId: new FormControl(control.itemUnitId),
              cost: new FormControl(control.cost, Validators.required),
              discountPercentage: new FormControl(control.discountPercentage),
              discountValue: new FormControl(control.discountValue),
              additionalDiscount: new FormControl(control.additionalDiscount),
              totalDiscount: new FormControl(control.totalDiscount),
              totalCost: new FormControl(control.totalCost),
              vatPercentage: new FormControl(control.vatPercentage),
              vatValue: new FormControl(control.vatValue),
              additionalCost: new FormControl(control.additionalCost),
              netValue: new FormControl(control.netValue, Validators.required),
              descriptionAr: new FormControl(control.descriptionAr),
              descriptionEn: new FormControl(control.descriptionEn),
              warehouseId: new FormControl(control.warehouseId),
              itemBatchDetails: itemBatchDetailsArray
            })
          );
        }
        for (const control of item.payments) {
          this.getPaymentsArray.push(
            new FormGroup({
              paymentMethod: new FormControl(control.paymentMethod, Validators.required),
              accountId: new FormControl(control.accountId, Validators.required),
              description: new FormControl(control.description),
              value: new FormControl(control.value, Validators.required),
              balance: new FormControl(control.balance, Validators.required),
              dueDateGregorian: new FormControl(new Date(control.dueDateGregorian + 'UTC')),
              dueDateHijri: new FormControl(control.dueDateHijri),
              documentNumber: new FormControl(control.documentNumber),
              documentDateGregorian: new FormControl(new Date(control.documentDateGregorian + 'UTC')),
              documentDateHijri: new FormControl(control.documentDateHijri),
              supplierId: new FormControl(control.supplierId, Validators.required),
            })
          );
        }
      }
    }
  }

  deleteSourceItem(id) {
    if (this.sourceItems.length > 1) {
      const index = this.generalService.getIndex(this.purchasingInvoiceForm.value.purchasingDetails, id, 'requestId');
      this.getPurchasingDetailsArray.removeAt(index);
      this.sourceItems = this.sourceItems.filter(d => d._id !== id);
    }
  }

  calculateTaxValue(group: FormGroup) {
    const value = group.value.value ? group.value.value : 0;
    const taxRate = group.value.taxRate ? group.value.taxRate : 0;

    group.patchValue({
      taxValue: +(((value * taxRate) / 100).toFixed(4))
    });
    this.calculateAdditionalCostNetValue(group);
  }

  calculateAdditionalCostNetValue(group: FormGroup) {
    const value = group.value.value ? group.value.value : 0;
    const taxValue = group.value.taxValue ? group.value.taxValue : 0;

    group.patchValue({
      netValue: +((value * taxValue).toFixed(4))
    });
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.purchasingInvoiceForm.value.gregorianDate !== 'string') {
      this.purchasingInvoiceForm.value.gregorianDate =
        this.generalService.format(this.purchasingInvoiceForm.value.gregorianDate);
    }
    if (typeof this.purchasingInvoiceForm.value.hijriDate !== 'string') {
      this.purchasingInvoiceForm.value.hijriDate =
        this.generalService.formatHijriDate(this.purchasingInvoiceForm.value.hijriDate);
    }


    for (
      let i = 0;
      i <= this.purchasingInvoiceForm.value.payments.length - 1;
      i++
    ) {

      {
        if (this.purchasingInvoiceForm.value.payments[i].dueDateGregorian) {
          if (
            typeof this.purchasingInvoiceForm.value.payments[i].dueDateGregorian !==
            'string'
          ) {
            this.purchasingInvoiceForm.value.payments[i].dueDateGregorian = this.generalService.format(
              this.purchasingInvoiceForm.value.payments[i].dueDateGregorian
            );
          }
          if (typeof this.purchasingInvoiceForm.value.payments[i].dueDateHijri !==
            'string') {
            this.purchasingInvoiceForm.value.payments[i].dueDateHijri = this.generalService.formatHijriDate(
              this.purchasingInvoiceForm.value.payments[i].dueDateHijri
            );
          }
        }
        if (this.purchasingInvoiceForm.value.payments[i].documentDateGregorian) {
          if (
            typeof this.purchasingInvoiceForm.value.payments[i].documentDateGregorian !==
            'string'
          ) {
            this.purchasingInvoiceForm.value.payments[i].documentDateGregorian = this.generalService.format(
              this.purchasingInvoiceForm.value.payments[i].documentDateGregorian
            );
          }
          if (typeof this.purchasingInvoiceForm.value.payments[i].documentDateHijri !==
            'string') {
            this.purchasingInvoiceForm.value.payments[i].documentDateHijri = this.generalService.formatHijriDate(
              this.purchasingInvoiceForm.value.payments[i].documentDateHijri
            );
          }
        }
      }

    }

    if (this.purchasingInvoice) {
      if (this.purchasingInvoiceForm.valid) {
        this.purchasingInvoiceForm.value.purchasingDetails.splice(0, 1);
        const newpurchasingInvoice = {
          _id: this.purchasingInvoice._id,
          ...this.generalService.checkEmptyFields(this.purchasingInvoiceForm.value)
        };
        this.data.put(purchasingInvoiceApi, newpurchasingInvoice).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/purchases/purchasingInvoice']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.purchasingInvoiceForm.valid) {
        this.purchasingInvoiceForm.value.purchasingDetails.splice(0, 1);
        const formValue = {
          ...this.generalService.checkEmptyFields(this.purchasingInvoiceForm.value)
        };
        this.subscriptions.push(
          this.data.post(purchasingInvoiceApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.purchasingInvoiceForm.reset();
            this.purchasingInvoiceForm.patchValue({
              isActive: true,
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let purchasingInvoicePeriodic = false;
    let purchasingInvoiceStatus = 'unposted';
    let purchasingInvoiceDescriptionAr = '';
    let purchasingInvoiceDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let deliveringDateGregorian = null;
    let deliveringDateHijri = null;
    let supplierId = '';
    let supplierBranchId = '';
    let referenceNumber = '';
    let currencyId = '';
    let currencyExchangeRate = 0;
    let discountTaxType = 'without';
    let discountTaxRate = 0;
    let targetType = 'warehouse';
    let warehouseId = '';
    let projectId = '';
    let sourceType = 'without';
    let discountItems = 0;
    let totalDiscount = 0;
    let totalCost = 0;
    let totalDiscountTax = 0;
    let totalVat = 0;
    let additionalDiscount = 0;
    let totalBalance = 0;
    let totalNet = 0;
    let variablesArray = new FormArray([]);
    let purchasingDetailsArray = new FormArray([]);
    let paymentsArray = new FormArray([
      new FormGroup({
        paymentMethod: new FormControl('CASH', Validators.required),
        accountId: new FormControl('', Validators.required),
        description: new FormControl(''),
        value: new FormControl(0, Validators.required),
        balance: new FormControl(0, Validators.required),
        dueDateGregorian: new FormControl(null),
        dueDateHijri: new FormControl(null),
        documentNumber: new FormControl(0),
        documentDateGregorian: new FormControl(null),
        documentDateHijri: new FormControl(null),
        supplierId: new FormControl('', Validators.required)
      })
    ]);
    let additionalCostsArray = new FormArray([
      new FormGroup({
        itemId: new FormControl(''),
        value: new FormControl(0),
        taxRate: new FormControl(0),
        taxValue: new FormControl(0),
        netValue: new FormControl(0),
        supplierId: new FormControl(''),
        paymentMethod: new FormControl('CASH'),
        accountId: new FormControl(''),
        currencyId: new FormControl(''),
        currencyExchangeRate: new FormControl(null),
        serviceInvoiceNumber: new FormControl(0)
      })
    ]);
    let isActive = true;

    if (this.purchasingInvoice) {
      code = this.purchasingInvoice.code;
      purchasingInvoicePeriodic = this.purchasingInvoice.purchasingInvoicePeriodic;
      purchasingInvoiceStatus = this.purchasingInvoice.purchasingInvoiceStatus;
      purchasingInvoiceDescriptionAr = this.purchasingInvoice.purchasingInvoiceDescriptionAr;
      purchasingInvoiceDescriptionEn = this.purchasingInvoice.purchasingInvoiceDescriptionEn;
      gregorianDate = new Date(this.purchasingInvoice.gregorianDate + 'UTC');
      hijriDate = this.purchasingInvoice.hijriDate;
      deliveringDateGregorian = new Date(this.purchasingInvoice.deliveringDateGregorian + 'UTC');
      deliveringDateHijri = this.purchasingInvoice.deliveringDateHijri;
      supplierId = this.purchasingInvoice.supplierId;
      supplierBranchId = this.purchasingInvoice.supplierBranchId;
      referenceNumber = this.purchasingInvoice.referenceNumber;
      currencyId = this.purchasingInvoice.currencyId;
      currencyExchangeRate = this.purchasingInvoice.currencyExchangeRate;
      discountTaxType = this.purchasingInvoice.discountTaxType;
      discountTaxRate = this.purchasingInvoice.discountTaxRate;
      targetType = this.purchasingInvoice.targetType;
      warehouseId = this.purchasingInvoice.warehouseId;
      projectId = this.purchasingInvoice.projectId;
      sourceType = this.purchasingInvoice.sourceType;
      isActive = this.purchasingInvoice.isActive;
      discountItems = this.purchasingInvoice.discountItems;
      totalDiscount = this.purchasingInvoice.totalDiscount;
      totalCost = this.purchasingInvoice.totalCost;
      totalDiscountTax = this.purchasingInvoice.totalDiscountTax;
      totalVat = this.purchasingInvoice.totalVat;
      additionalDiscount = this.purchasingInvoice.additionalDiscount;
      totalBalance = this.purchasingInvoice.totalBalance;
      totalNet = this.purchasingInvoice.totalNet;
      purchasingDetailsArray = new FormArray([]);
      for (const control of this.purchasingInvoice.purchasingDetails) {
        variablesArray = new FormArray([]);
        purchasingDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(''),
            itemType: new FormControl('', Validators.required),
            barCode: new FormControl('', Validators.required),
            itemId: new FormControl('', Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(1, Validators.required),
            freeQuantity: new FormControl(0),
            itemUnitId: new FormControl(''),
            cost: new FormControl(0, Validators.required),
            discountPercentage: new FormControl(0),
            discountValue: new FormControl(0),
            additionalDiscount: new FormControl(0),
            totalDiscount: new FormControl(0),
            totalCost: new FormControl(0),
            vatPercentage: new FormControl(0),
            vatValue: new FormControl(0),
            netValue: new FormControl(0, Validators.required),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
          })
        );
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        purchasingDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(control.requestId),
            itemType: new FormControl(control.itemType, Validators.required),
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            freeQuantity: new FormControl(control.freeQuantity),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(control.cost, Validators.required),
            discountPercentage: new FormControl(control.discountPercentage),
            discountValue: new FormControl(control.discountValue),
            additionalDiscount: new FormControl(control.additionalDiscount),
            totalDiscount: new FormControl(control.totalDiscount),
            totalCost: new FormControl(control.totalCost),
            vatPercentage: new FormControl(control.vatPercentage),
            vatValue: new FormControl(control.vatValue),
            netValue: new FormControl(control.netValue, Validators.required),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
          })
        );
      }

      paymentsArray = new FormArray([]);
      for (const control of this.purchasingInvoice.payments) {
        paymentsArray.push(
          new FormGroup({
            paymentMethod: new FormControl(control.paymentMethod, Validators.required),
            accountId: new FormControl(control.accountId, Validators.required),
            description: new FormControl(control.description),
            value: new FormControl(control.value, Validators.required),
            balance: new FormControl(control.balance, Validators.required),
            dueDateGregorian: new FormControl(new Date(control.dueDateGregorian + 'UTC')),
            dueDateHijri: new FormControl(control.dueDateHijri),
            documentNumber: new FormControl(control.documentNumber),
            documentDateGregorian: new FormControl(new Date(control.documentDateGregorian + 'UTC')),
            documentDateHijri: new FormControl(control.documentDateHijri),
            supplierId: new FormControl(control.supplierId, Validators.required),
          })
        );
      }

      if (this.purchasingInvoice.additionalCosts.length) {
        additionalCostsArray = new FormArray([]);
        for (const control of this.purchasingInvoice.additionalCosts) {
          additionalCostsArray.push(
            new FormGroup({
              itemId: new FormControl(control.itemId),
              value: new FormControl(control.value),
              taxRate: new FormControl(control.taxRate),
              taxValue: new FormControl(control.taxValue),
              netValue: new FormControl(control.netValue),
              supplierId: new FormControl(control.supplierId),
              paymentMethod: new FormControl(control.paymentMethod),
              accountId: new FormControl(control.accountId),
              currencyId: new FormControl(control.currencyId),
              currencyExchangeRate: new FormControl(control.currencyExchangeRate),
              serviceInvoiceNumber: new FormControl(control.serviceInvoiceNumber)
            })
          );
        }
      }
    }
    this.purchasingInvoiceForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      purchasingInvoicePeriodic: new FormControl(purchasingInvoicePeriodic),
      purchasingInvoiceStatus: new FormControl(purchasingInvoiceStatus),
      purchasingInvoiceDescriptionAr: new FormControl(purchasingInvoiceDescriptionAr),
      purchasingInvoiceDescriptionEn: new FormControl(purchasingInvoiceDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      deliveringDateGregorian: new FormControl(deliveringDateGregorian),
      deliveringDateHijri: new FormControl(deliveringDateHijri),
      supplierId: new FormControl(supplierId, Validators.required),
      supplierBranchId: new FormControl(supplierBranchId),
      referenceNumber: new FormControl(referenceNumber),
      currencyId: new FormControl(currencyId, Validators.required),
      currencyExchangeRate: new FormControl(currencyExchangeRate),
      targetType: new FormControl(targetType, Validators.required),
      warehouseId: new FormControl(warehouseId, Validators.required),
      projectId: new FormControl(projectId, Validators.required),
      sourceType: new FormControl(sourceType, Validators.required),
      discountTaxType: new FormControl(discountTaxType, Validators.required),
      discountTaxRate: new FormControl(discountTaxRate, Validators.required),
      discountItems: new FormControl(discountItems, Validators.required),
      totalDiscount: new FormControl(totalDiscount, Validators.required),
      totalCost: new FormControl(totalCost, Validators.required),
      totalDiscountTax: new FormControl(totalDiscountTax, Validators.required),
      totalVat: new FormControl(totalVat, Validators.required),
      additionalDiscount: new FormControl(additionalDiscount, Validators.required),
      totalBalance: new FormControl(totalBalance, Validators.required),
      totalNet: new FormControl(totalNet, Validators.required),
      purchasingDetails: purchasingDetailsArray,
      payments: paymentsArray,
      additionalCosts: additionalCostsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
