import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingInvoiceComponent } from './purchasing-invoice.component';

describe('PurchasingInvoiceComponent', () => {
  let component: PurchasingInvoiceComponent;
  let fixture: ComponentFixture<PurchasingInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
