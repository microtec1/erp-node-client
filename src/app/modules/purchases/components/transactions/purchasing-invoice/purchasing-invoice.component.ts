import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPurchasingInvoice } from '../../../interfaces/IPurchasingInvoice';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, purchasingInvoiceApi, postPurchasingInvoiceApi, unPostPurchasingInvoiceApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-purchasing-invoice',
  templateUrl: './purchasing-invoice.component.html',
  styleUrls: ['./purchasing-invoice.component.scss']
})
export class PurchasingInvoiceComponent implements OnInit, OnDestroy {
  purchasingInvoice: IPurchasingInvoice[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getPurchasingInvoiceFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(purchasingInvoiceApi, pageNumber).subscribe((res: IDataRes) => {
      this.purchasingInvoice = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(purchasingInvoiceApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.purchasingInvoice = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postPurchasingInvoiceApi}/${id}`, {}).subscribe(res => {
        this.purchasingInvoice = this.generalService.changeStatus(this.purchasingInvoice, id, 'posted', 'purchasingInvoiceStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostPurchasingInvoiceApi}/${id}`, {}).subscribe(res => {
        this.purchasingInvoice = this.generalService.changeStatus(this.purchasingInvoice, id, 'unposted', 'purchasingInvoiceStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getPurchasingInvoiceFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(purchasingInvoiceApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.purchasingInvoice = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      purchasingInvoiceDescriptionAr: new FormControl(''),
      purchasingInvoiceDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IPurchasingInvoice) {
    const initialState = {
      code: item.code,
      nameAr: item.purchasingInvoiceDescriptionAr,
      nameEn: item.purchasingInvoiceDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(purchasingInvoiceApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.purchasingInvoice = this.generalService.removeItem(this.purchasingInvoice, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getPurchasingInvoiceFirstPage() {
    this.subscriptions.push(
      this.data.get(purchasingInvoiceApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.purchasingInvoice = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
