import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPurchasingRequestComponent } from './add-purchasing-request.component';

describe('AddPurchasingRequestComponent', () => {
  let component: AddPurchasingRequestComponent;
  let fixture: ComponentFixture<AddPurchasingRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPurchasingRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPurchasingRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
