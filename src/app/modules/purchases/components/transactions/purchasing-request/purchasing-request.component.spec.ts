import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingRequestComponent } from './purchasing-request.component';

describe('PurchasingRequestComponent', () => {
  let component: PurchasingRequestComponent;
  let fixture: ComponentFixture<PurchasingRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
