import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPurchasingReturnsComponent } from './add-purchasing-returns.component';

describe('AddPurchasingReturnsComponent', () => {
  let component: AddPurchasingReturnsComponent;
  let fixture: ComponentFixture<AddPurchasingReturnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPurchasingReturnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPurchasingReturnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
