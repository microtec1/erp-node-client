import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasingReturnsComponent } from './purchasing-returns.component';

describe('PurchasingReturnsComponent', () => {
  let component: PurchasingReturnsComponent;
  let fixture: ComponentFixture<PurchasingReturnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasingReturnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasingReturnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
