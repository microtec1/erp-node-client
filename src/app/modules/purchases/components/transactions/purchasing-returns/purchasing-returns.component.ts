import { Component, OnInit, OnDestroy } from '@angular/core';
import { IPurchasingReturns } from '../../../interfaces/IPurchasingReturns';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, purchasingReturnsApi, postPurchasingReturnsApi, unPostPurchasingReturnsApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-purchasing-returns',
  templateUrl: './purchasing-returns.component.html',
  styleUrls: ['./purchasing-returns.component.scss']
})
export class PurchasingReturnsComponent implements OnInit, OnDestroy {
  purchasingReturns: IPurchasingReturns[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getPurchasingReturnsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(purchasingReturnsApi, pageNumber).subscribe((res: IDataRes) => {
      this.purchasingReturns = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(purchasingReturnsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.purchasingReturns = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postPurchasingReturnsApi}/${id}`, {}).subscribe(res => {
        this.purchasingReturns = this.generalService.changeStatus(this.purchasingReturns, id, 'posted', 'purchasingReturnsStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostPurchasingReturnsApi}/${id}`, {}).subscribe(res => {
        this.purchasingReturns = this.generalService.changeStatus(this.purchasingReturns, id, 'unposted', 'purchasingReturnsStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getPurchasingReturnsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(purchasingReturnsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.purchasingReturns = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      purchasingReturnsDescriptionAr: new FormControl(''),
      purchasingReturnsDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: IPurchasingReturns) {
    const initialState = {
      code: item.code,
      nameAr: item.purchasingReturnsDescriptionAr,
      nameEn: item.purchasingReturnsDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(purchasingReturnsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.purchasingReturns = this.generalService.removeItem(this.purchasingReturns, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getPurchasingReturnsFirstPage() {
    this.subscriptions.push(
      this.data.get(purchasingReturnsApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.purchasingReturns = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
