import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStockControlComponent } from './add-stock-control.component';

describe('AddStockControlComponent', () => {
  let component: AddStockControlComponent;
  let fixture: ComponentFixture<AddStockControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddStockControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStockControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
