import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { IPurchasingStockControl } from 'src/app/modules/purchases/interfaces/IPurchasingStockControl';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  purchasingStockControlApi,
  warehousesApi,
  projectsApi,
  suppliersApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi,
  checkReasonsRejectionApi,
  purchasingRequestApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { ISupplier } from 'src/app/modules/purchases/interfaces/ISupplier';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IProject } from 'src/app/modules/purchases/interfaces/IProject.model';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { ICheckReasonRejection } from 'src/app/modules/accounting/modules/cheques/interfaces/ICheckReasonRejection';
import { IPurchasingRequest } from 'src/app/modules/purchases/interfaces/IPurchasingRequest';

@Component({
  selector: 'app-add-stock-control',
  templateUrl: './add-stock-control.component.html',
  styleUrls: ['./add-stock-control.component.scss']
})
export class AddStockControlComponent implements OnInit, OnDestroy {
  purchasingStockControlForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  purchasingStockControl: IPurchasingStockControl;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Suppliers
  suppliers: ISupplier[] = [];
  suppliersInputFocused: boolean;
  hasMoreSuppliers: boolean;
  suppliersCount: number;
  selectedSuppliersPage = 1;
  suppliersPagesNo: number;
  noSuppliers: boolean;

  // Supplier Branches
  supplierBranches: any[] = [];
  supplierBranchesInputFocused: boolean;
  supplierBranchesCount: number;
  noSupplierBranches: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Projects
  projects: IProject[] = [];
  projectsInputFocused: boolean;
  hasMoreProjects: boolean;
  projectsCount: number;
  selectedProjectsPage = 1;
  projectsPagesNo: number;
  noProjects: boolean;

  // Rejection Reasons
  rejectionReasons: ICheckReasonRejection[] = [];
  rejectionReasonsInputFocused: boolean;
  hasMoreRejectionReasons: boolean;
  rejectionReasonsCount: number;
  selectedRejectionReasonsPage = 1;
  rejectionReasonsPagesNo: number;
  noRejectionReasons: boolean;

  // Source Items
  sourceItems: IPurchasingRequest[] = [];

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(purchasingStockControlApi, null, null, id).subscribe((purchasingStockControl: IPurchasingStockControl) => {
              this.purchasingStockControl = purchasingStockControl;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.purchasingStockControlForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.loadSourceItems();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(warehousesApi, 1).subscribe((res: IDataRes) => {
        this.warehousesPagesNo = res.pages;
        this.warehousesCount = res.count;
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        }
        this.warehouses.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(projectsApi, 1).subscribe((res: IDataRes) => {
        this.projectsPagesNo = res.pages;
        this.projectsCount = res.count;
        if (this.projectsPagesNo > this.selectedProjectsPage) {
          this.hasMoreProjects = true;
        }
        this.projects.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(suppliersApi, 1).subscribe((res: IDataRes) => {
        this.suppliersPagesNo = res.pages;
        this.suppliersCount = res.count;
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        }
        this.suppliers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(checkReasonsRejectionApi, 1).subscribe((res: IDataRes) => {
        this.rejectionReasonsPagesNo = res.pages;
        this.rejectionReasonsCount = res.count;
        if (this.rejectionReasonsPagesNo > this.selectedRejectionReasonsPage) {
          this.hasMoreRejectionReasons = true;
        }
        this.rejectionReasons.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  fillBranches(value) {
    const supplier = this.suppliers.find(x => x._id === value);
    this.supplierBranches = supplier.branches;
  }

  searchRejectionReasons(event) {
    const searchValue = event;
    const searchQuery = {
      checkReasonsRejectionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(checkReasonsRejectionApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noRejectionReasons = true;
            } else {
              this.noRejectionReasons = false;
              for (const item of res.results) {
                if (this.rejectionReasons.length) {
                  const uniqueRejectionReasons = this.rejectionReasons.filter(
                    x => x._id !== item._id
                  );
                  this.rejectionReasons = uniqueRejectionReasons;
                }
                this.rejectionReasons.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreRejectionReasons() {
    this.selectedRejectionReasonsPage = this.selectedRejectionReasonsPage + 1;
    this.subscriptions.push(
      this.data
        .get(checkReasonsRejectionApi, this.selectedRejectionReasonsPage)
        .subscribe((res: IDataRes) => {
          if (this.rejectionReasonsPagesNo > this.selectedRejectionReasonsPage) {
            this.hasMoreRejectionReasons = true;
          } else {
            this.hasMoreRejectionReasons = false;
          }
          for (const item of res.results) {
            if (this.rejectionReasons.length) {
              const uniquerejectionReasons = this.rejectionReasons.filter(x => x._id !== item._id);
              this.rejectionReasons = uniquerejectionReasons;
            }
            this.rejectionReasons.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSuppliers(event) {
    const searchValue = event;
    const searchQuery = {
      supplierNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(suppliersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSuppliers = true;
          } else {
            this.noSuppliers = false;
            for (const item of res.results) {
              if (this.suppliers.length) {
                const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
                this.suppliers = uniqueSuppliers;
              }
              this.suppliers.push(item);
            }
          }
          this.suppliers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(warehousesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noWarehouses = true;
          } else {
            this.noWarehouses = false;
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data.get(warehousesApi, this.selectedWarehousesPage).subscribe((res: IDataRes) => {
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        } else {
          this.hasMoreWarehouses = false;
        }
        for (const item of res.results) {
          if (this.warehouses.length) {
            const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
            this.warehouses = uniqueWarehouses;
          }
          this.warehouses.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchProjects(event) {
    const searchValue = event;
    const searchQuery = {
      projectNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noProjects = true;
            } else {
              this.noProjects = false;
              for (const item of res.results) {
                if (this.projects.length) {
                  const uniqueprojects = this.projects.filter(
                    x => x._id !== item._id
                  );
                  this.projects = uniqueprojects;
                }
                this.projects.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreProjects() {
    this.selectedProjectsPage = this.selectedProjectsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedProjectsPage)
        .subscribe((res: IDataRes) => {
          if (this.projectsPagesNo > this.selectedProjectsPage) {
            this.hasMoreProjects = true;
          } else {
            this.hasMoreProjects = false;
          }
          for (const item of res.results) {
            if (this.projects.length) {
              const uniqueProjects = this.projects.filter(
                x => x._id !== item._id
              );
              this.projects = uniqueProjects;
            }
            this.projects.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  loadMoreSuppliers() {
    this.selectedSuppliersPage = this.selectedSuppliersPage + 1;
    this.subscriptions.push(
      this.data.get(suppliersApi, this.selectedSuppliersPage).subscribe((res: IDataRes) => {
        if (this.suppliersPagesNo > this.selectedSuppliersPage) {
          this.hasMoreSuppliers = true;
        } else {
          this.hasMoreSuppliers = false;
        }
        for (const item of res.results) {
          if (this.suppliers.length) {
            const uniqueSuppliers = this.suppliers.filter(x => x._id !== item._id);
            this.suppliers = uniqueSuppliers;
          }
          this.suppliers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }


  filterItems(formValue) {
    console.log(formValue);
  }


  fillRow(item, formGroup) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];

    for (const x of item.variables) {
      variablesControl.push(
        new FormGroup({
          variableId: new FormControl(x.variableId),
          itemVariableNameId: new FormControl(x.itemVariableNameId)
        })
      );
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getPurchasingDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        if (value) {
          variables.push(
            { name: variable.itemsVariableNameAr, value: value.name }
          );
        }
      }
    }
    return variables;
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.purchasingStockControlForm.controls;
  }

  get getPurchasingDetailsArray() {
    return this.purchasingStockControlForm.get('purchasingDetails') as FormArray;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.purchasingStockControlForm.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.purchasingStockControlForm.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  loadSourceItems() {
    const searchValues = {
      purchasingRequestStatus: 'posted'
    };
    this.subscriptions.push(
      this.data.get(purchasingRequestApi, null, searchValues).subscribe((res: IDataRes) => {
        this.sourceItems = res.results;
        for (const item of this.sourceItems) {
          for (const [index, control] of item.purchasingDetails.entries()) {
            this.getPurchasingDetailsArray.push(
              new FormGroup({
                requestId: new FormControl(item._id, Validators.required),
                itemType: new FormControl(control.itemType, Validators.required),
                barCode: new FormControl(control.barCode, Validators.required),
                itemId: new FormControl(control.itemId, Validators.required),
                variables: new FormArray([]),
                quantity: new FormControl(control.quantity, Validators.required),
                itemUnitId: new FormControl(control.itemUnitId),
                warehouseBalance: new FormControl(0),
                approvedQuantity: new FormControl(0),
                balanceAfterApproved: new FormControl(0),
                unApprovedQuantity: new FormControl(0),
                descriptionAr: new FormControl(control.descriptionAr),
                descriptionEn: new FormControl(control.descriptionEn),
                rejectionReason: new FormControl('', Validators.required)
              })
            );
            this.fillRow(control, (this.getPurchasingDetailsArray.controls[index] as FormGroup));
          }
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  deleteSourceItem(id) {
    if (this.sourceItems.length > 1) {
      const index = this.generalService.getIndex(this.purchasingStockControlForm.value.purchasingDetails, id, 'requestId');
      this.getPurchasingDetailsArray.removeAt(index);
      this.sourceItems = this.sourceItems.filter(d => d._id !== id);
    }
  }

  calculatebalanceAfterApproved(item: FormGroup) {
    item.patchValue({
      balanceAfterApproved: item.value.warehouseBalance + item.value.approvedQuantity,
      unApprovedQuantity: item.value.quantity - item.value.approvedQuantity
    });
  }

  validateTarget(value) {
    if (value === 'warehosue') {
      this.purchasingStockControlForm.controls.warehouseId.setValidators(Validators.required);
      this.purchasingStockControlForm.controls.warehouseId.updateValueAndValidity();
      this.purchasingStockControlForm.controls.projectId.clearValidators();
      this.purchasingStockControlForm.controls.projectId.updateValueAndValidity();
    } else {
      this.purchasingStockControlForm.controls.projectId.setValidators(Validators.required);
      this.purchasingStockControlForm.controls.projectId.updateValueAndValidity();
      this.purchasingStockControlForm.controls.warehouseId.clearValidators();
      this.purchasingStockControlForm.controls.projectId.updateValueAndValidity();
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;

    if (typeof this.purchasingStockControlForm.value.gregorianDate !== 'string') {
      this.purchasingStockControlForm.value.gregorianDate =
        this.generalService.format(this.purchasingStockControlForm.value.gregorianDate);
    }
    if (typeof this.purchasingStockControlForm.value.hijriDate !== 'string') {
      this.purchasingStockControlForm.value.hijriDate =
        this.generalService.formatHijriDate(this.purchasingStockControlForm.value.hijriDate);
    }


    if (this.purchasingStockControlForm.value.targetType === 'warehouse') {
      delete this.purchasingStockControlForm.value.projectId;
    } else {
      delete this.purchasingStockControlForm.value.warehouseId;
    }


    if (this.purchasingStockControl) {
      if (this.purchasingStockControlForm.valid) {
        const newpurchasingStockControl = {
          _id: this.purchasingStockControl._id,
          ...this.generalService.checkEmptyFields(this.purchasingStockControlForm.value)
        };
        this.data.put(purchasingStockControlApi, newpurchasingStockControl).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/purchases/purchasingStockControl']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.purchasingStockControlForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.purchasingStockControlForm.value)
        };
        this.subscriptions.push(
          this.data.post(purchasingStockControlApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.purchasingStockControlForm.reset();
            this.purchasingStockControlForm.patchValue({
              isActive: true,
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let purchasingStockControlStatus = 'unposted';
    let purchasingStockControlDescriptionAr = '';
    let purchasingStockControlDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let supplierId = '';
    let supplierBranchId = '';
    let referenceNumber = '';
    let targetType = 'warehouse';
    let warehouseId = '';
    let projectId = '';
    let sourceType = 'purchaseRequest';
    let rejectionReason = '';
    let variablesArray = new FormArray([]);
    let purchasingDetailsArray = new FormArray([]);
    let isActive = true;

    if (this.purchasingStockControl) {
      code = this.purchasingStockControl.code;
      purchasingStockControlStatus = this.purchasingStockControl.purchasingStockControlStatus;
      purchasingStockControlDescriptionAr = this.purchasingStockControl.purchasingStockControlDescriptionAr;
      purchasingStockControlDescriptionEn = this.purchasingStockControl.purchasingStockControlDescriptionEn;
      gregorianDate = new Date(this.purchasingStockControl.gregorianDate + 'UTC');
      hijriDate = this.purchasingStockControl.hijriDate;
      supplierId = this.purchasingStockControl.supplierId;
      supplierBranchId = this.purchasingStockControl.supplierBranchId;
      referenceNumber = this.purchasingStockControl.referenceNumber;
      targetType = this.purchasingStockControl.targetType;
      warehouseId = this.purchasingStockControl.warehouseId;
      projectId = this.purchasingStockControl.projectId;
      sourceType = this.purchasingStockControl.sourceType;
      rejectionReason = this.purchasingStockControl.rejectionReason;
      isActive = this.purchasingStockControl.isActive;
      purchasingDetailsArray = new FormArray([]);
      for (const control of this.purchasingStockControl.purchasingDetails) {
        variablesArray = new FormArray([]);
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        purchasingDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(control.requestId, Validators.required),
            itemType: new FormControl(control.itemType, Validators.required),
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            itemUnitId: new FormControl(control.itemUnitId),
            warehouseBalance: new FormControl(control.warehouseBalance),
            approvedQuantity: new FormControl(control.approvedQuantity),
            balanceAfterApproved: new FormControl(control.balanceAfterApproved),
            unApprovedQuantity: new FormControl(control.unApprovedQuantity),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
            rejectionReason: new FormControl(control.rejectionReason, Validators.required),
          })
        );
      }
    }
    this.purchasingStockControlForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      purchasingStockControlStatus: new FormControl(purchasingStockControlStatus),
      purchasingStockControlDescriptionAr: new FormControl(purchasingStockControlDescriptionAr),
      purchasingStockControlDescriptionEn: new FormControl(purchasingStockControlDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      supplierId: new FormControl(supplierId),
      supplierBranchId: new FormControl(supplierBranchId),
      referenceNumber: new FormControl(referenceNumber),
      targetType: new FormControl(targetType),
      warehouseId: new FormControl(warehouseId, Validators.required),
      projectId: new FormControl(projectId),
      sourceType: new FormControl(sourceType),
      rejectionReason: new FormControl(rejectionReason, Validators.required),
      purchasingDetails: purchasingDetailsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
