export interface IInventoryRejectionReason {
  _id: string;
  image: string;
  code: string;
  inventoryControlRejectionReasonNameAr: string;
  inventoryControlRejectionReasonNameEn?: string;
  isActive: boolean;
}
