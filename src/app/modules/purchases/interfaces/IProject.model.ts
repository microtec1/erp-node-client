export interface IProject {
  _id: string;
  image: string;
  code: string;
  projectNameAr: string;
  projectNameEn: string;
  glCostAccountId: string;
  costCenterId: string;
  glRevenueAccountId: string;
  projectOwnerId: string;
  isActive: boolean;
}
