export interface IPurchaseQuotation {
  _id: string;
  code: string;
  purchaseQuotationPeriodic: boolean;
  purchaseQuotationStatus: string;
  purchaseQuotationDescriptionAr: string;
  purchaseQuotationDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  supplierId: string;
  supplierBranchId: string;
  referenceNumber: string;
  sourceType: string;
  currencyId: string;
  currencyExchangeRate: number;
  offerValidity: number;
  expiryDateGregorian: string;
  expiryDateHijri: string;
  discountTaxType: string;
  discountTaxRate: number;
  isActive: boolean;
  purchasingDetails: [
    {
      requestId: string,
      itemType: string,
      barCode: string,
      itemId: string,
      variables: [
        {
          variableId: string,
          itemVariableNameId: string
        }
      ],
      quantity: number,
      quantityBalance: number,
      itemUnitId: string,
      cost: number;
      discountPercentage: number;
      discountValue: number;
      totalCost: number;
      vatPercentage: number;
      vatValue: number;
      netValue: number;
      descriptionAr: string,
      descriptionEn: string,
      countryId: string
    }
  ];
  totalDiscount: number;
  totalCost: number;
  totalDiscountTax: number;
  totalVat: number;
  totalNet: number;
}
