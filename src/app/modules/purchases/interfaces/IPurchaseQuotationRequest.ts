export interface IPurchaseQuotationRequest {
  _id: string;
  code: string;
  purchaseQuotationRequestStatus: string;
  purchaseQuotationRequestDescriptionAr: string;
  purchaseQuotationRequestDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  supplierId: string;
  supplierBranchId: string;
  referenceNumber: string;
  sourceType: string;
  deliveryOptions: string;
  isActive: boolean;
  purchasingDetails: [
    {
      requestId: string,
      itemType: string,
      barCode: string,
      itemId: string,
      variables: [
        {
          variableId: string,
          itemVariableNameId: string
        }
      ],
      quantity: number,
      quantityBalance: number,
      itemUnitId: string,
      descriptionAr: string,
      descriptionEn: string,
      countryId: string
    }
  ];
}
