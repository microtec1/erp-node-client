export interface IPurchasesSettings {
  company: {};
  branch: {
    branchId: string;
    directInfluenceOnTheInventory: boolean;
    directInfluenceOnTheInventoryOptions: string;
    displayFreeQuantitiesInPurchasesAndInventoryReceiving: boolean;
    defaultPaymentsType: string;
    defaultPaymentsAccount: string;
    loadLastPurchasingPrice: boolean;
    loadLastPurchasingPriceOptions: string;
    cannotExceedMaximumLimitPurchasesOnThePurchaseInvoice: boolean;
    useAddedCostInPurchases: boolean;
    purchasingCycleTransactions: {
      purchaseRequest: boolean;
      inventoryControl: boolean;
      purchaseOfferRequest: boolean;
      purchaseOffer: boolean;
      comparePrices: boolean;
      purchaseOrder: boolean;
      purchaseInvoice: boolean;
      purchaseReturns: boolean;
    }
  };
}
