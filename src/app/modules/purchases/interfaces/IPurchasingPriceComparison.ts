export interface IPurchasingPriceComparison {
  _id: string;
  code: string;
  purchasingPriceComparisonStatus: string;
  purchasingPriceComparisonDescriptionAr: string;
  purchasingPriceComparisonDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  currencyId: string;
  exchangeRate: number;
  referenceNumber: string;
  sourceType: string;
  sourceNumberId: string;
  isActive: boolean;
  purchasingDetails: [
    {
      requestId: string,
      itemType: string,
      barCode: string,
      itemId: string,
      variables: [
        {
          variableId: string,
          itemVariableNameId: string
        }
      ],
      quantity: number,
      quantityBalance: number,
      itemUnitId: string,
      cost: number;
      discountPercentage: number;
      discountValue: number;
      totalCost: number;
      vatPercentage: number;
      vatValue: number;
      netValue: number;
      descriptionAr: string,
      descriptionEn: string,
      supplierId: string
      countryId: string
      bestOffer: boolean
    }
  ];
  totalDiscount: number;
  totalCost: number;
  totalDiscountTax: number;
  totalVat: number;
  totalNet: number;
}
