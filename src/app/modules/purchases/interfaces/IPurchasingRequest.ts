export interface IPurchasingRequest {
  _id: string;
  code: string;
  purchasingRequestPeriodic: boolean;
  purchasingRequestStatus: string;
  purchasingRequestDescriptionAr: string;
  purchasingRequestDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  supplierId: string;
  supplierBranchId: string;
  referenceNumber: string;
  targetType: string;
  warehouseId: string;
  projectId: string;
  deliveryOptions: string;
  isActive: boolean;
  purchasingDetails: [
    {
      itemType: string,
      barCode: string,
      itemId: string,
      variables: [
        {
          variableId: string,
          itemVariableNameId: string
        }
      ],
      quantity: number,
      itemUnitId: string,
      descriptionAr: string,
      descriptionEn: string,
      countryId: string
    }
  ];
}
