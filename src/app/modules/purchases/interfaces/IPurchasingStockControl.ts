export interface IPurchasingStockControl {
  _id: string;
  code: string;
  purchasingStockControlPeriodic: boolean;
  purchasingStockControlStatus: string;
  purchasingStockControlDescriptionAr: string;
  purchasingStockControlDescriptionEn: string;
  gregorianDate: string;
  hijriDate: string;
  supplierId: string;
  supplierBranchId: string;
  referenceNumber: string;
  targetType: string;
  warehouseId: string;
  projectId: string;
  sourceType: string;
  rejectionReason: string;
  isActive: boolean;
  purchasingDetails: [
    {
      requestId: string,
      itemType: string,
      barCode: string,
      itemId: string,
      variables: [
        {
          variableId: string,
          itemVariableNameId: string
        }
      ],
      quantity: number,
      quantityBalance: number,
      itemUnitId: string,
      warehouseBalance: number,
      approvedQuantity: number,
      balanceAfterApproved: number,
      unApprovedQuantity: number,
      descriptionAr: string,
      descriptionEn: string,
      rejectionReason: string
    }
  ];
}
