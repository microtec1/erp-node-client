export interface ISupplier {
  _id: string;
  image: string;
  code: string;
  supplierNameAr: string;
  supplierNameEn: string;
  suppliersGroupId: string;
  currencyId: string;
  exchangeRate: number;
  suppliersClassificationId: string;
  identity: {
    nationalityId: string;
    identityNumber: string;
    identitySource: string;
    releaseGregorianDate: string;
    releaseHijriDate: string;
  };
  officialDocuments: {
    tax: {
      taxRegistrationNumber: string;
      taxCardNumber: string;
      missionName: string;
      businessAddress: string;
      fileNumber: string;
      businessName: string;
      financierName: string;
    };
    commercialRegistration: {
      commercialName: string;
      commercialTrait: string;
      commercialBrand: string;
      traderName: string;
      nationalityId: string;
      financialSystem: string;
      commercialEligibility: string;
      tradeType: string;
      startBusinessGregorianDate: string;
      startBusinessHijriDate: string;
    };
  };
  openingBalances: [
    {
      balance: string;
      netBalance: string;
      currencyId: string;
      exchangeRate: number;
      accountSide: string;
      dueGregorianDate: string;
      dueHijriDate: string;
      referenceNumber: string;
      notes: string;
    }
  ];
  accountingInfo: {
    purchasesDuePeriod: string;
    glAccountId: string;
    netOpeningBalance: string;
    cannotPassCreditLimit: boolean;
    creditLimit: string;
    salesPersonCommission: string;
    banks: [
      {
        bankId: string;
        bankAccountNumber: string;
      }
    ];
  };
  contact: {
    address: {
      cityId: string;
      regionId: string;
      address: string;
    },
    contact: {
      phone: string;
      internalNumber: string;
      mobile: string;
      fax: string;
    },
    mail: {
      postalCode: string;
      postalBox: string;
    }
    socialAccounts: {
      website: string
    }
  };
  branches: [
    {
      code: string;
      branchName: string;
      cityId: string;
      regionId: string;
      address: string;
      phone: string;
      fax: string;
      mobile: string;
      email: string;
      officials: [
        {
          name: string;
          job: string;
          phone: string;
          internalNumber: string;
          mobile: string;
          email: string;

        }
      ]
    }
  ];
  files: [
    {
      attachmentTypeId: string;
      file: string;
    }
  ];
  supplierTransactions: [
    {
      branchId: string
    }
  ];
  isActive: boolean;
}
