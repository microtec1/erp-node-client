export interface ISupplierCLassification {
  _id: string;
  code: string;
  suppliersClassificationNameAr: string;
  suppliersClassificationNameEn: string;
  classificationType: string;
  parentId: string;
  companyId: string;
  isActive: boolean;
  level?: number;
  children: ISupplierCLassification[];
}
