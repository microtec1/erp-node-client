export interface ISuppliersGroup {
  _id: string;
  image: string;
  code: string;
  suppliersGroupNameAr: string;
  suppliersGroupNameEn?: string;
  isActive: boolean;
}
