import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// tslint:disable-next-line:max-line-length
import { InventoryControlRejectionReasonsComponent } from './components/files/inventory-control-rejection-reasons/inventory-control-rejection-reasons.component';
// tslint:disable-next-line:max-line-length
import { AddInventoryControlRejectionReasonComponent } from './components/files/inventory-control-rejection-reasons/add-inventory-control-rejection-reason/add-inventory-control-rejection-reason.component';
// tslint:disable-next-line:max-line-length
import { ProjectsComponent } from './components/files/projects/projects.component';
import { AddProjectComponent } from './components/files/projects/add-project/add-project.component';
import { SuppliersGroupsComponent } from './components/files/suppliers-groups/suppliers-groups.component';
import { AddSuppliersGroupComponent } from './components/files/suppliers-groups/add-suppliers-group/add-suppliers-group.component';
// tslint:disable-next-line:max-line-length
import { SuppliersClassificationsComponent } from './components/files/suppliers-classifications/suppliers-classifications.component';
// tslint:disable-next-line:max-line-length
import { AddSuppliersClassificationComponent } from './components/files/suppliers-classifications/add-suppliers-classification/add-suppliers-classification.component';
import { SuppliersComponent } from './components/files/suppliers/suppliers.component';
import { AddSupplierComponent } from './components/files/suppliers/add-supplier/add-supplier.component';
import { SettingsComponent } from './components/settings/settings.component';
import { PurchasingRequestComponent } from './components/transactions/purchasing-request/purchasing-request.component';
import { AddPurchasingRequestComponent } from './components/transactions/purchasing-request/add-purchasing-request/add-purchasing-request.component';
import { StockControlComponent } from './components/transactions/stock-control/stock-control.component';
import { AddStockControlComponent } from './components/transactions/stock-control/add-stock-control/add-stock-control.component';
import { PurchaseQuotationRequestComponent } from './components/transactions/purchase-quantities-request/purchase-quotation-request.component';
import { AddPurchaseQuotationRequestComponent } from './components/transactions/purchase-quantities-request/add-purchase-quotation-request/add-purchase-quotation-request.component';
import { PurchaseQuotationComponent } from './components/transactions/purchase-quotation/purchase-quotation.component';
import { AddPurchaseQuotationComponent } from './components/transactions/purchase-quotation/add-purchase-quotation/add-purchase-quotation.component';
import { PricesComparisonComponent } from './components/transactions/prices-comparison/prices-comparison.component';
import { AddPricesComparisonComponent } from './components/transactions/prices-comparison/add-prices-comparison/add-prices-comparison.component';
import { PurchaseOrderComponent } from './components/transactions/purchase-order/purchase-order.component';
import { AddPurchaseOrderComponent } from './components/transactions/purchase-order/add-purchase-order/add-purchase-order.component';
import { PurchasingInvoiceComponent } from './components/transactions/purchasing-invoice/purchasing-invoice.component';
import { AddPurchasingInvoiceComponent } from './components/transactions/purchasing-invoice/add-purchasing-invoice/add-purchasing-invoice.component';
import { PurchasingReturnsComponent } from './components/transactions/purchasing-returns/purchasing-returns.component';
import { AddPurchasingReturnsComponent } from './components/transactions/purchasing-returns/add-purchasing-returns/add-purchasing-returns.component';


const routes: Routes = [
  { path: '', redirectTo: 'inventoryControlRejectionReasons', pathMatch: 'full' },
  { path: 'inventoryControlRejectionReasons', component: InventoryControlRejectionReasonsComponent },
  { path: 'inventoryControlRejectionReasons/add', component: AddInventoryControlRejectionReasonComponent },
  { path: 'inventoryControlRejectionReasons/edit/:id', component: AddInventoryControlRejectionReasonComponent },
  { path: 'inventoryControlRejectionReasons/details/:id', component: AddInventoryControlRejectionReasonComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'projects/add', component: AddProjectComponent },
  { path: 'projects/edit/:id', component: AddProjectComponent },
  { path: 'projects/details/:id', component: AddProjectComponent },
  { path: 'suppliersGroups', component: SuppliersGroupsComponent },
  { path: 'suppliersGroups/add', component: AddSuppliersGroupComponent },
  { path: 'suppliersGroups/edit/:id', component: AddSuppliersGroupComponent },
  { path: 'suppliersGroups/details/:id', component: AddSuppliersGroupComponent },
  { path: 'suppliersClassifications', component: SuppliersClassificationsComponent },
  { path: 'suppliersClassifications/add', component: AddSuppliersClassificationComponent },
  { path: 'suppliers', component: SuppliersComponent },
  { path: 'suppliers/add', component: AddSupplierComponent },
  { path: 'suppliers/edit/:id', component: AddSupplierComponent },
  { path: 'suppliers/details/:id', component: AddSupplierComponent },
  { path: 'settings', component: SettingsComponent },


  { path: 'purchasingRequest', component: PurchasingRequestComponent },
  { path: 'purchasingRequest/add', component: AddPurchasingRequestComponent },
  { path: 'purchasingRequest/edit/:id', component: AddPurchasingRequestComponent },
  { path: 'purchasingRequest/details/:id', component: AddPurchasingRequestComponent },
  { path: 'purchasingStockControl', component: StockControlComponent },
  { path: 'purchasingStockControl/add', component: AddStockControlComponent },
  { path: 'purchasingStockControl/edit/:id', component: AddStockControlComponent },
  { path: 'purchasingStockControl/details/:id', component: AddStockControlComponent },

  { path: 'purchaseQuotationRequest', component: PurchaseQuotationRequestComponent },
  { path: 'purchaseQuotationRequest/add', component: AddPurchaseQuotationRequestComponent },
  { path: 'purchaseQuotationRequest/edit/:id', component: AddPurchaseQuotationRequestComponent },
  { path: 'purchaseQuotationRequest/details/:id', component: AddPurchaseQuotationRequestComponent },

  { path: 'purchaseQuotation', component: PurchaseQuotationComponent },
  { path: 'purchaseQuotation/add', component: AddPurchaseQuotationComponent },
  { path: 'purchaseQuotation/edit/:id', component: AddPurchaseQuotationComponent },
  { path: 'purchaseQuotation/details/:id', component: AddPurchaseQuotationComponent },

  { path: 'pricesComparison', component: PricesComparisonComponent },
  { path: 'pricesComparison/add', component: AddPricesComparisonComponent },
  { path: 'pricesComparison/edit/:id', component: AddPricesComparisonComponent },
  { path: 'pricesComparison/details/:id', component: AddPricesComparisonComponent },

  { path: 'purchaseOrder', component: PurchaseOrderComponent },
  { path: 'purchaseOrder/add', component: AddPurchaseOrderComponent },
  { path: 'purchaseOrder/edit/:id', component: AddPurchaseOrderComponent },
  { path: 'purchaseOrder/details/:id', component: AddPurchaseOrderComponent },

  { path: 'purchasingInvoice', component: PurchasingInvoiceComponent },
  { path: 'purchasingInvoice/add', component: AddPurchasingInvoiceComponent },
  { path: 'purchasingInvoice/edit/:id', component: AddPurchasingInvoiceComponent },
  { path: 'purchasingInvoice/details/:id', component: AddPurchasingInvoiceComponent },

  { path: 'purchasingReturns', component: PurchasingReturnsComponent },
  { path: 'purchasingReturns/add', component: AddPurchasingReturnsComponent },
  { path: 'purchasingReturns/edit/:id', component: AddPurchasingReturnsComponent },
  { path: 'purchasingReturns/details/:id', component: AddPurchasingReturnsComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchasesRoutingModule { }
