import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PurchasesRoutingModule } from './purchases-routing.module';
// tslint:disable-next-line:max-line-length
import { InventoryControlRejectionReasonsComponent } from './components/files/inventory-control-rejection-reasons/inventory-control-rejection-reasons.component';
// tslint:disable-next-line:max-line-length
import { AddInventoryControlRejectionReasonComponent } from './components/files/inventory-control-rejection-reasons/add-inventory-control-rejection-reason/add-inventory-control-rejection-reason.component';
// tslint:disable-next-line:max-line-length
import { ProjectsComponent } from './components/files/projects/projects.component';
import { AddProjectComponent } from './components/files/projects/add-project/add-project.component';
import { SuppliersGroupsComponent } from './components/files/suppliers-groups/suppliers-groups.component';
import { AddSuppliersGroupComponent } from './components/files/suppliers-groups/add-suppliers-group/add-suppliers-group.component';
// tslint:disable-next-line:max-line-length
import { SuppliersClassificationsComponent } from './components/files/suppliers-classifications/suppliers-classifications.component';
// tslint:disable-next-line:max-line-length
import { SuppliersClassificationsTreeComponent } from './components/files/suppliers-classifications/suppliers-classifications-tree/suppliers-classifications-tree.component';
// tslint:disable-next-line:max-line-length
import { AddSuppliersClassificationComponent } from './components/files/suppliers-classifications/add-suppliers-classification/add-suppliers-classification.component';
import { SuppliersComponent } from './components/files/suppliers/suppliers.component';
import { AddSupplierComponent } from './components/files/suppliers/add-supplier/add-supplier.component';
import { SettingsComponent } from './components/settings/settings.component';
import { PurchasingRequestComponent } from './components/transactions/purchasing-request/purchasing-request.component';
import { AddPurchasingRequestComponent } from './components/transactions/purchasing-request/add-purchasing-request/add-purchasing-request.component';
import { StockControlComponent } from './components/transactions/stock-control/stock-control.component';
import { AddStockControlComponent } from './components/transactions/stock-control/add-stock-control/add-stock-control.component';
import { PurchaseQuotationComponent } from './components/transactions/purchase-quotation/purchase-quotation.component';
import { AddPurchaseQuotationComponent } from './components/transactions/purchase-quotation/add-purchase-quotation/add-purchase-quotation.component';
import { PricesComparisonComponent } from './components/transactions/prices-comparison/prices-comparison.component';
import { AddPricesComparisonComponent } from './components/transactions/prices-comparison/add-prices-comparison/add-prices-comparison.component';
import { PurchaseOrderComponent } from './components/transactions/purchase-order/purchase-order.component';
import { AddPurchaseOrderComponent } from './components/transactions/purchase-order/add-purchase-order/add-purchase-order.component';
import { PurchasingInvoiceComponent } from './components/transactions/purchasing-invoice/purchasing-invoice.component';
import { AddPurchasingInvoiceComponent } from './components/transactions/purchasing-invoice/add-purchasing-invoice/add-purchasing-invoice.component';
import { PurchasingReturnsComponent } from './components/transactions/purchasing-returns/purchasing-returns.component';
import { AddPurchasingReturnsComponent } from './components/transactions/purchasing-returns/add-purchasing-returns/add-purchasing-returns.component';
import { PurchaseQuotationRequestComponent } from './components/transactions/purchase-quantities-request/purchase-quotation-request.component';
import { AddPurchaseQuotationRequestComponent } from './components/transactions/purchase-quantities-request/add-purchase-quotation-request/add-purchase-quotation-request.component';

@NgModule({
  declarations: [
    InventoryControlRejectionReasonsComponent,
    AddInventoryControlRejectionReasonComponent,
    ProjectsComponent,
    AddProjectComponent,
    SuppliersGroupsComponent,
    AddSuppliersGroupComponent,
    SuppliersClassificationsComponent,
    SuppliersClassificationsTreeComponent,
    AddSuppliersClassificationComponent,
    SuppliersComponent,
    AddSupplierComponent,
    SettingsComponent,
    PurchasingRequestComponent,
    AddPurchasingRequestComponent,
    StockControlComponent,
    AddStockControlComponent,
    PurchaseQuotationComponent,
    AddPurchaseQuotationComponent,
    PricesComparisonComponent,
    AddPricesComparisonComponent,
    PurchaseOrderComponent,
    AddPurchaseOrderComponent,
    PurchasingInvoiceComponent,
    AddPurchasingInvoiceComponent,
    PurchasingReturnsComponent,
    AddPurchasingReturnsComponent,
    PurchaseQuotationRequestComponent,
    AddPurchaseQuotationRequestComponent
  ],
  imports: [
    SharedModule,
    PurchasesRoutingModule
  ]
})
export class PurchasesModule { }
