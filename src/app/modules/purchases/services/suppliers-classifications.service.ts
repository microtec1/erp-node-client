import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ISupplierCLassification } from '../interfaces/ISupplierCLassification.model';

@Injectable({
  providedIn: 'root'
})
export class SuppliersClassificationsService {
  suppliersClassificationsChanged = new Subject<ISupplierCLassification[]>();
  fillFormChange = new Subject<ISupplierCLassification>();

  constructor() {}

}
