import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { TranslateService } from '@ngx-translate/core';
import { companyId } from 'src/app/common/constants/general.constants';
import { ICustomersClassification } from '../../../../interfaces/ICustomersClassification';
import { CustomersClassificationsService } from '../../../../services/customers-classifications.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { customersClassificationsApi, customersClassificationsTreeApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-add-customers-classification',
  templateUrl: './add-customers-classification.component.html',
  styleUrls: ['./add-customers-classification.component.scss']
})

export class AddCustomersClassificationComponent implements OnInit {
  @Input() item: ICustomersClassification;
  @Input() selectedItem: ICustomersClassification;
  @Input() sidebarMode: boolean;
  @Output() finished: EventEmitter<any> = new EventEmitter();
  customersClassificationForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  customersClassification: ICustomersClassification;
  constructor(
    private CCService: CustomersClassificationsService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.initForm();
    this.subscriptions.push(
      this.CCService.fillFormChange.subscribe((res: ICustomersClassification) => {
        this.customersClassification = res;
        if (this.customersClassification) {
          this.customersClassificationForm.patchValue({
            code: this.customersClassification.code,
            customersClassificationNameAr: this.customersClassification.customersClassificationNameAr,
            customersClassificationNameEn: this.customersClassification.customersClassificationNameEn,
            classificationType: this.customersClassification.classificationType,
            isActive: this.customersClassification.isActive,
          });
        } else {
          this.customersClassificationForm.patchValue({
            code: '',
            customersClassificationNameAr: '',
            customersClassificationNameEn: '',
            classificationType: '',
            isActive: true,
          });
        }
      })
    );
  }

  get form() {
    return this.customersClassificationForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  cancel() {
    this.finished.emit(true);
  }

  private initForm() {
    let code = '';
    let customersClassificationNameAr = '';
    let customersClassificationNameEn = '';
    let classificationType = 'main';
    let isActive = true;

    if (this.item) {
      code = this.item.code;
      customersClassificationNameAr = this.item.customersClassificationNameAr;
      customersClassificationNameEn = this.item.customersClassificationNameEn;
      classificationType = this.item.classificationType;
      isActive = this.item.isActive;
    }

    this.customersClassificationForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      customersClassificationNameAr: new FormControl(customersClassificationNameAr, Validators.required),
      customersClassificationNameEn: new FormControl(customersClassificationNameEn),
      classificationType: new FormControl(classificationType),
      isActive: new FormControl(isActive),
    });
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.customersClassification || this.item) {
      if (this.customersClassificationForm.valid) {
        const editItem = this.customersClassification && this.customersClassification || this.item && this.item;
        const newcustomersClassification = {
          _id: editItem._id,
          ...this.generalService.checkEmptyFields(this.customersClassificationForm.value),
          companyId
        };
        this.data.put(customersClassificationsApi, newcustomersClassification).subscribe(res => {
          this.loadingButton = false;
          this.submitted = false;
          this.uiService.isLoading.next(false);
          this.CCService.fillFormChange.next(null);
          this.finished.emit();
          this.subscriptions.push(
            this.data.getByRoute(customersClassificationsTreeApi).subscribe((response: ICustomersClassification[]) => {
              this.CCService.customersClassificationsChanged.next(response);
              this.uiService.isLoading.next(false);
            }, err => {
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            })
          );
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        },
          err => {
            this.uiService.isLoading.next(false);
            if (this.translate.currentLang === 'en') {
              this.uiService.showError(err.error.error.en, '');
            } else {
              this.uiService.showError(err.error.error.ar, '');
            }
          });
        this.loadingButton = false;
      }
    } else {
      if (this.customersClassificationForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.customersClassificationForm.value),
          companyId,
          parentId: this.selectedItem ? this.selectedItem._id : null
        };

        this.subscriptions.push(
          this.data.post(customersClassificationsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.customersClassificationForm.reset();
            this.customersClassificationForm.patchValue({
              isActive: true,
              classificationType: 'main'
            });
            this.subscriptions.push(
              this.data.getByRoute(customersClassificationsTreeApi).subscribe((response: ICustomersClassification[]) => {
                this.CCService.customersClassificationsChanged.next(response);
                this.uiService.isLoading.next(false);
              }, err => {
                this.uiService.isLoading.next(false);
                this.uiService.showErrorMessage(err);
              })
            );
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              if (this.translate.currentLang === 'en') {
                this.uiService.showError(err.error.error.en, '');
              } else {
                this.uiService.showError(err.error.error.ar, '');
              }
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

}
