import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { Subscription } from 'rxjs';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { ICustomersClassification } from '../../../../interfaces/ICustomersClassification';
import { CustomersClassificationsService } from '../../../../services/customers-classifications.service';
import { DataService } from 'src/app/common/services/shared/data.service';
import { customersClassificationsApi, customersClassificationsTreeApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-customers-classifications-tree',
  templateUrl: './customers-classifications-tree.component.html',
  styleUrls: ['./customers-classifications-tree.component.scss']
})
export class CustomersClassificationsTreeComponent implements OnInit, OnDestroy {
  @Input() items: ICustomersClassification[];
  selected: any;
  selectedCustomersClassification: ICustomersClassification;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  initAddForm: boolean;
  initEditForm: boolean;

  constructor(
    private modalService: BsModalService,
    private CCService: CustomersClassificationsService,
    private data: DataService,
    private uiService: UiService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
  }

  actionFinished() {
    this.initAddForm = false;
    this.initEditForm = false;
    this.selectedCustomersClassification = null;
    this.selected = null;
  }

  showDetails(item) {
    this.selected = item._id;
    this.selectedCustomersClassification = item;
    this.initAddForm = false;
    this.initEditForm = false;
  }

  deleteModal(customersClassification: ICustomersClassification) {
    const initialState = {
      code: customersClassification.code,
      nameAr: customersClassification.customersClassificationNameAr,
      nameEn: customersClassification.customersClassificationNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(customersClassification._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(customersClassificationsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.subscriptions.push(
          this.data.getByRoute(customersClassificationsTreeApi).subscribe((response: ICustomersClassification[]) => {
            this.actionFinished();
            this.CCService.customersClassificationsChanged.next(response);
            this.uiService.isLoading.next(false);
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  addNewItem() {
    this.initEditForm = false;
    this.initAddForm = true;
    this.CCService.fillFormChange.next(null);
  }

  editItem() {
    this.initAddForm = false;
    this.initEditForm = true;
    this.CCService.fillFormChange.next(this.selectedCustomersClassification);
  }


  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
