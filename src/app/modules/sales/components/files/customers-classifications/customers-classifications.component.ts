import { Component, OnInit, OnDestroy } from '@angular/core';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { Subscription } from 'rxjs';
import { CustomersClassificationsService } from '../../../services/customers-classifications.service';
import { ICustomersClassification } from '../../../interfaces/ICustomersClassification';
import { DataService } from 'src/app/common/services/shared/data.service';
import { customersClassificationsTreeApi, customersClassificationsApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';

@Component({
  selector: 'app-customers-classifications',
  templateUrl: './customers-classifications.component.html',
  styleUrls: ['./customers-classifications.component.scss']
})
export class CustomersClassificationsComponent implements OnInit, OnDestroy {
  customersClassifications: ICustomersClassification[];
  subscriptions: Subscription[] = [];
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;

  constructor(
    private CCService: CustomersClassificationsService,
    private data: DataService,
    private generalService: GeneralService,
    private uiService: UiService
  ) { }


  ngOnInit() {
    this.subscriptions.push(
      this.CCService.customersClassificationsChanged.subscribe((res: ICustomersClassification[]) => {
        this.customersClassifications = res;
      })
    );
    this.getCustomersClassificationsTree();
    this.initSearchForm();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      customersClassificationNameAr: new FormControl(''),
      customersClassificationNameEn: new FormControl('')
    });
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCustomersClassificationsTree();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(customersClassificationsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.customersClassifications = res.results;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  getCustomersClassificationsTree() {
    this.subscriptions.push(
      this.data.getByRoute(customersClassificationsTreeApi).subscribe((res: ICustomersClassification[]) => {
        this.customersClassifications = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
