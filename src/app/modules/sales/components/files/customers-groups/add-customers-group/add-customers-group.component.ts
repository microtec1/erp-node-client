import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, customersGroupsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { ICustomersGroup } from '../../../../interfaces/ICustomersGroup';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-customers-group',
  templateUrl: './add-customers-group.component.html',
  styleUrls: ['./add-customers-group.component.scss']
})

export class AddCustomersGroupComponent implements OnInit, OnDestroy {
  customersGroupForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  customersGroup: ICustomersGroup;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(customersGroupsApi, null, null, id)
              .subscribe((customersGroup: ICustomersGroup) => {
                this.customersGroup = customersGroup;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.customersGroupForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.customersGroupForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.customersGroup.image = '';
    this.customersGroupForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.customersGroupForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.customersGroup) {
      if (this.customersGroupForm.valid) {
        const newcustomersGroup = {
          _id: this.customersGroup._id,
          ...this.generalService.checkEmptyFields(this.customersGroupForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(customersGroupsApi, newcustomersGroup).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/sales/customersGroups']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.customersGroupForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.customersGroupForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(customersGroupsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.customersGroupForm.reset();
            this.customersGroupForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let customersGroupNameAr = '';
    let customersGroupNameEn = '';
    let isActive = true;

    if (this.customersGroup) {
      image = this.customersGroup.image;
      code = this.customersGroup.code;
      customersGroupNameAr = this.customersGroup.customersGroupNameAr;
      customersGroupNameEn = this.customersGroup.customersGroupNameEn;
      isActive = this.customersGroup.isActive;
    }
    this.customersGroupForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      customersGroupNameAr: new FormControl(customersGroupNameAr, Validators.required),
      customersGroupNameEn: new FormControl(customersGroupNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
