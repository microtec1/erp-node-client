import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, customersGroupsApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { ICustomersGroup } from '../../../interfaces/ICustomersGroup';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-customers-groups',
  templateUrl: './customers-groups.component.html',
  styleUrls: ['./customers-groups.component.scss']
})

export class CustomersGroupsComponent implements OnInit, OnDestroy {
  customersGroups: ICustomersGroup[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService
  ) { }

  ngOnInit() {
    this.getCustomersGroupsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(customersGroupsApi, pageNumber).subscribe((res: IDataRes) => {
      this.customersGroups = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(customersGroupsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.customersGroups = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCustomersGroupsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(customersGroupsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.customersGroups = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      customersGroupNameAr: new FormControl(''),
      customersGroupNameEn: new FormControl('')
    });
  }

  deleteModal(customersGroup: ICustomersGroup) {
    const initialState = {
      code: customersGroup.code,
      nameAr: customersGroup.customersGroupNameAr,
      nameEn: customersGroup.customersGroupNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(customersGroup._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(customersGroupsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.customersGroups = this.generalService.removeItem(this.customersGroups, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getCustomersGroupsFirstPage() {
    this.subscriptions.push(
      this.data.get(customersGroupsApi, 1).subscribe((res: IDataRes) => {
        this.customersGroups = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
