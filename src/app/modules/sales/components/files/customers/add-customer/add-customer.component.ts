import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  customersApi,
  companiesApi,
  companyCurrenciesApi,
  attachmentTypesApi,
  banksApi,
  nationalitiesApi,
  regionsApi,
  citiesApi,
  customersGroupsApi,
  pricingPoliciesApi,
  detailedCustomersClassificationsApi,
  detailedChartOfAccountsApi,
  employeesApi
} from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import {
  companyId,
  searchLength
} from 'src/app/common/constants/general.constants';
import { IRegion } from 'src/app/modules/general/interfaces/IRegion.model';
import { ICity } from 'src/app/modules/general/interfaces/ICity.model';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { INationality } from 'src/app/modules/general/interfaces/INationality';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { IAttachmentType } from 'src/app/modules/general/interfaces/IAttachmentType';
import { IBranch } from 'src/app/modules/general/interfaces/IBranch';
import { ICompany } from 'src/app/modules/general/interfaces/ICompany';
import { ICustomer } from '../../../../interfaces/ICustomer';
import { ICustomersGroup } from '../../../../interfaces/ICustomersGroup';
import { ICustomersClassification } from '../../../../interfaces/ICustomersClassification';
import { IPricingPolicy } from '../../../../interfaces/IPricingPolicy';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit, OnDestroy {
  customerForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  customer: ICustomer;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;

  @ViewChild('dropzone') dropzone: any;
  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesInputFocused2: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Cities
  cities: ICity[] = [];
  citiesInputFocused: boolean;
  hasMoreCities: boolean;
  citiesCount: number;
  selectedCitiesPage = 1;
  citiesPagesNo: number;
  noCities: boolean;

  // Regions
  regions: IRegion[] = [];
  regionsInputFocused: boolean;
  hasMoreRegions: boolean;
  regionsCount: number;
  selectedRegionsPage = 1;
  regionsPagesNo: number;
  noRegions: boolean;

  // Customers Groups
  customersGroups: ICustomersGroup[] = [];
  customersGroupsInputFocused: boolean;
  hasMoreCustomersGroups: boolean;
  customersGroupsCount: number;
  selectedCustomersGroupsPage = 1;
  customersGroupsPagesNo: number;
  noCustomersGroups: boolean;

  // Nationalities
  nationalities: INationality[] = [];
  nationalitiesInputFocused: boolean;
  hasMoreNationalities: boolean;
  nationalitiesCount: number;
  selectedNationalitiesPage = 1;
  nationalitiesPagesNo: number;
  noNationalities: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Attachment Types
  attachmentTypes: IAttachmentType[] = [];
  attachmentTypesInputFocused: boolean;
  hasMoreAttachmentTypes: boolean;
  attachmentTypesCount: number;
  selectedAttachmentTypesPage = 1;
  attachmentTypesPagesNo: number;
  noAttachmentTypes: boolean;

  // Customers Classifications
  customersClassifications: ICustomersClassification[] = [];
  customersClassificationsInputFocused: boolean;
  customersClassificationsCount: number;
  noCustomersClassifications: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Branches
  branches: IBranch[] = [];
  branchesInputFocused: boolean;
  branchesCount: number;
  noBranches: boolean;

  // Pricing Policies
  pricingPolicies: IPricingPolicy[] = [];
  pricingPoliciesInputFocused: boolean;
  hasMorePricingPolicies: boolean;
  pricingPoliciesCount: number;
  selectedPricingPoliciesPage = 1;
  pricingPoliciesPagesNo: number;
  noPricingPolicies: boolean;

  // Sales Representatives
  salesRepresentatives: IEmployee[] = [];
  salesRepresentativesInputFocused: boolean;
  salesRepresentativesCount: number;
  noSalesRepresentatives: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data
              .get(customersApi, null, null, id)
              .subscribe((customer: ICustomer) => {
                this.customer = customer;
                this.formReady = true;
                this.initForm();
                this.syncControls();
                if (this.detailsMode) {
                  this.customerForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
          this.syncControls();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(companiesApi, null, null, companyId).subscribe((res: ICompany) => {
        if (res.branches.length) {
          this.branchesCount = res.branches.length;
          this.branches.push(...res.branches);
        } else {
          this.noBranches = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .post(companyCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.companyCurrency.length) {
            this.currencies.push(...res.companyCurrency);
            this.currenciesCount = res.companyCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(attachmentTypesApi, 1).subscribe((res: IDataRes) => {
        this.attachmentTypesPagesNo = res.pages;
        this.attachmentTypesCount = res.count;
        if (this.attachmentTypesPagesNo > this.selectedAttachmentTypesPage) {
          this.hasMoreAttachmentTypes = true;
        }
        this.attachmentTypes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(nationalitiesApi, 1)
        .subscribe((res: IDataRes) => {
          this.nationalitiesPagesNo = res.pages;
          this.nationalitiesCount = res.count;
          if (this.nationalitiesPagesNo > this.selectedNationalitiesPage) {
            this.hasMoreNationalities = true;
          }
          this.nationalities.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(citiesApi, 1).subscribe((res: IDataRes) => {
        this.citiesPagesNo = res.pages;
        this.citiesCount = res.count;
        if (this.citiesPagesNo > this.selectedCitiesPage) {
          this.hasMoreCities = true;
        }
        this.cities.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(regionsApi, 1).subscribe((res: IDataRes) => {
        this.regionsPagesNo = res.pages;
        this.regionsCount = res.count;
        if (this.regionsPagesNo > this.selectedRegionsPage) {
          this.hasMoreRegions = true;
        }
        this.regions.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(customersGroupsApi, 1)
        .subscribe((res: IDataRes) => {
          this.customersGroupsPagesNo = res.pages;
          this.customersGroupsCount = res.count;
          if (this.customersGroupsPagesNo > this.selectedCustomersGroupsPage) {
            this.hasMoreCustomersGroups = true;
          }
          this.customersGroups.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(pricingPoliciesApi, 1).subscribe((res: IDataRes) => {
        this.pricingPoliciesPagesNo = res.pages;
        this.pricingPoliciesCount = res.count;
        if (this.pricingPoliciesPagesNo > this.selectedPricingPoliciesPage) {
          this.hasMorePricingPolicies = true;
        }
        this.pricingPolicies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.getByRoute(detailedCustomersClassificationsApi).subscribe(
        (res: IDataRes) => {
          if (res.results.length) {
            this.customersClassifications.push(...res.results);
            this.customersClassificationsCount = res.count;
          } else {
            this.noCustomersClassifications = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe(
        (res: IChartOfAccount[]) => {
          if (res.length) {
            this.chartOfAccounts.push(...res);
            this.chartOfAccountsCount = res.length;
          } else {
            this.noChartOfAccounts = true;
          }
          this.uiService.isLoading.next(false);
        }
      )
    );

    const searchQuery = {
      salesRepresentative: true,
      companyId
    };
    this.subscriptions.push(
      this.data
        .get(employeesApi, null, searchQuery)
        .subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSalesRepresentatives = true;
          } else {
            this.noSalesRepresentatives = false;
            for (const item of res.results) {
              if (this.salesRepresentatives.length) {
                const uniqueSalesRepresentatives = this.salesRepresentatives.filter(
                  x => x._id !== item._id
                );
                this.salesRepresentatives = uniqueSalesRepresentatives;
              }
              this.salesRepresentatives.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  syncControls() {
    this.subscriptions.push(
      this.customerForm.controls.openingBalances.valueChanges.subscribe(res => {
        const debitValues = [];
        const creditValues = [];
        for (const item of this.getOpeningBalancesArray.controls) {
          if (item.value.accountSide === 'credit') {
            creditValues.push(parseFloat(item.value.balance) * item.value.exchangeRate);
          }
          if (item.value.accountSide === 'debit') {
            debitValues.push(parseFloat(item.value.balance) * item.value.exchangeRate);
          }
        }
        const creditValuesSum = creditValues.reduce((acc, cur) => acc + cur, 0);
        const debitValuesSum = debitValues.reduce((acc, cur) => acc + cur, 0);

        this.customerForm.controls.accountingInfo.patchValue({
          netOpeningBalance: Math.abs((debitValuesSum - creditValuesSum) / this.customerForm.value.exchangeRate)
        });
      })
    );
  }

  fillRate(event, formGroup: FormGroup) {
    const currency = this.currencies.find(item => item.currencyId === event);
    if (formGroup) {
      formGroup.patchValue({
        exchangeRate: currency.exchangeRate
      });
    } else {
      this.customerForm.patchValue({
        exchangeRate: currency.exchangeRate
      });
    }
  }

  searchRegions(event) {
    const searchValue = event;
    const searchQuery = {
      regionNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(regionsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noRegions = true;
            } else {
              this.noRegions = false;
              for (const item of res.results) {
                if (this.regions.length) {
                  const uniqueRegions = this.regions.filter(
                    x => x._id !== item._id
                  );
                  this.regions = uniqueRegions;
                }
                this.regions.push(item);
              }
            }
            this.regions = res.results;
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreRegions() {
    this.selectedRegionsPage = this.selectedRegionsPage + 1;
    this.subscriptions.push(
      this.data
        .get(regionsApi, this.selectedRegionsPage)
        .subscribe((res: IDataRes) => {
          if (this.regionsPagesNo > this.selectedRegionsPage) {
            this.hasMoreRegions = true;
          } else {
            this.hasMoreRegions = false;
          }
          for (const item of res.results) {
            if (this.regions.length) {
              const uniqueRegions = this.regions.filter(
                x => x._id !== item._id
              );
              this.regions = uniqueRegions;
            }
            this.regions.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCities(event) {
    const searchValue = event;
    const searchQuery = {
      cityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(citiesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noCities = true;
            } else {
              this.noCities = false;
              for (const item of res.results) {
                if (this.cities.length) {
                  const uniqueCities = this.cities.filter(
                    x => x._id !== item._id
                  );
                  this.cities = uniqueCities;
                }
                this.cities.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreCities() {
    this.selectedCitiesPage = this.selectedCitiesPage + 1;
    this.subscriptions.push(
      this.data
        .get(citiesApi, this.selectedCitiesPage)
        .subscribe((res: IDataRes) => {
          if (this.citiesPagesNo > this.selectedCitiesPage) {
            this.hasMoreCities = true;
          } else {
            this.hasMoreCities = false;
          }
          for (const item of res.results) {
            if (this.cities.length) {
              const uniqueCities = this.cities.filter(x => x._id !== item._id);
              this.cities = uniqueCities;
            }
            this.cities.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchNationalities(event) {
    const searchValue = event;
    const searchQuery = {
      nationalityNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(nationalitiesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noNationalities = true;
            } else {
              this.noNationalities = false;
              for (const item of res.results) {
                if (this.nationalities.length) {
                  const uniqueNationalities = this.nationalities.filter(
                    x => x._id !== item._id
                  );
                  this.nationalities = uniqueNationalities;
                }
                this.nationalities.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreNationalities() {
    this.selectedNationalitiesPage = this.selectedNationalitiesPage + 1;
    this.subscriptions.push(
      this.data
        .get(nationalitiesApi, this.selectedNationalitiesPage)
        .subscribe((res: IDataRes) => {
          if (this.nationalitiesPagesNo > this.selectedNationalitiesPage) {
            this.hasMoreNationalities = true;
          } else {
            this.hasMoreNationalities = false;
          }
          for (const item of res.results) {
            if (this.nationalities.length) {
              const uniqueNationalities = this.nationalities.filter(
                x => x._id !== item._id
              );
              this.nationalities = uniqueNationalities;
            }
            this.nationalities.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchCustomersGroups(event) {
    const searchValue = event;
    const searchQuery = {
      customersGroupNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(customersGroupsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noCustomersGroups = true;
            } else {
              this.noCustomersGroups = false;
              for (const item of res.results) {
                if (this.customersGroups.length) {
                  const uniqueCustomersGroups = this.customersGroups.filter(
                    x => x._id !== item._id
                  );
                  this.customersGroups = uniqueCustomersGroups;
                }
                this.customersGroups.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreCustomersGroups() {
    this.selectedCustomersGroupsPage = this.selectedCustomersGroupsPage + 1;
    this.subscriptions.push(
      this.data
        .get(customersGroupsApi, this.selectedCustomersGroupsPage)
        .subscribe((res: IDataRes) => {
          if (this.customersGroupsPagesNo > this.selectedCustomersGroupsPage) {
            this.hasMoreCustomersGroups = true;
          } else {
            this.hasMoreCustomersGroups = false;
          }
          for (const item of res.results) {
            if (this.customersGroups.length) {
              const uniqueCustomersGroups = this.customersGroups.filter(
                x => x._id !== item._id
              );
              this.customersGroups = uniqueCustomersGroups;
            }
            this.customersGroups.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchPricingPolicies(event) {
    const searchValue = event;
    const searchQuery = {
      pricingPolicyNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(pricingPoliciesApi, null, searchQuery).subscribe(
          (res: IDataRes) => {
            if (!res.results.length) {
              this.noPricingPolicies = true;
            } else {
              this.noPricingPolicies = false;
              for (const item of res.results) {
                if (this.pricingPolicies.length) {
                  const uniquepricingPolicies = this.pricingPolicies.filter(
                    x => x._id !== item._id
                  );
                  this.pricingPolicies = uniquepricingPolicies;
                }
                this.pricingPolicies.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          }
        )
      );
    }
  }

  loadMorePricingPolicies() {
    this.selectedPricingPoliciesPage = this.selectedPricingPoliciesPage + 1;
    this.subscriptions.push(
      this.data.get(
        pricingPoliciesApi, this.selectedPricingPoliciesPage
      ).subscribe((res: IDataRes) => {
        if (this.pricingPoliciesPagesNo > this.selectedPricingPoliciesPage) {
          this.hasMorePricingPolicies = true;
        } else {
          this.hasMorePricingPolicies = false;
        }
        for (const item of res.results) {
          if (this.pricingPolicies.length) {
            const uniquePricingPolicies = this.pricingPolicies.filter(
              x => x._id !== item._id
            );
            this.pricingPolicies = uniquePricingPolicies;
          }
          this.pricingPolicies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  getLastWord(link) {
    const str = link;
    const n = str.lastIndexOf('/');
    const result = str.substring(n + 1);
    return result;
  }

  removeFile(index) {
    this.customer.files[index].file = null;
    this.getFilesArray.controls[index].patchValue({
      file: ''
    });
  }

  inputFileChanged(files: File[], formGroup) {
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        formGroup.patchValue({
          file: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetFile(dropzoneFile, formGroup) {
    formGroup.patchValue({
      file: ''
    });
    dropzoneFile.reset();
  }

  searchAttachmentTypes(event) {
    const searchValue = event;
    const searchQuery = {
      attachmentTypeNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(attachmentTypesApi, null, searchQuery).subscribe(
          (res: IDataRes) => {
            if (!res.results.length) {
              this.noAttachmentTypes = true;
            } else {
              this.noAttachmentTypes = false;
              for (const item of res.results) {
                if (this.attachmentTypes.length) {
                  const uniqueAttachmentTypes = this.attachmentTypes.filter(
                    x => x._id !== item._id
                  );
                  this.attachmentTypes = uniqueAttachmentTypes;
                }
                this.attachmentTypes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          }
        )
      );
    }
  }

  loadMoreAttachmentTypes() {
    this.selectedAttachmentTypesPage = this.selectedAttachmentTypesPage + 1;
    this.subscriptions.push(
      this.data.get(
        attachmentTypesApi, this.selectedAttachmentTypesPage
      ).subscribe((res: IDataRes) => {
        if (this.attachmentTypesPagesNo > this.selectedAttachmentTypesPage) {
          this.hasMoreAttachmentTypes = true;
        } else {
          this.hasMoreAttachmentTypes = false;
        }
        for (const item of res.results) {
          if (this.attachmentTypes.length) {
            const uniqueAttachmentTypes = this.attachmentTypes.filter(
              x => x._id !== item._id
            );
            this.attachmentTypes = uniqueAttachmentTypes;
          }
          this.attachmentTypes.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.customerForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.customer.image = '';
    this.customerForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.customerForm.controls;
  }

  get getOpeningBalancesArray() {
    return this.customerForm.get('openingBalances') as FormArray;
  }

  get getBanksArray() {
    return this.customerForm.controls.accountingInfo.get('banks') as FormArray;
  }

  get getBranchesArray() {
    return this.customerForm.get('branches') as FormArray;
  }

  get getFilesArray() {
    return this.customerForm.get('files') as FormArray;
  }

  get getCustomerTransactionsArray() {
    return this.customerForm.get('customerTransactions') as FormArray;
  }

  addCustomerTransaction() {
    const control = this.customerForm.get('customerTransactions') as FormArray;
    control.push(
      new FormGroup({
        branchId: new FormControl('')
      })
    );
  }

  deleteCustomerTransaction(index) {
    const control = this.customerForm.get('customerTransactions') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addFile() {
    const control = this.customerForm.get('files') as FormArray;
    control.push(
      new FormGroup({
        attachmentTypeId: new FormControl(''),
        file: new FormControl('')
      })
    );
  }

  deleteFile(index) {
    const control = this.customerForm.get('files') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addOfficial(control) {
    control.push(
      new FormGroup({
        name: new FormControl(''),
        job: new FormControl(''),
        phone: new FormControl(null),
        internalNumber: new FormControl(null),
        mobile: new FormControl(null),
        email: new FormControl('')
      })
    );
  }

  deleteOfficial(control, index) {
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBranch() {
    const control = this.customerForm.get('branches') as FormArray;
    control.push(
      new FormGroup({
        code: new FormControl(''),
        branchName: new FormControl(''),
        cityId: new FormControl(''),
        regionId: new FormControl(''),
        address: new FormControl(''),
        phone: new FormControl(null),
        fax: new FormControl(null),
        mobile: new FormControl(null),
        email: new FormControl(''),
        officials: new FormArray([
          new FormGroup({
            name: new FormControl(''),
            job: new FormControl(''),
            phone: new FormControl(null),
            internalNumber: new FormControl(null),
            mobile: new FormControl(null),
            email: new FormControl('')
          })
        ])
      })
    );
  }

  deleteBranch(index) {
    const control = this.customerForm.get('branches') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addOpeningBalance() {
    const control = this.customerForm.get('openingBalances') as FormArray;
    control.push(
      new FormGroup({
        balance: new FormControl(null),
        netBalance: new FormControl(null),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(''),
        accountSide: new FormControl(''),
        dueGregorianDate: new FormControl(''),
        dueHijriDate: new FormControl(''),
        referenceNumber: new FormControl(''),
        notes: new FormControl('')
      })
    );
  }

  deleteOpeningBalance(index) {
    const control = this.customerForm.get('openingBalances') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addBank() {
    const control = this.customerForm.controls.accountingInfo.get(
      'banks'
    ) as FormArray;
    control.push(
      new FormGroup({
        bankId: new FormControl(''),
        bankAccountNumber: new FormControl(null)
      })
    );
  }

  deleteBank(index) {
    const control = this.customerForm.controls.accountingInfo.get(
      'banks'
    ) as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  setHijriDate(value: Date, formGroup?, fieldName?: string) {
    if (value) {
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        if (typeof formGroup === 'string') {
          this.customerForm.controls[formGroup].patchValue({
            [fieldName]: {
              year: hijriDate.iYear(),
              month: hijriDate.iMonth() + 1,
              day: hijriDate.iDate()
            }
          });
        } else {
          formGroup.patchValue({
            [fieldName]: {
              year: hijriDate.iYear(),
              month: hijriDate.iMonth() + 1,
              day: hijriDate.iDate()
            }
          });
        }
      } else {
        this.customerForm.patchValue({
          hijriDate: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?, fieldName?: string) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (formGroup) {
        if (typeof formGroup === 'string') {
          this.customerForm.controls[formGroup].patchValue({
            [fieldName]: this.generalService.format(
              new Date(
                gegorianDate.year(),
                gegorianDate.month(),
                gegorianDate.date()
              )
            )
          });
        } else {
          formGroup.patchValue({
            [fieldName]: this.generalService.format(
              new Date(
                gegorianDate.year(),
                gegorianDate.month(),
                gegorianDate.date()
              )
            )
          });
        }

      } else {
        this.customerForm.patchValue({
          gregorianDate: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  validateCurrency(value, formGroup: FormGroup) {
    formGroup.patchValue({
      netBalance: value
    });

    if (value) {
      formGroup.controls.currencyId.setValidators(Validators.required);
      formGroup.controls.currencyId.updateValueAndValidity();
    } else {
      formGroup.controls.currencyId.clearValidators();
      formGroup.controls.currencyId.updateValueAndValidity();
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.customerForm.value.identity) {
      if (this.customerForm.value.identity.releaseGregorianDate) {
        if (
          typeof this.customerForm.value.identity.releaseGregorianDate !==
          'string'
        ) {
          this.customerForm.value.identity.releaseGregorianDate = this.generalService.format(
            this.customerForm.value.identity.releaseGregorianDate
          );
        }
        if (
          typeof this.customerForm.value.identity.releaseHijriDate !==
          'string'
        ) {
          this.customerForm.value.identity.releaseHijriDate = this.generalService.formatHijriDate(
            this.customerForm.value.identity.releaseHijriDate
          );
        }
      }
    }
    if (this.customerForm.value.officialDocuments.commercialRegistration) {
      if (
        this.customerForm.value.officialDocuments.commercialRegistration
          .startBusinessGregorianDate
      ) {
        if (
          typeof this.customerForm.value.officialDocuments.commercialRegistration
            .startBusinessGregorianDate !== 'string'
        ) {
          this.customerForm.value.officialDocuments.commercialRegistration.startBusinessGregorianDate = this.generalService.format(
            this.customerForm.value.officialDocuments.commercialRegistration
              .startBusinessGregorianDate
          );
        }
        if (
          typeof this.customerForm.value.officialDocuments.commercialRegistration
            .startBusinessHijriDate !== 'string'
        ) {
          this.customerForm.value.officialDocuments.commercialRegistration.startBusinessHijriDate = this.generalService.formatHijriDate(
            this.customerForm.value.officialDocuments.commercialRegistration
              .startBusinessHijriDate
          );
        }
      }
    }

    for (
      let i = 0;
      i <= this.customerForm.value.openingBalances.length - 1;
      i++
    ) {
      if (this.customerForm.value.openingBalances[i].dueGregorianDate) {
        if (
          typeof this.customerForm.value.openingBalances[i].dueGregorianDate !==
          'string'
        ) {
          this.customerForm.value.openingBalances[i].dueGregorianDate = this.generalService.format(
            this.customerForm.value.openingBalances[i].dueGregorianDate
          );
        }
        if (typeof this.customerForm.value.openingBalances[i].dueHijriDate !==
          'string') {
          this.customerForm.value.openingBalances[i].dueHijriDate = this.generalService.formatHijriDate(
            this.customerForm.value.openingBalances[i].dueHijriDate
          );
        }

      }
    }

    if (this.customer) {
      if (this.customerForm.valid) {
        const newCustomer = {
          _id: this.customer._id,
          ...this.generalService.checkEmptyFields(this.customerForm.value),
          companyId: this.companyId
        };
        const emptyObject = this.generalService.isEmpty(this.customerForm.value.files[0]);
        if (emptyObject) {
          newCustomer.files = [];
        }
        const emptyObject2 = this.generalService.isEmpty(this.customerForm.value.customerTransactions[0]);
        if (emptyObject2) {
          newCustomer.customerTransactions = [];
        }
        this.subscriptions.push(
          this.data.put(customersApi, newCustomer).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/sales/customers']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          }, err => {
            this.uiService.isLoading.next(false);
            this.uiService.showErrorMessage(err);
          })
        );
        this.loadingButton = false;
      }
    } else {
      const invalid = [];
      const controls = this.customerForm.controls;
      for (const name in controls) {
        if (controls[name].invalid) {
          invalid.push(name);
        }
      }
      console.log(invalid);

      if (this.customerForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.customerForm.value),
          companyId: this.companyId
        };
        const emptyObject = this.generalService.isEmpty(this.customerForm.value.files[0]);
        if (emptyObject) {
          formValue.files = [];
        }
        const emptyObject2 = this.generalService.isEmpty(this.customerForm.value.customerTransactions[0]);
        if (emptyObject2) {
          formValue.customerTransactions = [];
        }

        this.subscriptions.push(
          this.data.post(customersApi, formValue).subscribe(
            res => {
              this.loadingButton = false;
              this.submitted = false;
              this.uiService.isLoading.next(false);
              this.customerForm.reset();
              this.filesAdded = false;
              this.dropzone.reset();
              this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
            },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        const invalid = [];
        const controls = this.customerForm.controls;
        for (const name in controls) {
          if (controls[name].invalid) {
            invalid.push(name);
          }
        }
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let customerNameAr = '';
    let customerNameEn = '';
    let isActive = true;
    let pricingPolicyId = '';
    let customersGroupId = '';
    let currencyId = '';
    let exchangeRate = null;
    let salesPersonId = '';
    let customersClassificationId = '';
    let nationalityId = '';
    let identityNumber = null;
    let identitySource = '';
    let releaseGregorianDate = null;
    let releaseHijriDate = null;
    let taxRegistrationNumber = null;
    let taxCardNumber = null;
    let missionCode = '';
    let missionName = '';
    let businessAddress = '';
    let fileNumber = '';
    let businessName = '';
    let financierName = '';
    let commercialName = '';
    let commercialTrait = '';
    let commercialBrand = '';
    let traderName = '';
    let commercialRegistrationNationalityId = '';
    let financialSystem = '';
    let commercialEligibility = '';
    let tradeType = '';
    let startBusinessGregorianDate = null;
    let startBusinessHijriDate = null;
    let openingBalancesArray = new FormArray([
      new FormGroup({
        balance: new FormControl(null),
        netBalance: new FormControl(null),
        currencyId: new FormControl(''),
        exchangeRate: new FormControl(''),
        accountSide: new FormControl(''),
        dueGregorianDate: new FormControl(null),
        dueHijriDate: new FormControl(null),
        referenceNumber: new FormControl(''),
        notes: new FormControl('')
      })
    ]);
    let salesDuePeriod = null;
    let glAccountId = '';
    let netOpeningBalance = null;
    let cannotPassCreditLimit = false;
    let creditLimit = null;
    let salesPersonCommission = '';
    let banksArray = new FormArray([
      new FormGroup({
        bankId: new FormControl(''),
        bankAccountNumber: new FormControl(null)
      })
    ]);
    let cityId = '';
    let regionId = '';
    let address = '';
    let phone = '';
    let internalNumber = null;
    let fax = null;
    let mobile = null;
    let postalCode = null;
    let postalBox = '';
    let website = '';
    let officialsArray = new FormArray([
      new FormGroup({
        name: new FormControl(''),
        job: new FormControl(''),
        phone: new FormControl(null),
        internalNumber: new FormControl(null),
        mobile: new FormControl(null),
        email: new FormControl('')
      })
    ]);
    let branchesArray = new FormArray([
      new FormGroup({
        code: new FormControl(''),
        branchName: new FormControl(''),
        cityId: new FormControl(''),
        regionId: new FormControl(''),
        address: new FormControl(''),
        phone: new FormControl(null),
        fax: new FormControl(null),
        mobile: new FormControl(null),
        email: new FormControl(''),
        officials: officialsArray
      })
    ]);

    let filesArray = new FormArray([
      new FormGroup({
        attachmentTypeId: new FormControl(''),
        file: new FormControl('')
      })
    ]);

    let customerTransactionsArray = new FormArray([
      new FormGroup({
        branchId: new FormControl('')
      })
    ]);

    if (this.customer) {
      image = this.customer.image;
      code = this.customer.code;
      customerNameAr = this.customer.customerNameAr;
      customerNameEn = this.customer.customerNameEn;
      salesPersonId = this.customer.salesPersonId;
      exchangeRate = this.customer.exchangeRate;
      pricingPolicyId = this.customer.pricingPolicyId;
      isActive = this.customer.isActive;
      customersGroupId = this.customer.customersGroupId;
      currencyId = this.customer.currencyId;
      customersClassificationId = this.customer.customersClassificationId;
      if (this.customer.identity) {
        nationalityId = this.customer.identity.nationalityId;
        identityNumber = this.customer.identity.identityNumber;
        identitySource = this.customer.identity.identitySource;
        releaseGregorianDate = new Date(this.customer.identity.releaseGregorianDate + 'UTC');
      }
      if (this.customer.officialDocuments) {
        if (this.customer.officialDocuments.tax) {
          taxRegistrationNumber = this.customer.officialDocuments.tax
            .taxRegistrationNumber;
          taxCardNumber = this.customer.officialDocuments.tax.taxCardNumber;
          missionCode = this.customer.officialDocuments.tax.missionCode;
          missionName = this.customer.officialDocuments.tax.missionName;
          businessAddress = this.customer.officialDocuments.tax.businessAddress;
          fileNumber = this.customer.officialDocuments.tax.fileNumber;
          businessName = this.customer.officialDocuments.tax.businessName;
          financierName = this.customer.officialDocuments.tax.financierName;
        }
        if (this.customer.officialDocuments.commercialRegistration) {
          commercialName = this.customer.officialDocuments.commercialRegistration
            .commercialName;
          commercialTrait = this.customer.officialDocuments.commercialRegistration
            .commercialTrait;
          commercialBrand = this.customer.officialDocuments.commercialRegistration
            .commercialBrand;
          traderName = this.customer.officialDocuments.commercialRegistration
            .traderName;
          commercialRegistrationNationalityId = this.customer.officialDocuments
            .commercialRegistration.nationalityId;
          financialSystem = this.customer.officialDocuments.commercialRegistration
            .financialSystem;
          commercialEligibility = this.customer.officialDocuments.commercialRegistration
            .commercialEligibility;
          tradeType = this.customer.officialDocuments.commercialRegistration.tradeType;
          startBusinessGregorianDate = new Date(this.customer.officialDocuments
            .commercialRegistration.startBusinessGregorianDate + 'UTC');
        }
      }
      if (this.customer.openingBalances.length) {
        openingBalancesArray = new FormArray([]);
        for (const balanceItem of this.customer.openingBalances) {
          openingBalancesArray.push(
            new FormGroup({
              balance: new FormControl(balanceItem.balance),
              netBalance: new FormControl(balanceItem.netBalance),
              currencyId: new FormControl(
                balanceItem.currencyId
              ),
              exchangeRate: new FormControl(balanceItem.exchangeRate),
              accountSide: new FormControl(balanceItem.accountSide),
              dueGregorianDate: new FormControl(balanceItem.dueGregorianDate && new Date(balanceItem.dueGregorianDate + 'UTC')),
              dueHijriDate: new FormControl(balanceItem.dueHijriDate),
              referenceNumber: new FormControl(balanceItem.referenceNumber),
              notes: new FormControl(balanceItem.notes)
            })
          );
        }
      }

      salesDuePeriod = this.customer.accountingInfo.salesDuePeriod;
      glAccountId = this.customer.accountingInfo.glAccountId;
      netOpeningBalance = this.customer.accountingInfo.netOpeningBalance;
      creditLimit = this.customer.accountingInfo.creditLimit;
      cannotPassCreditLimit = this.customer.accountingInfo
        .cannotPassCreditLimit;
      salesPersonCommission = this.customer.accountingInfo
        .salesPersonCommission;

      if (this.customer.accountingInfo.banks.length) {
        banksArray = new FormArray([]);
        for (const bankItem of this.customer.accountingInfo.banks) {
          banksArray.push(
            new FormGroup({
              bankId: new FormControl(bankItem.bankId),
              bankAccountNumber: new FormControl(bankItem.bankAccountNumber)
            })
          );
        }
      }

      if (this.customer.contact) {
        if (this.customer.contact.address) {
          cityId = this.customer.contact.address.cityId;
          regionId = this.customer.contact.address.regionId;
          address = this.customer.contact.address.address;
        }
        if (this.customer.contact.contact) {
          phone = this.customer.contact.contact.phone;
          internalNumber = this.customer.contact.contact.internalNumber;
          fax = this.customer.contact.contact.fax;
          mobile = this.customer.contact.contact.mobile;
        }
        if (this.customer.contact.mail) {
          postalCode = this.customer.contact.mail.postalCode;
          postalBox = this.customer.contact.mail.postalBox;
        }
        if (this.customer.contact.socialAccounts) {
          website = this.customer.contact.socialAccounts.website;
        }
      }

      if (this.customer.branches.length) {
        branchesArray = new FormArray([]);
        for (const branchItem of this.customer.branches) {
          officialsArray = new FormArray([]);
          for (const officialItem of branchItem.officials) {
            officialsArray.push(
              new FormGroup({
                name: new FormControl(officialItem.name),
                job: new FormControl(officialItem.job),
                phone: new FormControl(officialItem.phone),
                internalNumber: new FormControl(officialItem.internalNumber),
                mobile: new FormControl(officialItem.mobile),
                email: new FormControl(officialItem.email)
              })
            );
          }
          branchesArray.push(
            new FormGroup({
              code: new FormControl(branchItem.code),
              branchName: new FormControl(branchItem.branchName),
              cityId: new FormControl(branchItem.cityId),
              regionId: new FormControl(branchItem.regionId),
              address: new FormControl(branchItem.address),
              phone: new FormControl(branchItem.phone),
              fax: new FormControl(branchItem.fax),
              mobile: new FormControl(branchItem.mobile),
              email: new FormControl(branchItem.email),
              officials: officialsArray
            })
          );
        }
      }

      if (this.customer.files.length) {
        filesArray = new FormArray([]);
        for (const fileItem of this.customer.files) {
          filesArray = new FormArray([
            new FormGroup({
              attachmentTypeId: new FormControl(fileItem.attachmentTypeId),
              file: new FormControl(fileItem.file)
            })
          ]);
        }
      }

      if (this.customer.customerTransactions.length) {
        customerTransactionsArray = new FormArray([]);
        for (const customerTransactionItem of this.customer
          .customerTransactions) {
          customerTransactionsArray.push(
            new FormGroup({
              branchId: new FormControl(customerTransactionItem.branchId)
            })
          );
        }
      }
    }
    this.customerForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      customerNameAr: new FormControl(customerNameAr, Validators.required),
      customerNameEn: new FormControl(customerNameEn),
      pricingPolicyId: new FormControl(pricingPolicyId),
      customersGroupId: new FormControl(customersGroupId),
      currencyId: new FormControl(currencyId, Validators.required),
      exchangeRate: new FormControl(exchangeRate),
      salesPersonId: new FormControl(salesPersonId),
      customersClassificationId: new FormControl(customersClassificationId),
      identity: new FormGroup({
        nationalityId: new FormControl(nationalityId),
        identityNumber: new FormControl(identityNumber),
        identitySource: new FormControl(identitySource),
        releaseGregorianDate: new FormControl(releaseGregorianDate),
        releaseHijriDate: new FormControl(releaseHijriDate)
      }),
      officialDocuments: new FormGroup({
        tax: new FormGroup({
          taxRegistrationNumber: new FormControl(taxRegistrationNumber),
          taxCardNumber: new FormControl(taxCardNumber),
          missionCode: new FormControl(missionCode),
          missionName: new FormControl(missionName),
          businessAddress: new FormControl(businessAddress),
          fileNumber: new FormControl(fileNumber),
          businessName: new FormControl(businessName),
          financierName: new FormControl(financierName)
        }),
        commercialRegistration: new FormGroup({
          commercialName: new FormControl(commercialName),
          commercialTrait: new FormControl(commercialTrait),
          commercialBrand: new FormControl(commercialBrand),
          traderName: new FormControl(traderName),
          nationalityId: new FormControl(commercialRegistrationNationalityId),
          financialSystem: new FormControl(financialSystem),
          commercialEligibility: new FormControl(commercialEligibility),
          tradeType: new FormControl(tradeType),
          startBusinessGregorianDate: new FormControl(
            startBusinessGregorianDate
          ),
          startBusinessHijriDate: new FormControl(startBusinessHijriDate)
        })
      }),
      openingBalances: openingBalancesArray,
      accountingInfo: new FormGroup({
        salesDuePeriod: new FormControl(salesDuePeriod),
        glAccountId: new FormControl(glAccountId),
        netOpeningBalance: new FormControl(netOpeningBalance),
        creditLimit: new FormControl(creditLimit),
        cannotPassCreditLimit: new FormControl(cannotPassCreditLimit),
        salesPersonCommission: new FormControl(salesPersonCommission),
        banks: banksArray
      }),
      contact: new FormGroup({
        address: new FormGroup({
          cityId: new FormControl(cityId),
          regionId: new FormControl(regionId),
          address: new FormControl(address)
        }),
        contact: new FormGroup({
          phone: new FormControl(phone),
          internalNumber: new FormControl(internalNumber),
          mobile: new FormControl(mobile),
          fax: new FormControl(fax)
        }),
        mail: new FormGroup({
          postalCode: new FormControl(postalCode),
          postalBox: new FormControl(postalBox)
        }),
        socialAccounts: new FormGroup({
          website: new FormControl(website)
        })
      }),
      branches: branchesArray,
      customerTransactions: customerTransactionsArray,
      files: filesArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
