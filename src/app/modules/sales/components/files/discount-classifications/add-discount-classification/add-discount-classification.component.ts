import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, discountClassificationsApi, itemsGroupsApi, customersGroupsApi, customersApi, storesItemsApi, salesInvoiceApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IDicountClassification } from '../../../../interfaces/IDicountClassification';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IItemsGroup } from 'src/app/modules/inventory/interfaces/IItemsGroup';
import { ICustomersGroup } from '../../../../interfaces/ICustomersGroup';
import { DataService } from 'src/app/common/services/shared/data.service';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { ISalesInvoice } from 'src/app/modules/sales/interfaces/ISalesInvoice';

@Component({
  selector: 'app-add-discount-classification',
  templateUrl: './add-discount-classification.component.html',
  styleUrls: ['./add-discount-classification.component.scss']
})

export class AddDiscountClassificationComponent implements OnInit, OnDestroy {
  discountClassificationForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  discountClassification: IDicountClassification;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  sourceItems: any[] = [];
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Items Groups
  itemsGroups: IItemsGroup[] = [];
  itemsGroupsInputFocused: boolean;
  hasMoreItemsGroups: boolean;
  itemsGroupsCount: number;
  selectedItemsGroupsPage = 1;
  itemsGroupsPagesNo: number;
  noItemsGroups: boolean;

  // Customer Groups
  customersGroups: ICustomersGroup[] = [];
  customersGroupsInputFocused: boolean;
  hasMoreCustomersGroups: boolean;
  customersGroupsCount: number;
  selectedCustomersGroupsPage = 1;
  customersGroupsPagesNo: number;
  noCustomersGroups: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Sales Invoices
  salesInvoices: ISalesInvoice[] = [];
  salesInvoicesInputFocused: boolean;
  hasMoreSalesInvoices: boolean;
  salesInvoicesCount: number;
  selectedSalesInvoicesPage = 1;
  salesInvoicesPagesNo: number;
  noSalesInvoices: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(discountClassificationsApi, null, null, id)
              .subscribe((discountClassification: IDicountClassification) => {
                this.discountClassification = discountClassification;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.discountClassificationForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(itemsGroupsApi, 1).subscribe((res: IDataRes) => {
        this.itemsGroupsPagesNo = res.pages;
        this.itemsGroupsCount = res.count;
        if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
          this.hasMoreItemsGroups = true;
        }
        this.itemsGroups.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersGroupsApi, 1).subscribe((res: IDataRes) => {
        this.customersGroupsPagesNo = res.pages;
        this.customersGroupsCount = res.count;
        if (this.customersGroupsPagesNo > this.selectedCustomersGroupsPage) {
          this.hasMoreCustomersGroups = true;
        }
        this.customersGroups.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    const searchValues = {
      salesInvoiceStatus: 'posted'
    };

    this.subscriptions.push(
      this.data.get(salesInvoiceApi, null, searchValues).subscribe((res: IDataRes) => {
        this.salesInvoicesPagesNo = res.pages;
        this.salesInvoicesCount = res.count;
        this.salesInvoices.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

  }

  searchItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      itemsGroupNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(itemsGroupsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noItemsGroups = true;
          } else {
            this.noItemsGroups = false;
            for (const item of res.results) {
              if (this.itemsGroups.length) {
                const uniqueItemsGroups = this.itemsGroups.filter(x => x._id !== item._id);
                this.itemsGroups = uniqueItemsGroups;
              }
              this.itemsGroups.push(item);
            }
          }
          this.itemsGroups = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreItemsGroups() {
    this.selectedItemsGroupsPage = this.selectedItemsGroupsPage + 1;
    this.subscriptions.push(
      this.data.get(itemsGroupsApi, this.selectedItemsGroupsPage).subscribe((res: IDataRes) => {
        if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
          this.hasMoreItemsGroups = true;
        } else {
          this.hasMoreItemsGroups = false;
        }
        for (const item of res.results) {
          if (this.itemsGroups.length) {
            const uniqueItemsGroups = this.itemsGroups.filter(x => x._id !== item._id);
            this.itemsGroups = uniqueItemsGroups;
          }
          this.itemsGroups.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCustomersGroups(event) {
    const searchValue = event;
    const searchQuery = {
      customersGroupNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersGroupsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomersGroups = true;
          } else {
            this.noCustomersGroups = false;
            for (const item of res.results) {
              if (this.customersGroups.length) {
                const uniqueCustomersGroups = this.customersGroups.filter(x => x._id !== item._id);
                this.customersGroups = uniqueCustomersGroups;
              }
              this.customersGroups.push(item);
            }
          }
          this.customersGroups = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomersGroups() {
    this.selectedCustomersGroupsPage = this.selectedCustomersGroupsPage + 1;
    this.subscriptions.push(
      this.data.get(customersGroupsApi, this.selectedCustomersGroupsPage).subscribe((res: IDataRes) => {
        if (this.customersGroupsPagesNo > this.selectedCustomersGroupsPage) {
          this.hasMoreCustomersGroups = true;
        } else {
          this.hasMoreCustomersGroups = false;
        }
        for (const item of res.results) {
          if (this.customersGroups.length) {
            const uniqueCustomersGroups = this.customersGroups.filter(x => x._id !== item._id);
            this.customersGroups = uniqueCustomersGroups;
          }
          this.customersGroups.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.discountClassificationForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.discountClassification.image = '';
    this.discountClassificationForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.discountClassificationForm.controls;
  }

  get getDiscountClassificationDetailsListArray() {
    return this.discountClassificationForm.get('discountClassificationDetailsList') as FormArray;
  }

  addDiscount() {
    const control = this.discountClassificationForm.get('discountClassificationDetailsList') as FormArray;
    control.push(
      new FormGroup({
        fromPercentage: new FormControl(null),
        toPercentage: new FormControl(null),
        applyOn: new FormControl('itemsGroup'),
        items: new FormControl(null),
      })
    );
  }

  deleteDiscount(index) {
    const control = this.discountClassificationForm.get('discountClassificationDetailsList') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.discountClassification) {
      if (this.discountClassificationForm.valid) {
        const newdiscountClassification = {
          _id: this.discountClassification._id,
          ...this.generalService.checkEmptyFields(this.discountClassificationForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(discountClassificationsApi, newdiscountClassification).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/sales/discountClassifications']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.discountClassificationForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.discountClassificationForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(discountClassificationsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.discountClassificationForm.reset();
            this.discountClassificationForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let discountClassificationNameAr = '';
    let discountClassificationNameEn = '';
    const fromPercentage = '';
    const toPercentage = '';
    const applyOn = 'itemsGroup';
    let items = [];
    let discountClassificationDetailsListArray = new FormArray([
      new FormGroup({
        fromPercentage: new FormControl(fromPercentage),
        toPercentage: new FormControl(toPercentage),
        applyOn: new FormControl(applyOn),
        items: new FormControl(items),
      })
    ]);
    let isActive = true;

    if (this.discountClassification) {
      image = this.discountClassification.image;
      code = this.discountClassification.code;
      discountClassificationNameAr = this.discountClassification.discountClassificationNameAr;
      discountClassificationNameEn = this.discountClassification.discountClassificationNameEn;
      isActive = this.discountClassification.isActive;
      for (const control of this.discountClassification.discountClassificationDetailsList) {
        discountClassificationDetailsListArray = new FormArray([]);
        items = [...control.items];
        discountClassificationDetailsListArray.push(
          new FormGroup({
            fromPercentage: new FormControl(control.fromPercentage),
            toPercentage: new FormControl(control.toPercentage),
            applyOn: new FormControl(control.applyOn),
            items: new FormControl(items),
          })
        );
      }
    }
    this.discountClassificationForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      discountClassificationNameAr: new FormControl(discountClassificationNameAr, Validators.required),
      discountClassificationNameEn: new FormControl(discountClassificationNameEn),
      discountClassificationDetailsList: discountClassificationDetailsListArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
