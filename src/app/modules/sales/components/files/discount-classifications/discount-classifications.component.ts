import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, discountClassificationsApi } from 'src/app/common/constants/api.constants';
import { pagingEnd, pagingStart } from 'src/app/common/constants/general.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IDicountClassification } from '../../../interfaces/IDicountClassification';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-discount-classifications',
  templateUrl: './discount-classifications.component.html',
  styleUrls: ['./discount-classifications.component.scss']
})

export class DiscountClassificationsComponent implements OnInit, OnDestroy {
  discountClassifications: IDicountClassification[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService
  ) { }

  ngOnInit() {
    this.getDiscountClassificationsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(discountClassificationsApi, pageNumber).subscribe((res: IDataRes) => {
      this.discountClassifications = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(discountClassificationsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.discountClassifications = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getDiscountClassificationsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(discountClassificationsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.discountClassifications = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      discountClassificationNameAr: new FormControl(''),
      discountClassificationNameEn: new FormControl('')
    });
  }

  deleteModal(discountClassification: IDicountClassification) {
    const initialState = {
      code: discountClassification.code,
      nameAr: discountClassification.discountClassificationNameAr,
      nameEn: discountClassification.discountClassificationNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(discountClassification._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(discountClassificationsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.discountClassifications = this.generalService.removeItem(this.discountClassifications, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getDiscountClassificationsFirstPage() {
    this.subscriptions.push(
      this.data.get(discountClassificationsApi, 1).subscribe((res: IDataRes) => {
        this.discountClassifications = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
