import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, pricingPoliciesApi, storesItemsApi, itemsPricesApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsPrice } from '../../../../interfaces/IItemsPrice';
import { IPricingPolicy } from '../../../../interfaces/IPricingPolicy';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-items-price',
  templateUrl: './add-items-price.component.html',
  styleUrls: ['./add-items-price.component.scss']
})

export class AddItemsPriceComponent implements OnInit, OnDestroy {
  itemsPriceForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  itemsPrice: IItemsPrice;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Pricing Policies
  pricingPolicies: IPricingPolicy[] = [];
  pricingPoliciesInputFocused: boolean;
  hasMorePricingPolicies: boolean;
  pricingPoliciesCount: number;
  selectedPricingPoliciesPage = 1;
  pricingPoliciesPagesNo: number;
  noPricingPolicies: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(itemsPricesApi, null, null, id)
              .subscribe((itemsPrice: IItemsPrice) => {
                this.itemsPrice = itemsPrice;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.itemsPriceForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
    this.subscriptions.push(
      this.data.get(pricingPoliciesApi, 1).subscribe((res: IDataRes) => {
        this.pricingPoliciesPagesNo = res.pages;
        this.pricingPoliciesCount = res.count;
        if (this.pricingPoliciesPagesNo > this.selectedPricingPoliciesPage) {
          this.hasMorePricingPolicies = true;
        }
        this.pricingPolicies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    const searchValues = {
      isActive: true
    };

    this.subscriptions.push(
      this.data.get(storesItemsApi, null, searchValues).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        for (const item of this.storeItems) {
          this.getItemsPriceDetailsListArray.push(
            new FormGroup({
              itemId: new FormControl(item._id),
              itemPrice: new FormControl(null),
              profitCostPercentage: new FormControl(null)
            })
          );
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchPricingPolicies(event) {
    const searchValue = event;
    const searchQuery = {
      pricingPolicyNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(pricingPoliciesApi, null, searchQuery).subscribe(
          (res: IDataRes) => {
            if (!res.results.length) {
              this.noPricingPolicies = true;
            } else {
              this.noPricingPolicies = false;
              for (const item of res.results) {
                if (this.pricingPolicies.length) {
                  const uniquepricingPolicies = this.pricingPolicies.filter(
                    x => x._id !== item._id
                  );
                  this.pricingPolicies = uniquepricingPolicies;
                }
                this.pricingPolicies.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          }
        )
      );
    }
  }

  loadMorePricingPolicies() {
    this.selectedPricingPoliciesPage = this.selectedPricingPoliciesPage + 1;
    this.subscriptions.push(
      this.data.get(
        pricingPoliciesApi, this.selectedPricingPoliciesPage
      ).subscribe((res: IDataRes) => {
        if (this.pricingPoliciesPagesNo > this.selectedPricingPoliciesPage) {
          this.hasMorePricingPolicies = true;
        } else {
          this.hasMorePricingPolicies = false;
        }
        for (const item of res.results) {
          if (this.pricingPolicies.length) {
            const uniquePricingPolicies = this.pricingPolicies.filter(
              x => x._id !== item._id
            );
            this.pricingPolicies = uniquePricingPolicies;
          }
          this.pricingPolicies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.itemsPriceForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.itemsPrice.image = '';
    this.itemsPriceForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.itemsPriceForm.controls;
  }

  get getItemsPriceDetailsListArray() {
    return this.itemsPriceForm.get('itemsPriceDetailsList') as FormArray;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.itemsPrice) {
      if (this.itemsPriceForm.valid) {
        const newitemsPrice = {
          _id: this.itemsPrice._id,
          ...this.generalService.checkEmptyFields(this.itemsPriceForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(itemsPricesApi, newitemsPrice).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/sales/itemsPrices']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.itemsPriceForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.itemsPriceForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(itemsPricesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.itemsPriceForm.reset();
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let pricingPolicyId = '';
    let automaticCalculation = 'without';
    let automaticCalculationPercentage = null;
    let isActive = true;
    const itemsPriceDetailsListArray = new FormArray([]);

    if (this.itemsPrice) {
      image = this.itemsPrice.image;
      pricingPolicyId = this.itemsPrice.pricingPolicyId;
      automaticCalculation = this.itemsPrice.automaticCalculation;
      automaticCalculationPercentage = this.itemsPrice.automaticCalculationPercentage;
      isActive = this.itemsPrice.isActive;
    }
    this.itemsPriceForm = new FormGroup({
      image: new FormControl(image),
      pricingPolicyId: new FormControl(pricingPolicyId, Validators.required),
      automaticCalculation: new FormControl(automaticCalculation),
      automaticCalculationPercentage: new FormControl(automaticCalculationPercentage),
      isActive: new FormControl(isActive, Validators.required),
      itemsPriceDetailsList: itemsPriceDetailsListArray
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
