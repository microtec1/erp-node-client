import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, itemsPricesApi } from 'src/app/common/constants/api.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IItemsPrice } from '../../../interfaces/IItemsPrice';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-items-prices',
  templateUrl: './items-prices.component.html',
  styleUrls: ['./items-prices.component.scss']
})

export class ItemsPricesComponent implements OnInit, OnDestroy {
  itemsPrices: IItemsPrice[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService
  ) { }

  ngOnInit() {
    this.getItemsPricesFirstPage();
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  deleteModal(itemsPrice: IItemsPrice) {
    const initialState = {
      code: itemsPrice.pricingPolicyId
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(itemsPrice._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(itemsPricesApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.itemsPrices = this.generalService.removeItem(this.itemsPrices, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getItemsPricesFirstPage() {
    this.subscriptions.push(
      this.data.get(itemsPricesApi, 1).subscribe((res: IDataRes) => {
        this.itemsPrices = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
