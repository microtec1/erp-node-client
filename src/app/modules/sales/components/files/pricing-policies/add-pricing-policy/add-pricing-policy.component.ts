import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, pricingPoliciesApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IPricingPolicy } from '../../../../interfaces/IPricingPolicy';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-pricing-policy',
  templateUrl: './add-pricing-policy.component.html',
  styleUrls: ['./add-pricing-policy.component.scss']
})

export class AddPricingPolicyComponent implements OnInit, OnDestroy {
  pricingPolicyForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  pricingPolicy: IPricingPolicy;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(pricingPoliciesApi, null, null, id)
              .subscribe((pricingPolicy: IPricingPolicy) => {
                this.pricingPolicy = pricingPolicy;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.pricingPolicyForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.pricingPolicyForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.pricingPolicy.image = '';
    this.pricingPolicyForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.pricingPolicyForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.pricingPolicy) {
      if (this.pricingPolicyForm.valid) {
        const newpricingPolicy = {
          _id: this.pricingPolicy._id,
          ...this.generalService.checkEmptyFields(this.pricingPolicyForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(pricingPoliciesApi, newpricingPolicy).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/sales/pricingPolicies']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.pricingPolicyForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.pricingPolicyForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(pricingPoliciesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.pricingPolicyForm.reset();
            this.pricingPolicyForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let pricingPolicyNameAr = '';
    let pricingPolicyNameEn = '';
    let isActive = true;

    if (this.pricingPolicy) {
      image = this.pricingPolicy.image;
      code = this.pricingPolicy.code;
      pricingPolicyNameAr = this.pricingPolicy.pricingPolicyNameAr;
      pricingPolicyNameEn = this.pricingPolicy.pricingPolicyNameEn;
      isActive = this.pricingPolicy.isActive;
    }
    this.pricingPolicyForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      pricingPolicyNameAr: new FormControl(pricingPolicyNameAr, Validators.required),
      pricingPolicyNameEn: new FormControl(pricingPolicyNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
