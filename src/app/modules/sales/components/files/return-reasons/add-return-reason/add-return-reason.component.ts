import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, RouterStateSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, returnReasonsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { IReturnReason } from '../../../../interfaces/IReturnReason';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-add-return-reason',
  templateUrl: './add-return-reason.component.html',
  styleUrls: ['./add-return-reason.component.scss']
})

export class AddReturnReasonComponent implements OnInit, OnDestroy {
  returnReasonForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  returnReason: IReturnReason;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(returnReasonsApi, null, null, id)
              .subscribe((returnReason: IReturnReason) => {
                this.returnReason = returnReason;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.returnReasonForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.returnReasonForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.returnReason.image = '';
    this.returnReasonForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.returnReasonForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.returnReason) {
      if (this.returnReasonForm.valid) {
        const newReturnReason = {
          _id: this.returnReason._id,
          ...this.generalService.checkEmptyFields(this.returnReasonForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(returnReasonsApi, newReturnReason).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/sales/returnReasons']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.returnReasonForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.returnReasonForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(returnReasonsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.returnReasonForm.reset();
            this.returnReasonForm.patchValue({
              isActive: true
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let returnReasonNameAr = '';
    let returnReasonNameEn = '';
    let isActive = true;

    if (this.returnReason) {
      image = this.returnReason.image;
      code = this.returnReason.code;
      returnReasonNameAr = this.returnReason.returnReasonNameAr;
      returnReasonNameEn = this.returnReason.returnReasonNameEn;
      isActive = this.returnReason.isActive;
    }
    this.returnReasonForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      returnReasonNameAr: new FormControl(returnReasonNameAr, Validators.required),
      returnReasonNameEn: new FormControl(returnReasonNameEn),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
