import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, returnReasonsApi } from 'src/app/common/constants/api.constants';
import { pagingEnd, pagingStart } from 'src/app/common/constants/general.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { IReturnReason } from '../../../interfaces/IReturnReason';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-return-reasons',
  templateUrl: './return-reasons.component.html',
  styleUrls: ['./return-reasons.component.scss']
})

export class ReturnReasonsComponent implements OnInit, OnDestroy {
  returnReasons: IReturnReason[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService
  ) { }

  ngOnInit() {
    this.getReturnReasonsFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(returnReasonsApi, pageNumber).subscribe((res: IDataRes) => {
      this.returnReasons = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(returnReasonsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.returnReasons = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getReturnReasonsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(returnReasonsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.returnReasons = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      returnReasonNameAr: new FormControl(''),
      returnReasonNameEn: new FormControl('')
    });
  }

  deleteModal(returnReason: IReturnReason) {
    const initialState = {
      code: returnReason.code,
      nameAr: returnReason.returnReasonNameAr,
      nameEn: returnReason.returnReasonNameEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(returnReason._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(returnReasonsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.returnReasons = this.generalService.removeItem(this.returnReasons, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getReturnReasonsFirstPage() {
    this.subscriptions.push(
      this.data.get(returnReasonsApi, 1).subscribe((res: IDataRes) => {
        this.returnReasons = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
