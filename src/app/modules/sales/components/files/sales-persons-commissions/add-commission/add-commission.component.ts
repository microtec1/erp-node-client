import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, salespersonsCommissionsApi, itemsGroupsApi, storesItemsApi, employeesApi, pricingPoliciesApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { ISalesPersonCommission } from '../../../../interfaces/ISalesPersonCommission';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { IItemsGroup } from 'src/app/modules/inventory/interfaces/IItemsGroup';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { DataService } from 'src/app/common/services/shared/data.service';
import { IPricingPolicy } from 'src/app/modules/sales/interfaces/IPricingPolicy';

@Component({
  selector: 'app-add-commission',
  templateUrl: './add-commission.component.html',
  styleUrls: ['./add-commission.component.scss']
})

export class AddCommissionComponent implements OnInit, OnDestroy {
  commissionForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  commission: ISalesPersonCommission;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;


  // Sales Representatives
  salesRepresentatives: IEmployee[] = [];
  salesRepresentativesInputFocused: boolean;
  salesRepresentativesCount: number;
  noSalesRepresentatives: boolean;

  // Items Groups
  itemsGroups: IItemsGroup[] = [];
  itemsGroupsInputFocused: boolean;
  hasMoreItemsGroups: boolean;
  itemsGroupsCount: number;
  selectedItemsGroupsPage = 1;
  itemsGroupsPagesNo: number;
  noItemsGroups: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Pricing Policies
  pricingPolicies: IPricingPolicy[] = [];
  pricingPoliciesInputFocused: boolean;
  hasMorePricingPolicies: boolean;
  pricingPoliciesCount: number;
  selectedPricingPoliciesPage = 1;
  pricingPoliciesPagesNo: number;
  noPricingPolicies: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(salespersonsCommissionsApi, null, null, id)
              .subscribe((commission: ISalesPersonCommission) => {
                this.commission = commission;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.commissionForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(itemsGroupsApi, 1).subscribe((res: IDataRes) => {
        this.itemsGroupsPagesNo = res.pages;
        this.itemsGroupsCount = res.count;
        if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
          this.hasMoreItemsGroups = true;
        }
        this.itemsGroups.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    const searchQuery = {
      salesRepresentative: true,
      companyId
    };
    this.subscriptions.push(
      this.data.get(employeesApi, null, searchQuery).subscribe((res: IDataRes) => {
        if (!res.results.length) {
          this.noSalesRepresentatives = true;
        } else {
          this.noSalesRepresentatives = false;
          this.salesRepresentativesCount = res.results.length;
          for (const item of res.results) {
            if (this.salesRepresentatives.length) {
              const uniqueSalesRepresentatives = this.salesRepresentatives.filter(x => x._id !== item._id);
              this.salesRepresentatives = uniqueSalesRepresentatives;
            }
            this.salesRepresentatives.push(item);
          }
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(pricingPoliciesApi, 1).subscribe((res: IDataRes) => {
        this.pricingPoliciesPagesNo = res.pages;
        this.pricingPoliciesCount = res.count;
        if (this.pricingPoliciesPagesNo > this.selectedPricingPoliciesPage) {
          this.hasMorePricingPolicies = true;
        }
        this.pricingPolicies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchItemsGroups(event) {
    const searchValue = event;
    const searchQuery = {
      itemsGroupNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(itemsGroupsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noItemsGroups = true;
          } else {
            this.noItemsGroups = false;
            for (const item of res.results) {
              if (this.itemsGroups.length) {
                const uniqueItemsGroups = this.itemsGroups.filter(x => x._id !== item._id);
                this.itemsGroups = uniqueItemsGroups;
              }
              this.itemsGroups.push(item);
            }
          }
          this.itemsGroups = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreItemsGroups() {
    this.selectedItemsGroupsPage = this.selectedItemsGroupsPage + 1;
    this.subscriptions.push(
      this.data.get(itemsGroupsApi, this.selectedItemsGroupsPage).subscribe((res: IDataRes) => {
        if (this.itemsGroupsPagesNo > this.selectedItemsGroupsPage) {
          this.hasMoreItemsGroups = true;
        } else {
          this.hasMoreItemsGroups = false;
        }
        for (const item of res.results) {
          if (this.itemsGroups.length) {
            const uniqueItemsGroups = this.itemsGroups.filter(x => x._id !== item._id);
            this.itemsGroups = uniqueItemsGroups;
          }
          this.itemsGroups.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(storesItemsApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noStoreItems = true;
          } else {
            this.noStoreItems = false;
            for (const item of res.results) {
              if (this.storeItems.length) {
                const uniqueStoreItems = this.storeItems.filter(x => x._id !== item._id);
                this.storeItems = uniqueStoreItems;
              }
              this.storeItems.push(item);
            }
          }
          this.storeItems = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMorestoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data.get(storesItemsApi, this.selectedStoreItemsPage).subscribe((res: IDataRes) => {
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        } else {
          this.hasMoreStoreItems = false;
        }
        for (const item of res.results) {
          if (this.storeItems.length) {
            const uniqueStoreItems = this.storeItems.filter(x => x._id !== item._id);
            this.storeItems = uniqueStoreItems;
          }
          this.storeItems.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchPricingPolicies(event) {
    const searchValue = event;
    const searchQuery = {
      pricingPolicyNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(pricingPoliciesApi, null, searchQuery).subscribe(
          (res: IDataRes) => {
            if (!res.results.length) {
              this.noPricingPolicies = true;
            } else {
              this.noPricingPolicies = false;
              for (const item of res.results) {
                if (this.pricingPolicies.length) {
                  const uniquepricingPolicies = this.pricingPolicies.filter(
                    x => x._id !== item._id
                  );
                  this.pricingPolicies = uniquepricingPolicies;
                }
                this.pricingPolicies.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          }
        )
      );
    }
  }

  loadMorePricingPolicies() {
    this.selectedPricingPoliciesPage = this.selectedPricingPoliciesPage + 1;
    this.subscriptions.push(
      this.data.get(
        pricingPoliciesApi, this.selectedPricingPoliciesPage
      ).subscribe((res: IDataRes) => {
        if (this.pricingPoliciesPagesNo > this.selectedPricingPoliciesPage) {
          this.hasMorePricingPolicies = true;
        } else {
          this.hasMorePricingPolicies = false;
        }
        for (const item of res.results) {
          if (this.pricingPolicies.length) {
            const uniquePricingPolicies = this.pricingPolicies.filter(
              x => x._id !== item._id
            );
            this.pricingPolicies = uniquePricingPolicies;
          }
          this.pricingPolicies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.commissionForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.commission.image = '';
    this.commissionForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.commissionForm.controls;
  }

  get getCommissionsArray() {
    return this.commissionForm.get('commissions') as FormArray;
  }

  addCommission() {
    const control = this.commissionForm.get('commissions') as FormArray;
    control.push(
      new FormGroup({
        commissionType: new FormControl(''),
        sliceType: new FormControl(''),
        from: new FormControl(null),
        to: new FormControl(null),
        policyType: new FormControl(''),
        ratio: new FormControl(null),
        appliedOn: new FormControl('all'),
        items: new FormControl(null),
        itemsGroups: new FormControl(null),
      })
    );
  }

  deleteCommission(index) {
    const control = this.commissionForm.get('commissions') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    for (const item of this.commissionForm.value.commissions) {
      if (item.appliedOn === 'items') {
        item.itemsGroups = [];
        item.items = [];
        if (item.itemsControl) {
          for (const innerItem of item.itemsControl) {
            item.items.push(
              { itemId: innerItem }
            );
          }
        }
        delete item.itemsControl;
        delete item.itemsGroupsControl;
      } else {
        item.itemsGroups = [];
        item.items = [];
        if (item.itemsGroupsControl) {
          for (const innerItem of item.itemsGroupsControl) {
            item.itemsGroups.push(
              { itemsGroupId: innerItem }
            );
          }
        }
        delete item.itemsControl;
        delete item.itemsGroupsControl;
      }
    }

    if (this.commission) {
      if (this.commissionForm.valid) {
        const newcommission = {
          _id: this.commission._id,
          ...this.generalService.checkEmptyFields(this.commissionForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(salespersonsCommissionsApi, newcommission).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/sales/salesPersonsCommissions']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.commissionForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.commissionForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(salespersonsCommissionsApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.commissionForm.reset();
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let salespersonId = '';

    let commissionsArray = new FormArray([
      new FormGroup({
        commissionType: new FormControl(''),
        sliceType: new FormControl(''),
        from: new FormControl(null),
        to: new FormControl(null),
        policyType: new FormControl(''),
        ratio: new FormControl(null),
        appliedOn: new FormControl('all'),
        itemsControl: new FormControl(),
        itemsGroupsControl: new FormControl(),
        items: new FormArray([]),
        itemsGroups: new FormArray([]),
      })
    ]);
    let isActive = true;

    if (this.commission) {
      image = this.commission.image;
      salespersonId = this.commission.salespersonId;
      isActive = this.commission.isActive;
      commissionsArray = new FormArray([]);
      for (const item of this.commission.commissions) {
        const itemsControlArray = [];
        const itemsGroupsArray = [];
        if (item.appliedOn === 'items') {
          item.itemsControl = [];
          for (const value of item.items) {
            itemsControlArray.push(
              value.itemId
            );
          }
        } else {
          item.itemsGroupsControl = [];
          for (const value of item.itemsGroups) {
            itemsGroupsArray.push(
              value.itemsGroupId
            );
          }
        }
        commissionsArray.push(
          new FormGroup({
            commissionType: new FormControl(item.commissionType),
            sliceType: new FormControl(item.sliceType),
            from: new FormControl(item.from),
            to: new FormControl(item.to),
            policyType: new FormControl(item.policyType),
            ratio: new FormControl(item.ratio),
            appliedOn: new FormControl(item.appliedOn),
            itemsControl: new FormControl(itemsControlArray),
            itemsGroupsControl: new FormControl(itemsGroupsArray),
            items: new FormArray([]),
            itemsGroups: new FormArray([]),
          })
        );
      }
    }

    this.commissionForm = new FormGroup({
      image: new FormControl(image),
      salespersonId: new FormControl(salespersonId),
      commissions: commissionsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
