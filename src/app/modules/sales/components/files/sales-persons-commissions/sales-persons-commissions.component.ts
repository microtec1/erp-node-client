import { Component, OnInit, OnDestroy } from '@angular/core';
import { baseUrl, salespersonsCommissionsApi } from 'src/app/common/constants/api.constants';
import { pagingEnd, pagingStart } from 'src/app/common/constants/general.constants';
import { FormGroup, FormControl } from '@angular/forms';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';
import { ISalesPersonCommission } from '../../../interfaces/ISalesPersonCommission';
import { DataService } from 'src/app/common/services/shared/data.service';

@Component({
  selector: 'app-sales-persons-commissions',
  templateUrl: './sales-persons-commissions.component.html',
  styleUrls: ['./sales-persons-commissions.component.scss']
})

export class SalesPersonsCommissionsComponent implements OnInit, OnDestroy {
  commissions: ISalesPersonCommission[];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private uiService: UiService,
    private generalService: GeneralService,
    private data: DataService
  ) { }

  ngOnInit() {
    this.getCommissionsFirstPage();
    // this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(salespersonsCommissionsApi, pageNumber).subscribe((res: IDataRes) => {
      this.commissions = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(salespersonsCommissionsApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.commissions = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  submit() {
    const searchValues = this.generalService.checkEmptyFields(this.searchForm.value);
    if (this.generalService.isEmpty(searchValues)) {
      this.getCommissionsFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(salespersonsCommissionsApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.commissions = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      commissionCode: new FormControl(''),
      commissionNameAr: new FormControl(''),
      commissionNameEn: new FormControl('')
    });
  }

  // deleteModal(commission: ISalesPersonCommission) {
  //   const initialState = {
  //     code: commission.commissionCode,
  //     nameAr: commission.commissionNameAr,
  //     nameEn: commission.commissionNameEn
  //   };
  //   this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
  //   this.subscriptions.push(
  //     this.bsModalRef.content.confirmed.subscribe(confirmed => {
  //       if (confirmed) {
  //         this.delete(commission._id);
  //         this.bsModalRef.hide();
  //       } else {
  //         this.bsModalRef.hide();
  //       }
  //     })
  //   );
  // }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(salespersonsCommissionsApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.commissions = this.generalService.removeItem(this.commissions, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getCommissionsFirstPage() {
    this.subscriptions.push(
      this.data.get(salespersonsCommissionsApi, 1).subscribe((res: IDataRes) => {
        this.commissions = res.results;
        this.pageInfo = res;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
