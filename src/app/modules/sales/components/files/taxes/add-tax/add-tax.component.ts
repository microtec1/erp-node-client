import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';
import { baseUrl, taxesApi, detailedChartOfAccountsApi } from 'src/app/common/constants/api.constants';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { companyId } from 'src/app/common/constants/general.constants';
import { ITax } from '../../../../interfaces/ITax';
import { CustomValidators } from 'ng2-validation';
import { DataService } from 'src/app/common/services/shared/data.service';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';

@Component({
  selector: 'app-add-tax',
  templateUrl: './add-tax.component.html',
  styleUrls: ['./add-tax.component.scss']
})

export class AddTaxComponent implements OnInit, OnDestroy {
  taxForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  tax: ITax;
  filesAdded: boolean;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  baseUrl = baseUrl;
  companyId = companyId;
  detailsMode: boolean;
  @ViewChild('dropzone') dropzone: any;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(taxesApi, null, null, id)
              .subscribe((tax: ITax) => {
                this.tax = tax;
                this.formReady = true;
                this.initForm();
                if (this.detailsMode) {
                  this.taxForm.disable({ onlySelf: true });
                }
                this.uiService.isLoading.next(false);
              })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  onFilesAdded(files: File[]) {
    this.filesAdded = true;
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result;
        this.taxForm.patchValue({
          image: content
        });
      };
      reader.readAsDataURL(file);
    });
  }

  resetUploader() {
    this.dropzone.reset();
    this.filesAdded = false;
  }

  removeImage() {
    this.tax.image = '';
    this.taxForm.patchValue({
      image: ''
    });
    this.filesAdded = false;
  }

  get form() {
    return this.taxForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.tax) {
      if (this.taxForm.valid) {
        const newtax = {
          _id: this.tax._id,
          ...this.generalService.checkEmptyFields(this.taxForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.put(taxesApi, newtax).subscribe(res => {
            this.uiService.isLoading.next(false);
            this.router.navigate(['/sales/taxes']);
            this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
          })
        );
        this.loadingButton = false;
      }
    } else {
      if (this.taxForm.valid) {
        const formValue = {
          ...this.generalService.checkEmptyFields(this.taxForm.value),
          companyId: this.companyId
        };
        this.subscriptions.push(
          this.data.post(taxesApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.taxForm.reset();
            this.taxForm.patchValue({
              isActive: true,
              taxClassification: 'VAT',
              taxType: 'add'
            });
            this.filesAdded = false;
            this.dropzone.reset();
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let image = '';
    let code = '';
    let taxNameAr = '';
    let taxNameEn = '';
    let ratio = null;
    let taxClassification = 'VAT';
    let taxType = 'add';
    let glAccountId = '';
    let appliedOnAllItems = false;
    let isActive = true;

    if (this.tax) {
      image = this.tax.image;
      code = this.tax.code;
      taxNameAr = this.tax.taxNameAr;
      taxNameEn = this.tax.taxNameEn;
      ratio = this.tax.ratio;
      taxClassification = this.tax.taxClassification;
      taxType = this.tax.taxType;
      glAccountId = this.tax.glAccountId;
      appliedOnAllItems = this.tax.appliedOnAllItems;
      isActive = this.tax.isActive;
    }
    this.taxForm = new FormGroup({
      image: new FormControl(image),
      code: new FormControl(code, Validators.required),
      taxNameAr: new FormControl(taxNameAr, Validators.required),
      taxNameEn: new FormControl(taxNameEn),
      ratio: new FormControl(ratio, [Validators.required, CustomValidators.number]),
      taxClassification: new FormControl(taxClassification, Validators.required),
      taxType: new FormControl(taxType),
      glAccountId: new FormControl(glAccountId),
      appliedOnAllItems: new FormControl(appliedOnAllItems),
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
