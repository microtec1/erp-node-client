import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { FormGroup, FormControl } from '@angular/forms';
import { searchLength, companyId, branchId } from 'src/app/common/constants/general.constants';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { Router } from '@angular/router';
import { ISalesSettings } from '../../interfaces/ISalesSettings';
import { ICustomer } from '../../interfaces/ICustomer';
import { IPricingPolicy } from '../../interfaces/IPricingPolicy';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { DataService } from 'src/app/common/services/shared/data.service';
import { getSalesSettingsApi, safeBoxesApi, pricingPoliciesApi, customersApi, banksApi, detailedChartOfAccountsApi, salesSettingsApi } from 'src/app/common/constants/api.constants';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit, OnDestroy {
  settingsForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  subscriptions: Subscription[] = [];
  settings: ISalesSettings;
  formReady: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Safe Boxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Pricing Policies
  pricingPolicies: IPricingPolicy[] = [];
  pricingPoliciesInputFocused: boolean;
  hasMorePricingPolicies: boolean;
  pricingPoliciesCount: number;
  selectedPricingPoliciesPage = 1;
  pricingPoliciesPagesNo: number;
  noPricingPolicies: boolean;

  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private router: Router
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.data.post(getSalesSettingsApi, {}).subscribe((res: ISalesSettings) => {
        this.settings = res;
        this.initForm();
        this.formReady = true;
        this.uiService.isLoading.next(false);
      })
    );
    this.formReady = true;
    this.initForm();

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(pricingPoliciesApi, 1).subscribe((res: IDataRes) => {
        this.pricingPoliciesPagesNo = res.pages;
        this.pricingPoliciesCount = res.count;
        if (this.pricingPoliciesPagesNo > this.selectedPricingPoliciesPage) {
          this.hasMorePricingPolicies = true;
        }
        this.pricingPolicies.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

  }

  get form() {
    return this.settingsForm.controls;
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    if (this.settingsForm.valid) {
      const newSettings = {
        companyId,
        ...this.generalService.checkEmptyFields(this.settingsForm.value)
      };
      if (newSettings.branch.directInfluenceOnTheInventory) {
        delete newSettings.branch.directInfluenceOnTheInventoryOptions;
      }
      if (!newSettings.company.checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransaction) {
        delete newSettings.company.checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransactionOptions;
      }
      if (!newSettings.company.checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactions) {
        delete newSettings.company.checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactionsOptions;
      }
      if (!newSettings.branch.loadLastSalePrice) {
        delete newSettings.branch.loadLastSalePriceOptions;
      }

      this.data.post(salesSettingsApi, newSettings).subscribe(res => {
        this.uiService.isLoading.next(false);
        this.submitted = false;
        this.loadingButton = false;
        this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        this.subscriptions.push(
          this.data.post(getSalesSettingsApi, {}).subscribe((data: ISalesSettings) => {
            const settings = JSON.stringify(data);
            localStorage.setItem('salesSettings', settings);
            this.uiService.isLoading.next(false);
          })
        );
      },
        err => {
          this.loadingButton = false;
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        });

    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
    }
  }

  private initForm() {
    let checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransaction = false;
    let checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransactionOptions = '';
    let checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactions = false;
    let checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactionsOptions = '';
    let cannotExceedingTheCustomerLimitInSalesInvoice = false;
    let overdraft = false;
    let cannotDuplicatingTheCommunicationDetailsOfTheCustomer = false;
    let preventSellingItemIfItDosenotHasRegisterNumberInFoodAndDrugAuthority = false;
    let directInfluenceOnTheInventory = false;
    let directInfluenceOnTheInventoryOptions = '';
    let displayingMoreThanOneSalesmanInTheSalesOrderOrInTheInvoice = false;
    let defaultPaymentType = 'cash';
    let defaultPaymentAccount = '';
    let defaultCustomer = '';
    let defaultSalesPolicy = '';
    let loadLastSalePrice = false;
    let loadLastSalePriceOptions = '';
    let preventSellingItemsNotFoundInTheSalesOrder = false;
    let sellingByAPriceLessThanTheCost = false;
    let exceedingTheQuantityOfTheItemsFoundInTheSalesOrder = false;
    let exceedingTheTransactionDateFoundInTheInvoiceDuringOpeningPeriod = false;
    let mandatoryToAddTheSalesmanToTheInvoice = false;
    let activateCustomerBranches = false;
    let preventDuplicationOfReferenceNumberInTheInvoice = false;
    let priceOffer = false;
    let salesContract = false;
    let salesOrder = false;
    let salesInvoice = false;
    let salesReturns = false;

    if (this.settings) {
      checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransaction =
        this.settings.company.checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransaction;
      checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactions =
        this.settings.company.checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactions;
      checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransactionOptions =
        this.settings.company.checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransactionOptions;
      checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactionsOptions =
        this.settings.company.checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactionsOptions;
      cannotExceedingTheCustomerLimitInSalesInvoice = this.settings.company.cannotExceedingTheCustomerLimitInSalesInvoice;
      overdraft = this.settings.company.overdraft;
      cannotDuplicatingTheCommunicationDetailsOfTheCustomer = this.settings.company.cannotDuplicatingTheCommunicationDetailsOfTheCustomer;
      preventSellingItemIfItDosenotHasRegisterNumberInFoodAndDrugAuthority =
        this.settings.company.preventSellingItemIfItDosenotHasRegisterNumberInFoodAndDrugAuthority;
      directInfluenceOnTheInventory = this.settings.branch.directInfluenceOnTheInventory;
      directInfluenceOnTheInventoryOptions = this.settings.branch.directInfluenceOnTheInventoryOptions;
      displayingMoreThanOneSalesmanInTheSalesOrderOrInTheInvoice =
        this.settings.branch.displayingMoreThanOneSalesmanInTheSalesOrderOrInTheInvoice;
      defaultPaymentType = this.settings.branch.defaultPaymentType;
      defaultPaymentAccount = this.settings.branch.defaultPaymentAccount;
      defaultCustomer = this.settings.branch.defaultCustomer;
      defaultSalesPolicy = this.settings.branch.defaultSalesPolicy;
      loadLastSalePrice = this.settings.branch.loadLastSalePrice;
      loadLastSalePriceOptions = this.settings.branch.loadLastSalePriceOptions;
      preventSellingItemsNotFoundInTheSalesOrder = this.settings.branch.preventSellingItemsNotFoundInTheSalesOrder;
      sellingByAPriceLessThanTheCost = this.settings.branch.sellingByAPriceLessThanTheCost;
      exceedingTheQuantityOfTheItemsFoundInTheSalesOrder = this.settings.branch.exceedingTheQuantityOfTheItemsFoundInTheSalesOrder;
      exceedingTheTransactionDateFoundInTheInvoiceDuringOpeningPeriod =
        this.settings.branch.exceedingTheTransactionDateFoundInTheInvoiceDuringOpeningPeriod;
      mandatoryToAddTheSalesmanToTheInvoice = this.settings.branch.mandatoryToAddTheSalesmanToTheInvoice;
      activateCustomerBranches = this.settings.branch.activateCustomerBranches;
      preventDuplicationOfReferenceNumberInTheInvoice = this.settings.branch.preventDuplicationOfReferenceNumberInTheInvoice;
      priceOffer = this.settings.branch.salesTransactionCycle.priceOffer;
      salesContract = this.settings.branch.salesTransactionCycle.salesContract;
      salesOrder = this.settings.branch.salesTransactionCycle.salesOrder;
      salesInvoice = this.settings.branch.salesTransactionCycle.salesInvoice;
      salesReturns = this.settings.branch.salesTransactionCycle.salesReturns;
    }

    this.settingsForm = new FormGroup({
      company: new FormGroup({
        checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransaction:
          new FormControl(checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransaction),
        checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransactionOptions:
          new FormControl(checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransactionOptions),
        checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactions:
          new FormControl(checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactions),
        checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactionsOptions:
          new FormControl(checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactionsOptions),
        cannotExceedingTheCustomerLimitInSalesInvoice: new FormControl(cannotExceedingTheCustomerLimitInSalesInvoice),
        overdraft: new FormControl(overdraft),
        cannotDuplicatingTheCommunicationDetailsOfTheCustomer: new FormControl(cannotDuplicatingTheCommunicationDetailsOfTheCustomer),
        preventSellingItemIfItDosenotHasRegisterNumberInFoodAndDrugAuthority:
          new FormControl(preventSellingItemIfItDosenotHasRegisterNumberInFoodAndDrugAuthority),
      }),
      branch: new FormGroup({
        branchId: new FormControl(branchId),
        displayingMoreThanOneSalesmanInTheSalesOrderOrInTheInvoice:
          new FormControl(displayingMoreThanOneSalesmanInTheSalesOrderOrInTheInvoice),
        directInfluenceOnTheInventory: new FormControl(directInfluenceOnTheInventory),
        directInfluenceOnTheInventoryOptions: new FormControl(directInfluenceOnTheInventoryOptions),
        defaultPaymentType: new FormControl(defaultPaymentType),
        defaultPaymentAccount: new FormControl(defaultPaymentAccount),
        defaultCustomer:
          new FormControl(defaultCustomer),
        defaultSalesPolicy: new FormControl(defaultSalesPolicy),
        loadLastSalePrice: new FormControl(loadLastSalePrice),
        loadLastSalePriceOptions: new FormControl(loadLastSalePriceOptions),
        preventSellingItemsNotFoundInTheSalesOrder: new FormControl(preventSellingItemsNotFoundInTheSalesOrder),
        sellingByAPriceLessThanTheCost: new FormControl(sellingByAPriceLessThanTheCost),
        exceedingTheQuantityOfTheItemsFoundInTheSalesOrder: new FormControl(exceedingTheQuantityOfTheItemsFoundInTheSalesOrder),
        exceedingTheTransactionDateFoundInTheInvoiceDuringOpeningPeriod:
          new FormControl(exceedingTheTransactionDateFoundInTheInvoiceDuringOpeningPeriod),
        mandatoryToAddTheSalesmanToTheInvoice: new FormControl(mandatoryToAddTheSalesmanToTheInvoice),
        activateCustomerBranches: new FormControl(activateCustomerBranches),
        preventDuplicationOfReferenceNumberInTheInvoice: new FormControl(preventDuplicationOfReferenceNumberInTheInvoice),
        salesTransactionCycle: new FormGroup({
          priceOffer: new FormControl(priceOffer),
          salesContract: new FormControl(salesContract),
          salesOrder: new FormControl(salesOrder),
          salesInvoice: new FormControl(salesInvoice),
          salesReturns: new FormControl(salesReturns),
        }),
      })
    });
  }

  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true;
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(safeBoxesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSafeBoxes = true;
          } else {
            this.noSafeBoxes = false;
            for (const item of res.results) {
              if (this.safeBoxes.length) {
                const uniqueSafeBox = this.safeBoxes.filter(x => x.safeBoxNameAr !== item.safeBoxNameAr);
                this.safeBoxes = uniqueSafeBox;
              }
              this.safeBoxes.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreSafeboxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data.get(safeBoxesApi, this.selectedSafeBoxesPage).subscribe((res: IDataRes) => {
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        } else {
          this.hasMoreSafeBoxes = false;
        }
        for (const item of res.results) {
          if (this.safeBoxes.length) {
            const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
            this.safeBoxes = uniqueSafeBoxes;
          }
          this.safeBoxes.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(banksApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noBanks = true;
          } else {
            this.noBanks = false;
            for (const item of res.results) {
              if (this.banks.length) {
                const uniqueBank = this.banks.filter(x => x._id !== item._id);
                this.banks = uniqueBank;
              }
              this.banks.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data.get(banksApi, this.selectedBanksPage).subscribe((res: IDataRes) => {
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        } else {
          this.hasMoreBanks = false;
        }
        for (const item of res.results) {
          if (this.banks.length) {
            const uniqueBanks = this.banks.filter(x => x._id !== item._id);
            this.banks = uniqueBanks;
          }
          this.banks.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchPricingPolicies(event) {
    const searchValue = event;
    const searchQuery = {
      pricingPolicyNameAr: searchValue,
      companyId
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(pricingPoliciesApi, null, searchQuery).subscribe(
          (res: IDataRes) => {
            if (!res.results.length) {
              this.noPricingPolicies = true;
            } else {
              this.noPricingPolicies = false;
              for (const item of res.results) {
                if (this.pricingPolicies.length) {
                  const uniquepricingPolicies = this.pricingPolicies.filter(
                    x => x._id !== item._id
                  );
                  this.pricingPolicies = uniquepricingPolicies;
                }
                this.pricingPolicies.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          }
        )
      );
    }
  }

  loadMorePricingPolicies() {
    this.selectedPricingPoliciesPage = this.selectedPricingPoliciesPage + 1;
    this.subscriptions.push(
      this.data.get(pricingPoliciesApi, this.selectedPricingPoliciesPage).subscribe((res: IDataRes) => {
        if (this.pricingPoliciesPagesNo > this.selectedPricingPoliciesPage) {
          this.hasMorePricingPolicies = true;
        } else {
          this.hasMorePricingPolicies = false;
        }
        for (const item of res.results) {
          if (this.pricingPolicies.length) {
            const uniquePricingPolicies = this.pricingPolicies.filter(
              x => x._id !== item._id
            );
            this.pricingPolicies = uniquePricingPolicies;
          }
          this.pricingPolicies.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
