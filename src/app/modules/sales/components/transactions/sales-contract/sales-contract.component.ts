import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISalesContract } from '../../../interfaces/ISalesContract';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { FormGroup, FormControl } from '@angular/forms';
import { baseUrl, salesContractApi, postSalesContractApi, unPostSalesContractApi } from 'src/app/common/constants/api.constants';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ConfirmModalComponent } from 'src/app/common/components/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-sales-contract',
  templateUrl: './sales-contract.component.html',
  styleUrls: ['./sales-contract.component.scss']
})
export class SalesContractComponent implements OnInit, OnDestroy {
  salesContract: ISalesContract[] = [];
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  pageInfo: IDataRes;
  currentPage = 1;
  searchForm: FormGroup;
  searchDone: boolean;
  showSearch: boolean;
  sortType: string;
  sortValue: string;
  baseUrl = baseUrl;

  constructor(
    private modalService: BsModalService,
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    this.getSalesContractFirstPage();
    this.initSearchForm();
  }

  paginate(pageNumber) {
    this.currentPage = pageNumber;
    this.sortType = null;
    this.data.get(salesContractApi, pageNumber).subscribe((res: IDataRes) => {
      this.salesContract = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sort(sortValue) {
    this.sortType = this.sortType === 'asc' ? 'desc' : 'asc';
    this.sortValue = sortValue;
    this.data.dataSort(salesContractApi, this.currentPage, sortValue, this.sortType).subscribe((res: IDataRes) => {
      this.salesContract = res.results;
      this.uiService.isLoading.next(false);
    });
  }

  sortingClass(sortValue) {
    if (this.sortValue === sortValue) {
      if (this.sortType === 'asc') {
        return 'asc';
      } else if (this.sortType === 'desc') {
        return 'desc';
      }
    } else {
      return false;
    }
  }

  setPost(id) {
    this.subscriptions.push(
      this.data.post(`${postSalesContractApi}/${id}`, {}).subscribe(res => {
        this.salesContract = this.generalService.changeStatus(this.salesContract, id, 'posted', 'salesContractStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  setUnPost(id) {
    this.subscriptions.push(
      this.data.post(`${unPostSalesContractApi}/${id}`, {}).subscribe(res => {
        this.salesContract = this.generalService.changeStatus(this.salesContract, id, 'unposted', 'salesContractStatus');
        this.uiService.isLoading.next(false);
      })
    );
  }

  submit() {
    const searchValues = {
      ...this.generalService.checkEmptyFields(this.searchForm.value)
    };
    if (this.generalService.isEmpty(searchValues)) {
      this.getSalesContractFirstPage();
      this.searchDone = false;
    } else {
      this.subscriptions.push(
        this.data.get(salesContractApi, null, searchValues).subscribe((res: IDataRes) => {
          if (res.results.length) {
            this.salesContract = res.results;
            this.pageInfo = res;
            this.searchDone = true;
            this.uiService.isLoading.next(false);
          } else {
            this.uiService.showError('GENERAL.noRecords', 'GENERAL.tryAgain');
            this.uiService.isLoading.next(false);
          }
        })
      );
    }
  }

  updateUrl(image: any) {
    image.src = 'assets/images/country.png';
  }

  openSearch() {
    this.showSearch = !this.showSearch;
  }

  clear() {
    this.searchForm.reset();
  }

  private initSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      salesContractDescriptionAr: new FormControl(''),
      salesContractDescriptionEn: new FormControl('')
    });
  }

  deleteModal(item: ISalesContract) {
    const initialState = {
      code: item.code,
      nameAr: item.salesContractDescriptionAr,
      nameEn: item.salesContractDescriptionEn
    };
    this.bsModalRef = this.modalService.show(ConfirmModalComponent, { initialState, class: 'confirm-modal' });
    this.subscriptions.push(
      this.bsModalRef.content.confirmed.subscribe(confirmed => {
        if (confirmed) {
          this.delete(item._id);
          this.bsModalRef.hide();
        } else {
          this.bsModalRef.hide();
        }
      })
    );
  }

  delete(id: string) {
    this.subscriptions.push(
      this.data.delete(salesContractApi, id).subscribe(res => {
        this.uiService.showSuccess('GENERAL.deletedSuccessfully', '');
        this.salesContract = this.generalService.removeItem(this.salesContract, id);
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  getSalesContractFirstPage() {
    this.subscriptions.push(
      this.data.get(salesContractApi, 1).subscribe((res: IDataRes) => {
        this.pageInfo = res;
        this.salesContract = res.results;
        this.uiService.isLoading.next(false);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
