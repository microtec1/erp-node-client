import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ISalesOrder } from 'src/app/modules/sales/interfaces/ISalesOrder';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  salesOrderApi,
  detailedChartOfAccountsApi,
  banksApi,
  safeBoxesApi,
  warehousesApi,
  branchCurrenciesApi,
  taxesApi,
  employeesApi,
  customersApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi,
  salesQuotationApi,
  salesContractApi,
  projectsApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';
import { ITax } from 'src/app/modules/sales/interfaces/ITax';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';
import { ISalesQuotation } from 'src/app/modules/sales/interfaces/ISalesQuotation';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { ActivatedRoute, Router, RouterStateSnapshot, Params } from '@angular/router';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { ISalesContract } from 'src/app/modules/sales/interfaces/ISalesContract';
import { IProject } from 'src/app/modules/purchases/interfaces/IProject.model';

@Component({
  selector: 'app-add-sales-order',
  templateUrl: './add-sales-order.component.html',
  styleUrls: ['./add-sales-order.component.scss']
})
export class AddSalesOrderComponent implements OnInit, OnDestroy {
  salesOrderForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  salesOrder: ISalesOrder;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Customer Branches
  customerBranches: any[] = [];
  customerBranchesInputFocused: boolean;
  customerBranchesCount: number;
  noCustomerBranches: boolean;

  // Sales Representatives
  salesRepresentatives: IEmployee[] = [];
  salesRepresentativesInputFocused: boolean;
  salesRepresentativesCount: number;
  noSalesRepresentatives: boolean;

  // Taxes
  taxes: ITax[] = [];
  taxesInputFocused: boolean;
  hasMoreTaxes: boolean;
  taxesCount: number;
  selectedTaxesPage = 1;
  taxesPagesNo: number;
  noTaxes: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Projects
  projects: IProject[] = [];
  projectsInputFocused: boolean;
  hasMoreProjects: boolean;
  projectsCount: number;
  selectedProjectsPage = 1;
  projectsPagesNo: number;
  noProjects: boolean;

  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  // Source Items
  sourceItems: ISalesQuotation[] = [];

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(salesOrderApi, null, null, id).subscribe((salesOrder: ISalesOrder) => {
              this.salesOrder = salesOrder;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.salesOrderForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(warehousesApi, 1).subscribe((res: IDataRes) => {
        this.warehousesPagesNo = res.pages;
        this.warehousesCount = res.count;
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        }
        this.warehouses.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(projectsApi, 1).subscribe((res: IDataRes) => {
        this.projectsPagesNo = res.pages;
        this.projectsCount = res.count;
        if (this.projectsPagesNo > this.selectedProjectsPage) {
          this.hasMoreProjects = true;
        }
        this.projects.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    const searchValues = {
      taxType: 'discount'
    };

    this.subscriptions.push(
      this.data
        .get(taxesApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.taxesPagesNo = res.pages;
          this.taxesCount = res.count + 1;
          if (this.taxesPagesNo > this.selectedTaxesPage) {
            this.hasMoreTaxes = true;
          }
          this.taxes.push(...res.results);
          const withoutItem = {
            _id: 'without',
            image: null,
            code: null,
            taxNameAr: 'بدون',
            taxNameEn: null,
            ratio: null,
            taxClassification: null,
            taxType: null,
            appliedOnAllItems: null,
            isActive: null
          };
          this.taxes.unshift(withoutItem);
          this.uiService.isLoading.next(false);
        })
    );

    const searchQuery = {
      salesRepresentative: true,
      companyId
    };
    this.subscriptions.push(
      this.data
        .get(employeesApi, null, searchQuery)
        .subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSalesRepresentatives = true;
          } else {
            this.noSalesRepresentatives = false;
            for (const item of res.results) {
              if (this.salesRepresentatives.length) {
                const uniqueSalesRepresentatives = this.salesRepresentatives.filter(
                  x => x._id !== item._id
                );
                this.salesRepresentatives = uniqueSalesRepresentatives;
              }
              this.salesRepresentatives.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
  }

  fillBranches(value) {
    const customer = this.customers.find(x => x._id === value);
    this.customerBranches = customer.branches;
    for (const item of this.getPaymentsArray.controls) {
      item.patchValue({
        customerId: value
      });
    }
  }

  fillRate(event) {
    const currency = this.currencies.find(item => item.currencyId === event);
    this.salesOrderForm.patchValue({
      currencyExchangeRate: currency.exchangeRate
    });
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(warehousesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noWarehouses = true;
          } else {
            this.noWarehouses = false;
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data.get(warehousesApi, this.selectedWarehousesPage).subscribe((res: IDataRes) => {
        if (this.warehousesPagesNo > this.selectedWarehousesPage) {
          this.hasMoreWarehouses = true;
        } else {
          this.hasMoreWarehouses = false;
        }
        for (const item of res.results) {
          if (this.warehouses.length) {
            const uniqueWarehouses = this.warehouses.filter(x => x._id !== item._id);
            this.warehouses = uniqueWarehouses;
          }
          this.warehouses.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchProjects(event) {
    const searchValue = event;
    const searchQuery = {
      projectNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noProjects = true;
            } else {
              this.noProjects = false;
              for (const item of res.results) {
                if (this.projects.length) {
                  const uniqueprojects = this.projects.filter(
                    x => x._id !== item._id
                  );
                  this.projects = uniqueprojects;
                }
                this.projects.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreProjects() {
    this.selectedProjectsPage = this.selectedProjectsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedProjectsPage)
        .subscribe((res: IDataRes) => {
          if (this.projectsPagesNo > this.selectedProjectsPage) {
            this.hasMoreProjects = true;
          } else {
            this.hasMoreProjects = false;
          }
          for (const item of res.results) {
            if (this.projects.length) {
              const uniqueProjects = this.projects.filter(
                x => x._id !== item._id
              );
              this.projects = uniqueProjects;
            }
            this.projects.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchTaxes(event) {
    const searchValue = event;
    const searchQuery = {
      taxNameAr: searchValue,
      taxType: 'discount'
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(taxesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noTaxes = true;
          } else {
            this.noTaxes = false;
            for (const item of res.results) {
              if (this.taxes.length) {
                const uniqueTaxes = this.taxes.filter(x => x._id !== item._id);
                this.taxes = uniqueTaxes;
              }
              this.taxes.push(item);
            }
          }
          this.taxes = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoresafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  checkValue(value, group: FormGroup) {
    if (value === 'POSTPONE') {
      const customer = this.customers.find(item => item._id === this.salesOrderForm.value.customerId);
      if (customer) {
        group.patchValue({
          accountId: customer.accountingInfo.glAccountId
        });
      }
    }
  }
  getItem(event, formGroup: FormGroup) {
    this.subscriptions.push(
      this.data.get(storesItemsApi, null, null, event).subscribe((res: any) => {
        this.subscriptions.push(
          this.data.get(taxesApi, null, null, res.otherInformation.vatId).subscribe((data: ITax) => {
            formGroup.patchValue({
              vatPercentage: data.ratio
            });
            this.calculateVatValue(formGroup);
          })
        );

        if (res.barCode.length > 1) {
          const initialState = {
            barCodes: res.barCode
          };
          this.bsModalRef = this.modalService.show(BarcodeModalComponent, { initialState });
          this.subscriptions.push(
            this.bsModalRef.content.confirmed.subscribe(confirmed => {
              if (confirmed) {
                this.searchBarcodes(confirmed.barCode, formGroup);
                this.bsModalRef.hide();
              } else {
                this.bsModalRef.hide();
              }
            })
          );
        } else {
          this.searchBarcodes(res.barCode[0].barCode, formGroup);
        }
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  fillRow(item, formGroup) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    for (const x of item.barCode.variables) {
      variablesControl.push(
        new FormGroup({
          variableId: new FormControl(x.variableId),
          itemVariableNameId: new FormControl(x.itemVariableNameId)
        })
      );
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
    formGroup.patchValue({
      barCode: item.barCode.barCode,
      itemUnitId: item.barCode.unitId
    });
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getSalesDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.salesOrderForm.controls;
  }

  get getSalesDetailsArray() {
    return this.salesOrderForm.get('salesDetails') as FormArray;
  }

  get getPaymentsArray() {
    return this.salesOrderForm.get('payments') as FormArray;
  }

  addPayment() {
    const control = this.salesOrderForm.get('payments') as FormArray;
    control.push(
      new FormGroup({
        paymentMethod: new FormControl('CASH', Validators.required),
        accountId: new FormControl('', Validators.required),
        description: new FormControl(''),
        value: new FormControl(0, Validators.required),
        balance: new FormControl(0, Validators.required),
        dueDateGregorian: new FormControl(null),
        dueDateHijri: new FormControl(null),
        documentNumber: new FormControl(0),
        documentDateGregorian: new FormControl(null),
        documentDateHijri: new FormControl(null),
        customerId: new FormControl('', Validators.required)
      })
    );
  }

  deletePayment(index) {
    const control = this.salesOrderForm.get('payments') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addItem(group: FormGroup, salesSource?) {
    if (this.getSalesDetailsArray.valid) {
      const variablesArray = new FormArray([]);
      for (const item of group.value.variables) {
        variablesArray.push(
          new FormGroup({
            variableId: new FormControl(item.variableId),
            itemVariableNameId: new FormControl(item.itemVariableNameId)
          })
        );
      }

      this.getSalesDetailsArray.push(
        new FormGroup({
          requestId: new FormControl(group.value.requestId, Validators.required),
          itemType: new FormControl(group.value.itemType, Validators.required),
          barCode: new FormControl(group.value.barCode, Validators.required),
          itemId: new FormControl(group.value.itemId, Validators.required),
          variables: variablesArray,
          quantity: new FormControl(group.value.quantity, Validators.required),
          freeQuantity: new FormControl(group.value.freeQuantity),
          itemUnitId: new FormControl(group.value.itemUnitId),
          cost: new FormControl(group.value.cost, Validators.required),
          discountPercentage: new FormControl(group.value.discountPercentage),
          discountValue: new FormControl(group.value.discountValue),
          additionalDiscount: new FormControl(group.value.additionalDiscount),
          totalDiscount: new FormControl(group.value.totalDiscount),
          totalCost: new FormControl(group.value.totalCost),
          vatPercentage: new FormControl(group.value.vatPercentage),
          vatValue: new FormControl(group.value.vatValue),
          netValue: new FormControl(group.value.netValue, Validators.required),
          descriptionAr: new FormControl(group.value.descriptionAr),
          descriptionEn: new FormControl(group.value.descriptionEn),
        })
      );
      group.reset();
    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      this.uiService.isLoading.next(false);
    }

  }

  deleteItem(index) {
    const control = this.salesOrderForm.get('salesDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      if (transactionDate) {
        if (this.salesOrderForm.value.offerValidity) {
          this.calculateExpiryDate(this.salesOrderForm.value.offerValidity, value);
        }
        for (const control of this.getPaymentsArray.controls) {
          control.patchValue({
            dueDateGregorian: value
          });
        }
      }
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.salesOrderForm.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (transactionDate) {
        for (const control of this.getPaymentsArray.controls) {
          control.patchValue({
            dueDateGregorian: this.generalService.format(
              new Date(
                gegorianDate.year(),
                gegorianDate.month(),
                gegorianDate.date()
              )
            )
          });
        }
      }
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.salesOrderForm.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  fillRatio(value) {
    if (value !== 'without') {
      const tax = this.taxes.find(x => x._id === value);
      this.salesOrderForm.patchValue({
        discountTaxRate: tax.ratio
      });
      this.calculateTotalItemsDiscount();
    }
  }

  calculateTotalCost(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const cost = group.value.cost ? group.value.cost : 0;
    const quantity = group.value.quantity ? group.value.quantity : 0;
    const freeQuantity = group.value.freeQuantity ? group.value.freeQuantity : 0;
    const totalDiscount = group.value.totalDiscount ? group.value.totalDiscount : 0;
    const value = (cost * (quantity + freeQuantity)) - totalDiscount;
    group.patchValue({
      totalCost: +(value.toFixed(4))
    });
    if (totalCost !== value) {
      this.calculateVatValue(group);
      this.calculateAdditionalDiscount(group);
      this.calculateNetValue(group);
      this.calculateDiscountValue(group);
    }
    this.calculateTotal('totalCost', 'totalCost', 'salesDetails');
    this.calculateTotalItemsDiscount();
  }

  calculateVatValue(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const vatPercentage = group.value.vatPercentage ? group.value.vatPercentage : 0;
    group.patchValue({
      vatValue: +(((totalCost * vatPercentage) / 100).toFixed(4))
    });
    this.calculateTotal('vatValue', 'totalVat', 'salesDetails');
    this.calculateNetValue(group);
  }

  calculateExpiryDate(value, date?: Date) {
    const days = +value;
    let transactionDate;
    if (date) {
      transactionDate = date;
    } else {
      transactionDate = this.salesOrderForm.value.gregorianDate;
    }
    const expiryDate = new Date();
    expiryDate.setDate(transactionDate.getDate() + days);
    this.salesOrderForm.patchValue({
      expiryDateGregorian: expiryDate
    });
  }

  calculateTotal(fieldName, totalName, arrayName, group?: FormGroup) {
    const array = this.salesOrderForm.get(arrayName) as FormArray;
    const valuesArray = [];
    for (const item of array.value) {
      valuesArray.push(+item[fieldName]);
    }
    const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);
    this.salesOrderForm.patchValue({
      [totalName]: valuesSum
    });

    if (group) {
      if (fieldName === 'discountValue') {
        const totalCost = group.value.totalCost ? group.value.totalCost : 0;
        group.patchValue({
          discountPercentage: +(((group.value[fieldName] * 100) / totalCost).toFixed(4))
        });
      }
      this.calculateAdditionalDiscount(group);
    }
  }

  calculateAdditionalDiscount(group: FormGroup) {
    const additionalDiscountTable = group.value.additionalDiscount ? group.value.additionalDiscount : 0;
    const cost = group.value.cost ? group.value.cost : 0;
    const quantity = group.value.quantity ? group.value.quantity : 0;
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const totalCost = this.salesOrderForm.value.totalCost ? this.salesOrderForm.value.totalCost : 0;
    const discountItems = this.salesOrderForm.value.discountItems ? this.salesOrderForm.value.discountItems : 0;
    const additionalDiscount = this.salesOrderForm.value.additionalDiscount ? this.salesOrderForm.value.additionalDiscount : 0;
    const value = (((cost * quantity) - discountValue) / (totalCost + discountItems)) * additionalDiscount;

    group.patchValue({
      additionalDiscount: +(value.toFixed(4))
    });
    if (value !== additionalDiscountTable) {
      this.calculateTotalDiscount(group);
    }
  }

  calculateTotalDiscount(group: FormGroup) {
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const additionalDiscount = group.value.additionalDiscount ? group.value.additionalDiscount : 0;
    group.patchValue({
      totalDiscount: +((discountValue + additionalDiscount).toFixed(4))
    });
    this.calculateTotalCost(group);
    this.calculateTotal('totalDiscount', 'totalDiscount', 'salesDetails');
  }

  calculateDiscountValue(group: FormGroup) {
    const discountPercentage = group.value.discountPercentage ? group.value.discountPercentage : 0;
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    group.patchValue({
      discountValue: +(((discountPercentage * totalCost) / 100).toFixed(4))
    });
    this.calculateAdditionalDiscount(group);
    this.calculateTotal('discountValue', 'discountItems', 'salesDetails');
  }

  calculateTotalItemsDiscount() {
    const totalCost = this.salesOrderForm.value.totalCost ? this.salesOrderForm.value.totalCost : 0;
    const discountTaxRate = this.salesOrderForm.value.discountTaxRate ? this.salesOrderForm.value.discountTaxRate : 0;
    this.salesOrderForm.patchValue({
      totalDiscountTax: +(((totalCost * discountTaxRate) / 100).toFixed(4))
    });
  }

  calculateNetValue(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const vatValue = group.value.vatValue ? group.value.vatValue : 0;
    group.patchValue({
      netValue: +((totalCost + vatValue).toFixed(4))
    });
    this.calculateTotal('netValue', 'totalNet', 'salesDetails');
  }

  calculateTable() {
    for (const item of this.getSalesDetailsArray.controls) {
      const group = item as FormGroup;
      this.calculateAdditionalDiscount(group);
    }
  }

  loadSourceItems(value) {
    if (value === 'salesQuotation') {
      const searchValues = {
        salesQuotationStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(salesQuotationApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillTables(this.sourceItems);
          this.uiService.isLoading.next(false);
        })
      );
    }
    if (value === 'salesContract') {
      const searchValues = {
        salesContractStatus: 'posted'
      };
      this.subscriptions.push(
        this.data.get(salesContractApi, null, searchValues).subscribe((res: IDataRes) => {
          this.sourceItems = res.results;
          this.fillTables(this.sourceItems);
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  fillTables(array: ISalesQuotation[]) {
    if (array.length) {
      this.getSalesDetailsArray.controls = [];
      this.getPaymentsArray.controls = [];
      for (const item of array) {
        for (const control of item.salesDetails) {
          const variablesArray = new FormArray([]);
          for (const item2 of control.variables) {
            variablesArray.push(
              new FormGroup({
                variableId: new FormControl(item2.variableId),
                itemVariableNameId: new FormControl(item2.itemVariableNameId)
              })
            );
          }
          this.getSalesDetailsArray.push(
            new FormGroup({
              requestId: new FormControl(item._id, Validators.required),
              itemType: new FormControl(control.itemType, Validators.required),
              barCode: new FormControl(control.barCode, Validators.required),
              itemId: new FormControl(control.itemId, Validators.required),
              variables: variablesArray,
              quantity: new FormControl(control.quantity, Validators.required),
              freeQuantity: new FormControl(control.freeQuantity),
              itemUnitId: new FormControl(control.itemUnitId),
              cost: new FormControl(control.cost, Validators.required),
              discountPercentage: new FormControl(control.discountPercentage),
              discountValue: new FormControl(control.discountValue),
              additionalDiscount: new FormControl(control.additionalDiscount),
              totalDiscount: new FormControl(control.totalDiscount),
              totalCost: new FormControl(control.totalCost),
              vatPercentage: new FormControl(control.vatPercentage),
              vatValue: new FormControl(control.vatValue),
              netValue: new FormControl(control.netValue, Validators.required),
              descriptionAr: new FormControl(control.descriptionAr),
              descriptionEn: new FormControl(control.descriptionEn),
            })
          );
        }
        for (const control of item.payments) {
          this.getPaymentsArray.push(
            new FormGroup({
              paymentMethod: new FormControl(control.paymentMethod, Validators.required),
              accountId: new FormControl(control.accountId, Validators.required),
              description: new FormControl(control.description),
              value: new FormControl(control.value, Validators.required),
              balance: new FormControl(control.balance, Validators.required),
              dueDateGregorian: new FormControl(new Date(control.dueDateGregorian + 'UTC')),
              dueDateHijri: new FormControl(control.dueDateHijri),
              documentNumber: new FormControl(control.documentNumber),
              documentDateGregorian: new FormControl(new Date(control.documentDateGregorian + 'UTC')),
              documentDateHijri: new FormControl(control.documentDateHijri),
              customerId: new FormControl(control.customerId, Validators.required),
            })
          );
        }
      }
    }
  }

  deleteSourceItem(id) {
    this.sourceItems = this.sourceItems.filter(x => x._id !== id);
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    const detailsArray = (this.salesOrderForm.get('salesDetails') as FormArray).controls;
    if (detailsArray.length > 1) {
      (detailsArray[0] as FormGroup).controls.barCode.clearValidators();
      (detailsArray[0] as FormGroup).controls.barCode.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.itemId.clearValidators();
      (detailsArray[0] as FormGroup).controls.itemId.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.quantity.clearValidators();
      (detailsArray[0] as FormGroup).controls.quantity.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.itemType.clearValidators();
      (detailsArray[0] as FormGroup).controls.itemType.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.cost.clearValidators();
      (detailsArray[0] as FormGroup).controls.cost.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.netValue.clearValidators();
      (detailsArray[0] as FormGroup).controls.netValue.updateValueAndValidity();
    } else {
      this.submitted = false;
      this.loadingButton = false;
      this.uiService.showError('GENERAL.rowMustBeAdded', '');
      return;
    }

    if (typeof this.salesOrderForm.value.gregorianDate !== 'string') {
      this.salesOrderForm.value.gregorianDate =
        this.generalService.format(this.salesOrderForm.value.gregorianDate);
    }
    if (typeof this.salesOrderForm.value.hijriDate !== 'string') {
      this.salesOrderForm.value.hijriDate =
        this.generalService.formatHijriDate(this.salesOrderForm.value.hijriDate);
    }

    if (this.salesOrderForm.value.expiryDateGregorian) {
      if (typeof this.salesOrderForm.value.expiryDateGregorian !== 'string') {
        this.salesOrderForm.value.expiryDateGregorian =
          this.generalService.format(this.salesOrderForm.value.expiryDateGregorian);
      }
      if (typeof this.salesOrderForm.value.expiryDateHijri !== 'string') {
        this.salesOrderForm.value.expiryDateHijri =
          this.generalService.formatHijriDate(this.salesOrderForm.value.expiryDateHijri);
      }
    }


    for (
      let i = 0;
      i <= this.salesOrderForm.value.payments.length - 1;
      i++
    ) {

      {
        if (this.salesOrderForm.value.payments[i].dueDateGregorian) {
          if (
            typeof this.salesOrderForm.value.payments[i].dueDateGregorian !==
            'string'
          ) {
            this.salesOrderForm.value.payments[i].dueDateGregorian = this.generalService.format(
              this.salesOrderForm.value.payments[i].dueDateGregorian
            );
          }
          if (typeof this.salesOrderForm.value.payments[i].dueDateHijri !==
            'string') {
            this.salesOrderForm.value.payments[i].dueDateHijri = this.generalService.formatHijriDate(
              this.salesOrderForm.value.payments[i].dueDateHijri
            );
          }
        }
        if (this.salesOrderForm.value.payments[i].documentDateGregorian) {
          if (
            typeof this.salesOrderForm.value.payments[i].documentDateGregorian !==
            'string'
          ) {
            this.salesOrderForm.value.payments[i].documentDateGregorian = this.generalService.format(
              this.salesOrderForm.value.payments[i].documentDateGregorian
            );
          }
          if (typeof this.salesOrderForm.value.payments[i].documentDateHijri !==
            'string') {
            this.salesOrderForm.value.payments[i].documentDateHijri = this.generalService.formatHijriDate(
              this.salesOrderForm.value.payments[i].documentDateHijri
            );
          }
        }
      }

    }

    if (this.salesOrder) {
      if (this.salesOrderForm.valid) {
        this.salesOrderForm.value.salesDetails.splice(0, 1);
        const newsalesOrder = {
          _id: this.salesOrder._id,
          ...this.generalService.checkEmptyFields(this.salesOrderForm.value)
        };
        this.data.put(salesOrderApi, newsalesOrder).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/sales/salesOrder']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.salesOrderForm.valid) {
        this.salesOrderForm.value.salesDetails.splice(0, 1);
        const formValue = {
          ...this.generalService.checkEmptyFields(this.salesOrderForm.value)
        };
        this.subscriptions.push(
          this.data.post(salesOrderApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.salesOrderForm.reset();
            this.salesOrderForm.patchValue({
              isActive: true,
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let salesOrderPeriodic = false;
    let salesOrderStatus = 'unposted';
    let salesOrderDescriptionAr = '';
    let salesOrderDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let customerId = '';
    let customerBranchId = '';
    let referenceNumber = '';
    let delegateId = '';
    let currencyId = '';
    let currencyExchangeRate = 0;
    let discountTaxType = 'without';
    let discountTaxRate = 0;
    let targetType = 'warehouse';
    let warehouseId = '';
    let projectId = '';
    let sourceType = 'without';
    let discountItems = 0;
    let totalDiscount = 0;
    let totalCost = 0;
    let totalDiscountTax = 0;
    let totalVat = 0;
    let additionalDiscount = 0;
    let totalBalance = 0;
    let totalNet = 0;
    let variablesArray = new FormArray([]);
    let salesDetailsArray = new FormArray([
      new FormGroup({
        requestId: new FormControl(''),
        itemType: new FormControl('STORE_CATEGORY', Validators.required),
        barCode: new FormControl('', Validators.required),
        itemId: new FormControl('', Validators.required),
        variables: variablesArray,
        quantity: new FormControl(1, Validators.required),
        freeQuantity: new FormControl(0),
        itemUnitId: new FormControl(''),
        cost: new FormControl(0, Validators.required),
        discountPercentage: new FormControl(0),
        discountValue: new FormControl(0),
        additionalDiscount: new FormControl(0),
        totalDiscount: new FormControl(0),
        totalCost: new FormControl(0),
        vatPercentage: new FormControl(0),
        vatValue: new FormControl(0),
        netValue: new FormControl(0, Validators.required),
        descriptionAr: new FormControl(''),
        descriptionEn: new FormControl(''),
      })
    ]);
    let paymentsArray = new FormArray([
      new FormGroup({
        paymentMethod: new FormControl('CASH', Validators.required),
        accountId: new FormControl('', Validators.required),
        description: new FormControl(''),
        value: new FormControl(0, Validators.required),
        balance: new FormControl(0, Validators.required),
        dueDateGregorian: new FormControl(null),
        dueDateHijri: new FormControl(null),
        documentNumber: new FormControl(0),
        documentDateGregorian: new FormControl(null),
        documentDateHijri: new FormControl(null),
        customerId: new FormControl('', Validators.required)
      })
    ]);
    let isActive = true;

    if (this.salesOrder) {
      code = this.salesOrder.code;
      salesOrderPeriodic = this.salesOrder.salesOrderPeriodic;
      salesOrderStatus = this.salesOrder.salesOrderStatus;
      salesOrderDescriptionAr = this.salesOrder.salesOrderDescriptionAr;
      salesOrderDescriptionEn = this.salesOrder.salesOrderDescriptionEn;
      gregorianDate = new Date(this.salesOrder.gregorianDate + 'UTC');
      hijriDate = this.salesOrder.hijriDate;
      customerId = this.salesOrder.customerId;
      customerBranchId = this.salesOrder.customerBranchId;
      referenceNumber = this.salesOrder.referenceNumber;
      delegateId = this.salesOrder.delegateId;
      currencyId = this.salesOrder.currencyId;
      currencyExchangeRate = this.salesOrder.currencyExchangeRate;
      discountTaxType = this.salesOrder.discountTaxType;
      discountTaxRate = this.salesOrder.discountTaxRate;
      targetType = this.salesOrder.targetType;
      warehouseId = this.salesOrder.warehouseId;
      projectId = this.salesOrder.projectId;
      sourceType = this.salesOrder.sourceType;
      isActive = this.salesOrder.isActive;
      discountItems = this.salesOrder.discountItems;
      totalDiscount = this.salesOrder.totalDiscount;
      totalCost = this.salesOrder.totalCost;
      totalDiscountTax = this.salesOrder.totalDiscountTax;
      totalVat = this.salesOrder.totalVat;
      additionalDiscount = this.salesOrder.additionalDiscount;
      totalBalance = this.salesOrder.totalBalance;
      totalNet = this.salesOrder.totalNet;
      salesDetailsArray = new FormArray([]);
      for (const control of this.salesOrder.salesDetails) {
        variablesArray = new FormArray([]);
        salesDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(''),
            itemType: new FormControl('', Validators.required),
            barCode: new FormControl('', Validators.required),
            itemId: new FormControl('', Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(1, Validators.required),
            freeQuantity: new FormControl(0),
            itemUnitId: new FormControl(''),
            cost: new FormControl(0, Validators.required),
            discountPercentage: new FormControl(0),
            discountValue: new FormControl(0),
            additionalDiscount: new FormControl(0),
            totalDiscount: new FormControl(0),
            totalCost: new FormControl(0),
            vatPercentage: new FormControl(0),
            vatValue: new FormControl(0),
            netValue: new FormControl(0, Validators.required),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
          })
        );
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        salesDetailsArray.push(
          new FormGroup({
            requestId: new FormControl(control.requestId),
            itemType: new FormControl(control.itemType, Validators.required),
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            freeQuantity: new FormControl(control.freeQuantity),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(control.cost, Validators.required),
            discountPercentage: new FormControl(control.discountPercentage),
            discountValue: new FormControl(control.discountValue),
            additionalDiscount: new FormControl(control.additionalDiscount),
            totalDiscount: new FormControl(control.totalDiscount),
            totalCost: new FormControl(control.totalCost),
            vatPercentage: new FormControl(control.vatPercentage),
            vatValue: new FormControl(control.vatValue),
            netValue: new FormControl(control.netValue, Validators.required),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
          })
        );
      }

      paymentsArray = new FormArray([]);
      for (const control of this.salesOrder.payments) {
        paymentsArray.push(
          new FormGroup({
            paymentMethod: new FormControl(control.paymentMethod, Validators.required),
            accountId: new FormControl(control.accountId, Validators.required),
            description: new FormControl(control.description),
            value: new FormControl(control.value, Validators.required),
            balance: new FormControl(control.balance, Validators.required),
            dueDateGregorian: new FormControl(new Date(control.dueDateGregorian + 'UTC')),
            dueDateHijri: new FormControl(control.dueDateHijri),
            documentNumber: new FormControl(control.documentNumber),
            documentDateGregorian: new FormControl(new Date(control.documentDateGregorian + 'UTC')),
            documentDateHijri: new FormControl(control.documentDateHijri),
            customerId: new FormControl(control.customerId, Validators.required),
          })
        );
      }
    }
    this.salesOrderForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      salesOrderPeriodic: new FormControl(salesOrderPeriodic),
      salesOrderStatus: new FormControl(salesOrderStatus),
      salesOrderDescriptionAr: new FormControl(salesOrderDescriptionAr),
      salesOrderDescriptionEn: new FormControl(salesOrderDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      customerId: new FormControl(customerId, Validators.required),
      customerBranchId: new FormControl(customerBranchId),
      referenceNumber: new FormControl(referenceNumber),
      delegateId: new FormControl(delegateId, Validators.required),
      currencyId: new FormControl(currencyId, Validators.required),
      currencyExchangeRate: new FormControl(currencyExchangeRate),
      targetType: new FormControl(targetType, Validators.required),
      warehouseId: new FormControl(warehouseId, Validators.required),
      projectId: new FormControl(projectId, Validators.required),
      sourceType: new FormControl(sourceType, Validators.required),
      discountTaxType: new FormControl(discountTaxType, Validators.required),
      discountTaxRate: new FormControl(discountTaxRate, Validators.required),
      discountItems: new FormControl(discountItems, Validators.required),
      totalDiscount: new FormControl(totalDiscount, Validators.required),
      totalCost: new FormControl(totalCost, Validators.required),
      totalDiscountTax: new FormControl(totalDiscountTax, Validators.required),
      totalVat: new FormControl(totalVat, Validators.required),
      additionalDiscount: new FormControl(additionalDiscount, Validators.required),
      totalBalance: new FormControl(totalBalance, Validators.required),
      totalNet: new FormControl(totalNet, Validators.required),
      salesDetails: salesDetailsArray,
      payments: paymentsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
