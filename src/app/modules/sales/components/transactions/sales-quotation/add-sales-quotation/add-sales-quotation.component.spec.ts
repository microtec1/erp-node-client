import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSalesQuotationComponent } from './add-sales-quotation.component';

describe('AddSalesQuotationComponent', () => {
  let component: AddSalesQuotationComponent;
  let fixture: ComponentFixture<AddSalesQuotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSalesQuotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSalesQuotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
