import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators, Form } from '@angular/forms';
import { ISalesQuotation } from 'src/app/modules/sales/interfaces/ISalesQuotation';
import { Subscription } from 'rxjs';
import {
  baseUrl,
  employeesApi,
  branchCurrenciesApi,
  salesQuotationApi,
  taxesApi,
  customersApi,
  storesItemsApi,
  itemsVariablesApi,
  getStoreItemViaBarcodeApi,
  itemsUnitsApi,
  warehousesApi,
  safeBoxesApi,
  banksApi,
  detailedChartOfAccountsApi
} from 'src/app/common/constants/api.constants';
import { companyId, searchLength } from 'src/app/common/constants/general.constants';
import { IItemsUnit } from 'src/app/modules/inventory/interfaces/IItemsUnit';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IItemsVariable } from 'src/app/modules/inventory/interfaces/IItemsVariable';
import { ICustomer } from 'src/app/modules/sales/interfaces/ICustomer';
import { RouterStateSnapshot, ActivatedRoute, Router, Params } from '@angular/router';
import { DataService } from 'src/app/common/services/shared/data.service';
import { UiService } from 'src/app/common/services/ui/ui.service';
import { GeneralService } from 'src/app/modules/general/services/general.service';
import { IStoreItem } from 'src/app/modules/inventory/interfaces/IStoreItem';
import { IEmployee } from 'src/app/modules/general/interfaces/IEmployee';
import { IDataRes } from 'src/app/common/interfaces/IDataResponse.model';
import { ITax } from 'src/app/modules/sales/interfaces/ITax';
import { BarcodeModalComponent } from 'src/app/common/components/barcode-modal/barcode-modal.component';
import { IBranch } from 'src/app/modules/general/interfaces/IBranch';
import { IWarehouse } from 'src/app/modules/inventory/interfaces/IWarehouse';
import { IBox } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBox';
import { IBank } from 'src/app/modules/accounting/modules/financial-transactions/interfaces/IBank';
import { IChartOfAccount } from 'src/app/modules/accounting/modules/gl/interfaces/IChartOfAccount';

@Component({
  selector: 'app-add-sales-quotation',
  templateUrl: './add-sales-quotation.component.html',
  styleUrls: ['./add-sales-quotation.component.scss']
})
export class AddSalesQuotationComponent implements OnInit, OnDestroy {
  salesQuotationForm: FormGroup;
  submitted: boolean;
  loadingButton: boolean;
  salesQuotation: ISalesQuotation;
  formReady: boolean;
  subscriptions: Subscription[] = [];
  detailsMode: boolean;
  baseUrl = baseUrl;
  companyId = companyId;
  barcodes: any[] = [];
  variables: any[] = [];
  itemsUnits: IItemsUnit[] = [];
  bsModalRef: BsModalRef;

  // Items Variables
  itemsVariables: IItemsVariable[] = [];
  itemsVariablesInputFocused: boolean;
  hasMoreItemsVariables: boolean;
  itemsVariablesCount: number;
  selectedItemsVariablesPage = 1;
  itemsVariablesPagesNo: number;
  noItemsVariables: boolean;

  // Customers
  customers: ICustomer[] = [];
  customersInputFocused: boolean;
  hasMoreCustomers: boolean;
  customersCount: number;
  selectedCustomersPage = 1;
  customersPagesNo: number;
  noCustomers: boolean;

  // Currencies
  currencies: any[] = [];
  currenciesInputFocused: boolean;
  currenciesCount: number;
  noCurrencies: boolean;

  // Customer Branches
  customerBranches: any[] = [];
  customerBranchesInputFocused: boolean;
  customerBranchesCount: number;
  noCustomerBranches: boolean;

  // Sales Representatives
  salesRepresentatives: IEmployee[] = [];
  salesRepresentativesInputFocused: boolean;
  salesRepresentativesCount: number;
  noSalesRepresentatives: boolean;

  // Taxes
  taxes: ITax[] = [];
  taxesInputFocused: boolean;
  hasMoreTaxes: boolean;
  taxesCount: number;
  selectedTaxesPage = 1;
  taxesPagesNo: number;
  noTaxes: boolean;

  // Store Items
  storeItems: IStoreItem[] = [];
  storeItemsInputFocused: boolean;
  hasMoreStoreItems: boolean;
  storeItemsCount: number;
  selectedStoreItemsPage = 1;
  storeItemsPagesNo: number;
  noStoreItems: boolean;

  // Warehouses
  warehouses: IWarehouse[] = [];
  warehousesInputFocused: boolean;
  hasMoreWarehouses: boolean;
  warehousesCount: number;
  selectedWarehousesPage = 1;
  warehousesPagesNo: number;
  noWarehouses: boolean;

  // Safeboxes
  safeBoxes: IBox[] = [];
  safeBoxesInputFocused: boolean;
  safeBoxesInputFocused2: boolean;
  hasMoreSafeBoxes: boolean;
  safeBoxesCount: number;
  selectedSafeBoxesPage = 1;
  safeBoxesPagesNo: number;
  noSafeBoxes: boolean;

  // Banks
  banks: IBank[] = [];
  banksInputFocused: boolean;
  hasMoreBanks: boolean;
  banksCount: number;
  selectedBanksPage = 1;
  banksPagesNo: number;
  noBanks: boolean;

  // Chart of Accounts
  chartOfAccounts: IChartOfAccount[] = [];
  chartOfAccountsInputFocused: boolean;
  chartOfAccountsCount: number;
  noChartOfAccounts: boolean;

  constructor(
    private data: DataService,
    private uiService: UiService,
    private generalService: GeneralService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    if (snapshot.url.includes('details')) {
      this.detailsMode = true;
    } else {
      this.detailsMode = false;
    }
    this.subscriptions.push(
      this.route.params.subscribe((params: Params) => {
        const id = params.id;
        if (id != null) {
          this.subscriptions.push(
            this.data.get(salesQuotationApi, null, null, id).subscribe((salesQuotation: ISalesQuotation) => {
              this.salesQuotation = salesQuotation;
              this.formReady = true;
              this.initForm();
              if (this.detailsMode) {
                this.salesQuotationForm.disable({ onlySelf: true });
              }
              this.uiService.isLoading.next(false);
            })
          );
        } else {
          this.formReady = true;
          this.initForm();
        }
      })
    );

    this.subscriptions.push(
      this.data.get(detailedChartOfAccountsApi, null, null, companyId).subscribe((res: IChartOfAccount[]) => {
        if (res.length) {
          this.chartOfAccounts.push(...res);
          this.chartOfAccountsCount = res.length;
        } else {
          this.noChartOfAccounts = true;
        }
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(banksApi, 1).subscribe((res: IDataRes) => {
        this.banksPagesNo = res.pages;
        this.banksCount = res.count;
        if (this.banksPagesNo > this.selectedBanksPage) {
          this.hasMoreBanks = true;
        }
        this.banks.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(safeBoxesApi, 1).subscribe((res: IDataRes) => {
        this.safeBoxesPagesNo = res.pages;
        this.safeBoxesCount = res.count;
        if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
          this.hasMoreSafeBoxes = true;
        }
        this.safeBoxes.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(warehousesApi, 1)
        .subscribe((res: IDataRes) => {
          this.warehousesPagesNo = res.pages;
          this.warehousesCount = res.count;
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          }
          for (const item of res.results) {
            if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data
        .post(branchCurrenciesApi, {})
        .subscribe((res: IDataRes) => {
          if (res.branches[0].branchCurrency.length) {
            this.currencies.push(...res.branches[0].branchCurrency);
            this.currenciesCount = res.branches[0].branchCurrency.length;
          } else {
            this.noCurrencies = true;
          }
          this.uiService.isLoading.next(false);
        })
    );

    const searchValues = {
      taxType: 'discount'
    };

    this.subscriptions.push(
      this.data
        .get(taxesApi, null, searchValues)
        .subscribe((res: IDataRes) => {
          this.taxesPagesNo = res.pages;
          this.taxesCount = res.count + 1;
          if (this.taxesPagesNo > this.selectedTaxesPage) {
            this.hasMoreTaxes = true;
          }
          this.taxes.push(...res.results);
          const withoutItem = {
            _id: 'without',
            image: null,
            code: null,
            taxNameAr: 'بدون',
            taxNameEn: null,
            ratio: null,
            taxClassification: null,
            taxType: null,
            appliedOnAllItems: null,
            isActive: null
          };
          this.taxes.unshift(withoutItem);
          this.uiService.isLoading.next(false);
        })
    );

    const searchQuery = {
      salesRepresentative: true,
      companyId
    };
    this.subscriptions.push(
      this.data
        .get(employeesApi, null, searchQuery)
        .subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noSalesRepresentatives = true;
          } else {
            this.noSalesRepresentatives = false;
            for (const item of res.results) {
              if (this.salesRepresentatives.length) {
                const uniqueSalesRepresentatives = this.salesRepresentatives.filter(
                  x => x._id !== item._id
                );
                this.salesRepresentatives = uniqueSalesRepresentatives;
              }
              this.salesRepresentatives.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );

    this.subscriptions.push(
      this.data.get(customersApi, 1).subscribe((res: IDataRes) => {
        this.customersPagesNo = res.pages;
        this.customersCount = res.count;
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        }
        this.customers.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data.get(storesItemsApi, 1).subscribe((res: IDataRes) => {
        this.storeItemsPagesNo = res.pages;
        this.storeItemsCount = res.count;
        if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
          this.hasMoreStoreItems = true;
        }
        this.storeItems.push(...res.results);
        this.uiService.isLoading.next(false);
      })
    );

    this.subscriptions.push(
      this.data
        .get(itemsVariablesApi, 1)
        .subscribe((res: IDataRes) => {
          this.itemsVariablesPagesNo = res.pages;
          this.itemsVariablesCount = res.count;
          if (this.itemsVariablesPagesNo > this.selectedItemsVariablesPage) {
            this.hasMoreItemsVariables = true;
          }
          this.itemsVariables.push(...res.results);
          this.uiService.isLoading.next(false);
        })
    );
  }

  fillBranches(value) {
    const customer = this.customers.find(x => x._id === value);
    this.customerBranches = customer.branches;
    for (const item of this.getPaymentsArray.controls) {
      item.patchValue({
        customerId: value
      });
    }
  }

  fillRate(event) {
    const currency = this.currencies.find(item => item.currencyId === event);
    this.salesQuotationForm.patchValue({
      currencyExchangeRate: currency.exchangeRate
    });
  }

  searchCustomers(event) {
    const searchValue = event;
    const searchQuery = {
      customerNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(customersApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noCustomers = true;
          } else {
            this.noCustomers = false;
            for (const item of res.results) {
              if (this.customers.length) {
                const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
                this.customers = uniqueCustomers;
              }
              this.customers.push(item);
            }
          }
          this.customers = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchWarehouses(event) {
    const searchValue = event;
    const searchQuery = {
      warehouseNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(warehousesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noWarehouses = true;
            } else {
              this.noWarehouses = false;
              const itemsArray = [];
              for (const item of res.results) {
                if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                  itemsArray.push(item);
                }
              }
              for (const item of itemsArray) {
                if (this.warehouses.length) {
                  const uniqueWarehouses = this.warehouses.filter(
                    x => x._id !== item._id
                  );
                  this.warehouses = uniqueWarehouses;
                }
                this.warehouses.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreWarehouses() {
    this.selectedWarehousesPage = this.selectedWarehousesPage + 1;
    this.subscriptions.push(
      this.data
        .get(warehousesApi, this.selectedWarehousesPage)
        .subscribe((res: IDataRes) => {
          if (this.warehousesPagesNo > this.selectedWarehousesPage) {
            this.hasMoreWarehouses = true;
          } else {
            this.hasMoreWarehouses = false;
            const itemsArray = [];
            for (const item of res.results) {
              if (item.warehouseType !== 'goodsByRoad' && item.warehouseType !== 'underDischargeGoods') {
                itemsArray.push(item);
              }
            }
            for (const item of res.results) {
              if (this.warehouses.length) {
                const uniqueWarehouses = this.warehouses.filter(
                  x => x._id !== item._id
                );
                this.warehouses = uniqueWarehouses;
              }
              this.warehouses.push(item);
            }
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  loadMoreCustomers() {
    this.selectedCustomersPage = this.selectedCustomersPage + 1;
    this.subscriptions.push(
      this.data.get(customersApi, this.selectedCustomersPage).subscribe((res: IDataRes) => {
        if (this.customersPagesNo > this.selectedCustomersPage) {
          this.hasMoreCustomers = true;
        } else {
          this.hasMoreCustomers = false;
        }
        for (const item of res.results) {
          if (this.customers.length) {
            const uniqueCustomers = this.customers.filter(x => x._id !== item._id);
            this.customers = uniqueCustomers;
          }
          this.customers.push(item);
        }
        this.uiService.isLoading.next(false);
      })
    );
  }

  searchTaxes(event) {
    const searchValue = event;
    const searchQuery = {
      taxNameAr: searchValue,
      taxType: 'discount'
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data.get(taxesApi, null, searchQuery).subscribe((res: IDataRes) => {
          if (!res.results.length) {
            this.noTaxes = true;
          } else {
            this.noTaxes = false;
            for (const item of res.results) {
              if (this.taxes.length) {
                const uniqueTaxes = this.taxes.filter(x => x._id !== item._id);
                this.taxes = uniqueTaxes;
              }
              this.taxes.push(item);
            }
          }
          this.taxes = res.results;
          this.uiService.isLoading.next(false);
        })
      );
    }
  }

  searchStoreItems(event) {
    const searchValue = event;
    const searchQuery = {
      itemsNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(storesItemsApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noStoreItems = true;
            } else {
              this.noStoreItems = false;
              for (const item of res.results) {
                if (this.storeItems.length) {
                  const uniqueStoreItems = this.storeItems.filter(
                    x => x._id !== item._id
                  );
                  this.storeItems = uniqueStoreItems;
                }
                this.storeItems.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreStoreItems() {
    this.selectedStoreItemsPage = this.selectedStoreItemsPage + 1;
    this.subscriptions.push(
      this.data
        .get(storesItemsApi, this.selectedStoreItemsPage)
        .subscribe((res: IDataRes) => {
          if (this.storeItemsPagesNo > this.selectedStoreItemsPage) {
            this.hasMoreStoreItems = true;
          } else {
            this.hasMoreStoreItems = false;
          }
          for (const item of res.results) {
            if (this.storeItems.length) {
              const uniqueStoreItems = this.storeItems.filter(
                x => x._id !== item._id
              );
              this.storeItems = uniqueStoreItems;
            }
            this.storeItems.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchSafeBoxes(event) {
    const searchValue = event;
    const searchQuery = {
      safeBoxNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(safeBoxesApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noSafeBoxes = true;
            } else {
              this.noSafeBoxes = false;
              for (const item of res.results) {
                if (this.safeBoxes.length) {
                  const uniqueSafeBoxes = this.safeBoxes.filter(
                    x => x._id !== item._id
                  );
                  this.safeBoxes = uniqueSafeBoxes;
                }
                this.safeBoxes.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoresafeBoxes() {
    this.selectedSafeBoxesPage = this.selectedSafeBoxesPage + 1;
    this.subscriptions.push(
      this.data
        .get(safeBoxesApi, this.selectedSafeBoxesPage)
        .subscribe((res: IDataRes) => {
          if (this.safeBoxesPagesNo > this.selectedSafeBoxesPage) {
            this.hasMoreSafeBoxes = true;
          } else {
            this.hasMoreSafeBoxes = false;
          }
          for (const item of res.results) {
            if (this.safeBoxes.length) {
              const uniqueSafeBoxes = this.safeBoxes.filter(x => x._id !== item._id);
              this.safeBoxes = uniqueSafeBoxes;
            }
            this.safeBoxes.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBanks(event) {
    const searchValue = event;
    const searchQuery = {
      bankNameAr: searchValue
    };
    if (searchValue.length >= searchLength) {
      this.subscriptions.push(
        this.data
          .get(banksApi, null, searchQuery)
          .subscribe((res: IDataRes) => {
            if (!res.results.length) {
              this.noBanks = true;
            } else {
              this.noBanks = false;
              for (const item of res.results) {
                if (this.banks.length) {
                  const uniqueBanks = this.banks.filter(
                    x => x._id !== item._id
                  );
                  this.banks = uniqueBanks;
                }
                this.banks.push(item);
              }
            }
            this.uiService.isLoading.next(false);
          })
      );
    }
  }

  loadMoreBanks() {
    this.selectedBanksPage = this.selectedBanksPage + 1;
    this.subscriptions.push(
      this.data
        .get(banksApi, this.selectedBanksPage)
        .subscribe((res: IDataRes) => {
          if (this.banksPagesNo > this.selectedBanksPage) {
            this.hasMoreBanks = true;
          } else {
            this.hasMoreBanks = false;
          }
          for (const item of res.results) {
            if (this.banks.length) {
              const uniqueBanks = this.banks.filter(x => x._id !== item._id);
              this.banks = uniqueBanks;
            }
            this.banks.push(item);
          }
          this.uiService.isLoading.next(false);
        })
    );
  }

  searchBarcodes(value, formGroup: FormGroup) {
    const body = {
      barCode: value
    };
    this.subscriptions.push(
      this.data
        .post(getStoreItemViaBarcodeApi, body)
        .subscribe((item: any) => {
          if (item) {
            this.fillRow(item, formGroup);
            formGroup.patchValue({
              itemId: item._id
            });
          }
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
    );
  }

  filterItems(formValue) {
    console.log(formValue);
  }

  checkValue(value, group: FormGroup) {
    if (value === 'POSTPONE') {
      const customer = this.customers.find(item => item._id === this.salesQuotationForm.value.customerId);
      if (customer) {
        group.patchValue({
          accountId: customer.accountingInfo.glAccountId
        });
      }
    }
  }
  getItem(event, formGroup: FormGroup) {
    this.subscriptions.push(
      this.data.get(storesItemsApi, null, null, event).subscribe((res: any) => {
        this.subscriptions.push(
          this.data.get(taxesApi, null, null, res.otherInformation.vatId).subscribe((data: ITax) => {
            formGroup.patchValue({
              vatPercentage: data.ratio
            });
            this.calculateVatValue(formGroup);
          })
        );

        if (res.barCode.length > 1) {
          const initialState = {
            barCodes: res.barCode
          };
          this.bsModalRef = this.modalService.show(BarcodeModalComponent, { initialState });
          this.subscriptions.push(
            this.bsModalRef.content.confirmed.subscribe(confirmed => {
              if (confirmed) {
                this.searchBarcodes(confirmed.barCode, formGroup);
                this.bsModalRef.hide();
              } else {
                this.bsModalRef.hide();
              }
            })
          );
        } else {
          this.searchBarcodes(res.barCode[0].barCode, formGroup);
        }
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  fillRow(item, formGroup) {
    this.getItemUnit(item);
    const variablesControl = formGroup.get('variables') as FormArray;
    variablesControl.controls = [];
    for (const x of item.barCode.variables) {
      variablesControl.push(
        new FormGroup({
          variableId: new FormControl(x.variableId),
          itemVariableNameId: new FormControl(x.itemVariableNameId)
        })
      );
    }
    for (const x of variablesControl.value) {
      this.subscriptions.push(
        this.data.get(itemsVariablesApi, null, null, x.variableId).subscribe((res: IItemsVariable) => {
          this.itemsVariables.push(res);
          this.uiService.isLoading.next(false);
        }, err => {
          this.uiService.isLoading.next(false);
          this.uiService.showErrorMessage(err);
        })
      );
    }
    formGroup.patchValue({
      barCode: item.barCode.barCode,
      itemUnitId: item.barCode.unitId
    });
  }

  getVariables(index) {
    const variables = [];
    const variablesControl = this.getSalesDetailsArray.controls[index].get('variables') as FormArray;
    for (const item of variablesControl.value) {
      const variable = this.itemsVariables.find(data => data._id === item.variableId);
      if (variable) {
        const value = variable.itemsVariables.find(d => d._id === item.itemVariableNameId);
        variables.push(
          { name: variable.itemsVariableNameAr, value: value.name }
        );
      }
    }
    return variables;
  }

  getItemUnit(body) {
    const searchBody = {
      _id: body.unitId
    };
    this.subscriptions.push(
      this.data.get(itemsUnitsApi, null, searchBody).subscribe((res: IDataRes) => {
        this.itemsUnits = res.results;
        this.uiService.isLoading.next(false);
      }, err => {
        this.uiService.isLoading.next(false);
        this.uiService.showErrorMessage(err);
      })
    );
  }

  get form() {
    return this.salesQuotationForm.controls;
  }

  get getSalesDetailsArray() {
    return this.salesQuotationForm.get('salesDetails') as FormArray;
  }

  get getPaymentsArray() {
    return this.salesQuotationForm.get('payments') as FormArray;
  }

  addPayment() {
    const control = this.salesQuotationForm.get('payments') as FormArray;
    control.push(
      new FormGroup({
        paymentMethod: new FormControl('CASH', Validators.required),
        accountId: new FormControl('', Validators.required),
        description: new FormControl(''),
        value: new FormControl(0, Validators.required),
        balance: new FormControl(0, Validators.required),
        dueDateGregorian: new FormControl(null),
        dueDateHijri: new FormControl(null),
        documentNumber: new FormControl(0),
        documentDateGregorian: new FormControl(null),
        documentDateHijri: new FormControl(null),
        customerId: new FormControl('', Validators.required)
      })
    );
  }

  deletePayment(index) {
    const control = this.salesQuotationForm.get('payments') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  addItem(group: FormGroup) {
    if (this.getSalesDetailsArray.valid) {
      const variablesArray = new FormArray([]);
      for (const item of group.value.variables) {
        variablesArray.push(
          new FormGroup({
            variableId: new FormControl(item.variableId),
            itemVariableNameId: new FormControl(item.itemVariableNameId)
          })
        );
      }

      this.getSalesDetailsArray.push(
        new FormGroup({
          itemType: new FormControl(group.value.itemType, Validators.required),
          barCode: new FormControl(group.value.barCode, Validators.required),
          itemId: new FormControl(group.value.itemId, Validators.required),
          variables: variablesArray,
          quantity: new FormControl(group.value.quantity, Validators.required),
          freeQuantity: new FormControl(group.value.freeQuantity),
          itemUnitId: new FormControl(group.value.itemUnitId),
          cost: new FormControl(group.value.cost, Validators.required),
          discountPercentage: new FormControl(group.value.discountPercentage),
          discountValue: new FormControl(group.value.discountValue),
          additionalDiscount: new FormControl(group.value.additionalDiscount),
          totalDiscount: new FormControl(group.value.totalDiscount),
          totalCost: new FormControl(group.value.totalCost),
          vatPercentage: new FormControl(group.value.vatPercentage),
          vatValue: new FormControl(group.value.vatValue),
          netValue: new FormControl(group.value.netValue, Validators.required),
          descriptionAr: new FormControl(group.value.descriptionAr),
          descriptionEn: new FormControl(group.value.descriptionEn),
        })
      );
      group.reset();
    } else {
      this.loadingButton = false;
      this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
      this.uiService.isLoading.next(false);
    }

  }

  deleteItem(index) {
    const control = this.salesQuotationForm.get('salesDetails') as FormArray;
    if (control.length === 1) {
      return;
    }
    control.removeAt(index);
  }

  showFeedback(fieldName) {
    if (fieldName.touched && fieldName.errors) {
      return true;
    }
  }

  setHijriDate(value: Date, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      if (transactionDate) {
        if (this.salesQuotationForm.value.offerValidity) {
          this.calculateExpiryDate(this.salesQuotationForm.value.offerValidity, value);
        }
        for (const control of this.getPaymentsArray.controls) {
          control.patchValue({
            dueDateGregorian: value
          });
        }
      }
      const hijriDate = this.generalService.convertToHijri(value);
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      } else {
        this.salesQuotationForm.patchValue({
          [fieldName]: {
            year: hijriDate.iYear(),
            month: hijriDate.iMonth() + 1,
            day: hijriDate.iDate()
          }
        });
      }
    }
  }

  setGregorianDate(value, formGroup?: FormGroup, fieldName?: string, transactionDate?: boolean) {
    if (value) {
      const gegorianDate = this.generalService.convertToGregorian(value);
      if (transactionDate) {
        for (const control of this.getPaymentsArray.controls) {
          control.patchValue({
            dueDateGregorian: this.generalService.format(
              new Date(
                gegorianDate.year(),
                gegorianDate.month(),
                gegorianDate.date()
              )
            )
          });
        }
      }
      if (formGroup) {
        formGroup.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      } else {
        this.salesQuotationForm.patchValue({
          [fieldName]: this.generalService.format(
            new Date(
              gegorianDate.year(),
              gegorianDate.month(),
              gegorianDate.date()
            )
          )
        });
      }

    }
  }

  fillRatio(value) {
    if (value !== 'without') {
      const tax = this.taxes.find(x => x._id === value);
      this.salesQuotationForm.patchValue({
        discountTaxRate: tax.ratio
      });
      this.calculateTotalItemsDiscount();
    }
  }

  calculateTotalCost(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const cost = group.value.cost ? group.value.cost : 0;
    const quantity = group.value.quantity ? group.value.quantity : 0;
    const freeQuantity = group.value.freeQuantity ? group.value.freeQuantity : 0;
    const totalDiscount = group.value.totalDiscount ? group.value.totalDiscount : 0;
    const value = (cost * (quantity + freeQuantity)) - totalDiscount;
    group.patchValue({
      totalCost: +(value.toFixed(4))
    });
    if (totalCost !== value) {
      this.calculateVatValue(group);
      this.calculateAdditionalDiscount(group);
      this.calculateNetValue(group);
      this.calculateDiscountValue(group);
    }
    this.calculateTotal('totalCost', 'totalCost', 'salesDetails');
    this.calculateTotalItemsDiscount();
  }

  calculateVatValue(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const vatPercentage = group.value.vatPercentage ? group.value.vatPercentage : 0;
    group.patchValue({
      vatValue: +(((totalCost * vatPercentage) / 100).toFixed(4))
    });
    this.calculateTotal('vatValue', 'totalVat', 'salesDetails');
    this.calculateNetValue(group);
  }

  calculateExpiryDate(value, date?: Date) {
    const days = +value;
    let transactionDate;
    if (date) {
      transactionDate = date;
    } else {
      transactionDate = this.salesQuotationForm.value.gregorianDate;
    }
    const expiryDate = new Date();
    expiryDate.setDate(transactionDate.getDate() + days);
    this.salesQuotationForm.patchValue({
      expiryDateGregorian: expiryDate
    });
  }

  calculateTotal(fieldName, totalName, arrayName, group?: FormGroup) {
    const array = this.salesQuotationForm.get(arrayName) as FormArray;
    const valuesArray = [];
    for (const item of array.value) {
      valuesArray.push(+item[fieldName]);
    }
    const valuesSum = valuesArray.reduce((acc, cur) => acc + cur, 0);
    this.salesQuotationForm.patchValue({
      [totalName]: valuesSum
    });

    if (group) {
      if (fieldName === 'discountValue') {
        const totalCost = group.value.totalCost ? group.value.totalCost : 0;
        group.patchValue({
          discountPercentage: +(((group.value[fieldName] * 100) / totalCost).toFixed(4))
        });
      }
      this.calculateAdditionalDiscount(group);
    }
  }

  calculateAdditionalDiscount(group: FormGroup) {
    const additionalDiscountTable = group.value.additionalDiscount ? group.value.additionalDiscount : 0;
    const cost = group.value.cost ? group.value.cost : 0;
    const quantity = group.value.quantity ? group.value.quantity : 0;
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const totalCost = this.salesQuotationForm.value.totalCost ? this.salesQuotationForm.value.totalCost : 0;
    const discountItems = this.salesQuotationForm.value.discountItems ? this.salesQuotationForm.value.discountItems : 0;
    const additionalDiscount = this.salesQuotationForm.value.additionalDiscount ? this.salesQuotationForm.value.additionalDiscount : 0;
    const value = (((cost * quantity) - discountValue) / (totalCost + discountItems)) * additionalDiscount;

    group.patchValue({
      additionalDiscount: +(value.toFixed(4))
    });
    if (value !== additionalDiscountTable) {
      this.calculateTotalDiscount(group);
    }
  }

  calculateTotalDiscount(group: FormGroup) {
    const discountValue = group.value.discountValue ? group.value.discountValue : 0;
    const additionalDiscount = group.value.additionalDiscount ? group.value.additionalDiscount : 0;
    group.patchValue({
      totalDiscount: +((discountValue + additionalDiscount).toFixed(4))
    });
    this.calculateTotalCost(group);
    this.calculateTotal('totalDiscount', 'totalDiscount', 'salesDetails');
  }

  calculateDiscountValue(group: FormGroup) {
    const discountPercentage = group.value.discountPercentage ? group.value.discountPercentage : 0;
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    group.patchValue({
      discountValue: +(((discountPercentage * totalCost) / 100).toFixed(4))
    });
    this.calculateAdditionalDiscount(group);
    this.calculateTotal('discountValue', 'discountItems', 'salesDetails');
  }

  calculateTotalItemsDiscount() {
    const totalCost = this.salesQuotationForm.value.totalCost ? this.salesQuotationForm.value.totalCost : 0;
    const discountTaxRate = this.salesQuotationForm.value.discountTaxRate ? this.salesQuotationForm.value.discountTaxRate : 0;
    this.salesQuotationForm.patchValue({
      totalDiscountTax: +(((totalCost * discountTaxRate) / 100).toFixed(4))
    });
  }

  calculateNetValue(group: FormGroup) {
    const totalCost = group.value.totalCost ? group.value.totalCost : 0;
    const vatValue = group.value.vatValue ? group.value.vatValue : 0;
    group.patchValue({
      netValue: +((totalCost + vatValue).toFixed(4))
    });
    this.calculateTotal('netValue', 'totalNet', 'salesDetails');
  }

  calculateTable() {
    for (const item of this.getSalesDetailsArray.controls) {
      const group = item as FormGroup;
      this.calculateAdditionalDiscount(group);
    }
  }

  submit() {
    this.submitted = true;
    this.loadingButton = true;
    const detailsArray = (this.salesQuotationForm.get('salesDetails') as FormArray).controls;
    if (detailsArray.length > 1) {
      (detailsArray[0] as FormGroup).controls.barCode.clearValidators();
      (detailsArray[0] as FormGroup).controls.barCode.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.itemId.clearValidators();
      (detailsArray[0] as FormGroup).controls.itemId.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.quantity.clearValidators();
      (detailsArray[0] as FormGroup).controls.quantity.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.itemType.clearValidators();
      (detailsArray[0] as FormGroup).controls.itemType.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.cost.clearValidators();
      (detailsArray[0] as FormGroup).controls.cost.updateValueAndValidity();
      (detailsArray[0] as FormGroup).controls.netValue.clearValidators();
      (detailsArray[0] as FormGroup).controls.netValue.updateValueAndValidity();
    } else {
      this.submitted = false;
      this.loadingButton = false;
      this.uiService.showError('GENERAL.rowMustBeAdded', '');
      return;
    }

    if (typeof this.salesQuotationForm.value.gregorianDate !== 'string') {
      this.salesQuotationForm.value.gregorianDate =
        this.generalService.format(this.salesQuotationForm.value.gregorianDate);
    }
    if (typeof this.salesQuotationForm.value.hijriDate !== 'string') {
      this.salesQuotationForm.value.hijriDate =
        this.generalService.formatHijriDate(this.salesQuotationForm.value.hijriDate);
    }

    if (this.salesQuotationForm.value.expiryDateGregorian) {
      if (typeof this.salesQuotationForm.value.expiryDateGregorian !== 'string') {
        this.salesQuotationForm.value.expiryDateGregorian =
          this.generalService.format(this.salesQuotationForm.value.expiryDateGregorian);
      }
      if (typeof this.salesQuotationForm.value.expiryDateHijri !== 'string') {
        this.salesQuotationForm.value.expiryDateHijri =
          this.generalService.formatHijriDate(this.salesQuotationForm.value.expiryDateHijri);
      }
    }


    for (
      let i = 0;
      i <= this.salesQuotationForm.value.payments.length - 1;
      i++
    ) {

      {
        if (this.salesQuotationForm.value.payments[i].dueDateGregorian) {
          if (
            typeof this.salesQuotationForm.value.payments[i].dueDateGregorian !==
            'string'
          ) {
            this.salesQuotationForm.value.payments[i].dueDateGregorian = this.generalService.format(
              this.salesQuotationForm.value.payments[i].dueDateGregorian
            );
          }
          if (typeof this.salesQuotationForm.value.payments[i].dueDateHijri !==
            'string') {
            this.salesQuotationForm.value.payments[i].dueDateHijri = this.generalService.formatHijriDate(
              this.salesQuotationForm.value.payments[i].dueDateHijri
            );
          }
        }
        if (this.salesQuotationForm.value.payments[i].documentDateGregorian) {
          if (
            typeof this.salesQuotationForm.value.payments[i].documentDateGregorian !==
            'string'
          ) {
            this.salesQuotationForm.value.payments[i].documentDateGregorian = this.generalService.format(
              this.salesQuotationForm.value.payments[i].documentDateGregorian
            );
          }
          if (typeof this.salesQuotationForm.value.payments[i].documentDateHijri !==
            'string') {
            this.salesQuotationForm.value.payments[i].documentDateHijri = this.generalService.formatHijriDate(
              this.salesQuotationForm.value.payments[i].documentDateHijri
            );
          }
        }
      }

    }

    if (this.salesQuotation) {
      if (this.salesQuotationForm.valid) {
        this.salesQuotationForm.value.salesDetails.splice(0, 1);
        const newsalesQuotation = {
          _id: this.salesQuotation._id,
          ...this.generalService.checkEmptyFields(this.salesQuotationForm.value)
        };
        this.data.put(salesQuotationApi, newsalesQuotation).subscribe(res => {
          this.uiService.isLoading.next(false);
          this.router.navigate(['/sales/salesQuotation']);
          this.uiService.showSuccess('GENERAL.updatedSuccessfully', '');
        }, err => {
          this.uiService.isLoading.next(false);
          this.loadingButton = false;
          this.uiService.showErrorMessage(err);
        });
        this.loadingButton = false;
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    } else {
      if (this.salesQuotationForm.valid) {
        this.salesQuotationForm.value.salesDetails.splice(0, 1);
        const formValue = {
          ...this.generalService.checkEmptyFields(this.salesQuotationForm.value)
        };
        this.subscriptions.push(
          this.data.post(salesQuotationApi, formValue).subscribe(res => {
            this.loadingButton = false;
            this.submitted = false;
            this.uiService.isLoading.next(false);
            this.salesQuotationForm.reset();
            this.salesQuotationForm.patchValue({
              isActive: true,
              gregorianDate: new Date()
            });
            this.uiService.showSuccess('GENERAL.addedSuccessfully', '');
          },
            err => {
              this.loadingButton = false;
              this.uiService.isLoading.next(false);
              this.uiService.showErrorMessage(err);
            }
          )
        );
      } else {
        this.loadingButton = false;
        this.uiService.showError('GENERAL.thereAreInvalidInputs', '');
        this.uiService.isLoading.next(false);
      }
    }
  }

  private initForm() {
    let code = '';
    let salesQuotationPeriodic = false;
    let salesQuotationStatus = 'unposted';
    let salesQuotationDescriptionAr = '';
    let salesQuotationDescriptionEn = '';
    let gregorianDate = new Date();
    let hijriDate = null;
    let customerId = '';
    let customerBranchId = '';
    let referenceNumber = '';
    let delegateId = '';
    let currencyId = '';
    let currencyExchangeRate = 0;
    let offerValidity = null;
    let expiryDateGregorian = null;
    let expiryDateHijri = null;
    let discountTaxType = 'without';
    let discountTaxRate = 0;
    let discountItems = 0;
    let totalDiscount = 0;
    let totalCost = 0;
    let totalDiscountTax = 0;
    let totalVat = 0;
    let additionalDiscount = 0;
    let totalBalance = 0;
    let totalNet = 0;
    let variablesArray = new FormArray([]);
    let salesDetailsArray = new FormArray([
      new FormGroup({
        itemType: new FormControl('STORE_CATEGORY', Validators.required),
        barCode: new FormControl('', Validators.required),
        itemId: new FormControl('', Validators.required),
        variables: variablesArray,
        quantity: new FormControl(1, Validators.required),
        freeQuantity: new FormControl(0),
        itemUnitId: new FormControl(''),
        cost: new FormControl(0, Validators.required),
        discountPercentage: new FormControl(0),
        discountValue: new FormControl(0),
        additionalDiscount: new FormControl(0),
        totalDiscount: new FormControl(0),
        totalCost: new FormControl(0),
        vatPercentage: new FormControl(0),
        vatValue: new FormControl(0),
        netValue: new FormControl(0, Validators.required),
        descriptionAr: new FormControl(''),
        descriptionEn: new FormControl(''),
      })
    ]);
    let paymentsArray = new FormArray([
      new FormGroup({
        paymentMethod: new FormControl('CASH', Validators.required),
        accountId: new FormControl('', Validators.required),
        description: new FormControl(''),
        value: new FormControl(0, Validators.required),
        balance: new FormControl(0, Validators.required),
        dueDateGregorian: new FormControl(null),
        dueDateHijri: new FormControl(null),
        documentNumber: new FormControl(0),
        documentDateGregorian: new FormControl(null),
        documentDateHijri: new FormControl(null),
        customerId: new FormControl('', Validators.required)
      })
    ]);
    let isActive = true;

    if (this.salesQuotation) {
      code = this.salesQuotation.code;
      salesQuotationPeriodic = this.salesQuotation.salesQuotationPeriodic;
      salesQuotationStatus = this.salesQuotation.salesQuotationStatus;
      salesQuotationDescriptionAr = this.salesQuotation.salesQuotationDescriptionAr;
      salesQuotationDescriptionEn = this.salesQuotation.salesQuotationDescriptionEn;
      gregorianDate = new Date(this.salesQuotation.gregorianDate + 'UTC');
      hijriDate = this.salesQuotation.hijriDate;
      customerId = this.salesQuotation.customerId;
      customerBranchId = this.salesQuotation.customerBranchId;
      referenceNumber = this.salesQuotation.referenceNumber;
      delegateId = this.salesQuotation.delegateId;
      currencyId = this.salesQuotation.currencyId;
      currencyExchangeRate = this.salesQuotation.currencyExchangeRate;
      offerValidity = this.salesQuotation.offerValidity;
      expiryDateGregorian = new Date(this.salesQuotation.expiryDateGregorian + 'UTC');
      expiryDateHijri = this.salesQuotation.expiryDateHijri;
      discountTaxType = this.salesQuotation.discountTaxType;
      discountTaxRate = this.salesQuotation.discountTaxRate;
      isActive = this.salesQuotation.isActive;
      discountItems = this.salesQuotation.discountItems;
      totalDiscount = this.salesQuotation.totalDiscount;
      totalCost = this.salesQuotation.totalCost;
      totalDiscountTax = this.salesQuotation.totalDiscountTax;
      totalVat = this.salesQuotation.totalVat;
      additionalDiscount = this.salesQuotation.additionalDiscount;
      totalBalance = this.salesQuotation.totalBalance;
      totalNet = this.salesQuotation.totalNet;
      salesDetailsArray = new FormArray([]);
      for (const control of this.salesQuotation.salesDetails) {
        variablesArray = new FormArray([]);
        salesDetailsArray.push(
          new FormGroup({
            itemType: new FormControl('', Validators.required),
            barCode: new FormControl('', Validators.required),
            itemId: new FormControl('', Validators.required),
            variables: new FormArray([]),
            quantity: new FormControl(1, Validators.required),
            freeQuantity: new FormControl(0),
            itemUnitId: new FormControl(''),
            cost: new FormControl(0, Validators.required),
            discountPercentage: new FormControl(0),
            discountValue: new FormControl(0),
            additionalDiscount: new FormControl(0),
            totalDiscount: new FormControl(0),
            totalCost: new FormControl(0),
            vatPercentage: new FormControl(0),
            vatValue: new FormControl(0),
            netValue: new FormControl(0, Validators.required),
            descriptionAr: new FormControl(''),
            descriptionEn: new FormControl(''),
          })
        );
        for (const item of control.variables) {
          variablesArray.push(
            new FormGroup({
              variableId: new FormControl(item.variableId),
              itemVariableNameId: new FormControl(item.itemVariableNameId)
            })
          );
        }
        this.barcodes = [];
        this.barcodes.push(control.barCode);
        salesDetailsArray.push(
          new FormGroup({
            itemType: new FormControl(control.itemType, Validators.required),
            barCode: new FormControl(control.barCode, Validators.required),
            itemId: new FormControl(control.itemId, Validators.required),
            variables: variablesArray,
            quantity: new FormControl(control.quantity, Validators.required),
            freeQuantity: new FormControl(control.freeQuantity),
            itemUnitId: new FormControl(control.itemUnitId),
            cost: new FormControl(control.cost, Validators.required),
            discountPercentage: new FormControl(control.discountPercentage),
            discountValue: new FormControl(control.discountValue),
            additionalDiscount: new FormControl(control.additionalDiscount),
            totalDiscount: new FormControl(control.totalDiscount),
            totalCost: new FormControl(control.totalCost),
            vatPercentage: new FormControl(control.vatPercentage),
            vatValue: new FormControl(control.vatValue),
            netValue: new FormControl(control.netValue, Validators.required),
            descriptionAr: new FormControl(control.descriptionAr),
            descriptionEn: new FormControl(control.descriptionEn),
          })
        );
      }

      paymentsArray = new FormArray([]);
      for (const control of this.salesQuotation.payments) {
        paymentsArray.push(
          new FormGroup({
            paymentMethod: new FormControl(control.paymentMethod, Validators.required),
            accountId: new FormControl(control.accountId, Validators.required),
            description: new FormControl(control.description),
            value: new FormControl(control.value, Validators.required),
            balance: new FormControl(control.balance, Validators.required),
            dueDateGregorian: new FormControl(new Date(control.dueDateGregorian + 'UTC')),
            dueDateHijri: new FormControl(control.dueDateHijri),
            documentNumber: new FormControl(control.documentNumber),
            documentDateGregorian: new FormControl(new Date(control.documentDateGregorian + 'UTC')),
            documentDateHijri: new FormControl(control.documentDateHijri),
            customerId: new FormControl(control.customerId, Validators.required),
          })
        )
      }
    }
    this.salesQuotationForm = new FormGroup({
      code: new FormControl(code, Validators.required),
      salesQuotationPeriodic: new FormControl(salesQuotationPeriodic),
      salesQuotationStatus: new FormControl(salesQuotationStatus),
      salesQuotationDescriptionAr: new FormControl(salesQuotationDescriptionAr),
      salesQuotationDescriptionEn: new FormControl(salesQuotationDescriptionEn),
      gregorianDate: new FormControl(gregorianDate, Validators.required),
      hijriDate: new FormControl(hijriDate, Validators.required),
      customerId: new FormControl(customerId, Validators.required),
      customerBranchId: new FormControl(customerBranchId),
      referenceNumber: new FormControl(referenceNumber),
      delegateId: new FormControl(delegateId, Validators.required),
      currencyId: new FormControl(currencyId, Validators.required),
      currencyExchangeRate: new FormControl(currencyExchangeRate),
      offerValidity: new FormControl(offerValidity),
      expiryDateGregorian: new FormControl(expiryDateGregorian),
      expiryDateHijri: new FormControl(expiryDateHijri),
      discountTaxType: new FormControl(discountTaxType, Validators.required),
      discountTaxRate: new FormControl(discountTaxRate, Validators.required),
      discountItems: new FormControl(discountItems, Validators.required),
      totalDiscount: new FormControl(totalDiscount, Validators.required),
      totalCost: new FormControl(totalCost, Validators.required),
      totalDiscountTax: new FormControl(totalDiscountTax, Validators.required),
      totalVat: new FormControl(totalVat, Validators.required),
      additionalDiscount: new FormControl(additionalDiscount, Validators.required),
      totalBalance: new FormControl(totalBalance, Validators.required),
      totalNet: new FormControl(totalNet, Validators.required),
      salesDetails: salesDetailsArray,
      payments: paymentsArray,
      isActive: new FormControl(isActive, Validators.required)
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
