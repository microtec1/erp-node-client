export interface ICustomer {
  _id: string;
  image: string;
  code: string;
  customerNameAr: string;
  customerNameEn: string;
  pricingPolicyId: string;
  customersGroupId: string;
  currencyId: string;
  exchangeRate: number;
  salesPersonId: string;
  customersClassificationId: string;
  identity: {
    nationalityId: string;
    identityNumber: string;
    identitySource: string;
    releaseGregorianDate: string;
    releaseHijriDate: string;
  };
  officialDocuments: {
    tax: {
      taxRegistrationNumber: string;
      taxCardNumber: string;
      missionCode: string;
      missionName: string;
      businessAddress: string;
      fileNumber: string;
      businessName: string;
      financierName: string;
    };
    commercialRegistration: {
      commercialName: string;
      commercialTrait: string;
      commercialBrand: string;
      traderName: string;
      nationalityId: string;
      financialSystem: string;
      commercialEligibility: string;
      tradeType: string;
      startBusinessGregorianDate: string;
      startBusinessHijriDate: string;
    };
  };
  openingBalances: [
    {
      balance: string;
      netBalance: string;
      currencyId: string;
      exchangeRate: number;
      accountSide: string;
      dueGregorianDate: string;
      dueHijriDate: string;
      referenceNumber: string;
      notes: string;
    }
  ];
  accountingInfo: {
    salesDuePeriod: string;
    glAccountId: string;
    netOpeningBalance: number;
    creditLimit: string;
    cannotPassCreditLimit: boolean;
    salesPersonCommission: string;
    banks: [
      {
        bankId: string;
        bankAccountNumber: string;
      }
    ];
  };
  contact: {
    address: {
      cityId: string;
      regionId: string;
      address: string;
    },
    contact: {
      phone: string;
      internalNumber: string;
      mobile: string;
      fax: string;
    },
    mail: {
      postalCode: string;
      postalBox: string;
    }
    socialAccounts: {
      website: string
    }
  };
  branches: [
    {
      code: string;
      branchName: string;
      cityId: string;
      regionId: string;
      address: string;
      phone: string;
      fax: string;
      mobile: string;
      email: string;
      officials: [
        {
          name: string;
          job: string;
          phone: string;
          internalNumber: string;
          mobile: string;
          email: string;
        }
      ]
    }
  ];
  files: [
    {
      attachmentTypeId: string;
      file: string;
    }
  ];
  customerTransactions: [
    {
      branchId: string
    }
  ];
  isActive: boolean;
}
