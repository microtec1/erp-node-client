export interface ICustomersClassification {
  _id: string;
  code: string;
  customersClassificationNameAr: string;
  customersClassificationNameEn: string;
  classificationType: string;
  parentId: string;
  companyId: string;
  isActive: boolean;
  level?: number;
  children: ICustomersClassification[];
}
