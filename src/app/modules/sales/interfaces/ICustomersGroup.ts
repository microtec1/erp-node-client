export interface ICustomersGroup {
  _id: string;
  image: string;
  code: string;
  customersGroupNameAr: string;
  customersGroupNameEn: string;
  isActive: boolean;
}
