export interface IDicountClassification {
  _id: string;
  image: string;
  code: string;
  discountClassificationNameAr: string;
  discountClassificationNameEn?: string;
  isActive: boolean;
  discountClassificationDetailsList: [
    {
      fromPercentage: number;
      toPercentage: number;
      items: string[];
      applyOn: string;
    }
  ];
}
