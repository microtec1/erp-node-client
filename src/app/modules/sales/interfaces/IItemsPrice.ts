export interface IItemsPrice   {
  _id: string;
  image: string;
  pricingPolicyId: string;
  automaticCalculation: string;
  automaticCalculationPercentage: number;
  isActive: boolean;
  itemsPriceDetailsList: [
    {
      itemId: string,
      itemPrice: number,
      profitCostPercentage: number
    }
  ];
}
