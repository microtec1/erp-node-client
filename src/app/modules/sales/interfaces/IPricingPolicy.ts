export interface IPricingPolicy {
  _id: string;
  image: string;
  code: string;
  pricingPolicyNameAr: string;
  pricingPolicyNameEn: string;
  isActive: boolean;
}
