export interface IReturnReason {
  _id: string;
  image: string;
  code: string;
  returnReasonNameAr: string;
  returnReasonNameEn?: string;
  isActive: boolean;
}
