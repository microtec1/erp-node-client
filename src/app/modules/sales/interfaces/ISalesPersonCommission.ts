export interface ISalesPersonCommission {
  _id: string;
  image: string;
  salespersonId: string;
  commissions: [
    {
      commissionType: string,
      sliceType: string,
      from: number,
      to: number,
      policyType: string,
      ratio: number,
      appliedOn: string,
      itemsControl: string[],
      itemsGroupsControl: string[],
      items: [
        {
          itemId: string
        }
      ],
      itemsGroups: [{
        itemsGroupId: string;
      }],
    }
  ];
  isActive: boolean;
}
