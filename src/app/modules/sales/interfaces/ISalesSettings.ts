export interface ISalesSettings {
  company: {
    checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransaction: boolean;
    checkingTheCustomerBalanceWhoExceedTheDueDateInTheTransactionOptions: string;
    checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactions: boolean;
    checkingTheCustomerBalanceWhoExceedTheDueDateWhileTransactionsOptions: string;
    cannotExceedingTheCustomerLimitInSalesInvoice: boolean;
    overdraft: boolean;
    cannotDuplicatingTheCommunicationDetailsOfTheCustomer: boolean;
    preventSellingItemIfItDosenotHasRegisterNumberInFoodAndDrugAuthority: boolean;
  };
  branch: {
    branchId: string;
    directInfluenceOnTheInventory: boolean;
    directInfluenceOnTheInventoryOptions: string;
    displayingMoreThanOneSalesmanInTheSalesOrderOrInTheInvoice: boolean;
    defaultPaymentType: string;
    defaultPaymentAccount: string;
    defaultCustomer: string;
    defaultSalesPolicy: string;
    loadLastSalePrice: boolean;
    loadLastSalePriceOptions: string;
    preventSellingItemsNotFoundInTheSalesOrder: boolean;
    sellingByAPriceLessThanTheCost: boolean;
    exceedingTheQuantityOfTheItemsFoundInTheSalesOrder: boolean;
    exceedingTheTransactionDateFoundInTheInvoiceDuringOpeningPeriod: boolean;
    mandatoryToAddTheSalesmanToTheInvoice: boolean;
    activateCustomerBranches: boolean;
    preventDuplicationOfReferenceNumberInTheInvoice: boolean;
    salesTransactionCycle: {
      priceOffer: boolean;
      salesContract: boolean;
      salesOrder: boolean;
      salesInvoice: boolean;
      salesReturns: boolean;
    }
  };
}

