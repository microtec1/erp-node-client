export interface ITax {
  _id: string;
  image: string;
  code: string;
  taxNameAr: string;
  taxNameEn: string;
  ratio: number;
  taxClassification: string;
  taxType: string;
  glAccountId?: string;
  appliedOnAllItems: boolean;
  isActive: boolean;
}
