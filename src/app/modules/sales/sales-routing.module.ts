import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReturnReasonsComponent } from './components/files/return-reasons/return-reasons.component';
import { AddReturnReasonComponent } from './components/files/return-reasons/add-return-reason/add-return-reason.component';
import { TaxesComponent } from './components/files/taxes/taxes.component';
import { AddTaxComponent } from './components/files/taxes/add-tax/add-tax.component';
import { CustomersGroupsComponent } from './components/files/customers-groups/customers-groups.component';
import { AddCustomersGroupComponent } from './components/files/customers-groups/add-customers-group/add-customers-group.component';
// tslint:disable-next-line:max-line-length
import { CustomersClassificationsComponent } from './components/files/customers-classifications/customers-classifications.component';
// tslint:disable-next-line:max-line-length
import { AddCustomersClassificationComponent } from './components/files/customers-classifications/add-customers-classification/add-customers-classification.component';
import { PricingPoliciesComponent } from './components/files/pricing-policies/pricing-policies.component';
import { AddPricingPolicyComponent } from './components/files/pricing-policies/add-pricing-policy/add-pricing-policy.component';
import { DiscountClassificationsComponent } from './components/files/discount-classifications/discount-classifications.component';
// tslint:disable-next-line:max-line-length
import { AddDiscountClassificationComponent } from './components/files/discount-classifications/add-discount-classification/add-discount-classification.component';
// tslint:disable-next-line:max-line-length
import { CustomersComponent } from './components/files/customers/customers.component';
import { AddCustomerComponent } from './components/files/customers/add-customer/add-customer.component';
import { SalesPersonsCommissionsComponent } from './components/files/sales-persons-commissions/sales-persons-commissions.component';
import { AddCommissionComponent } from './components/files/sales-persons-commissions/add-commission/add-commission.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ItemsPricesComponent } from './components/files/items-prices/items-prices.component';
import { AddItemsPriceComponent } from './components/files/items-prices/add-items-price/add-items-price.component';
import { SalesQuotationComponent } from './components/transactions/sales-quotation/sales-quotation.component';
import { AddSalesQuotationComponent } from './components/transactions/sales-quotation/add-sales-quotation/add-sales-quotation.component';
import { SalesContractComponent } from './components/transactions/sales-contract/sales-contract.component';
import { AddSalesContractComponent } from './components/transactions/sales-contract/add-sales-contract/add-sales-contract.component';
import { SalesOrderComponent } from './components/transactions/sales-order/sales-order.component';
import { AddSalesOrderComponent } from './components/transactions/sales-order/add-sales-order/add-sales-order.component';
import { SalesInvoiceComponent } from './components/transactions/sales-invoice/sales-invoice.component';
import { AddSalesInvoiceComponent } from './components/transactions/sales-invoice/add-sales-invoice/add-sales-invoice.component';
import { SalesReturnComponent } from './components/transactions/sales-return/sales-return.component';
import { AddSalesReturnComponent } from './components/transactions/sales-return/add-sales-return/add-sales-return.component';

const routes: Routes = [
  { path: '', redirectTo: 'returnReasons', pathMatch: 'full' },
  { path: 'returnReasons', component: ReturnReasonsComponent},
  { path: 'returnReasons/add', component: AddReturnReasonComponent},
  { path: 'returnReasons/edit/:id', component: AddReturnReasonComponent},
  { path: 'returnReasons/details/:id', component: AddReturnReasonComponent},
  { path: 'taxes', component: TaxesComponent},
  { path: 'taxes/add', component: AddTaxComponent},
  { path: 'taxes/edit/:id', component: AddTaxComponent},
  { path: 'taxes/details/:id', component: AddTaxComponent},
  { path: 'customersGroups', component: CustomersGroupsComponent},
  { path: 'customersGroups/add', component: AddCustomersGroupComponent},
  { path: 'customersGroups/edit/:id', component: AddCustomersGroupComponent},
  { path: 'customersGroups/details/:id', component: AddCustomersGroupComponent},
  { path: 'customersClassifications', component: CustomersClassificationsComponent },
  { path: 'customersClassifications/add', component: AddCustomersClassificationComponent },
  { path: 'pricingPolicies', component: PricingPoliciesComponent},
  { path: 'pricingPolicies/add', component: AddPricingPolicyComponent},
  { path: 'pricingPolicies/edit/:id', component: AddPricingPolicyComponent},
  { path: 'pricingPolicies/details/:id', component: AddPricingPolicyComponent},
  { path: 'discountClassifications', component: DiscountClassificationsComponent},
  { path: 'discountClassifications/add', component: AddDiscountClassificationComponent},
  { path: 'discountClassifications/edit/:id', component: AddDiscountClassificationComponent},
  { path: 'discountClassifications/details/:id', component: AddDiscountClassificationComponent},
  { path: 'customers', component: CustomersComponent},
  { path: 'customers/add', component: AddCustomerComponent},
  { path: 'customers/edit/:id', component: AddCustomerComponent},
  { path: 'customers/details/:id', component: AddCustomerComponent},
  { path: 'salesPersonsCommissions', component: SalesPersonsCommissionsComponent},
  { path: 'salesPersonsCommissions/add', component: AddCommissionComponent},
  { path: 'salesPersonsCommissions/edit/:id', component: AddCommissionComponent},
  { path: 'salesPersonsCommissions/details/:id', component: AddCommissionComponent},
  { path: 'settings', component: SettingsComponent},
  { path: 'itemsPrices', component: ItemsPricesComponent},
  { path: 'itemsPrices/add', component: AddItemsPriceComponent},
  { path: 'itemsPrices/edit/:id', component: AddItemsPriceComponent},
  { path: 'itemsPrices/details/:id', component: AddItemsPriceComponent},


  { path: 'salesQuotation', component: SalesQuotationComponent},
  { path: 'salesQuotation/add', component: AddSalesQuotationComponent},
  { path: 'salesQuotation/edit/:id', component: AddSalesQuotationComponent},
  { path: 'salesQuotation/details/:id', component: AddSalesQuotationComponent},

  { path: 'salesContract', component: SalesContractComponent},
  { path: 'salesContract/add', component: AddSalesContractComponent},
  { path: 'salesContract/edit/:id', component: AddSalesContractComponent},
  { path: 'salesContract/details/:id', component: AddSalesContractComponent},

  { path: 'salesOrder', component: SalesOrderComponent},
  { path: 'salesOrder/add', component: AddSalesOrderComponent},
  { path: 'salesOrder/edit/:id', component: AddSalesOrderComponent},
  { path: 'salesOrder/details/:id', component: AddSalesOrderComponent},

  { path: 'salesInvoice', component: SalesInvoiceComponent},
  { path: 'salesInvoice/add', component: AddSalesInvoiceComponent},
  { path: 'salesInvoice/edit/:id', component: AddSalesInvoiceComponent},
  { path: 'salesInvoice/details/:id', component: AddSalesInvoiceComponent},

  { path: 'salesReturn', component: SalesReturnComponent},
  { path: 'salesReturn/add', component: AddSalesReturnComponent},
  { path: 'salesReturn/edit/:id', component: AddSalesReturnComponent},
  { path: 'salesReturn/details/:id', component: AddSalesReturnComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
