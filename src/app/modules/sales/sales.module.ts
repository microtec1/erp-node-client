import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SalesRoutingModule } from './sales-routing.module';
import { ReturnReasonsComponent } from './components/files/return-reasons/return-reasons.component';
import { AddReturnReasonComponent } from './components/files/return-reasons/add-return-reason/add-return-reason.component';
import { TaxesComponent } from './components/files/taxes/taxes.component';
import { AddTaxComponent } from './components/files/taxes/add-tax/add-tax.component';
import { CustomersGroupsComponent } from './components/files/customers-groups/customers-groups.component';
// tslint:disable-next-line:max-line-length
import { AddCustomersGroupComponent } from './components/files/customers-groups/add-customers-group/add-customers-group.component';
import { CustomersClassificationsComponent } from './components/files/customers-classifications/customers-classifications.component';
// tslint:disable-next-line:max-line-length
import { AddCustomersClassificationComponent } from './components/files/customers-classifications/add-customers-classification/add-customers-classification.component';
// tslint:disable-next-line:max-line-length
import { CustomersClassificationsTreeComponent } from './components/files/customers-classifications/customers-classifications-tree/customers-classifications-tree.component';
import { PricingPoliciesComponent } from './components/files/pricing-policies/pricing-policies.component';
import { AddPricingPolicyComponent } from './components/files/pricing-policies/add-pricing-policy/add-pricing-policy.component';
import { DiscountClassificationsComponent } from './components/files/discount-classifications/discount-classifications.component';
// tslint:disable-next-line:max-line-length
// tslint:disable-next-line:max-line-length
import { AddDiscountClassificationComponent } from './components/files/discount-classifications/add-discount-classification/add-discount-classification.component';
import { CustomersComponent } from './components/files/customers/customers.component';
import { AddCustomerComponent } from './components/files/customers/add-customer/add-customer.component';
import { SalesPersonsCommissionsComponent } from './components/files/sales-persons-commissions/sales-persons-commissions.component';
import { AddCommissionComponent } from './components/files/sales-persons-commissions/add-commission/add-commission.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ItemsPricesComponent } from './components/files/items-prices/items-prices.component';
import { AddItemsPriceComponent } from './components/files/items-prices/add-items-price/add-items-price.component';
import { SalesQuotationComponent } from './components/transactions/sales-quotation/sales-quotation.component';
import { SalesContractComponent } from './components/transactions/sales-contract/sales-contract.component';
import { SalesOrderComponent } from './components/transactions/sales-order/sales-order.component';
import { SalesInvoiceComponent } from './components/transactions/sales-invoice/sales-invoice.component';
import { SalesReturnComponent } from './components/transactions/sales-return/sales-return.component';
import { AddSalesQuotationComponent } from './components/transactions/sales-quotation/add-sales-quotation/add-sales-quotation.component';
import { AddSalesContractComponent } from './components/transactions/sales-contract/add-sales-contract/add-sales-contract.component';
import { AddSalesInvoiceComponent } from './components/transactions/sales-invoice/add-sales-invoice/add-sales-invoice.component';
import { AddSalesOrderComponent } from './components/transactions/sales-order/add-sales-order/add-sales-order.component';
import { AddSalesReturnComponent } from './components/transactions/sales-return/add-sales-return/add-sales-return.component';

@NgModule({
  declarations: [
    ReturnReasonsComponent,
    AddReturnReasonComponent,
    TaxesComponent,
    AddTaxComponent,
    CustomersGroupsComponent,
    AddCustomersGroupComponent,
    CustomersClassificationsComponent,
    AddCustomersClassificationComponent,
    CustomersClassificationsTreeComponent,
    PricingPoliciesComponent,
    AddPricingPolicyComponent,
    DiscountClassificationsComponent,
    AddDiscountClassificationComponent,
    CustomersComponent,
    AddCustomerComponent,
    SalesPersonsCommissionsComponent,
    AddCommissionComponent,
    SettingsComponent,
    ItemsPricesComponent,
    AddItemsPriceComponent,
    SalesQuotationComponent,
    SalesContractComponent,
    SalesOrderComponent,
    SalesInvoiceComponent,
    SalesReturnComponent,
    AddSalesQuotationComponent,
    AddSalesContractComponent,
    AddSalesInvoiceComponent,
    AddSalesOrderComponent,
    AddSalesReturnComponent
  ],
  imports: [
    SharedModule,
    SalesRoutingModule
  ]
})
export class SalesModule { }
