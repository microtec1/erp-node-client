import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ICustomersClassification } from '../interfaces/ICustomersClassification';

@Injectable({
  providedIn: 'root'
})
export class CustomersClassificationsService {
  customersClassificationsChanged = new Subject<ICustomersClassification[]>();
  fillFormChange = new Subject<ICustomersClassification>();

  constructor() { }

}
