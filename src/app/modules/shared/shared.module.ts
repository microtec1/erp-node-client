import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CustomFormsModule } from "ng2-validation";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { TooltipModule } from "ng2-tooltip-directive";
import { ModalModule } from "ngx-bootstrap/modal";
import { ToastrModule } from "ngx-toastr";
import { SpinnerComponent } from "src/app/common/components/spinner/spinner.component";
import { NoDataComponent } from "src/app/common/components/no-data/no-data.component";
import { ConfirmModalComponent } from "src/app/common/components/confirm-modal/confirm-modal.component";
import { GeneralService } from "../general/services/general.service";
import { NgxSelectModule } from "ngx-select-ex";
import { NgxDropzoneModule } from "ngx-dropzone";
import { TabsModule } from "ngx-bootstrap/tabs";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { CostCentersService } from "../accounting/modules/gl/services/cost-centers.service";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ChartOfAccountsService } from "../accounting/modules/gl/services/chart-of-accounts.service";
import { DropdownDirective } from "src/app/common/directives/dropdown.directive";
import { TransactionsNumberingService } from "../general/services/transactions-numbering.service";
import { SuppliersClassificationsService } from "../purchases/services/suppliers-classifications.service";
import { CustomersClassificationsService } from "../sales/services/customers-classifications.service";
import {
  NgbDatepickerModule,
  NgbCalendar,
  NgbCalendarIslamicUmalqura,
  NgbDatepickerI18n,
} from "@ng-bootstrap/ng-bootstrap";
import { IslamicI18n } from "../general/services/islamic-datepicker";
import { VariablesModalComponent } from "src/app/common/components/variables-modal/variables-modal.component";
import { NgxPaginationModule } from "ngx-pagination";
import { DataService } from "src/app/common/services/shared/data.service";
import { RouterModule } from "@angular/router";
import { FilterationComponent } from "../accounting/modules/cheques/components/transactions/filteration/filteration.component";
import { BarcodeModalComponent } from "../../common/components/barcode-modal/barcode-modal.component";
import { BatchDataModalComponent } from "../../common/components/batch-data-modal/batch-data-modal.component";
import { ReportsFilterationComponent } from "src/app/common/components/reports-filteration/reports-filteration.component";
import { ReportDataComponent } from "../../common/components/report-data/report-data.component";
import { AgGridModule } from "ag-grid-angular/main";
import { NumbersOnlyDirective } from "src/app/modules/accounting/modules/gl/directives/numbers-only.directive";
import { NgSelectModule } from "@ng-select/ng-select";
import { EntriesModalComponent } from "src/app/common/components/entries-modal/entries-modal.component";

@NgModule({
  declarations: [
    SpinnerComponent,
    NoDataComponent,
    ConfirmModalComponent,
    VariablesModalComponent,
    DropdownDirective,
    FilterationComponent,
    BarcodeModalComponent,
    BatchDataModalComponent,
    ReportsFilterationComponent,
    ReportDataComponent,
    NumbersOnlyDirective,
    EntriesModalComponent,
  ],
  imports: [
    CommonModule,
    CustomFormsModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    TooltipModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxSelectModule,
    NgxDropzoneModule,
    NgxPaginationModule,
    RouterModule,
    NgbDatepickerModule,
    BsDropdownModule.forRoot(),
    // AgGridModule.withComponents([ReportDataComponent]),
    NgSelectModule,
  ],
  exports: [
    CommonModule,
    RouterModule,
    CustomFormsModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    TooltipModule,
    ModalModule,
    FilterationComponent,
    SpinnerComponent,
    NoDataComponent,
    NgxSelectModule,
    NgxDropzoneModule,
    TabsModule,
    BsDatepickerModule,
    BsDropdownModule,
    DropdownDirective,
    NgbDatepickerModule,
    NgxPaginationModule,
    ReportsFilterationComponent,
    ReportDataComponent,
    NumbersOnlyDirective,
    NgSelectModule,
  ],
  entryComponents: [
    ConfirmModalComponent,
    VariablesModalComponent,
    BarcodeModalComponent,
    BatchDataModalComponent,
    EntriesModalComponent,
  ],
  providers: [
    GeneralService,
    CostCentersService,
    ChartOfAccountsService,
    TransactionsNumberingService,
    SuppliersClassificationsService,
    CustomersClassificationsService,
    DataService,
    { provide: NgbCalendar, useClass: NgbCalendarIslamicUmalqura },
    { provide: NgbDatepickerI18n, useClass: IslamicI18n },
  ],
})
export class SharedModule {}
